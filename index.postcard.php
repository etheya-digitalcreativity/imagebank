<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set up constant for front end activities
define("WORKSPACE","FE");

// Set the include path for files used in this script
ini_set("include_path","resources/includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Send HTTP header and don't cache
pix_http_headers("html","");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

$objPixariaPostcard = new PixariaPostcard ();

class PixariaPostcard {
	
	var $imagedata;
	var $name;
	var $email;
	var $id;
	var $font_name;
	var $recipient;
	var $send_text;
	var $card_text;
	var $card_size;
	var $postcard_text;
	
	/*
	*	
	*	
	*
	*/
	function PixariaPostcard () {
	
		global $cfg, $smarty, $objEnvData, $ses;
		
		// Check postcards are on
		if (!$cfg['set']['func_postcards']) {
			
			// Set redirect url
			$meta_url	= $cfg['sys']['base_url'];
			
			// Print out the waiting screen
			$smarty->pixProcessing($meta_url,"1");
			
			// Stop running script
			exit;
			
		}
		
		if (!is_numeric($ses['psg_userid'])) {
			
			// Get the user's name and e-mail address
			$this->name		= $objEnvData->fetchGlobal('name');
			$this->email	= $objEnvData->fetchGlobal('email');
		
		} else { // Postcards on for registered users only
			
			// Get information about the user sending the message
			$sender = getUserContactDetails($ses['psg_userid']);
			
			// Load information needed to send the mail
			$this->email	= $sender[1];
			$this->name		= $sender[0];
		
		}
		
		$this->id				= $objEnvData->fetchGlobal('id');
		$this->font_name		= $objEnvData->fetchGlobal('font_name');
		$this->recipient		= $objEnvData->fetchGlobal('recipient');
		$this->send_text		= ereg_replace('"','&quot;',stripslashes($objEnvData->fetchGlobal('postcard_text')));
		$this->card_text		= ereg_replace('&quot;','"',stripslashes($objEnvData->fetchGlobal('postcard_text')));
		$this->postcard_text	= stripslashes($objEnvData->fetchGlobal('postcard_text'));

		switch ($objEnvData->fetchGlobal('cmd')) {
		
			case "postcardImage":
				$this->postcardImage();			
			break;
			
			case "sendPostcard":
				$this->sendPostcard();			
			break;
			
			case "formCheckPostcard":
				$this->formCheckPostcard();
			break;
			
			default:
				$this->formComposePostcard();
			break;
		
		}
		
		exit;
		
	}
	
	/*
	*	
	*	
	*
	*/
	function formComposePostcard () {
		
		global $smarty, $cfg, $ses, $objEnvData;
		
		if (!$ses['psg_userid'] && $cfg['set']['func_postcards_user'] == "1") {
		
			// If the user isn't logged in, bounce to the login screen
			pix_authorise_user("basic");
		
		}
		
		$smarty->assign("page_title",$GLOBALS['_STR_']['POSTCARD_27']);
		$smarty->assign("id",$this->id);
		$smarty->assign("font_name",$this->font_name);
		$smarty->assign("recipient",$this->recipient);
		$smarty->assign("return",$_SERVER['HTTP_REFERER']);
		$smarty->assign("card_size",$this->getCardSize());
		
		$smarty->assign("image_url",$cfg['sys']['base_url'].$cfg['fil']['index_postcard']."?cmd=postcardImage&amp;font_name=$this->font_name&amp;id=$this->id&amp;postcard_text=$this->postcard_text");
		
		// Print out the card editor page
		$smarty->pixDisplay('index.postcard/index.postcard.01.tpl');
	
	}
	
	/*
	*	
	*	
	*
	*/
	function formCheckPostcard () {
	
		global $smarty, $cfg, $ses, $objEnvData;
		
		$smarty->assign("page_title",$GLOBALS['_STR_']['POSTCARD_26']);
		$smarty->assign("font_name",$this->font_name);
		$smarty->assign("id",$this->id);
		$smarty->assign("send_text",$this->send_text);
		$smarty->assign("card_text",$this->card_text);
		$smarty->assign("name",$this->name);
		$smarty->assign("email",$this->email);
		$smarty->assign("recipient",$this->recipient);
		$smarty->assign("return",$objEnvData->fetchGlobal('return'));
		$smarty->assign("card_size",$this->getCardSize());
		
		if ($action && (!$this->email || !$this->recipient || !$this->name || !pix_validate_email($this->recipient) || !pix_validate_email($this->email))) {
		
			$smarty->assign("checked",false);
		
		} else {
		
			$smarty->assign("checked",true);
		
		}
		
		$smarty->assign("image_url",$cfg['sys']['base_url'].$cfg['fil']['index_postcard']."?cmd=postcardImage&amp;font_name=$this->font_name&amp;id=$this->id&amp;postcard_text=$this->postcard_text");
		
		// Print out the card preview page
		$smarty->pixDisplay('index.postcard/index.postcard.02.tpl');
	
	}
	
	/*
	*	
	*	
	*
	*/
	function sendPostcard () {
	
		global $smarty, $cfg, $ses, $objEnvData;
		
		// If there is a recipient e-mail address
		if ($this->recipient && $this->email && pix_validate_email($this->email)) {
		
			// Create the image and encode it as a base 64 text stream
			$this->imagedata = $this->outputPostcard($this->font_name,$this->imagefile,$this->card_text,$cfg['quality'],$respath,"FILE");
			
			// Send the email
			$this->sendEmail();
			
			$smarty->assign("page_title",$GLOBALS['_STR_']['POSTCARD_25']);
			$smarty->assign("font_name",$this->font_name);
			$smarty->assign("id",$this->id);
			$smarty->assign("name",$this->name);
			$smarty->assign("email",$this->email);
			$smarty->assign("recipient",$this->recipient);
			$smarty->assign("return",$objEnvData->fetchGlobal('return'));
			$smarty->assign("card_size",$this->getCardSize());
			
			$smarty->assign("image_url",$cfg['sys']['base_url'].$cfg['fil']['index_postcard']."?cmd=postcardImage&amp;font_name=$this->font_name&amp;id=$this->id&amp;postcard_text=$this->postcard_text");
		
			// Print out a confirmation page
			$smarty->pixDisplay('index.postcard/index.postcard.03.tpl');
		
		} else {
			print "no sender";
		}
	
	}
	
	/*
	*	
	*	
	*
	*/
	function getCardSize () {
		
		global $cfg;
		
		require_once ("class.PixariaImage.php");
		
		$objPixariaImage = new PixariaImage($this->id);
		
		$theimage = $cfg['sys']['base_library'].$objPixariaImage->getImagePath()."/".COMPING_DIR."/".$objPixariaImage->getImageFileName();
		
		// Create an image resource with the source photo
		$src_img = @getimagesize($theimage);
	
		// The aspect ratio of the original image
		$aspect_ratio = ($src_img[0] / $src_img[1]);
		
		// Insert code to find image aspect ratio
		if ($aspect_ratio > 0.85) {
		
			$template		= SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/images/postcards/template.01.jpg";
			
		} else {
		
			$template		= SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/images/postcards/template.02.jpg";
		
		}
		
		// Get the size of the template - useful for the HTML layouts
		$size = @getimagesize($template);
	
		return $size[3];
		
	}
	
	/*
	*	
	*	
	*
	*/
	function postcardImage () {
	
		global $smarty, $cfg, $ses, $objEnvData;
		
		$quality		= $cfg['set']['gd_quality'];
		
		// Send the browser a JPEG image header
		header( "Content-Type: image/jpeg");
		
		// Send the image data to the browser
		$this->outputPostcard($this->font_name,$this->imagefile,$this->postcard_text,$quality,$respath,"IMAGE");
	
	}

	/*
	*	
	*	
	*
	*/
	function sendEmail() {
	
		global $cfg, $ses, $smarty;
		
		require_once($cfg['sys']['base_path'] . "resources/ext/phpmailer/class.phpmailer.php");
		
		// Get the date and time
		$filename = time();
		
		//$smarty->assign("name",$this->name);
		//$smarty->assign("email",$this->email);
		//$smarty->assign("filename",$this->filename);
		$image = $this->imagedata;
		
		$mail    = new PHPMailer();
		
		$mail->From     = "$this->email";
		$mail->FromName = "$this->name";
		
		$mail->Subject = "$this->name " . $GLOBALS['_STR_']['POSTCARD_23'];
		
		$mail->AltBody = $GLOBALS['_STR_']['POSTCARD_24']; // optional, comment out and test
		
		$mail->AddEmbeddedImage($image, "image_file", "image.jpg");
		
		$body    = eregi_replace("[\]",'',$smarty->pixFetch("email.templates/postcard.content.tpl"));
		
		$mail->IsHTML(true);
		
		$mail->Body = $body;
		
		$mail->AddAddress("$this->recipient");
		
		$mail->Send();
	
	}
	
	/*
	*	
	*	
	*
	*/
	function outputPostcard($font_name,$imagefile,$postcard_text,$quality,$respath,$output_method) {
	
		// Get the values for GD2 and the URL into this function
		global $cfg, $ses;
		
		$GD2		= $cfg['set']['gd_popcard'];
		$quality	= $cfg['set']['gd_quality'];
		
		require_once ("class.PixariaImage.php");
		
		$objPixariaImage = new PixariaImage($this->id);
		
		$theimage = $cfg['sys']['base_library'].$objPixariaImage->getImagePath()."/".COMPING_DIR."/".$objPixariaImage->getImageFileName();
		
		
		// Check if watermarking is needed
		if ($cfg['set']['postcard_watermark'] == "10") { // Don't include any watermarks
		
			$watermarking = (bool)FALSE;
		
		} else { // Add watermarks
		
			$watermarking = (bool)TRUE;
		
		}
		
		if ($watermarking) {
				
			// Set the path to the watermarked image
			$theimage = $this->watermarkImage($theimage);
		
		}
		
		// Create an image resource with the source photo
		$src_img = imagecreatefromjpeg($theimage);
	
		// The width of original image
		$src_width = imagesx($src_img); 
		
		// The height of original image
		$src_height = imagesy($src_img); 
		
		// The aspect ratio of the original image
		$aspect_ratio = ($src_width / $src_height);
		
		// Insert code to find image aspect ratio
		if ($aspect_ratio > 0.85) {
		
			$port			= "FALSE";
			$template		= SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/images/postcards/template.01.jpg";
			$blank			= SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/images/postcards/blank.01.jpg";
			$imghgt			= 310;
			$imgwdt			= 618;
			$text_height	= 250;
			
		} else {
		
			$port			= "TRUE";
			$template		= SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/images/postcards/template.02.jpg";
			$blank			= SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/images/postcards/blank.01.jpg";
			$imghgt			= 398;
			$imgwdt			= 578;
			$text_height 	= 320;
		
		}
				
		// reduction ratio
		$rr = $imghgt / $src_height; 
		
		// New image width
		$new_width = $rr * $src_width; 
		
		// Check the version of GD set in the preferences
		if ($GD2 == "TRUE") {
		
			// Create a blank image space for the resized image 
			$dest_img = imagecreatetruecolor($new_width,$imghgt); 
			
			// Open the template file and create an image resource with it
			$template = imagecreatefromjpeg($template);
			
			// Resize the source picture to fit into the bounds of the template
			imagecopyresampled($dest_img, $src_img, 0, 0, 0 ,0, $new_width, $imghgt, $src_width, $src_height);
		
		} else {
		
			// Create a blank image space for the resized image 
			$dest_img = imagecreatefromjpeg($blank); 
			
			// Open the template file and create an image resource with it
			$template = imagecreatefromjpeg($template);
			
			// Resize the source picture to fit into the bounds of the template
			imagecopyresized($dest_img, $src_img, 0, 0, 0 ,0, $new_width, $imghgt, $src_width, $src_height);
		
		}
		
		// Copy the source image into the template image leaving a 1 pixel margin at the sides for the black border
		imagecopy ($template, $dest_img, 1, 1, 0, 0, $new_width, $imghgt);
		
		// Only continue of there is message text to work with.
		if ($postcard_text != "") {
			
			// Set the total width of the text box
			$text_width = round($imgwdt - $new_width - 20);
			
			// Distance in pixels to place the text box from the left edge of the template
			$im_position = $new_width + 10;
			
			// Distance in pixels to place the text box from the top edge of the template
			$top_offset = "60";
		
			$im = $this->renderPostcard($postcard_text, $text_width, $text_height, $font_name);
			
			// Copy the text image into our template positioning it correctly in the box.
			imagecopy ($template, $im, $im_position, $top_offset, 0, 0, $text_width, $text_height); 
		
		}
		
		// Select the correct mode in which to send the image data
		
		if ($output_method == "BASE64") {
		
			$this_image = time();
			
			$this_image_loc = $cfg['set']['temporary'] . $this_image;
			
			imagejpeg($template, $this_image_loc, $cfg['set']['gd_quality']);
			$handle = fopen($this_image_loc,'rb'); 
			$file_content = fread($handle,filesize($this_image_loc)); 
			fclose($handle); 
			$encoded = chunk_split(base64_encode($file_content)); 
							
			unlink($this_image_loc);
			
			return ($encoded);
		
		} elseif ($output_method == "IMAGE") {
		
			return(imagejpeg($template, '', $quality));
			
		} elseif ($output_method == "FILE") {
		
			$filename = $cfg['set']['temporary'] . md5(microtime()) . ".jpg";
			
			imagejpeg($template, "$filename", $quality);
				
			return $filename;
			
		}
	
		// Destroy used image resources
		imagedestroy($im); 
		imagedestroy($src_img); 
		imagedestroy($template); 
		imagedestroy($dest_img);				
		
		if ($watermarking == "TRUE" && eregi($cfg['set']['temporary'],$image_path)) {
		
			// Delete the temporary file
			unlink($image_path);
			
		}
	
	}

	/*
	*	
	*	
	*
	*/
	function renderPostcard($text, $text_width, $text_height, $font_name) {
	
		// Path to the fonts directory
		$fontfile = SYS_BASE_PATH."resources/fonts/$font_name";
		
		// Set font sizes for each specific font family
		if 		($font_name == "briquetn.ttf") { $size = "13"; }
		elseif 	($font_name == "arialbd.ttf") { $size = "11"; }
		elseif 	($font_name == "comicbd.ttf") { $size = "10"; }
	
		// Break the message text into words
		$ex = explode(" ", stripslashes(stripslashes(urldecode($this->convertUTF8_NCE($text)))));
		
		$p = 0; 
		$i = 0;
		
		// Split the text into lines of words that will fit into the space on the right of the card
		for ($i=0; $i<count($ex); $i++) {
		
			 $test = $full[$p] . " "; 
			 $test .= $ex[$i];
			 $bbox = imagettfbbox($size, 0, $fontfile, $test); 
		
			if(($bbox[4] - $bbox[6]) > $text_width) { 
			  $p++; 
			  $full[$p] = $ex[$i]; 
			 } else {
			  $full[$p] = $test; 
			}
		
		}
		
		for ($k=0; $k < count($full); $k++) {
			$line = each($full);
			$output .= $line[value] . "\r\n";
		}
		
		// Knock out the leading whitespace
		$output = substr($output, 1);  
		
		
		//exit;
		// Create an image
		$im = imagecreate($text_width,$text_height);
		
		// Assign the image white colour space (background colour)
		$white = imagecolorallocate($im, 255,255,255);
		
		// Assign the image black colour space (text colour)
		$black = imagecolorallocate($im, 0,0,0);
		
		// Write the text into our blank image as anti-aliased text.
		imagettftext($im, $size, 0, 0, 20, $black, $fontfile, $output);
		
		return $im;
		
		imagedestroy($im);
		
	}

	/*
	*	
	*	Converts character-values > 127 (both: UTF-8 + ANSI) to HTML's numeric coded entities
	*
	*/
	function convertUTF8_NCE($utf) { 
		
		if ($utf == '') { return $utf8; }
		
		$max_count = 5; // flag-bits in $max_mark ( 1111 1000 == 5 times 1) 
		$max_mark = 248; // marker for a (theoretical ;-)) 5-byte-char and mask for a 4-byte-char; 
		
		$html = ''; 
		for($str_pos = 0; $str_pos < strlen($utf); $str_pos++) {
		
			$old_chr = $utf{$str_pos}; 
			$old_val = ord( $utf{$str_pos} ); 
			$new_val = 0; 
			
			$utf8_marker = 0; 
			
			// skip non-utf-8-chars 
			if( $old_val > 127 ) { 
			  $mark = $max_mark; 
			  for($byte_ctr = $max_count; $byte_ctr > 2; $byte_ctr--) { 
				// actual byte is utf-8-marker? 
				if( ( $old_val & $mark  ) == ( ($mark << 1) & 255 ) ) { 
				  $utf8_marker = $byte_ctr - 1; 
				  break; 
				} 
				$mark = ($mark << 1) & 255; 
			  } 
			} 
			
			// marker found: collect following bytes 
			if($utf8_marker > 1 and isset( $utf{$str_pos + 1} ) ) { 
			  $str_off = 0; 
			  $new_val = $old_val & (127 >> $utf8_marker); 
			  for($byte_ctr = $utf8_marker; $byte_ctr > 1; $byte_ctr--) { 
			
				// check if following chars are UTF8 additional data blocks 
				// UTF8 and ord() > 127 
				if( (ord($utf{$str_pos + 1}) & 192) == 128 ) { 
				  $new_val = $new_val << 6; 
				  $str_off++; 
				  // no need for Addition, bitwise OR is sufficient 
				  // 63: more UTF8-bytes; 0011 1111 
				  $new_val = $new_val | ( ord( $utf{$str_pos + $str_off} ) & 63 ); 
				} 
				// no UTF8, but ord() > 127 
				// nevertheless convert first char to NCE 
				else { 
				  $new_val = $old_val; 
				} 
			  } 
			  // build NCE-Code 
			  $html .= '&#'.$new_val.';'; 
			  // Skip additional UTF-8-Bytes 
			  $str_pos = $str_pos + $str_off; 
			} 
		
			else { 
			
			  $html .= chr($old_val);
			  $new_val = $old_val;
			  
			} 
		
		} 
		
		return($html);
		
	}

	/*
	*	
	*	Create a watermarked image and return the system path to the file
	*	
	*/
	function watermarkImage($image_path) {
		
		global $cfg;
		
		// Create an image in GD
		$image_rsrc			= imagecreatefromjpeg($image_path);
		$microtime			= microtime();
		
		## Code to superimpose the PNG graphic watermark	
		$im = SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/images/watermark.png";
		$image_id = imagecreatefrompng($im);
		$im_X = ImageSX($image_id);
		$im_Y = ImageSY($image_id);
		$bg_X = ImageSX($image_rsrc);
		$bg_Y = ImageSY($image_rsrc);
		
		switch ($cfg['set']['image_watermark_pos']) {
		
			case 10: // Center
			
				$offset_X	= (($bg_X/2)-($im_X/2));
				$offset_Y	= (($bg_Y/2)-($im_Y/2));
				imagealphablending($image_rsrc, true);
				imagecopy($image_rsrc, $image_id, $offset_X, $offset_Y, 0, 0, $im_X, $im_Y);
				
			break;
			
			case 11: // Top left
			
				$offset_X	= 10;
				$offset_Y	= 10;
				imagealphablending($image_rsrc, true);
				imagecopy($image_rsrc, $image_id, $offset_X, $offset_Y, 0, 0, $im_X, $im_Y);
				
			break;
			
			case 12: // Top right
			
				$offset_X	= $bg_X - (10 + $im_X);
				$offset_Y	= 10;
				imagealphablending($image_rsrc, true);
				imagecopy($image_rsrc, $image_id, $offset_X, $offset_Y, 0, 0, $im_X, $im_Y);
				
			break;	
			
			case 13: // Bottom left
			
				$offset_X	= 10;
				$offset_Y	= $bg_Y - (10 + $im_Y);
				imagealphablending($image_rsrc, true);
				imagecopy($image_rsrc, $image_id, $offset_X, $offset_Y, 0, 0, $im_X, $im_Y);
	
			break;
			
			case 14: // Bottom right
			
				$offset_X	= $bg_X - (10 + $im_X);
				$offset_Y	= $bg_Y - (10 + $im_Y);
				imagealphablending($image_rsrc, true);
				imagecopy($image_rsrc, $image_id, $offset_X, $offset_Y, 0, 0, $im_X, $im_Y);
				
			break;
		
		}
		
		imagedestroy($image_id);
	
		// Write an image
		imagejpeg($image_rsrc,$cfg['set']['temporary'].$microtime,$cfg['set']['gd_quality']);
		imagedestroy($image_rsrc);
		
		// Set the path to the watermarked image
		$image_path	= $cfg['set']['temporary'].$microtime;
		
		return $image_path;
	
	}

}

?>