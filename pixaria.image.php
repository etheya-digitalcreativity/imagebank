<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","resources/includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Load the IPTC library
require_once(SYS_BASE_PATH.'resources/classes/class.Core.IPTC.php');

/*
*
*	If the selected image output format is Flash, we need to initialise the SWF object
*
*/
if ($cfg['set']['image_output_format'] == "11") {
	
	// Load FlashWriter and FlashUtils SWF Libraries
	require("class.FlashUtils.php");
	
	// Instantiate object
	$flashimg = new FlashImage;
	
}

// Instantiate the image class
$objIndexImage = new IndexImage();

/*
*
*	Class for output and display of comping images
*
*/
class IndexImage {
	
	var $image_path;
	var $original_path;
	var $exif_original;
	var $iptc_original;
	var $exif_comping;
	var $iptc_comping;
	var $image_dimensions;
	var $file_extension;
	var $watermarking;
	
	/*
	*
	*	
	*
	*/
	function IndexImage () {
	
		global $objEnvData, $ses, $cfg, $lan;
		
		// Load the image path
		$this->getImagePath();
		
		// Check that the path only refers to the comping image
		if (eregi("/".COMPING_DIR."/",$this->image_path)) {
			
			// Send a 404 error if the page is missing
			if (!file_exists($this->image_path)) {
				header("HTTP/1.0 404 Not Found");
				print("<h1>404 - Not found!</h1><br />The specified resource could not be found");
				exit;
			}
			
			// Get the path to the original image
			$this->original_path 	= eregi_replace("/".COMPING_DIR."/","/original/",$this->image_path);
			$this->comping_path 	= $this->image_path;
			
			// Get the file extension of this file so we can generate a MIME type
			$this->file_extension = strtolower(substr(strrchr($this->image_path,"."),1));
				
			// Get the dimensions of this image
			$this->image_dimensions = getimagesize($this->image_path);
		
			// Get IPTC metadata from original file (if present)
			$iptc = new IPTCCore ($this->original_path);
			$this->iptc_original = $iptc->getAll();
			
			// Get IPTC metadata from comping file (if present)
			$iptc = new IPTCCore ($this->comping_path);
			$this->iptc_comping = $iptc->getAll();
			
			// Apply a watermark to this image
			$this->applyWatermark();
			
		}
		
		if ($objEnvData->fetchGlobal('html') != "") {
		
			$this->outputHTML();
		
		}
		
		if ($objEnvData->fetchGlobal('dl') == 1) {
		
			$this->outputComp();
		
		}
	
		// Set which kind of image to output (standard / SWF / SVG)
		switch ($cfg['set']['image_output_format']) {
			
			case 10: // Output a standard image
				$this->outputJPG();
			break;
			
			case 11: // Output an Adobe Flash image
				$this->outputSWF();
			break;
		
			case 12: // Output an SVG image
				$this->outputSVG();
			break;
			
		}
	
	}
	
	/*
	*
	*	Output a comping image for download (the browser will force download it)
	*
	*/
	function outputComp () {
		
		global $cfg;
		
		$pathinfo = pathinfo($this->image_path);
		
		// Send forced download headers
		header("Content-Description: File Transfer");
		header("Content-Type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"" . $pathinfo['basename']."\"");

		$this->writeIPTC($this->image_path);
		
		header("Content-length:".(string)(filesize($this->image_path)));
		
		// Output the image into the HTTP stream
		$fd = fopen($this->image_path, 'rb');
		
		while(!feof($fd)) {
		
			$buffer = fread($fd, 32 * 1024);
			print $buffer;
			
		}
		
		fclose ($fd);
		
		if (is_array($this->iptc)) {
			@unlink($temporary_image);
		}
		
		// Delete watermarked file if present
		$this->deleteWatermarkedFile();
		
		// Exit the script
		exit;
		
	}
	
	/*
	*
	*	Output a raw JPEG image
	*
	*/
	function outputJPG () {
	
		// Send HTTP header and cache for 7 days
		pix_http_headers("jpg","7");
		
		$this->writeIPTC($this->image_path);
		
		// Output the image into the HTTP stream
		//echo fread(fopen($this->image_path, 'r'), filesize($this->image_path));
		readfile($this->image_path);
		
		// Delete watermarked file if present
		$this->deleteWatermarkedFile();
		
		// Exit the script
		exit;
		
	}
	
	/*
	*
	*	Output an SWF encapsulated image
	*
	*/
	function outputSWF () {
		
		global $flashimg;
		
		if ( !$flashimg->setImage( $this->image_path ) ) {
			$flashimg->setImage( $this->image_path );
		}
		if ( !$flashimg->checkImage() ) {
			$flashimg->setImage( $this->image_path );
		}
			
		$flashimg->outputSWF();
			
		// Delete watermarked file if present
		$this->deleteWatermarkedFile();
		
		// Exit the script
		exit;
		
	}
	
	/*
	*
	*	Output an SVG encapsulated image
	*
	*/
	function outputSVG () {
	
		$this->writeIPTC($this->image_path);
		
		$handle = fopen($this->image_path, "rb"); 
		$file_content = fread($handle,filesize($this->image_path)); 
		fclose($handle); 
		$encoded = chunk_split(base64_encode($file_content)); 
		
		// Send HTTP header and cache for 7 days
		//pix_http_headers("svg","7");
		
		$svgDATA = "<";
		$svgDATA .= "?xml version=\"1.0\" encoding=\"ISO-8859-1\" standalone=\"yes\"?";
		$svgDATA .= ">\n";
		$svgDATA .= "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 20010904//EN\" \"http://www.w3.org/TR/2001/REC-SVG-20010904/DTD/svg10.dtd\">\n";
		$svgDATA .= "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n";
		$svgDATA .= "<script language=\"Javascript\">\n";
		$svgDATA .= "function handleIt(evt)\n";
		$svgDATA .= "{\n";
		$svgDATA .= "evt.preventDefault();\n";
		$svgDATA .= "}\n";
		$svgDATA .= "</script>\n";
		$svgDATA .= "<g id=\"square\" onmousedown=\"handleIt(evt)\">\n";
		$svgDATA .= "<image id=\"image001.svg\" " . $this->image_dimensions[3] . " xlink:href=\"data:;base64," . $encoded . "\"/>\n";
		$svgDATA .= "</g>\n";
		$svgDATA .= "</svg>\n";
	
		// Output compressed SVG data as a binary gzip stream
		print(gzencode($svgDATA));
		
		// Delete watermarked file if present
		$this->deleteWatermarkedFile();
		
		// Exit the script
		exit;
		
	}
	
	/*
	*
	*	Output HTML page for the image
	*
	*/
	function outputHTML () {
		
		global $objEnvData, $ses, $cfg;
		
		// Initialise the smarty object
		$smarty = new Smarty_Pixaria;
	
		// Set the image filepath as base 64 string
		$smarty->assign("file",$objEnvData->fetchGlobal('html'));
	
		// Get image information
		$imageinfo = getimagesize(base64_decode($objEnvData->fetchGlobal('html')));
		
		// Get image width
		$smarty->assign("width",$imageinfo[0]);
	
		// Get image height
		$smarty->assign("height",$imageinfo[1]);
	
		// Display the html header
		$smarty->pixDisplay('index.image/image.output.tpl');
		
		exit;
		
	}
	
	/*
	*
	*	Get the path of the image from it's ID number or encoded file path
	*
	*/
	function getImagePath() {
		
		global $objEnvData, $ses, $cfg;
		
		$file		= realpath(base64_decode($objEnvData->fetchGlobal("file")));
		$image_id	= $objEnvData->fetchGlobal("image_id");
		$image_hash	= $objEnvData->fetchGlobal("image_hash");
		
		// Get the file path of the image we want to output
		if (is_numeric($image_id)) {
			
			require_once(SYS_BASE_PATH . 'resources/includes/class.PixariaImage.php');
			
			if ($image_hash == strtoupper(substr(md5($cfg['sys']['encryption_key'] . $image_id),0,6))) {
			
				$im = new PixariaImage($image_id);
			
				$this->image_path = $im->getImageCompPath();
			
			} else {
				
				header ("Location: ".SYS_BASE_URL);
				
			}
			
		} elseif ($file != "") {
		
			if (!preg_match("|^".preg_quote(realpath(SYS_BASE_LIBRARY))."|",$file) || !getimagesize($file)) {
				
				exit;
				
			}
			
			$this->image_path = $file;
			
		}
	
	}
	
	/*
	*
	*	Create a watermarked image and return the system path to the file
	*
	*/
	function applyWatermark() {
		
		global $cfg, $ses;
		
		$pathinfo = pathinfo($this->image_path);
		
		// Check if watermarking is needed
		if ($cfg['set']['image_watermark'] == "10") { // Don't include any watermarks
		
			$this->watermarking = (bool)FALSE;
			
			// Leave this method without applying the watermark
			return;
		
		} elseif ($cfg['set']['image_watermark'] == "11")  { // Only add watermarks for users who aren't logged in
		
			if ($ses['psg_userid']) {
				
				$this->watermarking = (bool)FALSE;
				
				// Leave this method without applying the watermark
				return;
		
			} else {
			
				$this->watermarking = (bool)TRUE;
				
			}
			
		} else { // Add watermarks for all users
		
			$this->watermarking = (bool)TRUE;
		
		}
		
		// Create an image in GD
		$image_rsrc			= imagecreatefromjpeg($this->image_path);
		$microtime			= microtime();
		
		## Code to superimpose the PNG graphic watermark	
		$im = SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/images/watermark.png";
		$image_id = imagecreatefrompng($im);
		$im_X = ImageSX($image_id);
		$im_Y = ImageSY($image_id);
		$bg_X = ImageSX($image_rsrc);
		$bg_Y = ImageSY($image_rsrc);
		
		switch ($cfg['set']['image_watermark_pos']) {
		
			case 10: // Center
			
				$offset_X	= (($bg_X/2)-($im_X/2));
				$offset_Y	= (($bg_Y/2)-($im_Y/2));
				imagealphablending($image_rsrc, true);
				imagecopy($image_rsrc, $image_id, $offset_X, $offset_Y, 0, 0, $im_X, $im_Y);
				
			break;
			
			case 11: // Top left
			
				$offset_X	= 10;
				$offset_Y	= 10;
				imagealphablending($image_rsrc, true);
				imagecopy($image_rsrc, $image_id, $offset_X, $offset_Y, 0, 0, $im_X, $im_Y);
				
			break;
			
			case 12: // Top right
			
				$offset_X	= $bg_X - (10 + $im_X);
				$offset_Y	= 10;
				imagealphablending($image_rsrc, true);
				imagecopy($image_rsrc, $image_id, $offset_X, $offset_Y, 0, 0, $im_X, $im_Y);
				
			break;	
			
			case 13: // Bottom left
			
				$offset_X	= 10;
				$offset_Y	= $bg_Y - (10 + $im_Y);
				imagealphablending($image_rsrc, true);
				imagecopy($image_rsrc, $image_id, $offset_X, $offset_Y, 0, 0, $im_X, $im_Y);
	
			break;
			
			case 14: // Bottom right
			
				$offset_X	= $bg_X - (10 + $im_X);
				$offset_Y	= $bg_Y - (10 + $im_Y);
				imagealphablending($image_rsrc, true);
				imagecopy($image_rsrc, $image_id, $offset_X, $offset_Y, 0, 0, $im_X, $im_Y);
				
			break;
		
		}
		
		imagedestroy($image_id);
	
		// Write an image
		imagejpeg($image_rsrc,$cfg['set']['temporary'].$pathinfo['basename'],$cfg['set']['gd_quality']);
		imagedestroy($image_rsrc);
		
		// Set the path to the watermarked image
		$this->image_path	= $cfg['set']['temporary'].$pathinfo['basename'];
	
		$this->writeIPTC($this->image_path);
		
	}
	
	/*
	*
	*	Delete the watermarked image file from temp directory
	*
	*/
	function deleteWatermarkedFile() {
		
		global $cfg;
		
		if ($this->watermarking == "TRUE" && eregi($cfg['set']['temporary'],$this->image_path)) {
		
			// Delete the temporary file
			unlink($this->image_path);
			
		}
	
	}
	
	/*
	*
	*	WRITE IPTC METADATA INTO A FILE
	*
	* 	If IPTC header information is present in the original file
	*	but not the comping file or if the watermarking system is
	*	turned on then load IPTC data back into the comping image.
	*
	*/
	function writeIPTC ($image_path) {
		
		if (is_writable($image_path) && file_exists($image_path)) {
			
			if ( is_array($this->iptc_original) && (!is_array($this->iptc_comping) || $this->watermarking) ) {
				
				$im = new IPTCCore($image_path);
				
				$im->setAll($this->iptc_original);
				
				$im->write();
				
			} elseif (!is_array($this->iptc_original) && (is_array($this->iptc_comping) || $this->watermarking) ) {
				
				$im = new IPTCCore($image_path);
				
				$im->setAll($this->iptc_comping);
				
				$im->write();
				
			}
			
			return true;
			
		} else {
			
			return false;
		
		}
	
	}
	
}


?>