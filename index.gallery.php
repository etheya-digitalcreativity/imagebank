<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set up constant for front end activities
define("WORKSPACE","FE");

// Set the include path for files used in this script
ini_set("include_path","resources/includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Send HTTP header and don't cache
pix_http_headers("html","");

// 	Go to login page if the default action for logged out users 
//	is to require logging in or registering before viewing content
pix_login_redirect();

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Initialise this page
$objIndexGallery = new IndexGallery();

/*
*	Class constructor for the group
*/
class IndexGallery {
	
	var $gallery_id;
	var $gallery_page;
	var $thumbnail_page;
	var $gallery_image;
	var $gallery_title;
	var $gallery_description;
	var $gallery_key_image;
	var $gallery_child_data;
	var $gallery_image_list;
	var $gallery_navigation;
	var $ipages;
	var $gpages;
	var $gallery_url;
	var $gallery_password;
	var $gallery_items;
	var $gallery_thumb_pages;
	
	var $_dbl;
	
	/*
	*	Class constructor for viewing a gallery
	*/
	function IndexGallery () {
		
		// Load globals
		global $objEnvData, $cfg, $ses, $smarty;
		
		// Load the database class
		require_once ('class.Database.php');
		require_once (SYS_BASE_PATH . 'resources/classes/class.InputData.php');
		
		// Create the database object
		$this->_dbl 	= new Database();
		$this->input 	= new InputData();
		$this->view 	=& $smarty;
		
		// Load the gallery class
		require_once ('class.PixariaGallery.php');
		
		// Load the global gallery and image group properties for page navigation
		$this->gallery_page		= $this->input->name('gpg'); // The gallery thumbnail icon page we are currently on
		$this->thumbnail_page	= $this->input->name('ipg'); // The image thumbnail icon page we are currently on
		$this->gallery_image	= $this->input->name('img'); // The number of a specific image we want to view in the gallery
		$this->gallery_id 		= $this->input->name('gid'); // The gallery ID number
		
		// If we're looking at RSS, then show all images in the gallery
		if ($this->input->name('cmd') == 'rss' || $this->input->name('cmd') == 'mrss') {
		
			$multi_image_show_all = true;
			
		}
		
		// Initialise the gallery object
		$objGallery = new PixariaGallery(	$this->gallery_id,
											$this->gallery_page,
											$this->thumbnail_page,
											$this->gallery_image,
											$multi_image_show_all);
		
		// Get information about this gallery
		$this->gallery_id			= $objGallery->getGalleryId();
		$this->gallery_rss_url		= $objGallery->getGalleryRssUrl();
		$this->gallery_title		= $objGallery->getGalleryTitle();
		$this->gallery_description	= $objGallery->getGalleryDescription();
		$this->gallery_key_image	= $objGallery->getGalleryKeyImageData();
		$this->gallery_permissions	= $objGallery->getGalleryPermissions();
		$this->gallery_child_data	= $objGallery->getGalleryChildData();
		$this->gallery_image_list	= $objGallery->getGalleryMultiImageData();
		$this->gallery_navigation	= $objGallery->getMultiImageNavigation();
		$this->ipages				= $objGallery->getMultiImagePageNavigation();
		$this->gpages				= $objGallery->getGalleryPageNavigation();
		$this->gallery_url			= $objGallery->getGalleryURL();
		$this->gallery_password		= $objGallery->getGalleryPassword();
		$this->gallery_image_data	= $objGallery->getGalleryImageData();
		
		// Run whichever method has been requested for this page
		switch ($this->input->name('cmd')) {
			
			case "verifyPassword": // We're verifying submitted password
				
				// Verify a submitted password
				$this->verifyPassword();
				
			break;			
			
			case 'rss':
			case 'mrss':
				
				// Check if a password is required
				$this->doPasswordCheck();
				
				// Check if the user has permission to see the gallery
				$this->doPermissionCheck();
				
				switch ($this->input->name('cmd')) {
					
					case 'rss':
						
						// View the gallery RSS feed
						$this->viewGalleryRss();
					
					break;
					
					case 'mrss':
					
						// View the gallery RSS feed
						$this->viewGalleryMediaRss();
					
					break;
					
				}

			break;
			
			default: // Show the cart contents for the current registered user
			
				// Check if a password is required
				$this->doPasswordCheck();
				
				// Check if the user has permission to see the gallery
				$this->doPermissionCheck();
				
				if (!is_numeric($this->gallery_image)) { // We're viewing thumbnails
					
					// Update the view counter for this gallery
					if (is_numeric($this->gallery_id) && $this->gallery_page < 1 || $this->gallery_page == "") {
						$objGallery->updateGalleryViewCounter ();
					}
					
					// View the gallery
					$this->viewGallery();
					
				} else { // We're viewing an image inside a gallery
				
					// View an image in a gallery
					$this->viewGalleryImage();

				}
				
			break;
		
		}
	
	}
	
	/*
	*	Show a page asking a user to supply a password for a gallery
	*/
	function requestPassword ($warning = "") {
		
		// Load the gallery ID into Smarty
		$this->view->assign("gallery_id",$this->gallery_id);

		// Load warning flag into Smarty
		$this->view->assign("warning",$warning);

		// Assign page title text
		$this->view->assign("page_title","Password Required");

		// Display the form to request a password
		$this->view->display('index.gallery/gallery.password.tpl');
		
		// Stop processing
		exit;
		
	}
	
	/*
	*	Verify that a submitted password matches the one set in the database
	*/
	function verifyPassword () {
	
		$submitted_password = $this->input->name('password');
		
		$gallery_password	= $this->gallery_password;
		
		if ($submitted_password === $gallery_password) {
			
			$this->setPasswordCookie();
			$this->viewGallery();
			
		} else {
			
			// Call the method to request a password with the warning flag set to TRUE
			$this->requestPassword(TRUE);
		
		}
		
		
	}
	
	/*
	*	Set a cookie containing the password for this gallery 
	*/
	function setPasswordCookie () {
		
		// Set the encoded password for storage in a cookie
		$gallery_encoded_password	= md5($this->gallery_id.$this->input->name('password'));
		
		// Set a Cookie for this search (the password is encrypted)
		setcookie("pixaria_gpwd", $gallery_encoded_password, time() + 63072000, "/");
	
	}
	
	/*
	*	Check that if a gallery requires a password then the user has entered it
	*/
	function doPasswordCheck () {
	
		// Set the encoded password which we hope to match with the cookie
		$gallery_encoded_password	= md5($this->gallery_id.$this->gallery_password);
		
		// Check if this gallery requires a password and if it does, whether it's been set correctly
		if (trim($this->gallery_password) != "" && $this->input->cookie('pixaria_gpwd') != $gallery_encoded_password) {
			
			// Call the method to request a password
			$this->requestPassword();
		
		}
		
	}
	
	/*
	*	Check permission
	*/
	function doPermissionCheck () {
	
		global $ses;
		
		if ($this->gallery_permissions == '' || $ses['psg_administrator']) {
			
			return true;
			
		} elseif ($this->gallery_permissions == 10) {
		
			return true;
			
		} elseif ($this->gallery_permissions == 11) {
		
			// If the user isn't logged in, bounce to the login screen
			pix_authorise_user("basic");
		
		} elseif ($this->gallery_permissions == 12) {
			
			list ($gallery_id, $userid) = $this->_dbl->sqlParseRows("SELECT * FROM ".PIX_TABLE_GVIE." WHERE gallery_id = '".$this->gallery_id."'",MYSQL_NUM);
						
			if (is_array($userid) && is_array($ses['psg_groups_member_of'])) {
				
				if (count(array_intersect($userid, $ses['psg_groups_member_of'])) < 1) {
					
					print "Permission error...!";
					exit;
				
				}
				
			} elseif ($ses['psg_groups_member_of'] == '') {
					
				print "Permission error...!";
				exit;
				
			}
		
		}
		
	}
	
	/*
	*	View a gallery
	*/
	function viewGallery () {
		
		// Load variables into Smarty
		$this->view->assign("nav_title",$this->gallery_navigation[0][0]);
		$this->view->assign("nav_href",$this->gallery_navigation[1][0]);
		$this->view->assign("galleries",$this->gallery_child_data);
		$this->view->assign("thumbnails",$this->gallery_image_list);
		$this->view->assign("gallery_avatar",$this->gallery_key_image);
		$this->view->assign("gallery_title",$this->gallery_title);
		$this->view->assign("gallery_media_rss",$this->gallery_rss_url);
		$this->view->assign("page_title",$this->gallery_title);
		$this->view->assign("gallery_description",$this->gallery_description);
		$this->view->assign("gallery_id",$this->gallery_id);
		$this->view->assign("gallery_rss",$this->gallery_rss);
		$this->view->assign("gallery_url",$this->gallery_url);
		$this->view->assign("gallery_thumb_pages",$this->gallery_thumb_pages);
		$this->view->assign("ipage_current",$this->ipages[0]);
		$this->view->assign("ipage_numbers",$this->ipages[1]);
		$this->view->assign("ipage_links",$this->ipages[2]);
		$this->view->assign("base_page",$this->ipages[4]);
		$this->view->assign("gpage_current",$this->gpages[0]);
		$this->view->assign("gpage_numbers",$this->gpages[1]);
		$this->view->assign("gpage_links",$this->gpages[2]);
		$this->view->assign("view_mode","gallery");
		
		// We need the lightbox javascript on this page
		$this->view->assign("js_lightbox",true);
		
		// Display the gallery page
		$this->view->display('index.gallery/gallery.view.tpl');
		
	}

	/*
	*	View an image inside a gallery
	*/
	function viewGalleryImage () {
		
		// Load variables into Smarty
		$this->view->assign("nav_title",$this->gallery_navigation[0]);
		$this->view->assign("nav_href",$this->gallery_navigation[1]);
		$this->view->assign("gallery_title",$this->gallery_title);
		$this->view->assign("gallery_description",$this->gallery_description);
		$this->view->assign("gallery_id",$this->gallery_id);
		$this->view->assign("gallery_media_rss",$this->gallery_rss_url);
		$this->view->assign("gallery_url",$this->gallery_url);
		$this->view->assign("image",$this->gallery_image_data['image_detail']);
		$this->view->assign("thumbnails",$this->gallery_image_data['image_neighbours']);
		$this->view->assign("thumbnails_urls",$this->gallery_image_data['image_urls']);
		$this->view->assign("thumbnails_numbers",$this->gallery_image_data['image_numbers']);
		$this->view->assign("thumbnails_count",$this->gallery_image_data['image_count']);
		$this->view->assign("image_next",$this->gallery_image_data['image_next_url']);
		$this->view->assign("image_prev",$this->gallery_image_data['image_prev_url']);
		$this->view->assign("page_title",$this->gallery_image_data['image_detail']['basic']['title']);
		$this->view->assign("view_mode","gallery");
		
		// Display the gallery page
		$this->view->display('index.gallery/gallery.view.image.tpl');
		
	}
	
	/*
	*
	*	
	*
	*/
	function viewGalleryRss () {
		
		//print_r($this->gallery_image_list);
	
	}
	
	/*
	*
	*	
	*
	*/
	function viewGalleryMediaRss () {
		
		global $smarty;
		
		while ( list ($key, $data) = @each($this->gallery_image_list) ) {
			
			$thumbs_basic[] = $data['basic'];
			$thumbs_files[] = $data['files'];
			$thumbs_url[]	= $data['url_inline'];
		
		}
				
		$this->view->assign('thumbs_basic',$thumbs_basic);
		$this->view->assign('thumbs_files',$thumbs_files);
		$this->view->assign('thumbs_url',$thumbs_url);
		
		$this->view->display('rss/mrss.xml');
		
	}
	
}


?>