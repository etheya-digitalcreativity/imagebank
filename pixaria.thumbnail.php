<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Decode the file path passed into the script and remove null bytes
$image_path = base64_decode(str_replace("\0",'',$_GET['file']));

/*
*
*	Check that we're only getting small images
*
*/
if ( (stristr($image_path, '/32x32/') || stristr($image_path, '/80x80/') || stristr($image_path, '/160x160/') ) && file_exists($image_path)) {
	
	// Get the file extension of this file
	$file_extension = strtolower(substr(strrchr($image_path,"."),1));

/*
*
*	Else return an 'image not found' image - requires database and settings initialisation
*
*/
} else {
	
	// Set the include path for files used in this script
	ini_set("include_path","resources/includes/");
	
	// Load in the Pixaria settings and includes
	include ("pixaria.Initialise.php");
	
	$image_path = SYS_BASE_PATH . "resources/themes/" . $cfg['set']['theme'] . "/images/160x160_missing.jpg";
	
	// Get the file extension of this file
	$file_extension = 'jpg';
		
}

// Set cache expiry to 7 days in the future
header("Expires: " . gmdate("D, d M Y H:i:s", time() + (7 * 86400)) . " GMT");

/*
*
*	Set the correct HTTP content type headers
*
*/
switch ($file_extension) {
	
	case "swf":
		
		// Tell the browser this is an Adobe Flash image
		header('Content-Type: application/x-shockwave-flash');
	
	break;

	case "svg":
	
		// Tell the browser this is a scalable vector image
		header('Content-Type: image/svg+xml');
	
	break;

	case "jpe": case "jpeg": case "jpg":
		
		// Tell the browser this is a JPEG image
		header('Content-Type: image/jpeg');
	
	break;

	case "gif":
		
		// Tell the browser this is a GIF image
		header('Content-Type: image/gif');
	
	break;

	case "png":
		
		// Tell the browser this is a PNG image
		header('Content-Type: image/png');
	
	break;

}

// Output the content length HTTP header
header('Content-Length: ' . filesize($image_path));

// Output the image into the HTTP stream
readfile($image_path);
	
?>