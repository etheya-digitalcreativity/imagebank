<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set up constant for front end activities
define("WORKSPACE","FE");

// Set the include path for files used in this script
ini_set("include_path","resources/includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Send HTTP header and don't cache
pix_http_headers("html","");

// INITIALISE THE SMARTY OBJECT
$smarty = new Smarty_Pixaria;
		
switch ($objEnvData->fetchGlobal('cmd')) {

	case "signout":
	
		// Delete all cookies
		setcookie("psg_userid","",time() - 1200,"/");
		
		// Delete all cookies
		setcookie("psg_persist","",time() - 1200,"/");
		
		// Set redirect url
		$meta_url	= $cfg['sys']['base_url'];
		
		// Print out the waiting screen
		$smarty->pixProcessing($meta_url,"1");
	
	break;
	
	default:
	
		// If there's no manual referer, set the referer to the http referer
		if ($objEnvData->fetchGlobal('referer') != "") {
		
			$referer	= $objEnvData->fetchGlobal('referer');
		
		} else {
			
			$referer	= urlencode($_SERVER['HTTP_REFERER']);
		
		}
		
		// If user is already logged in, then redirect them to the homepage
		if ($ses['psg_userid'] && $ses['psg_full_user']) {
			
			// Set redirect url
			$meta_url	= $cfg['sys']['base_url'];
			
			// Print out the waiting screen
			$smarty->pixProcessing($meta_url,"1");
			
			// Stop running script
			exit;
			
		}
		
		// Process login details if they exist
		
		$email_address = strtolower($objEnvData->fetchGlobal('email_address')); // Make sure e-mail address is lowercase
		
		if ($objEnvData->fetchGlobal('password') && $email_address) {
			
			// Count number of users with this e-mail address (should only be one)
			$number = sql_count("SELECT count(email_address) FROM " . PIX_TABLE_USER . " WHERE email_address = '$email_address'");
			
			if ($number == 1) {
			
				// Check that the user's e-mail address exists on the system
				$result = sql_select_row("SELECT * FROM " . PIX_TABLE_USER . " WHERE email_address = '$email_address'");
				
				$userid			= $result['userid'];
				$user_name		= $result['first_name'] . " " . $result['family_name'];
				$password		= $result['password'];
				$account_status	= $result['account_status'];

				
			}
			
			if ($number < 1) { // Check that the address is listed in the database
			
				$smarty->assign("output_problem",1);
				$smarty->assign("output_problem_status",$GLOBALS['_STR_']['LOGIN_ERROR_01']);
						
				// Add the failed login information to the database
				pix_register_login($userid,'0',$email_address);
				
			} elseif (md5(apost(password)) != $password) { // Passwords don't match
			
				$smarty->assign("output_problem",1);
				$smarty->assign("output_problem_status",$GLOBALS['_STR_']['LOGIN_ERROR_02']);
				
				// Add the failed login information to the database
				pix_register_login($userid,'0',$email_address);
				
			} elseif ($account_status == "0") { // User account isn't active
			
				$smarty->assign("output_problem",1);
				$smarty->assign("output_problem_status",$GLOBALS['_STR_']['LOGIN_ERROR_03']);
				
				// Add the failed login information to the database
				pix_register_login($userid,'0',$email_address);
				
			} elseif (md5(apost(password)) === $password) { // Passwords do match
				
				// Set the session cookie
				pix_set_session($userid);
				
				// Add the login information to the database
				pix_register_login($userid,'1',$email_address);
								
				// If the referring page is off this site, send the user back to the gallery homepage
				if ($cfg['set']['login_default'] || !eregi($cfg['sys']['base_url'],urldecode($referer)) || eregi("signout",urldecode($referer))) {
				
					// Set redirect URL to the homepage
					$meta_url	= $cfg['sys']['base_url'];
					
				} else {
				
					// Set redirect URL to the referring page
					$meta_url	= urldecode($referer);
				
				}
				
				if ($cfg['set']['login_notify'] == 1) { // Send e-mail to the admin if the user has logged in
					
					$smarty->assign("ip_address",$_SERVER['REMOTE_ADDR']);
					$smarty->assign("user_name",$user_name);
					
					// Load the message text from the template
					$message = smartyPixFetch('email.templates/login.notify.tpl');
					
					// Set the message subject
					$subject = $cfg['set']['site_name']." Login: $user_name, IP: " . $_SERVER['REMOTE_ADDR'];
					
					// Send a confirmation e-mail to the user
					sendEmail($cfg['set']['contact_email'], $cfg['set']['contact_name'], $message, $cfg['set']['contact_email'], $cfg['set']['contact_name'], $subject);
				
				}
				
				// Print out the waiting screen
				$smarty->pixProcessing($meta_url,"1");
				
				// Stop running the script
				exit;
				
			}
		
		}
		
		// If we're not processing a login, show the login screen
		
		// Set the page title
		$smarty->assign("page_title",$GLOBALS['_STR_']['LOGIN_001']);
		
		// Add a variable holding referer information
		$smarty->assign("output_referer",$referer);
		
		// Display the html footer
		$smarty->pixDisplay('index.account/account.login.tpl');
		
	break;

}

?>