<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set up constant for front end activities
define("WORKSPACE","FE");

// Set the include path for files used in this script
ini_set("include_path","resources/includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Send HTTP header and don't cache
pix_http_headers("html","0");

// 	Go to login page if the default action for logged out users 
//	is to require logging in or registering before viewing content
pix_login_redirect();

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

$objIndexSearch = new IndexSearch();

class IndexSearch {
	
	// Class variables
	var $search_failed;
	
	// Search terms
	var $keywords_simple_boolean;
	var $gallery_id;
	var $srch_date_st;
	var $srch_date_en;
	var $date_srch;
	var $srch_date_is_dd;
	var $srch_date_is_mm;
	var $srch_date_is_yy;
	var $srch_date_st_dd;
	var $srch_date_st_mm;
	var $srch_date_st_yy;
	var $srch_date_en_dd;
	var $srch_date_en_mm;
	var $srch_date_en_yy;
	var $model_release;
	var $property_release;
	var $rights_managed;
	
	// Search data output
	var $output_total;
	var $output_keywords_simple_boolean;
	var $output_gallery_id;
	var $output_srch_date_st;
	var $output_srch_date_en;
	var $output_date_srch;
	var $output_date_is_time;
	var $output_date_st_time;
	var $output_date_en_time;
	var $output_model_release;
	var $output_property_release;
	var $output_rights_managed;
	var $output_gallery_list;
	var $output_ipages;
	var $output_image_data;
	
	/*
	*
	*
	*
	*/
	function IndexSearch () {
	
		global $cfg, $ses, $objEnvData, $smarty;
		
		$this->thumbnail_page		= $objEnvData->fetchGlobal('ipg'); 				// The image thumbnail icon page we are currently on
		$this->search_image			= $objEnvData->fetchGlobal('img'); 				// The number of a specific image we want to view in the search results list
		$this->original_search_id	= $objEnvData->fetchGlobal('search_within'); 	// The Id of the previous search (if there was one)
		$this->search_id			= $objEnvData->fetchGlobal('sid'); 				// The Id of the current search (if there is one)
		
		/*
		*	Load the search library
		*/
		require_once ('class.PixariaSearch.php');
		
		/*
		*	Run the search query
		*/
		$this->objSearch = new PixariaSearch($this->thumbnail_page, $this->search_image, null, $this->search_id, $this->original_search_id);
		
		/*
		*	Populate the local scopre with information about the search
		*/
		$this->search_image_list		= $this->objSearch->getSearchMultiImageData();		// Array of image data for search results
		$this->ipages					= $this->objSearch->getMultiImagePageNavigation();	// Array of page navigation data
		$this->search_image_data		= $this->objSearch->getSearchImageData();			// Array of information about the search results
		$this->search_results_count		= $this->objSearch->getSearchResultsCount();		// Total number of returned results
		$this->output_total				= $this->search_results_count;						// Total number of returned results
		$this->search_id				= $this->objSearch->getSearchId();					// The current search Id
		$this->keywords_simple_boolean	= $this->objSearch->keywords_simple_boolean;		// The keywords to search for
		
		/*
		*	First check if this is a search within that's returned no results
		*/
		if (is_numeric($this->original_search_id) && $this->search_results_count < 1) {
		
			header ("Location: " . $cfg['sys']['base_url'] . $cfg['fil']['index_search'] . "?sid=" . $this->original_search_id . "&cmd=noresults");
		
		}
		
		
		/*
		*	Next check if this is a new search that has returned result
		*/
		if ($objEnvData->fetchGlobal('cmd') == "doSearch" || $objEnvData->fetchGlobal('sid') != "" || $objEnvData->fetchGlobal('kwd') == 1) {
			
			// Run the search
			$this->doSearch();
			
		} else {
		
			// Make sure we record that this is a working search
			$this->search_failed = false;
			
			// Show the search form to the user
			$this->showSearchForm();
			
		}
	
	}
	
	/*
	*
	*
	*
	*/
	function showSearchForm () {
		
		global $cfg, $ses, $objEnvData, $smarty;
		
		// Erase previous seearch cookies
		setcookie("psg_search","",time() - 1200,"/");
		
		if ($this->output_total == 0): $smarty->assign("results_count","NULL"); endif;

		$this->output_image_colr_r 			= $this->objSearch->image_colr_r;
		$this->output_image_colr_g 			= $this->objSearch->image_colr_g;
		$this->output_image_colr_b 			= $this->objSearch->image_colr_b;
		$this->output_image_colr_mod 		= $this->objSearch->getImageModRGB();
		$this->output_image_colr_dec 		= $this->objSearch->getImageDecRGB();
		$this->output_image_colr_hex 		= $this->objSearch->getImageHexRGB();

		$smarty->assign("image_colr_mod",$this->output_image_colr_mod);
		$smarty->assign("image_colr_hex",$this->output_image_colr_hex);
		$smarty->assign("image_colr_dec",$this->output_image_colr_dec);
		$smarty->assign("image_colr_mod",$this->output_image_colr_mod);
		$smarty->assign("image_colr_r",$this->objSearch->getImageModRGB());
		$smarty->assign("image_colr_g",$this->objSearch->getImageModRGB());
		$smarty->assign("image_colr_b",$this->objSearch->getImageModRGB());
		
		// If we're showing this page after a failed search, tell Smarty
		$smarty->assign("search_failed",$this->search_failed);
		$smarty->assign("search_within",$objEnvData->fetchGlobal('search_within'));
		
		// Set the page title
		$smarty->assign("page_title","Search the library");
		
		// Display the category list
		$smarty->pixDisplay('index.search/search.form.tpl');	
	
	}
		
	/*
	*
	*
	*
	*/
	function doSearch () {
	
		global $cfg, $ses, $objEnvData, $smarty;
		
		if ($this->search_results_count > 0) { // If there are results, show them
			
			// Trim whitespace off the end of the search keywords
			$keywords_simple_boolean = rtrim($this->keywords_simple_boolean);
			
			// Create an array of punctuation to remove from the keyword fields
			$punctuation = array(",","'","\"","\\","/",";","\$","%","&","(",")","{","}","[","]","+","*","<",">");
		
			// Pass original variables back to Smarty for refine search
			$this->output_keywords_simple_boolean = $keywords_simple_boolean;
			
			$this->output_gallery_id 			= $this->objSearch->gallery_id;
			$this->output_srch_date_st 			= $this->objSearch->srch_date_st;
			$this->output_srch_date_en 			= $this->objSearch->srch_date_en;
			$this->output_date_srch 			= $this->objSearch->date_srch;
			
			$this->output_date_is_time			= $this->objSearch->srch_date_is_yy."-".$this->objSearch->srch_date_is_mm."-".$this->objSearch->srch_date_is_dd;
			$this->output_date_st_time 			= $this->objSearch->srch_date_st_yy."-".$this->objSearch->srch_date_st_mm."-".$this->objSearch->srch_date_st_dd;
			$this->output_date_en_time 			= $this->objSearch->srch_date_en_yy."-".$this->objSearch->srch_date_en_mm."-".$this->objSearch->srch_date_en_dd;
			
			$this->output_model_release 		= $this->objSearch->model_release;
			$this->output_property_release 		= $this->objSearch->property_release;
			$this->output_rights_managed 		= $this->objSearch->rights_managed;
			$this->output_results_order 		= $this->objSearch->results_order;
			$this->output_image_orientation 	= $this->objSearch->image_orientation;

			$this->output_image_colr_enable 	= $this->objSearch->image_colr_enable;
			$this->output_image_colr_r 			= $this->objSearch->image_colr_r;
			$this->output_image_colr_g 			= $this->objSearch->image_colr_g;
			$this->output_image_colr_b 			= $this->objSearch->image_colr_b;
			$this->output_image_colr_mod 		= $this->objSearch->getImageModRGB();
			$this->output_image_colr_dec 		= $this->objSearch->getImageDecRGB();
			$this->output_image_colr_hex 		= $this->objSearch->getImageHexRGB();
			
			// Get the search ID and pass it into Smarty
			$smarty->assign("sid",$this->objSearch->getSearchId());
			
			// Load an array of nested galleres
			$this->output_gallery_list 		= galleryListingArray();
			
			// Log the search terms
			$this->objSearch->logSearchTerms();
			
				if (!is_numeric($this->search_image)) { // We're viewing thumbnails
				
					// View the search results list
					$this->viewSearchResults();
										
				} else { // We're viewing an image inside a search
					
					// View image returned in results list
					$this->viewSearchImage();
					
				}
			
		} else { // Else there are no results so we need to show the search form again
			
			// Make sure we record that this is a failed search
			$this->search_failed = true;
			
			// Show the search form
			$this->showSearchForm();
	
		}
	
	}
	
	/*
	*
	*
	*
	*/
	function viewSearchResults () {
		
		global $cfg, $ses, $smarty, $objEnvData;
		
		$smarty->assign("results_total",$this->output_total);
		$smarty->assign("keywords_simple_boolean",$this->output_keywords_simple_boolean);
		$smarty->assign("srch_date_st",$this->output_srch_date_st);
		$smarty->assign("srch_date_en",$this->output_srch_date_en);
		$smarty->assign("date_srch",$this->output_date_srch);
		$smarty->assign("date_is_time",$this->output_date_is_time);
		$smarty->assign("date_st_time",$this->output_date_st_time);
		$smarty->assign("date_en_time",$this->output_date_en_time);
		$smarty->assign("model_release",$this->output_model_release);
		$smarty->assign("property_release",$this->output_property_release);
		$smarty->assign("rights_managed",$this->output_rights_managed);
		$smarty->assign("results_order",$this->output_results_order);
		$smarty->assign("menu_gallery_title",$this->output_gallery_list[0]);
		$smarty->assign("gallery_id",$this->output_gallery_id);
		$smarty->assign("image_orientation",$this->output_image_orientation);
		$smarty->assign("image_colr_mod",$this->output_image_colr_mod);
		$smarty->assign("image_colr_hex",$this->output_image_colr_hex);
		$smarty->assign("image_colr_dec",$this->output_image_colr_dec);
		$smarty->assign("image_colr_mod",$this->output_image_colr_mod);
		$smarty->assign("image_colr_enable",$this->output_image_colr_enable);
		$smarty->assign("image_colr_r",$this->objSearch->getImageModRGB());
		$smarty->assign("image_colr_g",$this->objSearch->getImageModRGB());
		$smarty->assign("image_colr_b",$this->objSearch->getImageModRGB());
		
		// Load variables into Smarty
		$smarty->assign("search_id",$this->search_id);
		$smarty->assign("query_string","?sid=".$this->search_id);
		$smarty->assign("thumbnails",$this->search_image_list);
		$smarty->assign("cmd",$objEnvData->fetchGlobal('cmd'));
		$smarty->assign("ipage_current",$this->ipages[0]);
		$smarty->assign("ipage_numbers",$this->ipages[1]);
		$smarty->assign("ipage_links",$this->ipages[2]);
		$smarty->assign("base_page",$this->ipages[4]);
		$smarty->assign("view_mode","search");
		
		// We need the lightbox javascript on this page
		$smarty->assign("js_lightbox",true);

		// Set the page title
		$smarty->assign("page_title","Search results");
		
		// Display the lightbox page
		$smarty->pixDisplay('index.search/search.view.tpl');
	
		
	}
		
	/*
	*
	*
	*
	*/
	function viewSearchImage () {
		
		global $cfg, $ses, $smarty, $objEnvData;
		
		// Load variables into Smarty
		$smarty->assign("search_id",$this->search_id);
		$smarty->assign("ipage_current",$this->ipages[0]);
		$smarty->assign("ipage_numbers",$this->ipages[1]);
		$smarty->assign("ipage_links",$this->ipages[2]);
		$smarty->assign("view_mode","search");
		$smarty->assign("image",$this->search_image_data['image_detail']);
		$smarty->assign("thumbnails",$this->search_image_data['image_neighbours']);
		$smarty->assign("thumbnails_urls",$this->search_image_data['image_urls']);
		$smarty->assign("thumbnails_numbers",$this->search_image_data['image_numbers']);
		$smarty->assign("thumbnails_count",$this->search_image_data['image_count']);
		$smarty->assign("image_next",$this->search_image_data['image_next_url']);
		$smarty->assign("image_prev",$this->search_image_data['image_prev_url']);
		$smarty->assign("page_title",$this->search_image_data['image_detail']['basic']['title']);
		
		// Display the lightbox page
		$smarty->pixDisplay('index.search/search.view.image.tpl');
		
	}
	
}

?>