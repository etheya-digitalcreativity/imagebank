<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set up constant for front end activities
define("WORKSPACE","FE");

// Set the include path for files used in this script
ini_set("include_path","resources/includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Send HTTP header and don't cache
pix_http_headers("html","");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Initialise this page
$objIndexStore = new IndexStore();

/*
*	Class constructor for the store
*/
class IndexStore {
	
	/*
	*	Class constructor for the store
	*/
	function IndexStore () {
		
		global $objEnvData, $cfg;
		
		switch ($objEnvData->fetchGlobal('cmd')) {
			
			// We're viewing the purchase options for an image
			case "purchaseOptionsInline":
			case "purchaseOptionsPopup":
				$this->purchaseOptions();
			break;
			
			
			// We are calculating the price of an image
			case "calculatePriceInline":
			case "calculatePricePopup":
				$this->calculatePrice();
			break;
			
			
			// We're adding physical products to the cart
			case "addProductsInline":
			case "addProductsPopup":
				$this->addProducts();
			break;
			

			// We're adding an image quote request to the cart
			case "addImageQuoteInline":
			case "addImageQuotePopup":
				$this->addImageQuote();
			break;

			
			// We're adding an image with calculated price to the cart
			case "addImageCalcInline":
			case "addImageCalcPopup":
				$this->addImageCalculated();
			break;

			
			// We're viewing options for recalculating the price of an image already in the cart
			case "recalculateOptionsInline":
			case "recalculateOptionsPopup":
				$this->recalculateOptions();
			break;
			
			
			// We're recalculating the price of an image already in the cart
			case "recalculatePriceInline":
			case "recalculatePricePopup":
				$this->recalculatePrice();
			break;

			
			// We're updating the price of an image already in the cart
			case "updateImagePriceInline":
			case "updateImagePricePopup":
				$this->updateImagePrice();
			break;
			

			// We're removing an item from the cart
			case 'removeCartItem':
				$this->removeCartItem();
			break;
			

			// We're saving a user's changes to a cart
			case 'saveCartBasic':
				$this->saveCartBasic();
			break;
			

			// We're saving a user's changes to a cart
			case 'saveCartIntermediate':
				$this->saveCartIntermediate();
			break;
			

			// We're submitting a completed quotation request on a basic cart
			case 'submitCartBasic':
				$this->submitCartBasic();
			break;
			
			// We're submitting a completed quotation request on a basic cart
			case 'submitCartIntermediate':
				$this->submitCartIntermediate();
			break;
			
			
			// We're submitting an advanced cart quotation request
			case 'submitCartAdvanced':
				$this->submitCartAdvanced();
			break;
			
			
			// We're submitting payment information for a complete cart in offline mode
			case 'submitPaymentMethod':
				$this->submitPaymentMethod();
			break;
			
			
			// We're viewing a transaction
			case 'viewTransaction':
				$this->viewTransaction();
			break;
			
			
			// Add a message to a transaction
			case 'transactionAddMessage':
				$this->transactionAddMessage();
			break;
			
			
			// Download a high resolution image
			case 'downloadImage':
				$this->downloadImage();
			break;
			
			
			// Download a high resolution image for free
			case 'downloadImageFree':
				$this->downloadImageFree();
			break;
			
			
			// Empty the user's active shopping cart
			case 'emptyCart':
				$this->emptyCart();
			break;
			
			
			// Update the number of items in the cart
			case 'updateProductQuantities':
				$this->updateProductQuantities();
			break;
			
			
			// Show the cart contents for the current registered user
			default:
				$this->viewShoppingCart();
			break;
		
		}
	
	}
	
	/*
	*	View the shopping cart.  This is the default method for this page
	*/
	function viewShoppingCart() {
	
		global $cfg, $lang, $ses, $objEnvData, $smarty;
		
		// If the user isn't logged in, bounce to the login screen
		pix_authorise_user("basic");
		
		// Load the shopping cart class
		require_once ('class.PixariaCartObjects.php');
		
		// Instantiate cart object
		$objCart = new PixariaCartObjects();
		
		$smarty->assign("terms",$objEnvData->fetchPost('terms'));
		$smarty->assign("referer",$objEnvData->fetchPost('referer'));
		
		if ($objCart->cart_contents) {
						
			$smarty->assign("cart_id",$objCart->cart_id);
			$smarty->assign("calculator_href",$objCart->cart_item_calculator_href);
			$smarty->assign("cart_item_id",$objCart->cart_item_id);
			$smarty->assign("cart_item_type",$objCart->cart_item_type);
			$smarty->assign("item_usage_text",$objCart->cart_item_usage_text);
			$smarty->assign("item_price",$objCart->cart_item_price);
			$smarty->assign("cart_item_shipping",$objCart->getCartItemShipping());
			$smarty->assign("cart_item_shipping_multiple",$objCart->getCartItemShippingMultiple());
			$smarty->assign("cart_item_quantity",$objCart->getCartItemQuantity());
			$smarty->assign("usage_text",$objCart->cart_usage_text);			
			$smarty->assign("cart_item_usage_text",$objCart->cart_item_usage_text);			
			$smarty->assign("cart_item_price",$objCart->cart_item_price);			
			$smarty->assign("quote_complete",$objCart->cart_quote_complete);
			$smarty->assign("price_complete",$objCart->cart_price_complete);	
			$smarty->assign("image_id",$objCart->cart_item_image_id);
			$smarty->assign("image_title",$objCart->cart_item_image_title);
			$smarty->assign("image_filename",$objCart->cart_item_image_filename);
			$smarty->assign("image_icon_path",$objCart->cart_item_image_icon_path);
			$smarty->assign("image_icon_dims",$objCart->cart_item_image_icon_dims);
			$smarty->assign("image_small_path",$objCart->cart_item_image_small_path);
			$smarty->assign("image_small_dims",$objCart->cart_item_image_small_dims);
			$smarty->assign("image_large_path",$objCart->cart_item_image_large_path);
			$smarty->assign("image_large_dims",$objCart->cart_item_image_large_dims);
			$smarty->assign("image_comp_path",$objCart->cart_item_image_comp_path);
			$smarty->assign("image_comp_size",$objCart->cart_item_image_comp_size);
							
			// Set the HTML page title and masthead text
			$smarty->assign("page_title",$GLOBALS['_STR_']['STORE_192']);
			
			// Load variables into Smarty
			$smarty->assign("shipping",$objCart->cart_shipping);
			$smarty->assign("subtotal",$objCart->cart_subtotal);
			$smarty->assign("tax_total",$objCart->cart_tax_total);
			$smarty->assign("total",$objCart->cart_total);
			
			switch ($cfg['set']['store_type']) {
			
				case 10:
				
					// Display the basic cart page
					$smarty->pixDisplay('index.store/cart.view.basic.tpl');
					
				break;
				
				case 11:
				
					// Display the intermediate cart page
					$smarty->pixDisplay('index.store/cart.view.intermediate.tpl');
					
				break;
				
				case 12: // Advanced mode
										
					// Display the advanced cart page
					$smarty->pixDisplay('index.store/cart.view.advanced.tpl');
					
				break;
			
			}
			
		} else { // There is nothing in the cart or there is no cart to view
			
			// Set the HTML page title and masthead text
			$smarty->assign("page_title",$GLOBALS['_STR_']['STORE_193']);
		
			// Display the empty cart page
			$smarty->pixDisplay('index.store/cart.empty.tpl');

		}
		
		exit;
		
	}
	
	/*
	*	Get a quotation on a product or digital image in the main browser window
	*/
	function purchaseOptions() {
		
		global $cfg;
		global $lang;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		// Get the id of the image
		$image_id = $objEnvData->fetchGlobal('image_id');

		// Intitialise the image object
		require_once ('class.PixariaImage.php');
		$objImage = new PixariaImage($image_id);
		
		// Load information about the image into Smarty
		$smarty->assign("image_data",$objImage->getImageDataComplete());
		
		// Intitialise the products object
		require_once ('class.PixariaProductsPhysical.php');
		$objProductsPhysical = new PixariaProductsPhysical();
		
		// Pass data to Smarty
		$smarty->assign("products_physical",$objProductsPhysical->getProductsPhysical());
		$smarty->assign("prod_id",$objProductsPhysical->getProductId());
		$smarty->assign("prod_name",$objProductsPhysical->getProductName());
		$smarty->assign("prod_description",$objProductsPhysical->getProductDescription());
		$smarty->assign("prod_price",$objProductsPhysical->getProductPrice());
		$smarty->assign("image_id",$image_id);
		
		// Intitialise the products object
		require_once ('class.PixariaProductsDigital.php');
		
		$objProductsDigital = new PixariaProductsDigital();
		
		// Pass data to Smarty
		$smarty->assign("products_digital",$objProductsDigital->getProductsDigital());
		$smarty->assign("calc_description",$objProductsDigital->getCalculationDescription());
		$smarty->assign("calc_active",$objProductsDigital->getCalculationActive());
		$smarty->assign("calc_name",$objProductsDigital->getCalculationName());
		$smarty->assign("calc_id",$objProductsDigital->getCalculationId());
		$smarty->assign("calc_ar_id",$objProductsDigital->getArithmeticId());
		$smarty->assign("calc_ar_name",$objProductsDigital->getArithmeticName());
		$smarty->assign("calc_ar_operand",$objProductsDigital->getArithmeticOperand());
		$smarty->assign("calc_ar_factor",$objProductsDigital->getArithmeticFactor());
		$smarty->assign("calc_ar_function",$objProductsDigital->getArithmeticFunction());
		
		$smarty->assign("referer",$_SERVER['HTTP_REFERER']);
		
		// Put the image id into Smarty
		$smarty->assign("image_id",$image_id);
		
		require_once('class.PixariaProduct.php');
		
		$objProducts = new PixariaProduct();
		
		$product_data = $objProducts->getProducts();
		
		$smarty->assign("product_id",$product_data['prod_id']);
		$smarty->assign("product_name",$product_data['prod_name']);
		$smarty->assign("product_description",$product_data['prod_description']);
		
		// Set the page title
		$smarty->assign("page_title",$GLOBALS['_STR_']['STORE_194']);
		
		if ($objEnvData->fetchGlobal('cmd') == "purchaseOptionsPopup") {
		
			// Output the HTML page
			$smarty->pixDisplay("index.store/purchase.options.popup.tpl");
			
		} elseif ($objEnvData->fetchGlobal('cmd') == "purchaseOptionsInline") {
		
			// Output the HTML page
			$smarty->pixDisplay("index.store/purchase.options.inline.tpl");
			
		}
	
	}
	
	/*
	*	Get a quotation on a product or digital image
	*/
	function calculatePrice() {
	
		global $cfg;
		global $lang;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		// Get post data
		$image_id 			= $objEnvData->fetchGlobal('image_id');
		$cart_item_id		= $objEnvData->fetchGlobal('cart_item_id');
		$cart_id			= $objEnvData->fetchGlobal('cart_id');
		$cart_member_type	= $objEnvData->fetchGlobal('cart_member_type');
		$quote_text			= $objEnvData->fetchGlobal('quote_text');
		
		// Load the cart class
		require_once ('class.PixariaCartObject.php');
		
		// Intitialise the cart object
		$objCartObject = new PixariaCartObject($cart_item_id);
		
		// Load the image class
		require_once ('class.PixariaImage.php');
		
		// Intitialise the cart object
		$objImage = new PixariaImage($image_id);
		
		// Get properties of the image
		$image_path		= $objImage->getImagePath();
		$image_price 	= $objImage->getImagePriceCalculated();
		
		// Load information about the image into Smarty
		$smarty->assign("image_data",$objImage->getImageDataComplete());
		
		// Pass the required data back into Smarty
		$smarty->assign("cart_item_id",$cart_item_id);
		$smarty->assign("cart_id",$cart_id);
		
		// Get the selected options for this image
		$options = $objEnvData->fetchGlobal('options');
		
		// Calculate the price of the image
		$pricing = $objCartObject->calculatePrice($options,$image_price);
		
		// Output the calculated price to Smarty
		$smarty->assign("new_price",$pricing[new_price]);
		$smarty->assign("image_id",$image_id);
		$smarty->assign("ar_id",$options);
		$smarty->assign("ar_description",$pricing[ar_description]);
		
		// Load the page title
		$smarty->assign("page_title",$GLOBALS['_STR_']['STORE_194']);
		
		if ($objEnvData->fetchGlobal('cmd') == "calculatePricePopup") {
		
			// Output the HTML page
			$smarty->pixDisplay('index.store/price.calculate.popup.tpl');
			
		} elseif ($objEnvData->fetchGlobal('cmd') == "calculatePriceInline") {
		
			// Output the HTML page
			$smarty->pixDisplay("index.store/price.calculate.inline.tpl");
			
		}
	
	}
	
	/*
	*	Display options for recalculating the price of a digital image
	*/
	function recalculateOptions() {
		
		global $cfg;
		global $lang;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		// Get the id of the image
		$cart_item_id	= $objEnvData->fetchGlobal('cart_item_id');
		
		// Load the cart class
		require_once ('class.PixariaCartObject.php');
		
		// Intitialise the cart object
		$objCartObject = new PixariaCartObject($cart_item_id);
		
		// Get variables from cart object
		$cart_id		= $objCartObject->getCartId();
		$image_id		= $objCartObject->getImageId();
		$image_path		= $objCartObject->getImagePath();

		// Intitialise the image object
		require_once ('class.PixariaImage.php');
		$objImage = new PixariaImage($image_id);
		
		// Load information about the image into Smarty
		$smarty->assign("image_data",$objImage->getImageDataComplete());
		
		// Intitialise the digital products object
		require_once ('class.PixariaProductsDigital.php');	
		$objProductsDigital = new PixariaProductsDigital();
		
		// Pass data to Smarty
		$smarty->assign("products_digital",$objProductsDigital->getProductsDigital());
		$smarty->assign("calc_description",$objProductsDigital->getCalculationDescription());
		$smarty->assign("calc_active",$objProductsDigital->getCalculationActive());
		$smarty->assign("calc_name",$objProductsDigital->getCalculationName());
		$smarty->assign("calc_id",$objProductsDigital->getCalculationId());
		$smarty->assign("calc_ar_id",$objProductsDigital->getArithmeticId());
		$smarty->assign("calc_ar_name",$objProductsDigital->getArithmeticName());
		$smarty->assign("calc_ar_operand",$objProductsDigital->getArithmeticOperand());
		$smarty->assign("calc_ar_factor",$objProductsDigital->getArithmeticFactor());
		$smarty->assign("calc_ar_function",$objProductsDigital->getArithmeticFunction());
		$smarty->assign("cart_item_id",$cart_item_id);
		
		// Set the page title
		$smarty->assign("page_title",$GLOBALS['_STR_']['STORE_195']);
		
		if ($objEnvData->fetchGlobal('cmd') == "recalculateOptionsPopup") {
		
			// Output the HTML page
			$smarty->pixDisplay("index.store/recalculate.options.popup.tpl");
			
		} elseif ($objEnvData->fetchGlobal('cmd') == "recalculateOptionsInline") {
		
			// Output the HTML page
			$smarty->pixDisplay("index.store/recalculate.options.inline.tpl");
			
		}
	
	}
	
	/*
	*	Get a quotation on a product or digital image
	*/
	function recalculatePrice() {
	
		global $cfg;
		global $lang;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		// Get post data
		$cart_item_id		= $objEnvData->fetchGlobal('cart_item_id');
		$cart_id			= $objEnvData->fetchGlobal('cart_id');
		$cart_member_type	= $objEnvData->fetchGlobal('cart_member_type');
		$quote_text			= $objEnvData->fetchGlobal('quote_text');
		
		// Load the cart class
		require_once ('class.PixariaCartObject.php');
		
		// Intitialise the cart object
		$objCartObject = new PixariaCartObject($cart_item_id);
		
		// Get the image ID
		$image_id		= $objCartObject->getImageId();
		
		// Load the image class
		require_once ('class.PixariaImage.php');
		
		// Intitialise the cart object
		$objImage = new PixariaImage($image_id);
		
		// Get properties of the image
		$image_path		= $objImage->getImagePath();
		$image_price 	= $objImage->getImagePriceCalculated();
		
		// Load information about the image into Smarty
		$smarty->assign("image_data",$objImage->getImageDataComplete());
		
		// Pass the required data back into Smarty
		$smarty->assign("cart_item_id",$cart_item_id);
		$smarty->assign("cart_id",$cart_id);
		
		// Get the selected options for this image
		$options = $objEnvData->fetchGlobal('options');
		
		// Calculate the price of the image
		$pricing = $objCartObject->calculatePrice($options,$image_price);
		
		// Output the calculated price to Smarty
		$smarty->assign("new_price",$pricing[new_price]);
		$smarty->assign("ar_id",$options);
		$smarty->assign("ar_description",$pricing[ar_description]);
		
		// Load the page title
		$smarty->assign("page_title",$GLOBALS['_STR_']['STORE_194']);
		
		if ($objEnvData->fetchGlobal('cmd') == "recalculatePricePopup") {
		
			// Output the HTML page
			$smarty->pixDisplay('index.store/price.recalculate.popup.tpl');
			
		} elseif ($objEnvData->fetchGlobal('cmd') == "recalculatePriceInline") {
		
			// Output the HTML page
			$smarty->pixDisplay("index.store/price.recalculate.inline.tpl");
			
		}
	
	}
	
	/*
	*	Add one or more products to the cart
	*/
	function addProducts () {
	
		global $cfg;
		global $lang;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		$image_id			= $objEnvData->fetchGlobal('image_id');
		$cart_member_type	= $objEnvData->fetchGlobal('cart_member_type');
		$quote_text			= $objEnvData->fetchGlobal('quote_text');
		$prod_id			= $objEnvData->fetchGlobal('prod_id');
		$quantity			= $objEnvData->fetchGlobal('quantity');
		$referer			= $objEnvData->fetchGlobal('referer');
		
		// Load the image class
		require_once ('class.PixariaImage.php');
		
		// Intitialise the image object
		$objImage = new PixariaImage($image_id);
		
		// Get image properties from the object
		$image_id		= $objImage->getImageId();
		$image_title	= addslashes($objImage->getImageTitle());
		$image_filename	= $objImage->getImageFileName();
		$image_path		= $objImage->getImagePath();
		$image_price 	= $objImage->getImagePriceCalculated();
		
		// Load information about the image into Smarty
		$smarty->assign("image_data",$objImage->getImageDataComplete());
		
		// Load the cart class
		require_once ('class.PixariaCartObject.php');
		
		// Intitialise the cart object
		$objCartObject = new PixariaCartObject();
		
		if (is_array($prod_id)) {
			
			// Load the product class
			require_once ('class.PixariaProduct.php');
			
			foreach ($prod_id as $key => $product) {
				
				$objProduct = new PixariaProduct($product);
				
				$quote_text 		= $objProduct->getProdDescription();
				$usage_text 		= $objProduct->getProdDescription();
				$price 				= $objProduct->getProdPrice();
				$shipping 			= $objProduct->getProdShipping();
				$shipping_multiple 	= $objProduct->getProdShippingMultiple();
				$prod_type	 		= $objProduct->getProdType();
				$prod_dnl_size	 	= $objProduct->getProdDownloadSize();
				
				// Set the cart properties
				$objCartObject->setImageId($image_id);
				
				// Set the product type (physical or download)
				if ($prod_type == 'IMG') {
					$objCartObject->setItemType('physical');
				} elseif ($prod_type == 'DNL') {
					$objCartObject->setItemType('digital');
				}
				
				$objCartObject->setImagePath($image_path);
				$objCartObject->setUsageText($quote_text);
				$objCartObject->setImageFileName($image_filename);
				$objCartObject->setImageTitle($image_title);
				$objCartObject->setQuoteText($quote_text);
				$objCartObject->setPrice($price);
				$objCartObject->setCartShipping($shipping);
				$objCartObject->setCartShippingMultiple($shipping_multiple);
				$objCartObject->setCartQuantity($quantity[$product]);
				$objCartObject->setCartProductCode($prod_type);
				$objCartObject->setCartDownloadSize($prod_dnl_size);
				
				// Add this item to the cart
				$objCartObject->createCartObject();
				
			}
			
		}
	
		if ($objEnvData->fetchGlobal('cmd') == "addProductsPopup") {
		
			// Set the page title
			$smarty->assign("page_title",$GLOBALS['_STR_']['STORE_196']);
			
			// Output the HTML page
			$smarty->pixDisplay("index.store/product.added.popup.tpl");
			
		} elseif ($objEnvData->fetchGlobal('cmd') == "addProductsInline") {
		
			// Set redirect url
			$this->viewShoppingCart();
			
		}
	
	}
	
	/*
	*	Add a single image to the cart
	*/
	function addImageQuote() {
	
		global $cfg;
		global $lang;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		$image_id			= $objEnvData->fetchGlobal('image_id');
		$cart_member_type	= $objEnvData->fetchGlobal('cart_member_type');
		$quote_text			= $objEnvData->fetchGlobal('quote_text');
		
		// Load the cart class
		require_once ('class.PixariaCartObject.php');
		$objCartObject = new PixariaCartObject();
		
		// Load the image class
		require_once ('class.PixariaImage.php');
		$objImage = new PixariaImage($image_id);
		
		// Load information about the image into Smarty
		$smarty->assign("image_data",$objImage->getImageDataComplete());
		
		// Pass the required data back into Smarty
		$smarty->assign("cart_item_id",$cart_item_id);
		$smarty->assign("cart_id",$cart_id);
		
		// Set the properties of this image object
		$objCartObject->setImageId($image_id);
		$objCartObject->setImagePath($objImage->getImagePath());
		$objCartObject->setUsageText($quote_text);
		$objCartObject->setPrice("");
		$objCartObject->setItemType('digital');
		$objCartObject->setImageFileName($objImage->getImageFileName());
		$objCartObject->setImageTitle($objImage->getImageTitle());
		$objCartObject->setQuoteText($quote_text);
		
		// Update the cart
		$objCartObject->createCartObject();
					
		if ($objEnvData->fetchGlobal('cmd') == "addImageQuotePopup") {
		
			// Load the page title
			$smarty->assign("page_title",$GLOBALS['_STR_']['STORE_196']);
			
			// Output the HTML page
			$smarty->pixDisplay('index.store/image.added.quote.popup.tpl');
			
		} elseif ($objEnvData->fetchGlobal('cmd') == "addImageQuoteInline") {
		
			// Set redirect url
			$meta_url	= $cfg['sys']['base_url'].$cfg['fil']['index_store'];
			
			// Print out the waiting screen
			$smarty->pixProcessing($meta_url,"1");
			
		}
	
	}
	
	/*
	*	Add a single image with a calculated price to the cart
	*/
	function addImageCalculated () {
		
		global $cfg;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		// Load the cart class
		require_once ('class.PixariaCartObject.php');
		$objCartObject = new PixariaCartObject();
		
		// Get the image ID
		$image_id = $objEnvData->fetchGlobal('image_id');
		
		// Load the image class
		require_once ('class.PixariaImage.php');
		$objImage = new PixariaImage($image_id);
		
		// Load information about the image into Smarty
		$smarty->assign("image_data",$objImage->getImageDataComplete());
		
		// Calculate the price of the image
		$pricing = $objCartObject->calculatePrice($objEnvData->fetchGlobal('options'),$objImage->getImagePriceCalculated());
		
		// Create text to tell the user and admin what pricing options were chosen
		$usage_text = $GLOBALS['_STR_']['STORE_197'].":\n" . implode(",\n", $objEnvData->fetchGlobal('ar_descriptions'));
		
		// Set properties for the changes to the cart object
		$objCartObject->setImageId($image_id);
		$objCartObject->setImagePath($objImage->getImagePath());
		$objCartObject->setPrice($pricing['new_price']);
		$objCartObject->setUsageText($usage_text);
		$objCartObject->setItemType('digital');
		$objCartObject->setImageFileName($objImage->getImageFileName());
		$objCartObject->setImageTitle($objImage->getImageTitle());
		$objCartObject->setQuoteText($usage_text);
		
		// Update the cart
		$objCartObject->createCartObject();

		if ($objEnvData->fetchGlobal('cmd') == "addImageCalcPopup") {
		
			// Load the page title
			$smarty->assign("page_title",$GLOBALS['_STR_']['STORE_194']);
				
			// Output the HTML page
			$smarty->pixDisplay('index.store/image.added.calc.popup.tpl');
			
		} elseif ($objEnvData->fetchGlobal('cmd') == "addImageCalcInline") {
		
			// Set redirect url
			$meta_url	= $cfg['sys']['base_url'].$cfg['fil']['index_store'];
			
			// Print out the waiting screen
			$smarty->pixProcessing($meta_url,"1");
			
		}
	
	}

	/*
	*	Update an image price with a new, recalculated price
	*/
	function updateImagePrice () {
		
		global $cfg;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		$cart_item_id	= $objEnvData->fetchGlobal('cart_item_id');
	
		// Load the cart class
		require_once ('class.PixariaCartObject.php');
		
		// Intitialise the cart object
		$objCartObject = new PixariaCartObject($cart_item_id);
		
		// Get the image ID
		$image_id = $objCartObject->getImageId();
		
		// Load the image class
		require_once ('class.PixariaImage.php');
		
		// Intitialise the image object
		$objImage = new PixariaImage($image_id);
		
		// Load information about the image into Smarty
		$smarty->assign("image_data",$objImage->getImageDataComplete());
		
		// Calculate the price of the image
		$pricing = $objCartObject->calculatePrice($objEnvData->fetchGlobal('options'),$objImage->getImagePriceCalculated());
		
		// Create text to tell the user and admin what pricing options were chosen
		$usage_text = $GLOBALS['_STR_']['STORE_197'].":\n" . implode(",\n", $objEnvData->fetchGlobal('ar_descriptions'));
		
		// Set properties for the changes to the cart object
		$objCartObject->setPrice($pricing['new_price']);
		$objCartObject->setUsageText($usage_text);
		$objCartObject->setQuoteText($usage_text);

		// Update the cart
		$objCartObject->updateCartObject();

		// Load the page title
		$smarty->assign("page_title",$GLOBALS['_STR_']['STORE_194']);

		if ($objEnvData->fetchGlobal('cmd') == "updateImagePricePopup") {
		
			// Output the HTML page
			$smarty->pixDisplay('index.store/image.updated.calc.tpl');
			
		} elseif ($objEnvData->fetchGlobal('cmd') == "updateImagePriceInline") {
		
			// Set redirect url
			$meta_url	= $cfg['sys']['base_url'].$cfg['fil']['index_store'];
			
			// Print out the waiting screen
			$smarty->pixProcessing($meta_url,"1");
			
		}
			
	}
	
	/*
	*	Update an image price with a new, recalculated price
	*/
	function removeCartItem () {
	
		global $cfg;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		// Load the cart class
		require_once ('class.PixariaCartObject.php');
		
		// Intitialise the cart object
		$objCartObject = new PixariaCartObject($objEnvData->fetchGlobal('cart_item_id'));
		
		// Remove the item from the cart
		$objCartObject->deleteCartObject();
		
		// Go back to the cart view page
		$this->viewShoppingCart();
		
	}
	
	/*
	*	Save changes to a basic shopping cart
	*/
	function saveCartBasic () {
	
		global $cfg;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		$image_title		= $objEnvData->fetchGlobal('image_title');
		$image_id			= $objEnvData->fetchGlobal('image_id');
		$image_filename		= $objEnvData->fetchGlobal('image_filename');
		$usage_text			= addslashes($objEnvData->fetchGlobal('usage_text'));
		$cart_id			= $objEnvData->fetchGlobal('cart_id');
		$store_type			= $cfg['set']['store_type'];	
		
		// Load the cart class
		require_once ('class.PixariaCart.php');
		
		// Intitialise the cart object
		$objCart = new PixariaCart($cart_id);
			
		// Change the store type and usage text properties
		$objCart->setStoreType($store_type);
		$objCart->setUsageText($usage_text);
		
		// Update the cart with the new information
		$objCart->updateCart($usage_text);

		// Update the quantities of items in this cart
		$this->updateProductQuantities();
		
		// Set redirect url to the referring page
		$meta_url	= $_SERVER['HTTP_REFERER'];
		
		// Print out the waiting screen
		$smarty->pixProcessing($meta_url,"1");
		
		// Stop running script
		exit;
				
	}
	
	/*
	*	Submit a basic cart to get a quote
	*/
	function submitCartBasic () {
	
		global $cfg;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		$payment_method	= $objEnvData->fetchGlobal('payment_method');
		$image_title	= $objEnvData->fetchGlobal('image_title');
		$image_id		= $objEnvData->fetchGlobal('image_id');
		$image_filename	= $objEnvData->fetchGlobal('image_filename');
		$usage_text		= addslashes($objEnvData->fetchGlobal('usage_text'));
		$cart_id		= $objEnvData->fetchGlobal('cart_id');
		$store_type		= $cfg['set']['store_type'];	
		
		if (isset($usage_text) && $usage_text != "" && is_numeric($cart_id)) {
			
			//	Set the cart status to 'pending', add the store operation mode 'store_type',
			//	add the usage_text for the user's quote and set the date processed to now.
			
			// Load the cart class
			require_once ('class.PixariaCart.php');
			
			// Intitialise the cart object
			$objCart = new PixariaCart($cart_id);
			
			// Set new properties for the cart
			$objCart->setStoreType($store_type);
			$objCart->setUsageText($usage_text);
			$objCart->setStatus("2");
			$objCart->setDateProcessed("NOW");
			$objCart->setPaymentMethod($payment_method);
			
			// Save changes to the cart
			$objCart->updateCart();

			// Get the user's name and e-mail address
			$details = getUserContactDetails($ses['psg_userid']);
			
			// Build required values for e-mails
			$to_name	= $details[0];
			$to_email	= $details[1];
			
			// Load variables required to send e-mail
			$smarty->assign("to_name",$to_name);
			$smarty->assign("to_email",$to_email);
			$smarty->assign("cart_id",$cart_id);
			
			// Load the e-mail class
			require_once ('class.PixariaEmail.php');
			
			// Initialise the e-mail object
			$objEmail = new PixariaEmail();
			
			// Set the object properties
			$objEmail->setSubject($cfg['set']['site_name']." - ".$GLOBALS['_STR_']['STORE_198']);
			$objEmail->setMessage($smarty->pixFetch('email.templates/submit.cart.user.tpl'));
			$objEmail->setRecipientAddress($to_email);
			$objEmail->setRecipientName($to_name);
			$objEmail->setSenderAddress($cfg['set']['contact_email']);
			$objEmail->setSenderName($cfg['set']['contact_name']);
			
			// Send the e-mail
			$objEmail->sendEmail();
			
			// Set the object properties
			$objEmail->setSubject($cfg['set']['site_name']." - ".$GLOBALS['_STR_']['STORE_198']);
			$objEmail->setMessage($smarty->pixFetch('email.templates/submit.cart.admin.tpl'));
			$objEmail->setRecipientAddress($cfg['set']['contact_email']);
			$objEmail->setRecipientName($cfg['set']['contact_name']);
			$objEmail->setSenderAddress($cfg['set']['contact_email']);
			$objEmail->setSenderName($cfg['set']['contact_name']);
			
			// Send the e-mail
			$objEmail->sendEmail();
			
			// Run the quantity updater function
			$this->updateQuantities();
		
			// Set the HTML page title and masthead text
			$smarty->assign("page_title",$GLOBALS['_STR_']['STORE_199']);
		
			// Display the empty cart page
			$smarty->pixDisplay('index.store/cart.processed.basic.tpl');

		} else { // The user hasn't entered any quote information
		
			// Run the quantity updater function
			$this->updateQuantities();
		
			// Set redirect url
			$meta_url	= $cfg['sys']['base_url'].$cfg['fil']['index_store'];
			
			// Print out the waiting screen
			$smarty->pixProcessing($meta_url,"1");
			
			// Stop running script
			exit;
		
		}
	
	}
	
	/*
	*	Save changes to an intermediate level cart
	*/
	function saveCartIntermediate () {
	
		global $cfg;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		$image_title		= $objEnvData->fetchGlobal('image_title');
		$image_id			= $objEnvData->fetchGlobal('image_id');
		$image_filename		= $objEnvData->fetchGlobal('image_filename');
		$usage_text			= $objEnvData->fetchGlobal('usage_text');
		$cart_id			= $objEnvData->fetchGlobal('cart_id');
		$cart_item_id		= $objEnvData->fetchGlobal('cart_item_id');
		$cart_item_type		= $objEnvData->fetchGlobal('cart_item_type');
		$store_type			= $cfg['set']['store_type'];	
		
		// Load the cart class
		require_once ('class.PixariaCart.php');
		
		// Intitialise the cart object
		$objCart = new PixariaCart($cart_id);
			
		// Load the cart object class
		require_once ('class.PixariaCartObject.php');
		
		if (is_array($cart_item_id) && is_array($usage_text)) {
			
			// Save changes to the cart
			foreach ($cart_item_id as $key => $value) {
				
				if ($cart_item_type[$key] == "digital") { // Only update the cart item if it's a digital image product
				
					// Intitialise the cart object
					$objCartObject = new PixariaCartObject($value);
					
					// Update the usage text for this item
					$objCartObject->setUsageText(addslashes($usage_text[$key]));
					
					// Save changes to this item
					$objCartObject->updateCartObject();				
				
				}
				
			}
			
			// Set the store type
			$objCart->setStoreType($store_type);
			
			// Update the cart
			$objCart->updateCart();
			
		}
		
		// Update the quantities of items in this cart
		$this->updateProductQuantities();
		
		// Set redirect url to the referring page
		$meta_url	= $_SERVER['HTTP_REFERER'];
		
		// Print out the waiting screen
		$smarty->pixProcessing($meta_url,"1");
		
		// Stop running script
		exit;
				
	}
	
	/*
	*	Submit an intermediate cart to get a quote
	*/
	function submitCartIntermediate () {
	
		global $cfg;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		$image_title	= $objEnvData->fetchGlobal('image_title');
		$image_id		= $objEnvData->fetchGlobal('image_id');
		$image_filename	= $objEnvData->fetchGlobal('image_filename');
		$usage_text		= $objEnvData->fetchGlobal('usage_text');
		$cart_id		= $objEnvData->fetchGlobal('cart_id');
		$cart_item_id	= $objEnvData->fetchGlobal('cart_item_id');
		$cart_item_type	= $objEnvData->fetchGlobal('cart_item_type');
		$store_type		= $cfg['set']['store_type'];	
		
		if (is_numeric($cart_id) && is_array($image_id) && is_array($usage_text)) {
			
			//	Set the cart status to 'pending', add the store operation mode 'store_type',
			//	add the usage_text for the user's quote and set the date processed to now.
			
			// Set the error message to false before we start looping through the array
			$usage_text_error = (bool)FALSE;
			
			// Load the cart object class
			require_once ('class.PixariaCartObject.php');
		
			// Load the cart class
			require_once ('class.PixariaCart.php');
		
			foreach ($image_id as $key => $value) {
				
				if ($cart_item_type[$key] == "digital") {
				
					$objCartObject = new PixariaCartObject($cart_item_id[$key]);
					
					$objCartObject->setUsageText(addslashes($usage_text[$key]));
					
					$objCartObject->updateCartObject();
					
					if ($usage_text[$key] == "") { $usage_text_error = (bool)TRUE; }
				
				}
				
			}
			
			if (!$usage_text_error) {
				
				// Generate cart object
				$objCart = new PixariaCart();
				
				// Set new properties for the cart
				$objCart->setStoreType($store_type);
				$objCart->setStatus("2");
				$objCart->setDateProcessed("NOW");
				
				// Save changes to the cart
				$objCart->updateCart();
			
				// Get the user's name and e-mail address
				$details = getUserContactDetails($ses['psg_userid']);
				
				// Build required values for e-mails
				$to_name	= $details[0];
				$to_email	= $details[1];
				
				// Load variables required to send e-mail
				$smarty->assign("to_name",$to_name);
				$smarty->assign("to_email",$to_email);
				$smarty->assign("cart_id",$cart_id);
				
				// Load the e-mail class
				require_once ('class.PixariaEmail.php');
				
				// Initialise the e-mail object
				$objEmail = new PixariaEmail();
				
				// Set the object properties
				$objEmail->setSubject($cfg['set']['site_name']." - ".$GLOBALS['_STR_']['STORE_198']);
				$objEmail->setMessage($smarty->pixFetch('email.templates/submit.cart.user.tpl'));
				$objEmail->setRecipientAddress($to_email);
				$objEmail->setRecipientName($to_name);
				$objEmail->setSenderAddress($cfg['set']['contact_email']);
				$objEmail->setSenderName($cfg['set']['contact_name']);
				
				// Send the e-mail
				$objEmail->sendEmail();
				
				// Set the object properties
				$objEmail->setSubject($cfg['set']['site_name']." - ".$GLOBALS['_STR_']['STORE_198']);
				$objEmail->setMessage($smarty->pixFetch('email.templates/submit.cart.admin.tpl'));
				$objEmail->setRecipientAddress($cfg['set']['contact_email']);
				$objEmail->setRecipientName($cfg['set']['contact_name']);
				$objEmail->setSenderAddress($cfg['set']['contact_email']);
				$objEmail->setSenderName($cfg['set']['contact_name']);
				
				// Send the e-mail
				$objEmail->sendEmail();
				
				// Set the HTML page title and masthead text
				$smarty->assign("page_title",$GLOBALS['_STR_']['STORE_199']);
			
				// Display the empty cart page
				$smarty->pixDisplay('index.store/cart.processed.intermediate.tpl');
				
				exit;
			}
			
		}
				
		// Update the quantities of items in this cart
		$this->updateProductQuantities();
		
		// Set redirect url to the referring page
		$meta_url	= $_SERVER['HTTP_REFERER'];
		
		// Print out the waiting screen
		$smarty->pixProcessing($meta_url,"1");
		
		// Stop running script
		exit;
				
	}
	
	/*
	*	Submit an advanced, intermediate or basic cart for offline payment
	*/
	function submitCartAdvanced () {
	
		global $cfg;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		// Get the cart_id
		$cart_id		= $objEnvData->fetchGlobal('cart_id');
		$payment_method	= $objEnvData->fetchGlobal('payment_method');
	
		// Load the cart class
		require_once ('class.PixariaCart.php');
		
		// Intitialise the cart object
		$objCart = new PixariaCart($cart_id);
		
		// Set the properties of the cart
		$objCart->setStoreType("11");
		$objCart->setStatus("2");
		$objCart->setDateProcessed("NOW");
		$objCart->setPaymentMethod($payment_method);
	
		// Update the cart
		$objCart->updateCart();
		
		// Get the user's name and e-mail address
		$details = getUserContactDetails($ses['psg_userid']);
		
		// Build required values for e-mails
		$to_name	= $details[0];
		$to_email	= $details[1];
		
		// Load variables required to send e-mail
		$smarty->assign("to_name",$to_name);
		$smarty->assign("to_email",$to_email);
		$smarty->assign("cart_id",$cart_id);
		
		// Load the e-mail class
		require_once ('class.PixariaEmail.php');
		
		// Initialise the e-mail object
		$objEmail = new PixariaEmail();
		
		// Set the object properties
		$objEmail->setSubject($cfg['set']['site_name']." - ".$GLOBALS['_STR_']['STORE_198']);
		$objEmail->setMessage($smarty->pixFetch('email.templates/submit.cart.user.tpl'));
		$objEmail->setRecipientAddress($to_email);
		$objEmail->setRecipientName($to_name);
		$objEmail->setSenderAddress($cfg['set']['contact_email']);
		$objEmail->setSenderName($cfg['set']['contact_name']);
		
		// Send the e-mail
		$objEmail->sendEmail();
		
		// Set the object properties
		$objEmail->setSubject($cfg['set']['site_name']." - ".$GLOBALS['_STR_']['STORE_198']);
		$objEmail->setMessage($smarty->pixFetch('email.templates/submit.cart.admin.tpl'));
		$objEmail->setRecipientAddress($cfg['set']['contact_email']);
		$objEmail->setRecipientName($cfg['set']['contact_name']);
		$objEmail->setSenderAddress($cfg['set']['contact_email']);
		$objEmail->setSenderName($cfg['set']['contact_name']);
		
		// Send the e-mail
		$objEmail->sendEmail();
			
		// Set the HTML page title and masthead text
		$smarty->assign("page_title",$GLOBALS['_STR_']['STORE_199']);
	
		// Display the empty cart page
		$smarty->pixDisplay('index.store/cart.processed.advanced.tpl');
	
	}
	
	/*
	*	Allows the user to submit a cart and set the payment method they wish to use
	*/
	function submitPaymentMethod () {
	
		global $cfg;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		$cart_id		= $objEnvData->fetchGlobal('cart_id');
		$store_type		= $cfg['set']['store_type'];	

		// Load the cart class
		require_once ('class.PixariaCart.php');
		
		// Generate cart object
		$objCart = new PixariaCart();
		
		// Set new properties for the cart
		$objCart->setStoreType($store_type);
		$objCart->setStatus("3");
		$objCart->setPaymentMethod($objEnvData->fetchGlobal('payment_method'));
		$objCart->setDateProcessed("NOW");
		
		// Save changes to the cart
		$objCart->updateCart();
	
		// Get the user's name and e-mail address
		$details = getUserContactDetails($ses['psg_userid']);
		
		// Build required values for e-mails
		$to_name	= $details[0];
		$to_email	= $details[1];
		
		// Load variables required to send e-mail
		$smarty->assign("to_name",$to_name);
		$smarty->assign("to_email",$to_email);
		$smarty->assign("cart_id",$cart_id);
		
		// Load the e-mail class
		require_once ('class.PixariaEmail.php');
		
		// Initialise the e-mail object
		$objEmail = new PixariaEmail();
		
		// Set the object properties
		$objEmail->setSubject($cfg['set']['site_name']." - ".$GLOBALS['_STR_']['STORE_200']);
		$objEmail->setMessage($smarty->pixFetch('email.templates/submit.cart.user.tpl'));
		$objEmail->setRecipientAddress($to_email);
		$objEmail->setRecipientName($to_name);
		$objEmail->setSenderAddress($cfg['set']['contact_email']);
		$objEmail->setSenderName($cfg['set']['contact_name']);
		
		// Send the e-mail
		$objEmail->sendEmail();
		
		// Set the object properties
		$objEmail->setSubject($cfg['set']['site_name']." - ".$GLOBALS['_STR_']['STORE_200']);
		$objEmail->setMessage($smarty->pixFetch('email.templates/submit.cart.admin.tpl'));
		$objEmail->setRecipientAddress($cfg['set']['contact_email']);
		$objEmail->setRecipientName($cfg['set']['contact_name']);
		$objEmail->setSenderAddress($cfg['set']['contact_email']);
		$objEmail->setSenderName($cfg['set']['contact_name']);
		
		// Send the e-mail
		$objEmail->sendEmail();
		
		// Set the HTML page title and masthead text
		$smarty->assign("page_title",$GLOBALS['_STR_']['STORE_199']);
	
		// Display the empty cart page
		$smarty->pixDisplay('index.store/cart.processed.intermediate.tpl');
		
		exit;

	}
	
	/*
	*	View details of a transaction
	*/
	function viewTransaction () {
	
		global $cfg;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		// Load required variables
		$cart_id	= $objEnvData->fetchGlobal('cart_id');
		$userid		= $ses['psg_userid'];
		
		// If the user isn't logged in, bounce to the login screen
		pix_authorise_user("basic");
		
		// Load the cart object class
		require_once ('class.PixariaCartObjects.php');
		
		// Intitialise the cart object
		$objCart = new PixariaCartObjects($cart_id);
			
		if ($objCart->cart_contents) {
							
			$smarty->assign("cart_id",$objCart->cart_id);
			$smarty->assign("calculator_href",$objCart->cart_item_calculator_href);
			$smarty->assign("cart_item_id",$objCart->cart_item_id);
			$smarty->assign("cart_item_type",$objCart->cart_item_type);
			$smarty->assign("item_usage_text",$objCart->cart_item_usage_text);
			$smarty->assign("cart_item_price",$objCart->cart_item_price);
			$smarty->assign("cart_usage_text",$objCart->cart_usage_text);			
			$smarty->assign("cart_item_usage_text",$objCart->cart_item_usage_text);			
			$smarty->assign("cart_item_price",$objCart->cart_item_price);			
			$smarty->assign("cart_item_shipping",$objCart->getCartItemShipping());			
			$smarty->assign("cart_item_shipping_multiple",$objCart->getCartItemShippingMultiple());			
			$smarty->assign("cart_item_prod_code",$objCart->getCartItemProductCode());			
			$smarty->assign("cart_item_dnl_size",$objCart->getCartItemDownloadSize());			
			$smarty->assign("cart_item_quantity",$objCart->getCartItemQuantity());			
			$smarty->assign("quote_complete",$objCart->cart_quote_complete);
			$smarty->assign("price_complete",$objCart->cart_price_complete);	
			$smarty->assign("image_id",$objCart->cart_item_image_id);
			$smarty->assign("image_title",$objCart->cart_item_image_title);
			$smarty->assign("image_filename",$objCart->cart_item_image_filename);
			$smarty->assign("image_icon_path",$objCart->cart_item_image_icon_path);
			$smarty->assign("image_icon_dims",$objCart->cart_item_image_icon_dims);
			$smarty->assign("image_small_path",$objCart->cart_item_image_small_path);
			$smarty->assign("image_small_dims",$objCart->cart_item_image_small_dims);
			$smarty->assign("image_large_path",$objCart->cart_item_image_large_path);
			$smarty->assign("image_large_dims",$objCart->cart_item_image_large_dims);
			$smarty->assign("image_comp_path",$objCart->cart_item_image_comp_path);
			$smarty->assign("image_comp_size",$objCart->cart_item_image_comp_size);

			$smarty->assign("cart_status",$objCart->getStatus());
			$smarty->assign("payment_method",$objCart->getPaymentMethod());
			$smarty->assign("download_link",$objCart->getDownloadLink());
			
			$smarty->assign("download_jpg",$objCart->getItemDownloadJPG());
			$smarty->assign("download_jp2",$objCart->getItemDownloadJP2());
			$smarty->assign("download_tif",$objCart->getItemDownloadTIF());
			$smarty->assign("download_psd",$objCart->getItemDownloadPSD());
			$smarty->assign("download_remaining",$objCart->getDownloadRemaining());
			
			// Give Smarty the URI of the current page
			$smarty->assign("referer",$_SERVER['REQUEST_URI']);
				
			// Set the HTML page title and masthead text
			$smarty->assign("page_title",$GLOBALS['_STR_']['STORE_192']);
			
			// Load variables into Smarty
			$smarty->assign("shipping",$objCart->cart_shipping);
			$smarty->assign("subtotal",$objCart->cart_subtotal);
			$smarty->assign("tax_total",$objCart->cart_tax_total);
			$smarty->assign("total",$objCart->cart_total);
			
			if ($objCart->getMessagesOn()) { // If there are messages
				
				$smarty->assign("message_id",$objCart->getMessagesId());
				$smarty->assign("message_username",$objCart->getMessagesUserName());
				$smarty->assign("message_user_email",$objCart->getMessagesEmail());
				$smarty->assign("message_text",$objCart->getMessagesText());
				$smarty->assign("message_time",$objCart->getMessagesTime());
				$smarty->assign("message_on",$objCart->getMessagesOn());
				
			}
			
			// Give Smarty the URI of the current page
			$smarty->assign("referer",$_SERVER['REQUEST_URI']);
				
			// Set the HTML page title and masthead text
			$smarty->assign("page_title",$GLOBALS['_STR_']['STORE_201']);
			
			switch ($objCart->getStoreType()) {
			
				case 10:
				
					// Display the basic cart page
					$smarty->pixDisplay('index.store/transaction.view.basic.tpl');
					
				break;
				
				case 11:
				case 12:
				
					// Display the advanced/intermediate cart page
					$smarty->pixDisplay('index.store/transaction.view.intermediate.tpl');
					
				break;
				
			}
			
		} else { // There is nothing in the cart or there is no cart to view
			
			// Set the HTML page title and masthead text
			$smarty->assign("page_title",$GLOBALS['_STR_']['STORE_202']);
		
			// Display the empty cart page
			$smarty->pixDisplay('index.store/cart.empty.tpl');

		}

	}
	
	/*
	*	Add a message to a transaction
	*/
	function transactionAddMessage () {
	
		global $cfg;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		// Load required variables
		$cart_id	= $objEnvData->fetchGlobal('cart_id');
		$message	= addslashes($objEnvData->fetchGlobal('message'));
		$userid		= $ses['psg_userid'];
		
		// Load the e-mail class
		require_once ('class.PixariaCart.php');
		
		// Initialise the cart
		$objCart = new PixariaCart($cart_id);
		
		// Create a new message and append it to the cart
		$objCart->createCartMessage ($message, $iUserId);
		
		// Get the user's name and e-mail address
		$details = getUserContactDetails($ses['psg_userid']);
		
		// Build required values for e-mails
		$to_name	= $details[0];
		$to_email	= $details[1];
		
		// Load variables required to send e-mail
		$smarty->assign("to_name",$to_name);
		$smarty->assign("to_email",$to_email);
		$smarty->assign("cart_id",$cart_id);
		
		// Load the e-mail class
		require_once ('class.PixariaEmail.php');
		
		// Initialise the e-mail object
		$objEmail = new PixariaEmail();
		
		// Set the object properties
		$objEmail->setSubject($cfg['set']['site_name']." - ".$GLOBALS['_STR_']['STORE_198']);
		$objEmail->setMessage($smarty->pixFetch('email.templates/transaction.message.admin.tpl'));
		$objEmail->setRecipientAddress($cfg['set']['contact_email']);
		$objEmail->setRecipientName($cfg['set']['contact_name']);
		$objEmail->setSenderAddress($cfg['set']['contact_email']);
		$objEmail->setSenderName($cfg['set']['contact_name']);
		
		// Send the e-mail
		$objEmail->sendEmail();

		// Set redirect url
		$meta_url	= $cfg['sys']['base_url'].$cfg['fil']['index_store']."?cmd=viewTransaction&amp;cart_id=$cart_id";
		
		// Print out the waiting screen
		$smarty->pixProcessing($meta_url,"1");
		
		// Stop running script
		exit;

	}
	
	function transactionDeleteItem () {
	
		global $cfg;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		$userid				= $ses['psg_userid'];
		$cart_item_id		= $objEnvData->fetchGlobal('cart_item_id');
		$cart_id			= $objEnvData->fetchGlobal('cart_id');
		
		if (isset($cart_item_id) && is_numeric($cart_item_id)) {
			
			// Load the cart object class
			require_once ('class.PixariaCartObject.php');
			
			// Intitialise the cart object
			$objCartObject = new PixariaCartObject($cart_item_id);
			
			// Remove the item from the cart
			$objCartObject->deleteCartObject();
						
		}

		// Set redirect url
		$meta_url	= $cfg['sys']['base_url'].$cfg['fil']['index_store']."?cart_id=$cart_id&amp;cmd=viewTransaction";
		
		// Print out the waiting screen
		$smarty->pixProcessing($meta_url,"1");
		
		// Stop running script
		exit;
		
	}
	
	/*
	*
	*	This method allows a user to download a purchased image
	*
	*/
	function downloadImage () {
	
		global $cfg;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		// Check that the user is logged in
		pix_authorise_user("basic");
		
		// Get required variables
		$cart_id		= $objEnvData->fetchGlobal('cart_id');
		$cart_item_id	= $objEnvData->fetchGlobal('cart_item_id');
		$file_extension	= $objEnvData->fetchGlobal('extension');
		
		// Store error checking variables
		$error			= false;
		$error_message	= array();
		
		/*
		*	Get the file type and extension to download
		*/
		switch ($file_extension) {
		
			case "tif":
				$extension	= ".tif";
				$type		= "tif";
			break;
			
			case "TIF":
				$extension	= ".TIF";
				$type		= "tif";
			break;
			
			case "psd":
				$extension	= ".psd";
				$type		= "psd";
			break;
			
			case "PSD":
				$extension	= ".PSD";
				$type		= "psd";
			break;
			
			case "jp2":
				$extension	= ".jp2";
				$type		= "jp2";
			break;
			
			case "JP2":
				$extension	= ".JP2";
				$type		= "jp2";
			break;
			
			case "jpg":
				$extension	= ".jpg";
				$type		= "jpg";
			break;
		
			case "JPG":
				$extension	= ".JPG";
				$type		= "jpg";
			break;
		
			case "eps":
				$extension	= ".EPS";
				$type		= "eps";
			break;
		
			case "EPS":
				$extension	= ".eps";
				$type		= "eps";
			break;
		
		}	
			
		// Load the cart object class
		require_once ('class.PixariaCartObject.php');
		
		// Intitialise the cart object
		$objCartObject = new PixariaCartObject($cart_item_id);
		
		// Get the current user's id
		$userid				= $ses['psg_userid'];
		
		// Get the image data
		$image_id			= $objCartObject->getImageId();
		$download_count		= $objCartObject->getItemDownloadCount();
		$new_download_count	= $objCartObject->getItemDownloadCount() + 1;
		$image_path			= $objCartObject->getImagePath();
		$image_filename		= $objCartObject->getImageFileName();
		$image_basename		= $objCartObject->getImageBaseName();
		$image_extension	= $objCartObject->getImageExtension();
		$item_type			= $objCartObject->getItemType();
		$item_prod_code		= $objCartObject->getItemProductCode();
		$item_dnl_size		= $objCartObject->getItemDownloadSize();
		
		// Load the cart object
		require_once ("class.PixariaCart.php");
		
		// Instantiate the cart object
		$objCart = new PixariaCart($cart_id);
				
		// Check if the cart has been paid for
		if ($objCart->getStatus() != "4") {
			$error = true;
			$error_message[] = $GLOBALS['_STR_']['STORE_139'];
		}		
		
		// Check the number of download attempts
		if ($download_count >= $cfg['set']['store_download_attempts']) {
			$error = true;
			$error_message[] = $GLOBALS['_STR_']['STORE_140'];
		}
		
		// Check the product type in the cart allows digital image downloads
		if ($item_prod_code != "DNL" && $item_type != "digital") {
			$error = true;
			$error_message[] = $GLOBALS['_STR_']['STORE_141'];
		}

		/*
		*	Choose the correct path to load the image from
		*/
		if ($item_prod_code == "DNL") {
		
			if ($item_dnl_size == "comping") {
			
				$subdir = COMPING_DIR;
				
			} else {
			
				$subdir = "original";
				
			}
		
		} else {
		
			$subdir = "original";
		
		}
		
		// Load the cart object class
		require_once ('class.PixariaImage.php');
			
		// Intitialise the cart object
		$objImage = new PixariaImage($image_id);
			
		// Log the image download
		$objImage->logImageDownload($type,'pay');
		
		
		
		// If no errors were found, then the user can download the file
		if (!$error) {
			
			// Set the new download counter
			$objCartObject->setCartItemDownloadCounter($new_download_count);
			
			// Update the download count
			$objCartObject->updateCartObject();
			
			// Set the path to the file
			$image_path = $cfg['sys']['base_library'].$image_path."/" . $subdir . "/".$image_basename . $extension;
			
			// Send forced download headers
			header("Content-Description: File Transfer");
			header("Content-Type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"" . $image_basename . $extension."\"");
			header("Content-length:".(string)(filesize($image_path)));
			
			// Output the image into the HTTP stream
			$fd = fopen($image_path, 'rb');
			while(!feof($fd)) {
			$buffer = fread($fd, 32 * 1024);
			print $buffer;
			}  
			fclose ($fd);
			
			// Exit the script
			exit;
			
		} else { // Caught an error that prevents the user downloading the file
			
			$smarty->assign("error_message",$error_message);
			$smarty->pixDisplay('index.store/transaction.downloads.exceeded.tpl');
		
		}

	}
	
	function downloadImageFree () {
	
		global $cfg;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		$download = (bool)FALSE;
		
		if ($cfg['set']['func_downloads']) {
		
			if ($cfg['set']['func_downloads_user'] == "10") {
			
				$download = (bool)TRUE;
			
			} elseif ($cfg['set']['func_downloads_user'] == "11" && $ses['psg_full_user']) {
			
				$download = (bool)TRUE;
			
			} elseif ($cfg['set']['func_downloads_user'] == "12" && $ses['psg_download']) {
			
				$download = (bool)TRUE;
			
			}
		
		}
		
		if ($download) {
		
			// Get the image_id
			// Get the image_id
			$image_id 		= $objEnvData->fetchGlobal('image_id');
			$file_extension	= $objEnvData->fetchGlobal('extension');
			
			switch ($file_extension) {
			
				case "tif":
				
					$extension		= ".tif";
					$type			= "tif";
				
				break;
				
				case "TIF":
				
					$extension		= ".TIF";
					$type			= "tif";
				
				break;
				
				case "psd":
				
					$extension		= ".psd";
					$type			= "psd";
				
				break;
				
				case "PSD":
				
					$extension		= ".PSD";
					$type			= "psd";
				
				break;
				
				case "jp2":
				
					$extension		= ".jp2";
					$type			= "jp2";
				
				break;
				
				case "JP2":
				
					$extension		= ".JP2";
					$type			= "jp2";
				
				break;
				
				case "jpg":
				
					$extension		= ".jpg";
					$type			= "jpg";
				
				break;
			
				case "JPG":
				
					$extension		= ".JPG";
					$type			= "jpg";
				
				break;
			
				case "eps":
				
					$extension		= ".EPS";
					$type			= "eps";
				
				break;
			
				case "EPS":

					$extension		= ".eps";
					$type			= "eps";
				
				break;
			
			}
		
			// Load the cart object class
			require_once ('class.PixariaImage.php');
			
			// Intitialise the cart object
			$objImage = new PixariaImage($image_id);
			
			// Get information about the images
			$image_filename 	= $objImage->getImageFileName();
			$image_path			= $objImage->getImagePath();
			$image_extension	= $objImage->getImageExtension();
			$image_basename		= $objImage->getImageBaseName();
			
			// Log the image download
			$objImage->logImageDownload($type,'free');
			
			// Set the path to the file
			$image_path = $cfg['sys']['base_library'].$image_path."/original/".$image_basename . $extension;
			
			// Send forced download headers
			header("Content-Description: File Transfer");
			header("Content-Type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"" . $image_basename . $extension."\"");
			header("Content-length:".(string)(@filesize($image_path)));
				
			// Output the image into the HTTP stream
			$fd = @fopen($image_path, 'rb');
			
			if ($fd) {
			
				while(!feof($fd)) {
				
					$buffer = fread($fd, 32 * 1024);
					echo $buffer;
				
				}
				
				fclose ($fd);
				
			}
			
			// Exit the script
			exit;
		
		} else {
		
			// Generate a redirection URL 
			$meta_url	= $cfg['sys']['base_url'];
						
			// Show redirect page
			$smarty->pixProcessing($meta_url,"1");
		
		}
	
	}
	
	/*
	*
	*	This method emtpies the cart of the currently logged in user
	*
	*/
	function emptyCart () {
	
		global $cfg;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		// Check that the user is logged in
		pix_authorise_user("basic");
		
		// Load the class file
		require_once ('class.PixariaCart.php');
		
		// Initialise the class
		$objCart = new PixariaCart();
	
		// Empty the cart
		$objCart->emptyCart();
		
		// Go back to the cart view page
		$this->viewShoppingCart();
		
	}
	
	/*
	*
	*	This method updates the quantity for each product in the user's cart
	*
	*/
	function updateProductQuantities () {
		
		global $cfg;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		// Run the quantity updater function
		$this->updateQuantities();
		
		$this->viewShoppingCart();
		
	}
	
	function updateQuantities () {
	
		global $cfg;
		global $ses;
		global $objEnvData;
		global $smarty;
		
		// Check that the user is logged in
		pix_authorise_user("basic");
		
		$cart_item_id	= $objEnvData->fetchPost('cart_item_id');
		$quantity		= $objEnvData->fetchPost('quantity');
		
		if (is_array($cart_item_id)) {
		
			foreach ($cart_item_id as $key => $value) {
			
				// Load the class file
				require_once ('class.PixariaCartObject.php');
				
				// Initialise the class
				$objCart = new PixariaCartObject($value);
				
				// Set the quantity
				$objCart->setCartQuantity($quantity[$key]);
				
				// Update the cart to reflect the new quantity
				$objCart->updateCartObject();
				
			}
		
		}
					
	}
	
}



?>