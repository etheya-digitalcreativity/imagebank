<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set up constant for front end activities
define("WORKSPACE","FE");

// Set the include path for files used in this script
ini_set("include_path","resources/includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Send HTTP header and don't cache
pix_http_headers("html","");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Load Markdown library
require_once ("functions.Markdown.php");
		
// Initialise the news object
$objIndexNews = new Index_News();

// This is the class for all the news methods
class Index_News {
	
	// Class constructor to define which methods are called
	function Index_News () {
		
		// Load in required data
		global $objEnvData;
		
		if ($objEnvData->fetchGet('id') != "") {
			
			// Show a single news item
			$this->showNewsItem($objEnvData->fetchGet('id'));
		
		} else {
			
			// Show the list of news articles
			$this->showNewsList();
		
		}
	
	}
	
	/*
	*	Show a list of news items
	*/
	function showNewsList () {
	
		// Load in required data
		global $smarty, $cfg, $objEnvData;
		
		// Set the page title
		$smarty->assign("page_title","News");
				
		$this_page = $cfg['base_url'].$cfg['fil']['index_news'];
		
		// Display html footer
		$smarty->pixDisplay('index.news/index.news.tpl');
	
	}
	
	/*
	*	Show a single news item
	*/
	function showNewsItem ($blog_item_id) {
		
		// Load in required data
		global $smarty, $cfg, $objEnvData;
		
		$sql = "SELECT ".$cfg['sys']['table_blog'].".*,
				
				".$cfg['sys']['table_imgs'].".image_path,
				
				".$cfg['sys']['table_imgs'].".image_filename,
				
				".$cfg['sys']['table_gall'].".gallery_title,
				
				".$cfg['sys']['table_gall'].".gallery_description,
				
				".$cfg['sys']['table_gall'].".gallery_short_title
				
				FROM ".$cfg['sys']['table_blog']."
		
				LEFT JOIN ".$cfg['sys']['table_gall']." ON ".$cfg['sys']['table_blog'].".blog_gallery_id = ".$cfg['sys']['table_gall'].".gallery_id
				
				LEFT JOIN ".$cfg['sys']['table_imgs']." ON ".$cfg['sys']['table_gall'].".gallery_key = ".$cfg['sys']['table_imgs'].".image_id
							
				WHERE blog_item_id = '$blog_item_id'";
			
		
		
		$result = sql_select_row($sql);
		
		if (is_array($result)) {
		
			$blog_item_id			= $result['blog_item_id'];
			$blog_date				= $result['blog_date'];
			$blog_title				= stripslashes($result['blog_title']);
			$blog_content			= stripslashes($result['blog_content']);
			$blog_href				= $result['blog_href'];
			$blog_gallery_id		= $result['blog_gallery_id'];
			$gallery_short_title	= $result['gallery_short_title'];
			$gallery_title			= $result['gallery_title'];
		
			$image_file_path		= $cfg['sys']['base_library'].$result['image_path']."/160x160/".$result['image_filename'];
			
			if (file_exists($image_file_path)) {
			
				$image_icon_size = getimagesize($image_file_path);
				$smarty->assign("image_icon_path",base64_encode($image_file_path));
				$smarty->assign("image_icon_dims",$image_icon_size[3]);
		
		
			}
		
		}
		
		// Load variables into Smarty
		$smarty->assign("blog_item_id",$blog_item_id);
		$smarty->assign("blog_date",$blog_date);
		$smarty->assign("blog_title",$blog_title);
		$smarty->assign("blog_content",markdown($blog_content));
		$smarty->assign("blog_href",$blog_href);
		$smarty->assign("gallery_title",$gallery_title);
		$smarty->assign("blog_gallery_id",$blog_gallery_id);
		
		if ($cfg['set']['func_mod_rewrite']) {
			if ($gallery_short_title != "") {
				$smarty->assign("blog_gallery_url",$cfg['sys']['base_url'] . "gallery/" . $gallery_short_title);
			} else {
				$smarty->assign("blog_gallery_url",$cfg['sys']['base_url'] . "gallery/" . $blog_gallery_id);
			}
		} else {
			$smarty->assign("blog_gallery_url",$cfg['sys']['base_url'] . $cfg['fil']['index_gallery'] . "?gid=" . $blog_gallery_id);
		}
		
		// Get the list of other blog items
		$result = sql_select_rows("SELECT * FROM ".$cfg['sys']['table_blog']." ORDER BY blog_date DESC LIMIT 0,20");
		
		if (is_array($result)) {
		
			foreach($result as $key => $value) {
			
				$list_blog_item_id[]		= $value['blog_item_id'];
				$list_blog_date[]			= $value['blog_date'];
				$list_blog_title[]			= stripslashes($value['blog_title']);
				$list_blog_content[]		= stripslashes($value['blog_content']);
				$list_blog_href[]			= $value['blog_href'];		
				
				if ($cfg['set']['func_mod_rewrite']) {
					$list_blog_url[] = $cfg['sys']['base_url'] . "news/" . $value['blog_item_id'];
				} else {
					$list_blog_url[] = $cfg['sys']['base_url'] . $cfg['fil']['index_news'] . "?id=" . $value['blog_item_id'];
				}
				
			}
		
		}
		
		$smarty->assign("list_blog_item_id",$list_blog_item_id);
		$smarty->assign("list_blog_date",$list_blog_date);
		$smarty->assign("list_blog_title",$list_blog_title);
		$smarty->assign("list_blog_content",$list_blog_content);
		$smarty->assign("list_blog_href",$list_blog_href);
		
		
		// Page title
		$smarty->assign("page_title",$blog_title);
		
		// Display the page header
		$smarty->pixDisplay('index.news/index.news.item.tpl');
	
	}

}



?>