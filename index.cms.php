<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set up constant for front end activities
define("WORKSPACE","FE");

// Set the include path for files used in this script
ini_set("include_path","resources/includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Send HTTP header and don't cache
pix_http_headers("html","");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Check that the user is a SuperUser
pix_authorise_user("photographer");

// Instantiate this page
$objIndexImageCMS = new IndexImageCMS();

class IndexImageCMS {
	
	var $_dbl;
	
	/*
	*
	*	Class constructor
	*
	*/
	function IndexImageCMS () {
		
		global $cfg, $smarty, $objEnvData, $ses;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		switch ($objEnvData->fetchGlobal('cmd')) {
						
			case "uploadNewImages":
				$this->uploadNewImages();
			break;
			
			case "processUploadJPEG":
				$this->processUploadJPEG();
			break;
			
			case "processUploadArchive":
				$this->processUploadArchive();
			break;
			
			case "listImagesAwaitingImport":
				$this->listImagesAwaitingImport();
			break;
			
			case "approveMetaData":
				$this->approveMetaData();
			break;
			
			case "importNewImages":
				$this->importNewImages();
			break;
			
			case "actionSaveImage";
				$this->actionSaveImage();
			break;
			
			case "formEditImage";
				$this->formEditImage();
			break;
			
			case "listCurrentImages": default:
				$this->listCurrentImages();
			break;
			
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function actionSaveImage () {
	
		global $smarty, $cfg, $ses, $objEnvData;
		
		// Load the image management class
		require_once ('class.PixariaImageAdmin.php');
		
		// Create a new image object
		$objImage = new PixariaImageAdmin($objEnvData->fetchPost('image_id'));
		
		if ($objImage->getImageUserId() != $ses['psg_userid']) {
		
			print "Action failed";
			exit;
		
		}
		
		// Get the correct date information to save as a timestamp
		switch ($objEnvData->fetchPost('date_option')) {
		
			case "EXIF":
		
				// Update the image date
				$objImage->setImageDate($objEnvData->fetchPost('exif_Year'),
										$objEnvData->fetchPost('exif_Month'),
										$objEnvData->fetchPost('exif_Day'),
										$objEnvData->fetchPost('exif_Hour'),
										$objEnvData->fetchPost('exif_Minute'),
										$objEnvData->fetchPost('exif_Second'));
			
			break;
			
			case "IPTC":
		
				// Update the image date
				$objImage->setImageDate($objEnvData->fetchPost('iptc_Year'),
										$objEnvData->fetchPost('iptc_Month'),
										$objEnvData->fetchPost('iptc_Day'),
										$objEnvData->fetchPost('iptc_Hour'),
										$objEnvData->fetchPost('iptc_Minute'),
										$objEnvData->fetchPost('iptc_Second'));
			
			break;
			
			case "CUST": default;
		
				// Update the image date
				$objImage->setImageDate($objEnvData->fetchPost('toda_Year'),
										$objEnvData->fetchPost('toda_Month'),
										$objEnvData->fetchPost('toda_Day'),
										$objEnvData->fetchPost('toda_Hour'),
										$objEnvData->fetchPost('toda_Minute'),
										$objEnvData->fetchPost('toda_Second'));
			
			break;
		
		}
		
		// Update the properties
		$objImage->setImageTitle($objEnvData->fetchPost('image_title'));
		$objImage->setImageCaption($objEnvData->fetchPost('image_caption'));
		$objImage->setImageKeywords($objEnvData->fetchPost('image_keywords'));
		$objImage->setImageWidth($objEnvData->fetchPost('image_width'));
		$objImage->setImageHeight($objEnvData->fetchPost('image_height'));
		$objImage->setImageCopyright($objEnvData->fetchPost('image_copyright'));
		$objImage->setImageColrEnable($objEnvData->fetchPost('image_colr_enable'));
		$objImage->setImageColrR($objEnvData->fetchPost('image_colr_r'));
		$objImage->setImageColrG($objEnvData->fetchPost('image_colr_g'));
		$objImage->setImageColrB($objEnvData->fetchPost('image_colr_b'));
		$objImage->setImageExtra1($objEnvData->fetchPost('image_extra_01'));
		$objImage->setImageExtra2($objEnvData->fetchPost('image_extra_02'));
		$objImage->setImageExtra3($objEnvData->fetchPost('image_extra_03'));
		$objImage->setImageExtra4($objEnvData->fetchPost('image_extra_04'));
		$objImage->setImageExtra5($objEnvData->fetchPost('image_extra_05'));
		
		$smarty->assign("referer",$objEnvData->fetchGlobal('referer'));
		$smarty->assign("return",$objEnvData->fetchGlobal('return'));
		
		//print_r($objImage);exit;
		
		// Update the details of this image
		$objImage->updateImage();		
		
		// Set the page title
		$smarty->assign("page_title","Changes saved");
		
		// Display a confirmation page
		$smarty->pixDisplay('index.cms/cms.image.edit.02.tpl');
	
	}
	
	/*
	*
	*	
	*
	*/
	function formEditImage () {
	
		global $smarty, $cfg, $ses, $objEnvData;
		
		// Set the page title
		$smarty->assign("page_title","Edit an image");
		
		// Get the image_id
		$image_id = $objEnvData->fetchGlobal('id');
		
		// Load the PixariaImage class so we can extend it
		require_once ('class.PixariaImageAdmin.php');
		
		// Initialise image object
		$objImage = new PixariaImageAdmin($image_id);
		
		// Load information into Smarty for the edit image screen
		$smarty->assign("image_id",$objImage->getImageId());
		$smarty->assign("image_active",$objImage->getImageActive());
		$smarty->assign("image_title",$objImage->getImageTitle());				
		$smarty->assign("image_caption",$objImage->getImageCaption());	
		$smarty->assign("image_keywords",$objImage->getImageKeywords());
		$smarty->assign("image_width",$objImage->getImageWidth());				
		$smarty->assign("image_height",$objImage->getImageHeight());				
		$smarty->assign("image_copyright",$objImage->getImageCopyright());
		$smarty->assign("image_permissions",$objImage->getImagePermissions());	
		$smarty->assign("image_rights_type",$objImage->getImageRightsType());				
		$smarty->assign("image_rights_text",$objImage->getImageRightsText());				
		$smarty->assign("image_model_release",$objImage->getImagemodelRelease());				
		$smarty->assign("image_property_release",$objImage->getImagePropertyRelease());				
		$smarty->assign("image_price",$objImage->getImagePrice());				
		$smarty->assign("image_mysql_date",$objImage->getImageDate());				
		$smarty->assign("image_sale",$objImage->getImageSale());				
		$smarty->assign("image_product_link",$objImage->getImageProductLink());				
		$smarty->assign("image_extra_01",$objImage->getImageExtra1());				
		$smarty->assign("image_extra_02",$objImage->getImageExtra2());				
		$smarty->assign("image_extra_03",$objImage->getImageExtra3());				
		$smarty->assign("image_extra_04",$objImage->getImageExtra4());				
		$smarty->assign("image_extra_05",$objImage->getImageExtra5());								
		$smarty->assign("image_colr_enable",$objImage->getImageColrEnable());				
		$smarty->assign("image_colr_mod",$objImage->getImageModRGB());				
		$smarty->assign("image_colr_hex",$objImage->getImageHexRGB());				
		$smarty->assign("image_colr_dec",$objImage->getImageDecRGB());				
		$smarty->assign("image_exif_date",$objImage->getExifDate());
		$smarty->assign("image_iptc_date",$objImage->getIptcDate());
		$smarty->assign("image_colr_average_hex",$objImage->getImageColrAverageHex());
		$smarty->assign("image_colr_average_dec",$objImage->getImageColrAverageDec());
		$smarty->assign("image_large_encoded",$objImage->getImageLargeEncoded());
		$smarty->assign("image_comp_encoded",$objImage->getImageCompEncoded());
		$smarty->assign("image_large_dims",$objImage->getImageLargeDimensions());
		$smarty->assign("image_comp_dims",$objImage->getImageCompDimensions());
		$smarty->assign("category_title",$objImage->getCategoryListTitles());
		$smarty->assign("category_id",$objImage->getCategoryListIds());
		$smarty->assign("group_title",$objImage->getGroupListTitles());
		$smarty->assign("group_id",$objImage->getGroupListIds());		
		$smarty->assign("image_category_links",$objImage->getImageCategoryLinks());
		$smarty->assign("image_group_links",$objImage->getImageGroupLinks());
		$smarty->assign("image_product_array",$objImage->getImageProducts());

		$smarty->assign("return",$objEnvData->fetchGlobal('return'));
		$smarty->assign("referer",$objEnvData->fetchServer('HTTP_REFERER'));
		
		require_once('class.PixariaProduct.php');
		
		$objProducts = new PixariaProduct();
		
		$product_data = $objProducts->getProducts();
		
		$smarty->assign("product_id",$product_data['prod_id']);
		$smarty->assign("product_name",$product_data['prod_name']);
		
		// Output the HTML page
		$smarty->pixDisplay('index.cms/cms.image.edit.01.tpl');
	
	}
	
	/*
	*
	*	
	*
	*/
	function listCurrentImages () {
	
		global $smarty, $cfg, $ses, $objEnvData;
		
		// Get the userid for the person whose images we want to look at
		$userid = $ses['psg_userid'];
		
		// Load the user class
		require_once('class.PixariaUser.php');
		
		// Create the user object
		$objUser = new PixariaUser($userid);
		
		// Get what page we're on
		$page = $objEnvData->fetchGlobal('page');
		
		// Generate SQL to view the user's images
		$sql = "SELECT * FROM ".PIX_TABLE_IMGS."
				
				WHERE image_userid = '$userid'
				
				ORDER BY image_filename";
		
		// Count the total number of orphan images
		$total	= $this->_dbl->sqlCountSelectRows($sql);
		
		// Select all images that are no in the gallery_order table (i.e. not yet in any galleries)
		$result = $this->_dbl->sqlSelectRows($sql . getMultiImagePageLimitSQL($page));

		$ipages = getMultiImagePageNavigation ("cms_images", $page, $total);
		
		if (is_array($result)) {
		
			// Initialise arrays to hold image data
			$image_id	 	= array();
			$image_active	= array();
			$image_filename = array();
			$image_path		= array();
			$image_date		= array();
			$icon_path		= array();
			$icon_width		= array();
			$icon_height	= array();
			$comp_path		= array();
			$comp_width		= array();
			$comp_height	= array();
			$image_width	= array();
			$image_height	= array();
			$image_title	= array();
		
			foreach ($result as $key => $value) {
				
				$image_id[]			= $value['image_id'];
				$image_active[]		= $value['image_active'];
				$image_filename[] 	= $value['image_filename'];
				$image_path[]		= $value['image_path'];
				$image_date[]		= $value['image_date'];
				
				$icon_path[]		= base64_encode($cfg['sys']['base_library'].$value[image_path]."/32x32/".$value[image_filename]);
				$icon_size			= @getimagesize($cfg['sys']['base_library'].$value[image_path]."/32x32/".$value[image_filename]);
				$icon_width[]		= $icon_size[0];
				$icon_height[]		= $icon_size[1];
				
				$comp_path[]		= base64_encode($cfg['sys']['base_library'].$value[image_path]."/".COMPING_DIR."/".$value[image_filename]);
				$comp_size			= @getimagesize($cfg['sys']['base_library'].$value[image_path]."/".COMPING_DIR."/".$value[image_filename]);
				$comp_width[]		= $comp_size[0];
				$comp_height[]		= $comp_size[1];
				
				$image_width[]		= $value[image_width];
				$image_height[]		= $value[image_height];
				
				$image_title[]		= $value[image_title];
				
			}
			
			// Assign image information to Smarty
			$smarty->assign("image_id",$image_id);
			$smarty->assign("image_active",$image_active);
			$smarty->assign("image_filename",$image_filename);
			$smarty->assign("image_path",$image_path);
			$smarty->assign("image_date",$image_date);
			$smarty->assign("icon_path",$icon_path);
			$smarty->assign("icon_width",$icon_width);
			$smarty->assign("icon_height",$icon_height);
			$smarty->assign("comp_path",$comp_path);
			$smarty->assign("comp_width",$comp_width);
			$smarty->assign("comp_height",$comp_height);
			$smarty->assign("image_width",$image_width);
			$smarty->assign("image_height",$image_height);
			$smarty->assign("image_title",$image_title);
			
			$smarty->assign("images_present",(bool)true);
			
			$smarty->assign("ipage_current",$ipages[0]);
			$smarty->assign("ipage_numbers",$ipages[1]);
			$smarty->assign("ipage_links",$ipages[2]);
			
		}	
		
		$smarty->assign("user_name",$objUser->getName());
		
		// Set the page title
		$smarty->assign("page_title","Your uploaded images");
		
		// Output the library page
		$smarty->pixDisplay('index.cms/cms.images.list.tpl');
		
	}
	
	/*
	*
	*	
	*
	*/
	function uploadNewImages () {
		
		global $smarty, $cfg, $ses;
		
		// Remove any old bits belonging to this user
		$directories = glob($cfg['set']['temporary']."/".$ses['psg_userid']."*");
		
		if (is_array($directories)) { 
			
			foreach ($directories as $key => $directory) {
				removeDirectoryComplete($directory);
			}
			
		}
		
		// Define html page title
		$smarty->assign("page_title","Upload images");
		
		// We need the lightbox javascript on this page
		$smarty->assign("js_multifile",true);

		// Output html from template file
		$smarty->pixDisplay('index.cms/cms.form.upload.tpl');

	}
	
	/*
	*
	*	
	*
	*/
	function processUploadJPEG () {
		
		global $smarty, $cfg, $ses;
		
		if (is_array($_FILES)) {
		
			foreach ($_FILES as $key => $value) {
				
				$file_name		= $value['name'];
				$file_type		= $value['type'];
				$file_temp_name	= $value['tmp_name'];
				$file_size		= $value['size'];
				$file_error		= $value['error'];
				
				if (file_exists($file_temp_name)) {
					
					if ($file_type == "image/jpeg" || $file_type == "image/jpg" || $file_type == "image/pjpeg") {
					
						$file_data = @getimagesize($file_temp_name);
						
						if ($file_data[2] == 2) {
							
							$new_path = $cfg['set']['temporary']."/".$ses['psg_userid']."/";
							
							if (!is_dir($new_path)) { mkdir($new_path); }
							
							// The file is a JPEG file so we move it to the incoming directory
							rename($file_temp_name,$new_path.$file_name);
							$old_umask = umask(0);
							@chmod($new_path.$file_name,0777);
							umask($old_umask);
							
						} else {
						
							// The file is not a JPEG file so we delete it
							@unlink($file_temp_name);
							
							// Set an error message
							$error = "The file you uploaded was not a valid JPEG file - please try again.";
							
						}
					
					} else {
						
						// The file is not a JPEG file so we delete it
						@unlink($file_temp_name);
							
						// Set an error message
						$error = "The file you uploaded was not a valid JPEG file - please try again.";
							
					}
					
				}
				
			}
		
			// There was a problem with the archive
			if ($error != "") { $this->showUploadError($error); }
			
			// Go to the next step
			$this->listImagesAwaitingImport();
							
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function processUploadArchive () {
	
		global $cfg, $smarty, $objEnvData, $ses;
		
		require_once (SYS_BASE_PATH.'resources/ext/archive_extractor/ArchiveExtractor.class.php');

		if (is_array($_FILES) && count($_FILES) == 1) { // There is a single archive file
		
			foreach ($_FILES as $key => $value) {
				
				$file_name		= $value['name'];
				$file_type		= $value['type'];
				$file_temp_name	= $value['tmp_name'];
				$file_size		= $value['size'];
				$file_error		= $value['error'];
				
				if (file_exists($file_temp_name)) {
					
					if ($file_type == "application/zip" || $file_type == "application/x-tar" || $file_type == "application/x-gzip") {
						
						$file_name_info = explode(".",$file_name);
						
						$file_extension = array_pop($file_name_info);
						
						if ($file_extension == ("zip" || "gz" || "gzip" || "tgz" || "tar")) {
						
							$new_path = $cfg['set']['temporary']."".$ses['psg_userid']."/";
							
							if (!is_dir($new_path)) { mkdir($new_path); }

							// The file is a valid archive
							rename($file_temp_name,$new_path.$file_name);
							$old_umask = umask(0);
							@chmod($new_path.$file_name,0777);
							umask($old_umask);
							
							// Init. ArchiveExtractor Object
							$archExtractor = new ArchiveExtractor();
							
							/* Extract */
							$extractedFileList = $archExtractor->extractArchive($new_path.$file_name,$new_path);
							
							// Delete the archive
							@unlink($new_path.$file_name);
							
							foreach ($extractedFileList as $key => $value) {
								
								$path_parts = pathinfo($value['filename']);
								
								$file_name = $path_parts['basename'];
							
								$file_list .= "fn[]=" . urlencode($file_name) . "&amp;";
							
							}
							
							// Go to the next step
							$this->listImagesAwaitingImport();
							
						} else {
						
							// The file is not an archive file so we delete it
							@unlink($file_temp_name);
							
							// Set an error message
							$error = "The file you uploaded was not a valid zip, tar or gzip archive - please try again making sure that the archive contains only JPEG images.";
							
						}
					
					} else {
						
						// The file is not an archive file so we delete it
						@unlink($file_temp_name);
						
						// Set an error message
						$error = "The file you uploaded was not a valid zip, tar or gzip archive - please try again making sure that the archive contains only JPEG images.";
						
					}
					
				} else { // Something went wrong during the upload as the file doesn't exist
				
					// Set an error message
					$error = "No valid archive was uploaded.  This may be because the file you attempted to upload was larger than PHP's upload limit or there is a problem with the configuration of your Pixaria website.";
						
				}

			}
			
		} else { // No file was uploaded or too many files were uploaded
		
			// Set an error message
			$error = "No valid archive was uploaded.  You can only upload a single zip, tar or gzip archive of JPEG images.";
						
		}
		
		// There was a problem with the archive
		if ($error != "") { $this->showUploadError($error); }
			
	}
	
	/*
	*
	*	
	*
	*/
	function showUploadError ($error) {
		
		global $smarty;
		
		$smarty->assign("error_message",$error);
		
		// Show the error message to the user
		$smarty->pixDisplay('index.cms/cms.error.upload.tpl');
		
		// Exit the script
		exit;
	
	}
	
	/*
	*
	*	
	*
	*/
	function listImagesAwaitingImport () {
	
		global $smarty, $ses, $cfg, $objEnvData;
		
		// Set the path where the images will be stored while they await import
		$temp_path = $cfg['set']['temporary']."".$ses['psg_userid']."/";
		
		// Find out what valid photo albums are on the system and present them as options to the user
		$handle=@opendir($temp_path);
		if($handle != FALSE) {
			$fn=1;
			while (false!==($filename = readdir($handle))) { 
				
				if (substr($filename,0,1) != ".") { // Exclude filenames starting with a period
					
					$pathinfo = pathinfo($temp_path."/".$filename);
					if (strtolower($pathinfo['extension'])==("tif"||"jpg"||"mov"||"gif"||"png"||"tiff"||"avi")) { // Only include image files
					
						$imageinfo = @getimagesize($temp_path."/".$filename);
						
						$image_data[$fn]['filepath']	= base64_encode($temp_path."/".$filename);
						$image_data[$fn]['filename']	= $filename;
						$image_data[$fn]['filesize']	= round((filesize($temp_path."/".$filename) / 1024),2);
						$image_data[$fn]['width']		= $imageinfo[0];
						$image_data[$fn]['height']		= $imageinfo[1];
						$image_data[$fn]['filemtime']	= filemtime($temp_path."/".$filename);
						$image_data[$fn]['resizeable']	= imageIsResizeable($temp_path."/".$filename);
						$fn++;
						
					}
					
				}
				
			}
			
		}
		
		@closedir($handle);
		
		if (is_array($image_data)) {
		
			// Sort the items into alphabetical order (not all servers do it automatically)
			@natsort($image_data);
			
			foreach ($image_data as $key => $value) {
			
				$filepath[]		= $value['filepath'];
				$filename[]		= $value['filename'];
				$filesize[]		= $value['filesize'];
				$width[]		= $value['width'];
				$height[]		= $value['height'];
				$filemtime[]	= $value['filemtime'];
				$resizeable[]	= $value['resizeable'];
			
			}
		
		}
		
		/*
		*	List directories created by this user in the library directory
		*/
		$cwd = getcwd();
		chdir($cfg['sys']['base_library']);
		$library = glob($ses['psg_userid']."*");
		chdir($cwd);
		
		if (is_array($library)) {
		
			$show_directories = (bool)TRUE;
		
		}
				
		/*
		*	Input directory information into Smarty
		*/
		$smarty->assign("show_directories",$show_directories);
		$smarty->assign("library",$library);
		
		// Tell Smarty whether or not to show a table of items
		if (is_array($filepath)) { $smarty->assign("display","TRUE"); }
		
		// Assign variables to Smarty object
		$smarty->assign("filepath",$filepath);
		$smarty->assign("filename",$filename);
		$smarty->assign("filesize",$filesize);
		$smarty->assign("width",$width);
		$smarty->assign("height",$height);
		$smarty->assign("filemtime",$filemtime);
		$smarty->assign("resizeable",$resizeable);
		
		// Set the page title
		$smarty->assign("page_title","Import images");
		
		// Display the html header
		$smarty->pixDisplay('index.cms/cms.form.import.01.tpl');
		
		exit;
		
	}
	
	/*
	*
	*	
	*
	*/
	function approveMetaData () {
	
		global $objEnvData, $cfg, $smarty, $ses;
		
		// Load the directory name
		$directory_name = $objEnvData->fetchPost('directory_name');
		
		// Replace disallowed characters with hyphen characters
		$directory_name = preg_replace("/[\s\\.\\,\\+\\(\\)\\{\\}\[\]\\~\\!\\$\\%\\^\\&\\*\\@\\#\\<\\>\\?\\=]/","-",$directory_name);

		if ($directory_name != "" && !eregi('[^a-zA-Z0-9_-]{1,}', $directory_name)) {
			
			if (!eregi("^".$ses['psg_userid']."_",$directory_name)) {
			
				// Use the user's provided directory name
				$temp_name = $ses['psg_userid'] . "_" . $directory_name;
			
			} else {
				
				// Use the user's provided directory name
				$temp_name = $directory_name;
				
			}
			
		} else {
		
			// Generate a username for the user
			$temp_name = $ses['psg_userid'] . "_" . date("Y-m-d-",time()) . substr(strtoupper(md5(microtime())),0,4);
		
		}
		
		// Check that the user isn't refreshing the page and if they are, take them direct to the import screen
		if (is_dir($cfg['set']['temporary'].$temp_name."/")) {
			
			// The images have already been resized, we now need only to check they're all there
			$this->checkAllImagesPresent($temp_name);
			
		}
		
		$images = $objEnvData->fetchPost('images');
		
		if (is_array($images)) {
			
			$read_write 	= "TRUE";
			$image_writable	= array();
			
			foreach($images as $key => $value) {
			
				if (!is_writable($cfg['set']['temporary'] . "/" . $ses['psg_userid'])) {
					
					$read_write 		= "FALSE";
				
				} elseif (!is_writable($cfg['set']['temporary'] . $ses['psg_userid'] . "/" . $value)) {
				
					$read_write			= "FALSE";
					$images_writable[]	= "FALSE";
					
				} else {
				
					$images_writable[]	= "TRUE";
					
				}
			
			}
			
			if ($read_write == "TRUE") {
					
				$src_path = $cfg['set']['temporary']."/".$ses['psg_userid']."/";
				
				$des_path = $cfg['set']['temporary'].$temp_name."/";
				
				// Make a new directory for this import
				$directory_created	= $this->createDirectory($des_path);
				
				// Make a new directory for the original images
				$directory_created	= $this->createDirectory($des_path."/original/");
				
				// Make a new directory for the comp images
				$directory_created	= $this->createDirectory($des_path."/".COMPING_DIR."/");
				
				// Make a new directory for the large thumbnail images
				$directory_created	= $this->createDirectory($des_path."/160x160/");
				
				// Make a new directory for the small thumbnail images
				$directory_created	= $this->createDirectory($des_path."/80x80/");
				
				// Make a new directory for the icon images
				$directory_created	= $this->createDirectory($des_path."/32x32/");
				
				$icon_path			= array();
				$icon_width			= array();
				$icon_height		= array();
				$file_name			= array();
				$upload_arr_o		= array();
				$upload_arr_c		= array();
				$upload_arr_l		= array();
				$upload_arr_s		= array();
				$upload_arr_i		= array();
				
				foreach($images as $key => $value) {
					
					$image_size = @getimagesize($src_path.$value);
					
					if ($image_size[0] > COMPING_SIZE || $image_size[1] > COMPING_SIZE) {
					
						// Move original images into the new original directory
						if (@rename($src_path.$value, $des_path."/original/".$value)) {
						
							// Create comp image 630 x 630 and save
							$this->resizeImageFile($des_path."/original/".$value, $des_path."/".COMPING_DIR."/".$value,COMPING_SIZE);
							
							// Create large thumbnail image 160 x 160 and save
							$this->resizeImageFile($des_path."/".COMPING_DIR."/".$value, $des_path."/160x160/".$value,"160");
							
							// Create small thumbnail image 80 x 80 and save
							$this->resizeImageFile($des_path."/160x160/".$value, $des_path."/80x80/".$value,"80");
							
							// Create icon image 32 x 32 and save
							$this->resizeImageFile($des_path."/80x80/".$value, $des_path."/32x32/".$value,"32");
							
						}
						
					} else {
					
						// Move original images into the new original directory
						if (@rename($src_path.$value, $des_path."/".COMPING_DIR."/".$value)) {
						
							// Create large thumbnail image 160 x 160 and save
							$this->resizeImageFile($des_path."/".COMPING_DIR."/".$value, $des_path."/160x160/".$value,"160");
							
							// Create small thumbnail image 80 x 80 and save
							$this->resizeImageFile($des_path."/160x160/".$value, $des_path."/80x80/".$value,"80");
							
							// Create icon image 32 x 32 and save
							$this->resizeImageFile($des_path."/80x80/".$value, $des_path."/32x32/".$value,"32");
							
						}
						
					}
					
				}
				
				// Take the user to the import screen
				$this->checkAllImagesPresent($temp_name);
			
			} else {
				
				// Set the page title
				$smarty->assign("page_title","File permissions problem");
				
				// Set the title to show
				$smarty->assign("error_title","File permissions problem");
				
				// Load the array of images' read/write status into Smarty
				$smarty->assign("images_writable",$images_writable);
				
				// Load the array of image names into Smarty
				$smarty->assign("images",$images);
			
				// Display the library import page
				$smarty->pixDisplay('admin.library/library.import.02.tpl');
			
			}
			
		}
		
	}
	
	/*
	*
	*	This method checks that required images are present, creates thumbnails and puts images into the correct locations
	*
	*/
	function checkAllImagesPresent ($temp_name) {
		
		global $objEnvData, $cfg, $smarty;
		
		$directory = $cfg['set']['temporary']."/".$temp_name."/";
		
		// Check that PHP can read and write to this directory
		if (is_writable($directory)) { $permission_ok = "TRUE"; }

		// Check minimum required directories are present and writable
		if ($permission_ok && (is_dir($directory."/".COMPING_DIR."/") || is_dir($directory."/original/"))) { 
			
			// Initialise array
			$images = array();
			
			// Open directory for reading contents
			$handle=@opendir($directory."/".COMPING_DIR."/");
			
			// If the directory is accessible
			if($handle != FALSE) {
			
				while (false!==($file = readdir($handle))) { 
					
					if (strtolower(substr($file,-4,4)) == ".jpg") { 
					  	
					  	// Put image file names into array
						$images[] = $file;
	
					}
				
				}
				
			}
			
			// Close the directory
			@closedir($handle);
			
			if (is_array($images)) {
			
				// Sort the list alphabetically
				natsort($images);
			
				$icon_path			= array();
				$icon_width			= array();
				$icon_height		= array();
				$file_name			= array();
				$upload_arr_o		= array();
				$upload_arr_c		= array();
				$upload_arr_l		= array();
				$upload_arr_s		= array();
				$upload_arr_i		= array();
				
				// Assume all images are present and correct
				$all_present = "TRUE";
				
				// Load the metadata class
				require_once ('class.Library.MetaData.php');
				
				// Instantiate the metadata class
				$objMetaData = new PixariaMetaData();
		
				/*
				*	Loop through the array of image file names
				*/
				foreach ($images as $key => $value) {
										
					if ($upload_arr_i[]		= file_exists($directory."/32x32/".$value)) 	{ $icon_size = @getimagesize($directory."/32x32/".$value); }
					if ($upload_arr_s[]		= file_exists($directory."/80x80/".$value)) 	{ $small_size = @getimagesize($directory."/80x80/".$value); }
					if ($upload_arr_l[]		= file_exists($directory."/160x160/".$value)) 	{ $large_size = @getimagesize($directory."/160x160/".$value); }
					if ($upload_arr_c[]		= file_exists($directory."/".COMPING_DIR."/".$value)) 	{ $comp_size = @getimagesize($directory."/".COMPING_DIR."/".$value); }
					if ($upload_arr_o[]		= file_exists($directory."/original/".$value)) 	{ $original_size = @getimagesize($directory."/original/".$value); }
					
					$icon_path[] 			= base64_encode($directory."/32x32/".$value);
					$small_path[] 			= base64_encode($directory."/80x80/".$value);
					$large_path[] 			= base64_encode($directory."/160x160/".$value);
					$comp_path[] 			= base64_encode($directory."/".COMPING_DIR."/".$value);
					$original_path[] 		= base64_encode($directory."/original/".$value);
					
					$icon_width[]			= $icon_size[0];
					$icon_height[]			= $icon_size[1];
					
					$small_width[]			= $small_size[0];
					$small_height[]			= $small_size[1];
					
					$large_width[]			= $large_size[0];
					$large_height[]			= $large_size[1];
					
					$comp_width[]			= $comp_size[0];
					$comp_height[]			= $comp_size[1];
					
					$original_width[]		= $original_size[0];
					$original_height[]		= $original_size[1];
					
					$file_name[]			= $value;
					
					$upload_arr_o[] 		= file_exists($directory."/original/".$value);
					
					if (!$upload_arr_c[] 	= file_exists($directory."/".COMPING_DIR."/".$value)) 	{ $all_present = "FALSE"; }
					if (!$upload_arr_l[] 	= file_exists($directory."/160x160/".$value)) 	{ $all_present = "FALSE"; }
					if (!$upload_arr_s[] 	= file_exists($directory."/80x80/".$value)) 	{ $all_present = "FALSE"; }
					if (!$upload_arr_i[] 	= file_exists($directory."/32x32/".$value)) 	{ $all_present = "FALSE"; }
					
					if (file_exists($directory."/original/".$value)) {
						$image_width[] 		= $original_size[0];
						$image_height[] 	= $original_size[1];
					} else {
						$image_width[] 		= $comp_size[0];
						$image_height[] 	= $comp_size[1];
					}
					
					if (file_exists($directory."/original/".$value)) {
					
						$image_size[] 	= @getimagesize($directory."/original/".$value);
						$image_exif		= $objMetaData->extractEXIFMetaData($directory."/original/".$value);
						$image_iptc		= $objMetaData->extractIPTCMetaData($directory."/original/".$value);
						
					} elseif (file_exists($directory."/".COMPING_DIR."/".$value)) {
					
						$image_size[] 	= @getimagesize($directory."/".COMPING_DIR."/".$value);
						$image_exif		= $objMetaData->extractEXIFMetaData($directory."/".COMPING_DIR."/".$value);
						$image_iptc		= $objMetaData->extractIPTCMetaData($directory."/".COMPING_DIR."/".$value);
						
					}

					/*
					*	Load IPTC metadata
					*/
					$iptc_copyright[]	= $image_iptc['copyright'];
					$iptc_objname[]		= $image_iptc['objname'];
					$iptc_headline[]	= $image_iptc['headline'];
					$iptc_creator[]		= $image_iptc['byline'];
					$iptc_jobtitle[]	= $image_iptc['bltitle'];
					$iptc_city[]		= $image_iptc['city'];
					$iptc_country[]		= $image_iptc['country'];
					$iptc_credit[]		= $image_iptc['credit'];
					$iptc_source[]		= $image_iptc['source'];
					$iptc_byline[]		= $image_iptc['byline'];
					$iptc_object[]		= $image_iptc['objname'];
					$iptc_keywords[] 	= $image_iptc['keywords'];
					$iptc_caption[] 	= $image_iptc['caption'];
					
					// Create a Unix style timestamp for the EXIF image capture date
					$exif_timestamp		= $image_exif[1];
										
					if ($image_iptc['date'] != "") {
	
						$iptc_date_yy	= substr($image_iptc['date'],0,4);
						$iptc_date_mm	= substr($image_iptc['date'],4,2);
						$iptc_date_dd	= substr($image_iptc['date'],6,2);
						$iptc_date_hh	= 0;
						$iptc_date_mi	= 0;
						$iptc_date_ss	= 0;
						
					} else {
					
						$iptc_date_yy	= 0;
						$iptc_date_mm	= 0;
						$iptc_date_dd	= 0;
						$iptc_date_hh	= 0;
						$iptc_date_mi	= 0;
						$iptc_date_ss	= 0;
						
					}
				
					// Get the image date, IPTC takes precedence
					if ($iptc_date_yy != "0") {
						
						$image_datetime[] = "$iptc_date_yy-$iptc_date_mm-$iptc_date_dd 00:00:00";
						
					} elseif ($image_exif['timestamp'] != "") {
	
						$image_datetime[] = date("Y-m-d H:i:s",$image_exif['timestamp']);
					
					} else {
					
						$image_datetime[] = date("Y-m-d H:i:s",filemtime($file_fullpath));
						
					}
					
					// Get the image date, IPTC takes precedence
					if ($iptc_date_yy != "0") {
						
						$iptc_date[] = $iptc_date_yy."-".$iptc_date_mm."-".$iptc_date_dd." 00:00:00";
						
					} else {
					
						$iptc_date[] = (bool)false;
					
					}
					
					if ($exif_timestamp != "") {
	
						$exif_date[] = date("Y-m-d H:i:s",$exif_timestamp);
					
					} else {
					
						$exif_date[] = (bool)false;
					
					}
					
				}			
	
				// Load array of image information into Smarty
				$smarty->assign("directory",$directory);
				$smarty->assign("temp_name",$temp_name);
				$smarty->assign("icon_path",$icon_path);
				$smarty->assign("icon_width",$icon_width);
				$smarty->assign("icon_height",$icon_height);
				$smarty->assign("small_path",$small_path);
				$smarty->assign("small_width",$small_width);
				$smarty->assign("small_height",$small_height);
				$smarty->assign("large_path",$large_path);
				$smarty->assign("large_width",$large_width);
				$smarty->assign("large_height",$large_height);
				$smarty->assign("comp_path",$comp_path);
				$smarty->assign("comp_width",$comp_width);
				$smarty->assign("comp_height",$comp_height);
				$smarty->assign("file_name",$file_name);
				$smarty->assign("image_datetime",$image_datetime);
				$smarty->assign("image_width",$image_width);
				$smarty->assign("image_height",$image_height);
				$smarty->assign("upload_arr_o",$upload_arr_o);
				$smarty->assign("upload_arr_c",$upload_arr_c);
				$smarty->assign("upload_arr_l",$upload_arr_l);
				$smarty->assign("upload_arr_s",$upload_arr_s);
				$smarty->assign("upload_arr_i",$upload_arr_i);
				$smarty->assign("exif_date",$exif_date);
				
				// Load IPTC info into Smarty
				$smarty->assign("iptc_date",$iptc_date);
				$smarty->assign("iptc_keywords",$iptc_keywords);
				$smarty->assign("iptc_caption",$iptc_caption);
				$smarty->assign("iptc_copyright",$iptc_copyright);
				$smarty->assign("iptc_objname",$iptc_objname);
				$smarty->assign("iptc_headline",$iptc_headline);
				$smarty->assign("iptc_creator",$iptc_creator);
				$smarty->assign("iptc_jobtitle",$iptc_jobtitle);
				$smarty->assign("iptc_city",$iptc_city);
				$smarty->assign("iptc_country",$iptc_country);
				$smarty->assign("iptc_credit",$iptc_credit);
				$smarty->assign("iptc_source",$iptc_source);
				$smarty->assign("iptc_object",$iptc_object);
				$smarty->assign("iptc_byline",$iptc_byline);

				// Tell Smarty if any images are missing
				$smarty->assign("all_present",$all_present);
				
			}
			
			// Set the page title
			$smarty->assign("page_title","Check image metadata");
		
			// Output the library page
			$smarty->pixDisplay('index.cms/cms.form.import.02.tpl');		

		} else {
		
			// Set the page title
			$smarty->assign("page_title","File permissions problem");
			
			// Display the library import page
			$smarty->pixDisplay('admin.library/library.import.02.tpl');
		
		}
		
		// Stop running the script here
		exit;
		
	}
	
	/*
	*
	*	This method imports the images into the database and moves the files into the library directory
	*
	*/
	function importNewImages () {
		
		global $ses, $objEnvData, $smarty;
		
		$image_path 			= $objEnvData->fetchPost('temp_name');
		$image_filename 		= $objEnvData->fetchPost('image_filename');
		$image_title 			= $objEnvData->fetchPost('image_title');
		$image_caption			= $objEnvData->fetchPost('image_caption');
		$image_keywords			= $objEnvData->fetchPost('image_keywords');
		$image_width			= $objEnvData->fetchPost('image_width');
		$image_height			= $objEnvData->fetchPost('image_height');
		$date_type 				= $objEnvData->fetchPost('date_type');
		$date_custom 			= $objEnvData->fetchPost('date_custom');
		$date_exif 				= $objEnvData->fetchPost('date_exif');
		$date_iptc 				= $objEnvData->fetchPost('date_iptc');
		$image_property_release = $objEnvData->fetchPost('image_property_release');
		$image_model_release 	= $objEnvData->fetchPost('image_model_release');
		$image_rights_type 		= $objEnvData->fetchPost('image_rights_type');
		$image_rights_text 		= $objEnvData->fetchPost('image_rights_text');
		$iptc_creator			= $objEnvData->fetchPost('iptc_creator');
		$iptc_jobtitle			= $objEnvData->fetchPost('iptc_jobtitle');
		$iptc_city				= $objEnvData->fetchPost('iptc_city');
		$iptc_country			= $objEnvData->fetchPost('iptc_country');
		$iptc_credit			= $objEnvData->fetchPost('iptc_credit');
		$iptc_source			= $objEnvData->fetchPost('iptc_source');
		$iptc_object			= $objEnvData->fetchPost('iptc_object');
		$iptc_byline			= $objEnvData->fetchPost('iptc_byline');
		
		if (is_array($image_filename)) {
			
			require_once ('class.PixariaImage.php');
			
			$objImage = new PixariaImage();
			
			foreach ($image_filename as $key => $value) {
									
				list($icon_w, $icon_h) = @getimagesize($cfg['sys']['temporary'] . $image_path . "/32x32/" . $image_filename[$key]);
				list($smal_w, $smal_h) = @getimagesize($cfg['sys']['temporary'] . $image_path . "/80x80/" . $image_filename[$key]);
				list($larg_w, $larg_h) = @getimagesize($cfg['sys']['temporary'] . $image_path . "/160x160/" . $image_filename[$key]);
				list($comp_w, $comp_h) = @getimagesize($cfg['sys']['temporary'] . $image_path . "/".COMPING_DIR."/" . $image_filename[$key]);
				list($orig_w, $orig_h) = @getimagesize($cfg['sys']['temporary'] . $image_path . "/original/" . $image_filename[$key]);

				$objImage->setImageActive("0");
				$objImage->setImageFileName($image_filename[$key]);
				$objImage->setImageTitle($image_title[$key]);
				$objImage->setImageCaption($image_caption[$key]);
				$objImage->setImageWidth($image_width[$key]);
				$objImage->setImageHeight($image_height[$key]);
				$objImage->setImageKeywords($image_keywords[$key]);
				$objImage->setImageUserId($ses['psg_userid']);
				$objImage->setImagePath($image_path);
				$objImage->setImageModelRelease($image_model_release[$key]);
				$objImage->setImagePropertyRelease($image_property_release[$key]);
				$objImage->setImageCopyright($ses['psg_name']);
				$objImage->setImageRightsType($image_rights_type[$key]);
				$objImage->setImageRightsText($image_rights_text[$key]);
				
				$objImage->setIconWidth($icon_w);
				$objImage->setIconHeight($icon_h);
				$objImage->setSmallWidth($smal_w);
				$objImage->setSmallHeight($smal_h);
				$objImage->setLargeWidth($larg_w);
				$objImage->setLargeHeight($larg_h);
				$objImage->setCompingWidth($comp_w);
				$objImage->setCompingHeight($comp_h);
				$objImage->setOriginalWidth($orig_w);
				$objImage->setOriginalHeight($orig_h);
				
				$objImage->setIPTCCreator($iptc_creator[$key]);
				$objImage->setIPTCJobtitle($iptc_jobtitle[$key]);
				$objImage->setIPTCCity($iptc_city[$key]);
				$objImage->setIPTCCountry($iptc_country[$key]);
				$objImage->setIPTCCredit($iptc_credit[$key]);
				$objImage->setIPTCSource($iptc_source[$key]);
				$objImage->setIPTCObject($iptc_object[$key]);
				$objImage->setIPTCByline($iptc_byline[$key]);

				$dkey = $key . "Day";
				$mkey = $key . "Month";
				$ykey = $key . "Year";
				$hkey = $key . "Hour";
				$nkey = $key . "Minute";
				$skey = $key . "Second";

				switch ($date_type[$key]) {
				
					case "custom":
						$objImage->setImageDate($date_custom[$ykey],$date_custom[$mkey],$date_custom[$dkey],"00","00","00");
					break;
					
					case "iptc":
						$objImage->setImageDate($date_iptc[$ykey],$date_iptc[$mkey],$date_iptc[$dkey],"00","00","00");
					break;

					case "exif":
						$objImage->setImageDate($date_exif[$ykey],$date_exif[$mkey],$date_exif[$dkey],$date_exif[$hkey],$date_exif[$nkey],$date_exif[$skey]);
					break;
				
				}
			
				// Move the image file into the library directory
				$move_success = $this->moveImageFileToLibrary($image_filename[$key], $image_path);
				
				if ($move_success) {
					
					$objImage->createImage();
				
				} else {
					
					$partial_failure = (bool)true;
					$failed_images[] = $image_filename;
				
				}
				
			}
		
			if (count($image_filenames) == count($failed_images)) {
				$complete_failure = (bool)true;
			}
			
			// Set the page title
			$smarty->assign("page_title","Import process complete");
			
			// Load the HTML page
			$smarty->pixDisplay('index.cms/cms.form.import.03.tpl');
		
		}
		
	}
	
	/*
	*
	*	Move an image file from the incoming to library folder
	*
	*/
	function moveImageFileToLibrary($image_filename, $image_path) {
	
		global $cfg;
				
		$m_dir_i = $cfg['set']['temporary'] . "/" . $image_path;
		$o_dir_i = $cfg['set']['temporary'] . "/" . $image_path . "/original/";
		$c_dir_i = $cfg['set']['temporary'] . "/" . $image_path . "/".COMPING_DIR."/";
		$l_dir_i = $cfg['set']['temporary'] . "/" . $image_path . "/160x160/";
		$s_dir_i = $cfg['set']['temporary'] . "/" . $image_path . "/80x80/";
		$i_dir_i = $cfg['set']['temporary'] . "/" . $image_path . "/32x32/";
		
		$m_dir_l = $cfg['sys']['base_library'] . $image_path;
		$o_dir_l = $cfg['sys']['base_library'] . $image_path . "/original/";
		$c_dir_l = $cfg['sys']['base_library'] . $image_path . "/".COMPING_DIR."/";
		$l_dir_l = $cfg['sys']['base_library'] . $image_path . "/160x160/";
		$s_dir_l = $cfg['sys']['base_library'] . $image_path . "/80x80/";
		$i_dir_l = $cfg['sys']['base_library'] . $image_path . "/32x32/";
		
		if (!is_dir($m_dir_l)) { // Image directory doesn't exist so we need to make it
			$this->createDirectory($m_dir_l);
		}
		
		if (!is_dir($o_dir_l)) {
			$this->createDirectory($o_dir_l);
		}
	
		if (!is_dir($c_dir_l)) {
			$this->createDirectory($c_dir_l);
		}
	
		if (!is_dir($l_dir_l)) {
			$this->createDirectory($l_dir_l);
		}
	
		if (!is_dir($s_dir_l)) {
			$this->createDirectory($s_dir_l);
		}
	
		if (!is_dir($i_dir_l)) {
			$this->createDirectory($i_dir_l);
		}
		
		// Get an array of all types of this file in the original directory
		$original_files = glob($cfg['set']['temporary'] . "/" .  $image_path . "/original/" . substr($image_filename,0,-4) . "*");
		
		if (is_array($original_files)) {
		
			// Loop through files in the original (full size directory) to move any files matching the name of this one
			foreach ($original_files as $filename) {
				
				$path_parts = pathinfo($filename);
				$current_file = $path_parts['basename'];
				if (file_exists($o_dir_i.$current_file) && !file_exists($o_dir_l.$current_file)) {
					// Move the original image to the library	
					@rename("$o_dir_i$current_file" , "$o_dir_l$current_file");
				}	
			
			}
			
		} else {
		
			if (file_exists($o_dir_i.$image_filename) && !file_exists($o_dir_l.$image_filename)) {
				// Move the original image to the library	
				@rename("$o_dir_i$image_filename" , "$o_dir_l$image_filename");
			}	
			
		}
		
		if (file_exists($c_dir_i.$image_filename) && !file_exists($c_dir_l.$image_filename)) {
			// Move the original image to the library	
			@rename("$c_dir_i$image_filename" , "$c_dir_l$image_filename");
		} else {
			return false;
		}
		
		if (file_exists($l_dir_i.$image_filename) && !file_exists($l_dir_l.$image_filename)) {
			// Move the original image to the library	
			@rename("$l_dir_i$image_filename" , "$l_dir_l$image_filename");
		} else {
			return false;
		}
		
		if (file_exists($s_dir_i.$image_filename) && !file_exists($s_dir_l.$image_filename)) {
			// Move the original image to the library	
			@rename("$s_dir_i$image_filename" , "$s_dir_l$image_filename");
		} else {
			return false;
		}
		
		if (file_exists($i_dir_i.$image_filename) && !file_exists($i_dir_l.$image_filename)) {
			// Move the original image to the library	
			@rename("$i_dir_i$image_filename" , "$i_dir_l$image_filename");
		} else {
			return false;
		}
		
		return true;
		
	}

	/*
	*
	*	Check if a directory exists and if it doesn't make it
	*
	*/
	function createDirectory($path) {
		
		global $cfg;
		
		if (!file_exists($path)) {
			
			$umask_old = umask(0);
			mkdir($path, 0777);
			$directory_created = TRUE;
			umask($old);
			
		} elseif (substr(sprintf('%o', fileperms($path)), -4) != 0777) {
			
			$path = umask(0);
			@chmod($path,0777);
			umask($old);
			
			$directory_created = FALSE;
			
		}
		
		return $directory_created;
	
	}
	
	/*
	*
	*	Take an image ($src_path), resize it to fixed width/height ($new_dims) and save it to a new location ($des_path)
	*
	*/
	function resizeImageFile($src_path,$des_path,$new_dims) {
		
		global $cfg;
		
		// Load the source graphic
		$source = @imagecreatefromjpeg($src_path);
		
		if ($source) {
		
			$imageX = @imagesx($source);
			$imageY = @imagesy($source);
			
			if ($imageX >= $imageY) {
			
				$thumbX = $new_dims;
				$thumbY = (int)(($thumbX*$imageY) / $imageX );
				
			} else {
			
				$thumbY = $new_dims;
				$thumbX = (int)(($thumbY*$imageX) / $imageY );
				
			}
		
			$dest_thum  = @imagecreatetruecolor($thumbX, $thumbY);
			@imagecopyresampled ($dest_thum, $source, 0, 0, 0, 0, $thumbX, $thumbY, $imageX, $imageY);
			@imageinterlace($dest_thum);
			@imagejpeg($dest_thum,$des_path,$cfg['set']['gd_quality']);
			
			return TRUE;
		
		} else {
		
			return FALSE;
		
		}
		
	}

}

?>