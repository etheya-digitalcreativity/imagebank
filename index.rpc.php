<?php 

/*
*
*	Pixaria Gallery
*	CopyrightJamie Longstaff
*
*/

// Set up constant for front end activities
define("WORKSPACE","FE");

// Set the include path for files used in this script
ini_set("include_path","resources/includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

$objIndexPreferences = new IndexPreferences;

class IndexPreferences {
	
	var $version;
	var $img_per_page;
	var $gal_per_page;
	var $thumb_size;
	
	/*
	*	
	*	
	*
	*/
	function IndexPreferences () {
	
		global $objEnvData, $ses, $cfg, $smarty;
		
		$this->version		= $ses[0];
		$this->img_per_page = $ses[1];
		$this->gal_per_page = $ses[2];
		$this->thumb_size	= $ses[3];
		
		switch ($objEnvData->fetchGlobal('cmd')) {
			
			// Set the preferences for large thumbnail images
			case "setThumbSizeLarge":
				$this->setThumbSizeLarge();
			break;
			
			// Set the preferences for small thumbnail images
			case "setThumbSizeSmall":
				$this->setThumbSizeSmall();
			break;
			
			// The default action for this page with no cmd variable
			default:
				$this->setDefaultThumbsPerPage();
			break;
		
		}
	
	}
	
	/*
	*
	*
	*
	*/
	function setDefaultThumbsPerPage () {
	
		$this->setGalleriesPerPage($objEnvData->fetchGlobal('dsg'));
		$this->setImagesPerPage($objEnvData->fetchGlobal('dsp'));
		
		// Send HTTP header and don't cache
		pix_http_headers("html","");
		
		// Load the preferred viewing prefs into a cookie
		setcookie("psg_prefs","1|".$this->img_per_page."|".$this->gal_per_page."|".$this->thumb_size."",time() + 63072000,"/");
		
		// Print out the waiting screen
		$smarty->pixProcessing($_SERVER['HTTP_REFERER'],"1");
	
	}
	
	/*
	*	
	*	
	*
	*/
	function setThumbSizeLarge () {
	
		
	
	}
	
	/*
	*	
	*	
	*
	*/
	function setThumbSizeSmall () {
	
		
	
	}
	
	/*
	*	
	*	
	*
	*/
	function setImagesPerPage($img_per_page) {
	
		$this->img_per_page = $img_per_page;
	
	}
	
	/*
	*	
	*	
	*
	*/
	function setGalleriesPerPage($gal_per_page) {
	
		$this->gal_per_page = $gal_per_page;
	
	}
	
}



?>