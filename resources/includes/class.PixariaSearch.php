<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

// Load the main Smarty class
require_once ("class.PixariaMultiImage.php");

class PixariaSearch extends PixariaMultiImage {
	
	var $_dbl;
	
	// Search terms
	var $keywords_simple_boolean;
	var $gallery_id;
	var $srch_date_st;
	var $srch_date_en;
	var $date_srch;
	var $srch_date_is_dd;
	var $srch_date_is_mm;
	var $srch_date_is_yy;
	var $srch_date_st_dd;
	var $srch_date_st_mm;
	var $srch_date_st_yy;
	var $srch_date_en_dd;
	var $srch_date_en_mm;
	var $srch_date_en_yy;
	var $model_release;
	var $property_release;
	var $rights_managed;
	var $image_colr_enable;
	var $image_colr_r;
	var $image_colr_g;
	var $image_colr_b;
	var $image_orientation;
	var $search_directory;
	var $search_userid;
	var $log_search;
	var $search_tokens;
	
	// Search data output
	var $output_total;
	var $output_keywords_simple_boolean;
	var $output_gallery_id;
	var $output_srch_date_st;
	var $output_srch_date_en;
	var $output_date_srch;
	var $output_date_is_time;
	var $output_date_st_time;
	var $output_date_en_time;
	var $output_model_release;
	var $output_property_release;
	var $output_rights_managed;
	var $output_gallery_list;
	var $output_ipages;
	var $output_image_data;
	var $search_logged;
	
	// Core search properties
	var $search_image;
	var $search_image_data;
	var $search_image_count;
	var $search_terms_error;
	
	/*
	*
	*	
	*
	*/
	function PixariaSearch ($multi_image_page = "", $search_image = "", $type = "", $sid = "", $search_within = "") {
		
		global $cfg, $ses, $objEnvData;
		
		$this->search_type 			= $type;
		
		if ($type == "admin") {
			
			$this->multi_image_type		= "admin_search";
		
		} elseif ($type == "admin_popup") {
			
			$this->multi_image_type		= "admin_search_popup";
		
		} else {
			
			$this->multi_image_type		= "search";
		
		}
		
		$this->multi_image_page		= $multi_image_page;
		$this->multi_image_image	= $search_image;
		$this->multi_image_sid		= $sid;
		$this->search_within 		= $search_within;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		// Get the search terms (from cookie or form data
		$this->getSearchTerms();
		
		// Check that the bare minimum of search terms are present
		$this->checkSearchTerms();
		
		// If the minimum required search terms are present do the search, otherwise report error and exit
		if (!$this->search_terms_error) {
			
			/*
			*
			*
			*	This is an existing search
			*
			*
			*/
			if (is_numeric($this->multi_image_sid)) {
				
				// Run the search on the stored searches table
				$total_results = $this->_dbl->sqlCountRows("SELECT COUNT(image_id) FROM ".PIX_TABLE_SRCS." WHERE search_id = '".$this->multi_image_sid."'");
				
				// Update the timeout on search results - not in use yet...!
				//$this->_dbl->sqlQuery("UPDATE ".PIX_TABLE_SRCS." SET date = ".time()." WHERE search_id = '".$this->multi_image_sid."'");
				
				// Get the total number of search results
				$this->search_image_count = $total_results;
				
				// Set the number of search results
				$this->multi_image_count = $this->search_image_count;
			
			/*
			*
			*
			*	This is a new search
			*
			*
			*/
			} else {
				
				// Run the search on the main database
				$total_results = $this->_dbl->sqlSelectRows($this->generateSearchQuery());
			
				// Get the total number of search results
				$this->search_image_count = $this->countSearchResults($total_results);
				
				// Set the number of search results
				$this->multi_image_count = $this->search_image_count;
			
				// Save the search terms in the database
				$this->setSearchInDB();
				
				if ($objEnvData->fetchGlobal('cmd') == "doSearch") {
					$this->saveSearchResultsIndex($total_results);
				}

			}
			
			/*
			*
			*
			*	We're looking at a single image
			*
			*
			*/
			if (is_numeric($this->multi_image_image)) {
				
				// If we're looking at a single image page, load required information
				$this->search_image_data = $this->processSearchImageDetail();
			
			}
			
		}
		
	}
	
	/*
	*
	*	Saves search results into a temporary storage table for the 'search within' function
	*
	*/
	function saveSearchResultsIndex ($data) {
		
		global $ses;
		
		if (is_array($data) && is_numeric($this->multi_image_sid)) {
			
			/*
			*	Delete current stored results for this user
			*/
			$sql = "DELETE FROM ".PIX_TABLE_SRCS." WHERE search_id = '".$this->multi_image_sid."'";
			$this->_dbl->sqlQuery($sql);
			
			/*
			*	Delete any stored search results older than 1 hours
			*/
			$sql = "DELETE FROM ".PIX_TABLE_SRCS." WHERE date < '". (time() - 3600) ."'";
			$this->_dbl->sqlQuery($sql);
			
			/*
			*	Loop through the search results data and save the information in the database
			*/
			foreach ($data as $key => $value) {
				
				$sql_insert[] = " ( '0', '".$ses['psg_userid']."', '".$this->multi_image_sid."', '".$value['image_id']."', '".time()."' ) ";
			}
			
			// Now doing the insert in one go as this is more efficient...
			$sql = "INSERT INTO ".PIX_TABLE_SRCS." VALUES ". implode(" , ",$sql_insert) .";\n";
			$this->_dbl->sqlQuery($sql);

					
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function checkSearchTerms () {
		
		if ( 	
			$this->keywords_simple_boolean != "" ||
			$this->image_colr_enable == "on" ||
			($this->image_orientation != "ne" && $this->image_orientation != "") ||
			$this->model_release == "on" ||
			$this->property_release == "on" ||
			$this->gallery_id >= 1 ||
			$this->search_directory != "" ||
			$this->search_userid != "" ||
			$this->date_srch == "11" ||
			$this->date_srch == "12"
			) {
			
			$this->search_terms_error = (bool)false;
		
		} else {
		
			$this->search_terms_error = (bool)true;
		
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function getSearchResultsCount () {
	
		return $this->search_image_count;
	
	}
	
	/*
	*
	*	This function saves a search query as a cookie so that it can easily be reused
	*
	*/
	function setSearchCookie () {
		
		$keywords_simple_boolean = rtrim($this->keywords_simple_boolean);
		
		$cookie = urlencode("$keywords_simple_boolean|$this->gallery_id|$this->srch_date_st|$this->srch_date_en|$this->date_srch|$this->srch_date_is_dd|$this->srch_date_is_mm|$this->srch_date_is_yy|$this->srch_date_st_dd|$this->srch_date_st_mm|$this->srch_date_st_yy|$this->srch_date_en_dd|$this->srch_date_en_mm|$this->srch_date_en_yy|$this->model_release|$this->property_release|$this->results_order|$this->image_colr_enable|$this->image_colr_r|$this->image_colr_g|$this->image_colr_b|$this->image_orientation|$this->rights_managed");
		
		// Set a Cookie for this search
		setcookie("psg_search",$cookie,null,"/");
		
	}
	
	/*
	*
	*	
	*
	*/
	function getSearchCookie () {
	
		global $cfg, $ses, $objEnvData, $smarty;
		
		$items = explode("|",urldecode($objEnvData->fetchCookie('psg_search')));
		
		$this->parseSearchTerms($items);
		
	}
	
	/*
	*
	*	This method adds a search query to the database
	*
	*/
	function setSearchInDB () {
		
		global $ses;
		
		if ($this->multi_image_sid == "") {
			
			// Set the type of search (admin or front end)
			if (eregi("admin",$this->search_type)) { $type = 0; } else { $type = 1; }
			
			// Set the current user's userid
			$userid = $ses['psg_userid'];
			
			// Get the keywords
			$keywords_simple_boolean = rtrim($this->keywords_simple_boolean);
			
			// Compile an array of the search terms
			$items = array($keywords_simple_boolean, $this->gallery_id, $this->srch_date_st, $this->srch_date_en, $this->date_srch, $this->srch_date_is_dd, $this->srch_date_is_mm, $this->srch_date_is_yy, $this->srch_date_st_dd, $this->srch_date_st_mm, $this->srch_date_st_yy, $this->srch_date_en_dd, $this->srch_date_en_mm, $this->srch_date_en_yy, $this->model_release, $this->property_release, $this->results_order, $this->image_colr_enable, $this->image_colr_r, $this->image_colr_g, $this->image_colr_b, $this->image_orientation, $this->rights_managed, $this->search_directory, $this->search_within);
			
			// Serialize the array
			$string = serialize($items);
			
			// Create SQL query to store the serialised array in the database
			$sql = "INSERT INTO ".PIX_TABLE_SRCH." VALUES ('0','$userid','$type','".mysql_real_escape_string($string)."');";	
			
			// Store the data
			$this->_dbl->sqlQuery($sql);
			
			// Set the search ID
			$this->multi_image_sid = $this->_dbl->sqlInsertId();
			
		}
		
		
	}
	
	/*
	*
	*	This method retrieves a search query stored in the database
	*
	*/
	function getSearchInDB ($id="", $type="", $userid="") {
		
		if ($id != "" &&  $userid != "") {
		
			$sql = "SELECT string FROM ".PIX_TABLE_SRCH." WHERE userid = '$userid' AND id = '$id';";
	
		} elseif ($id != "") {		
		
			$sql = "SELECT string FROM ".PIX_TABLE_SRCH." WHERE userid = '0' AND id = '$id';";
	
		} else {
		
			return false;
		
		}
		
		$data = $this->_dbl->sqlSelectRow($sql);
		
		$items = unserialize($data['string']);
		
		$this->parseSearchTerms($items);
		
	}
	
	/*
	*
	*	
	*
	*/
	function parseSearchTerms ($items) {
	
		$this->keywords_simple_boolean	= $items[0];
		$this->gallery_id				= $items[1];
		$this->srch_date_st				= $items[2];
		$this->srch_date_en				= $items[3];
		$this->date_srch				= $items[4];
		$this->srch_date_is_dd			= $items[5];
		$this->srch_date_is_mm			= $items[6];
		$this->srch_date_is_yy			= $items[7];
		$this->srch_date_st_dd			= $items[8];
		$this->srch_date_st_mm			= $items[9];
		$this->srch_date_st_yy			= $items[10];
		$this->srch_date_en_dd			= $items[11];
		$this->srch_date_en_mm			= $items[12];
		$this->srch_date_en_yy			= $items[13];
		$this->model_release			= $items[14];
		$this->property_release			= $items[15];
		$this->results_order			= $items[16];
		$this->image_colr_enable		= $items[17];
		$this->image_colr_r				= $items[18];
		$this->image_colr_g				= $items[19];
		$this->image_colr_b				= $items[20];
		$this->image_orientation		= $items[21];
		$this->rights_managed			= $items[22];
		$this->search_directory			= $items[23];
		$this->search_within			= $items[24];
		
	}
	
	/*
	*
	*	
	*
	*/
	function getSearchTerms() {
	
		global $cfg, $ses, $objEnvData, $smarty;
		
		if ($this->multi_image_sid != "" && $objEnvData->fetchGlobal('new') != "1") {
			
			// We don't want to log searches run from a cookie
			$this->log_search = (bool)false;
			
			// Load search terms from cookie data
			$this->getSearchInDB($this->multi_image_sid, null, $ses['psg_userid']);
			
		} else {
			
			// We need to log this search because it's new
			$this->log_search = (bool)true;
			
			if ($objEnvData->fetchGlobal('kwd') == 1) { // This search comes from linked keywords
				
				if (is_array($objEnvData->fetchGlobal('keywords_array'))) {
				
					$keywords_data = urldecode(implode(" AND ",$objEnvData->fetchGlobal('keywords_array')));
				
				} else {
					
					$keywords_data = $objEnvData->fetchGlobal('keywords');
				
				}
				
				$this->keywords_simple_boolean 	= stripslashes($keywords_data);
				$this->gallery_id				= 0;
				$this->date_srch				= 10;
				
			} else { // This search comes from a search form
				
			 	$this->keywords_simple_boolean	= $this->preParseBooleanKeywords();
				$this->gallery_id				= $objEnvData->fetchGlobal('gallery_id');
				$this->date_srch				= $objEnvData->fetchGlobal('date_srch');
			
			}
			
			// If no keyword is shown, then use the default wildcard operator
			//if ($this->keywords_simple_boolean == "") {
			//	$this->keywords_simple_boolean = "*";
			//}
			
			// Get other search parameters
			$this->srch_date_st			= $objEnvData->fetchGlobal('srch_date_st');
			$this->srch_date_en			= $objEnvData->fetchGlobal('srch_date_en');
			$this->srch_date_is_dd		= str_pad($objEnvData->fetchGlobal('date_is_Day'),2,"00",STR_PAD_LEFT);
			$this->srch_date_is_mm		= str_pad($objEnvData->fetchGlobal('date_is_Month'),2,"00",STR_PAD_LEFT);
			$this->srch_date_is_yy		= $objEnvData->fetchGlobal('date_is_Year');
			$this->srch_date_st_dd		= str_pad($objEnvData->fetchGlobal('date_st_Day'),2,"00",STR_PAD_LEFT);
			$this->srch_date_st_mm		= str_pad($objEnvData->fetchGlobal('date_st_Month'),2,"00",STR_PAD_LEFT);
			$this->srch_date_st_yy		= $objEnvData->fetchGlobal('date_st_Year');
			$this->srch_date_en_dd		= str_pad($objEnvData->fetchGlobal('date_en_Day'),2,"00",STR_PAD_LEFT);
			$this->srch_date_en_mm		= str_pad($objEnvData->fetchGlobal('date_en_Month'),2,"00",STR_PAD_LEFT);
			$this->srch_date_en_yy		= $objEnvData->fetchGlobal('date_en_Year');
			$this->model_release		= $objEnvData->fetchGlobal('model_release');
			$this->property_release		= $objEnvData->fetchGlobal('property_release');
			$this->rights_managed		= $objEnvData->fetchGlobal('rights_managed');
			$this->image_colr_enable	= $objEnvData->fetchGlobal('image_colr_enable');
			$this->image_colr_r			= $objEnvData->fetchGlobal('image_colr_r');
			$this->image_colr_g			= $objEnvData->fetchGlobal('image_colr_g');
			$this->image_colr_b			= $objEnvData->fetchGlobal('image_colr_b');
			$this->image_orientation	= $objEnvData->fetchGlobal('image_orientation');
			$this->search_directory		= $objEnvData->fetchGlobal('search_directory');
			$this->search_userid		= $objEnvData->fetchGlobal('search_userid');
			
			if ($objEnvData->fetchGlobal('results_order') == "") { $this->results_order = $cfg['set']['search_default_order']; } else { $this->results_order = $objEnvData->fetchGlobal('results_order'); }
		
		}
		
	}
	
	/*
	*	Tidies up input keywords if user hasn't added any boolean operators
	*	This makes the search engine more user friendly
	*/
	function preParseBooleanKeywords () {
		
		global $objEnvData;
		
		// Get the search keywords
		$input			= stripslashes($objEnvData->fetchGlobal('keywords_simple_boolean'));
		
		// Punctuation characters to strip out of the keywords
		$punctuation	= "/[\\,\\+\\;\\~\\!\\$\\%\\^\\&\\@\\#\\<\\>\\?\\=]/";
		
		// Clean up the input data
		$input			= preg_replace($punctuation,"",$input);
		
		if (eregi(" and ",$input) || eregi(" or ",$input) || eregi(" not ",$input) || eregi("\(",$input) || eregi("\)",$input) || $input == "") {
		
			$output = $input;
		
		} else {
		
			$str = $input;
			$t = 0;
			while($str != "")
			{
				$str = trim($str);
	
				// Take the first character in the remaining query
				$c = substr($str, 0, 1);
	
				// If a quote character, match anything up to the next
				// matching quote
				if($c == "\"")
				{
					preg_match("/^$c([^$c]*)$c/", $str, $matches);
					$val = preg_quote($matches[1]);
					$str = preg_replace("/^$c$val$c/", "", $str);
					$tokens[] = $matches[1];
				}
	
				// Ptherwise match any non-special characters to find
				// the next word or word operator
				else
				{
					preg_match("/^([^\s\(\)\"]+)/", $str, $matches);
					$val = preg_quote($matches[1]);
					$str = preg_replace("/^$val/", "", $str);
					$tokens[] = $matches[1];
	
				}
				$t++;
	
				// If the query is too long or something else has gone
				// awry, break out of the loop
				if($t > 100) { return false; }
				
			}
			
			if (is_array($tokens)) {
			
				foreach ($tokens as $key => $token) {
					
					if (ereg(" ", $token)) {
						$parsed[] = "\"".$token."\"";
					} else {
						$parsed[] = $token;
					}
				}
				
				$output = implode(" and ",  $parsed);
			
			}
			
		}
		
		return $output;
		
	}
	
	/*
	*	Return array of modulus RGB colors
	*/
	function getImageModRGB () {
		
		if ($this->image_colr_enable) {
		
			return array($this->image_colr_r,$this->image_colr_g,$this->image_colr_b);
		
		} else {
		
			return array(8,8,8);
		
		}
		
	}
	
	/*
	*	Return array of hexadecimal RGB colors
	*/
	function getImageHexRGB () {
		
		$hex_r 		= strtoupper(str_pad(dechex($this->image_colr_r * 16),2,"00",STR_PAD_LEFT));
		$hex_g 		= strtoupper(str_pad(dechex($this->image_colr_g * 16),2,"00",STR_PAD_LEFT));
		$hex_b 		= strtoupper(str_pad(dechex($this->image_colr_b * 16),2,"00",STR_PAD_LEFT));
		
		$hex_str 	= $hex_r . $hex_g . $hex_b;
		
		if ($this->image_colr_enable) {
		
			return array($hex_r,$hex_g,$hex_b,$hex_str);
		
		} else {
		
			return array(80,80,80,"808080");
		
		}
	
	}
	
	/*
	*	Return array of 8 bit decimal RGB colors
	*/
	function getImageDecRGB () {
	
		$dec_r = $this->image_colr_r * 16;
		$dec_g = $this->image_colr_g * 16;
		$dec_b = $this->image_colr_b * 16;
		
		$dec_str 	= $dec_r . ", " . $dec_g . ", " . $dec_b;
		
		if ($this->image_colr_enable) {
		
			return array($dec_r,$dec_g,$dec_b,$dec_str);
		
		} else {
		
			return array(128,128,128,"128, 128, 128");
		
		}
	
	}
	
	/*
	*	Return the search directory
	*/
	function getSearchDirectory () {
	
		return $this->search_directory;
	
	}
	
	/*
	*	Return the search ID
	*/
	function getSearchId () {
		
		if (is_numeric($this->multi_image_sid)) {
			return $this->multi_image_sid;
		} else {
			return false;
		}
	}
	
	/*
	*	Get detailed information about an image and it's neighbours in this search
	*
	*	@return array $this->search_image_data an array of info about an image and its neighbours in a search results list
	*/
	function getSearchImageData() {
	
		return $this->search_image_data;
	
	}

	/*
	*	Get data for the images in this search results list
	*/
	function getSearchMultiImageData() {
		
		if ($this->search_terms_error) { return; }
		
		global $cfg, $ses;
			
		$sql = "SELECT ".PIX_TABLE_IMGS.".*  FROM ".PIX_TABLE_IMGS."
		
				LEFT JOIN ".PIX_TABLE_GORD." ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_GORD.".image_id 
				
				LEFT JOIN ".PIX_TABLE_GALL." ON ".PIX_TABLE_GORD.".gallery_id = ".PIX_TABLE_GALL.".gallery_id 
				
				LEFT JOIN ".PIX_TABLE_SRCS." ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_SRCS.".image_id 
				
				WHERE ".PIX_TABLE_SRCS.".search_id = ".$this->multi_image_sid."
				
				GROUP BY ".PIX_TABLE_IMGS.".image_id
				
				ORDER BY ".PIX_TABLE_SRCS.".id ASC";
		
		// Get the SQL search query and append the page limiting SQL
		//$search_query = $this->generateSearchQuery() . " " . $this->getMultiImagePageLimitSQL();
		$search_query = $sql . " " . $this->getMultiImagePageLimitSQL();
		
		// Execute the search query
		$search_images = $this->_dbl->sqlSelectRows($search_query);
		
		// Runs the method in the parent class to process a list of the images in this search results list
		return $this->processMultiImageData($search_images);
	
	}
		
	/*
	*
	*	
	*
	*/
	function processNeighbourURLs($start, $number) {
		
		global $cfg;
		
		for ($i=($start + 1); $i <= ($start + $number + 1); $i++) {
	
			$data[]	= $cfg['sys']['base_url'] . FILE_PUB_SEARCH . "?sid=".$this->multi_image_sid."&amp;img=" . $i;
				
		}
		
		return $data;
		
	}
	
	/*
	*	Outputs an array of information about a single image in a search results list and limited detail of those near it
	*	
	*	@return array $images_data information about an image in a search results list and the five images nearest to it
	*/
	function processSearchImageDetail () {
	
		global $cfg, $ses, $smarty;
		
		$results_view		= $this->getBrowsingLimitData ('results_view');
		$results_lim_sta	= $this->getBrowsingLimitData ('results_lim_sta');
		$results_lim_end	= $this->getBrowsingLimitData ('results_lim_end');
		$image_key			= $this->getBrowsingLimitData ('image_key');
		
		$sql = "SELECT * FROM ".PIX_TABLE_IMGS."
		
				LEFT JOIN ".PIX_TABLE_GORD." ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_GORD.".image_id 
				
				LEFT JOIN ".PIX_TABLE_GALL." ON ".PIX_TABLE_GORD.".gallery_id = ".PIX_TABLE_GALL.".gallery_id 
				
				LEFT JOIN ".PIX_TABLE_SRCS." ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_SRCS.".image_id 
				
				WHERE ".PIX_TABLE_SRCS.".search_id = ".$this->multi_image_sid."
				
				GROUP BY ".PIX_TABLE_IMGS.".image_id
				
				ORDER BY ".PIX_TABLE_SRCS.".id ASC
				
				LIMIT $results_lim_sta, $results_lim_end";

		// Retrieve gallery information from database
		$image_data 	= $this->_dbl->sqlSelectRows($sql);
		
		// Load the PixariaImage class 
		require_once ('class.PixariaImage.php');
		
		// Create the image object
		$objPixariaImage = new PixariaImage($image_data[$image_key]['image_id']);
		
		// Update the view counter for this image
		$this->updateImageViewCounter($image_data[$image_key]['image_id']);
		
		// Add the image_id to the array of data
		$images_data['image_id']			= $image_data[$image_key]['image_id'];
		
		// Add detailed information about this image to the array
		$images_data['image_detail']		= $objPixariaImage->getImageDataComplete();
		
		// Add basic information about the neighbouring images to the array
		$images_data['image_neighbours']	= $this->processMultiImageData($image_data);
		
		$images_data['image_count']			= count($images_data['image_neighbours']);
		
		$images_data['image_urls']			= $this->processNeighbourURLs($results_lim_sta,$results_lim_end);
			
		$images_data['image_numbers']		= $this->processNeighbourImageNumbers($results_lim_sta,$results_lim_end);
			
		if ($this->multi_image_image == 1) {
			$images_data['image_prev_url'] = $cfg['sys']['base_url'] . FILE_PUB_SEARCH . "?sid=".$this->multi_image_sid;
		} else {
			$images_data['image_prev_url'] = $cfg['sys']['base_url'] . FILE_PUB_SEARCH . "?sid=".$this->multi_image_sid."&amp;img=" . ($this->multi_image_image - 1);
		}
		
		if ($this->multi_image_image == $this->multi_image_count) {
			$images_data['image_next_url'] = $cfg['sys']['base_url'] . FILE_PUB_SEARCH . "?sid=".$this->multi_image_sid;
		} else {
			$images_data['image_next_url'] = $cfg['sys']['base_url'] . FILE_PUB_SEARCH . "?sid=".$this->multi_image_sid."&amp;img=" . ($this->multi_image_image + 1);
		}
				
		// Return the image data as an array
		return $images_data;
		
	}
		
	
	/*
	*
	*	This function searches the database for images and returns an array of image data
	*	The function takes two optional arguments to limit the number of results returned
	*
	*	$results_lim_sta = Point at which to start saving the results
	*	$results_lim_num = Number of results to return in this query
	*
	*/
	function generateSearchQuery ($results_lim_sta="",$results_lim_num="") {
		
		global $cfg, $ses, $objEnvData;
		
		// This is a simple search for all images in a directory
		if ($this->search_directory != "") {
			
			$sql = "SELECT * FROM ". PIX_TABLE_IMGS ." WHERE image_path = '$this->search_directory'";
			
			return $sql;
		
		}
		
		if ($results_lim_sta || $results_lim_num) {
			$limit = "LIMIT $results_lim_sta,$results_lim_num";
		} else {
			$limit = "LIMIT 0,".$cfg['set']['search_max_limit'];
		}
		
		// Create an array of punctuation to remove from the keyword fields
		$punctuation = array(",","\"","\\","/",";","\$","%","&","(",")","{","}","[","]","+","*","<",">");
		
		$search_within_sql		= $this->getSearchWithinSQL();
		$gallery_sql 			= $this->getGallerySQL();
		$orientation_sql		= $this->getImageOrientationSQL();
		$color_search_sql		= $this->getColorSearchSQL();
		$srch_date_query		= $this->getSearchDateSQL();
		$order_sql				= $this->getResultsOrderSQL();
		$userid_sql				= $this->getUserImagesSQL();
		$release_sql			= $this->getReleaseSQL();
		$rm_sql					= $this->getRightsManagementSQL();
		$gallery_protection_sql	= $this->getGalleryProtectionSQL();
		
		if ($this->keywords_simple_boolean != "") { // Simple boolean searching is turned on
			
			require_once ("class.BooleanToSQL.php");
		
			$objBooleanToSQL 		= new BooleanToSQL($this->keywords_simple_boolean);
			
			$varnames				= array(image_keywords, image_caption, image_title, image_filename);
			
			if ($cfg['set']['search_opt_wildcard_keywords']) {
			
				$whereclause = $objBooleanToSQL->getSQL($varnames, "or", "regexp", "[[:<:]]", "[[:>:]]", true);
			
				$whereclause .= " OR " . $objBooleanToSQL->getSQL($varnames, "or", "regexp", "[[:<:]]", "[[:>:]]", false);
				
			} else {
			
				$whereclause = $objBooleanToSQL->getSQL($varnames, "or", "regexp", "[[:<:]]", "[[:>:]]", false);
			
			}
			
			$this->search_tokens	= $objBooleanToSQL->getTokenValues();
			
			// Update the search log database with these keywords
			$this->logSearchTerms($this->keywords_simple_boolean);
			
		 	$sql = "SELECT 	".PIX_TABLE_IMGS.".image_id,
							
							".PIX_TABLE_IMGS.".image_date,
							
							".PIX_TABLE_IMGS.".image_title,
							
							".PIX_TABLE_IMGS.".image_filename,
							
							".PIX_TABLE_GORD.".gallery_id as gallery_id
						
					FROM ".PIX_TABLE_IMGS."
						
					LEFT JOIN ".PIX_TABLE_GORD." ON ".PIX_TABLE_GORD.".image_id = ".PIX_TABLE_IMGS.".image_id
						
					LEFT JOIN ".PIX_TABLE_IMVI." ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_IMVI.".image_id
					
					LEFT JOIN ".PIX_TABLE_GALL." ON ".PIX_TABLE_GORD.".gallery_id = ".PIX_TABLE_GALL.".gallery_id
		
					LEFT JOIN ".PIX_TABLE_GVIE." ON ".PIX_TABLE_GORD.".gallery_id = ".PIX_TABLE_GVIE.".gallery_id
				
					LEFT JOIN ".PIX_TABLE_SRCS." ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_SRCS.".image_id
				
					$gallery_join_sql
					
					WHERE ($whereclause)
					
					$search_within_sql
					
					$gallery_sql
					
					$srch_date_query
					
					$color_search_sql
						
					$orientation_sql
					
					$release_sql
					
					$rm_sql
					
					".$cfg['set']['image_access_sql']."
					
					".$cfg['set']['image_visibility_sql']."
					
					$gallery_protection_sql
					
					$userid_sql
					
					GROUP BY ".PIX_TABLE_IMGS.".image_id
					
					$order_sql
					
					$limit";
			
		} elseif ($keywords_simple_boolean == "" && !$this->search_terms_error) {  // NO KEYWORDS
			
			// Get the SQL
			$sql = "SELECT 	".PIX_TABLE_IMGS.".image_id,
							
							".PIX_TABLE_IMGS.".image_date,
							
							".PIX_TABLE_IMGS.".image_title,
							
							".PIX_TABLE_IMGS.".image_filename,
							
							".PIX_TABLE_GORD.".gallery_id as gallery_id
					
					FROM ".PIX_TABLE_IMGS."
					
					LEFT JOIN ".PIX_TABLE_GORD." ON ".PIX_TABLE_GORD.".image_id = ".PIX_TABLE_IMGS.".image_id
					
					LEFT JOIN ".PIX_TABLE_IMVI." ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_IMVI.".image_id
					
					LEFT JOIN ".PIX_TABLE_GALL." ON ".PIX_TABLE_GORD.".gallery_id = ".PIX_TABLE_GALL.".gallery_id
		
					LEFT JOIN ".PIX_TABLE_GVIE." ON ".PIX_TABLE_GORD.".gallery_id = ".PIX_TABLE_GVIE.".gallery_id

					LEFT JOIN ".PIX_TABLE_SRCS." ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_SRCS.".image_id
				
					$gallery_join_sql
					
					WHERE ".PIX_TABLE_IMGS.".image_id
					
					$search_within_sql
					
					$gallery_sql
					
					$srch_date_query
					
					$color_search_sql
					
					$orientation_sql
					
					$release_sql
					
					$rm_sql
					
					".$cfg['set']['image_access_sql']."
					
					".$cfg['set']['image_visibility_sql']."
					
					$gallery_protection_sql
					
					$userid_sql
					
					GROUP BY ".PIX_TABLE_IMGS.".image_id
					
					$order_sql
					
					$limit";
				
		}
		
		// Pass back the array of results
		return $sql;
		
	}

	/*
	*
	*	This function saves the keywords entered in a search into a table in the database
	*	logging can be turned on and off from the general settings control panel
	*
	*/
	function logSearchTerms() {
	
		global $cfg, $ses;
		
		if (!$this->search_logged) {
		
			$this->search_logged = (bool)true;
			
			if ($cfg['set']['func_search_log'] == TRUE) {
				
				// For each search keyword, add an entry to the search keyword log
				if (is_array($this->search_tokens)) {
					
					foreach ($this->search_tokens as $key => $value) {
						
						$this->_dbl->sqlQuery("INSERT INTO ".PIX_TABLE_SLOG." VALUES ('0','".mysql_real_escape_string($value)."','ANY');");
					
					}
				}
				
			}
		
		}
		
		return true;
		
	}
	
	/*
	*
	*
	*
	*/
	function getSearchWithinSQL () {
	
		if (is_numeric($this->search_within)) {
		
			$sql = " AND ".PIX_TABLE_SRCS.".search_id = '".$this->search_within."' ";
			
			return $sql;
			
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function getGalleryProtectionSQL () {
		
		global $cfg, $objEnvData;
		
		if ($objEnvData->fetchCookie('pixaria_gpwd')) {
			
			$md5_password = $objEnvData->fetchCookie('pixaria_gpwd');
			
			$gpwd_sql = " OR MD5(CONCAT(".PIX_TABLE_GALL.".gallery_id,".PIX_TABLE_GALL.".gallery_password)) = '$md5_password' ";
		
		}
		
		$sql = "AND (".PIX_TABLE_GALL.".gallery_id IS NULL
		
				OR ".PIX_TABLE_GALL.".gallery_password = ''
				
				$gpwd_sql
				
				".$cfg['set']['gallery_visibility_sql']."
				
				".$cfg['set']['gallery_access_sql'].")";
				
		return $sql;
	
	}
	
	/*
	*
	*	
	*
	*/
	function getUserImagesSQL () {
		
		if ($this->search_userid != "") {
			$sql  = "AND image_userid = $this->search_userid";
			return $sql;
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function getRightsManagementSQL () {
		
		global $cfg;
		
		if ($this->rights_managed == "2") {
		
			$rm_sql = " AND ".PIX_TABLE_IMGS.".image_rights_type = '11' ";
		
		} elseif ($this->rights_managed == "3") {
		
			$rm_sql = " AND ".PIX_TABLE_IMGS.".image_rights_type = '10' ";
		
		}
		
		return $rm_sql;		
		
	}
	
	/*
	*
	*	
	*
	*/
	function getReleaseSQL () {
		
		if ($this->model_release == "on" || $this->model_release) {
		
			$release_sql .= " AND ".PIX_TABLE_IMGS.".image_model_release = '1' ";
		
		}
	
		if ($this->property_release == "on" || $this->property_release) {
		
			$release_sql .= " AND ".PIX_TABLE_IMGS.".image_property_release = '1' ";
		
		}
		
		return $release_sql;
		
	}
	
	/*
	*  Generate a SQL query string containing a search for image by colour
	*
	*	The the required variable for this function is the colour which should be
	*	a string in the form r,g,b where r, g and b are RGB colour indices divided by 16
	* 	and rounded to the nearest integer
	*/
	function getColorSearchSQL() {
	
		global $cfg;
		
		if ($this->image_colr_enable) {
		
			$colr_r			= $this->image_colr_r;
			$colr_g			= $this->image_colr_g;
			$colr_b			= $this->image_colr_b;
			
			## To get a more accurate search by colour, we will search within 12.5% of the RGB hue value for each primary colour.
			$var_colr		= 2;
			
			## Check if image is greyscale or colour and use correct search query
			if (($colr_r == $colr_g) && ($colr_r == $colr_b)) {
			
				## Search using the greyscale search query
				$sql_string		.= "(".PIX_TABLE_IMGS.".image_colr_g = '$colr_r') AND (".PIX_TABLE_IMGS.".image_colr_b = '$colr_r') AND (".PIX_TABLE_IMGS.".image_colr_r = '$colr_r') AND (".PIX_TABLE_IMGS.".image_colr_r <= '".($colr_r + 2)."' AND ".PIX_TABLE_IMGS.".image_colr_r >= '".($colr_r - 2)."')";
			
			} else {
			
				## Search using the colour search query
				$sql_string		.= "(".PIX_TABLE_IMGS.".image_colr_r <= '".($colr_r + $var_colr)."' AND ".PIX_TABLE_IMGS.".image_colr_r >= '".($colr_r - $var_colr)."') AND ";
				$sql_string		.= "(".PIX_TABLE_IMGS.".image_colr_g <= '".($colr_g + $var_colr)."' AND ".PIX_TABLE_IMGS.".image_colr_g >= '".($colr_g - $var_colr)."') AND ";
				$sql_string		.= "(".PIX_TABLE_IMGS.".image_colr_b <= '".($colr_b + $var_colr)."' AND ".PIX_TABLE_IMGS.".image_colr_b >= '".($colr_b - $var_colr)."')";
				
			}
			
			return " AND " . $sql_string;
		
		} else {
		
				return "";
		
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function getSearchDateSQL () {
	
		/*
		
		Date search types:
		
		$date_srch = 10 - Don't search by date
		$date_srch = 11 - Search for a single date
		$date_srch = 12 - Search a range of dates
		
		*/
		
		// If the user is entering a date search, build some SQL to insert into the query
		if ($this->srch_date_st == "" && $this->srch_date_en == "") {
			
			if ($this->date_srch == 10) {
			
				$srch_date_query = "";
			
			} elseif ($this->date_srch == 11) {
			
				$srch_date_query	= " AND DATE_FORMAT(".PIX_TABLE_IMGS.".image_date, '%Y-%m-%d' ) = '$this->srch_date_is_yy-$this->srch_date_is_mm-$this->srch_date_is_dd' ";
			
			} elseif ($this->date_srch == 12) {
	
				$srch_date_query	= " AND DATE_FORMAT(".PIX_TABLE_IMGS.".image_date, '%Y-%m-%d' ) BETWEEN '$this->srch_date_st_yy-$this->srch_date_st_mm-$this->srch_date_st_dd' AND '$this->srch_date_en_yy-$this->srch_date_en_mm-$this->srch_date_en_dd' ";
			
			}
		
		}
		
		return $srch_date_query;
		
	}
	
	/*
	*
	*	
	*
	*/
	function getGallerySQL () {
		
		if (!$this->gallery_id) {
			return;
		}
		
		if ($this->gallery_id != "0") {
		
			$gallery_sql = " AND ( ".PIX_TABLE_GORD.".gallery_id = '$this->gallery_id'";
			
			// Get an array of sub galleries of the one being searched for
			$gallery_array = galleryListSubgalleries($this->gallery_id);
			
			if (is_array($gallery_array)) {
				
				foreach($gallery_array as $key => $value) {
				
					$gallery_sql .= " OR ".PIX_TABLE_GORD.".gallery_id = '$value'";
				
				}
				
			}
			
			$gallery_sql .= " ) ";
		
		}
		
		return $gallery_sql;
		
	}
	
	/*
	*
	*	
	*
	*/
	function getImageOrientationSQL () {
	
		// Search by image orientation
		switch ($this->image_orientation) {
			
			case "ls":
			
				$orientation_sql = " AND (".PIX_TABLE_IMGS.".image_width > ".PIX_TABLE_IMGS.".image_height) ";
			
			break;
			
			case "po":
			
				$orientation_sql = " AND (".PIX_TABLE_IMGS.".image_width < ".PIX_TABLE_IMGS.".image_height) ";
			
			break;
			
			case "sq":
			
				$orientation_sql = " AND (".PIX_TABLE_IMGS.".image_width = ".PIX_TABLE_IMGS.".image_height) ";
			
			break;
			
			case "pn":
			
				$orientation_sql = " AND (".PIX_TABLE_IMGS.".image_width >= 2* ".PIX_TABLE_IMGS.".image_height) ";
			
			break;
			
			default:
			
				$orientation_sql = "";
				
			break;
		
		}
		
		return $orientation_sql;
		
	}
	
	/*
	*
	*	
	*
	*/
	function GetResultsOrderSQL () {
	
		/*
		
		Results order types
		
		$results_order = 1 - Most recent first
		$results_order = 2 - Most relevant first
		$results_order = 3 - File name A-Z
		$results_order = 4 - File name Z-A
		$results_order = 5 - Title A-Z
		$results_order = 6 - Title Z-A
		
		*/
		
		switch ($this->results_order) {
		
			case 1:
			
				$order_sql = "ORDER BY ".PIX_TABLE_IMGS.".image_date DESC";
			
			break;
		
			case 2:
			
				$order_sql = "";
			
			break;
		
			case 3:
			
				$order_sql = "ORDER BY ".PIX_TABLE_IMGS.".image_filename DESC";
			
			break;
		
			case 4:
			
				$order_sql = "ORDER BY ".PIX_TABLE_IMGS.".image_filename ASC";
			
			break;
		
			case 5:
			
				$order_sql = "ORDER BY ".PIX_TABLE_IMGS.".image_title ASC";
			
			break;
		
			case 6:
			
				$order_sql = "ORDER BY ".PIX_TABLE_IMGS.".image_title DESC";
			
			break;
		
			default:
			
				$order_sql = "ORDER BY ".PIX_TABLE_IMGS.".image_date DESC";
			
			break;
		
		}
		
		return $order_sql;
		
	}
	
	/*
	*
	*	
	*
	*/
	function countSearchResults ($array) {
		
		if (is_array($array)) {
		
			while (list($key, $value) = each($array)) { 
			
				if ($value) { 
				
					$count++; 
				
				} 
			} 
			
			return $count;
		
		} else {
		
			return 0;
		
		}
	}
	
}


?>