<?php

/*
*
*	This class processes an XML file into an array
*
*/
class parseXmlData {
	
	var $XML_LIST_ELEMENTS;
	var $xmlarray;
	
	/*
	*
	*/
	function parseXmlData ($xmldata,$list_elements=array()) {
		
		$this->XML_LIST_ELEMENTS = $list_elements;
		
		// Create a new XML parser
		$parser = xml_parser_create();
		xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,0);
		xml_parser_set_option($parser,XML_OPTION_SKIP_WHITE,1);
		xml_parse_into_struct($parser,$xmldata,$values,$tags);
		xml_parser_free($parser);
		
		// Store the XML data
		$hash_stack = array();
		
		// This is the target for the data
		$ret = array();
		
		foreach ($values as $key => $val) {
		  
		  switch ($val['type']) {
			  case 'open':
				 array_push($hash_stack, $val['tag']);
				if (isset($val['attributes']))
					$ret = $this->composeXMLArray($ret, $hash_stack, $val['attributes']);
				else
					$ret = $this->composeXMLArray($ret, $hash_stack);
				break;
	
			  case 'close':
				 array_pop($hash_stack);
				break;
	
			  case 'complete':
				 array_push($hash_stack, $val['tag']);
				 $ret = $this->composeXMLArray($ret, $hash_stack, $val['value']);
				 array_pop($hash_stack);
				
				 // handle attributes
				 if (isset($val['attributes']))
				{
					while(list($a_k,$a_v) = each($val['attributes']))
					{
					   $hash_stack[] = $val['tag']."_attribute_".$a_k;
					   $ret = $this->composeXMLArray($ret, $hash_stack, $a_v);
					   array_pop($hash_stack);
					}
				}
				
				
				break;
		  }
		}
		
		$this->xmlarray	= $ret;
	}
	
	/*
	*	Converts a parsed XML data source into an multidimensional array
	*/
	function composeXMLArray ($array, $elements, $value=array())	{
	  
	   // get current element
	   $element = array_shift($elements);
	  
	   // does the current element refer to a list
	   if (in_array($element,$this->XML_LIST_ELEMENTS))
	  {
		  // more elements?
		  if(sizeof($elements) > 0)
		  {
			 $array[$element][sizeof($array[$element])-1] = $this->composeXMLArray($array[$element][sizeof($array[$element])-1], $elements, $value);
		  }
		  else // if (is_array($value))
		  {
			 $array[$element][sizeof($array[$element])] = $value;
		  }
	  }
	  else
	  {
		  // more elements?
		  if(sizeof($elements) > 0)
		  {
			 $array[$element] = $this->composeXMLArray($array[$element], $elements, $value);
		  }
		  else
		  {
			 $array[$element] = $value;
		  }
	  }
		
		return $array;
	}
		
}

?>