<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

class PixariaCountry {

	// Private variables
	var $_dbl;
	var $_update_flag = false;
	
	// Public variables
	var $sIsoCode;
	var $sPrintableName;
	var $sIsoCodes;
	var $sPrintableNames;
	
	// Error log for malformed data
	var $error 		= false;
	var $error_log 	= array();
	
	function PixariaCountry ($country_id = "") {
	
		// Localise globals
		global $ses, $cfg;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		if ($country_id != "") {
		
			$sql = "SELECT iso, printable_name FROM " . PIX_TABLE_CTRY . " WHERE iso = '$country_id'";
			
			$result = $this->_dbl->sqlSelectRow($sql);
			
			if (is_array($result)) {
			
				$this->sIsoCode			= $result['iso'];
				$this->sPrintableName	= $result['printable_name'];
			
			}
			
		} else {
		
			$sql = "SELECT iso, printable_name FROM " . PIX_TABLE_CTRY . " ORDER BY printable_name ASC";
			
			$result = $this->_dbl->sqlSelectRows($sql);
			
			if (is_array($result)) {
			
				foreach ($result as $key => $value) {
				
					$this->sIsoCodes[]			= $value['iso'];
					$this->sPrintableNames[]	= $value['printable_name'];
				
				}
			
			}
			
		}
		
	}
	
	/*
	*	Get the ISO country codes for all countries as an array
	*/
	function getIsoCodes () {
	
		return $this->sIsoCodes;
	
	}
	
	/*
	*	Get the printable name for all countries as an array
	*/
	function getPrintableNames () {
	
		return $this->sPrintableNames;
	
	}
	
	/*
	*	Get the ISO country codes for all countries as an array
	*/
	function getIsoCode () {
	
		return $this->sIsoCode;
	
	}
	
	/*
	*	Get the printable name for all countries as an array
	*/
	function getPrintableName () {
	
		return $this->sPrintableName;
	
	}
	
}

?>