<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

/*
*
*	Class to count time elapsed from a given point defined as TIMER_START_TIME
*	
*	You can use:
*	
*	define ("TIMER_START_TIME", microtime());
*	
*	Or to explicitly start the timer at a given point in a script:
*	
*	ScriptTimer::StartTimer();
*	
*	A constant definition is used so the starting time can be retrieved from anywhere.
*	When you want to retrieve the time difference (from start to finish) use:
*	
*	$total_time = ScriptTimer::GetTime(3);    // The $decimals argument is optional and will default to 2 
*/

class ScriptTimer {

	function ScriptTimer()
	{
		return true;
	}
	
	function StartTimer()
	{
		define ("TIMER_START_TIME", microtime());
		
		return true;
	}
	
	function GetTime ($decimals=2)
	{
		// $decimals will set the number of decimals you want for your milliseconds.
	
		// format start time
		$start_time = explode (" ", TIMER_START_TIME);
		$start_time = $start_time[1] + $start_time[0];
		// get and format end time
		$end_time = explode (" ", microtime());
		$end_time = $end_time[1] + $end_time[0];
		
		return number_format ($end_time - $start_time, $decimals);
	}
	
}


?>