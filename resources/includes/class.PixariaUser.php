<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

class PixariaUser {

	// Private variables
	var $_dbl;
	var $_update_flag = false;
	
	// Public variables
	var $userid;
	var $auth_level;
	var $email_address;
	var $formal_title;
	var $other_title;
	var $first_name;
	var $middle_initial;
	var $family_name;
	var $password;
	var $password_1;
	var $password_2;
	var $password_rem_que;
	var $password_rem_ans;
	var $date_registered;
	var $date_edited;
	var $date_expiration;
	var $account_status;
	var $telephone;
	var $mobile;
	var $fax;
	var $addr1;
	var $addr2;
	var $addr3;
	var $city;
	var $region;
	var $country;
	var $postal_code;
	var $storage_space;
	var $renewal_period;
	var $active_image_box;
	var $other_business_type;
	var $other_business_position;
	var $other_image_interest;
	var $other_frequency;
	var $other_circulation;
	var $other_territories;
	var $other_website;
	var $other_message;
	var $other_company_name;
	var	$is_administrator;
	var	$is_editor;
	var	$is_photographer;
	var	$groups;
	var	$profile_valid = false;
	var	$profile_errors;
	
	
	/*
	*	
	*	This is the class constructor for the PixariaUser class
	*	
	*	PixariaUser -- Initialise the database connection
	*	
	*	class PixariaUser([int userid])
	*
	*/
	function PixariaUser($userid = "") {
	
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		// If there is a userid supplied when the class is called, call the loadUser function
		if (is_numeric($userid)) {
			
			// Load a user's details by userid
			$this->loadUser($userid);
		
		}
		
	}
	
	/*
	*	
	*	This function loads a user's details by referencing their user id
	*	
	*	loadUser -- Load data for a user
	*	
	*	function loadUser(int userid)
	*
	*/
	function loadUser($userid) {
	
		if (is_numeric($userid)) {
		
			$result = $this->_dbl->sqlSelectRow("SELECT * FROM ". PIX_TABLE_USER ." WHERE userid = '$userid'");
		
		} else {
		
			return;
		
		}
		
		
		
		if (is_array($result)) {
			
			$this->userid					= $result['userid'];
			$this->auth_level				= $result['auth_level'];
			$this->email_address			= $result['email_address'];
			$this->formal_title				= $result['formal_title'];
			$this->other_title				= $result['other_title'];
			$this->first_name				= $result['first_name'];
			$this->middle_initial			= $result['middle_initial'];
			$this->family_name				= $result['family_name'];
			$this->password					= $result['password'];
			$this->password_rem_que			= $result['password_rem_que'];
			$this->password_rem_ans			= $result['password_rem_ans'];
			$this->date_registered			= $result['date_registered'];
			$this->date_edited				= $result['date_edited'];
			$this->date_expiration			= $result['date_expiration'];
			$this->account_status			= $result['account_status'];
			$this->telephone				= $result['telephone'];
			$this->mobile					= $result['mobile'];
			$this->fax						= $result['fax'];
			$this->addr1					= $result['addr1'];
			$this->addr2					= $result['addr2'];
			$this->addr3					= $result['addr3'];
			$this->image_id					= $result['image_id'];
			$this->city						= $result['city'];
			$this->region					= $result['region'];
			$this->country					= $result['country'];
			$this->postal_code				= $result['postal_code'];
			$this->storage_space			= $result['storage_space'];
			$this->renewal_period			= $result['renewal_period'];
			$this->active_image_box			= $result['active_image_box'];
			$this->other_business_type		= $result['other_business_type'];
			$this->other_business_position	= $result['other_business_position'];
			$this->other_image_interest		= $result['other_image_interest'];
			$this->other_frequency			= $result['other_frequency'];
			$this->other_circulation		= $result['other_circulation'];
			$this->other_territories		= $result['other_territories'];
			$this->other_website			= $result['other_website'];
			$this->other_message			= $result['other_message'];
			$this->other_company_name		= $result['other_company_name'];
				
		} else {
		
			return;
		
		}
		
	}
	
	function getClassName() {
		return 'PixariaUser';
	}
	
	function getUserId () {
		return 	$this->userid;
	}
	
	function getAuthorisationLevel() {
		return 	$this->auth_level;
	}
	
	function getEmail() {
		return 	$this->email_address;
	}
	
	function getSalutation() {
		return 	$this->formal_title;
	}
	
	function getOtherSalutation() {
		return 	$this->other_title;
	}
	
	function getFirstName() {
		return 	$this->first_name;
	}
	
	function getName() {
		return 	$this->first_name . " " . $this->family_name;
	}
	
	function getInitial() {
		return 	$this->middle_initial;
	}
	
	function getFamilyName() {
		return 	$this->family_name;
	}
	
	function getPassword() {
		return 	$this->password;
	}
	
	function getReminderQuestion() {
		return 	$this->password_rem_que;
	}
	
	function getReminderAnswer() {
		return 	$this->password_rem_ans;
	}
	
	function getDateRegistered() {
		return 	$this->date_registered;
	}
	
	function getDateEdited() {
		return 	$this->date_edited;
	}
	
	function getDateExpires() {
		return 	$this->date_expiration;
	}
	
	function getAccountStatus() {
		return 	$this->account_status;
	}
	
	function getTelephone() {
		return 	$this->telephone;
	}
	
	function getMobile() {
		return 	$this->mobile;
	}
	
	function getFax() {
		return 	$this->fax;
	}
	
	function getAddress1() {
		return 	$this->addr1;
	}
	
	function getAddress2() {
		return 	$this->addr2;
	}
	
	function getAddress3() {
		return 	$this->addr3;
	}
	
	function getCity() {
		return 	$this->city;
	}
	
	function getRegion() {
		return 	$this->region;
	}
	
	function getCountry() {
		return 	$this->country;
	}
	
	function getCountryName() {
		require_once ('class.PixariaCountry.php');
		$objCountry = new PixariaCountry($this->country);
		return 	$objCountry->getPrintableName();
	}
	
	function getPostCode() {
		return 	$this->postal_code;
	}
	
	function getStorageSpace() {
		return 	$this->storage_space;
	}
	
	function getRenewalPeriod() {
		return 	$this->renewal_period;
	}
	
	function getLightbox() {
		return 	$this->active_image_box;
	}
	
	function getBusinessType() {
		return 	$this->other_business_type;
	}
	
	function getBusinessPosition() {
		return 	$this->other_business_position;
	}
	
	function getInterest() {
		return 	$this->other_image_interest;
	}
	
	function getPubFrequency() {
		return 	$this->other_frequency;
	}
	
	function getPubCirculation() {
		return 	$this->other_circulation;
	}
	
	function getTerritories() {
		return 	$this->other_territories;
	}
	
	function getWebsite() {
		return 	$this->other_website;
	}
	
	function getMessage() {
		return 	$this->other_message;
	}
	
	function getCompanyName() {
		return 	$this->other_company_name;
	}
	
	function getProfileErrors() {
		return 	$this->profile_errors;
	}
	
	function setUserID($userid) {
		$this->userid = $userid;
		$this->_update_flag = (bool)true;
	}
	
	function setAuthorisationLevel($auth_level) {
		$this->auth_level = $auth_level;
		$this->_update_flag = (bool)true;
	}
	
	function setEmail($email_address) {
		$this->email_address = $email_address;
		$this->_update_flag = (bool)true;
	}
	
	function setSalutation($formal_title) {
		$this->formal_title = $formal_title;
		$this->_update_flag = (bool)true;
	}
	
	function setOtherSalutation($other_title) {
		$this->other_title = $other_title;
		$this->_update_flag = (bool)true;
	}
	
	function setFirstName($first_name) {
		$this->first_name = $first_name;
		$this->_update_flag = (bool)true;
	}
	
	function setInitial($middle_initial) {
		$this->middle_initial = $middle_initial;
		$this->_update_flag = (bool)true;
	}
	
	function setFamilyName($family_name) {
		$this->family_name = $family_name;
		$this->_update_flag = (bool)true;
	}
	
	function setPassword1($password_1) {
		$this->password_1 = $password_1;
		$this->_update_flag = (bool)true;
	}
	
	function setPassword2($password_2) {
		$this->password_2 = $password_2;
		$this->_update_flag = (bool)true;
	}
	
	function setReminderQuestion($password_rem_que) {
		$this->password_rem_que = $password_rem_que;
		$this->_update_flag = (bool)true;
	}
	
	function setReminderAnswer($password_rem_ans) {
		$this->password_rem_ans = $password_rem_ans;
		$this->_update_flag = (bool)true;
	}
	
	function setDateExpires($date_expiration) {
		$this->date_expiration = $date_expiration;
		$this->_update_flag = (bool)true;
	}
	
	function setAccountStatus($account_status) {
		if ($account_status == "on" || $account_status) {
			$this->account_status = 1;
			$this->_update_flag = (bool)true;
		} else {
			$this->account_status = 0;
			$this->_update_flag = (bool)true;
		}
	}
	
	function setTelephone($telephone) {
		$this->telephone = $telephone;
		$this->_update_flag = (bool)true;
	}
	
	function setMobile($mobile) {
		$this->mobile = $mobile;
		$this->_update_flag = (bool)true;
	}
	
	function setFax($fax) {
		$this->fax = $fax;
		$this->_update_flag = (bool)true;
	}
	
	function setAddress1($addr1) {
		$this->addr1 = $addr1;
		$this->_update_flag = (bool)true;
	}
	
	function setAddress2($addr2) {
		$this->addr2 = $addr2;
		$this->_update_flag = (bool)true;
	}
	
	function setAddress3($addr3) {
		$this->addr3 = $addr3;
		$this->_update_flag = (bool)true;
	}
	
	function setCity($city) {
		$this->city = $city;
		$this->_update_flag = (bool)true;
	}
	
	function setRegion($region) {
		$this->region = $region;
		$this->_update_flag = (bool)true;
	}
	
	function setCountry($country) {
		$this->country = $country;
		$this->_update_flag = (bool)true;
	}
	
	function setPostCode($postal_code) {
		$this->postal_code = $postal_code;
		$this->_update_flag = (bool)true;
	}
	
	function setStorageSpace($storage_space) {
		$this->storage_space = $storage_space;
		$this->_update_flag = (bool)true;
	}
	
	function setRenewalPeriod($renewal_period) {
		$this->renewal_period = $renewal_period;
		$this->_update_flag = (bool)true;
	}
	
	function setLightbox($active_image_box) {
		$this->active_image_box = $active_image_box;
		$this->_update_flag = (bool)true;
	}
	
	function setBusinessType($other_business_type) {
		$this->other_business_type = $other_business_type;
		$this->_update_flag = (bool)true;
	}
	
	function setPosition($other_business_position) {
		$this->other_business_position = $other_business_position;
		$this->_update_flag = (bool)true;
	}
	
	function setInterest($other_image_interest) {
		$this->other_image_interest = $other_image_interest;
		$this->_update_flag = (bool)true;
	}
	
	function setPubFrequency($other_frequency) {
		$this->other_frequency = $other_frequency;
		$this->_update_flag = (bool)true;
	}
	
	function setPubCirculation($other_circulation) {
		$this->other_circulation = $other_circulation;
		$this->_update_flag = (bool)true;
	}
	
	function setTerritories($other_territories) {
		$this->other_territories = $other_territories;
		$this->_update_flag = (bool)true;
	}
	
	function setWebsite($other_website) {
		$this->other_website = $other_website;
		$this->_update_flag = (bool)true;
	}
	
	function setMessage($other_message) {
		$this->other_message = $other_message;
		$this->_update_flag = (bool)true;
	}
	
	function setCompanyName($other_company_name) {
		$this->other_company_name = $other_company_name;
		$this->_update_flag = (bool)true;
	}
	
	function setGroups ($groups) {
		if (is_array($groups)) {
			$this->groups = $groups;
			$this->_update_flag = (bool)true;
		}
	}
	
	function setIsAdministrator ($is_admin) {
		if ($is_admin || $is_admin == "on") {
			$this->is_administrator = (bool)true;
			$this->_update_flag 	= (bool)true;
		}
	}
	
	function setIsEditor ($is_editor) {
		if ($is_editor || $is_editor == "on") {
			$this->is_editor 	= (bool)true;
			$this->_update_flag = (bool)true;
		}
	}
	
	function setIsPhotographer ($is_photo) {
		if ($is_photo || $is_photo == "on") {
			$this->is_photographer = (bool)true;
			$this->_update_flag 	= (bool)true;
		}
	}
	
	function setIsDelete ($is_delete) {
		if ($is_delete || $is_delete == "on") {
			$this->is_delete 	= (bool)true;
			$this->_update_flag = (bool)true;
		}
	}
	
	function setIsDownload ($is_download) {
		if ($is_download || $is_download == "on") {
			$this->is_download 	= (bool)true;
			$this->_update_flag = (bool)true;
		}
	}
	
	/*
	*
	*	This method creates a temporary user account
	*
	*/
	function createNewUserTemp () {
	
		global $smarty, $cfg;
		
		// Create SQL code for the new user insertion
		$sql = "INSERT INTO " . PIX_TABLE_USER . " VALUES (
				'0',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				NOW(),
				NOW(),
				NOW(),
				'0',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'0',
				'10',
				'0',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				'',
				''
				)";

		$this->_dbl->sqlQuery($sql);
		
		$this->userid = $this->_dbl->sqlInsertId();
		
		/*
		*	Set up the new user's lightbox
		*/
	
		// Create an empty lightbox for this new user
		$this->_dbl->sqlQuery("INSERT INTO " . PIX_TABLE_LBOX . " VALUES ('0','My lightbox',NOW(),'$this->userid','10','10');");
		
		// Get the id of the newly made lightbox for this user
		$active_lightbox = $this->_dbl->sqlInsertId();
		
		// Update the user's profile with this lightbox
		$this->_dbl->sqlQuery("UPDATE " . PIX_TABLE_USER . " SET active_image_box = '$active_lightbox' WHERE userid = '$this->userid';");
		
		return true;
				
	}
	
	/*
	*
	*	
	*
	*/
	function createNewUserMinimal () {
		
		global $smarty, $cfg;
		
		// Validate the information submitted for the profile
		$this->validateUserProfileData('admin','create',false);
		
		// If the information doesn't validate, return false
		if (!$this->profile_valid) { return false; }
		
		if ($this->_update_flag) {
			
			// Set whether or not the user can log in without approval by an administrator
			if ($cfg['set']['user_auto_approve']) { $account_status = 1; } else { $account_status = 0; }
			
			// Create SQL code for the new user insertion
			$sql = "INSERT INTO " . PIX_TABLE_USER . " VALUES (
					'0',
					'0',
					'".mysql_real_escape_string($this->email_address)."',
					'',
					'',
					'".mysql_real_escape_string($this->first_name)."',
					'',
					'".mysql_real_escape_string($this->family_name)."',
					'',
					'',
					'',
					NOW(),
					NOW(),
					NOW(),
					'$account_status',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'0',
					'10',
					'0',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					'',
					''
					)";

			$this->_dbl->sqlQuery($sql);
			
			$this->userid = $this->_dbl->sqlInsertId();
			
			/*
			*	Set up the new user's lightbox
			*/
		
			// Create an empty lightbox for this new user
			$this->_dbl->sqlQuery("INSERT INTO " . PIX_TABLE_LBOX . " VALUES ('0','My first lightbox',NOW(),'$this->userid','10','10');");
			
			// Get the id of the newly made lightbox for this user
			$active_lightbox = $this->_dbl->sqlInsertId();
			
			// Update the user's profile with this lightbox
			$this->_dbl->sqlQuery("UPDATE " . PIX_TABLE_USER . " SET active_image_box = '$active_lightbox' WHERE userid = '$this->userid';");
			
			/*
			*	Apply the group memberships
			*/
			
			$this->applyGroupMemberships();
			
			/*
			*	We need to send an e-mail to the user with a link to complete the registration
			*/
			
			// Get information required to send a confirmation
			$userid 	= $this->userid;
			$to_name	= $this->first_name . " " .  $this->family_name;
			
			// Generate security key for registration link
			$key = strtoupper(substr(md5($userid.$cfg['sys']['encryption_key']),0,6));
			
			// Generate the link
			$smarty->assign("registration_link",$cfg['sys']['base_url'] . FILE_PUB_ACCOUNT . "?cmd=acceptInvitation&userid=$userid&key=$key");
			
			// Set the to address
			$smarty->assign("to_name",$to_name);
			
			// Load the e-mail template
			$message = $smarty->pixFetch("email.templates/new.user.invitation.tpl");
			
			// Send the e-mail
			sendEmail($this->email_address, $to_name, $message, $cfg['set']['contact_email'], $cfg['set']['contact_name'], $cfg['set']['site_name']." Invitation");
			
			return true;
			
		} else {
		
			return false;
		
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function createNewUserComplete () {
		
		global $cfg, $smarty;
		
		// Validate the information submitted for the profile
		$this->validateUserProfileData('user','create',false);
		
		// If the information doesn't validate, return false
		if (!$this->profile_valid) { return false; }
		
		if ($cfg['set']['user_auto_approve'] == 1) { $approved = 1; } else { $approved = 0; }
		
		if ($this->_update_flag) {
			
			$query = "INSERT INTO " . PIX_TABLE_USER . " VALUES (
																	'0',
																	'10',
																	'".mysql_real_escape_string($this->email_address)."',
																	'".mysql_real_escape_string($this->formal_title)."',
																	'".mysql_real_escape_string($this->other_title)."',
																	'".mysql_real_escape_string($this->first_name)."',
																	'".mysql_real_escape_string($this->middle_initial)."',
																	'".mysql_real_escape_string($this->family_name)."',
																	'".md5($this->password_1)."',
																	'".mysql_real_escape_string($this->password_rem_que)."',
																	'".mysql_real_escape_string($this->password_rem_ans)."',
																	NOW(),
																	NOW(),
																	'',
																	'$approved',
																	'".mysql_real_escape_string($this->telephone)."',
																	'".mysql_real_escape_string($this->mobile)."',
																	'".mysql_real_escape_string($this->fax)."',
																	'".mysql_real_escape_string($this->addr1)."',
																	'".mysql_real_escape_string($this->addr2)."',
																	'".mysql_real_escape_string($this->addr3)."',
																	'".mysql_real_escape_string($this->city)."',
																	'".mysql_real_escape_string($this->region)."',
																	'".mysql_real_escape_string($this->country)."',
																	'".mysql_real_escape_string($this->postal_code)."',
																	'',
																	'',
																	'0',
																	'".mysql_real_escape_string($this->other_business_type)."',
																	'".mysql_real_escape_string($this->other_business_position)."',
																	'".mysql_real_escape_string($this->other_image_interest)."',
																	'".mysql_real_escape_string($this->other_frequency)."',
																	'".mysql_real_escape_string($this->other_circulation)."',
																	'".mysql_real_escape_string($this->other_territories)."',
																	'".mysql_real_escape_string($this->other_website)."',
																	'".mysql_real_escape_string($this->other_message)."',
																	'".mysql_real_escape_string($this->other_company_name)."'
																	);";
														
			$this->_dbl->sqlQuery($query);	
			
			$this->userid = $this->_dbl->sqlInsertId();
		
			/*
			*	Set up the new user's lightbox
			*/
		
			// Create an empty lightbox for this new user
			$this->_dbl->sqlQuery("INSERT INTO " . PIX_TABLE_LBOX . " VALUES ('0','My first lightbox',NOW(),'$this->userid','10','10');");
			
			// Get the id of the newly made lightbox for this user
			$active_lightbox = $this->_dbl->sqlInsertId();
			
			// Update the user's profile with this lightbox
			$this->_dbl->sqlQuery("UPDATE " . PIX_TABLE_USER . " SET active_image_box = '$active_lightbox' WHERE userid = '$this->userid';");
			
			/*
			*	Send the user a welcome e-mail
			*/
			
			$to_name 	= $this->first_name." ".$this->family_name;
			$to_email 	= $this->email_address;
			
			// Load variables required to send e-mail
			$smarty->assign("to_name",$to_name);
			$smarty->assign("to_email",$to_email);
			$smarty->assign("password",$this->password_2);
			
			// Load the message text from the template
			$message = $smarty->pixFetch('email.templates/registration.confirmation.01.tpl');
			
			// Send a confirmation e-mail to the user
			sendEmail($to_email, $to_name, $message, $cfg['set']['contact_email'], $cfg['set']['contact_name'], $cfg['set']['site_name']." Registration Information");
			
			/*
			*	Send the administrator a notification e-mail
			*/
		
			// Load the message text from the template
			$message = $smarty->pixFetch('email.templates/registration.confirmation.02.tpl');
			
			// Send a confirmation e-mail to the administrator
			sendEmail($cfg['set']['contact_email'], $cfg['set']['contact_name'], $message, $cfg['set']['contact_email'], $cfg['set']['contact_name'], $cfg['set']['site_name']." New Registration");
			
			return true;
			
		} else {
		
			return false;
		
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function editUser ($mode,$type,$invite) {
		
		// Validate the information submitted for the profile
		$this->validateUserProfileData($mode,$type,$invite);
		
		// If the information doesn't validate, return false
		if (!$this->profile_valid) { return false; }
		
		if ($this->_update_flag) {
			
			// Only change the password if it's not empty and both submitted passwords match
			if (($this->password_1 == $this->password_2) && $this->password_1 != "") {
				$password_sql = "password = '" . md5($this->password_1) . "',\n";
			}
			
			$sql = "UPDATE " . PIX_TABLE_USER . " SET 
			
					account_status			= '".mysql_real_escape_string($this->account_status)."',
					email_address			= '".mysql_real_escape_string($this->email_address)."',
					formal_title			= '".mysql_real_escape_string($this->formal_title)."',
					other_title				= '".mysql_real_escape_string($this->other_title)."',
					first_name				= '".mysql_real_escape_string($this->first_name)."',
					family_name				= '".mysql_real_escape_string($this->family_name)."',
					middle_initial			= '".mysql_real_escape_string($this->middle_initial)."',
					$password_sql
					password_rem_que		= '".mysql_real_escape_string($this->password_rem_que)."',
					password_rem_ans		= '".mysql_real_escape_string($this->password_rem_ans)."',
					telephone				= '".mysql_real_escape_string($this->telephone)."',
					mobile					= '".mysql_real_escape_string($this->mobile)."',
					fax						= '".mysql_real_escape_string($this->fax)."',
					addr1					= '".mysql_real_escape_string($this->addr1)."',
					addr2					= '".mysql_real_escape_string($this->addr2)."',
					addr3					= '".mysql_real_escape_string($this->addr3)."',
					city					= '".mysql_real_escape_string($this->city)."',
					region					= '".mysql_real_escape_string($this->region)."',
					country					= '".mysql_real_escape_string($this->country)."',
					postal_code				= '".mysql_real_escape_string($this->postal_code)."',
					date_edited				= NOW(),
					other_company_name		= '".mysql_real_escape_string($this->other_company_name)."',
					other_business_type		= '".mysql_real_escape_string($this->other_business_type)."',
					other_business_position	= '".mysql_real_escape_string($this->other_business_position)."',
					other_image_interest	= '".mysql_real_escape_string($this->other_image_interest)."',
					other_frequency			= '".mysql_real_escape_string($this->other_frequency)."',
					other_circulation		= '".mysql_real_escape_string($this->other_circulation)."',
					other_territories		= '".mysql_real_escape_string($this->other_territories)."',
					other_website			= '".mysql_real_escape_string($this->other_website)."'
					
					WHERE userid 			= '".$this->userid."';";
			
			$this->_dbl->sqlQuery($sql);
			
			// If we're in admin mode, update the group permissions as well
			if ($mode == "admin") {	$this->applyGroupMemberships(); }
			
			return true;
			
		} else {
		
			return false;
		
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function resetPassword () {
		
		if ($this->_update_flag) {
		
			$query	= "UPDATE " . PIX_TABLE_USER . " SET	date_edited		= NOW(),
															password		= '".md5($this->password_1)."'
																		
															WHERE userid	= '".$this->userid."';";
			$this->_dbl->sqlQuery($query);
			
			// Build required values for e-mails
			$to_name 	= $this->first_name." ".$this->family_name;
			$to_email 	= $this->email_address;
			
			// Load variables required to send e-mail
			$smarty->assign("to_name",$to_name);
			$smarty->assign("to_email",$to_email);
			$smarty->assign("password",$this->password_1);
			
			// Load the message text from the template
			$message = $smarty->pixFetch('email.templates/password.reset.tpl');
			
			// Send a confirmation e-mail to the user
			sendEmail($to_email, $to_name, $message, $cfg['set']['contact_email'], $cfg['set']['contact_name'], $cfg['set']['site_name']." Password Reset");
			
			return true;
			
		} else {
		
			return false;
		
		}
		
	}
	
	/*
	*	MAKE THIS USER A CONTRIBUTING PHOTOGRAPHER
	*/
	function makePhotographer () {
		
		if ($this->userid != '' && is_numeric($this->userid)) {
			
			// Now add the user back to the group
			$this->_dbl->sqlQuery("INSERT INTO " . PIX_TABLE_SGME . " VALUES ('3','$this->userid');");
			
		}
		
	}
	
	/*
	*	MAKE THIS USER AN IMAGE EDITOR
	*/
	function makeImageEditor () {
	
		if ($this->userid != '' && is_numeric($this->userid)) {
			
			// Now add the user back to the group
			$this->_dbl->sqlQuery("INSERT INTO " . PIX_TABLE_SGME . " VALUES ('2','$this->userid');");
			
		}
		
	}
	
	/*
	*	MAKE THIS USER A ABLE TO DELETE OWN PHOTOGRAPHS
	*/
	function makeImageDelete () {
	
		if ($this->userid != '' && is_numeric($this->userid)) {
			
			// Now add the user back to the group
			$this->_dbl->sqlQuery("INSERT INTO " . PIX_TABLE_SGME . " VALUES ('4','$this->userid');");
			
		}
		
	}
	
	/*
	*	MAKE THIS USER ABLE TO DOWNLOAD HIGH-RESOLUTION IMAGES
	*/
	function makeHiResDownload () {
	
		if ($this->userid != '' && is_numeric($this->userid)) {
			
			// Now add the user back to the group
			$this->_dbl->sqlQuery("INSERT INTO " . PIX_TABLE_SGME . " VALUES ('5','$this->userid');");
			
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function applyGroupMemberships () {
		
		global $ses;
		
		// Process the administrator group membership
		if ($this->is_administrator && $this->_update_flag && isset($this->userid)) { // User needs to be added to the group members table as group_id = 1
			
			// First delete the user from this group (in case they're already in it)
			$this->_dbl->sqlQuery("DELETE FROM " . PIX_TABLE_SGME . " WHERE sys_userid = '$this->userid' AND sys_group_id = '1';");
		
			// Now add the user back to the group
			$this->_dbl->sqlQuery("INSERT INTO " . PIX_TABLE_SGME . " VALUES ('1','$this->userid');");
			
		} elseif ($this->userid != $ses['psg_userid']) { // User needs to be deleted from the group members table as group_id = 1
		
			// Delete the user from this group
			$this->_dbl->sqlQuery("DELETE FROM " . PIX_TABLE_SGME . " WHERE sys_userid = '$this->userid' AND sys_group_id = '1';");
			
		}
		
		// Process the image editor group membership
		if ($this->is_editor && $this->_update_flag && isset($this->userid)) { // User needs to be added to the group members table as group_id = 2
			
			// First delete the user from this group (in case they're already in it)
			$this->_dbl->sqlQuery("DELETE FROM " . PIX_TABLE_SGME . " WHERE sys_userid = '$this->userid' AND sys_group_id = '2';");
		
			// Now add the user back to the group
			$this->_dbl->sqlQuery("INSERT INTO " . PIX_TABLE_SGME . " VALUES ('2','$this->userid');");
			
		} else { // User needs to be deleted from the group members table as group_id = 2
		
			// Delete the user from this group
			$this->_dbl->sqlQuery("DELETE FROM " . PIX_TABLE_SGME . " WHERE sys_userid = '$this->userid' AND sys_group_id = '2';");
			
		}
		
		// Process the photographer group membership
		if ($this->is_photographer && $this->_update_flag && isset($this->userid)) { // User needs to be added to the group members table as group_id = 2
			
			// First delete the user from this group (in case they're already in it)
			$this->_dbl->sqlQuery("DELETE FROM " . PIX_TABLE_SGME . " WHERE sys_userid = '$this->userid' AND sys_group_id = '3';");
		
			// Now add the user back to the group
			$this->_dbl->sqlQuery("INSERT INTO " . PIX_TABLE_SGME . " VALUES ('3','$this->userid');");
			
		} else { // User needs to be deleted from the group members table as group_id = 3
		
			// Delete the user from this group
			$this->_dbl->sqlQuery("DELETE FROM " . PIX_TABLE_SGME . " WHERE sys_userid = '$this->userid' AND sys_group_id = '3';");
			
		}
		
		// Process the image delete group membership
		if ($this->is_delete && $this->_update_flag && isset($this->userid)) { // User needs to be added to the group members table as group_id = 2
			
			// First delete the user from this group (in case they're already in it)
			$this->_dbl->sqlQuery("DELETE FROM " . PIX_TABLE_SGME . " WHERE sys_userid = '$this->userid' AND sys_group_id = '4';");
		
			// Now add the user back to the group
			$this->_dbl->sqlQuery("INSERT INTO " . PIX_TABLE_SGME . " VALUES ('4','$this->userid');");
			
		} else { // User needs to be deleted from the group members table as group_id = 4
		
			// Delete the user from this group
			$this->_dbl->sqlQuery("DELETE FROM " . PIX_TABLE_SGME . " WHERE sys_userid = '$this->userid' AND sys_group_id = '4';");
			
		}
		
		// Process the image download group membership
		if ($this->is_download && $this->_update_flag && isset($this->userid)) { // User needs to be added to the group members table as group_id = 2
			
			// First delete the user from this group (in case they're already in it)
			$this->_dbl->sqlQuery("DELETE FROM " . PIX_TABLE_SGME . " WHERE sys_userid = '$this->userid' AND sys_group_id = '5';");
		
			// Now add the user back to the group
			$this->_dbl->sqlQuery("INSERT INTO " . PIX_TABLE_SGME . " VALUES ('5','$this->userid');");
			
		} else { // User needs to be deleted from the group members table as group_id = 5
		
			// Delete the user from this group
			$this->_dbl->sqlQuery("DELETE FROM " . PIX_TABLE_SGME . " WHERE sys_userid = '$this->userid' AND sys_group_id = '5';");
			
		}
		
		// Loop through $_POST['groups'] and assign this user back to those groups
		if (is_array($this->groups) && $this->_update_flag && isset($this->userid)) {
		
			// First delete the user from all the groups (in case they're already in one)
			$this->_dbl->sqlQuery("DELETE FROM " . PIX_TABLE_GRME . " WHERE userid = '$this->userid' AND group_id != '1' AND group_id != '2';");
		
			foreach($this->groups as $key => $value) {
					
				// Now add the user back to the group
				$this->_dbl->sqlQuery("INSERT INTO " . PIX_TABLE_GRME . " VALUES ('$value','$this->userid');");
				
			}
		
		} else {
		
			// Delete the user from this group
			$this->_dbl->sqlQuery("DELETE FROM " . PIX_TABLE_GRME . " WHERE userid = '$this->userid' AND group_id != '1' AND group_id != '2';");
			
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function validateEmailAddress($email) {
	
		if (eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{1,4}",$email)) {
		
			return TRUE;
		
		} else {
		
			return FALSE;
			
		}
	
	}
	
	/*
	*
	*	Validates the user profile data properties of this object
	*	
	*	$type 		= the type of user doing the action and can be 'admin' or 'user'
	*	$mode 		= the mode of action being performed can be 'create' or 'edit'
	*	$invite 	= Boolean value of whether we are validating an invitation completion request (e-mail isn't validated if true)
	*
	*/
	function validateUserProfileData($type,$mode,$invite="") {
		
		global $cfg, $lang;
		
		$result = $this->_dbl->sqlQuery("SELECT * FROM " . PIX_TABLE_USDA);
		
		if (is_array($result)) {
		
			foreach ($result as $key => $value) {
			
				$data[$value['name']] = $value['value'];
	
			}
		
		}
		
		$formal_title				= $this->formal_title;
		$other_title				= $this->other_title;
		$first_name					= $this->first_name;
		$middle_intial				= $this->middle_intial;
		$family_name				= $this->family_name;
		$email_address				= $this->email_address;
		$email_address_old			= $this->email_address_old;
		$telephone					= $this->telephone;
		$mobile						= $this->mobile;
		$fax						= $this->fax;
		$addr1						= $this->addr1;
		$addr2						= $this->addr2;
		$addr3						= $this->addr3;
		$city						= $this->city;
		$region						= $this->region;
		$country					= $this->country;
		$postal_code				= $this->postal_code;
		$password_1					= $this->password_1;
		$password_2					= $this->password_2;
		$password_rem_que			= $this->password_rem_que;
		$password_rem_ans			= $this->password_rem_ans;
		$other_business_type		= $this->other_business_type;
		$other_business_position	= $this->other_business_position;
		$other_image_interest		= $this->other_image_interest;
		$other_frequency			= $this->other_frequency;
		$other_circulation			= $this->other_circulation;
		$other_territories			= $this->other_territories;
		$other_website				= $this->other_website;
		$other_company_name			= $this->other_company_name;
		$other_message				= $this->other_message;
	
		// First assume no errors
		$problem = "0";
		
		// Create an array to hold each error in
		$problem_status = array();
		
		if ($type != "admin" && $data['salutation_req'] == 1 && $formal_title == "") {
		$problem = "1";
		$problem_status[] = "You must provide a salutation or title.";
		}
		
		if ($first_name == "") {
		$problem = "1";
		$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_01'];
		}
		
		if ($family_name == "") {
		$problem = "1";
		$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_02'];
		}
		
		if ($email_address == "") {
		$problem = "1";
		$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_03'];
		}
		
		if ($cfg['set']['user_ban_domains'] == 1 && $type != "admin") {
			
			// Load list of banned domain names
			$sql = "SELECT domain_name FROM ".PIX_TABLE_DOMN." ORDER BY domain_name ASC";
			
			$result = $this->_dbl->sqlSelectRows($sql);
			
			$banned = (bool)FALSE;
			
			if (is_array($result)) {
			
				foreach ($result as $value) {
					
					if (eregi($value['domain_name'],$email_address)) {
						
						$banned = (bool)TRUE;
						$domain = $value['domain_name'];
					
					}
				
				}
			
			}
			
			if ($banned) {
			
				$problem = "1";
				$problem_status[] = "You cannot register on this site using an e-mail address at the domain '$domain'.";
	
			}
			
		}
		
		if (($mode == "create" && $email_address != "") || ($mode != "create" && $email_address != $email_address_old)) {
		
			$email_address = strtolower($email_address);
			
			if (!$this->validateEmailAddress($email_address)) {
			
				$problem = "1";
				$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_04'];
			
			} else 	{
			
				if ($mode != "edit" && !$invite) {
				
					# Check that the user does not already exist on the system.
					$result = $this->_dbl->sqlCountRows("SELECT COUNT(userid) FROM " . PIX_TABLE_USER . " WHERE email_address = '$email_address'");
					
					// If e-mail address exists, $result will return a userid.				
					if ($result > 0) {
					
						$problem = "1";
						$problem_status[] = sprintf($GLOBALS['_STR_']['PROFILE_VALID_05'],$email_address,$cfg['sys']['base_url'].$cfg['index_login']);
					}
					
				}
					
			}
			
		}
		
		if ( ( ($mode == "edit" || $type == "admin") && ($password_1 != "" || $password_2 != "") ) || ($mode == "create" && $type != "admin")) {
		
			if ($password_1 == "") {
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_06'];
			}
			
			if ($password_2 == "") {
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_07'];
			}
			
			if ($password_1 != $password_2) {
			
				$problem = "1";
				$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_08'];
			
			} else {
			
				if (!eregi("^([a-zA-Z0-9]{4})([a-zA-Z0-9]+)", $password_1)) {
				$problem = "1";
				$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_09'];
				}
				
			}
			
		}
		
		if ($type != "admin") {
		
			if ($password_rem_que == "") {
			$problem = "1";
			$problem_status[] .= $GLOBALS['_STR_']['PROFILE_VALID_10'];
			}
			
			if ($password_rem_ans == "") {
			$problem = "1";
			$problem_status[] .= $GLOBALS['_STR_']['PROFILE_VALID_11'];
			}
		
		}
		
		// We won't check required information if this is an admin edit
		if ($type != "admin") {
			
			if ($data['telephone_req'] == 1 && $telephone == "") {
				$problem = "1";
				$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_12'];
			}
		
			if ($data['mobile_req'] == 1 && $mobile == "") {
				$problem = "1";
				$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_13'];
			}
		
			if ($data['facsimile_req'] == 1 && $fax == "") {
				$problem = "1";
				$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_14'];
			}
		
			if ($data['company_name_req'] == 1 && $other_company_name == "") {
				$problem = "1";
				$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_15'];
			}
		
			if ($data['business_type_req'] == 1 && $other_business_type == "") {
				$problem = "1";
				$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_16'];
			}
		
			if ($data['business_position_req'] == 1 && $other_business_position == "") {
				$problem = "1";
				$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_17'];
			}
		
			if ($mode == "create" && $data['image_interest_req'] == 1 && $other_image_interest == "") {
				$problem = "1";
				$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_18'];
			}
		
			if ($data['frequency_req'] == 1 && $other_frequency == "") {
				$problem = "1";
				$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_19'];
			}
		
			if ($data['circulation_req'] == 1 && $other_circulation == "") {
				$problem = "1";
				$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_20'];
			}
		
			if ($data['territories_req'] == 1 && $other_territories == "") {
				$problem = "1";
				$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_21'];
			}
		
			if ($data['website_req'] == 1 && $other_website == "") {
				$problem = "1";
				$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_22'];
			}
		
			if ($data['address_req'] == 1) {
				
				if ($addr1 == "") {
					$problem = "1";
					$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_23'];
				}
				
				if ($city == "") {
					$problem = "1";
					$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_24'];
				}
				
				if ($postal_code == "") {
					$problem = "1";
					$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_25'];
				}
				
			}
			
			if ($mode == "create" && $data['message_req'] == 1 && $other_message == "") {
				$problem = "1";
				$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_26'];
			}
		
		}
		
		if ($problem == "1") {
			
			$this->profile_valid	= (bool)false;
			$this->profile_errors	= $problem_status;
			
		} else {
			
			$this->profile_valid	= (bool)true;
			$this->profile_errors	= '';
			
		}
		
	}

}
?>