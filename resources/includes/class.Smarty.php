<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

/*
*
*	Smarty Pixaria Gallery Class
*	
*	This class creates an instance of Smarty with variables and settings
*	specific to Pixaria Gallery including all the global system and language
*	settings and style attributes
*
*/

// Load the main Smarty class
require_once (SYS_BASE_PATH . "resources/application/smarty.php");
require_once (SYS_BASE_PATH . "resources/smarty/libs/Smarty.class.php");

class Smarty_Pixaria extends Smarty {

	function Smarty_Pixaria()

	{
   		global $cfg, $lang, $objEnvData, $style, $page_title, $ses, $admin_page_section;
   		
		// Class Constructor. These automatically get set with each new instance.

		$this->Smarty();
		
		// Define template directory path
		if (eregi("resources/admin",$_SERVER['REQUEST_URI'])) {
		
			$this->template_dir		= SYS_BASE_PATH.'resources/templates_admin/';
			
		} else {
		
			$this->template_dir		= SYS_BASE_PATH.'resources/templates_default/';
			
		}
		
		// Set up smarty environment
		$this->compile_dir		= SYS_BASE_PATH.'resources/smarty/templates_c/';
		$this->config_dir		= SYS_BASE_PATH.'resources/smarty/configs/';
		$this->cache_dir		= SYS_BASE_PATH.'resources/smarty/cache/';
		$this->compile_id		= LOCAL_LANGUAGE;
		
		// Assign smarty configuration variables
		$this->compile_check 	= false;
		$this->debugging 		= false;
		$this->caching 			= false;
		
		if ($cfg['set']['smarty_cache']==1) { $this->clear_compiled_tpl(); }
		
		$this->assign("browser_name",$objEnvData->getBrowserAppName());
		$this->assign("browser_version",$objEnvData->getBrowserAppVersion());
		$this->assign("browser_OS_name",$objEnvData->getBrowserSystemName());
		$this->assign("browser_OS_version",$objEnvData->getBrowserSystemVersion());
		
		// Assign site properties to smarty
		$this->assign("admin_page_section",$admin_page_section);
		$this->assign("site_name",$cfg['set']['site_name']);
		$this->assign("base_url",$cfg['sys']['base_url']);
		$this->assign("base_path",SYS_BASE_PATH);
		$this->assign("base_incoming",$cfg['sys']['base_incoming']);
		$this->assign("page_title",$page_title);
		$this->assign("base_uri",htmlentities($_SERVER['REQUEST_URI']));
		$this->assign("query_string",htmlentities($_SERVER['QUERY_STRING']));
		$this->assign("base_complete",$cfg['sys']['base_url'] . ltrim($_SERVER['REQUEST_URI'],"/"));
		
		// Register Pixaria's custom Smarty functions
		$this->register_function("include_template","smarty_include_template");
		$this->register_function("load_pixie","smarty_load_pixie");
		$this->register_function("html_fancy_select_multiple","smarty_fancy_select_multiple");
		$this->register_function("html_fancy_select_single","smarty_fancy_select_single");
		$this->register_function("fancy_column","smarty_fancy_column");
		$this->register_function("date_picker","smarty_date_picker");
		$this->register_function("textbox_list","smarty_textbox_list");
		
		foreach ($lang as $key => $value) {
		
			$this->assign("lan_".$key,$value);
		
		}
		
		foreach ($cfg['sys'] as $key => $value) {
		
			$this->assign("sys_".$key,$value);
		
		}

		foreach ($cfg['set'] as $key => $value) {
		
			$this->assign("set_".$key,$value);
		
		}

		foreach ($cfg['fil'] as $key => $value) {
		
			$this->assign("fil_".$key,$value);
		
		}

		if (count($ses) > 0) {
		
			foreach ($ses as $key => $value) {
			
				$this->assign("ses_".$key,$value);
			
			}
			
		}
		
		$this->register_postfilter("pixSmartyMLP");
		
		// Remove system level variables from Smarty that might pose a security risk
		$this->clear_assign(array('sys_db_uname','sys_db_host','sys_db_name','sys_db_pword','sys_db_encryption_key'));

	}
	
	/*
	*	Checks to see if template file is in the theme directory and
	*	if it isn't loads the default template file instead
	*/
	function pixDisplay($template) {
		
		global $cfg, $admin_page;
		
		// Security
		if (eregi("http://",$template)) { return; }
		if (eregi("ftp://",$template)) { return; }
		
		if ($admin_page) {
		
			Smarty::display($template);
			
		} else {
			
			if (file_exists(SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/templates/".$template)) {
				
				Smarty::display(SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/templates/".$template);
			
			} else {
				
				Smarty::display($template);
			
			}
		
		}
		
	}
	
	/*
	*
	*	EXTENDS THE PARENT DISPLAY METHOD
	*
	*	Checks to see if template file is in the theme directory and
	*	if it isn't loads and displays the default template file instead.
	*
	*/
	function display ($template) {
		
		global $cfg, $admin_page;
		
		// Security
		if (eregi("http://",$template)) { return; }
		if (eregi("ftp://",$template)) { return; }
		
		if ($admin_page) {
		
			Smarty::display($template);
			
		} else {
		
			if (file_exists(SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/templates/".$template)) {
				
				Smarty::display(SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/templates/".$template);
			
			} else {
				
				Smarty::display($template);
			
			}
		
		}
		
	}
	
	/*
	*	Checks to see if template file is in the theme directory and
	*	if it isn't loads the default template file instead
	*/
	function pixFetch($template) {
		
		global $cfg, $admin_page;
		
		// Security
		if (eregi("http://",$template)) { return; }
		if (eregi("ftp://",$template)) { return; }
		
		if ($admin_page) {
		
			$result = $this->fetch($template);
			
		} else {
		
			if (file_exists(SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/templates/".$template)) {
				
				$result = $this->fetch(SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/templates/".$template);
			
			} else {
				
				$result = $this->fetch($template);
			
			}
		
		}
		
		return $result;
		
	}

	/*
	*
	*	This function outputs a page telling the user
	*	to wait while their request is processed
	*	
	*	This function is for the admin controls only
	*
	*/
	
	function pixWaiting($meta_url,$meta_time=4) {
	
		global $cfg, $lang;
	
		$this->assign("meta_header","<meta http-equiv=\"Refresh\" content=\"$meta_time;url=$meta_url\">");
		
		$this->assign("meta_link","If this page does not refresh after 10 seconds, <a href=\"$meta_url\">click here</a>");
		
		$this->assign("wait_mess","Processing your request, please wait!");
		
		$this->pixDisplay("admin.snippets/admin.html.processing.tpl");
		
	}
	
	/*
	*
	*	This function outputs a page telling the user
	*	to wait while their request is processed
	*	
	*	This function is for the front end only
	*
	*/
	
	function pixProcessing($meta_url,$meta_time=4) {
	
		global $cfg, $lang;
	
		$this->assign("meta_header","<meta http-equiv=\"Refresh\" content=\"$meta_time;url=$meta_url\">");
		
		$this->assign("meta_url",$meta_url);
		
		$this->pixDisplay("index.snippets/pixaria.processing.tpl");
		
	}
	
}

/*
*
*	Multi-Language Processor
*
*/
function pixSmartyMLP($tpl_source, &$smarty) {
	
	if (!is_array($GLOBALS['_STR_'])) {
	  die("Error loading Multilanguage Support");
	}
	
	// Now replace the matched language strings with the entry in the file
	return preg_replace_callback('/##(.+?)##/', 'pixSmartyMLP_Callback', $tpl_source);
	
}

/*
*
*	Multi-Language Processor Callback
*
*/
function pixSmartyMLP_Callback($key) {

	$data = $key[1];
	
	return pixSmartyMLVP($GLOBALS['_STR_'][$data]);
	
}

/*
*
*	
*
*/
function pixSmartyMLVP($string) {
	
	// Now replace the matched language strings with the entry in the file
	return preg_replace_callback('/\$([a-zA-Z0-9_]*)/', 'pixSmartyMLVP_Callback', $string);
	
}

/*
*
*
*
*/
function pixSmartyMLVP_Callback($key) {

	global $smarty;
	
	if (!is_object($smarty)) {
	  die("Error accessing Smarty object");
	}
	
	$data = $key[1];
	
	return ("<?php echo \$this->_tpl_vars['$data']; ?>");
	
}

?>