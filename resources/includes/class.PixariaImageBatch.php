<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

// Load the PixariaImage class so we can extend it
require_once ('class.PixariaImage.php');
		
/*
*	
*	A class to handle working with multiple images.
*	
*	Extends the PixariaImage class
*	
*	Exposes the following methods:
*	
*	public	setChangeProperty()		Set the properties that must be updated
*	
*/

class PixariaImageBatch extends PixariaImage {
	
	// Private variables
	var $_chg_image_path 				= false;
	var $_chg_image_userid 				= false;
	var $_chg_image_category_id 		= false;
	var $_chg_image_filename 			= false;
	var $_chg_image_title 				= false;
	var $_chg_image_caption 			= false;
	var $_chg_image_filetype 			= false;
	var $_chg_image_keywords 			= false;
	var $_chg_image_width 				= false;
	var $_chg_image_height 				= false;
	var $_chg_image_copyright 			= false;
	var $_chg_image_headline 			= false;
	var $_chg_image_date 				= false;
	var $_chg_image_permissions 		= false;
	var $_chg_image_rights_type 		= false;
	var $_chg_image_rights_text 		= false;
	var $_chg_image_colr_r 				= false;
	var $_chg_image_colr_g 				= false;
	var $_chg_image_colr_b 				= false;
	var $_chg_image_price 				= false;
	var $_chg_image_sale 				= false;
	var $_chg_image_synd 				= false;
	var $_chg_image_synd_url 			= false;
	var $_chg_image_model_release 		= false;
	var $_chg_image_property_release	= false;
	var $_chg_image_counter 			= false;
	var $_chg_image_extra_01 			= false;
	var $_chg_image_extra_02 			= false;
	var $_chg_image_extra_03 			= false;
	var $_chg_image_extra_04 			= false;
	var $_chg_image_extra_05			= false;
	var $_chg_image_viewers 			= false;
	var $_chg_image_categories 			= false;
	
	/*
	*	
	*	This is the class constructor for the PixariaImages class
	*	
	*	PixariaImages -- Load data for an image
	*	
	*	mixed PixariaImage([int image_id])
	*
	*/
	
	function PixariaImageBatch() {
		
		// Initialise the Pixaria Images class
		$this->PixariaImage();
	
	}
	
	/*
	*	Set which properties we want to change for the group of images we will be working with
	*	
	*	setChangeProperty -- Set property values to be changed
	*	
	*	mixed setChangeProperty(string property)
	*	
	*/
	
	function setChangeProperty($property) {
	
		switch ($property) {
		
			case 'image_path':
			
				$this->_chg_image_path = true;
				break;

			case 'image_userid':
				
				$this->_chg_image_userid = true;
				break;

			case 'image_filename':
				
				$this->_chg_image_filename = true;
				break;

			case 'image_title':
				
				$this->_chg_image_title = true;
				break;

			case 'image_caption':
				
				$this->_chg_image_caption = true;
				break;

			case 'image_filetype':
				
				$this->_chg_image_filetype = true;
				break;

			case 'image_keywords':
				
				$this->_chg_image_keywords = true;
				break;

			case 'image_width':
				
				$this->_chg_image_width = true;
				break;

			case 'image_height':
				
				$this->_chg_image_height = true;
				break;

			case 'image_copyright':
				
				$this->_chg_image_copyright = true;
				break;

			case 'image_headline':
				
				$this->_chg_image_headline = true;
				break;

			case 'image_date':
				
				$this->_chg_image_date = true;
				break;

			case 'image_permissions':
				
				$this->_chg_image_permissions = true;
				break;

			case 'image_rights_type':
				
				$this->_chg_image_rights_type = true;
				break;

			case 'image_rights_text':
				
				$this->_chg_image_rights_text = true;
				break;

			case 'image_colr_r':
				
				$this->_chg_image_colr_r = true;
				break;

			case 'image_colr_g':
				
				$this->_chg_image_colr_g = true;
				break;

			case 'image_colr_b':
				
				$this->_chg_image_colr_b = true;
				break;

			case 'image_price':
				
				$this->_chg_image_price = true;
				break;

			case 'image_sale':
				
				$this->_chg_image_sale = true;
				break;

			case 'image_synd':
				
				$this->_chg_image_synd = true;
				break;

			case 'image_synd_url':
				
				$this->_chg_image_synd_url = true;
				break;

			case 'image_model_release':
				
				$this->_chg_image_model_release = true;
				break;

			case 'image_property_release':
				
				$this->_chg_image_property_release = true;
				break;

			case 'image_counter':
				
				$this->_chg_image_counter = true;
				break;

			case 'image_extra_01':
				
				$this->_chg_image_extra_01 = true;
				break;

			case 'image_extra_02':
				
				$this->_chg_image_extra_02 = true;
				break;

			case 'image_extra_03':
				
				$this->_chg_image_extra_03 = true;
				break;

			case 'image_extra_04':
				
				$this->_chg_image_extra_04 = true;
				break;

			case 'image_extra_05':
				
				$this->_chg_image_extra_05 = true;
				break;

			case 'image_viewers':
				
				$this->_chg_image_viewers = true;
				break;

			case 'image_categories':
				
				$this->_chg_image_categories = true;
				break;
		
		}
	
	}
	
	/*
	*	Update all images in the array of ids passed to this function
	*	
	*	updateImages -- Update all the images in this array
	*	
	*	mixed updateImages(array image_ids)
	*	
	*/
	
	function updateImages($image_ids) {
		
		global $cfg;
		
		if (!is_array($image_ids)) {
			return;
		}
		
		// Load image properties for inclusion in the database and escape dangerous items
		$image_id				= $this->image_id;
		$image_title			= addslashes($this->image_title);
		$image_caption			= addslashes($this->image_caption);
		$image_keywords			= addslashes($this->image_keywords);
		$image_width			= $this->image_width;
		$image_height			= $this->image_height;
		$image_copyright		= addslashes($this->image_copyright);
		$image_date				= $this->image_date;
		$image_permissions		= $this->image_permissions;
		$image_rights_type		= $this->image_rights_type;
		$image_rights_text		= addslashes($this->image_rights_text);
		$image_colr_r			= $this->image_colr_r;
		$image_colr_g			= $this->image_colr_g;
		$image_colr_b			= $this->image_colr_b;
		$image_price			= addslashes($this->image_price);
		$image_sale				= $this->image_sale;
		$image_model_release	= $this->image_model_release;
		$image_property_release	= $this->image_property_release;
		$image_extra_01			= addslashes($this->image_extra_01);
		$image_extra_02			= addslashes($this->image_extra_02);
		$image_extra_03			= addslashes($this->image_extra_03);
		$image_extra_04			= addslashes($this->image_extra_04);
		$image_extra_05			= addslashes($this->image_extra_05);
		
		foreach ($image_ids as $key => $image_id) {
		
			$sql = "UPDATE ". PIX_TABLE_IMGS ." SET ";
			
			// Compile a SQL string to update these images
			if ($this->_chg_image_date == "on") 			{ $sql .= "image_date = '$image_date', "; }
			if ($this->_chg_image_title == "on") 			{ $sql .= "image_title = '$image_title', "; }
			if ($this->_chg_image_headline == "on") 		{ $sql .= "image_headline = '$image_headline', "; }
			if ($this->_chg_image_caption == "on") 			{ $sql .= "image_caption = '$image_caption', "; }
			if ($this->_chg_image_keywords == "on") 		{ $sql .= "image_keywords = '$image_keywords', "; }
			if ($this->_chg_image_width == "on") 			{ $sql .= "image_width = '$image_width', "; }
			if ($this->_chg_image_height == "on") 			{ $sql .= "image_height = '$image_height', "; }
			if ($this->_chg_image_copyright == "on") 		{ $sql .= "image_copyright = '$image_copyright', "; }
			if ($this->_chg_image_sale == "on") 			{ $sql .= "image_sale = '$image_sale', "; }
			if ($this->_chg_image_price == "on") 			{ $sql .= "image_price = '$image_price', "; }
			if ($this->_chg_image_permissions == "on") 		{ $sql .= "image_permissions = '$image_permissions', "; }
			if ($this->_chg_image_rights_type == "on") 		{ $sql .= "image_rights_type = '$image_rights_type', "; }
			if ($this->_chg_image_rights_text == "on") 		{ $sql .= "image_rights_text = '$image_rights_text', "; }
			if ($this->_chg_image_model_release == "on") 	{ $sql .= "image_model_release = '$image_model_release', "; }
			if ($this->_chg_image_property_release == "on") { $sql .= "image_property_release = '$image_property_release', "; }
			if ($this->_chg_image_image_extra_01 == "on") 	{ $sql .= "image_extra_01 = '$image_extra_01', "; }
			if ($this->_chg_image_image_extra_02 == "on") 	{ $sql .= "image_extra_02 = '$image_extra_02', "; }
			if ($this->_chg_image_image_extra_03 == "on") 	{ $sql .= "image_extra_03 = '$image_extra_03', "; }
			if ($this->_chg_image_image_extra_04 == "on") 	{ $sql .= "image_extra_04 = '$image_extra_04', "; }
			if ($this->_chg_image_image_extra_05 == "on") 	{ $sql .= "image_extra_05 = '$image_extra_05', "; }
		
			$sql .= " image_id = '$image_id' WHERE image_id = '$image_id';";
			
			// Pass the SQL command to the database object
			$this->_dbl->sqlQuery($sql);
		
			// Update the groups who can view this image
			if ($this->_chg_image_viewers) { $this->updateViewers($image_id); }
			
			// Update the categories this image is in
			if ($this->_chg_image_categories) { $this->updateCategories($image_id); }
			
		}
	
	}
	
}


?>