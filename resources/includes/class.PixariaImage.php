<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

/*
*	
*	A class to handle working with images
*	
*	Exposes the following methods:
*	
*	public	getImagePath()		Get the directory name the image is stored in
*	
*/

class PixariaImage {
	
	// Private variables
	var $_dbl;
	var $_update_flag 				= false;
	var $_update_categories 		= false;
	var $_update_viewers	 		= false;
	
	// Public variables
	var $image_id;
	var $image_active;
	var $image_path;
	var $image_userid;
	var $image_category_id;
	var $image_filename;
	var $image_title;
	var $image_caption;
	var $image_filetype;
	var $image_keywords;
	var $image_width;
	var $image_height;
	var $image_copyright;
	var $image_headline;
	var $image_date;
	var $image_permissions;
	var $image_rights_type;
	var $image_rights_text;
	var $image_colr_enable;
	var $image_colr_r;
	var $image_colr_g;
	var $image_colr_b;
	var $image_price;
	var $image_sale;
	var $image_synd;
	var $image_synd_url;
	var $image_model_release;
	var $image_property_release;
	var $image_counter;
	var $image_trashed;
	var $image_content_type;
	var $image_extra_01;
	var $image_extra_02;
	var $image_extra_03;
	var $image_extra_04;
	var $image_extra_05;

	// Computed properties
	var $image_basename;
	var $image_extension;
	var $image_base_path;
	var $image_icon_path;
	var $image_small_path;
	var $image_large_path;
	var $image_comp_path;
	var $image_original_path;
	var $image_icon_dims;
	var $image_small_dims;
	var $image_large_dims;
	var $image_comp_dims;
	var $image_original_dims;
	var $image_icon_encoded;
	var $image_small_encoded;
	var $image_large_encoded;
	var $image_comp_encoded;
	var $image_original_encoded;
	var $image_original_filesize;
	var $image_product_array = array();
	
	var $arrImageData = array();
	
	/*
	*	
	*	This is the class constructor for the PixariaImage class
	*	
	*	PixariaImage -- Load data for an image
	*	
	*	class PixariaImage([int image_id])
	*
	*/
	
	function PixariaImage($image_id = "") {
		
		global $cfg, $ses;
		
		// Load the database class
		require_once ('class.Database.php');
		
		$this->_cfg =& $cfg;
		$this->_ses =& $ses;
		
		// Create the database object
		$this->_dbl = new Database();
		
		if (is_numeric($image_id)) {
			
			if ($this->mode == "admin") {
				$sql = "SELECT * FROM ". PIX_TABLE_IMGS ." WHERE image_id = '$image_id';";
				$result = $this->_dbl->sqlSelectRow($sql);
			} else {
				$sql = "SELECT * FROM ". PIX_TABLE_IMGS ." WHERE image_id = '$image_id' ".$cfg['set']['image_visibility_sql'] . ";";
				$result = $this->_dbl->sqlSelectRow($sql);
			}
			
		} else {
		
			return;
		
		}
		
		if (is_array($result)) {
			
			// Database properties
			$this->image_id					= $result['image_id'];
			$this->image_active				= $result['image_active'];
			$this->image_path				= $result['image_path'];
			$this->image_userid				= $result['image_userid'];
			$this->image_filename			= $result['image_filename'];
			$this->image_title				= stripslashes($result['image_title']);
			$this->image_caption			= stripslashes($result['image_caption']);
			$this->image_filetype			= $result['image_filetype'];
			$this->image_keywords			= stripslashes($result['image_keywords']);
			$this->image_width				= $result['image_width'];
			$this->image_height				= $result['image_height'];
			$this->image_copyright			= stripslashes($result['image_copyright']);
			$this->image_headline			= stripslashes($result['image_headline']);
			$this->image_date				= $result['image_date'];
			$this->image_permissions		= $result['image_permissions'];
			$this->image_rights_type		= $result['image_rights_type'];
			$this->image_rights_text		= $result['image_rights_text'];
			$this->image_colr_enable		= $result['image_colr_enable'];
			$this->image_colr_r				= $result['image_colr_r'];
			$this->image_colr_g				= $result['image_colr_g'];
			$this->image_colr_b				= $result['image_colr_b'];
			$this->image_price				= $result['image_price'];
			$this->image_sale				= $result['image_sale'];
			$this->image_product_link		= $result['image_product_link'];
			$this->image_synd				= $result['image_synd'];
			$this->image_synd_url			= $result['image_synd_url'];
			$this->image_model_release		= $result['image_model_release'];
			$this->image_property_release	= $result['image_property_release'];
			$this->image_counter			= $result['image_counter'];
			$this->image_trashed			= $result['image_trashed'];
			$this->image_content_type		= $result['image_content_type'];
			$this->image_extra_01			= $result['image_extra_01'];
			$this->image_extra_02			= $result['image_extra_02'];
			$this->image_extra_03			= $result['image_extra_03'];
			$this->image_extra_04			= $result['image_extra_04'];
			$this->image_extra_05			= $result['image_extra_05'];
			$this->icon_w					= $result['icon_w'];
			$this->icon_h					= $result['icon_h'];
			$this->smal_w					= $result['smal_w'];
			$this->smal_h					= $result['smal_h'];
			$this->larg_w					= $result['larg_w'];
			$this->larg_h					= $result['larg_h'];
			$this->scom_w					= $result['scom_w'];
			$this->scom_h					= $result['scom_h'];
			$this->comp_w					= $result['comp_w'];
			$this->comp_h					= $result['comp_h'];
			$this->orig_w					= $result['orig_w'];
			$this->orig_h					= $result['orig_h'];
			$this->iptc_creator				= $result['iptc_creator'];
			$this->iptc_jobtitle			= $result['iptc_jobtitle'];
			$this->iptc_city				= $result['iptc_city'];
			$this->iptc_country				= $result['iptc_country'];
			$this->iptc_credit				= $result['iptc_credit'];
			$this->iptc_source				= $result['iptc_source'];
			$this->iptc_object				= $result['iptc_object'];
			$this->iptc_byline				= $result['iptc_byline'];
			$this->gps_longitude			= $result['gps_longitude'];
			$this->gps_latitude				= $result['gps_latitude'];
			
			// Computed properties
			$this->image_base_path			= $cfg['sys']['base_library'].$this->image_path."/";
			$this->image_icon_path			= $this->image_base_path . "32x32/" . $this->image_filename;
			$this->image_small_path			= $this->image_base_path . "80x80/" . $this->image_filename;
			$this->image_large_path			= $this->image_base_path . "160x160/" . $this->image_filename;
			$this->image_comp_path			= $this->image_base_path . COMPING_DIR."/" . $this->image_filename;
			$this->image_original_path		= $this->image_base_path . "original/" . $this->image_filename;
			
			$file_info = pathinfo($this->image_comp_path);
			
			$this->image_extension	= $file_info['extension'];
			$this->image_basename	= substr($file_info['basename'], 0, -(strlen($file_info['extension']) + ($file_info['extension'] == '' ? 0 : 1)));
			
			$this->image_icon_dims			= $this->getImageDimensions($this->icon_w, $this->icon_h);
			$this->image_small_dims			= $this->getImageDimensions($this->smal_w, $this->smal_h);
			$this->image_large_dims			= $this->getImageDimensions($this->larg_w, $this->larg_h);
			$this->image_comp_dims			= $this->getImageDimensions($this->comp_w, $this->comp_h);
			$this->image_original_dims		= $this->getImageDimensions($this->orig_w, $this->orig_h);
			
			$this->image_icon_encoded		= base64_encode($this->image_icon_path);
			$this->image_small_encoded		= base64_encode($this->image_small_path);
			$this->image_large_encoded		= base64_encode($this->image_large_path);
			$this->image_comp_encoded		= base64_encode($this->image_comp_path);
			$this->image_original_encoded	= base64_encode($this->image_original_path);
			
			$this->loadImageProducts();
			
			$this->arrImageData['basic']['id']						= $this->getImageId();
			$this->arrImageData['basic']['active']					= $this->getImageActive();
			$this->arrImageData['basic']['path']					= $this->getImagePath();
			$this->arrImageData['basic']['userid']					= $this->getImageUserId();
			$this->arrImageData['basic']['file_name']				= $this->getImageFileNameClean();
			$this->arrImageData['basic']['file_name_full']			= $this->getImageFileName();
			$this->arrImageData['basic']['title']					= $this->getImageTitle();
			$this->arrImageData['basic']['caption']					= $this->getImageCaption();
			$this->arrImageData['basic']['file_type']				= $this->getImageFileType();
			$this->arrImageData['basic']['keywords']				= $this->getImageKeywords();
			$this->arrImageData['basic']['keywords_array']			= $this->getImageKeywordsArray();
			$this->arrImageData['basic']['keywords_array_escaped']	= $this->getImageKeywordsArrayEscaped();
			$this->arrImageData['basic']['keywords_present']		= $this->getImageKeywordsPresent();
			$this->arrImageData['basic']['width']					= $this->getImageWidth();
			$this->arrImageData['basic']['height']					= $this->getImageHeight();
			$this->arrImageData['basic']['copyright']				= $this->getImageCopyright();
			$this->arrImageData['basic']['headline']				= $this->getImageHeadline();
			$this->arrImageData['basic']['date']					= $this->getImageDate();
			$this->arrImageData['basic']['permissions']				= $this->getImagePermissions();
			$this->arrImageData['basic']['rights_type']				= $this->getImageRightsType();
			$this->arrImageData['basic']['rights_text']				= $this->getImageRightsText();
			$this->arrImageData['basic']['colr_enable']				= $this->getImageColrEnable();
			$this->arrImageData['basic']['colr_mod']				= $this->getImageModRGB();
			$this->arrImageData['basic']['colr_hex']				= $this->getImageHexRGB();
			$this->arrImageData['basic']['colr_rgb']				= $this->getImageDecRGB();
			$this->arrImageData['basic']['price']					= $this->getImagePrice();
			$this->arrImageData['basic']['calculated_price']		= $this->getImagePriceCalculated();
			$this->arrImageData['basic']['for_sale']				= $this->getImageSale();
			$this->arrImageData['basic']['image_product_link']		= $this->getImageProductLink();
			$this->arrImageData['basic']['syndication']				= $this->getImageSyndication();
			$this->arrImageData['basic']['syndication_url']			= $this->getImageSyndicationURL();
			$this->arrImageData['basic']['model_release']			= $this->getImageModelRelease();
			$this->arrImageData['basic']['property_release']		= $this->getImagePropertyRelease();
			$this->arrImageData['basic']['view_count']				= $this->getImageCounter();
			$this->arrImageData['basic']['cart_link']				= $this->getImageCartLink();
			$this->arrImageData['basic']['cart_add']				= $this->getImageCartURL();
			$this->arrImageData['basic']['lightbox_add']			= $this->getImageLightboxURL();
			$this->arrImageData['basic']['lightbox_remove']			= $this->getImageLightboxRemoveURL();
			$this->arrImageData['basic']['trashed']					= $this->getImageTrashed();
			$this->arrImageData['basic']['content_type']			= $this->getImageContentType();
			$this->arrImageData['basic']['extra_01']				= $this->getImageExtra1();
			$this->arrImageData['basic']['extra_02']				= $this->getImageExtra2();
			$this->arrImageData['basic']['extra_03']				= $this->getImageExtra3();
			$this->arrImageData['basic']['extra_04']				= $this->getImageExtra4();
			$this->arrImageData['basic']['extra_05']				= $this->getImageExtra5();
			$this->arrImageData['basic']['icon_w']					= $this->getImageIconWidth();
			$this->arrImageData['basic']['icon_h']					= $this->getImageIconHeight();
			$this->arrImageData['basic']['smal_w']					= $this->getImageSmallWidth();
			$this->arrImageData['basic']['smal_h']					= $this->getImageSmallHeight();
			$this->arrImageData['basic']['larg_w']					= $this->getImageLargeWidth();
			$this->arrImageData['basic']['larg_h']					= $this->getImageLargeHeight();
			$this->arrImageData['basic']['scom_w']					= $this->getImageSmallCompingWidth();
			$this->arrImageData['basic']['scom_h']					= $this->getImageSmallCompingHeight();
			$this->arrImageData['basic']['comp_w']					= $this->getImageCompingWidth();
			$this->arrImageData['basic']['comp_h']					= $this->getImageCompingHeight();
			$this->arrImageData['basic']['orig_w']					= $this->getImageOriginalWidth();
			$this->arrImageData['basic']['orig_h']					= $this->getImageOriginalHeight();
			$this->arrImageData['basic']['iptc_creator']			= $this->getIPTCCreator();
			$this->arrImageData['basic']['iptc_jobtitle']			= $this->getIPTCJobTitle();
			$this->arrImageData['basic']['iptc_city']				= $this->getIPTCCity();
			$this->arrImageData['basic']['iptc_country']			= $this->getIPTCCountry();
			$this->arrImageData['basic']['iptc_credit']				= $this->getIPTCCredit();
			$this->arrImageData['basic']['iptc_source']				= $this->getIPTCSource();
			$this->arrImageData['basic']['iptc_object']				= $this->getIPTCObject();
			$this->arrImageData['basic']['iptc_byline']				= $this->getIPTCByline();
			$this->arrImageData['basic']['gps_longitude']			= $this->getGpsLongitude();
			$this->arrImageData['basic']['gps_latitude']			= $this->getGpsLatitude();
			
			$this->arrImageData['files']['icon_path']				= $this->getImageIconEncoded();
			$this->arrImageData['files']['small_path']				= $this->getImageSmallEncoded();
			$this->arrImageData['files']['large_path']				= $this->getImageLargeEncoded();
			$this->arrImageData['files']['comp_path']				= $this->getImageCompEncoded();
			$this->arrImageData['files']['original_path']			= $this->getImageOriginalEncoded();
			$this->arrImageData['files']['icon_size']				= $this->getImageIconDimensions();
			$this->arrImageData['files']['small_size']				= $this->getImageSmallDimensions();
			$this->arrImageData['files']['large_size']				= $this->getImageLargeDimensions();
			$this->arrImageData['files']['comp_size']				= $this->getImageCompDimensions();
			$this->arrImageData['files']['original_size']			= $this->getImageOriginalDimensions();
			$this->arrImageData['files']['canonical_url']			= $this->getImageCanonicalURL();
			$this->arrImageData['files']['icon_url']				= $this->getImageIconURL();
			$this->arrImageData['files']['small_url']				= $this->getImageSmallURL();
			$this->arrImageData['files']['large_url']				= $this->getImageLargeURL();
			$this->arrImageData['files']['comp_url']				= $this->getImageCompURL();
			$this->arrImageData['files']['original_url']			= $this->getImageOriginalURL();
			$this->arrImageData['files']['download_comp']			= $this->getImageDownloadComp();
			$this->arrImageData['files']['download_urls']			= $this->getImageDownloadURLs();
			$this->arrImageData['files']['admin_edit_return']		= $this->getImageAdminEditURL(true);
			$this->arrImageData['files']['admin_edit_noreturn']		= $this->getImageAdminEditURL(false);
			
			$this->arrImageData['extra']['weight']					= $this->getImageWeight();
			$this->arrImageData['extra']['max_print_size']			= $this->getImageMaxPrintSize();
			$this->arrImageData['extra']['products']				= $this->getImageProducts();
			$this->arrImageData['extra']['file_size_original']		= $this->getImageFileSize('original');

			
		} else {
		
			return;
		
		}
		
	}
	
	/*
	*	Returns an array of all the image data for use by Smarty
	*/
	function getImageDataComplete () {
	
		return $this->arrImageData;
		
	}
	
	/*
	*	Return the image_id
	*/
	function getImageId () {
	
		return $this->image_id;
	
	}
	
	/*
	*	Return the image_active
	*/
	function getImageActive () {
	
		return $this->image_active;
	
	}
	
	/*
	*	Return the image_path
	*/
	function getImagePath () {
	
		return $this->image_path;
	
	}
	
	/*
	*	Return the image_userid
	*/
	function getImageUserId () {
	
		return $this->image_userid;
	
	}
	
	/*
	*	Return the image_filename
	*/
	function getImageFileName () {
	
		return $this->image_filename;
	
	}
	
	/*
	*	Return the image_filename
	*/
	function getImageFileNameClean () {
		
		if (!$this->_cfg['set']['filename_noext'] && preg_match("/\./",$this->image_filename)) {
			
			$filename = explode(".", $this->image_filename);
			
			array_pop($filename);
			
			return implode(".",$filename);
			
		} else {
		
			return $this->image_filename;
			
		}
		
	}
	
	/*
	*	Return the image_title
	*/
	function getImageTitle () {
	
		return $this->image_title;
	
	}
	
	/*
	*	Return the image_caption
	*/
	function getImageCaption () {
	
		return $this->image_caption;
	
	}
	
	/*
	*	Return the image_filetype
	*/
	function getImageFileType () {
	
		return $this->image_filetype;
	
	}
	
	/*
	*	Return the image_keywords
	*/
	function getImageKeywords () {
	
		return $this->image_keywords;
	
	}
	
	/*
	*	Return the image_keywords_present
	*/
	function getImageKeywordsPresent () {
		
		if (trim($this->image_keywords != "")) {
		
			return true;
		
		} else {
		
			return false;
			
		}
	
	}
	
	/*
	*	Return the image_keywords_array_escaped data
	*/
	function getImageKeywordsArrayEscaped ($escape="") {
	
		$keywords = keywordsTranslateOutput(explode(",",$this->image_keywords),$escape);
		
		return $keywords;
		
	}

	/*
	*	Return the image_keywords_array
	*/
	function getImageKeywordsArray () {
	
		$keywords = explode(",",$this->image_keywords);
		
		if (is_array($keywords)) {
		
			foreach ($keywords as $key => $value) {
				
				// Dont show keywords with special characters in them
				if (preg_match('/\*|\.|\|/',$value)) {
					break;
				}
				
				if (eregi(" ",trim($value))) {
					$keywords_clean[] = "\"".trim($value)."\"";
				} else {
					$keywords_clean[] = trim($value);
				}
				
			}
		
		}
		
		return $keywords_clean;
		
	}

	/*
	*	Return the image_width
	*/
	function getImageWidth () {
	
		return $this->image_width;
	
	}
	
	/*
	*	Return the image_height
	*/
	function getImageHeight () {
	
		return $this->image_height;
	
	}
	
	/*
	*	Return the image_copyright
	*/
	function getImageCopyright () {
	
		return $this->image_copyright;
	
	}
	
	/*
	*	Return the image_headline
	*/
	function getImageHeadline () {
	
		return $this->image_headline;
	
	}
	
	/*
	*	Return the image_date
	*/
	function getImageDate () {
	
		return $this->image_date;
	
	}
	
	/*
	*	Return the image_permissions
	*/
	function getImagePermissions () {
	
		return $this->image_permissions;
	
	}
	
	/*
	*	Return the image_rights_type
	*/
	function getImageRightsType () {
	
		return $this->image_rights_type;
	
	}
	
	/*
	*	Return the image_rights_text
	*/
	function getImageRightsText () {
	
		return $this->image_rights_text;
	
	}
	
	/*
	*	Return array of modulus RGB colors
	*/
	function getImageModRGB () {
		
		if ($this->image_colr_enable) {
		
			return array($this->image_colr_r,$this->image_colr_g,$this->image_colr_b);
		
		} else {
		
			return array(6,6,6);
		
		}
		
	}
	
	/*
	*	Return array of hexadecimal RGB colors
	*/
	function getImageHexRGB () {
		
		$hex_r 		= strtoupper(str_pad(dechex($this->image_colr_r * 16),2,"00",STR_PAD_LEFT));
		$hex_g 		= strtoupper(str_pad(dechex($this->image_colr_g * 16),2,"00",STR_PAD_LEFT));
		$hex_b 		= strtoupper(str_pad(dechex($this->image_colr_b * 16),2,"00",STR_PAD_LEFT));
		
		$hex_str 	= $hex_r . $hex_g . $hex_b;
		
		if ($this->image_colr_enable) {
		
			return array($hex_r,$hex_g,$hex_b,$hex_str);
		
		} else {
		
			return array(60,60,60,"606060");
		
		}
	
	}
	
	/*
	*	Return array of 8 bit decimal RGB colors
	*/
	function getImageDecRGB () {
	
		$dec_r = $this->image_colr_r * 16;
		$dec_g = $this->image_colr_g * 16;
		$dec_b = $this->image_colr_b * 16;
		
		$dec_str 	= $dec_r . ", " . $dec_g . ", " . $dec_b;
		
		if ($this->image_colr_enable) {
		
			return array($dec_r,$dec_g,$dec_b,$dec_str);
		
		} else {
		
			return array(96,96,96,"96, 96, 96");
		
		}
	
	}
	
	/*
	*	Return whether colour searching is turned on for this image
	*/
	function getImageColrEnable () {
	
		return $this->image_colr_enable;
	
	}
	
	/*
	*	Return the image_price
	*/
	function getImagePrice () {
	
		return $this->image_price;
	
	}
	
	/*
	*	Return the calculated image_price
	*/
	function getImagePriceCalculated () {
		
		global $cfg;
		
		if ($this->image_price > 0) {
		
			return $this->image_price;
			
		} else {
		
			return $cfg['set']['store_default_price'];
			
		}
	
	}
	
	/*
	*	Return the image_sale
	*/
	function getImageSale () {
	
		return $this->image_sale;
	
	}
	
	/*
	*	Return the image_product_link
	*/
	function getImageProductLink () {
	
		return $this->image_product_link;
	
	}
	
	/*
	*	Return the image_synd
	*/
	function getImageSyndication () {
	
		return $this->image_synd;
	
	}

	/*
	*	Return the image_synd_url
	*/
	function getImageSyndicationURL () {
	
		return $this->image_synd_url;
	
	}

	/*
	*	Return the image_model_release
	*/
	function getImageModelRelease () {
	
		return $this->image_model_release;
	
	}

	/*
	*	Return the image_property_release
	*/
	function getImagePropertyRelease () {
	
		return $this->image_property_release;
	
	}

	/*
	*	Return the image_counter
	*/
	function getImageCounter () {
	
		return $this->image_counter;
	
	}

	/*
	*	Return the image_extra_01
	*/
	function getImageTrashed () {
	
		return $this->image_trashed;
	
	}

	/*
	*	Return the image_extra_01
	*/
	function getImageContentType () {
	
		return $this->image_content_type;
	
	}

	/*
	*	Return the image_extra_01
	*/
	function getImageExtra1 () {
	
		return $this->image_extra_01;
	
	}

	/*
	*	Return the image_extra_02
	*/
	function getImageExtra2 () {
	
		return $this->image_extra_02;
	
	}

	/*
	*	Return the image_extra_03
	*/
	function getImageExtra3 () {
	
		return $this->image_extra_03;
	
	}

	/*
	*	Return the image_extra_04
	*/
	function getImageExtra4 () {
	
		return $this->image_extra_04;
	
	}

	/*
	*	Return the image_extra_05
	*/
	function getImageExtra5 () {
	
		return $this->image_extra_05;
	
	}

	/*
	*	Return the image_icon_dims
	*/
	function getImageIconDimensions () {
	
		return $this->image_icon_dims;
	
	}

	/*
	*	Return the image_small_dims
	*/
	function getImageSmallDimensions () {
	
		return $this->image_small_dims;
	
	}

	/*
	*	Return the image_large_dims
	*/
	function getImageLargeDimensions () {
	
		return $this->image_large_dims;
	
	}

	/*
	*	Return the image_comp_dims
	*/
	function getImageCompDimensions () {
	
		return $this->image_comp_dims;
	
	}

	/*
	*	Return the image_original_dims
	*/
	function getImageOriginalDimensions () {
	
		return $this->image_original_dims;
	
	}
	
	/*
	*	Return icon_w 
	*/
	function getImageIconWidth () {
		return $this->icon_w;
	}
	
	/*
	*	Return icon_h 
	*/
	function getImageIconHeight () {
		return $this->icon_h;
	}
	
	/*
	*	Return smal_w
	*/
	function getImageSmallWidth () {
		return $this->smal_w;
	}
	
	/*
	*	Return smal_h
	*/
	function getImageSmallHeight () {
		return $this->smal_h;
	}
	
	/*
	*	Return larg_w
	*/
	function getImageLargeWidth () {
		return $this->larg_w;
	}
	
	/*
	*	Return larg_h
	*/
	function getImageLargeHeight () {
		return $this->larg_h;
	}
	
	/*
	*	Return scom_w
	*/
	function getImageSmallCompingWidth () {
		return $this->scom_w;
	}
	
	/*
	*	Return scom_h
	*/
	function getImageSmallCompingHeight () {
		return $this->scom_h;
	}
	
	/*
	*	Return comp_w
	*/
	function getImageCompingWidth () {
		return $this->comp_w;
	}
	
	/*
	*	Return comp_h
	*/
	function getImageCompingHeight () {
		return $this->comp_h;
	}
	
	/*
	*	Return orig_w
	*/
	function getImageOriginalWidth () {
		return $this->orig_w;
	}
	
	/*
	*	Return orig_h
	*/
	function getImageOriginalHeight () {
		return $this->orig_h;
	}
	
	/*
	*	Return iptc_creator
	*/
	function getIPTCCreator () {
		return $this->iptc_creator;
	}
	
	/*
	*	Return iptc_jobtitle
	*/
	function getIPTCJobTitle () {
		return $this->iptc_jobtitle;
	}
	
	/*
	*	Return iptc_city
	*/
	function getIPTCCity () {
		return $this->iptc_city;
	}
	
	/*
	*	Return iptc_country
	*/
	function getIPTCCountry () {
		return $this->iptc_country;
	}
	
	/*
	*	Return iptc_credit
	*/
	function getIPTCCredit () {
		return $this->iptc_credit;
	}
	
	/*
	*	Return iptc_source
	*/
	function getIPTCSource () {
		return $this->iptc_source;
	}
	
	/*
	*	Return iptc_object
	*/
	function getIPTCObject () {
		return $this->iptc_object;
	}
	
	/*
	*	Return iptc_byline
	*/
	function getIPTCByline () {
		return $this->iptc_byline;
	}
	
	/*
	*	Return gps_longitude
	*/
	function getGpsLongitude () {
		return $this->gps_longitude;
	}
	
	/*
	*	Return gps_latitude
	*/
	function getGpsLatitude () {
		return $this->gps_latitude;
	}
	
	/*
	*	Return the image_icon_encoded
	*/
	function getImageIconEncoded () {
	
		return $this->image_icon_encoded;
	
	}

	/*
	*	Return the image_small_encoded
	*/
	function getImageSmallEncoded () {
	
		return $this->image_small_encoded;
	
	}

	/*
	*	Return the image_large_encoded
	*/
	function getImageLargeEncoded () {
	
		return $this->image_large_encoded;
	
	}

	/*
	*	Return the image_comp_encoded
	*/
	function getImageCompEncoded () {
	
		return $this->image_comp_encoded;
	
	}

	/*
	*	Return the image_original_encoded
	*/
	function getImageOriginalEncoded () {
	
		return $this->image_original_encoded;
	
	}

	/*
	*	Return the image_icon_path
	*/
	function getImageIconPath () {
	
		return $this->image_icon_path;
	
	}

	/*
	*	Return the image_small_path
	*/
	function getImageSmallPath () {
	
		return $this->image_small_path;
	
	}

	/*
	*	Return the image_large_path
	*/
	function getImageLargePath () {
	
		return $this->image_large_path;
	
	}

	/*
	*	Return the image_comp_path
	*/
	function getImageSmallCompPath () {
	
		return $this->image_scom_path;
	
	}

	/*
	*	Return the image_comp_path
	*/
	function getImageCompPath () {
	
		return $this->image_comp_path;
	
	}

	/*
	*	Return the image_original_path
	*/
	function getImageOriginalPath () {
	
		return $this->image_original_path;
	
	}
	
	/*
	*	Return the canonical (absolute, default) URL of this image
	*/
	function getImageCanonicalURL () {
		
		global $cfg;
		
		if ($cfg['set']['func_mod_rewrite']) {
			
			return $cfg['sys']['base_url'] . "image/detail/" . $this->image_id;
		
		} else {
		
			return $cfg['sys']['base_url'] . $cfg['fil']['index_image']."?id=".$this->image_id;
		
		}
	
	}
	
	/*
	*	Return the URL of the icon thumbnail
	*/
	function getImageIconURL () {

		global $cfg;
		return $cfg['sys']['base_url'] . $cfg['fil']['psg_image_thumbnail']."?file=".$this->image_icon_encoded;
	
	}

	/*
	*	Return the URL of the small thumbnail
	*/
	function getImageSmallURL () {

		global $cfg;
		return $cfg['sys']['base_url'] . $cfg['fil']['psg_image_thumbnail']."?file=".$this->image_small_encoded;
	
	}

	/*
	*	Return the URL of the large thumbnail
	*/
	function getImageLargeURL () {

		global $cfg;
		return $cfg['sys']['base_url'] . $cfg['fil']['psg_image_thumbnail']."?file=".$this->image_large_encoded;
	
	}

	/*
	*	Return the URL of the comping image
	*/
	function getImageCompURL () {

		global $cfg;
		
		if ($cfg['set']['func_mod_rewrite']) {
			
			return $cfg['sys']['base_url'] . "image/" . $this->image_id . "/" . strtoupper(substr(md5($cfg['sys']['encryption_key'] . $this->image_id),0,6)) . "/" . $this->image_filename;
		
		} else {
		
			return $cfg['sys']['base_url'] . $cfg['fil']['psg_image_comping']."?file=".$this->image_comp_encoded;
		
		}
		
	}

	/*
	*	Return the URL of the original image
	*/
	function getImageOriginalURL () {

		global $cfg;
		return $cfg['sys']['base_url'] . $cfg['fil']['psg_image_comping']."?file=".$this->image_original_encoded;
	
	}

	/*
	*	Return the download URL of the comping image
	*	
	*	This function is duplicated in class.PixariaMultiImage.php
	*/
	function getImageDownloadComp () {

		global $cfg;
		return $cfg['sys']['base_url'] . $cfg['fil']['psg_image_comping']."?file=".$this->image_comp_encoded . "&amp;dl=1";
	
	}

	/*
	*	Return the array of download URLs
	*/
	function getImageDownloadURLs () {

		global $cfg, $ses;

		$original_path 	= $cfg['sys']['base_library'].$this->image_path."/original/".$this->image_filename;
		
		// Generate a download link if this is enabled in the site settings
		if ($cfg['set']['func_downloads']) {
			
			if ($cfg['set']['func_downloads_user'] == "10" || ($cfg['set']['func_downloads_user'] == "11" && $ses['psg_userid'] != "") || ($cfg['set']['func_downloads_user'] == "12" && $ses['psg_download'])) {
				
				$download_url['jpg'] = $this->generateDownloadURL($original_path, "JPG", $this->image_id);
				$download_url['jp2'] = $this->generateDownloadURL($original_path, "JP2", $this->image_id);
				$download_url['tif'] = $this->generateDownloadURL($original_path, "TIF", $this->image_id);
				$download_url['psd'] = $this->generateDownloadURL($original_path, "PSD", $this->image_id);
				$download_url['eps'] = $this->generateDownloadURL($original_path, "EPS", $this->image_id);
				
				return $download_url;
				
			} else {
			
				return false;
			
			}
		
		} else {
		
			return false;
		
		}
	
	}

	/*
	*	Return boolean for cart link display
	*	
	*	This function is duplicated in class.PixariaMultiImage.php
	*/
	function getImageCartLink () {

		global $cfg;

		if ($cfg['set']['func_store'] == 0 || $cfg['set']['store_images_forsale'] == 10) {
		
			return FALSE;
		
		} elseif ($cfg['set']['store_images_forsale'] == 11 && $this->image_price > 0) {
		
			return TRUE;
		
		} elseif ($cfg['set']['store_images_forsale'] == 12 && $this->image_sale == 1) {
			
			return TRUE;
		
		} elseif ($cfg['set']['store_images_forsale'] == 13) {
		
			return TRUE;
		
		} else {
		
			return FALSE;
		
		}
	
	}

	/*
	*	Return the URL of the add to cart link
	*	
	*	This function is duplicated in class.PixariaMultiImage.php
	*/
	function getImageCartURL () {

		global $cfg;
		
		// URL for creating a popup window to add to cart
		$url_popup	= "javascript:openPop('".$cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=purchaseOptionsPopup&amp;image_id=$this->image_id','" . $cfg['set']['cart_popup_width'] . "','" . $cfg['set']['cart_popup_height'] . "','yes','yes','no','no','yes','no','no');";
		
		// URL for adding to cart if already in a popup window
		$url_inline_pop = $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=purchaseOptionsPopup&amp;image_id=$this->image_id";
		
		// URL for current window
		$url_inline	= $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=purchaseOptionsInline&amp;image_id=$this->image_id";

		// Set the default value
		if ($cfg['set']['cart_href'] == "popup") {
		
			$url_default = $url_popup;
		
		} else {
		
			$url_default = $url_inline;
		
		}

		$cart_add['url_default']		= $url_default;
		$cart_add['url_popup']			= $url_popup;
		$cart_add['url_inline']			= $url_inline;
		$cart_add['url_inline_pop']		= $url_inline_pop;
	
		return $cart_add;
	
	}
	
	/*
	*	Return the URL of the add to lightbox link
	*	
	*	This function is duplicated in class.PixariaMultiImage.php
	*/
	function getImageLightboxURL () {

		global $cfg, $ses;
		
		if (!$ses['psg_userid']) { // The user isn't logged in
		
			// URL for creating a popup window to add to cart
			$url_popup			= $cfg['sys']['base_url'] . $cfg['fil']['index_lightbox'] . "?cmd=addImageToLightbox&amp;image_id=$this->image_id";
			
			// URL for adding to lightbox if already in a popup window
			$url_inline_pop		= $cfg['sys']['base_url'] . $cfg['fil']['index_lightbox'] . "?cmd=addImageToLightbox&amp;image_id=$this->image_id";
			
			// URL for current window
			$url_inline			= $cfg['sys']['base_url'] . $cfg['fil']['index_lightbox'] . "?cmd=addImageToLightbox&amp;image_id=$this->image_id";
		
		} else { // The user is logged in
		
			// URL for creating a popup window to add to lightbox
			$url_popup			= "javascript:openPop('" . $cfg['sys']['base_url'] . $cfg['fil']['index_lightbox'] . "?cmd=addImageToLightbox&amp;image_id=$this->image_id','" . $cfg['set']['lbox_popup_width'] . "','" . $cfg['set']['lbox_popup_width'] . "','yes','yes','no','no','yes','no','no');";
			
			// URL for adding to lightbox if already in a popup window
			$url_inline_pop		= $cfg['sys']['base_url'] . $cfg['fil']['index_lightbox'] . "?cmd=addImageToLightbox&amp;image_id=$this->image_id";
			
			// URL for current window
			$url_inline			= $cfg['sys']['base_url'] . $cfg['fil']['index_lightbox'] . "?cmd=addImageToLightbox&amp;image_id=$this->image_id";
		
		}
		
		// Set the default value
		if ($cfg['set']['lightbox_href'] == "popup") {
		
			$url_default = $url_popup;
		
		} else {
		
			$url_default = $url_inline;
		
		}

		$lightbox_add['url_default']		= $url_default;
		$lightbox_add['url_popup']			= $url_popup;
		$lightbox_add['url_inline']			= $url_inline;
		$lightbox_add['url_inline_pop']		= $url_inline_pop;
	
		return $lightbox_add;
	
	}

	/*
	*	Return the URL of the remove from lightbox link
	*	
	*	This function is duplicated in class.PixariaMultiImage.php
	*/
	function getImageLightboxRemoveURL () {
	
		global $cfg, $ses;
		
		return $cfg['sys']['base_url'] . $cfg['fil']['index_lightbox'] . "?cmd=removeImageFromLightbox&amp;image_id=$this->image_id";
		
	}
	
	/*
	*	Return the URL of the image edit page
	*/
	function getImageAdminEditURL ($return) {
	
		global $cfg, $ses;
		
		if ($return) {
			return $cfg['sys']['base_url'] . $cfg['fil']['admin_image'] . "?cmd=edit&amp;image_id=$this->image_id&amp;return=1";
		} else {
			return $cfg['sys']['base_url'] . $cfg['fil']['admin_image'] . "?cmd=edit&amp;image_id=$this->image_id";
		}
	}
	
	/*
	*	
	*	Courtesy of Chris Sprucefield cs@pix.bz
	*	Released to Pixaria under the BSD License
	*
	*	Return the image weight
	*/
	function getImageWeight () {

		$iw = floor($this->getImageWidth() * $this->getImageHeight() * 3 / (1024 * 1024) );
		if ($iw < 6):   $iwe = "1 Mb"; endif;
		if ($iw >= 6):  $iwe = "12 Mb"; endif;
		if ($iw >= 19): $iwe = "26 Mb"; endif;
		if ($iw >= 38): $iwe = "50 Mb"; endif;
		if ($iw >  60): $iwe = "> 50 Mb"; endif;
		
		return $iwe;
		
	}

	/*
	*	Courtesy of Chris Sprucefield cs@pix.bz
	*	Released to Pixaria under the BSD License
	*
	*	Return the image maximum print size
	*/
	function getImageMaxPrintSize () {

		$pr = 150 ;     // Dpi as minimum print reference..
		$long = max($this->getImageWidth(),$this->getImageHeight()) / $pr ;
		
		if ($long < 4):   $ip = "A7 @ ".$pr."dpi (74 * 105)  (2 7/8 * 4 1/8)"; endif;
		if ($long >= 4):  $ip = "A6 @ ".$pr."dpi (105 * 148) (4 1/8 * 5 7/8)"; endif;
		if ($long >= 8):  $ip = "A5 @ ".$pr."dpi (148 * 210) (5 7/8 * 8 1/4)"; endif;
		if ($long >= 11): $ip = "A4 @ ".$pr."dpi (210 * 297) (8 1/4 * 11 3/4)"; endif;
		if ($long >= 16): $ip = "A3 @ ".$pr."dpi (297 * 420) (11 3/4 * 16 1/2)"; endif;
		if ($long >= 23): $ip = "A2 @ ".$pr."dpi (420 * 594) (16 1/2 * 23 3/8)"; endif;
		if ($long >= 33): $ip = "A1 @ ".$pr."dpi (594 * 841) (23 3/8 * 33)"; endif;
		if ($long >= 46): $ip = "A0 @ ".$pr."dpi (841 * 1189) (33 * 46 3/4) or bigger"; endif;
		
		return $ip;
		
	}
	
	/*
	*	Return the nicely formatted file size of an image
	*/
	function getImageFileSize ($type) {
		$path = $cfg['sys']['base_library'].$this->image_path."/".$type."/".$this->image_filename;
		if (file_exists($path)) {
			$file_size = array_reduce (
				array (" B", " KB", " MB"), create_function (
					'$a,$b', 'return is_numeric($a)?($a>=1024?$a/1024:number_format($a,2).$b):$a;'
				), filesize ($path)
			);
			return $file_size;
		} else {
			return false;
		}
	}
	
	/*
	*	Return the image_basename
	*/
	function getImageBaseName () {
	
		return $this->image_basename;
	
	}

	/*
	*	Return the image_extension
	*/
	function getImageExtension () {
	
		return $this->image_extension;
	
	}
	
	/*
	*	Return the image_product_array
	*/
	function getImageProducts () {
	
		return $this->image_product_array;
		
	}
	
	/*
	*	Set the value of image_path
	*/
	function setImagePath ($image_path) {
	
		$this->image_path 	= $image_path;
		$this->_update_flag = (bool)true;
	
	}
		
	/*
	*	Set the value of image_active
	*/
	function setImageActive ($image_active) {
		
		if ($image_active == "on" || $image_active) {
		
			$this->image_active = "1";
			$this->_update_flag = (bool)true;
			
		} else {
		
			$this->image_active 	= "0";
			$this->_update_flag = (bool)true;
		
		}
		
	}
		
	/*
	*	Set the value of image_userid
	*/
	function setImageUserId ($image_userid) {
	
		if (!is_numeric($image_userid)) {
			return;
		}
		
		$this->image_userid 	= $image_userid;
		$this->_update_flag 	= (bool)true;
	
	}
	
	/*
	*	Set the value of image_filename
	*/
	function setImageFileName ($image_filename) {
	
		$this->image_filename 	= $image_filename;
		$this->_update_flag 	= (bool)true;
	
	}
	
	/*
	*	Set the value of image_title
	*/
	function setImageTitle ($image_title) {
	
		$this->image_title 	= $image_title;
		$this->_update_flag = (bool)true;
	
	}
	
	/*
	*	Set the value of image_caption
	*/
	function setImageCaption ($image_caption) {
	
		$this->image_caption 	= $image_caption;
		$this->_update_flag		= (bool)true;
	
	}
	
	/*
	*	Set the value of image_filetype
	*/
	function setImageFileType ($image_filetype) {
	
		$this->image_filetype 	= $image_filetype;
		$this->_update_flag 	= (bool)true;
	
	}
	
	/*
	*	Set the value of image_keywords
	*/
	function setImageKeywords ($image_keywords, $append="") {
		
		if (is_array($image_keywords)) {
			$image_keywords = implode(', ', $image_keywords);
		}
		
		// Remove duplicate keywords
		$keywords 		= explode(", ",$image_keywords);
		$image_keywords = implode(", ",array_unique($keywords));
		
		if ($append) {
		
			if (trim($this->image_keywords) == '') {
			
				$this->image_keywords = keywordsEncode($image_keywords);
				
			} else {
			
				$this->image_keywords 	= trim($this->image_keywords) . ", " . keywordsEncode($image_keywords);
				
			}
			
			$this->_update_flag 	= (bool)true;
			
		} else {
		
			$this->image_keywords 	= keywordsEncode($image_keywords);
			$this->_update_flag 	= (bool)true;
			
		}
		
	}
	
	/*
	*	Set the value of image_width
	*/
	function setImageWidth ($image_width) {
	
		if (!is_numeric($image_width)) {
			return;
		}
		
		$this->image_width 	= $image_width;
		$this->_update_flag = (bool)true;
	
	}
	
	/*
	*	Set the value of image_height
	*/
	function setImageHeight ($image_height) {
	
		if (!is_numeric($image_height)) {
			return;
		}
		
		$this->image_height 	= $image_height;
		$this->_update_flag 	= (bool)true;
	
	}
	
	/*
	*	Set the value of image_copyright
	*/
	function setImageCopyright ($image_copyright) {
	
		$this->image_copyright 	= $image_copyright;
		$this->_update_flag 	= (bool)true;
	
	}
	
	/*
	*	Set the value of image_headline
	*/
	function setImageHeadline ($image_headline) {
	
		$this->image_headline 	= $image_headline;
		$this->_update_flag 	= (bool)true;
	
	}
	
	/*
	*	Set the value of iptc_creator
	*/
	function setIPTCCreator ($iptc_creator) {
	
		$this->iptc_creator = $iptc_creator;
		$this->_update_flag = (bool)true;
	
	}
	
	/*
	*	Set the value of iptc_jobtitle
	*/
	function setIPTCJobTitle ($iptc_jobtitle) {
	
		$this->iptc_jobtitle = $iptc_jobtitle;
		$this->_update_flag  = (bool)true;
	
	}
	
	/*
	*	Set the value of iptc_city
	*/
	function setIPTCCity ($iptc_city) {
	
		$this->iptc_city 	= $iptc_city;
		$this->_update_flag = (bool)true;
	
	}
	
	/*
	*	Set the value of iptc_country
	*/
	function setIPTCCountry ($iptc_country) {
	
		$this->iptc_country = $iptc_country;
		$this->_update_flag = (bool)true;
	
	}
	
	/*
	*	Set the value of iptc_credit
	*/
	function setIPTCCredit ($iptc_credit) {
	
		$this->iptc_credit 	= $iptc_credit;
		$this->_update_flag = (bool)true;
	
	}
	
	/*
	*	Set the value of iptc_source
	*/
	function setIPTCSource ($iptc_source) {
	
		$this->iptc_source 	= $iptc_source;
		$this->_update_flag = (bool)true;
	
	}
	
	/*
	*	Set the value of iptc_object
	*/
	function setIPTCObject ($iptc_object) {
	
		$this->iptc_object 	= $iptc_object;
		$this->_update_flag 	= (bool)true;
	
	}
	
	/*
	*	Set the value of iptc_byline
	*/
	function setIPTCByline ($iptc_byline) {
	
		$this->iptc_byline 	= $iptc_byline;
		$this->_update_flag = (bool)true;
	
	}
	
	/*
	*	Set the value of gps_longitude
	*/
	function setGpsLongitude ($gps_longitude) {
	
		$this->gps_longitude 	= $gps_longitude;
		$this->_update_flag 	= (bool)true;
	
	}
	
	/*
	*	Set the value of gps_latitude
	*/
	function setGpsLatitude ($gps_latitude) {
	
		$this->gps_latitude = $gps_latitude;
		$this->_update_flag = (bool)true;
	
	}
	
	/*
	*	Set the value of image_date
	*/
	function setImageMySQLDate ($date_string) {
	
		$this->image_date 	= $date_string;
		$this->_update_flag = (bool)true;
	
	}
	
	/*
	*	Set the value of image_date
	*/
	function setImageDate ($image_yy,$image_mo,$image_dd,$image_hh,$image_mi,$image_ss) {
		
		if (func_num_args() != 6) {
			return;
		}
		
		$image_yy = $image_yy;
		
		$image_mo = str_pad($image_mo,2,"00",STR_PAD_LEFT);
		$image_dd = str_pad($image_dd,2,"00",STR_PAD_LEFT);
		$image_hh = str_pad($image_hh,2,"00",STR_PAD_LEFT);
		$image_mi = str_pad($image_mi,2,"00",STR_PAD_LEFT);
		$image_ss = str_pad($image_ss,2,"00",STR_PAD_LEFT);
		
		$image_date			= "$image_yy-$image_mo-$image_dd $image_hh:$image_mi:$image_ss";

		$this->image_date 	= $image_date;
		$this->_update_flag = (bool)true;
	
	}
	
	/*
	*	Set the value of image_permissions
	*/
	function setImagePermissions ($image_permissions) {
	
		if (!is_numeric($image_permissions)) {
			return;
		}
		
		$this->image_permissions 	= $image_permissions;
		$this->_update_flag 		= (bool)true;
	
	}
	
	/*
	*	Set the value of image_rights_type
	*/
	function setImageRightsType ($image_rights_type) {
	
		if (!is_numeric($image_rights_type)) {
			return;
		}
		
		$this->image_rights_type 	= $image_rights_type;
		$this->_update_flag 		= (bool)true;
	
	}
	
	/*
	*	Set the value of image_rights_text
	*/
	function setImageRightsText ($image_rights_text) {
	
		$this->image_rights_text 	= $image_rights_text;
		$this->_update_flag 		= (bool)true;
	
	}
	
	/*
	*	Set the value of image_colr_enable
	*/
	function setImageColrEnable ($image_colr_enable) {
	
		if ($image_colr_enable == "on" || $image_colr_enable == "1") {
		
			$this->image_colr_enable 	= "1";
			$this->_update_flag 		= (bool)true;
		
		} else {
		
			$this->image_colr_enable 	= "0";
			$this->_update_flag 		= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of image_colr_r
	*/
	function setImageColrR ($image_colr_r) {
	
		if (!is_numeric($image_colr_r)) {
			return;
		}
		
		$this->image_colr_r = $image_colr_r;
		$this->_update_flag = (bool)true;
	
	}
	
	/*
	*	Set the value of image_colr_g
	*/
	function setImageColrG ($image_colr_g) {
	
		if (!is_numeric($image_colr_g)) {
			return;
		}
		
		$this->image_colr_g = $image_colr_g;
		$this->_update_flag = (bool)true;
	
	}
	
	/*
	*	Set the value of image_colr_b
	*/
	function setImageColrB ($image_colr_b) {
	
		if (!is_numeric($image_colr_b)) {
			return;
		}
		
		$this->image_colr_b = $image_colr_b;
		$this->_update_flag	= (bool)true;
	
	}
	
	/*
	*	Set the value of image_price
	*/
	function setImagePrice ($image_price) {
		
		if (!is_numeric($image_price)) {
			return;
		}
		
		$this->image_price 	= $image_price;
		$this->_update_flag = (bool)true;
		
	}
	
	/*
	*	Set the value of image_sale
	*/
	function setImageSale ($image_sale) {
		
		if ($image_sale == "on" || $image_sale == "1") {
		
			$this->image_sale 	= "1";
			$this->_update_flag = (bool)true;
		
		} else {
		
			$this->image_sale 	= "0";
			$this->_update_flag = (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of image_product_link
	*/
	function setImageProductLink ($image_product_link) {
		
		if ($image_product_link == "1") {
		
			$this->image_product_link 	= "1";
			$this->_update_flag 		= (bool)true;
		
		} else {
		
			$this->image_product_link 	= "0";
			$this->_update_flag 		= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of image_synd
	*/
	function setImageSyndication ($image_synd) {
	
		if ($image_synd == "on" || $image_synd == "1") {
		
			$this->image_synd 	= "1";
			$this->_update_flag = (bool)true;
		
		} else {
		
			$this->image_synd 	= "0";
			$this->_update_flag = (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of image_synd_url
	*/
	function setImageSyndicationURL ($image_synd_url) {
	
		$this->image_synd_url 	= $image_synd_url;
		$this->_update_flag 	= (bool)true;
	
	}
	
	/*
	*	Set the value of image_model_release
	*/
	function setImageModelRelease ($image_model_release) {
	
		if ($image_model_release == "on" || $image_model_release == "1") {
		
			$this->image_model_release 	= "1";
			$this->_update_flag 		= (bool)true;
		
		} else {
		
			$this->image_model_release 	= "0";
			$this->_update_flag 		= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of image_property_release
	*/
	function setImagePropertyRelease ($image_property_release) {
	
		if ($image_property_release == "on" || $image_property_release == "1") {
		
			$this->image_property_release 	= "1";
			$this->_update_flag 			= (bool)true;
		
		} else {
		
			$this->image_property_release 	= "0";
			$this->_update_flag 			= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of image_trashed
	*/
	function setImageTrashed ($image_trashed) {
	
		$this->image_trashed 	= $image_trashed;
		$this->_update_flag 	= (bool)true;
	
	}
	
	/*
	*	Set the value of image_content_type
	*/
	function setImageContentType ($image_content_type) {
		
		switch ($image_content_type) {
		
			case "creative": case "10":
				$this->image_content_type 	= "10";
			break;
		
			case "editorial": case "11":
				$this->image_content_type 	= "11";
			break;
		
			case "publicity": case "12":
				$this->image_content_type 	= "12";
			break;
		
			case "sport": case "13":
				$this->image_content_type 	= "13";
			break;
			
			default:
				$this->image_content_type 	= "10";
			break;
			
		}
		
		$this->_update_flag 		= (bool)true;
	
	}
	
	/*
	*	Set the value of image_extra_01
	*/
	function setImageExtra1 ($image_extra_01) {
	
		$this->image_extra_01 	= $image_extra_01;
		$this->_update_flag 	= (bool)true;
	
	}
	
	/*
	*	Set the value of image_extra_02
	*/
	function setImageExtra2 ($image_extra_02) {
	
		$this->image_extra_02 	= $image_extra_02;
		$this->_update_flag 	= (bool)true;
	
	}
	
	/*
	*	Set the value of image_extra_03
	*/
	function setImageExtra3 ($image_extra_03) {
	
		$this->image_extra_03 	= $image_extra_03;
		$this->_update_flag 	= (bool)true;
	
	}
	
	/*
	*	Set the value of image_extra_04
	*/
	function setImageExtra4 ($image_extra_04) {
	
		$this->image_extra_04 	= $image_extra_04;
		$this->_update_flag 	= (bool)true;
	
	}
	
	/*
	*	Set the value of image_extra_05
	*/
	function setImageExtra5 ($image_extra_05) {
	
		$this->image_extra_05 	= $image_extra_05;
		$this->_update_flag 	= (bool)true;
	
	}

	/*
	*	Set the value of icon_w
	*/
	function setIconWidth ($icon_w) {
	
		$this->icon_w 			= $icon_w;
		$this->_update_flag 	= (bool)true;
	
	}

	/*
	*	Set the value of icon_h
	*/
	function setIconHeight ($icon_h) {
	
		$this->icon_h 			= $icon_h;
		$this->_update_flag 	= (bool)true;
	
	}

	/*
	*	Set the value of smal_w
	*/
	function setSmallWidth ($smal_w) {
	
		$this->smal_w 			= $smal_w;
		$this->_update_flag 	= (bool)true;
	
	}

	/*
	*	Set the value of smal_h
	*/
	function setSmallHeight ($smal_h) {
	
		$this->smal_h 			= $smal_h;
		$this->_update_flag 	= (bool)true;
	
	}

	/*
	*	Set the value of larg_w
	*/
	function setLargeWidth ($larg_w) {
	
		$this->larg_w 			= $larg_w;
		$this->_update_flag 	= (bool)true;
	
	}

	/*
	*	Set the value of larg_h
	*/
	function setLargeHeight ($larg_h) {
	
		$this->larg_h 			= $larg_h;
		$this->_update_flag 	= (bool)true;
	
	}

	/*
	*	Set the value of scom_w
	*/
	function setSmallCompingWidth ($scom_w) {
	
		$this->scom_w 			= $scom_w;
		$this->_update_flag 	= (bool)true;
	
	}

	/*
	*	Set the value of scom_h
	*/
	function setSmallCompingHeight ($scom_h) {
	
		$this->scom_h 			= $scom_h;
		$this->_update_flag 	= (bool)true;
	
	}

	/*
	*	Set the value of comp_w
	*/
	function setCompingWidth ($comp_w) {
	
		$this->comp_w 			= $comp_w;
		$this->_update_flag 	= (bool)true;
	
	}

	/*
	*	Set the value of comp_h
	*/
	function setCompingHeight ($comp_h) {
	
		$this->comp_h 			= $comp_h;
		$this->_update_flag 	= (bool)true;
	
	}

	/*
	*	Set the value of orig_w
	*/
	function setOriginalWidth ($orig_w) {
	
		$this->orig_w 			= $orig_w;
		$this->_update_flag 	= (bool)true;
	
	}

	/*
	*	Set the value of orig_h
	*/
	function setOriginalHeight ($orig_h) {
	
		$this->orig_h 			= $orig_h;
		$this->_update_flag 	= (bool)true;
	
	}

	/*
	*	Set the value of image_viewers
	*/
	function setImageViewers ($image_viewers) {
	
		$this->image_viewers 	= $image_viewers;
		$this->_update_viewers 	= (bool)true;
	
	}

	/*
	*	Set the value of image_products
	*/
	function setImageProducts ($image_product_array) {
		
		if (is_array($image_product_array)) {
			$this->image_product_array 	= $image_product_array;
			$this->_update_products 	= (bool)true;
		} else {
			return false;
		}
	}
	
	/*
	*	
	*	Create a new image in the database
	*	
	*	null createImage(void)
	*	
	*/
	function createImage () {
	
		// Load global variables
		global $cfg;
		
		// If there are no properties to change or there is no image ID, don't run the update
		if (!$this->_update_flag) {
			return;
		}

		// Load image properties for inclusion in the database and escape dangerous items
		$image_active			= $this->image_active;
		$image_path				= $this->image_path;
		$image_userid			= $this->image_userid;
		$image_filename			= $this->image_filename;
		$image_title			= $this->image_title;
		$image_caption			= $this->image_caption;
		$image_keywords			= $this->image_keywords;
		$image_width			= $this->image_width;
		$image_height			= $this->image_height;
		$image_copyright		= $this->image_copyright;
		$image_date				= $this->image_date;
		$image_permissions		= $this->image_permissions;
		$image_rights_type		= $this->image_rights_type;
		$image_rights_text		= $this->image_rights_text;
		$image_colr_enable		= $this->image_colr_enable;
		$image_colr_r			= $this->image_colr_r;
		$image_colr_g			= $this->image_colr_g;
		$image_colr_b			= $this->image_colr_b;
		$image_price			= $this->image_price;
		$image_sale				= $this->image_sale;
		$image_product_link		= $this->image_product_link;
		$image_model_release	= $this->image_model_release;
		$image_property_release	= $this->image_property_release;
		$image_extra_01			= $this->image_extra_01;
		$image_content_type		= $this->image_content_type;
		$image_extra_02			= $this->image_extra_02;
		$image_extra_03			= $this->image_extra_03;
		$image_extra_04			= $this->image_extra_04;
		$image_extra_05			= $this->image_extra_05;		
		$icon_w					= $this->icon_w;		
		$icon_h					= $this->icon_h;		
		$smal_w					= $this->smal_w;		
		$smal_h					= $this->smal_h;		
		$larg_w					= $this->larg_w;		
		$larg_h					= $this->larg_h;		
		$scom_w					= $this->scom_w;		
		$scom_h					= $this->scom_h;		
		$comp_w					= $this->comp_w;		
		$comp_h					= $this->comp_h;		
		$orig_w					= $this->orig_w;		
		$orig_h					= $this->orig_h;		
		$iptc_creator			= $this->iptc_creator;		
		$iptc_jobtitle			= $this->iptc_jobtitle;		
		$iptc_city				= $this->iptc_city;		
		$iptc_country			= $this->iptc_country;		
		$iptc_credit			= $this->iptc_credit;		
		$iptc_source			= $this->iptc_source;		
		$iptc_object			= $this->iptc_object;		
		$iptc_byline			= $this->iptc_byline;		
		$gps_longitude			= $this->gps_longitude;		
		$gps_latitude			= $this->gps_latitude;		
	
		$sql = "INSERT INTO `".PIX_TABLE_IMGS."` VALUES 	(
															'0',
															'".$this->_dbl->escape($image_active)."',
															'".$this->_dbl->escape($image_path)."',
															'".$this->_dbl->escape($image_userid)."',
															'".$this->_dbl->escape($image_category_id)."',
															'".$this->_dbl->escape($image_filename)."',
															'".$this->_dbl->escape($image_title)."',
															'".$this->_dbl->escape($image_caption)."',
															'image/jpeg',
															'".$this->_dbl->escape($image_keywords)."',
															'".$this->_dbl->escape($image_width)."',
															'".$this->_dbl->escape($image_height)."',
															'".$this->_dbl->escape($image_copyright)."',
															'".$this->_dbl->escape($image_headline)."',
															'".$this->_dbl->escape($image_date)."',
															'".$this->_dbl->escape($image_permissions)."',
															'".$this->_dbl->escape($image_rights_type)."',
															'".$this->_dbl->escape($image_rights_text)."',
															'".$this->_dbl->escape($image_colr_enable)."',
															'".$this->_dbl->escape($image_colr_r)."',
															'".$this->_dbl->escape($image_colr_g)."',
															'".$this->_dbl->escape($image_colr_b)."',
															'".$this->_dbl->escape($image_price)."',
															'".$this->_dbl->escape($image_sale)."',
															'".$this->_dbl->escape($image_product_link)."',
															'0',
															'',
															'',
															'".$this->_dbl->escape($image_model_release)."',
															'".$this->_dbl->escape($image_property_release)."',
															'0',
															'0',
															'".$this->_dbl->escape($image_content_type)."',
															'".$this->_dbl->escape($image_extra_01)."',
															'".$this->_dbl->escape($image_extra_02)."',
															'".$this->_dbl->escape($image_extra_03)."',
															'".$this->_dbl->escape($image_extra_04)."',
															'".$this->_dbl->escape($image_extra_05)."',
															'".$this->_dbl->escape($icon_w)."',
															'".$this->_dbl->escape($icon_h)."',
															'".$this->_dbl->escape($smal_w)."',
															'".$this->_dbl->escape($smal_h)."',
															'".$this->_dbl->escape($larg_w)."',
															'".$this->_dbl->escape($larg_h)."',
															'".$this->_dbl->escape($scom_w)."',
															'".$this->_dbl->escape($scom_h)."',
															'".$this->_dbl->escape($comp_w)."',
															'".$this->_dbl->escape($comp_h)."',
															'".$this->_dbl->escape($orig_w)."',
															'".$this->_dbl->escape($orig_h)."',
															'".$this->_dbl->escape($iptc_creator)."',
															'".$this->_dbl->escape($iptc_jobtitle)."',
															'".$this->_dbl->escape($iptc_city)."',
															'".$this->_dbl->escape($iptc_country)."',
															'".$this->_dbl->escape($iptc_credit)."',
															'".$this->_dbl->escape($iptc_source)."',
															'".$this->_dbl->escape($iptc_object)."',
															'".$this->_dbl->escape($iptc_byline)."',
															'".$this->_dbl->escape($gps_longitude)."',
															'".$this->_dbl->escape($gps_latitude)."'
															);";
		
		
		// Add the image to the library
		$this->_dbl->sqlQuery($sql);
		
		// Get the new image_id
		$this->image_id = $this->_dbl->sqlInsertId();
		
		// Update the groups who can view this image
		if ($this->_update_viewers) { $this->updateViewers($this->image_id); }
		
		// Update the products linked to this image
		if ($this->_update_products) { $this->updateProducts($this->image_id); }
		
	}
	
	/*
	*	
	*	Update the details of a single image overwriting blank entries
	*	
	*	null updateImage(void)
	*	
	*/
	function updateImage () {
		
		// Load global variables
		global $cfg;
		
		// If there are no properties to change or there is no image ID, don't run the update
		if (!$this->_update_flag || $this->image_id == "") {
			return;
		}
		
		// Load image properties for inclusion in the database and escape dangerous items
		$image_id				= $this->image_id;
		$image_active			= $this->image_active;
		$image_userid			= $this->image_userid;
		$image_title			= $this->image_title;
		$image_caption			= $this->image_caption;
		$image_keywords			= $this->image_keywords;
		$image_width			= $this->image_width;
		$image_height			= $this->image_height;
		$image_copyright		= $this->image_copyright;
		$image_date				= $this->image_date;
		$image_permissions		= $this->image_permissions;
		$image_rights_type		= $this->image_rights_type;
		$image_rights_text		= $this->image_rights_text;
		$image_colr_enable		= $this->image_colr_enable;
		$image_colr_r			= $this->image_colr_r;
		$image_colr_g			= $this->image_colr_g;
		$image_colr_b			= $this->image_colr_b;
		$image_price			= $this->image_price;
		$image_sale				= $this->image_sale;
		$image_product_link		= $this->image_product_link;
		$image_model_release	= $this->image_model_release;
		$image_property_release	= $this->image_property_release;
		$image_trashed			= $this->image_trashed;
		$image_content_type		= $this->image_content_type;
		$image_extra_01			= $this->image_extra_01;
		$image_extra_02			= $this->image_extra_02;
		$image_extra_03			= $this->image_extra_03;
		$image_extra_04			= $this->image_extra_04;
		$image_extra_05			= $this->image_extra_05;
		$iptc_creator			= $this->iptc_creator;		
		$iptc_jobtitle			= $this->iptc_jobtitle;		
		$iptc_city				= $this->iptc_city;		
		$iptc_country			= $this->iptc_country;		
		$iptc_credit			= $this->iptc_credit;		
		$iptc_source			= $this->iptc_source;		
		$iptc_object			= $this->iptc_object;		
		$iptc_byline			= $this->iptc_byline;		
		$gps_longitude			= $this->gps_longitude;		
		$gps_latitude			= $this->gps_latitude;		
		
		// Construct a SQL statement for updating this image
		$sql = "UPDATE ". PIX_TABLE_IMGS ." SET 	
								
											image_active			= '".$this->_dbl->escape($image_active)."',
											image_userid			= '".$this->_dbl->escape($image_userid)."',
											image_title				= '".$this->_dbl->escape($image_title)."',
											image_caption			= '".$this->_dbl->escape($image_caption)."',
											image_keywords			= '".$this->_dbl->escape($image_keywords)."',
											image_width				= '".$this->_dbl->escape($image_width)."',
											image_height			= '".$this->_dbl->escape($image_height)."',
											image_copyright			= '".$this->_dbl->escape($image_copyright)."',
											image_date				= '".$this->_dbl->escape($image_date)."',
											image_permissions		= '".$this->_dbl->escape($image_permissions)."',
											image_rights_type		= '".$this->_dbl->escape($image_rights_type)."',
											image_rights_text		= '".$this->_dbl->escape($image_rights_text)."',
											image_colr_enable		= '".$this->_dbl->escape($image_colr_enable)."',
											image_colr_r			= '".$this->_dbl->escape($image_colr_r)."',
											image_colr_g			= '".$this->_dbl->escape($image_colr_g)."',
											image_colr_b			= '".$this->_dbl->escape($image_colr_b)."',
											image_price				= '".$this->_dbl->escape($image_price)."',
											image_sale				= '".$this->_dbl->escape($image_sale)."',
											image_product_link		= '".$this->_dbl->escape($image_product_link)."',
											image_model_release		= '".$this->_dbl->escape($image_model_release)."',
											image_property_release	= '".$this->_dbl->escape($image_property_release)."',
											image_trashed			= '".$this->_dbl->escape($image_trashed)."',
											image_content_type		= '".$this->_dbl->escape($image_content_type)."',
											image_extra_01			= '".$this->_dbl->escape($image_extra_01)."',
											image_extra_02			= '".$this->_dbl->escape($image_extra_02)."',
											image_extra_03			= '".$this->_dbl->escape($image_extra_03)."',
											image_extra_04			= '".$this->_dbl->escape($image_extra_04)."',
											image_extra_05			= '".$this->_dbl->escape($image_extra_05)."',
											iptc_creator			= '".$this->_dbl->escape($iptc_creator)."',
											iptc_jobtitle			= '".$this->_dbl->escape($iptc_jobtitle)."',
											iptc_city				= '".$this->_dbl->escape($iptc_city)."',
											iptc_country			= '".$this->_dbl->escape($iptc_country)."',
											iptc_credit				= '".$this->_dbl->escape($iptc_credit)."',
											iptc_source				= '".$this->_dbl->escape($iptc_source)."',
											iptc_object				= '".$this->_dbl->escape($iptc_object)."',
											iptc_byline				= '".$this->_dbl->escape($iptc_byline)."',
											gps_longitude			= '".$this->_dbl->escape($gps_longitude)."',
											gps_latitude			= '".$this->_dbl->escape($gps_latitude)."'
					
											WHERE image_id = '$image_id'";
		
		// Pass the SQL command to the database object
		$this->_dbl->sqlQuery($sql);
		
		// Update the groups who can view this image
		if ($this->_update_viewers) { $this->updateViewers($this->image_id); }
		
		// Update the products linked to this image
		if ($this->_update_products) { $this->updateProducts($this->image_id); }
		
	}
	
	/*
	*	
	*	Update the user groups who can view this image
	*	
	*	mixed updateViewers(int image_id)
	*	
	*/
	function updateViewers ($image_id) {
		
		global $ses, $cfg;
		
		if (!$this->_update_viewers) {
			return;
		}
		
		$image_viewers = $this->image_viewers;
		
		if (is_array($image_viewers) && count($image_viewers) > 0) { // There are groups to link this image to
			
			$this->_dbl->sqlQuery("DELETE FROM ". PIX_TABLE_IMVI ." WHERE image_id = '".$this->_dbl->escape($image_id)."'");
			
			foreach($image_viewers as $key => $value) { // Loop through the groups and add the image_id link to each one
				
				$this->_dbl->sqlQuery("INSERT INTO ". PIX_TABLE_IMVI ." VALUES ('0','".$this->_dbl->escape($image_id)."','".$this->_dbl->escape($value)."');");
				
			}
			
		} else { // This image is not linked to any categories, delete any existing links
		
			$this->_dbl->sqlQuery("DELETE FROM ". PIX_TABLE_IMVI ." WHERE image_id = '".$this->_dbl->escape($image_id)."'");
					
		}
		
	
	}
	
	/*
	*	
	*	Update the products linked to this image
	*	
	*	mixed updateProducts(int image_id)
	*	
	*/
	function updateProducts($image_id) {
	
		global $ses, $cfg;
		
		if (!$this->_update_products) {
			return;
		}
		
		$image_products = $this->image_product_array;

		if (is_array($image_products) && count($image_products) > 0 && $this->image_product_link == "1") { // There are products to link to this image
			
			$this->_dbl->sqlQuery("DELETE FROM ". PIX_TABLE_IPRD ." WHERE image_id = '".$this->_dbl->escape($image_id)."'");
			
			foreach($image_products as $key => $value) { // Loop through the products and add the image_id link to each one
				
				$this->_dbl->sqlQuery("INSERT INTO ". PIX_TABLE_IPRD ." VALUES ('0','".$this->_dbl->escape($image_id)."','".$this->_dbl->escape($value)."')");
				
			}
			
		} else { // This image is not linked to any categories, delete any existing links
		
			$this->_dbl->sqlQuery("DELETE FROM ". PIX_TABLE_IPRD ." WHERE image_id = '".$this->_dbl->escape($image_id)."'");
					
		}
		
	}
		
	/*
	*
	*	Get the size of an image from the image path
	*
	*/
	function getImageDimensions($width, $height) {
		
		if (is_numeric($width) && is_numeric($height) && $width >= 1 && $height >= 1) {
		
			$result[0]	= $width;									// Width in pixels
			$result[1]	= $height;									// Height in pixels
			$result[2]	= "width=\"$width\" height=\"$height\"";	// HTML width and height attributes
			$result[3]	= $width / $height;							// The aspect ratio of the image
		
			return $result;
		
		} else {
		
			return false;
		
		}
		
	}
	
	function generateDownloadURL($path, $mime_type, $image_id) {
		
		global $cfg;
		
		$path_parts = pathinfo($path);
		
		$directory 	= $path_parts['dirname'];
		$filename	= substr($path_parts['basename'],0,-4);
		
		switch ($mime_type) {
		
			case "JPG":
				
				if (file_exists("$directory/$filename.jpg")) {
					$url = $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=downloadImageFree&amp;image_id=$image_id&amp;extension=jpg";
				} elseif (file_exists("$directory/$filename.JPG")) {
					$url = $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=downloadImageFree&amp;image_id=$image_id&amp;extension=JPG";
				} else {
					$url = (bool)FALSE;
				}
				
			break;
		
			case "JP2":
				
				if (file_exists("$directory/$filename.jp2")) {
					$url = $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=downloadImageFree&amp;image_id=$image_id&amp;extension=jp2";
				} elseif (file_exists("$directory/$filename.JP2")) {
					$url = $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=downloadImageFree&amp;image_id=$image_id&amp;extension=JP2";
				} else {
					$url = (bool)FALSE;
				}
				
			break;
		
			case "TIF":
				
				if (file_exists("$directory/$filename.tif")) {
					$url = $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=downloadImageFree&amp;image_id=$image_id&amp;extension=tif";
				} elseif (file_exists("$directory/$filename.TIF")) {
					$url = $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=downloadImageFree&amp;image_id=$image_id&amp;extension=TIF";
				} else {
					$url = (bool)FALSE;
				}
				
			break;
		
			case "PSD":
				
				if (file_exists("$directory/$filename.psd")) {
					$url = $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=downloadImageFree&amp;image_id=$image_id&amp;extension=psd";
				} elseif (file_exists("$directory/$filename.PSD")) {
					$url = $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=downloadImageFree&amp;image_id=$image_id&amp;extension=PSD";
				} else {
					$url = (bool)FALSE;
				}
				
			break;
		
			case "EPS":
				
				if (file_exists("$directory/$filename.eps")) {
					$url = $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=downloadImageFree&amp;image_id=$image_id&amp;extension=eps";
				} elseif (file_exists("$directory/$filename.EPS")) {
					$url = $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=downloadImageFree&amp;image_id=$image_id&amp;extension=EPS";
				} else {
					$url = (bool)FALSE;
				}
				
			break;
		
		}
		
		return $url;
		
	}
	
	/*
	*
	*	This method increments the view counter for this gallery by one
	*
	*/
	function updateImageViewCounter () {
		
		global $ses, $cfg;
		
		if (!$ses['psg_administrator']) { // Only update counter if user is not an admin
		
			// Update the number of times this gallery's been viewed
			$this->_dbl->sqlQuery("UPDATE `".PIX_TABLE_IMGS."` SET image_counter = image_counter + 1 WHERE image_id = '".$this->_dbl->escape($this->image_id)."';");
		
		}
			
	}
	
	/*
	*
	*	This method records the action of a user downloading an image
	*
	*/
	function logImageDownload($type, $transaction) {
	
		global $ses;
		
		$userid = $ses['psg_userid'];
		
		if ($userid != "") {
		
			// Insert entry in the image download log
			$this->_dbl->sqlQuery("INSERT INTO ".PIX_TABLE_DLOG." VALUES ('0','".$this->_dbl->escape($this->image_id)."','".$this->_dbl->escape($userid)."',NOW(),'".$this->_dbl->escape($type)."','".$this->_dbl->escape($transaction)."');");
			
		}
	}
	
	/*
	*
	*	This method loads an array of which physical products are linked to this image
	*
	*/
	function loadImageProducts () {
		
		$sql = "SELECT ".PIX_TABLE_PROD.".* FROM ".PIX_TABLE_IPRD."
		
				LEFT JOIN ".PIX_TABLE_PROD." ON ".PIX_TABLE_IPRD.".product_id = ".PIX_TABLE_PROD.".prod_id
				
				WHERE ".PIX_TABLE_IPRD.".image_id = '".$this->_dbl->escape($this->image_id)."'";
		
		$result = $this->_dbl->sqlSelectRows($sql);
		
		if (is_array($result)) {
		
			foreach ($result as $key => $value) {
			
				$this->image_product_array[] = $value['prod_id'];
			
			}
		
		}
		
	}

	
}

?>