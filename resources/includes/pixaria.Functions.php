<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

function pix_fileperms($path) {
	
	// Courtesy http://uk.php.net/
	
	$perms = @fileperms($path);
	
	if (($perms & 0xC000) == 0xC000) {
	   // Socket
	   $info = 's';
	} elseif (($perms & 0xA000) == 0xA000) {
	   // Symbolic Link
	   $info = 'l';
	} elseif (($perms & 0x8000) == 0x8000) {
	   // Regular
	   $info = '-';
	} elseif (($perms & 0x6000) == 0x6000) {
	   // Block special
	   $info = 'b';
	} elseif (($perms & 0x4000) == 0x4000) {
	   // Directory
	   $info = 'd';
	} elseif (($perms & 0x2000) == 0x2000) {
	   // Character special
	   $info = 'c';
	} elseif (($perms & 0x1000) == 0x1000) {
	   // FIFO pipe
	   $info = 'p';
	} else {
	   // Unknown
	   $info = 'u';
	}
	
	// Owner
	$info .= (($perms & 0x0100) ? 'r' : '-');
	$info .= (($perms & 0x0080) ? 'w' : '-');
	$info .= (($perms & 0x0040) ?
	           (($perms & 0x0800) ? 's' : 'x' ) :
	           (($perms & 0x0800) ? 'S' : '-'));
	
	// Group
	$info .= (($perms & 0x0020) ? 'r' : '-');
	$info .= (($perms & 0x0010) ? 'w' : '-');
	$info .= (($perms & 0x0008) ?
	           (($perms & 0x0400) ? 's' : 'x' ) :
	           (($perms & 0x0400) ? 'S' : '-'));
	
	// World
	$info .= (($perms & 0x0004) ? 'r' : '-');
	$info .= (($perms & 0x0002) ? 'w' : '-');
	$info .= (($perms & 0x0001) ?
	           (($perms & 0x0200) ? 't' : 'x' ) :
	           (($perms & 0x0200) ? 'T' : '-'));
	
	return $info;

}

/*
*
*	This function parses an XML data file
*
*/

function pix_xml_tree($file) {
	// read file
	$open_file = fopen($file, "r"); 
	$data = "";
	while ($r=fread($open_file,8192) ) {
		 $data .= $r;
	}

	// create parser
	$parser = xml_parser_create();
	xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,0);
	xml_parser_set_option($parser,XML_OPTION_SKIP_WHITE,1);
	xml_parse_into_struct($parser,$data,$values,$tags);
	xml_parser_free($parser);

	// we store our path here
	$hash_stack = array();
	
	// this is our target
	$ret = array();
	foreach ($values as $key => $val) {
	  
	  switch ($val['type']) {
		  case 'open':
			 array_push($hash_stack, $val['tag']);
			if (isset($val['attributes']))
				$ret = pix_compose_xml_array($ret, $hash_stack, $val['attributes']);
			else
				$ret = pix_compose_xml_array($ret, $hash_stack);
			break;

		  case 'close':
			 array_pop($hash_stack);
			break;

		  case 'complete':
			 array_push($hash_stack, $val['tag']);
			 $ret = pix_compose_xml_array($ret, $hash_stack, $val['value']);
			 array_pop($hash_stack);
			
			 // handle attributes
			 if (isset($val['attributes']))
			{
				while(list($a_k,$a_v) = each($val['attributes']))
				{
				   $hash_stack[] = $val['tag']."_attribute_".$a_k;
				   $ret = pix_compose_xml_array($ret, $hash_stack, $a_v);
				   array_pop($hash_stack);
				}
			}
			
			
			break;
	  }
  }
  
  return $ret;
}

/*
*
*	This function converts XML parser output into an array
*
*/

function &pix_compose_xml_array($array, $elements, $value=array())	{
  global $XML_LIST_ELEMENTS;
  
   // get current element
   $element = array_shift($elements);
  
   // does the current element refer to a list
   if (in_array($element,$XML_LIST_ELEMENTS))
  {
	  // more elements?
	  if(sizeof($elements) > 0)
	  {
		 $array[$element][sizeof($array[$element])-1] = &pix_compose_xml_array($array[$element][sizeof($array[$element])-1], $elements, $value);
	  }
	  else // if (is_array($value))
	  {
		 $array[$element][sizeof($array[$element])] = $value;
	  }
  }
  else
  {
	  // more elements?
	  if(sizeof($elements) > 0)
	  {
		 $array[$element] = &pix_compose_xml_array($array[$element], $elements, $value);
	  }
	  else
	  {
		 $array[$element] = $value;
	  }
  }
	
  return $array;
}



/*
*
*	This function created HTML for a drop down menu
*
*/

function pix_select_menu($dir,$name,$current="",$none="",$class="forminput") {
	
	global $cfg;
	
	$output  = "<select name=\"$name\" style=\"width:100%;\" class=\"".$class."\">";
	
	if ($none != FALSE) { $output .= "<option value=\"\">None</option>"; }
	
	## Find out what valid photo albums are on the system and present them as options to the user
	$handle=@opendir($dir);
	$fn=0;
	if($handle != FALSE) {
	while (false!==($file = readdir($handle))) { 
		if (substr($file,0,1) != ".") { 
			$number = $fn + 1;
			  
				if ($current == $file) {
				
				$output .= "<option value=\"$file\" selected=\"selected\">$file (current)</option>\n";
				
				} else {
				
				$output .= "<option value=\"$file\">$file</option>\n";
				
				}
			  
			 ++$fn;
			 }
		}
	}

	@closedir($handle);
		
	$output .= "</select>";

	return($output);
	
}

/*
*	This function sends the user to the login page if the default action for
*	logged out users is to require logging in or registering before viewing content
*/
function pix_login_redirect() {
	
	global $ses, $cfg;
	
	if (!$ses['psg_userid'] && $cfg['set']['login_default']=="1") {
	
	header("Location: ".$cfg['sys']['base_url'].$cfg['fil']['index_login']);
	
	exit;
	
	}


}

/*
*	Recursive function to delete a directory
*/
function pix_delete_directory($dir)
{
   if (substr($dir, strlen($dir)-1, 1) != '/')
       $dir .= '/';

   //echo $dir;

   if ($handle = opendir($dir))
   {
       while ($obj = readdir($handle))
       {
           if ($obj != '.' && $obj != '..')
           {
               if (is_dir($dir.$obj))
               {
                   if (!pix_delete_directory($dir.$obj))
                       return false;
               }
               elseif (is_file($dir.$obj))
               {
                   if (!unlink($dir.$obj))
                       return false;
               }
           }
       }

       closedir($handle);

       if (!@rmdir($dir))
           return false;
       return true;
   }
   return false;
}



function pix_send_email($toemail, $toname, $message, $fromemail, $fromname, $subject) {

	global $cfg;
	
	//if (!ereg(".loc",$_SERVER['SERVER_NAME'])) {
	
		if ($toname !== '') {
		
		$to = "\"".$toname."\" <".$toemail.">";
		
		} else {
		
		$to = "<".$toemail.">";
		
		}
	
		$replyto = "<".$fromemail.">";
	
		if ($fromname !== '') {
		
		$headers = "From: \"".$fromname."\" <".$fromemail.">\r\n";
		
		} else {
		
		$headers = "From: $fromemail\r\n";
		
		}
	
		$subject = stripslashes($subject);
	
		$message = stripslashes($message);
	
		$headers .= "X-Mailer: PHP/" . phpversion() . "\r\n"; 
	
		$headers .= "MIME-Version: 1.0\r\n";
	
		$headers .= "Reply-To: $replyto\r\n";
	
		if (!@mail($to, $subject, $message, $headers, "-f $fromemail")) {
		
			return(FALSE);
		
		} else {
		
			return(TRUE);
		
		}
		
	//}
	
}



function pix_clear_cache() {

	global $cfg;
	
	// Delete the contents of the Smarty templates_c and caches directory
	$handle=@opendir(SYS_BASE_PATH."resources/smarty/templates_c/");
	$fn=0;
	if($handle != FALSE) {
	
		while (false!==($file = readdir($handle))) { 
		
			if (substr($file,0,1) != ".") { 
			
				@unlink(SYS_BASE_PATH."resources/smarty/templates_c/".$file);
			
			}
		}
	}

	@closedir($handle);
	
}



function pix_empty_directory($dir) {

	if (is_dir($dir)) {

		$dl=opendir($dir);
		
		if ($dl) {
		
			while($name = readdir($dl)) {
			
				if (!is_dir("$dir/$name")) {
				
					return false;
					break;
				
				}
			
			}
			
			closedir($dl);
		
			return true;

		} else {
		
			return true;
		
		}
	
	}

}


function pix_validate_email($email) {

	if (eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{1,4}",$email)) {
	
		return TRUE;
	
	} else {
	
		return FALSE;
		
	}

}


function pix_validate_profile($mode) {
	
	global $lang, $cfg, $style;
	
	$result = sql_select_rows("SELECT * FROM ".$cfg['sys']['table_usda']);
	
	if (is_array($result)) {
	
		foreach ($result as $key => $value) {
		
			$data[$value['name']] = $value['value'];

		}
	
	}
	
	$formal_title				= aglobal(formal_title);
	$other_title				= aglobal(other_title);
	$first_name					= aglobal(first_name);
	$middle_intial				= aglobal(middle_intial);
	$family_name				= aglobal(family_name);
	$email_address				= aglobal(email_address);
	$email_address_old			= aglobal(email_address_old);
	$telephone					= aglobal(telephone);
	$mobile						= aglobal(mobile);
	$fax						= aglobal(fax);
	$addr1						= aglobal(addr1);
	$addr2						= aglobal(addr2);
	$addr3						= aglobal(addr3);
	$city						= aglobal(city);
	$region						= aglobal(region);
	$country					= aglobal(country);
	$postal_code				= aglobal(postal_code);
	$password_1					= aglobal(password_1);
	$password_2					= aglobal(password_2);
	$password_rem_que			= aglobal(password_rem_que);
	$password_rem_ans			= aglobal(password_rem_ans);
	$other_business_type		= aglobal(other_business_type);
	$other_business_position	= aglobal(other_business_position);
	$other_image_interest		= aglobal(other_image_interest);
	$other_frequency			= aglobal(other_frequency);
	$other_circulation			= aglobal(other_circulation);
	$other_territories			= aglobal(other_territories);
	$other_website				= aglobal(other_website);
	$other_company_name			= aglobal(other_company_name);
	$other_message				= aglobal(other_message);

	// First assume no errors
	$problem = "0";
	
	// Create an array to hold each error in
	$problem_status = array();
	
	if ($mode != "admin_edit" && $data['salutation_req'] == 1 && $formal_title == "") {
	$problem = "1";
	$problem_status[] = "You must provide a salutation or title.";
	}
	
	if ($first_name == "") {
	$problem = "1";
	$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_01'];
	}
	
	if ($family_name == "") {
	$problem = "1";
	$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_02'];
	}
	
	if ($email_address == "") {
	$problem = "1";
	$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_03'];
	}
	
	if ($cfg['set']['user_ban_domains'] == 1) {
		
		// Load list of banned domain names
		$sql = "SELECT * FROM ".$cfg['sys']['table_domn']." ORDER BY domain_name ASC";
		
		$result = sql_select_rows($sql);
		
		$banned = (bool)FALSE;
		
		if (is_array($result)) {
		
			foreach ($result as $value) {
				
				if (eregi($value['domain_name'],$email_address)) {
					
					$banned = (bool)TRUE;
					$domain = $value['domain_name'];
				
				}
			
			}
		
		}
		
		if ($banned) {
		
			$problem = "1";
			$problem_status[] = "You cannot register on this site using an e-mail address at the domain '$domain'.";
			return($problem_status);

		}
		
	}
	
	if (($mode == "create" && $email_address != "") || ($mode != "create" && $email_address != $email_address_old)) {
	
		$email_address = strtolower($email_address);
		
		if (!pix_validate_email($email_address)) {
		
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_04'];
		
		} else 	{
		
			if ($mode != "edit") {
			
				# Check that the user does not already exist on the system.
				$result = sql_select_row ("SELECT * FROM " . $cfg['sys']['table_user'] . " WHERE email_address = '$email_address'");
				
				// If e-mail address exists, $result[0] will return a userid.				
				if ($result[0] > 0) {
				
					$problem = "1";
					$problem_status[] = sprintf($GLOBALS['_STR_']['PROFILE_VALID_06'],$email_address,$cfg['sys']['base_url'].$cfg['index_login']);
				}
				
			}
				
		}
		
	}
	
	if ( ( ($mode == "edit" || $mode == "admin_edit") && ($password_1 != "" || $password_2 != "") ) || $mode == "create") {
	
		if ($password_1 == "") {
		$problem = "1";
		$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_06'];
		}
		
		if ($password_2 == "") {
		$problem = "1";
		$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_07'];
		}
		
		if ($password_1 != $password_2) {
		
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_08'];
		
		} else {
		
			if (!eregi("^([a-zA-Z0-9]{4})([a-zA-Z0-9]+)", $password_1)) {
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_09'];
			}
			
		}
		
	}
	
	if ($password_rem_que == "") {
	$problem = "1";
	$problem_status[] .= $GLOBALS['_STR_']['PROFILE_VALID_10'];
	}
	
	if ($password_rem_ans == "") {
	$problem = "1";
	$problem_status[] .= $GLOBALS['_STR_']['PROFILE_VALID_11'];
	}
	
	// We won't check required information if this is an admin edit
	if ($mode != "admin_edit") {
		
		if ($data['telephone_req'] == 1 && $telephone == "") {
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_12'];
		}
	
		if ($data['mobile_req'] == 1 && $mobile == "") {
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_13'];
		}
	
		if ($data['facsimile_req'] == 1 && $fax == "") {
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_14'];
		}
	
		if ($data['company_name_req'] == 1 && $other_company_name == "") {
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_15'];
		}
	
		if ($data['business_type_req'] == 1 && $other_business_type == "") {
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_16'];
		}
	
		if ($data['business_position_req'] == 1 && $other_business_position == "") {
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_17'];
		}
	
		if ($mode == "create" && $data['image_interest_req'] == 1 && $other_image_interest == "") {
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_18'];
		}
	
		if ($data['frequency_req'] == 1 && $other_frequency == "") {
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_19'];
		}
	
		if ($data['circulation_req'] == 1 && $other_circulation == "") {
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_20'];
		}
	
		if ($data['territories_req'] == 1 && $other_territories == "") {
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_21'];
		}
	
		if ($data['website_req'] == 1 && $other_website == "") {
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_22'];
		}
	
		if ($data['address_req'] == 1) {
			
			if ($addr1 == "") {
				$problem = "1";
				$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_23'];
			}
			
			if ($city == "") {
				$problem = "1";
				$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_24'];
			}
			
			if ($postal_code == "") {
				$problem = "1";
				$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_25'];
			}
			
		}
		
		if ($mode == "create" && $data['message_req'] == 1 && $other_message == "") {
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['PROFILE_VALID_26'];
		}
	
	}
	
	
	if ($problem == "1") {
	
		return($problem_status);
		
	} else {
	
		return((bool)FALSE);
		
	}
	
}


/*
*
*	This function converts a MySQL date into a human friendly English date
*
*/

function pix_mysql_time($date) {
	
	global $cfg;
	
	$time = strtotime($date);
	
	if ($cfg['set']['date_format'] == "mdy") {
	
		$date = date('F jS Y',$time);
	
	} elseif ($cfg['set']['date_format'] == "dmy") {
	
		$date = date('jS F Y',$time);
	
	}
	
	return($date);
	
}

/*
*
*	This function creates an array of date information for use in building date selector menus
*
*/

function pix_date_menu() {
	
		$unix_time = time();
		
		$this_year	= date("Y",$unix_time);
		$this_month = date("m",$unix_time);
		$this_day	= date("d",$unix_time);
		
		// Make an array of years
		for ($i=$this_year; $i>=1850; $i--) {
			$years[] = $i;
		}

		// Make an array of months in the year
		for ($i=1; $i<=12; $i++) {
			$months[] = $i;
		}

		// Make an array of days in the month
		for ($i=1; $i<=31; $i++) {
			$days[] = $i;
		}
		
		$date_menus = array($years,$months,$days,$this_year,$this_month,$this_day);
		
		return $date_menus;
		
}

/*
*
*	This function allows control over HTTP headers
*
*/

function pix_http_headers($type,$cache="",$filename="") {
	
	if ($cache == "") { // Don't cache pages
		
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT"); // always modified
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0"); // HTTP/1.1
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache"); // HTTP/1.0
		
	} else { // Cache pages for $cache days
	
		// Set cache expiry to $cache days in the future
		header("Expires: " . gmdate("D, d M Y H:i:s", time() + ($cache * 86400)) . " GMT");
	
	}
	
	switch ($type) {
		
		case "json":
		
			// Tell the browser this is JSON content
			header('Content-Type: application/json');
			
		break;
		
		case "rss":
		
			// Tell the browser this is an RSS page
			header('Content-Type: application/rss+xml; charset=utf-8');
			
		break;
		
		case "xml":
		
			// Tell the browser this is an XML page
			header('Content-Type: text/xml');
			
		break;
		
		case "html":
			
			// Tell the browser this is UTF-8 encoded HTML
			header('Content-Type: text/html; charset=utf-8');
		
		break;
	
		case "css":
			
			// Tell the browser this is UTF-8 encoded HTML
			header('Content-Type: text/css; charset=utf-8');
		
		break;
	
		case "swf":
			
			// Tell the browser this is an Adobe Flash image
			header('Content-Type: application/x-shockwave-flash');
		
		break;
	
		case "svg":
		
			// Tell the browser this is a scalable vector image
			header('Content-Type: image/svg+xml');
		
		break;
	
		case "jpe": case "jpeg": case "jpg":
			
			// Tell the browser this is a JPEG image
			header('Content-Type: image/jpeg');
		
		break;
	
		case "gif":
			
			// Tell the browser this is a GIF image
			header('Content-Type: image/gif');
		
		break;
	
		case "png":
			
			// Tell the browser this is a PNG image
			header('Content-Type: image/png');
		
		break;
	
		case "csv":
			
			// Send forced download headers for a CSV file
			header("Content-Description: File Transfer");
			header("Content-Type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=$filename");
				
		break;
	
	}
	
}

/*
*
*	This function returns the total number of images in the library
*
*/

function pix_image_count() {

	global $cfg;
	
	$count_sql	= "SELECT count(image_id) FROM " . $cfg['sys']['table_imgs'];
	$result		= sql_count($count_sql);

	return $result;

}

/*
*	
*	mixed pix_template_width([[int factor], [int deductor]])
*	
*	Returns a width in pixels taken by dividing the site width 
*	by $factor and subtracting the variable $deductor
*
*/

function pix_template_width($factor="",$deductor="") {

	global $cfg, $show_tools;
	
	$theme_width = $cfg['set']['theme_width'] - 20;
	
	$numarg = func_num_args();

	switch($numarg) {

		case '2':
			$width = floor(($theme_width / $factor) - $deductor);
			break;
		case '1':
			$width = floor(($theme_width / $factor) - 1);
			break;
		default:
			$width = $theme_width;
			break;
	}
	
	return $width;

}

/*
*	print_array -- Print out PHP output of array contents in HTML format
*
*	mixed print_array(array array)
*/

function print_array($array) {

	if (is_array($array)) {
	
		echo("<pre style=\"font-family:Monaco,'Andale Mono'; font-size:8pt;\">");
		print_r($array);
		echo("</pre>");
	
	} else {
	
		return(FALSE);
		
	}
	
}


/*
*	apost -- $_POST array scan for specified key.
*	
*	string  apost(string key)
*	
*	If key matches key in $_POST array, Returns string which is value of key from $_POST array 
*	
*	if key does NOT match key in $_POST array, Returns EMPTY STRING.
*/

function apost($name){

	foreach($_POST as $key=>$value){

		if($name == $key){if($value !== ''){$output = $value;}}
	}

	if(!isset($output)){$output = '';}

	// Remove poison null bytes
	$output = str_replace("\0","",$output);
			
	$output = strip_tags(substr(trim($output),0,5000));

	return($output);

}

/*
*	acookie -- $_COOKIE array scan for specified key.
*	
*	string  apost(string key)
*	
*	If key matches key in $_COOKIE array, Returns string which is value of key from $_COOKIE array 
*	
*	if key does NOT match key in $_COOKIE array, Returns EMPTY STRING.
*/

function acookie($name){

	foreach($_COOKIE as $key=>$value){

		if($name == $key){if($value !== ''){$output = $value;}}
	}

	if(!isset($output)){$output = '';}

	// Remove poison null bytes
	$output = str_replace("\0","",$output);
			
	return($output);
	
}

/*	
*	aget -- $_GET array scan for specified key.
*	
*	string  aget(string key)
*	
*	If key matches key in $_GET array, Returns string which is value of key from $_GET array 
*	
*	if key does NOT match key in $_POST array, Returns EMPTY STRING.
*/

function aget($name){

	foreach($_GET as $key=>$value){

		if($name == $key){if($value !== ''){$output = $value;}}
	}

	if(!isset($output)){$output = '';}
	
	
	$output = strip_tags(substr(trim($output),0,5000));

	// Remove poison null bytes
	$output = str_replace("\0","",$output);
			
	return($output);

}

/*	
*	aglobal -- $_POST and $_GET array scan for specified key.
*	
*	string  aglocal(string key)
*	
*	If key matches key in $_POST or $_GET array, Returns string which is value of key from $_POST or $_GET array.
*	
*	if key does NOT match key in $_POST or $_GET array, Returns EMPTY STRING.
*	
*	by order of execution, precedence is given to $_POST.
*/

function aglobal($name){

	foreach($_GET as $key=>$value){

		if($name == $key){if($value !== ''){$output = $value;}}
	}

	foreach($_POST as $key=>$value){

		if($name == $key){if($value !== ''){$output = $value;}}
	}

	if(!isset($output)){$output = '';}

	$output = strip_tags(substr(trim($output),0,5000));

	// Remove poison null bytes
	$output = str_replace("\0","",$output);
			
	return($output);

}

?>