<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*	
*	Encode keywords entered from the admin interface (comma separated)
*	
*/
function keywordsEncode($keywords) {

	// Define pattern for matching grouped keywords
	$pattern = " ";
	
	// Create an array of punctuation to remove from the keyword fields
	$punctuation = array("'","\"","\\","/",";","\$","%","&","(",")","{","}","[","]","+","<",">");
	
	if (is_array($keywords)) {
	
		foreach ($keywords as $keyword) {
		
			// If two keywords are bracketed by single quotes, concatenate them into one word joined by 'QZQ'
			$keywords_parsed[] = trim($keyword);
		
		}
		
		// Return the result
		if (is_array($keywords_parsed)) {
		
			return $keywords_parsed;
		
		}
		
	} elseif (is_string($keywords)) {
		
		// Load the individual keywords into an array
		$keywords = explode(",",$keywords);
		
		if (is_array($keywords)) {
		
			foreach ($keywords as $key => $value) {
			
				$keywords_parsed[] = str_replace($punctuation,"",trim($value));
			
			}
		
		}
		
		// Return the result
		if (is_array($keywords_parsed)) {
		
			$keywords_parsed = implode(", ",$keywords_parsed);
			
			return $keywords_parsed;
			
		}
		
	} else {
	
		return false;
	
	}
	
}

/*
*	
*	Decode keywords for on screen display in admin interface
*	
*/
function keywordsDecode($keywords,$escape="") {

	if (is_array($keywords)) {
	
		foreach ($keywords as $keyword) {
		
			if (strlen($keyword) == 5) {
			
				$keywords_parsed[] = ereg_replace("QQ","",$keyword);
			
			} elseif (ereg("QZQ",$keyword)) {
			
				$keywords_parsed[] = $escape . ereg_replace("QZQ"," ",$keyword) . $escape;
			
			} else {
			
				$keywords_parsed[] = $keyword;
			
			}
		
		}
		
		// Return the result
		if (is_array($keywords_parsed)) {
		
			return $keywords_parsed;
		
		}
		
	} elseif (is_string($keywords)) {
	
		$keywords = explode(" ",$keywords);
		
		if (is_array($keywords)) {
				
			foreach ($keywords as $keyword) {
			
				if (strlen($keyword) == 5) {
				
					$keywords_parsed[] = ereg_replace("QQ","",$keyword);
				
				} elseif (ereg("QZQ",$keyword)) {
			
					$keywords_parsed[] = $escape . ereg_replace("QZQ"," ",$keyword) . $escape;
			
				} else {
				
					$keywords_parsed[] = $keyword;
				
				}
			
			}
	
		}
		
		// Return the result
		if (is_array($keywords_parsed)) {
		
			$keywords_parsed = implode(", ",$keywords_parsed);
			
			return $keywords_parsed;
			
		}
		
	} else {
	
		return false;
	
	}
	
}

/*
*
*	Decode keywords for display in front end user interface
*
*/
function keywordsTranslateOutput($keywords,$escape="") {

	if (is_array($keywords)) {
	
		foreach ($keywords as $keyword) {
			
			$keyword = trim($keyword);
			
			// Dont show keywords with special characters in them
			if (preg_match('/\*|\.|\|/',$keyword)) {
				break;
			}
			
			if (ereg(" ",$keyword)) {
			
				$keywords_parsed[] = $escape . "&quot;" . $keyword . $escape . "&quot;";
			
			} else {
			
				$keywords_parsed[] = $keyword;
			
			}
		
		}
		
		// Return the result
		if (is_array($keywords_parsed)) {
		
			return $keywords_parsed;
		
		}
		
	} elseif (is_string($keywords)) {
	
		$keywords = explode(",",$keywords);
		
		if (is_array($keywords)) {
				
			foreach ($keywords as $keyword) {
			
				// Dont show keywords with special characters in them
				if (preg_match('/\*|\.|\|/',$keyword)) {
					break;
				}
				
				$keyword = trim($keyword);
			
				if (ereg(" ",$keyword)) {
			
					$keywords_parsed[] = $escape . "&quot;" . $keyword . $escape . "&quot;";
			
				} else {
				
					$keywords_parsed[] = $keyword;
				
				}
			
			}
	
		}
		
		// Return the result
		if (is_array($keywords_parsed)) {
		
			$keywords_parsed = implode(", ",$keywords_parsed);
			
			return $keywords_parsed;
			
		}
		
	} else {
	
		return false;
	
	}
	
}

/*
*
*	Encode keywords for display in front end user interface
*
*/
function keywordsTranslateInput($keywords) {
	
	// Define pattern for matching grouped keywords
	$pattern = "'([[:alnum:]]+) ([[:alnum:]]+)'";
	
	// Create an array of punctuation to remove from the keyword fields
	$punctuation = array(",","'","\"","\\","/",";","\$","%","&","(",")","{","}","[","]","+","*","<",">");
	
	if (is_array($keywords)) {
	
		foreach ($keywords as $keyword) {
		
			// If two keywords are bracketed by single quotes, concatenate them into one word joined by 'QZQ'
			$keywords_parsed[] = eregi_replace($pattern,"\\1QZQ\\2",stripslashes($keyword));
		
			if (strlen($keyword) == 3) {
			
				$keywords_parsed[] = "QQ".$keyword;
			
			} else {
			
				$keywords_parsed[] = $keyword;
			
			}
		
		}
		
		// Return the result
		if (is_array($keywords_parsed)) {
		
			return $keywords_parsed;
		
		}
		
	} elseif (is_string($keywords)) {
		
		// If two keywords are bracketed by single quotes, concatenate them into one word joined by 'QZQ'
		$keywords = eregi_replace($pattern,"\\1QZQ\\2",stripslashes($keywords));
		
		// Get the optional keywords to search for
		$keywords = str_replace($punctuation,"",$keywords);

		// Load the individual keywords into an array
		$keywords = explode(" ",$keywords);
		
		if (is_array($keywords)) {
				
			foreach ($keywords as $keyword) {
			
				if (strlen($keyword) == 3) {
				
					$keywords_parsed[] = "QQ".$keyword;
				
				} else {
				
					$keywords_parsed[] = $keyword;
				
				}
			
			}
	
		}
		
		// Return the result
		if (is_array($keywords_parsed)) {
		
			$keywords_parsed = implode(" ",$keywords_parsed);
			
			return $keywords_parsed;
			
		}
		
	} else {
	
		return false;
	
	}
	
}

function generateUserGroupsArray () {

	global $cfg;
	
	// Administrators can do everything so we don't include them in this list
	$sql = "SELECT * FROM " . $cfg['sys']['table_grps'] . " WHERE group_id != '1' AND group_id != 2 ORDER BY group_name ASC";
	
	$result = sql_select_rows($sql);
	
	if (is_array($result)) {
		
		$group_id = array();
		$group_name	= array();
		
		foreach ($result as $key => $value) {
		
			$group_id[] 	= $value['group_id'];
			$group_name[]	= $value['group_name'];
		
		}
	
	}

	$group_info[] = $group_name;
	$group_info[] = $group_id;
	
	return $group_info;
}

function sendEmail($toemail, $toname, $message, $fromemail, $fromname, $subject) {

	global $cfg;
	
	//if (!ereg(".loc",$_SERVER['SERVER_NAME'])) {
	
		if ($toname !== '') {
		
		$to = "\"".$toname."\" <".$toemail.">";
		
		} else {
		
		$to = "<".$toemail.">";
		
		}
	
		$replyto = "<".$fromemail.">";
	
		if ($fromname !== '') {
		
		$headers = "From: \"".$fromname."\" <".$fromemail.">\r\n";
		
		} else {
		
		$headers = "From: $fromemail\r\n";
		
		}
	
		$subject = stripslashes($subject);
	
		$message = stripslashes($message);
	
		$headers .= "X-Mailer: PHP/" . phpversion() . "\r\n"; 
	
		$headers .= "MIME-Version: 1.0\r\n";
	
		$headers .= "Reply-To: $replyto\r\n";
	
		if (!mail($to, $subject, $message, $headers)) {
		
			return(FALSE);
		
		} else {
		
			return(TRUE);
		
		}
		
	//}
	
}

/*
*
*	This function creates an array of nested gallery
*
*/
function galleryNestedArray($id) {

	global $gallery_parents, $gallery_name, $cfg, $gallery_id, $gallery_array, $indent;

	$gallery_array[]	= $indent.$gallery_name[$id];
	
	$gallery_id[]		= $id;
	
	if(isset($gallery_parents[$id]) && is_array($gallery_parents[$id])) {
	
	
		foreach($gallery_parents[$id] as $kind) {
			$indent .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			galleryNestedArray($kind);
		}
		
	}
	
	$indent = substr($indent,0,-36);

}



/*
*
*	Check a directory is empty.
*	Returns true if dir is empty, false if it isn't
*
*/
function isEmptyFolder ($dir) {

	if (is_dir($dir)) {

		$dl = opendir($dir);
		
		if ($dl) {
		
			while($name = readdir($dl)) {
			
				if (!is_dir("$dir/$name")) { //<--- corrected here
				
					return false;
					
					break;
				
				}
			
			}

			closedir($dl);
		
		}
		
		return true;
	
	} else {
	
		return true;
	
	}

}

/*
*
*	Check if a string is UTF-8
*	Returns true if the string is valid UTF8, false if it isn't
*
*/
function stringIsUTF8($string) {
    
    // From http://w3.org/International/questions/qa-forms-utf-8.html
    return preg_match('%^(?:
          [\x09\x0A\x0D\x20-\x7E]            # ASCII
        | [\xC2-\xDF][\x80-\xBF]             # non-overlong 2-byte
        |  \xE0[\xA0-\xBF][\x80-\xBF]        # excluding overlongs
        | [\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
        |  \xED[\x80-\x9F][\x80-\xBF]        # excluding surrogates
        |  \xF0[\x90-\xBF][\x80-\xBF]{2}     # planes 1-3
        | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
        |  \xF4[\x80-\x8F][\x80-\xBF]{2}     # plane 16
    )*$%xs', $string);
    
} 

/*
*
*	Check if a string is UTF-8
*	Returns true if the string is valid UTF8, false if it isn't
*
*/
function safeConvertUTF8($string) {
    
    global $ses;
    
    if (stringIsUTF8($string)) {
    
    	return $string;
    	
    } else {
    	
    	//  If the user has picked a charset to override ISO-8859-1,
    	//	then use that for the conversion to UTF-8
    	
    	if ($ses['pref_iptc_import_charset'] != '') {
    	
    		return iconv($ses['pref_iptc_import_charset'],'UTF-8',$string);
    		
    	} else {
			
    		return utf8_encode($string);
    		
    	}
    	
    }
    
} 

/*
*
*	This function returns a two dimensional array of nested gallery names
*	and the id number of each gallery.  Output is sorted alphabetically.
*
*/
function galleryListingArray($filter="") {

	global $cfg, $gallery_array, $gallery_id, $gallery_parents, $gallery_name;
	
	$gallery_name		= array();
	$gallery_parents	= array();
	$gallery_array		= array();
	$gallery_id			= array();
	
	if ($filter != "") {
		$filter = " AND " . $filter . " ";
	}
	
	$sql	= 	"SELECT ".PIX_TABLE_GALL.".gallery_id, ".PIX_TABLE_GALL.".gallery_title, ".PIX_TABLE_GALL.".gallery_parent FROM ".PIX_TABLE_GALL."
				
				LEFT JOIN ".PIX_TABLE_GVIE." ON ".PIX_TABLE_GALL.".gallery_id = ".PIX_TABLE_GVIE.".gallery_id
				
				WHERE ".PIX_TABLE_GALL.".gallery_id != '0'
				
				".$cfg['set']['gallery_access_sql']."
				
				".$cfg['set']['gallery_visibility_sql']."
				
				$filter
				
				GROUP BY ".PIX_TABLE_GALL.".gallery_id";
	
	$result = sql_select_rows($sql);
	
	if (is_array($result) && count($result) > 0) {
		
		foreach ($result as $key => $value) {
		
			$gallery_parents[$value["gallery_parent"]][] 	= $value["gallery_id"];
			$gallery_name[$value["gallery_id"]]				= $value["gallery_title"];
		
		}
		
		ksort($gallery_parents);
		
		$sql		= 	"SELECT ".PIX_TABLE_GALL.".gallery_id FROM ".PIX_TABLE_GALL."
				
						LEFT JOIN ".PIX_TABLE_GVIE." ON ".$cfg['sys']['table_gall'] .".gallery_id = ".PIX_TABLE_GVIE.".gallery_id
				
						WHERE ".PIX_TABLE_GALL.".gallery_parent = '0'
						
						".$cfg['set']['gallery_access_sql']."
						
						".$cfg['set']['gallery_visibility_sql']."
						
						$filter
						
						GROUP BY ".PIX_TABLE_GALL.".gallery_id
						
						ORDER by ".PIX_TABLE_GALL.".gallery_title ASC";
		
		$parents	= sql_select_rows($sql);
		
		if (is_array($parents)) {
		
			foreach ($parents as $key => $value) {
			
				galleryNestedArray($value['gallery_id']);
			
			}
		
		}
		
	}
	
	$gallery_info[] = $gallery_array;
	$gallery_info[] = $gallery_id;
	
	return $gallery_info;

}


/*
*
*	Load an array with the gallery_ids of all subgalleries of a gallery with gallery_id of $gallery_id
*
*/
function galleryListSubgalleries($gallery_id) {

	global $cfg, $subgallery_array;
	
	$subgallery_array = array();
	
	galleryGetSubgalleryId($gallery_id);
		
	return $subgallery_array;

}

/*
*
*	Load an array with the gallery_ids of all subgalleries of a gallery with gallery_id of $gallery_id
*
*/
function galleryGetSubgalleryId($id) {
	
	global $cfg, $subgallery_array;

	$sql = "SELECT gallery_id FROM ".$cfg['sys']['table_gall']." WHERE gallery_parent = '$id'";
	
	$result = sql_select_rows($sql);
	
	if (is_array($result)) {
		
		foreach($result as $key => $value) {
			
			$subgallery_array[] = $value['gallery_id'];
			
			galleryGetSubgalleryId($value['gallery_id']);
		
		}
		
	}

}

/*
*
*	This function returns a two dimensional array of user group names
*	and the id number of each group.  Output is sorted alphabetically.
*
*/
function groupListArray($selected_items = "") {
	
	global $cfg;
	
	// Administrators can do everything so we don't include them in this list
	$sql = "SELECT * FROM " . PIX_TABLE_GRPS . " WHERE group_id != '1' AND group_id != 2 ORDER BY group_name ASC";
	
	$result = sql_select_rows($sql);
	
	if (is_array($result)) {
		
		$group_id = array();
		$group_name	= array();
		
		foreach ($result as $key => $value) {
		
			$group_id[] 		= $value['group_id'];
			$group_name[]		= $value['group_name'];
			
			if (is_array($selected_items)) {
				if (in_array($value['group_id'],$selected_items)) { $group_selected[]	= (bool)true; } else { $group_selected[] = (bool)false; }
			}
		}
	
	}

	$group_info[] = $group_name;
	$group_info[] = $group_id;
	$group_info[] = $group_selected;
	
	return $group_info;
		
}
/*
*
*	This function returns a two dimensional array of user group names
*	and the id number of each group.  Output is sorted alphabetically.
*
*/
function photographerListArray() {
	
	global $cfg;
	
	// Administrators can do everything so we don't include them in this list
	$sql = "SELECT CONCAT(".PIX_TABLE_USER.".family_name,', ',".PIX_TABLE_USER.".first_name) AS user_name, userid
	
			FROM ".PIX_TABLE_USER."
					
			LEFT JOIN ".PIX_TABLE_SGME." ON
			
			".PIX_TABLE_USER.".userid = ".PIX_TABLE_SGME.".sys_userid
			
			WHERE ".PIX_TABLE_SGME.".sys_group_id = '3' OR ".PIX_TABLE_SGME.".sys_group_id = '1'
			
			ORDER BY ".PIX_TABLE_USER.".family_name ASC";
	
	$result = sql_select_rows($sql);
	
	if (is_array($result)) {
		
		$userid 	= array();
		$user_name	= array();
		
		foreach ($result as $key => $value) {
		
			$userid[] 		= $value['userid'];
			$user_name[]	= $value['user_name'];
			
		}
	
	}

	$group_info[] = $userid;
	$group_info[] = $user_name;
	
	return $group_info;
		
}

/*
*
*	This function returns the name of a group from the group_id
*
*/
function getGroupName($group_id) {

	global $cfg;
	
	$result	= sql_select_row("SELECT * FROM ".PIX_TABLE_GRPS." WHERE group_id = '$group_id'");
	
	if (is_array($result)) {
	
		$group_name 	= $result[group_name];
	
	}
	
	return $group_name;

}

/*
*
*	This function returns the name and e-mail address of a user as an array from their userid
*
*/
function getUserContactDetails($userid) {

	global $cfg;
	
	$result	= sql_select_row("SELECT * FROM " . PIX_TABLE_USER . " WHERE userid = '$userid'");
	
	if (is_array($result)) {
	
		$first_name 	= $result['first_name'];
		$family_name	= $result['family_name'];
	
		$name			= $first_name." ".$family_name;
		$email_address	= $result['email_address'];
		
		return array($name, $email_address);

	} else {
		
		return array(false, false);
	
	}
	

}

/*
*
*	This function permanently deletes an image from the library
*
*/
function deleteImageFromLibrary($image_id,$delete_option) {
	
	global $cfg, $errors;
	
	if (!isset($cfg['sys']['temp_name'])) { $cfg['sys']['temp_name'] = $temp_name = date("Y-m-d",time()); }
	
	$result = sql_select_row("SELECT * FROM ".PIX_TABLE_IMGS." WHERE image_id = '$image_id'");
	
	if (is_array($result)) {
	
		$library_path	= $cfg['sys']['base_library'];
		$incoming_path	= $cfg['sys']['base_incoming'];
		
		$image_path		= $result['image_path'];
		$image_filename = $result['image_filename'];
		
		$file_32x32		= $library_path.$image_path."/32x32/$image_filename";
		$file_80x80		= $library_path.$image_path."/80x80/$image_filename";
		$file_160x160	= $library_path.$image_path."/160x160/$image_filename";
		$file_320x320	= $library_path.$image_path."/".SMALL_COMPING_DIR."/$image_filename";
		$file_630x630	= $library_path.$image_path."/".LARGE_COMPING_DIR."/$image_filename";
		$file_original	= $library_path.$image_path."/original/$image_filename";
		
		if ($delete_option == 10) { // We're going to completely delete this image
			
			// Delete each version of the image
			if (!@unlink($file_32x32)) 	{ $error_array[] = $file_32x32; }
			if (!@unlink($file_80x80)) 	{ $error_array[] = $file_80x80; }
			if (!@unlink($file_160x160)) { $error_array[] = $file_160x160; }
			if (!@unlink($file_320x320)) { $error_array[] = $file_320x320; }
			if (!@unlink($file_630x630)) { $error_array[] = $file_630x630; }
			
			if (file_exists($file_original)) { // If there is a high-res original file, delete it
			
				if (!@unlink($file_original)) { $error_array[] = $file_original; }
			
			}
			
			if ($image_path != "archive" && $image_path != "" && pix_empty_directory($library_path.$image_path."/32x32/")) { // If the directory is empty, delete it as well
				
				if (!@rmdir($library_path.$image_path."/32x32")) { $error_array[] = $library_path.$image_path."/32x32"; }
				if (!@rmdir($library_path.$image_path."/80x80")) { $error_array[] = $library_path.$image_path."/80x80"; }
				if (!@rmdir($library_path.$image_path."/160x160")) { $error_array[] = $library_path.$image_path."/160x160"; }
				if (!@rmdir($library_path.$image_path."/".SMALL_COMPING_DIR)) { $error_array[] = $library_path.$image_path."/".SMALL_COMPING_DIR; }
				if (!@rmdir($library_path.$image_path."/".LARGE_COMPING_DIR)) { $error_array[] = $library_path.$image_path."/".LARGE_COMPING_DIR; }
				if (file_exists($library_path.$image_path."/original")) { if (!@rmdir($library_path.$image_path."/original")) { $error_array[] = $library_path.$image_path."/original"; } }
				if (!@rmdir($library_path.$image_path)) { $error_array[] = $library_path.$image_path; }
				
			}
				
		} elseif ($delete_option == 11) { // We're going to move this image back into the incoming directory
			
			if ($image_path == "archive") { // This image was uploaded by HTTP so we need to put one file back into the incoming directory
			
				if (file_exists($library_path.$image_path."/original/$image_filename")) {
					
					rename($library_path.$image_path."/original/$image_filename",$incoming_path."/$image_filename");
					
					// Move each version of the image
					if (!@unlink($file_32x32)) { $error_array[] = $file_32x32; }
					if (!@unlink($file_80x80)) { $error_array[] = $file_80x80; }
					if (!@unlink($file_160x160)) { $error_array[] = $file_160x160; }
					if (!@unlink($file_320x320)) { $error_array[] = $file_320x320; }
					if (!@unlink($file_630x630)) { $error_array[] = $file_630x630; }
			
				} else {
					
					rename($library_path.$image_path."/".COMPING_DIR."/$image_filename",$incoming_path."/$image_filename");
					
					// Move each version of the image
					if (!@unlink($file_32x32)) { $error_array[] = $file_32x32; }
					if (!@unlink($file_80x80)) { $error_array[] = $file_80x80; }
					if (!@unlink($file_160x160)) { $error_array[] = $file_160x160; }
					if (!@unlink($file_320x320)) { $error_array[] = $file_320x320; }
			
				}
			
			} else { // The image is part of a directory and we need to move the entire directory
				
				if (!is_dir($incoming_path."/".$image_path)) { // If the image directory in the incoming directory doesn't exist then make it
					mkdir($incoming_path."/".$image_path);
				}
				
				if (!is_dir($incoming_path."/".$image_path."/32x32")) { // If the image directory in the incoming directory doesn't exist then make it
					mkdir($incoming_path."/".$image_path."/32x32");
				}
				
				if (!is_dir($incoming_path."/".$image_path."/80x80")) { // If the image directory in the incoming directory doesn't exist then make it
					mkdir($incoming_path."/".$image_path."/80x80");
				}
				
				if (!is_dir($incoming_path."/".$image_path."/160x160")) { // If the image directory in the incoming directory doesn't exist then make it
					mkdir($incoming_path."/".$image_path."/160x160");
				}
				
				if (!is_dir($incoming_path."/".$image_path."/".SMALL_COMPING_DIR)) { // If the image directory in the incoming directory doesn't exist then make it
					mkdir($incoming_path."/".$image_path."/".SMALL_COMPING_DIR);
				}
				
				if (!is_dir($incoming_path."/".$image_path."/".LARGE_COMPING_DIR)) { // If the image directory in the incoming directory doesn't exist then make it
					mkdir($incoming_path."/".$image_path."/".LARGE_COMPING_DIR);
				}
				
				if (!is_dir($incoming_path."/".$image_path."/original")) { // If the image directory in the incoming directory doesn't exist then make it
					mkdir($incoming_path."/".$image_path."/original");
				}
				
				// Now move the images into 
				if (!@rename($file_32x32,$incoming_path."/".$image_path."/32x32/$image_filename")) { $error_array[] = $file_32x32; }
				if (!@rename($file_80x80,$incoming_path."/".$image_path."/80x80/$image_filename")) { $error_array[] = $file_80x80; }
				if (!@rename($file_160x160,$incoming_path."/".$image_path."/160x160/$image_filename")) { $error_array[] = $file_160x160; }
				if (!@rename($file_320x320,$incoming_path."/".$image_path."/".SMALL_COMPING_DIR."/$image_filename")) { $error_array[] = $file_320x320; }
				if (!@rename($file_630x630,$incoming_path."/".$image_path."/".LARGE_COMPING_DIR."/$image_filename")) { $error_array[] = $file_630x630; }
	
				if (file_exists($file_original)) { // If there is a high-res original file, move it
				
					if (!@rename($file_original,$incoming_path."/".$image_path."/original/$image_filename")) { $error_array[] = $file_original; }
				
				}
			
				if ($image_path != "" && pix_empty_directory($library_path.$image_path."/32x32/")) { // If the directory is empty, delete it as well
						
					if (!@rmdir($library_path.$image_path."/32x32")) { $error_array[] = $library_path.$image_path."/32x32"; }
					if (!@rmdir($library_path.$image_path."/80x80")) { $error_array[] = $library_path.$image_path."/80x80"; }
					if (!@rmdir($library_path.$image_path."/160x160")) { $error_array[] = $library_path.$image_path."/160x160"; }
					if (!@rmdir($library_path.$image_path."/".SMALL_COMPING_DIR)) { $error_array[] = $library_path.$image_path."/".SMALL_COMPING_DIR; }
					if (!@rmdir($library_path.$image_path."/".LARGE_COMPING_DIR)) { $error_array[] = $library_path.$image_path."/".LARGE_COMPING_DIR; }
					if (file_exists($library_path.$image_path."/original")) { if (!@rmdir($library_path.$image_path."/original")) { $error_array[] = $library_path.$image_path."/original"; } }
					if (!@rmdir($library_path.$image_path)) { $error_array[] = $library_path.$image_path; }
					
				}
				
			}		
			
		}
		
		// Delete the image from the main library table
		@mysql_query("DELETE FROM ".PIX_TABLE_IMGS." WHERE image_id = '$image_id'");
		
		// Delete the image from the gallery order table
		@mysql_query("DELETE FROM ".PIX_TABLE_GORD." WHERE image_id = '$image_id'");
		
		// Delete the image from the image viewers table
		@mysql_query("DELETE FROM ".PIX_TABLE_IMVI." WHERE image_id = '$image_id'");
		
		// Delete the image from the image categories table
		@mysql_query("DELETE FROM ".PIX_TABLE_CAME." WHERE image_id = '$image_id'");
		
		// Delete the image from the image lightbox table
		@mysql_query("DELETE FROM ".PIX_TABLE_LBME." WHERE image_id = '$image_id'");
		
		// Add any errors from this run to the global error array
		if (is_array($error_array) && is_array($errors)) { $errors = array_merge($errors,$error_array); }
		
	} else {
	
		return false;
	
	}
	
}

/*
*
*
*	This function deletes images from the library and filesystem
*	
*	It takes:
*
*	$images			required	An array of images
*	$action			required	The URL to take the user to when they got to the next page
*	$delete_option	required	The type of delete to use (trash the images files or move to incoming)
*	$hidden_name	optional	Array of hidden form field names
*	$hidden_value	optional	Array of hidden form field values
*	
*
*/
function resultDeleteImageFromLibrary($images,$action,$delete_option,$hidden_name="",$hidden_value="") {
	
	global $smarty, $cfg, $errors;
	
	// Only continue if we've got an array of images (as image_id)
	if (is_array($images)) {
			
		$errors = array();
		
		foreach ($images as $key => $value) { // Loop through the array and delete each image
		
			// Delete the image from the database
			deleteImageFromLibrary($value,$delete_option);
							
		}
		
		// Load any errors into Smarty
		$smarty->assign("errors",$errors);
		$smarty->assign("errors_count",count($errors));
		
		// Pass the URL of the next page to the Smarty template
		$smarty->assign("action",$action);
		
		// Hidden form field names and values for the HTML output as two arrays
		$smarty->assign("hidden_name",$hidden_name);
		$smarty->assign("hidden_value",$hidden_value);

		// Set the page title
		$smarty->assign("page_title","Images deleted");
		
		// Load the HTML page
		$smarty->pixDisplay('admin.library/library.images.delete.02.tpl');
	
	} else { // No images selected, go back to the orphan image view
	
		// Generate a redirection URL 
		$meta_url	= $action;
				
		// Show redirect page
		$smarty->pixWaiting($meta_url,"1");	
		
		exit;
	
	}

}

/*
*
*
*	This function shows a confirmation page for deleting images from the library and filesystem
*	
*	It takes:
*
*	$images			required	An array of images
*	$action			required	The URL to take the user to when they got to the next page
*	$hidden_name	optional	Array of hidden form field names
*	$hidden_value	optional	Array of hidden form field values
*	
*
*/
function formDeleteImageFromLibrary($images,$action,$hidden_name="",$hidden_value="") {
	
	global $smarty, $cfg;
	
	// Only continue if we've got an array of images (as image_id)
	if (is_array($images)) {
	
		// Initialise arrays to hold image data
		$image_id	 	= array();
		$image_filename = array();
		$image_path		= array();
		$image_date		= array();
		$icon_path		= array();
		$icon_width		= array();
		$icon_height	= array();
		$comp_path		= array();
		$comp_width		= array();
		$comp_height	= array();
		$comp_filesize	= array();
		$image_width	= array();
		$image_height	= array();
		$image_title	= array();
		
		foreach($images as $key => $value) {
			
			// Get the name of the file from the database
			$result = sql_select_row("SELECT * FROM ".PIX_TABLE_IMGS." WHERE image_id = '$value';");
			
			$image_filename[]	= $result['image_filename'];
			$image_path[]		= $result['image_path'];
			
			$image_id[]			= $result['image_id'];
			$image_date[]		= $result['image_date'];
			
			$icon_path[]		= base64_encode($cfg['sys']['base_library'].$result[image_path]."/32x32/".$result[image_filename]);
			$icon_size			= getimagesize($cfg['sys']['base_library'].$result[image_path]."/32x32/".$result[image_filename]);
			$icon_width[]		= $icon_size[0];
			$icon_height[]		= $icon_size[1];
			
			$comp_path[]		= base64_encode($cfg['sys']['base_library'].$result[image_path]."/".COMPING_DIR."/".$result[image_filename]);
			$comp_size			= getimagesize($cfg['sys']['base_library'].$result[image_path]."/".COMPING_DIR."/".$result[image_filename]);
			$comp_width[]		= $comp_size[0];
			$comp_height[]		= $comp_size[1];
			$comp_filesize[]	= round((filesize($cfg['sys']['base_library'].$result[image_path]."/".COMPING_DIR."/".$result[image_filename]) / 1024),0);
			
			$image_width[]		= $result['image_width'];
			$image_height[]		= $result['image_height'];
						
		}

		// Assign image information to Smarty
		$smarty->assign("image_id",$image_id);
		$smarty->assign("image_filename",$image_filename);
		$smarty->assign("image_path",$image_path);
		$smarty->assign("image_date",$image_date);
		$smarty->assign("icon_path",$icon_path);
		$smarty->assign("icon_width",$icon_width);
		$smarty->assign("icon_height",$icon_height);
		$smarty->assign("comp_path",$comp_path);
		$smarty->assign("comp_width",$comp_width);
		$smarty->assign("comp_height",$comp_height);
		$smarty->assign("comp_filesize",$comp_filesize);
		$smarty->assign("image_width",$image_width);
		$smarty->assign("image_height",$image_height);
		
		$smarty->assign("display",(bool)TRUE);
		
	}
	
	// Pass the URL of the next page to the Smarty template
	$smarty->assign("action",$action);
	
	// Hidden form field names and values for the HTML output as two arrays
	$smarty->assign("hidden_name",$hidden_name);
	$smarty->assign("hidden_value",$hidden_value);
	
	$smarty->assign("page_title","Delete images");
	
	// Load the HTML page
	$smarty->pixDisplay('image/images.delete.01.html');

}

/*
*
*	
*
*/
function formAddImagesToGallery () {

	global $objEnvData, $smarty, $cfg;
	
	$images = $objEnvData->fetchGlobal('images');
	
	// This case displays a page to import images into an existing gallery
	
	// If the user is starting with a pre-made directory, load it into Smarty
	$smarty->assign("image_path",$image_path);
	
	// Load the images into Smarty
	$smarty->assign("images",$images);
	
	// Load an array of nested galleries
	$gallery_list = galleryListingArray();
	
	// Load the gallery list data into Smarty
	$smarty->assign("menu_gallery_title",$gallery_list[0]);
	$smarty->assign("menu_gallery_id",$gallery_list[1]);

	// Set the page title
	$smarty->assign("page_title","Add images to gallery");
	
	// Display the admin status page
	$smarty->pixDisplay('gallery/form.images.add.html');
		
}

/*
*
*	
*
*/
function formMoveImagesToGallery () {

	global $objEnvData, $smarty, $cfg;
	
	$images = $objEnvData->fetchGlobal('images');
	
	// This case displays a page to import images into an existing gallery
	
	// If the user is starting with a pre-made directory, load it into Smarty
	$smarty->assign("image_path",$image_path);
	
	// Load the images into Smarty
	$smarty->assign("images",$images);
	
	// Load an array of nested galleries
	$gallery_list = galleryListingArray();
	
	// Load the gallery list data into Smarty
	$smarty->assign("menu_gallery_title",$gallery_list[0]);
	$smarty->assign("menu_gallery_id",$gallery_list[1]);

	// Set the page title
	$smarty->assign("page_title","Add images to gallery");
	
	// Display the admin status page
	$smarty->pixDisplay('admin.gallery/gallery.add.01.tpl');
		
}

/*
*
*	
*
*/
function getImageMemoryRequirement ($image) {

	$imageInfo = @getimagesize($image);
	
	$MB = Pow(1024,2);		// number of bytes in 1M
	$K64 = Pow(2,16);		// number of bytes in 64K
	$TWEAKFACTOR = 1.8;		// Or whatever works for you
	$memoryNeeded = round(
							( $imageInfo[0] * $imageInfo[1]
											* $imageInfo['bits']
											* $imageInfo['channels'] / 8
							  + $K64
							) * $TWEAKFACTOR
						 );
						 
	return ceil($memoryNeeded / $MB);

}

/*
*
*	
*
*/
function imageIsResizeable ($image) {

	$memory_limit		= (integer) rtrim(get_cfg_var('memory_limit'),"M");
	$memory_required	= (integer) getImageMemoryRequirement($image);
	
	if ($memory_limit >= $memory_required) {
	
		return array("1","$memory_required");
	
	} else {
	
		return array("0","$memory_required");
	
	}
	
}

/*
*
*	
*
*/
function countDirectoryItems($path) {

	if (file_exists($path)) {
	
		$handle=@opendir($path);
		
		$fn=0;
		
		$thu_number=0;
		
		if($handle != FALSE) {
		
			while (false!==($file = readdir($handle))) {
				
				$file_extension = strtolower(substr($file,-4,4));
				
				if ($file_extension == ".jpg") { 
			 	
			 		$thu_number = $fn + 1;
			  		$fn++;
			  	
			  	}
			
			}
		
		}
		
		@closedir($handle);
		
		return($thu_number);
	
	} else { return FALSE; }

}

/*
*
*	Function to recursively remove directory contents
*	Modified from: http://uk.php.net/manual/en/function.rmdir.php
*
*/
function removeDirectoryComplete($dir) {

	if ( !is_writable($dir)) {
	
		if (!@chmod( $dir, 0777)) {
			return FALSE;
		}
		
	}
	
	$d = dir($dir);
	
	while (FALSE !== ($entry = $d->read())) {
		
		if ($entry == '.' || $entry == '..') {
			continue;
		}
		
		$entry = $dir . '/' . $entry;
		
		if (is_dir($entry )) {
		
			if (!removeDirectoryComplete($entry)) {
				return FALSE;
			}
			
			continue;
			
		}
		
		if ( !@unlink($entry)) {
		
			$d->close();
			
			return FALSE;
			
		}
		
	}
	
	$d->close();
	
	rmdir($dir);
	
	return TRUE;
	
}

/*
*	This is a Smarty extension function used to include templates from within another template
*	it is required in all scripts in order for Smarty to intialise correctly
*/
function smartyIncludeTemplate($params, &$smarty)
{	
	if (!isset($params['file'])) {
	
		$smarty->trigger_error("eval: missing 'file' parameter");
		return;
		
	} else {
		
		// Run any localisation strings through the translation engine
		$smarty->assign("form_title_text", preg_replace_callback('/\$([a-zA-Z0-9_]*)/', 'pixSmartyMLVP_Callback', $params['form_title_text']));
		
		// Security
		if (eregi("http://",$params['file'])) { return; }
		if (eregi("ftp://",$params['file'])) { return; }
		
		$result = $smarty->pixFetch($params['file']);
		//$result = smartyPixFetch($params['file']);
		
	}
	
	return $result;
}

/*
*	This is a Smarty extension function used to load Pixies
*	It is not yet integrated and should not be used
*/
function smartyLoadPixie($params, &$smarty) {

	global $cfg, $ses;

	if (!isset($params['name'])) {
	
		$smarty->trigger_error("eval: Missing 'name' parameter");
		return;
		
	} elseif (!is_dir(SYS_BASE_PATH."resources/pixies/".$params['name'])) {
		
		$smarty->trigger_error("eval: Requested Pixie does not exist");
		return;
		
	} else {
		
		// Set the path to this Pixie
		$pixie_dir = SYS_BASE_PATH."resources/pixies/".$params['name']."/";
		
		// Get the options for this Pixie
		$pixie_params = explode(":",$params['options']);
		
		// Security
		if (eregi("http://",$pixie_dir)) { return; }
		if (eregi("ftp://",$pixie_dir)) { return; }
		
		// Include the main Pixie file
		include($pixie_dir . "pixie.logic.php");
		
	}
	
	return $pixie_return;
	
}

/*
*
*	
*
*/
function smartyFancySelectMultiple ($params, &$smarty) {
	
    require_once $smarty->_get_plugin_filepath('shared','escape_special_chars');

    foreach($params as $_key => $_val) {
    
        switch($_key) {
        
       		case 'default_option':
       		case 'default_value':
       		case 'name':
       		case 'rows':
       		case 'width':
        		if ($_val != "") {
        			$$_key = $_val;
        		} else {
                    $smarty->trigger_error("html_fancy_select_multiple: attribute '$_key' must be present", E_USER_NOTICE);
        		}
        	break;
        	
 			case 'selected':
                if(is_array($_val)) {
                    $$_key = smarty_function_escape_special_chars($_val);
                } elseif ($_val == "") {
                	$$_key = array();
                } else {
                    $smarty->trigger_error("html_fancy_select_multiple: extra attribute '$_key' must be an array", E_USER_NOTICE);
                }
 			break;
 			
 			case 'options':
 			case 'values':
                if(is_array($_val)) {
                    $$_key = smarty_function_escape_special_chars($_val);
                } else {
                    $smarty->trigger_error("html_fancy_select_multiple: extra attribute '$_key' must be an array", E_USER_NOTICE);
                }
            break;
            
        }
        
	}
	
	if ($default_option != "") {
		
		if ($selected == "") { $selected = $default_value; }
		
		$default_option = array($default_option);
		$default_value	= array($default_value);
		
		array_merge($default_option,$options);
		array_merge($default_value,$values);
		
	}
	
	if (count($options) < $rows) {
	
		for ($i=0; $i < $rows; $i++) {
			$row[] = $i;
		}
	
	} else {
	
		for ($i=0; $i < count($options); $i++) {
			$row[] = $i;
		}
	
	}
	
	if (is_numeric($rows)) {
		
		$height = $rows * 25;
	
	}
	
	$smarty->assign("fsm_name",$name);
	$smarty->assign("fsm_rows",$row);
	$smarty->assign("fsm_width",$width);
	$smarty->assign("fsm_height",$height);
	$smarty->assign("fsm_selected",$selected);
	$smarty->assign("fsm_options",$options);
	$smarty->assign("fsm_values",$values);
	
	$template = "admin.snippets/fancy.select.multiple.html";
	
	$smarty->display($template);

}

/*
*
*	
*
*/
function smartyFancySelectSingle ($params, &$smarty) {
	
    require_once $smarty->_get_plugin_filepath('shared','escape_special_chars');

    foreach($params as $_key => $_val) {
    
        switch($_key) {
        
       		case 'default_option':
       		case 'default_value':
       		case 'name':
       		case 'rows':
       		case 'width':
        		if ($_val != "") {
        			$$_key = $_val;
        		} else {
                    $smarty->trigger_error("html_fancy_select_single: attribute '$_key' must be present", E_USER_NOTICE);
        		}
        	break;
        	
 			case 'selected':
                if ($_val != "") {
                	$$_key = $_val;
                } else {
                    $$_key = "";
                }
 			break;
 			
 			case 'options':
 			case 'values':
                if(is_array($_val)) {
                    $$_key = smarty_function_escape_special_chars($_val);
                } else {
                    $smarty->trigger_error("html_fancy_select_single: extra attribute '$_key' must be an array", E_USER_NOTICE);
                }
            break;
            
        }
        
	}
	
	if ($default_option != "") {
		
		if ($selected == "") { $selected = $default_value; }
		
		$default_option = array($default_option);
		$default_value	= array($default_value);
		
		$options 		= array_merge($default_option,$options);
		$values 		= array_merge($default_value,$values);
		
	}

	if (count($options) < $rows) {
	
		for ($i=0; $i < $rows; $i++) {
			$row[] = $i;
		}
	
	} else {
	
		for ($i=0; $i < count($options); $i++) {
			$row[] = $i;
		}
	
	}
	
	if (is_numeric($rows)) {
		
		$height = $rows * 25;
	
	}
	
	if ($selected == "") { $selected = $values[0]; }
	
	$smarty->assign("fss_name",$name);
	$smarty->assign("fss_rows",$row);
	$smarty->assign("fss_width",$width);
	$smarty->assign("fss_height",$height);
	$smarty->assign("fss_selected",$selected);
	$smarty->assign("fss_options",$options);
	$smarty->assign("fss_values",$values);
	
	$template = "admin.snippets/fancy.select.single.html";
	
	$smarty->display($template);

}

/*
*	Checks to see if template file is in the theme directory and
*	if it isn't loads the default template file instead
*/
function smartyPixDisplay($template) {
	
	global $cfg, $smarty, $admin_page;
	
	// Security
	if (eregi("http://",$template)) { return; }
	if (eregi("ftp://",$template)) { return; }
	
	if ($admin_page) {
	
		$smarty->display($template);
		
	} else {
	
		if (file_exists(SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/templates/".$template)) {
			
			$smarty->display(SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/templates/".$template);
		
		} else {
			
			$smarty->display($template);
		
		}
	
	}
	
}

/*
*	Checks to see if template file is in the theme directory and
*	if it isn't loads the default template file instead
*/
function smartyPixFetch($template) {
	
	global $cfg, $smarty, $admin_page;
	
	// Security
	if (eregi("http://",$template)) { return; }
	if (eregi("ftp://",$template)) { return; }
	
	if ($admin_page) {
	
		$result = $smarty->fetch($template);
		
	} else {
	
		if (file_exists(SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/templates/".$template)) {
			
			$result = $smarty->fetch(SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/templates/".$template);
		
		} else {
			
			$result = $smarty->fetch($template);
		
		}
	
	}
	
	return $result;
	
}

/**
 * Smarty truncate modifier plugin
 *
 * Type:     modifier<br>
 * Name:     truncate<br>
 * Purpose:  Truncate a string to a certain length if necessary,
 *           optionally splitting in the middle of a word, and
 *           appending the $etc string or inserting $etc into the middle.
 * @link http://smarty.php.net/manual/en/language.modifier.truncate.php
 *          truncate (Smarty online manual)
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @param string
 * @param integer
 * @param string
 * @param boolean
 * @param boolean
 * @return string
 */
function textTruncate ($string, $length = 80, $etc = '...', $break_words = false, $middle = false) {
    if ($length == 0)
        return '';

    if (strlen($string) > $length) {
        $length -= min($length, strlen($etc));
        if (!$break_words && !$middle) {
            $string = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $length+1));
        }
        if(!$middle) {
            return substr($string, 0, $length) . $etc;
        } else {
            return substr($string, 0, $length/2) . $etc . substr($string, -$length/2);
        }
    } else {
        return $string;
    }
}



/*
*
* 	Convert a numeric decimal string into a two sig fig decimal string
*	e.g. 120.0 -> 120.00
*
*/
function getSignificantFigure($var) {

	$var = round($var,2);
	
	list ($base,$decimal) = split('[/.-]', $var);
	
	$newvar = $base . "." . str_pad($decimal, 2, "0", STR_PAD_RIGHT);
	
	return($newvar);

}

/*
*
*	Get SQL to limit the number of image thumbs to the current page
*
*/
function getMultiImagePageLimitSQL ($page = "") {
	
	global $cfg;
	
	// This statement ensure we only load image data for the page we're on
	if (!$page) {
		
		$results_lim_sta 	= 0;
		$results_lim_end	= $cfg['set']['thumb_per_page'];
		
	} else {
		
		$results_lim_sta 	= ($page - 1) * $cfg['set']['thumb_per_page'];
		$results_lim_end	= $cfg['set']['thumb_per_page'];
		
	}
	
	return " LIMIT $results_lim_sta, $results_lim_end";

}

/*
*
*
*
*/
function getMultiImagePageNavigation ($mode, $current_page, $total_items, $query_string = "") {
	
	global $cfg;
	
	if ($current_page == ""): $current_page = 1; endif;
	
	$images 		= $total_items;
	$items			= $cfg['set']['thumb_per_page'];
	$pages			= ceil($images / $items);
	$this_page		= $current_page;
	
	// Get the page number of the next and previous pages
	if ($current_page == 1) { $previous_page = 1; } else { $previous_page = $current_page - 1; }
	if ($current_page == $pages) { $next_page = $pages; } else { $next_page = $current_page + 1; }
	
	switch ($mode) {
	
		case "cms_images":
			
			/*
			*	Set the 'previous' page text and link
			*/
			$page[] = "&laquo; Previous";
			$href[] = $cfg['sys']['base_url'].FILE_PUB_CMS."?page=".$previous_page;
			
			for ($i = 1; $i <= $pages; $i++) {
			
				$page[] = $i;
				$href[] = $cfg['sys']['base_url'].FILE_PUB_CMS."?page=".$i;
				
			}
			
			/*
			*	Set the 'next' page text and link
			*/
			$page[] = "Next &raquo;";
			$href[] = $cfg['sys']['base_url'].FILE_PUB_CMS."?page=".$next_page;
			
			return paginationTidy($current_page, $page, $href, $pages, null);
		
		break;
		
		case "orphans":
			
			/*
			*	Set the 'previous' page text and link
			*/
			$page[] = "&laquo; Previous";
			$href[] = $cfg['sys']['base_url'].FILE_ADM_IMAGE."?page=".$previous_page;
			
			for ($i = 1; $i <= $pages; $i++) {
			
				$page[] = $i;
				$href[] = $cfg['sys']['base_url'].FILE_ADM_IMAGE."?page=".$i;
				
			}
			
			/*
			*	Set the 'next' page text and link
			*/
			$page[] = "Next &raquo;";
			$href[] = $cfg['sys']['base_url'].FILE_ADM_IMAGE."?page=".$next_page;
			
			return paginationTidy($current_page, $page, $href, $pages, null);
		
		break;
		
		case "users": case "user_images":
			
			/*
			*	Set the 'previous' page text and link
			*/
			$page[] = "&laquo; Previous";
			$href[] = $cfg['sys']['base_url'].FILE_ADM_USER."?page=".$previous_page."&amp;$query_string";
			
			for ($i = 1; $i <= $pages; $i++) {
			
				$page[] = $i;
				$href[] = $cfg['sys']['base_url'].FILE_ADM_USER."?page=".$i."&amp;$query_string";
				
			}
			
			/*
			*	Set the 'next' page text and link
			*/
			$page[] = "Next &raquo;";
			$href[] = $cfg['sys']['base_url'].FILE_ADM_USER."?page=".$next_page."&amp;$query_string";
			
			return paginationTidy($current_page, $page, $href, $pages, null);
		
		break;
				
	}
		
}

/*
*
*
*
*/
function captchaKey () {
	
	$key = md5(uniqid('G'));
	
	list($timestamp) = sql_select_row("SELECT CURRENT_TIMESTAMP AS timestamp_now");
	
	@mysql_query("DELETE FROM ".PIX_TABLE_CAPT." WHERE DATE_ADD(date, INTERVAL 30 MINUTE) < '$timestamp'");
	
	@mysql_query("INSERT INTO ".PIX_TABLE_CAPT." VALUES ( '0', '$key', '', NOW() )");
	
	return $key;
	
}

/*
*
*	This method slims down long lists of page numbers to a manageable size
*
*/
function paginationTidy ($current_page, $page, $href, $pages, $base_page) {
	
	$pagination_count = count($page);
	
	if ($pagination_count >= 11 && $current_page < 6) {
		
		$z = $pagination_count - 1;
		$y = $pagination_count - 2;
		$x = $pagination_count - 3;
		$w = $pagination_count - 4;
		
		$page_new[] = $page[0];
		$page_new[] = $page[1];
		$page_new[] = $page[2];
		$page_new[] = $page[3];
		$page_new[] = $page[4];
		$page_new[] = $page[5];
		$page_new[] = $page[6];
		
		$page_new[] = "...";
		
		$page_new[] = $page[$w];
		$page_new[] = $page[$x];
		$page_new[] = $page[$y];
		$page_new[] = $page[$z];
		
		
		$href_new[] = $href[0];
		$href_new[] = $href[1];
		$href_new[] = $href[2];
		$href_new[] = $href[3];
		$href_new[] = $href[4];
		$href_new[] = $href[5];
		$href_new[] = $href[6];

		$href_new[] = "";

		$href_new[] = $href[$w];
		$href_new[] = $href[$x];
		$href_new[] = $href[$y];
		$href_new[] = $href[$z];
		
		return array($current_page, $page_new, $href_new, $pages, $base_page);
	
	} elseif ($pagination_count >= 11 && ($current_page > $pagination_count - 6)) {
		
		$z = $pagination_count - 1;
		$y = $pagination_count - 2;
		$x = $pagination_count - 3;
		$w = $pagination_count - 4;
		$v = $pagination_count - 5;
		$u = $pagination_count - 6;
		$t = $pagination_count - 7;
		
		$page_new[] = $page[0];
		$page_new[] = $page[1];
		$page_new[] = $page[2];
		$page_new[] = $page[3];
		
		$page_new[] = "...";
		
		$page_new[] = $page[$t];
		$page_new[] = $page[$u];
		$page_new[] = $page[$v];
		$page_new[] = $page[$w];
		$page_new[] = $page[$x];
		$page_new[] = $page[$y];
		$page_new[] = $page[$z];

		$href_new[] = $href[0];
		$href_new[] = $href[1];
		$href_new[] = $href[2];
		$href_new[] = $href[3];

		$href_new[] = "";

		$href_new[] = $href[$t];
		$href_new[] = $href[$u];
		$href_new[] = $href[$v];
		$href_new[] = $href[$w];
		$href_new[] = $href[$x];
		$href_new[] = $href[$y];
		$href_new[] = $href[$z];
		
		return array($current_page, $page_new, $href_new, $pages, $base_page);
	
	} elseif (($current_page > 3 && $current_page < ($pagination_count - 2)) && $pagination_count > 8) {
		
		$i = $current_page - 1;
		$j = $current_page;
		$k = $current_page + 1;
		
		$z = $pagination_count - 1;
		$y = $pagination_count - 2;
		$x = $pagination_count - 3;
		$w = $pagination_count - 4;
		
		/* Pages */
		$page_new[] = $page[0];
		$page_new[] = $page[1];
		$page_new[] = $page[2];
		$page_new[] = $page[3];
		
		$page_new[] = "...";
		
		$page_new[] = $page[$i];
		$page_new[] = $page[$j];
		$page_new[] = $page[$k];
		
		$page_new[] = "...";
		
		$page_new[] = $page[$w];
		$page_new[] = $page[$x];
		$page_new[] = $page[$y];
		$page_new[] = $page[$z];
		
		/* Links */
		$href_new[] = $href[0];
		$href_new[] = $href[1];
		$href_new[] = $href[2];
		$href_new[] = $href[3];

		$href_new[] = "";
		
		$href_new[] = $href[$i];
		$href_new[] = $href[$j];
		$href_new[] = $href[$k];
		
		$href_new[] = "";

		$href_new[] = $href[$w];
		$href_new[] = $href[$x];
		$href_new[] = $href[$y];
		$href_new[] = $href[$z];
		
		return array($current_page, $page_new, $href_new, $pages, $base_page);
	
	} else {
		
		return array($current_page, $page, $href, $pages, $base_page);
	
	}

}

/*
*
*
*	'213.232.110.0 - 213.232.110.255'
*	'213.232.92.0 - 213.232.95.255'
*	'83.170.64.0 - 83.170.127.255'
*	'77.92.64.1 - 77.92.95.255'
*	'84.45.121.1 - 84.45.123.255'
*	
*
*/
function internetProtocolControl() {
	
	$ip = array();
	
	$ip[]	= "213.232.110.*";
	
	$ip[]	= "213.232.92.*";
	$ip[]	= "213.232.93.*";
	$ip[]	= "213.232.94.*";
	$ip[]	= "213.232.95.*";
	
	$ip[]	= "83.170.64.*";
	$ip[]	= "83.170.65.*";
	$ip[]	= "83.170.66.*";
	$ip[]	= "83.170.67.*";
	$ip[]	= "83.170.68.*";
	$ip[]	= "83.170.69.*";
	$ip[]	= "83.170.7*.*";
	$ip[]	= "83.170.8*.*";
	$ip[]	= "83.170.9*.*";
	$ip[]	= "83.170.10*.*";
	$ip[]	= "83.170.120.*";
	$ip[]	= "83.170.121.*";
	$ip[]	= "83.170.122.*";
	$ip[]	= "83.170.123.*";
	$ip[]	= "83.170.124.*";
	$ip[]	= "83.170.125.*";
	$ip[]	= "83.170.126.*";
	$ip[]	= "83.170.127.*";
	
	$ip[]	= "77.92.64.*";
	$ip[]	= "77.92.65.*";
	$ip[]	= "77.92.66.*";
	$ip[]	= "77.92.67.*";
	$ip[]	= "77.92.68.*";
	$ip[]	= "77.92.7*.*";
	$ip[]	= "77.92.8*.*";
	$ip[]	= "77.92.90.*";
	$ip[]	= "77.92.91.*";
	$ip[]	= "77.92.92.*";
	$ip[]	= "77.92.93.*";
	$ip[]	= "77.92.94.*";
	$ip[]	= "77.92.95.*";
	
	$ip[]	= "84.45.121.*";
	$ip[]	= "84.45.122.*";
	$ip[]	= "84.45.123.*";
	
	for($i=0, $cnt=count($ip); $i<$cnt; $i++) {
	
		$ipregex = preg_replace("/\./", "\.", $ip[$i]);
		$ipregex = preg_replace("/\*/", ".*", $ipregex);

		if(preg_match('/'.$ipregex.'/', $_SERVER[SERVER_ADDR])) {
		
			return true;
			
		
		}
		
	}
	
	print("<h1>Pixaria cannot run on this server.</h1>Please go to http://www.pixaria.com/ for the latest edition of this software or contact UK2.NET technical support for assistance.");
	exit;

}

/*
*
*/
function scriptTimer ($line) {

	$mtime = microtime(); 
	$mtime = explode(" ",$mtime); 
	$mtime = $mtime[1] + $mtime[0]; 
	$endtime = $mtime; 
	$totaltime = ($endtime - START_TIME); 
	echo "\n\n<!-- TIME @ LINE $line: ".round($totaltime,4)." seconds -->\n\n";
	
}


?>