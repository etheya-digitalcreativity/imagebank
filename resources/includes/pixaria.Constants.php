<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	Warning - settings in this file should not be modified
*	Changes made to this file may be overridden by Pixaria Gallery
*
*/

/*** DEPRECATED ***/
// Define database table names as variables
$cfg['sys']['table_blog'] = "psg_blog";
$cfg['sys']['table_caar'] = "psg_calc_arithmetic";
$cfg['sys']['table_caob'] = "psg_calc_objects";
$cfg['sys']['table_catg'] = "psg_categories";
$cfg['sys']['table_came'] = "psg_categories_members";
$cfg['sys']['table_cart'] = "psg_cart";
$cfg['sys']['table_crme'] = "psg_cart_members";
$cfg['sys']['table_domn'] = "psg_banned_domains";
$cfg['sys']['table_gall'] = "psg_galleries";
$cfg['sys']['table_gord'] = "psg_gallery_order";
$cfg['sys']['table_gvie'] = "psg_gallery_view";
$cfg['sys']['table_grps'] = "psg_groups";
$cfg['sys']['table_grme'] = "psg_groups_members";
$cfg['sys']['table_imgs'] = "psg_images";
$cfg['sys']['table_imvi'] = "psg_images_viewers";
$cfg['sys']['table_lbox'] = "psg_lightboxes";
$cfg['sys']['table_lbme'] = "psg_lightbox_members";
$cfg['sys']['table_psdl'] = "psg_payscript_log";
$cfg['sys']['table_sett'] = "psg_settings";
$cfg['sys']['table_sgrp'] = "psg_sys_groups";
$cfg['sys']['table_sgme'] = "psg_sys_groups_members";
$cfg['sys']['table_slog'] = "psg_search_log";
$cfg['sys']['table_tmsg'] = "psg_txn_messages";
$cfg['sys']['table_ulog'] = "psg_user_log";
$cfg['sys']['table_user'] = "psg_users";
$cfg['sys']['table_usda'] = "psg_users_data";

// Define database table names as constants
define("PIX_TABLE_BLOG","psg_blog");
define("PIX_TABLE_CAAR","psg_calc_arithmetic");
define("PIX_TABLE_CAOB","psg_calc_objects");
define("PIX_TABLE_CATG","psg_categories");
define("PIX_TABLE_CAME","psg_categories_members");
define("PIX_TABLE_CAPT","psg_captcha");
define("PIX_TABLE_CART","psg_cart");
define("PIX_TABLE_CHAR","psg_charset");
define("PIX_TABLE_CTRY","psg_country");
define("PIX_TABLE_CRME","psg_cart_members");
define("PIX_TABLE_DGAL","psg_divestock_gallery");
define("PIX_TABLE_DLOG","psg_download_log");
define("PIX_TABLE_DOMN","psg_banned_domains");
define("PIX_TABLE_GALL","psg_galleries");
define("PIX_TABLE_GORD","psg_gallery_order");
define("PIX_TABLE_GVIE","psg_gallery_view");
define("PIX_TABLE_GRPS","psg_groups");
define("PIX_TABLE_GRME","psg_groups_members");
define("PIX_TABLE_IMGS","psg_images");
define("PIX_TABLE_IMVI","psg_images_viewers");
define("PIX_TABLE_IPRD","psg_images_products");
define("PIX_TABLE_KTAG","psg_keywords_tags");
define("PIX_TABLE_KIMG","psg_keywords_images");
define("PIX_TABLE_LBOX","psg_lightboxes");
define("PIX_TABLE_LBME","psg_lightbox_members");
define("PIX_TABLE_PREF","psg_preference");
define("PIX_TABLE_PROD","psg_products");
define("PIX_TABLE_PSDL","psg_payscript_log");
define("PIX_TABLE_SETT","psg_settings");
define("PIX_TABLE_SGRP","psg_sys_groups");
define("PIX_TABLE_SGME","psg_sys_groups_members");
define("PIX_TABLE_SLOG","psg_search_log");
define("PIX_TABLE_SRCH","psg_search");
define("PIX_TABLE_SRCS","psg_search_storage");
define("PIX_TABLE_TIMG","psg_tags_images");
define("PIX_TABLE_TAGS","psg_tags");
define("PIX_TABLE_TMSG","psg_txn_messages");
define("PIX_TABLE_ULOG","psg_user_log");
define("PIX_TABLE_UPRF","psg_user_preference");
define("PIX_TABLE_USER","psg_users");
define("PIX_TABLE_USDA","psg_users_data");


/*** DEPRECATED ***/
//	Script filenames (admin area)
$cfg['fil']['admin_appearance'] 		= "resources/admin/settings.php?cmd=appearance";
$cfg['fil']['admin_blog'] 				= "resources/admin/admin.blog.php";
$cfg['fil']['admin_calculators'] 		= "resources/admin/admin.calculators.php";
$cfg['fil']['admin_category'] 			= "resources/admin/admin.category.php";
$cfg['fil']['admin_dashboard']			= "resources/admin/dashboard.php";
$cfg['fil']['admin_diagnostics']		= "resources/admin/dashboard.php?cmd=status";
$cfg['fil']['admin_divestock']			= "resources/admin/divestock.php";
$cfg['fil']['admin_export']				= "resources/admin/export.php";
$cfg['fil']['admin_gallery'] 			= "resources/admin/gallery.php";
$cfg['fil']['admin_groups'] 			= "resources/admin/admin.groups.php";
$cfg['fil']['admin_image'] 				= "resources/admin/image.php";
$cfg['fil']['admin_image_find']	 		= "resources/admin/admin.image.find.php";
$cfg['fil']['admin_image_output'] 		= "resources/admin/admin.image.output.php";
$cfg['fil']['admin_import'] 			= "resources/admin/import.php";
$cfg['fil']['admin_jclient'] 			= "resources/admin/admin.jclient.php";
$cfg['fil']['admin_library'] 			= "resources/admin/admin.library.php";
$cfg['fil']['admin_library_add'] 		= "resources/admin/admin.library.add.php";
$cfg['fil']['admin_library_ftp'] 		= "resources/admin/admin.library.files.php";
$cfg['fil']['admin_library_import'] 	= "resources/admin/admin.library.import.php";
$cfg['fil']['admin_library_search'] 	= "resources/admin/admin.library.search.php";
$cfg['fil']['admin_library_upload'] 	= "resources/admin/admin.library.upload.php";
$cfg['fil']['admin_lightbox'] 			= "resources/admin/lightbox.php";
$cfg['fil']['admin_log'] 				= "resources/admin/admin.log.php";
$cfg['fil']['admin_log_keywords'] 		= "resources/admin/admin.log.keywords.php";
$cfg['fil']['admin_log_login'] 			= "resources/admin/admin.log.login.php";
$cfg['fil']['admin_log_payscripts'] 	= "resources/admin/admin.log.php?cmd=showPaymentScriptLog";
$cfg['fil']['admin_pixie'] 				= "resources/admin/admin.pixie.php";
$cfg['fil']['admin_preferences'] 		= "resources/admin/preferences.php";
$cfg['fil']['admin_products'] 			= "resources/admin/admin.products.php";
$cfg['fil']['admin_sales'] 				= "resources/admin/admin.sales.php";
$cfg['fil']['admin_settings'] 			= "resources/admin/settings.php";
$cfg['fil']['admin_settings_login']		= "resources/admin/settings.php?cmd=login";
$cfg['fil']['admin_settings_search']	= "resources/admin/settings.php?cmd=search";
$cfg['fil']['admin_software']			= "resources/admin/dashboard.php?cmd=status";
$cfg['fil']['admin_status']				= "resources/admin/dashboard.php?cmd=status";
$cfg['fil']['admin_store']		 		= "resources/admin/admin.store.php";
$cfg['fil']['admin_store_sales']		= "resources/admin/admin.store.php?cmd=salesView";
$cfg['fil']['admin_store_settings']		= "resources/admin/admin.store.php?cmd=settingsView";
$cfg['fil']['admin_store_report'] 		= "resources/admin/admin.store.php?cmd=salesReportForm";
$cfg['fil']['admin_syndication'] 		= "resources/admin/admin.syndication.php";
$cfg['fil']['admin_transaction'] 		= "resources/admin/admin.transaction.php";
$cfg['fil']['admin_upgrade'] 			= "resources/admin/admin.upgrade.php";
$cfg['fil']['admin_upload'] 			= "resources/admin/upload.php";
$cfg['fil']['admin_user'] 				= "resources/admin/admin.user.php";
$cfg['fil']['admin_users'] 				= "resources/admin/admin.users.php";
$cfg['fil']['admin_user_data'] 			= "resources/admin/admin.userdata.settings.php";
$cfg['fil']['admin_user_edit'] 			= "resources/admin/admin.user.edit.php";
$cfg['fil']['admin_user_settings'] 		= "resources/admin/admin.user.settings.php";

// Define admin area filenames
define("FILE_ADM_APPEARANCE","resources/admin/settings.php?cmd=appearance");
define("FILE_ADM_BLOG","resources/admin/admin.blog.php");
define("FILE_ADM_CALCULATORS","resources/admin/admin.calculators.php");
define("FILE_ADM_CATEGORY","resources/admin/admin.category.php");
define("FILE_ADM_DASHBOARD","resources/admin/dashboard.php");
define("FILE_ADM_DIAGNOSTICS","resources/admin/dashboard.php?cmd=status");
define("FILE_ADM_DIVESTOCK","resources/admin/divestock.php");
define("FILE_ADM_EXPORT","resources/admin/export.php");
define("FILE_ADM_GALLERY","resources/admin/gallery.php");
define("FILE_ADM_GROUPS","resources/admin/admin.groups.php");
define("FILE_ADM_IMAGE","resources/admin/image.php");
define("FILE_ADM_IMAGE_FIND","resources/admin/admin.image.find.php");
define("FILE_ADM_IMAGE_OUPUT","resources/admin/admin.image.output.php");
define("FILE_ADM_IMPORT","resources/admin/import.php");
define("FILE_ADM_JCLIENT","resources/admin/admin.jclient.php");
define("FILE_ADM_LIBRARY","resources/admin/admin.library.php");
define("FILE_ADM_LIBRARY_ADD","resources/admin/admin.library.add.php");
define("FILE_ADM_LIBRARY_FTP","resources/admin/admin.library.files.php");
define("FILE_ADM_LIBRARY_IMPORT","resources/admin/admin.library.import.php");
define("FILE_ADM_LIBRARY_SEARCH","resources/admin/admin.library.search.php");
define("FILE_ADM_LIBRARY_UPLOAD","resources/admin/admin.library.upload.php");
define("FILE_ADM_LIGHTBOX","resources/admin/lightbox.php");
define("FILE_ADM_LOG","resources/admin/admin.log.php");
define("FILE_ADM_LOG_KEYWORDS","resources/admin/admin.log.keywords.php");
define("FILE_ADM_LOG_LOGIN","resources/admin/admin.log.login.php");
define("FILE_ADM_LOG_PAYSCRIPTS","resources/admin/admin.log.php?cmd=showPaymentScriptLog");
define("FILE_ADM_PIXIE","resources/admin/admin.pixie.php");
define("FILE_ADM_PREFERENCES","resources/admin/preferences.php");
define("FILE_ADM_PRODUCTS","resources/admin/admin.products.php");
define("FILE_ADM_SALES","resources/admin/admin.sales.php");
define("FILE_ADM_SALES_REPORT","resources/admin/admin.sales.report.php");
define("FILE_ADM_SETTINGS","resources/admin/settings.php");
define("FILE_ADM_SETTINGS_LOGIN","resources/admin/settings.php?cmd=login");
define("FILE_ADM_SETTINGS_SEARCH","resources/admin/settings.php?cmd=search");
define("FILE_ADM_SOFTWARE","resources/admin/dashboard.php?cmd=status");
define("FILE_ADM_STATUS","resources/admin/dashboard.php?cmd=status");
define("FILE_ADM_STORE","resources/admin/admin.store.php");
define("FILE_ADM_STORE_SALES","resources/admin/admin.store.php?cmd=salesView");
define("FILE_ADM_STORE_SETTINGS","resources/admin/admin.store.php?cmd=settingsView");
define("FILE_ADM_SYNDICATION","resources/admin/admin.syndication.php");
define("FILE_ADM_TRANSACTION","resources/admin/admin.transaction.php");
define("FILE_ADM_UPGRADE","resources/admin/admin.upgrade.php");
define("FILE_ADM_UPLOAD","resources/admin/upload.php");
define("FILE_ADM_USER","resources/admin/admin.user.php");
define("FILE_ADM_USERS","resources/admin/admin.users.php");
define("FILE_ADM_USERDATA_SETTINGS","resources/admin/admin.userdata.settings.php");
define("FILE_ADM_USER_EDIT","resources/admin/admin.user.edit.php");
define("FILE_ADM_USER_SETTINGS","resources/admin/admin.user.settings.php");


/*** DEPRECATED ***/
//	Script filenames (front end)
$cfg['fil']['index'] 					= "index.php";
$cfg['fil']['index_account'] 			= "index.account.php";
$cfg['fil']['index_account_create'] 	= "index.account.create.php";
$cfg['fil']['index_account_edit'] 		= "index.account.edit.php";
$cfg['fil']['index_account_lightbox'] 	= "index.account.lightbox.php";
$cfg['fil']['index_account_reset'] 		= "index.account.reset.php";
$cfg['fil']['index_calculator'] 		= "index.calculator.php";
$cfg['fil']['index_cart'] 				= "index.store.php";
$cfg['fil']['index_cms'] 				= "index.cms.php";
$cfg['fil']['index_contact'] 			= "index.contact.php";
$cfg['fil']['index_gallery'] 			= "index.gallery.php";
$cfg['fil']['index_group'] 				= "index.group.php";
$cfg['fil']['index_image'] 				= "index.image.php";
$cfg['fil']['index_lightbox'] 			= "index.lightbox.php";
$cfg['fil']['index_login'] 				= "index.login.php";
$cfg['fil']['index_news'] 				= "index.news.php";
$cfg['fil']['index_news_item'] 			= "index.news.item.php";
$cfg['fil']['psg_paypal']				= "index.paypal.php";
$cfg['fil']['index_postcard'] 			= "index.postcard.php";
$cfg['fil']['index_postcard_image'] 	= "index.postcard.image.php";
$cfg['fil']['index_rpc'] 				= "index.rpc.php";
$cfg['fil']['index_rss'] 				= "index.rss.php";
$cfg['fil']['index_search'] 			= "index.search.php";
$cfg['fil']['index_store'] 				= "index.store.php";
$cfg['fil']['index_transaction'] 		= "index.transaction.php";
$cfg['fil']['psg_image_thumbnail']		= "pixaria.thumbnail.php";
$cfg['fil']['psg_image_comping']		= "pixaria.image.php";

// Define public area filenames
define("FILE_PUB_INDEX","index.php");
define("FILE_PUB_ACCOUNT","index.account.php");
define("FILE_PUB_ACCOUNT_CREATE","index.account.create.php");
define("FILE_PUB_ACCOUNT_EDIT","index.account.edit.php");
define("FILE_PUB_ACCOUNT_LIGHTBOX","index.account.lightbox.php");
define("FILE_PUB_ACCOUNT_RESET","index.account.reset.php");
define("FILE_PUB_CALCULATOR","index.calculator.php");
define("FILE_PUB_CMS","index.cms.php");
define("FILE_PUB_CONTACT","index.contact.php");
define("FILE_PUB_GALLERY","index.gallery.php");
define("FILE_PUB_GROUP","index.group.php");
define("FILE_PUB_IMAGE","index.image.php");
define("FILE_PUB_LIGHTBOX","index.lightbox.php");
define("FILE_PUB_LOGIN","index.login.php");
define("FILE_PUB_NEWS","index.news.php");
define("FILE_PUB_NEWS_ITEM","index.news.item.php");
define("FILE_PUB_PAYPAL","index.paypal.php");
define("FILE_PUB_POSTCARD","index.postcard.php");
define("FILE_PUB_POSTCARD_IMAGE","index.postcard.image.php");
define("FILE_PUB_RPC","index.rpc.php");
define("FILE_PUB_RSS","index.rss.php");
define("FILE_PUB_SEARCH","index.search.php");
define("FILE_PUB_STORE","index.store.php");
define("FILE_PUB_TRANSACTION","index.transaction.php");
define("FILE_PUB_IMAGE_THUMBNAIL","pixaria.thumbnail.php");
define("FILE_PUB_IMAGE_COMPING","pixaria.image.php");

/*	Software name */
$cfg['sys']['software_name']			= "Pixaria Gallery";

// Current software version
$cfg['set']['software_version']			= "2.7.4";		

// Expected number of database tables
$cfg['set']['table_count']				= "34";		

$cfg['sys']['safe_mode']				= ini_get('safe_mode');			

/*
*	
*	DO NOT CHANGE THESE VALUES ON A WORKING INSTALLATION
*	OF PIXARIA AS YOU RISK IRRETRIEVABLY BREAKING YOUR SITE
*	
*/
// Defines the maximum width and height of 'comping' images
define("COMPING_SIZE","630");
define("LARGE_COMPING_SIZE",COMPING_SIZE);
define("SMALL_COMPING_SIZE","320");

$cfg['sys']['comping_size'] = LARGE_COMPING_SIZE;
$cfg['sys']['large_comping_size'] = LARGE_COMPING_SIZE;
$cfg['sys']['small_comping_size'] = SMALL_COMPING_SIZE;

define("COMPING_DIR",COMPING_SIZE."x".COMPING_SIZE);
define("LARGE_COMPING_DIR",LARGE_COMPING_SIZE."x".LARGE_COMPING_SIZE);
define("SMALL_COMPING_DIR",SMALL_COMPING_SIZE."x".SMALL_COMPING_SIZE);

$cfg['sys']['comping_dir'] = COMPING_DIR;
$cfg['sys']['large_comping_dir'] = LARGE_COMPING_DIR;
$cfg['sys']['small_comping_dir'] = SMALL_COMPING_DIR;


?>