<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

// Load the main Smarty class
require_once ("class.PixariaMultiImage.php");

class PixariaGallery extends PixariaMultiImage {
	
	var $_dbl;
	var $_update_flag		= false;
	
	// Core gallery properties
	var $gallery_id;
	var $gallery_active;
	var $gallery_archived;
	var $gallery_page;
	var $gallery_items;
	var $gallery_image;
	
	var $gallery_date;
	var $gallery_parent;
	var $gallery_type;
	var $gallery_userid;
	var $gallery_title;
	var $gallery_short_title;
	var $gallery_description;
	var $gallery_password;
	var $gallery_permissions;
	var $gallery_key;
	var $gallery_created;
	var $gallery_edited;
	var $gallery_counter;
	var $gallery_image_count;
	var $gallery_child_count;
	var $gallery_hierarchy;
	var $gallery_image_data;
	
	function PixariaGallery ($gallery_id, $gallery_page = "", $multi_image_page = "", $gallery_image = "", $multi_image_show_all = false) {
		
		global $cfg, $ses;
		
		$this->multi_image_type			= "gallery";
		$this->multi_image_page			= $multi_image_page;
		$this->gallery_page				= $gallery_page;
		$this->multi_image_image		= $gallery_image;
		$this->multi_image_show_all		= $multi_image_show_all;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		// Load the gallery data access object
		require_once ('class.DAO.PixariaGallery.php');
		
		/*
		*	Check if this gallery is being accessed via a mod_rewrite short name
		*	If it is, then look up the real gallery_id from the short name
		*/
		if ($gallery_id != "" && !is_numeric($gallery_id)) { // Look up the short name
			
			$sql = "SELECT * FROM `".PIX_TABLE_GALL."` WHERE gallery_short_title = '".$this->_dbl->escape($gallery_id)."'";
			
			$result = $this->_dbl->row($sql);
			
			if (is_numeric($result['gallery_id'])) {
				
				$this->gallery_id = $result['gallery_id'];
			
			} else {
			
				$this->gallery_id = "";
				
			}
			
		} else {
		
			$this->gallery_id = $gallery_id; // The gallery ID number
			
		}

		// Create the gallery data access object
		$objPixariaGallery = new DAO_PixariaGallery($this->gallery_id);
		
		// If there is a numeric gallery ID set then we will use that
		if ($this->gallery_id != "") {
			
			$this->gallery_image_count 	= $objPixariaGallery->getGalleryImageCount();
			$this->multi_image_count 	= $objPixariaGallery->getGalleryImageCount();
			$this->gallery_child_count 	= $objPixariaGallery->getGalleryChildCount();
			$this->gallery_id			= $objPixariaGallery->getGalleryId();
			$this->gallery_rss			= $objPixariaGallery->getGalleryRss();
			$this->gallery_date			= $objPixariaGallery->getGalleryDate();
			$this->gallery_parent		= $objPixariaGallery->getGalleryParentId();
			$this->gallery_type			= $objPixariaGallery->getGalleryType();
			$this->gallery_userid		= $objPixariaGallery->getGalleryUserId();
			$this->gallery_title		= $objPixariaGallery->getGalleryTitle();
			$this->gallery_active		= $objPixariaGallery->getGalleryActive();
			$this->gallery_archived		= $objPixariaGallery->getGalleryArchived();
			$this->gallery_short_title	= $objPixariaGallery->getGalleryShortTitle();
			$this->gallery_description	= $objPixariaGallery->getGalleryDescription();
			$this->gallery_password		= $objPixariaGallery->getGalleryPassword();
			$this->gallery_permissions	= $objPixariaGallery->getGalleryPermissions();
			$this->gallery_key			= $objPixariaGallery->getGalleryKeyImageId();
			$this->gallery_created		= $objPixariaGallery->getGalleryDateCreated();
			$this->gallery_edited		= $objPixariaGallery->getGalleryDateEdited();
			$this->gallery_counter		= $objPixariaGallery->getGalleryViewCount();
			
			if (is_numeric($this->multi_image_image)) {
				
				// If we're looking at a single image page, load required information
				$this->gallery_image_data = $this->processGalleryImageDetail();
			
			}
			
		} else { // There was no gallery Id provided so we're probably looking at the master gallery list
			
			// Set the number of child galleries
			$this->gallery_child_count 	= $objPixariaGallery->getGalleryChildCount();
			
			// Set the gallery title to a default value when we're not browsing an actual gallery
			$this->gallery_title		= "Browse Galleries";
			
			return;
		
		}
		
	}
		
	/*
	*	Get the gallery ID
	*	
	*	@return string $this->gallery_id the ID of the current gallery
	*/
	function getGalleryId() {
	
		return $this->gallery_id;
	
	}
	
	/*
	*	Get the gallery RSS status
	*	
	*/
	function getGalleryRss() {
	
		return $this->gallery_rss;
	
	}
		
	/*
	*	Get the gallery page
	*/
	function getGalleryPage() {
	
		return $this->gallery_page;
	
	}

	/*
	*	Get the gallery date
	*/
	function getGalleryDate() {
	
		return $this->gallery_date;
	
	}

	/*
	*	Get the gallery parent ID
	*/
	function getGalleryParentId() {
	
		return $this->gallery_parent;
	
	}

	/*
	*	Get the gallery type
	*/
	function getGalleryType() {
	
		return $this->gallery_type;
	
	}

	/*
	*	Get the gallery user ID
	*/
	function getGalleryUserId() {
	
		return $this->gallery_userid;
	
	}

	/*
	*	Get the gallery title
	*/
	function getGalleryTitle() {
	
		return $this->gallery_title;
	
	}

	/*
	*	Get the gallery active
	*/
	function getGalleryActive() {
	
		return $this->gallery_active;
	
	}

	/*
	*	Get the gallery archived
	*/
	function getGalleryArchived() {
	
		return $this->gallery_archived;
	
	}

	/*
	*	Get the gallery short title
	*/
	function getGalleryShortTitle() {
	
		return $this->gallery_short_title;
	
	}

	/*
	*	Get the gallery description
	*/
	function getGalleryDescription() {
	
		return $this->gallery_description;
	
	}

	/*
	*	Get the gallery password
	*/
	function getGalleryPassword() {
	
		return $this->gallery_password;
	
	}

	/*
	*	Get the gallery permissions
	*/
	function getGalleryPermissions() {
	
		return $this->gallery_permissions;
	
	}

	/*
	*	Get the gallery key image ID
	*/
	function getGalleryKeyImageId() {
	
		return $this->gallery_key;
	
	}

	/*
	*	Get the gallery creation date
	*/
	function getGalleryDateCreated() {
	
		return $this->gallery_created;
	
	}

	/*
	*	Get the gallery edit date
	*/
	function getGalleryDateEdited() {
	
		return $this->gallery_edited;
	
	}

	/*
	*	Get the gallery view count
	*/
	function getGalleryViewCount() {
	
		return $this->gallery_counter;
	
	}

	/*
	*	Get the gallery image count
	*/
	function getGalleryImageCount() {
	
		return $this->gallery_image_count;
	
	}

	/*
	*	Get detailed information about an image and it's neighbours in this gallery
	*
	*	@return array $this->gallery_image_data an array of info about an image and its neighbours in a gallery
	*/
	function getGalleryImageData() {
	
		return $this->gallery_image_data;
	
	}

	/*
	*	Get the gallery URL
	*	
	*	@return string $gallery_url link to the current gallery main page
	*/
	function getGalleryURL() {
	
		global $cfg;
		
		// Set the URL of this page
		if ($cfg['set']['func_mod_rewrite']) {

			if ($this->gallery_short_title != "") {
				$gallery_path = $this->gallery_short_title;
			} else {
				$gallery_path = $this->gallery_id;
			}
			
			if ($this->gallery_id != "") {
				$gallery_url = $cfg['sys']['base_url'] . "gallery/" . $gallery_path . "/";
			} else {
				$gallery_url = $cfg['sys']['base_url'] . "gallery/";
			}
			
		} else {
		
			if ($this->gallery_id != "") {
				$gallery_url = $cfg['sys']['base_url'] . FILE_PUB_GALLERY . "?gid=" . $this->gallery_id;
			} else {
				$gallery_url = $cfg['sys']['base_url'] . FILE_PUB_GALLERY;
			}
		
		}

		return $gallery_url;
	
	}

	/*
	*	Get the gallery RSS flag
	*/
	function getGalleryRssUrl() {
	
		if ($this->getGalleryRss() && $this->getGalleryId() != '') {
			
			$url = SYS_BASE_URL . FILE_PUB_GALLERY . "?gid=" . $this->gallery_id . "&amp;cmd=mrss";
			
			return "<link rel=\"alternate\" href=\"".$url."\" type=\"application/rss+xml\" title=\"Media RSS\" id=\"media_rss\" />";
		
		} else {
			
			return false;
		
		}
	
	}

	/*
	*	Get complete data for the key image
	*/
	function getGalleryKeyImageData() {
		
		// Load the PixariaImage class
		require_once('class.PixariaImage.php');
		
		// Instantiate the object
		$objPixariaImage = new PixariaImage($this->gallery_key);
		
		// Get the complete array of image data for the key image
		$key_image_data = $objPixariaImage->getImageDataComplete();
		
		// If the image data is an array
		if (is_array($key_image_data)) {
		
			// Return the array
			return $key_image_data;
		
		}
	}

	/*
	*	Get key data about children of this gallery
	*/
	function getGalleryChildData() {
		
		global $cfg, $ses;
		
		$gallery_id = $this->gallery_id;
		
		if ($this->multi_image_page < 2) { // Load child galleries if we're on the first page.

			if ($cfg['set']['gallery_order'] == 10) {
			
				$order_sql = " ORDER BY ".PIX_TABLE_GALL.".gallery_date DESC";
			
			} elseif ($cfg['set']['gallery_order'] == 11) {
			
				$order_sql = " ORDER BY ".PIX_TABLE_GALL.".gallery_date ASC";
			
			} elseif ($cfg['set']['gallery_order'] == 12) {
			
				$order_sql = " ORDER BY ".PIX_TABLE_GALL.".gallery_title ASC";
			
			} elseif ($cfg['set']['gallery_order'] == 13) {
			
				$order_sql = " ORDER BY ".PIX_TABLE_GALL.".gallery_title DESC";
			
			}
			
			// Get information about child galeries
			$sql = "SELECT " . PIX_TABLE_GALL . ".*," . PIX_TABLE_IMGS . ".*  FROM " . PIX_TABLE_GALL . "
			
					LEFT JOIN ".PIX_TABLE_IMGS." ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_GALL.".gallery_key
			
					LEFT JOIN ".PIX_TABLE_GVIE." ON ".PIX_TABLE_GALL.".gallery_id = ".PIX_TABLE_GVIE.".gallery_id
			
					WHERE gallery_parent = '$gallery_id'
					
					".$cfg['set']['gallery_access_sql']."
					
					".$cfg['set']['gallery_visibility_sql']."
					
					GROUP BY ".$cfg['sys']['table_gall'].".gallery_id
					
					$order_sql" . 
					
					$this->getGalleryPageLimitSQL();
			
			$result = $this->_dbl->sqlSelectRows($sql);
			
			// Runs a method in this class to get a list of information for all child galleries
			return $this->processGalleryChildData($result);
		
		} else {
		
			return false;
		
		}
	
	}

	/*
	*	Get data for the images in this gallery
	*/
	function getGalleryMultiImageData() {
		
		global $cfg, $ses;
		
		// Get the gallery ID property
		$gallery_id = $this->gallery_id;
		
		if ($gallery_id != "") { // If there is a gallery ID then load information about sub-galleries for this one
		
			$sql = "SELECT DISTINCT ". PIX_TABLE_IMGS .".* FROM `". PIX_TABLE_GORD ."`
			
					LEFT JOIN `". PIX_TABLE_IMGS ."` ON ". PIX_TABLE_GORD .".image_id = ". PIX_TABLE_IMGS .".image_id
						
					LEFT JOIN `". PIX_TABLE_IMVI ."` ON ". PIX_TABLE_IMGS .".image_id = ". PIX_TABLE_IMVI .".image_id
					
					WHERE ". PIX_TABLE_GORD .".gallery_id = '$gallery_id'
						
					".$cfg['set']['image_access_sql']."
					
					".$cfg['set']['image_visibility_sql']."
					
					ORDER BY ". PIX_TABLE_GORD .".order_id ASC" . 
					
					$this->getMultiImagePageLimitSQL();
			
			$gallery_images = $this->_dbl->sqlSelectRows($sql);
			
			// Runs the method in the parent class to process a list of the images in this gallery
			return $this->processMultiImageData($gallery_images);
		
		} else { // If no gallery ID then return false
		
			return false;
		
		}
	
	}
	
	/*
	*	
	*	Loops through the gallery table to get a nested hierarchy of gallery_titles and gallery_ids
	*
	*/
	function getGalleryHierarchy($gallery_unique_id) {
		
		global $cfg;
		
		$result = $this->_dbl->sqlSelectRow("SELECT * FROM ". PIX_TABLE_GALL ." WHERE gallery_id = '$gallery_unique_id'");
	
		if (is_array($result)) {
		
			$this->gallery_hierarchy['title'][]	= $result['gallery_title'];
			$this->gallery_hierarchy['id'][]	= $result['gallery_id'];
					
			if ($result['gallery_parent'] != 0) {
			
				$this->getGalleryHierarchy($result['gallery_parent']);
			
			}
			
		}
		
		return $this->gallery_hierarchy;
		
	}

	/*
	*
	*	Get SQL to limit the number of galleries to the current page
	*
	*/
	function getGalleryPageLimitSQL () {
		
		global $cfg;
		
		// This statement ensure we only load image data for the page we're on
		if (!$this->gallery_page) {
			
			$results_lim_sta 	= 0;
			$results_lim_end	= $cfg['set']['gallery_per_page'];
			
		} else {
			
			$results_lim_sta 	= ($this->gallery_page - 1) * $cfg['set']['gallery_per_page'];
			$results_lim_end	= $cfg['set']['gallery_per_page'];
			
		}
		
		return " LIMIT $results_lim_sta, $results_lim_end";
	
	}
	
	/*
	*	
	*	Generates links to navigate between pages of a thumbnail view
	*
	*/
	function getGalleryPageNavigation() {
		
		global $cfg, $smarty;
				
		$current_page	= $this->gallery_page;
		
		if ($current_page == ""): $current_page = 1; endif;
		
		$images 		= $this->gallery_child_count;
		$items			= $cfg['set']['gallery_per_page'];
		$pages			= ceil($images / $items);
		$this_page		= $this->gallery_page;
		
		for ($i = 1; $i <= $pages; $i++) {
			
			$page[] = $i;
			
			if ($cfg['set']['func_mod_rewrite']) {
			
				if ($this->gallery_short_title != "") {
					$gallery_path = $this->gallery_short_title;
				} elseif ($this->gallery_id == "") {
					$gallery_path = "main";
				} else {
					$gallery_path = $this->gallery_id;
				}
				
				$href[] = $cfg['sys']['base_url']."gallery/".$gallery_path."/page/".$i."/";
			
			} else {
			
				$href[] = $cfg['sys']['base_url'].$cfg['fil']['index_gallery']."?gid=".$this->gallery_id."&amp;gpg=".$i;
			
			}
			
		}
		
		return array($current_page, $page, $href);
		
	}

	/*
	*
	*	Turn a combined array of image and gallery data into information needed
	*	for showing a series of gallery link thumbnail images
	*	
	*	@return array $gallery information the child galleries of the one being viewed
	*/
	function processGalleryChildData ($data) {
		
		global $cfg, $ses;
		
		if (is_array($data)) {
		
			foreach ($data as $key => $value) {
				
				$key_base_path = $cfg['sys']['base_library'].$value['image_path']."/";
				
				$key_icon_path 		= $key_base_path . "32x32/" . $value['image_filename'];
				$key_small_path 	= $key_base_path . "80x80/" . $value['image_filename'];
				$key_large_path 	= $key_base_path . "160x160/" . $value['image_filename'];
				
				if (file_exists($key_icon_path)) {
					$key_icon_dims = $this->getImageDimensions($value['icon_w'], $value['icon_h']);
				} else {
					$key_icon_path = SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/images/32x32_missing.jpg";
					$key_icon_dims = $this->getImageDimensions(32,24);
				}
				
				if (file_exists($key_small_path)) {
					$key_small_dims = $this->getImageDimensions($value['smal_w'],$value['smal_h']);
				} else {
					$key_small_path = SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/images/80x80_missing.jpg";
					$key_small_dims = $this->getImageDimensions(80,60);
				}
				
				if (file_exists($key_large_path)) {
					$key_large_dims = $this->getImageDimensions($value['larg_w'],$value['larg_h']);
				} else {
					$key_large_path = SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/images/160x160_missing.jpg";
					$key_large_dims = $this->getImageDimensions(160,120);
				}
				
				$key_icon_encoded 		= base64_encode($key_icon_path);
				$key_small_encoded		= base64_encode($key_small_path);
				$key_large_encoded		= base64_encode($key_large_path);
	
				if ($cfg['set']['func_mod_rewrite']) {
				
					if ($value['gallery_short_title'] != "") {
						$gallery_path = $value['gallery_short_title'];
					} else {
						$gallery_path = $value['gallery_id'];
					}
					
					$gallery[$key]['url'] = $cfg['sys']['base_url'] . "gallery/" . $gallery_path . "/";
				
				} else {
				
					$gallery[$key]['url'] = $cfg['sys']['base_url'] . FILE_PUB_GALLERY . "?gid=" . $value['gallery_id'];
				
				}
				
				
				$gallery[$key]['title']				= $value['gallery_title'];
				$gallery[$key]['description']		= $value['gallery_description'];
				$gallery[$key]['date']				= $value['gallery_date'];
				$gallery[$key]['counter'] 			= $value['gallery_counter'];
				$gallery[$key]['image_count'] 		= $this->getChildGalleryImageCount($value['gallery_id']);
				$gallery[$key]['key_icon_dims']		= $key_icon_dims;
				$gallery[$key]['key_icon_path']		= $key_icon_encoded;
				$gallery[$key]['key_small_dims']	= $key_small_dims;
				$gallery[$key]['key_small_path']	= $key_small_encoded;
				$gallery[$key]['key_large_dims']	= $key_large_dims;
				$gallery[$key]['key_large_path']	= $key_large_encoded;
			
			}
			
			return $gallery;
			
		} else {
		
			return false;
		
		}
	
	}
	
	/*
	*
	*	Returns the number of images inside the gallery with the specified ID
	*	
	*	@return int number of images in gallery with id $gallery_id
	*/
	function getChildGalleryImageCount ($gallery_id) {
		
		global $gallery_child_ids, $cfg, $ses;
		
		$gallery_child_ids = array();
		
		$this->getGalleryChildIdsRecursive($gallery_id);
		
		foreach ($gallery_child_ids as $key => $value) {
		
			$gallery_sql .= " OR ". PIX_TABLE_GORD .".gallery_id = '".$this->_dbl->escape($value)."'";
		
		}
		
		$sql = "SELECT ".PIX_TABLE_IMGS.".image_id FROM ".PIX_TABLE_GORD." 
		
				LEFT JOIN `". PIX_TABLE_IMGS ."` ON ". PIX_TABLE_GORD .".image_id = ". PIX_TABLE_IMGS .".image_id
						
				LEFT JOIN `". PIX_TABLE_IMVI ."` ON ". PIX_TABLE_IMGS .".image_id = ". PIX_TABLE_IMVI .".image_id
					
				LEFT JOIN `". PIX_TABLE_GVIE ."` ON ". PIX_TABLE_GORD .".gallery_id = ". PIX_TABLE_GVIE .".gallery_id
					
				LEFT JOIN `". PIX_TABLE_GALL ."` ON ". PIX_TABLE_GORD .".gallery_id = ". PIX_TABLE_GALL .".gallery_id
					
				WHERE (". PIX_TABLE_GORD .".gallery_id = '$gallery_id' $gallery_sql) 
				
				".$cfg['set']['gallery_access_sql']."
				
				".$cfg['set']['gallery_visibility_sql']."
				
				".$cfg['set']['image_access_sql']."
				
				".$cfg['set']['image_visibility_sql']."
				
				GROUP BY ". PIX_TABLE_IMGS .".image_id;";
		
		return count($this->_dbl->rows($sql));
		
	}
	
	/*
	*
	*	Loop through gallery children of $gallery_id recursively and add child gallery_ids to global $gallery_child_ids array
	*
	*/
	function getGalleryChildIdsRecursive ($gallery_id) {
	
		global $gallery_child_ids;
		
		$sql = "SELECT gallery_id FROM ".PIX_TABLE_GALL." WHERE gallery_parent = '$gallery_id'";
		
		$result = $this->_dbl->sqlSelectRows($sql);
		
		if (is_array($result)) {
			
			foreach ($result as $key => $value) {
				
				if ($value['gallery_id'] != "") {
				
					$gallery_child_ids[] = $value['gallery_id'];
					
					$this->getGalleryChildIdsRecursive($value['gallery_id']);
					
				}
				
			}
		
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function processNeighbourURLs($start, $number) {
		
		global $cfg;
		
		for ($i=($start + 1); $i <= ($start + $number + 1); $i++) {
	
			if ($cfg['set']['func_mod_rewrite']) {
	
				if ($this->gallery_short_title != "") {
					$gallery_path = $this->gallery_short_title;
				} else {
					$gallery_path = $this->gallery_id;
				}
				
				$data[]	= $cfg['sys']['base_url'] . "gallery/" . $gallery_path . "/image/" . $i . "/";
				
			} else {
			
				$data[]	= $cfg['sys']['base_url'] . FILE_PUB_GALLERY . "?gid=" . $this->gallery_id . "&amp;img=" . $i;
				
			}
				
		}
		
		return $data;
		
	}
	
	/*
	*	Outputs an array of information about a single image in a gallery and limited detail of those near it
	*	
	*	@return array $images_data information about an image in a gallery and the five images nearest to it
	*/
	function processGalleryImageDetail () {
	
		global $cfg, $ses, $smarty;
		
		$results_image = $this->multi_image_image;
		$results_total = $this->gallery_image_count;
		
		$results_view		= $this->getBrowsingLimitData ('results_view');
		$results_lim_sta	= $this->getBrowsingLimitData ('results_lim_sta');
		$results_lim_end	= $this->getBrowsingLimitData ('results_lim_end');
		$image_key			= $this->getBrowsingLimitData ('image_key');
		
		//print("Image number: $results_image<br>Total images:$results_total<br>$results_view - <br>SQL limit start:$results_lim_sta<br>SQL limit end:$results_lim_end<br>Current image:$image_key ");
		
		// Build SQL to select all the necessary gallery information from the database
		$results_sql = "SELECT ".$cfg['sys']['table_imgs'].".*,".$cfg['sys']['table_gall'].".* FROM `".$cfg['sys']['table_gord']."`
		
						LEFT JOIN `".$cfg['sys']['table_imgs']."` ON ".$cfg['sys']['table_gord'].".image_id = ".$cfg['sys']['table_imgs'].".image_id
						
						LEFT JOIN `".$cfg['sys']['table_gall']."` ON ".$cfg['sys']['table_gord'].".gallery_id = ".$cfg['sys']['table_gall'].".gallery_id
						
						LEFT JOIN `".$cfg['sys']['table_imvi']."` ON ".$cfg['sys']['table_imgs'].".image_id = ".$cfg['sys']['table_imvi'].".image_id
					
						WHERE ".$cfg['sys']['table_gord'].".gallery_id = '".$this->gallery_id."'
						
						".$cfg['set']['image_access_sql']."
						
						".$cfg['set']['gallery_visibility_sql']."
						
						".$cfg['set']['image_visibility_sql']."
						
						GROUP by ".$cfg['sys']['table_imgs'].".image_id
							
						ORDER by ".$cfg['sys']['table_gord'].".order_id
						
						LIMIT $results_lim_sta,$results_lim_end";
		
		// Retrieve gallery information from database
		$image_data 	= $this->_dbl->sqlSelectRows($results_sql);
		
		// Load the PixariaImage class 
		require_once ('class.PixariaImage.php');
		
		// Create the image object
		$objPixariaImage = new PixariaImage($image_data[$image_key]['image_id']);
		
		// Update the view counter for this image
		$objPixariaImage->updateImageViewCounter();
		
		// Add the image_id to the array of data
		$images_data['image_id']			= $image_data[$image_key]['image_id'];
		
		// Add detailed information about this image to the array
		$images_data['image_detail']		= $objPixariaImage->getImageDataComplete();
		
		// Add basic information about the neighbouring images to the array
		$images_data['image_neighbours']	= $this->processMultiImageData($image_data);
		
		$images_data['image_count']			= count($images_data['image_neighbours']);
		
		$images_data['image_urls']			= $this->processNeighbourURLs($results_lim_sta,$results_lim_end);
			
		$images_data['image_numbers']		= $this->processNeighbourImageNumbers($results_lim_sta,$results_lim_end);
			
		if ($cfg['set']['func_mod_rewrite']) {
			
			if ($this->gallery_short_title != "") {
				$gallery_path = $this->gallery_short_title;
			} else {
				$gallery_path = $this->gallery_id;
			}
			
			if ($this->multi_image_image == 1) {
				$images_data['image_prev_url'] = $cfg['sys']['base_url'] . "gallery/" . $gallery_path . "/";
			} else {
				$images_data['image_prev_url'] = $cfg['sys']['base_url'] . "gallery/" . $gallery_path . "/image/" . ($this->multi_image_image - 1) . "/";
			}
			
			if ($this->multi_image_image == $this->multi_image_count) {
				$images_data['image_next_url'] = $cfg['sys']['base_url'] . "gallery/" . $gallery_path . "/";
			} else {
				$images_data['image_next_url'] = $cfg['sys']['base_url'] . "gallery/" . $gallery_path . "/image/" . ($this->multi_image_image + 1) . "/";
			}
		
		} else {
		
			if ($this->multi_image_image == 1) {
				$images_data['image_prev_url'] = $cfg['sys']['base_url'] . FILE_PUB_GALLERY . "?gid=" . $this->gallery_id;
			} else {
				$images_data['image_prev_url'] = $cfg['sys']['base_url'] . FILE_PUB_GALLERY . "?gid=" . $this->gallery_id . "&amp;img=" . ($this->multi_image_image - 1);
			}
			
			if ($this->multi_image_image == $this->multi_image_count) {
				$images_data['image_next_url'] = $cfg['sys']['base_url'] . FILE_PUB_GALLERY . "?gid=" . $this->gallery_id;
			} else {
				$images_data['image_next_url'] = $cfg['sys']['base_url'] . FILE_PUB_GALLERY . "?gid=" . $this->gallery_id . "&amp;img=" . ($this->multi_image_image + 1);
			}
		
		}
				
		// Return the image data as an array
		return $images_data;
		
	}

	/*
	*
	*	This method increments the view counter for this gallery by one
	*
	*/
	function updateGalleryViewCounter () {
		
		global $ses, $cfg;
		
		if (!$ses['psg_administrator']) { // Only update counter if user is not an admin
		
			// Update the number of times this gallery's been viewed
			$this->_dbl->sqlQuery("UPDATE `".PIX_TABLE_GALL."` SET gallery_counter = gallery_counter + 1 WHERE gallery_id = '".$this->gallery_id."';");
		
		}
		
	}
	
}


?>