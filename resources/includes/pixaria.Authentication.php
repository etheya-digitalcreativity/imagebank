<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	This function creates a cookie to store a user's session
*
*/

function pix_set_session($userid, $duration = "") {

	global $cfg, $objEnvData;	
	
	$now = time();
	
	if ($objEnvData->fetchGlobal('cookie_persist') == "on" || $objEnvData->fetchCookie('psg_persist') == "1") {
	
		$duration = $now + 63072000;
		
	} elseif ($duration != "") {
	
		$duration = $now + $duration;
		
	} else {
	
		$duration = null;
	
	}
		
	$cookie_string = md5($userid.$cfg['sys']['encryption_key']) . "|" . $userid;
	
	if ($cookie_string) {
		
		// Bypass microsoft's lousy security management settings in ie 6
		header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');

		if ($objEnvData->fetchGlobal('cookie_persist') == "on" || $objEnvData->fetchCookie('psg_persist') == "1") {
			
			// Load the user's id into a cookie expiring in 2 years
			setcookie("psg_userid",$cookie_string,$duration,"/");
			
			// Load the user's id into a cookie expiring in 2 years
			setcookie("psg_persist","1",$duration,"/");
			
		} else {
		
			// Load the user's id into a cookie expiring in 20 mins
			setcookie("psg_userid",$cookie_string,$duration,"/");

		}
		
	}
	
}

/*
*
* 	This function checks for session cookies and if the user
*
*/

function pixGetSession($psg_userid) {

	if ($psg_userid) {
		
		global $cfg;
		
		// Create array to hold session info
		$ses = array();
		
		$data = explode("|",$psg_userid);
		
		if ($data[0] === md5($data[1].$cfg['sys']['encryption_key'])) {
			
			// Add user id to session
			$ses['psg_userid'] = $data[1];
			
			$sql = 	"SELECT account_status,group_id,active_image_box, email_address, concat(first_name,' ',family_name) AS name
					
					FROM ".PIX_TABLE_USER."
			
					LEFT JOIN ".PIX_TABLE_GRME."
										
					ON ".PIX_TABLE_GRME.".userid = ".PIX_TABLE_USER.".userid
					
					WHERE ".PIX_TABLE_USER.".userid = '".$ses['psg_userid']."'";
					
			
			// Load key user data from user table
			$groups_result	= sql_select_rows($sql);
			
			$sql = 	"SELECT account_status,sys_group_id,active_image_box
					
					FROM ".PIX_TABLE_USER."
			
					LEFT JOIN ".PIX_TABLE_SGME."
					
					ON ".PIX_TABLE_SGME.".sys_userid = ".PIX_TABLE_USER.".userid
										
					WHERE ".PIX_TABLE_USER.".userid = '".$ses['psg_userid']."'";
					
			
			// Load key user data from user table
			$sys_groups_result	= sql_select_rows($sql);
			
			// Load the user's name into the session array
			$ses['psg_name'] = trim($groups_result[0]['name']);
			
			// Set the account status for this user
			$ses['psg_account_status'] = $groups_result[0]['account_status'];
			
			// Set the active lightbox_id for this user
			$ses['psg_lightbox_id'] = $groups_result[0]['active_image_box'];
			
			// If the user has a complete profile with username and password
			if  ($groups_result[0]['email_address'] != "") {
				
				$ses['psg_full_user'] = true;
			
			} else {
				
				$ses['psg_full_user'] = false;
			
			}
			
			// If the userid is set and the account is active, load an array of the groups this user is in
			if ($psg_userid != "" && $groups_result[0]['account_status'] == 1) {
			
				// Get the user's privileges
				$ses['psg_groups_member_of'] = pix_get_privileges($groups_result,"groups");
				
			}
			
			// If the userid is set and the account is active, load an array of the system levelgroups this user is in
			if ($psg_userid != "" && $sys_groups_result[0]['account_status'] == 1) {
				
				// Get the user's privileges
				$ses['psg_sys_groups_member_of'] = pix_get_privileges($sys_groups_result,"sys_groups");
				
			}			
			
			// If the system group membership array exists
			if (is_array($ses['psg_sys_groups_member_of'])) {
			
				// Check if the user is an administrator
				if ( in_array('1',$ses['psg_sys_groups_member_of'],TRUE) ) {
					
					$ses['psg_administrator'] = (bool)TRUE;
					
				}
				
				// Check if the user is a global image editor
				if ( in_array('2',$ses['psg_sys_groups_member_of'],TRUE) ) {
					
					$ses['psg_image_editor'] = (bool)TRUE;
					
				}
				
				// Check if the user is a global image editor
				if ( in_array('3',$ses['psg_sys_groups_member_of'],TRUE) ) {
					
					$ses['psg_photographer'] = (bool)TRUE;
					
				}
				
				// Check if the user is a global image editor
				if ( in_array('4',$ses['psg_sys_groups_member_of'],TRUE) ) {
					
					$ses['psg_delete'] = (bool)TRUE;
					
				}
				
				// Check if the user is a global image editor
				if ( in_array('5',$ses['psg_sys_groups_member_of'],TRUE) ) {
					
					$ses['psg_download'] = (bool)TRUE;
					
				}
				
			}
			
			// Refresh the session timeout
			pix_set_session($ses['psg_userid']);
			
			// Output the session information back into the global domain
			return $ses;
		
		}
		
	}

}

/*
*
*	This function creates a temporary user id if the user is not logged in
*
*/
function pixCreateTemporaryUserId () {
	
	global $ses, $cfg;
	
	if ($ses['psg_userid'] == "" && !$ses['psg_full_user']) {
	
		require_once ('class.PixariaUser.php');
		
		$objUser = new PixariaUser();
		
		$objUser->createNewUserTemp();
		
		// Set a new session cookie expiring in 28 days
		pix_set_session($objUser->getUserId(), "2419200");
		
		// Output the session information back into the global domain
		$ses = pixGetSession(md5($objUser->getUserId().$cfg['sys']['encryption_key'])."|".$objUser->getUserId());
		
	}
	
}

/*
*
* 	This function gets the user's permissions and puts them into an array
*
*/

function pix_get_privileges($group_data,$group_type) {

	global $cfg;
	
	if (is_array($group_data)) {
		
		// Initialise the groups array
		$groups = array();
		
		// Loop through each group that this user is a member of
		foreach ($group_data as $key => $value) {
			
			if ($group_type == "groups") { // We're build an array of user groups
			
				// Add the group_id to an array
				$groups[] = $value['group_id'];
		
			} elseif ($group_type == "sys_groups") { // We're building an array of system groups
			
				// Add the sys_group_id to an array
				$groups[] = $value['sys_group_id'];
		
			}
			
		}
		
		// Put the array of groups into the existing $ses array
		return $groups;
		
	}

}


/*
*	
*	Check that the user has the correct access privileges
*	If the user doesn't have access, bounce them to the login page
*
*/
function pix_authorise_user($level) {
	
	global $cfg, $ses;
	
	$redirect_url = $cfg['sys']['base_url'].$cfg['fil']['index_login'] . "?referer=" . urlencode($cfg['sys']['base_url'].ltrim($_SERVER['REQUEST_URI'],"/"));
	
	if ($_SERVER['QUERY_STRING'] != "" && !preg_match("/\?/",$_SERVER['REQUEST_URI'])) { $redirect_url .= urlencode("?" . $_SERVER['QUERY_STRING']); }
	
	switch ($level) {
		
		case "temp":
			
			if (!isset($ses['psg_userid'])) {
			
				header ("Location: $redirect_url");
				exit;
				
			}
			
		break;
		
		case "basic": // User is registered but their account is not active to view restricted pages
			
			if (!isset($ses['psg_userid']) || $ses['psg_name'] == "") {
			
				header ("Location: $redirect_url");
				exit;
				
			}
			
		break;
		
		
		case "registered": // User is registered with an active account
		
			if ($ses['psg_account_status'] === "0" && !isset($ses['psg_userid'])) {
			
				header ("Location: $redirect_url");
				exit;
				
			}
			
		break;
		
		
		case "image_editor": // User is registered and active with an administrator's account
	
			if (!$ses['psg_administrator'] && !$ses['psg_image_editor']) {
				
				header ("Location: " . $cfg['sys']['base_url'] . $cfg['fil']['index_account']);
				exit;
				
			}
			
		break;
						
		case "administrator": // User is registered and active with an administrator's account
	
			if (!$ses['psg_administrator']) {
				
				header ("Location: $redirect_url");
				exit;
				
			}
			
		break;
						
		case "photographer": // User is not registered and active with an photographer's account
	
			if (!$ses['psg_photographer']) {
				
				header ("Location: " . $cfg['sys']['base_url'] . $cfg['fil']['index_account']);
				exit;
				
			}
			
		break;
						
		case "deletor": // User is not registered and active with an photographer's account
	
			if (!$ses['psg_deletor']) {
				
				header ("Location: " . $cfg['sys']['base_url'] . $cfg['fil']['index_account']);
				exit;
				
			}
			
		break;
						
	}

	return true;
	
}

/*
*	
*	This function generates a short SQL fragment to include in part of the WHERE section
*	of a SQL statement.  The SQL will prevent MySQL returning images the user isn't authorised to view.
*
*/

function pix_image_access_sql() {

	global $ses, $cfg;
	
	if ($ses['psg_administrator']) { // User is an admin, there is no need to limit what they can see
	
		$image_access_sql = "";
	
	} elseif (!$ses['psg_administrator'] && is_array($ses['psg_groups_member_of'])) { // User is registered but is not an admin (place limits on which images they can view)
	
		$image_access_sql = "AND (";
			
		foreach ($ses['psg_groups_member_of'] as $key => $value) {
		
			$image_access_sql .= "(".$cfg['sys']['table_imvi'].".group_id = '$value') OR ";
		
		}
		
		$image_access_sql .= "(".$cfg['sys']['table_imgs'].".image_permissions != '12'))";
		
	} else { // User isn't logged in at all, we can only show them images where there are no restrictions
	
		$image_access_sql = "AND ".$cfg['sys']['table_imgs'].".image_permissions = '10'";
	
	}
	
	$cfg['set']['image_access_sql'] = $image_access_sql;
	
}

/*
*	
*	This function generates a short SQL fragment to include in part of the WHERE section
*	of a SQL statement.  The SQL will prevent MySQL returning images the user isn't authorised to view.
*
*/

function pix_gallery_access_sql() {

	global $ses, $cfg;
	
	if ($ses['psg_administrator']) { // User is an admin, there is no need to limit what they can see
	
		$gallery_access_sql = "";
	
	} elseif (!$ses['psg_administrator'] && is_array($ses['psg_groups_member_of'])) { // User is registered but is not an admin (place limits on which galleries they can view)
	
		$gallery_access_sql = "AND (";
			
		foreach ($ses['psg_groups_member_of'] as $key => $value) {
		
			$gallery_access_sql .= "(".PIX_TABLE_GVIE.".userid = '$value') OR ";
		
		}
		
		$gallery_access_sql .= "(".PIX_TABLE_GALL.".gallery_permissions != '12'))";
		
	} else { // User isn't logged in at all, we can only show them images where there are no restrictions
		
		$gallery_access_sql = "AND ".PIX_TABLE_GALL.".gallery_permissions = '10'";
	
	}
	
	$cfg['set']['gallery_access_sql'] = $gallery_access_sql;
	
}

/*
*	
*	
*
*/
function pix_user_preferences () {
	
	global $ses;
	
	$db = new Database();
	
	$sql = "SELECT 	 pref.name
					,pref.default_value
					,pref.type
					,uprf.value
					
			FROM ".PIX_TABLE_PREF." pref
	
			JOIN ".PIX_TABLE_UPRF." uprf ON pref.preference_id = uprf.preference_id
			
			WHERE user_id = '".$db->escape($ses['psg_userid'])."'";
	
	list (
		
		$user_preference_name,
		$user_preference_default_value,
		$user_preference_type,
		$user_preference_value
	
	) = $db->rowsAsColumns($sql);
	
	while ( list($key, $value) = @each($user_preference_name) ) {
		
		if ($user_preference_type[$key] == 'array') {
			$ses['pref_' . $value] = unserialize(base64_decode($user_preference_value[$key]));
		} else {
			$ses['pref_' . $value] = $user_preference_value[$key];
		}
		
	}
	
}

/*
*	
*	This function stores the time and IP address of a user login
*
*/

function pix_register_login($log_userid,$log_success="",$email="") {

	global $cfg;
	
	$email		= addslashes($email);
	
	$log_ip		= $_SERVER['REMOTE_ADDR'];
	$log_host	= addslashes(gethostbyaddr($log_ip));
	
	@mysql_query("INSERT INTO ".PIX_TABLE_ULOG." VALUES ('$log_userid','$log_success','$email','$log_ip',NOW(),'$log_host');");
	
}

?>