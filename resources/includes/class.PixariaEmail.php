<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

class PixariaEmail {

	var $sSubject;
	var $sRecipientName;
	var $sRecipientAddress;
	var $sSenderName;
	var $sSenderAddress;
	var $sMessage;
	
	function PixariaEmail () {
	
		return;
	
	}
	
	function setSubject ($sSubject) {
	
		$this->sSubject = stripslashes($sSubject);
	
	}

	function setRecipientName ($sRecipientName) {
	
		$this->sRecipientName = $sRecipientName;
	
	}

	function setRecipientAddress ($sRecipientAddress) {
	
		$this->sRecipientAddress = $sRecipientAddress;
	
	}

	function setSenderName ($sSenderName) {
		
		$this->sSenderName = $sSenderName;
	
	}

	function setSenderAddress ($sSenderAddress) {
	
		$this->sSenderAddress = $sSenderAddress;
	
	}

	function setMessage ($sMessage) {
	
		$this->sMessage = stripslashes($sMessage);
	
	}

	function sendEmail () {
		
		if ($this->sRecipientAddress != "" && $this->sSubject != "" && $this->sMessage != "") {
		
			if ($this->sSenderName !== '') {
			
				$headers = "From: \"".$this->sSenderName."\" <".$this->sSenderAddress.">\r\n";
			
			} else {
			
				$headers = "From: ".$this->sSenderName."\r\n";
			
			}
		
			$headers .= "X-Mailer: PHP/PixariaGallery\r\n"; 
		
			$headers .= "MIME-Version: 1.0\r\n";
		
			$headers .= "Reply-To: <".$this->sSenderAddress.">\r\n";
		
			if (mail($this->sRecipientAddress, $this->sSubject, $this->sMessage, $headers, "-f ".$this->sSenderAddress)) {
			
				return true;
			
			} else {
			
				return false;
			
			}
		
		} else {
		
			return false;
		
		}
		
	}

}

?>