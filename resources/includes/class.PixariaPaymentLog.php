<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

/*
*
*	Class to work with data in the payment script data log
*
*/

class PixariaPaymentLog {

	// Private variables
	var $_dbl;
	var $_update_flag = false;
	
	// Public variables
	var $id;
	var $ip_address;
	var $host_name;
	var $post_vars;
	var $get_vars;
	var $cookie_vars;
	var $event_time;
	
	/*
	*	Class constructor
	*	
	*	@return void
	*/
	function PixariaPaymentLog ($id = "") {
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		if (is_numeric($id)) {
		
			$sql = "SELECT * FROM " .PIX_TABLE_PSDL. " WHERE id = '$id';";
			
			$data = $this->_dbl->sqlSelectRow($sql);
			
			if (is_array($data)) {
				
				$this->id 			= $data['id'];
				$this->ip_address 	= $data['ip_address'];
				$this->host_name 	= $data['host_name'];
				$this->event_time 	= $data['event_time'];
				
				$this->post_vars 	= $this->processArray(unserialize(urldecode($data['post_vars'])));
				$this->get_vars 	= $this->processArray(unserialize(urldecode($data['get_vars'])));
				$this->cookie_vars 	= $this->processArray(unserialize(urldecode($data['cookie_vars'])));
				
				
			}
		
		}
		
	}	
	
	/*
	*	Get ID
	*
	*	@return string $this->id
	*/
	function getID () {
		return $this->ID;
	}
	
	/*
	*	Get IP Address
	*
	*	@return string $this->ip_address
	*/
	function getIPAddress () {
		return $this->ip_address;
	}
	
	/*
	*	Get host name
	*
	*	@return string $this->host_name
	*/
	function getHostName () {
		return $this->host_name;
	}
	
	/*
	*	Get post vars
	*
	*	@return array $this->post_vars
	*/
	function getPostVars () {
		return $this->post_vars;
	}
	
	/*
	*	Get get vars
	*
	*	@return array $this->get_vars
	*/
	function getGetVars () {
		return $this->get_vars;
	}
	
	/*
	*	Get cookie vars
	*
	*	@return array $this->cookie_vars
	*/
	function getCookieVars () {
		return $this->cookie_vars;
	}
	
	/*
	*	Get post count
	*
	*	@return int
	*/
	function getPostCount () {
		return count($this->post_vars);
	}
	
	/*
	*	Get get count
	*
	*	@return int
	*/
	function getGetCount () {
		return count($this->get_vars);
	}
	
	/*
	*	Get cookie count
	*
	*	@return int
	*/
	function getCookieCount () {
		return count($this->cookie_vars);
	}
	
	/*
	*	Get event time
	*
	*	@return string $this->event_time
	*/
	function getEventTime () {
		return $this->event_time;
	}
	
	/*
	*	Get array of multiple entries from this log or false if no data
	*	
	*	@return mixed $output
	*/
	function getMultipleEntries ($start = "", $number = "", $sort_column = "", $sort_order = "") {
		
		// Generate SQL for sorting columns
		$sort_sql = $this->generateSortSQL(array('event_time','ip_address','host_name'), $sort_column, $sort_order, 'event_time', 'DESC');
		
		if (is_int($start) && is_int($number)) {
		
			$sql = "SELECT * FROM " .PIX_TABLE_PSDL. " LIMIT $start, $number" . $sort_sql;
		
		} else {
		
			$sql = "SELECT * FROM " .PIX_TABLE_PSDL . $sort_sql;
		
		}
		
		$data = $this->_dbl->sqlSelectRows($sql);
		
		if (is_array($data)) {
		
			foreach ($data as $key => $value) {
			
				$id[] 			= $value['id'];
				$ip_address[] 	= $value['ip_address'];
				$host_name[] 	= $value['host_name'];
				$post_vars[] 	= unserialize(urldecode($value['post_vars']));
				$get_vars[] 	= unserialize(urldecode($value['get_vars']));
				$cookie_vars[] 	= unserialize(urldecode($value['cookie_vars']));
				$event_time[] 	= $value['event_time'];
				
				$post_count[]	= count(unserialize(urldecode($value['post_vars'])));
				$get_count[]	= count(unserialize(urldecode($value['get_vars'])));
				$cookie_count[]	= count(unserialize(urldecode($value['cookie_vars'])));
				
			}
			
			$output['id'] 			= $id;
			$output['ip_address'] 	= $ip_address;
			$output['host_name'] 	= $host_name;
			$output['post_vars'] 	= $post_vars;
			$output['get_vars'] 	= $get_vars;
			$output['cookie_vars'] 	= $cookie_vars;
			$output['event_time'] 	= $event_time;
			
			$output['post_count']	= $post_count;
			$output['get_count']	= $get_count;
			$output['cookie_count']	= $cookie_count;
			
			return $output;
			
		} else {
		
			return false;
		
		}
		
	}
	
	/*
	*	Generate code for sorting SQL columns
	*	
	*	@return string SQL
	*/
	function generateSortSQL ($columns, $sort_column, $sort_order, $default_col, $default_ord) {
	
		if (trim($sort_column) != "" && trim($sort_order) != "") {
		
			if (is_array($columns)) {
			
				if (in_array($sort_column, $columns)) {
				
					if ($sort_order == "ASC") {
					
						return " ORDER BY $sort_column ASC";
					
					} elseif ($sort_order == "DESC") {
					
						return " ORDER BY $sort_column DESC";
					
					}
				
				}
			
			} else {
			
				return " ORDER BY $default_col $default_ord";
			
			}
		
		} else {
			
			return " ORDER BY $default_col $default_ord";
			
		}
	
	}
	
	/*
	*	Set variables for the current environment
	*	
	*	@return void
	*/
	function setDataVars () {
	
		global $objEnvData;
		
		$this->ip_address 	= $objEnvData->fetchServer('REMOTE_ADDR');
		$this->host_name 	= gethostbyaddr($objEnvData->fetchServer('REMOTE_ADDR'));
		$this->post_vars 	= urlencode(serialize($_POST));
		$this->get_vars 	= urlencode(serialize($_GET));
		$this->cookie_vars 	= urlencode(serialize($_COOKIE));
		
		$this->_update_flag = TRUE;
		
	}
	
	/*
	*	Adds a new entry to the payment script data log
	*	
	*	@return bool status
	*/
	function createEntry () {
		
		$ip_address 	= $this->ip_address;
		$host_name 		= $this->host_name;
		$post_vars 		= $this->post_vars;
		$get_vars 		= $this->get_vars;
		$cookie_vars 	= $this->cookie_vars;
		
		$sql = "INSERT INTO " .PIX_TABLE_PSDL. " VALUES (
														'0',
														'$ip_address',
														'$host_name',
														NOW(),
														'$post_vars',
														'$get_vars',
														'$cookie_vars'
														);";
		
		
		if ($this->_update_flag) {
		
			return $this->_dbl->sqlQuery($sql);
		
		} else {
		
			return false;
	
		}
		
	}
	
	/*
	*	Process an array of cookie, or form data
	*
	*	@return array $array
	*/
	function processArray ($data) {
	
		if (is_array($data)) {
		
			$i = 0;
			
			foreach ($data as $key => $value) {
			
				$array[$i] = array($key,$value);
				
				$i++;
				
			}
			
			return $array;
			
		}
	
	}
	
}

?>