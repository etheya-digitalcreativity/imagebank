<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

/*
*	
*	A class to handle working with digital product options
*	
*	Exposes the following methods:
*	
*/

class PixariaProductsDigital {
	
	// Private variables
	var $_dbl;
	var $_update_flag = false;
	
	// Public variables
	var $calc_id			= array();
	var $calc_name			= array();
	var $calc_active		= array();
	var $calc_description	= array();
	var $calc_ar_id			= array();
	var $calc_ar_name		= array();
	var $calc_ar_operand	= array();
	var $calc_ar_factor		= array();
	var $calc_ar_function	= array();
	var $products_digital;
	
	// Error log for malformed data
	var $error 		= false;
	var $error_log 	= array();
	
	/*
	*	
	*	This is the class constructor for the PixariaProductsDigital class
	*	
	*	PixariaProductsDigital -- Load data for all digital product options
	*	
	*	class PixariaProductsDigital()
	*
	*/
	
	function PixariaProductsDigital() {
		
		// Localise globals
		global $ses, $cfg;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		$rules	= $this->_dbl->sqlSelectRows("SELECT * FROM ".PIX_TABLE_CAOB." ORDER BY calc_order ASC");
		
		if (is_array($rules)) {
		
			foreach ($rules as $key => $value) {
			
				$this->calc_id[]			= $value['calc_id'];
				$this->calc_name[]			= $value['calc_name'];
				$this->calc_description[]	= $value['calc_description'];
				$this->calc_active[]		= (bool)$value['calc_active'];
				
				$options = $this->_dbl->sqlSelectRows("SELECT * FROM ".PIX_TABLE_CAAR." WHERE ar_calc_id = '".$value['calc_id']."' ORDER BY ar_name ASC");
				
				$ar_id			= array();
				$ar_name		= array();
				$ar_operand		= array();
				$ar_factor		= array();
				$ar_function	= array();
					
				if (is_array($options)) {
					
					foreach($options as $opt_key => $opt_value) {
					
						$ar_id[]		= $opt_value['ar_id']; 
						$ar_name[]		= $opt_value['ar_name']; 
						$ar_operand[]	= $opt_value['ar_operand']; 
						$ar_factor[]	= $opt_value['ar_factor']; 
					
					}
					
				}
				
				$this->calc_ar_id[]			= $ar_id;
				$this->calc_ar_name[]		= $ar_name;
				$this->calc_ar_operand[]	= $ar_operand;
				$this->calc_ar_factor[]		= $ar_factor;
				$this->calc_ar_function[]	= $ar_function;
				
			}
			
			$this->products_digital = true;
			
		} else {
		
			return;
			
		}
		
	}
	
	/*
	*	Return the calc_id
	*/
	function getCalculationId () {
	
		return $this->calc_id;
	
	}
		
	/*
	*	Return the calc_name
	*/
	function getCalculationName () {
	
		return $this->calc_name;
	
	}
		
	/*
	*	Return the calc_description
	*/
	function getCalculationDescription () {
	
		return $this->calc_description;
	
	}
		
	/*
	*	Return the calc_active
	*/
	function getCalculationActive () {
	
		return $this->calc_active;
	
	}
		
	/*
	*	Return the calc_ar_id
	*/
	function getArithmeticId () {
	
		return $this->calc_ar_id;
	
	}
		
	/*
	*	Return the calc_ar_name
	*/
	function getArithmeticName () {
	
		return $this->calc_ar_name;
	
	}
		
	/*
	*	Return the calc_ar_operand
	*/
	function getArithmeticOperand () {
	
		return $this->calc_ar_operand;
	
	}
		
	/*
	*	Return the calc_ar_factor
	*/
	function getArithmeticFactor () {
	
		return $this->calc_ar_factor;
	
	}
		
	/*
	*	Return the calc_ar_function
	*/
	function getArithmeticFunction () {
	
		return $this->calc_ar_function;
	
	}
		
	/*
	*	Return the products_digital
	*/
	function getProductsDigital () {
	
		return $this->products_digital;
	
	}
		
}


?>