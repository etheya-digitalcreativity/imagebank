<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

/*
*	
*	A class to manage Pixaria settings
*	
*	Exposes the following methods:
*	
*	
*/

class PixariaSettings {
	
	// Private variables
	var $_dbl;
	var $_settings;
	
	/*
	*	
	*	This is the class constructor for the PixariaSettings class
	*	
	*	PixariaSettings -- Manage settings and configuration
	*	
	*	mixed PixariaSettings(void)
	*
	*/
	
	function PixariaSettings() {
		
		global $cfg;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		// Get the stored settings from the database
		$this->_settings = $this->_dbl->sqlSelectRows("SELECT * FROM " .  PIX_TABLE_SETT);
		
		// Loads all the stored settings and pre-defined settings into an array
		$this->loadConfiguration();
		
	}
	
	/*
	*	
	*	loadConfiguration -- Load database settings and pre-defined variables into a top level array $cfg
	*	
	*	mixed loadConfiguration(void)
	*
	*/
	
	function loadConfiguration() {
		
		global $cfg, $objEnvData;
		
		//	Feed data into settings array
		for ($i=0; $i<count($this->_settings); $i++) {
			
			$name = $this->_settings[$i]['name'];
			
			if ($this->_settings[$i]['type'] == 'array') {

				$cfg['set'][$name] = unserialize(base64_decode($this->_settings[$i]['value']));
			
			} else {
			
				$cfg['set'][$name] = stripslashes($this->_settings[$i]['value']);
			
			}
			
		}
		
		if ($objEnvData->fetchCookie('pixaria_language') != '') {
			define("LOCAL_LANGUAGE",$objEnvData->fetchCookie('pixaria_language'));
		} else {
			define("LOCAL_LANGUAGE",$cfg['set']['local_language']);
		}
		
		// Preserve the default values for the number of images/galleries per page
		$cfg['set']['thumb_per_page_def']	= $cfg['set']['thumb_per_page'];
		$cfg['set']['gallery_per_page_def']	= $cfg['set']['gallery_per_page'];
		
		// The number of images that can be imported in one go in the admin area
		$cfg['set']['import_limit'] 		= "100";
		
		// Load variables from user preferences cookie
		if ($psg_prefs = $objEnvData->fetchCookie('psg_prefs')) {
		
			$vars = explode("|",$psg_prefs);
			
			// If the user has set a number of thumbnail items to show, set that as the default
			if (is_numeric($vars[1])) { $cfg['set']['thumb_per_page'] = $vars[1]; }
			
			// If the user has set a number of gallery items to show, set that as the default
			if (is_numeric($vars[2])) { $cfg['set']['gallery_per_page'] = $vars[2]; }
			
			// If the user has set a preferred thumbnail view use this
			if ($vars[3] == "large" || $vars[3] == "small") { $cfg['set']['view_thumb_view'] = $vars[3]; } else { $cfg['set']['view_thumb_view'] = "large"; }
			
		}
		
		// Shopping cart popup window sizes
		$cfg['set']['popup_cart_width']		= "700";
		$cfg['set']['popup_cart_height']	= "600";
		
		// If the photo storage directoy is an absolute path (Unix or Windows)
		if (substr($cfg['set']['path_library'],0,1) == '/' || substr($cfg['set']['path_library'],1,1) == ':') {
				
			//  Photo storage directory
			$cfg['sys']['base_library']			= $cfg['set']['path_library'];
			
		} else {
				
			//  Photo storage directory
			$cfg['sys']['base_library']			= SYS_BASE_PATH . $cfg['set']['path_library'];
			
		}
		
		define("SYS_BASE_LIBRARY",$cfg['sys']['base_library']);
		define("LIBRARY_PATH",$cfg['sys']['base_library']);
		
		// If the incoming directoy is an absolute path (Unix or Windows)
		if (substr($cfg['set']['path_incoming'],0,1) == '/' || substr($cfg['set']['path_incoming'],1,1) == ':') {
				
			//  FTP upload directory
			$cfg['sys']['base_incoming']		= $cfg['set']['path_incoming'];
			
		} else {
				
			//  FTP upload directory
			$cfg['sys']['base_incoming']		= SYS_BASE_PATH . $cfg['set']['path_incoming'];
			
		}
		
		define("SYS_BASE_URL",$cfg['sys']['base_url']);
		define("SYS_BASE_INCOMING",$cfg['sys']['base_incoming']);
		define("INCOMING_PATH",$cfg['sys']['base_incoming']);

		switch ($cfg['set']['store_currency']) {
		
			case "USD": // Currency is dollars
			case "CAD":
			case "AUD":
			case "NZD":
			case "SGD":
				$cfg['set']['store_symbol'] 	= "\$";
			break;
			
			case "GBP": // Currency is Pounds Sterling
				$cfg['set']['store_symbol'] 	= "&pound;";
			break;
			
			case "EUR": // Currency is Euros
				$cfg['set']['store_symbol'] 	= "&euro;";
			break;
			
			case "ILS": // Currency is Israeli New Shekel
				$cfg['set']['store_symbol'] 	= "&#8362;";
			break;
			
			case "JPY": // store_symbol is Japanese Yen
				$cfg['set']['store_symbol'] 	= "&yen;";
			break;
			
			case "MXN": // store_symbol is Mexican Pesos
				$cfg['set']['store_symbol'] 	= "Mex\$";
			break;
			
			case "CHF": // Currency is Swiss Francs
				$cfg['set']['store_symbol'] 	= "Fr ";
			break;
			
			case "HUF": // Currency is Hungarian Forint
				$cfg['set']['store_symbol'] 	= "Ft ";
			break;
			
			case "PLN": // Currency is Polish Zloty
				$cfg['set']['store_symbol'] 	= "z&#322; ";
			break;
			
			case "CZK": // Currency is Czech Koruna
				$cfg['set']['store_symbol'] 	= "K&#269; ";
			break;
			
			case "NOK": // Currency is Kroner
			case "DKK":
			case "SEK":
				$cfg['set']['store_symbol'] 	= "kr ";
			break;
			
			default:
				$cfg['set']['store_symbol'] 	= $cfg['set']['store_currency'];
			break;
		
		}
		
		$cfg['set']['gd_version']			= $this->getGDVersion();
		
		// Check if we can use popcard and web based uploading
		// These features require gd 2.0.1 Or higher
		if (($cfg['set']['gd_version'][1] >= 2) && ($cfg['set']['gd_version'][2] >= 0) && ($cfg['set']['gd_version'][3] > 0)) {
		
			$cfg['set']['gd_popcard']		= (bool)TRUE;
			$cfg['set']['gd_upload']		= (bool)TRUE;
		
		} elseif (($cfg['set']['gd_version'][1] >= 2) && ($cfg['set']['gd_version'][2] >= 0) && ($cfg['set']['gd_version'][3] >= 0)) {
		
			$cfg['set']['gd_popcard']		= (bool)FALSE;
			$cfg['set']['gd_upload']		= (bool)TRUE;
		
		} else {
		
			$cfg['set']['gd_popcard']		= (bool)FALSE;
			$cfg['set']['gd_upload']		= (bool)FALSE;
		
		}
		
		/*
		*
		* 	Get mysql server information
		*
		*/
		
		$cfg['set']['mysql_version'] = explode(".",mysql_get_server_info());
		
		if (($cfg['set']['mysql_version'][0] >= 4) && ($cfg['set']['mysql_version'][1] >= 0) && (substr($cfg['set']['mysql_version'][2],0,1) > 0)) {
		
			$cfg['set']['search_boolean'] = (bool)TRUE;
		
		} else {
		
			$cfg['set']['search_boolean'] = (bool)FALSE;
		
		}
		
		/*
		*
		* 	Find out whether the store module is installed (rudimentary but it works)
		*
		*/
		
		// *** DEPRECATED ***
		$cfg['sys']['store_enabled']	= (bool)TRUE;
		
		// Detect if the path is at the file system root
		if (substr($cfg['set']['temporary'],0,1) == "/" || substr($cfg['set']['temporary'],1,1) == ":") {
			
			$cfg['set']['temporary']	= $cfg['set']['temporary'];
			
		} else {
		
			$cfg['set']['temporary_original']	= $cfg['set']['temporary'];
			$cfg['set']['temporary']			= SYS_BASE_PATH . $cfg['set']['temporary'];
		
		}

		/*
		*
		* 	Define key system settings
		*
		*/
		
		// Unique reference id for this site (used for cookies and forms)
		$cfg['sys']['unique_id']			= strtoupper(substr(md5($_SERVER['SERVER_NAME']),0,6));
		
		// Path to the default front end template directory
		$cfg['sys']['default_template_dir'] = SYS_BASE_PATH."resources/templates_default/";
		
		$cfg['sys']['memory_limit'] = rtrim(get_cfg_var('memory_limit'),"M");
		
		// Check the PHP version
		$cfg['sys']['php_version'] = phpversion();
		
		if (version_compare($cfg['sys']['php_version'],'5.0.0') >= 0) { define("PHP_GT_5",true); } else { define("PHP_GT_5",false); }
		if (version_compare($cfg['sys']['php_version'],'6.0.0') >= 0) { define("PHP_GT_6",true); } else { define("PHP_GT_6",false); }
		
		/*
		*
		*	Tell Pixaria whether we need to obey image visibility settings
		*
		*/
		if (WORKSPACE == "FE") {
			$cfg['set']['image_visibility_sql'] 	= " AND ". PIX_TABLE_IMGS .".image_active = '1' ";
			$cfg['set']['gallery_visibility_sql'] 	= " AND ". PIX_TABLE_GALL .".gallery_active = '1' ";
		}
		
		// Get HTML stats code and remove slashes
		$cfg['set']['html_statcode'] = stripslashes($cfg['set']['html_statcode']);
		
	}

	/*
	*	Return the version number of the installed GD Library
	*	
	*	Thanks to: Justin Greer
	*/
	function getGDVersion() {
	
		static $gd_version_number = null;
		
		if ($gd_version_number === null) {
		
			/*	
			*	Use output buffering to get results from phpinfo() 
			*	without disturbing the page we're in.  Output 
			*	buffering is "stackable" so we don't even have to 
			*	worry about previous or encompassing buffering.
			*/
			
			ob_start();
			
			phpinfo(8);
			
			$module_info = ob_get_contents();
			ob_end_clean();
			if (preg_match("/\bgd\s+version\b[^\d\n\r]+?([\d\.]+)/i",
					$module_info,$matches)) {
				$gd_version_number = $matches[1];
			} else {
				$gd_version_number = 0;
			}
		}
		
		$fragments = explode(".",$gd_version_number);
		
		$gd_version_parts	= array();
		$gd_version_parts[]	= $gd_version_number;
		$gd_version_parts[]	= (int)$fragments[0];
		$gd_version_parts[]	= (int)$fragments[1];
		$gd_version_parts[]	= (int)$fragments[2];
		
		return $gd_version_parts;
		
	}
	
}

?>