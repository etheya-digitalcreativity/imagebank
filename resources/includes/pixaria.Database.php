<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*	
*	sql_select_row -- Select one row from table
*
*	mixed sql_select_field_random(string sql)
*
*	Returns FALSE if SQL fails or no rows found or bad argument count, otherwise Returns Array.
*
*	Array is row from specified SQL results.
*/

function sql_select_row($sql){
	
	$result = @mysql_query($sql);

	if (($numrow = @mysql_num_rows($result)) < 1) { return(FALSE); }

	$array = @mysql_fetch_array($result);
	
return($array);

}

/*
*	
*	sql_count -- Count number of rows.
*
*	mixed sql_count(string table, string field, [[string wname, string wvalue,] [string wname, string wvalue]])
*
*	Returns 0 if SQL fails or no rows found or bad argument count, otherwise Returns int which is count of specified   ##  SQL. 
*
*/

function sql_count($sql){
	
	$result = @mysql_query($sql);

	if(($numrow = @mysql_num_rows($result)) < 1){return('0');}

	$array = @mysql_fetch_array($result);
	
return($array[0]);

}

/*
*	
*	sql_select_rows -- Select rows
*
*	mixed sql_select_rows(string sql)
*
*	order should be asc or desc.
*
*	Returns FALSE if SQL fails or no rows found or bad argument count, otherwise Returns Multidimentional Array.
*
*	First Dimension uses numerical keys with offset 0 each representing a table row.
*
*	Second Dimension is the row as Column Name key and Column Value value.
*/

function sql_select_rows($sql) {
	
	$result = @mysql_query($sql);

	if(($numrow = @mysql_num_rows($result)) < 1){return(FALSE);}

	$i=0;while($i < $numrow){

		$array[$i] = @mysql_fetch_array($result);

		$i++;
	}

return($array);

}



?>