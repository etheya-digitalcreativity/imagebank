<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

/* ## @@ZEND@@ ## */

/*
*
*	
*
*/
class PixariaMetaData {
	
	var $XML_LIST_ELEMENTS;
	var $xmparray;
	var $image_info;
	var $iptcarray;
	var $exifarray;
	
	function ParseMetaData ($filename) {
		
		// Get image metadata from header information
		@getimagesize($filename, $this->image_info);
		
		$this->XML_LIST_ELEMENTS	= array( "rdf:li","rdf:Description" );
		$this->xmparray				= $this->extractXMPMetaData($filename);
		$this->iptcarray 			= $this->extractIPTCMetaData($filename);
		$this->exifarray 			= $this->extractEXIFMetaData($filename);
		
	}
	
	/*
	*
	*	extractEXIFMetaData -- Load key EXIF data from an image
	*	
	*	string  extractEXIFMetaData(string filepath)
	*	
	*	If EXIF data found returns an array of the data with the value of the first key set to true
	*	
	*	If EXIF data not found or and error occurs returns an array with the value of the first key set to false
	*
	*/
	function extractEXIFMetaData($filepath) {
		
		if (function_exists(exif_read_data)) {
			
			if (!@exif_read_data($filepath, 'IFD0')) {
			
				$exif_data['exif_data'] = false;
			
			} else {
			
				$exif = @exif_read_data($filepath, 0, true);
				
				$exif_date_dat = explode(" ",$exif['EXIF']['DateTimeOriginal']);
				
				$exif_date_ymd	= explode(":",$exif_date_dat[0]);
				$exif_date_hms	= explode(":",$exif_date_dat[1]);
				
				$exif_date_day	= $exif_date_ymd[2];
				$exif_date_mon	= $exif_date_ymd[1];
				$exif_date_yrs	= $exif_date_ymd[0];
				
				$exif_date_sec	= $exif_date_hms[2];
				$exif_date_min	= $exif_date_hms[1];
				$exif_date_hrs	= $exif_date_hms[0];
				
				## Create a Unix style timestamp for the image capture date
				$exif_date_unx	= @mktime($exif_date_hrs,$exif_date_min,$exif_date_sec,$exif_date_mon,$exif_date_day,$exif_date_yrs);
				
				$exif_data['exif_present']	= true;
				$exif_data['timestamp']		= $exif_date_unx;
				$exif_data['exif_data']		= $exif;
								
			}
		
		} else {
		
			$exif_data[0] = false;
		
		}
		
		return $exif_data;
	
	}

	/*
	*
	*	extractIPTCMetaData -- Get IPTC header information from a JPEG file.
	*	
	*	string extractIPTCMetaData(string filepath)
	*	
	*	Requires 1 argument.
	*	
	*	string is a filepath provided at runtime.
	*	
	*	Returns array for IPTC header strings.
	*
	*/
	function extractIPTCMetaData($image_path) {
	
		global $cfg;
		
		// Change: Allow this example file to be easily relocatable - as of version 1.11
		$Toolkit_Dir = SYS_BASE_PATH."resources/ext/jpeg_metadata_toolkit/";     // Ensure dir name includes trailing slash
		
		// Hide any unknown EXIF tags
		$GLOBALS['HIDE_UNKNOWN_TAGS'] = TRUE;
		
		require_once $Toolkit_Dir . 'Toolkit_Version.php';          // Change: added as of version 1.11
		require_once $Toolkit_Dir . 'JPEG.php';                     // Change: Allow this example file to be easily relocatable - as of version 1.11
		//include $Toolkit_Dir . 'JFIF.php';
		//include $Toolkit_Dir . 'PictureInfo.php';
		//include $Toolkit_Dir . 'XMP.php';
		require_once $Toolkit_Dir . 'Photoshop_IRB.php';
		//include $Toolkit_Dir . 'EXIF.php';
				
		// Retrieve the header information
		$jpeg_header_data = get_jpeg_header_data( $image_path );
		
		$iptc_data = get_Photoshop_IPTC( get_Photoshop_IRB( $jpeg_header_data ), $image_path );
		
		if (is_array($iptc_data)) {
		
			foreach ($iptc_data as $key => $value) {
				
				$type = $value['IPTC_Type'];
				
				$type = explode(":",$value['IPTC_Type']);
				
				$type = $type[0] . "#" . str_pad($type[1],3,"0",STR_PAD_LEFT);
				
				$iptc[$type][] = $value['RecData'];
				
			}
		
		}
		
		if (is_array($iptc)) {
		//print "<pre>";print_R($iptc);exit;
			foreach (array_keys($iptc) as $s) {   
			
				$c = count ($iptc[$s]);
				for ($i=0; $i <$c; $i++) {
				
					## Get the IPTC Headline
					if ($s == "2#105") { $IPTC['headline']	 	= safeConvertUTF8($iptc[$s][$i]); }
				
					## Get the IPTC Byline
					if ($s == "2#080") { $IPTC['byline']	 	= safeConvertUTF8($iptc[$s][$i]); }
				
					## Get the IPTC Byline Title
					if ($s == "2#085") { $IPTC['bltitle']	 	= safeConvertUTF8($iptc[$s][$i]); }
				
					## Get the IPTC Credit
					if ($s == "2#110") { $IPTC['credit']	 	= safeConvertUTF8($iptc[$s][$i]); }
				
					## Get the IPTC Source
					if ($s == "2#115") { $IPTC['source']	 	= safeConvertUTF8($iptc[$s][$i]); }
				
					## Get the IPTC Object Name
					if ($s == "2#005") { $IPTC['objname']	 	= safeConvertUTF8($iptc[$s][$i]); }
				
					## Get the IPTC City
					if ($s == "2#090") { $IPTC['city']		 	= safeConvertUTF8($iptc[$s][$i]); }
				
					## Get the IPTC Province
					if ($s == "2#095") { $IPTC['province']	 	= safeConvertUTF8($iptc[$s][$i]); }
				
					## Get the IPTC Country
					if ($s == "2#101") { $IPTC['country']	 	= safeConvertUTF8($iptc[$s][$i]); }
				
					## Get the IPTC Reference
					if ($s == "2#103") { $IPTC['reference']	 	= safeConvertUTF8($iptc[$s][$i]); }
				
					## Get the IPTC Copyright
					if ($s == "2#116") { $IPTC['copyright']	 	= safeConvertUTF8($iptc[$s][$i]); }
				
					## Get the IPTC Caption
					if ($s == "2#120") { $IPTC['caption']	 	= safeConvertUTF8($iptc[$s][$i]); }
				
					## Get the IPTC Date
					if ($s == "2#055") { $IPTC['date']		 	= $iptc[$s][$i]; }
				
					## Get the IPTC Writer/Editor
					if ($s == "2#122") { $IPTC['writer']	 	= safeConvertUTF8($iptc[$s][$i]); }
				
					## Get the instructions
					if ($s == "2#040") { $IPTC['instructions']	= safeConvertUTF8($iptc[$s][$i]); }
				
					## Get the Content Location Name
					if ($s == "2#024") { $IPTC['iv_location']	= safeConvertUTF8($iptc[$s][$i]); }
				
					## Get the IPTC Keywords
					if ($s == "2#025") {
					
						if ($x == 0) {
							$IPTC['keywords']	.= $iptc[$s][$i];
							$x = 1;
						} else {
							$IPTC['keywords']	.= ", " . $iptc[$s][$i];
						}
						
					}
		
				}
				
			} 
						   
		}
		
		$IPTC['descriptors']['keywords']		= "Keywords";								// Imported
		$IPTC['descriptors']['headline']		= "Headline";								// Imported
		$IPTC['descriptors']['byline']			= "Creator / Byline";						// 
		$IPTC['descriptors']['bltitle']			= "Creator's Jobtitle / Byline title";		// 
		$IPTC['descriptors']['credit']			= "Provider / Credit";						// 
		$IPTC['descriptors']['source']			= "Source";									// 
		$IPTC['descriptors']['objname']			= "Object Name";							// 
		$IPTC['descriptors']['city']			= "City";									// 
		$IPTC['descriptors']['province']		= "Province/State";							// 
		$IPTC['descriptors']['country']			= "Country";								// 
		$IPTC['descriptors']['reference']		= "Job Identifier / Reference";				// 
		$IPTC['descriptors']['copyright']		= "Copyright";								// Imported
		$IPTC['descriptors']['caption']			= "Caption / Description";					// Imported
		$IPTC['descriptors']['date']			= "Date";									// Imported
		$IPTC['descriptors']['writer']			= "Writer / Editor";						// 
		$IPTC['descriptors']['instructions']	= "Instructions";							// 
		$IPTC['descriptors']['iv_location']		= "iView MediaPro Location";				// 
		
		return $IPTC;         
		
	}
	
	function extractXMPMetaData ($filename) {
		
		// Get image metadata from header information
		@getimagesize($filename, $this->image_info);

		// Get the APP1 JPEG headers
		$source = $this->image_info['APP1'];
		
		// Extract clean XML RDF content from the APP1 header
		$xmpdata_start = strpos($source,"<x:xmpmeta");
		$xmpdata_end = strpos($source,"</x:xmpmeta>");
		$xmplength = $xmpdata_end-$xmpdata_start;
		$xmpdata = substr($source,$xmpdata_start,$xmplength+12);
		
		// Create a new XML parser
		$parser = xml_parser_create();
		xml_parser_set_option($parser,XML_OPTION_CASE_FOLDING,0);
		xml_parser_set_option($parser,XML_OPTION_SKIP_WHITE,1);
		xml_parse_into_struct($parser,$xmpdata,$values,$tags);
		xml_parser_free($parser);
		
		// Store the XML data
		$hash_stack = array();
		
		// This is the target for the data
		$ret = array();
		
		foreach ($values as $key => $val) {
		  
		  switch ($val['type']) {
			  case 'open':
				 array_push($hash_stack, $val['tag']);
				if (isset($val['attributes']))
					$ret = $this->composeXMLArray($ret, $hash_stack, $val['attributes']);
				else
					$ret = $this->composeXMLArray($ret, $hash_stack);
				break;
	
			  case 'close':
				 array_pop($hash_stack);
				break;
	
			  case 'complete':
				 array_push($hash_stack, $val['tag']);
				 $ret = $this->composeXMLArray($ret, $hash_stack, $val['value']);
				 array_pop($hash_stack);
				
				 // handle attributes
				 if (isset($val['attributes']))
				{
					while(list($a_k,$a_v) = each($val['attributes']))
					{
					   $hash_stack[] = $val['tag']."_attribute_".$a_k;
					   $ret = $this->composeXMLArray($ret, $hash_stack, $a_v);
					   array_pop($hash_stack);
					}
				}
				
				
				break;
		  }
		}
		
		return $ret;
	
	}
	
	/*
	*	Converts a parsed XML data source into an multidimensional array
	*
	*/
	function composeXMLArray ($array, $elements, $value=array())	{
	  
	   // get current element
	   $element = array_shift($elements);
	  
	   // does the current element refer to a list
	   if (in_array($element,$this->XML_LIST_ELEMENTS))
	  {
		  // more elements?
		  if(sizeof($elements) > 0)
		  {
			 $array[$element][sizeof($array[$element])-1] = $this->composeXMLArray($array[$element][sizeof($array[$element])-1], $elements, $value);
		  }
		  else // if (is_array($value))
		  {
			 $array[$element][sizeof($array[$element])] = $value;
		  }
	  }
	  else
	  {
		  // more elements?
		  if(sizeof($elements) > 0)
		  {
			 $array[$element] = $this->composeXMLArray($array[$element], $elements, $value);
		  }
		  else
		  {
			 $array[$element] = $value;
		  }
	  }
		
	  return $array;
	}

}



/*

    $xmp_parsed = array();
    
    
    $regexps = array(
    array("name" => "DC creator", "regexp" => "/<dc:creator>\s*<rdf:Seq>\s*<rdf:li>.+<\/rdf:li>\s*<\/rdf:Seq>\s*<\/dc:creator>/"),
    array("name" => "TIFF camera model", "regexp" => "/<tiff:Model>.+<\/tiff:Model>/"),
    array("name" => "TIFF maker", "regexp" => "/<tiff:Make>.+<\/tiff:Make>/"),
    array("name" => "EXIF exposure time", "regexp" => "/<exif:ExposureTime>.+<\/exif:ExposureTime>/"),
    array("name" => "EXIF f number", "regexp" => "/<exif:FNumber>.+<\/exif:FNumber>/"),
    array("name" => "EXIF aperture value", "regexp" => "/<exif:ApertureValue>.+<\/exif:ApertureValue>/"),
    array("name" => "EXIF exposure program", "regexp" => "/<exif:ExposureProgram>.+<\/exif:ExposureProgram>/"),
    array("name" => "EXIF iso speed ratings", "regexp" => "/<exif:ISOSpeedRatings>\s*<rdf:Seq>\s*<rdf:li>.+<\/rdf:li>\s*<\/rdf:Seq>\s*<\/exif:ISOSpeedRatings>/"),
    array("name" => "EXIF datetime original", "regexp" => "/<exif:DateTimeOriginal>.+<\/exif:DateTimeOriginal>/"),
    array("name" => "EXIF exposure bias value", "regexp" => "/<exif:ExposureBiasValue>.+<\/exif:ExposureBiasValue>/"),
    array("name" => "EXIF metering mode", "regexp" => "/<exif:MeteringMode>.+<\/exif:MeteringMode>/"),
    array("name" => "EXIF focal lenght", "regexp" => "/<exif:FocalLength>.+<\/exif:FocalLength>/"),
    array("name" => "AUX lens", "regexp" => "/<aux:Lens>.+<\/aux:Lens>/")
    );
    
    foreach ($regexps as $key => $k) {
            $name         = $k["name"];
            $regexp     = $k["regexp"];
            unset($r);
            preg_match ($regexp, $xmpdata, $r);
            $xmp_item = "";
            $xmp_item = @$r[0];
            array_push($xmp_parsed,array("item" => $name, "value" => $xmp_item));
    }
    
    if ($printout == 1) {
        foreach ($xmp_parsed as $key => $k) {
                $item         = $k["item"];
                $value         = $k["value"];
                print "<br><b>" . $item . ":</b> " . $value;
        }
    }

return ($xmp_parsed);


*/

?>
