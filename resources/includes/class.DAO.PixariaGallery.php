<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

class DAO_PixariaGallery {
	
	var $_dbl;
	var $_update_flag		= false;
	
	var $gallery_id;
	var $gallery_active;
	var $gallery_archived;
	var $gallery_date;
	var $gallery_parent;
	var $gallery_type;
	var $gallery_userid;
	var $gallery_title;
	var $gallery_short_title;
	var $gallery_description;
	var $gallery_password;
	var $gallery_permissions;
	var $gallery_key;
	var $gallery_created;
	var $gallery_edited;
	var $gallery_counter;
	var $gallery_rss;
	var $gallery_image_count;
	var $gallery_viewers;
	var $gallery_valid;
	var $gallery_errors;

	function DAO_PixariaGallery ($gallery_id = "") {
	
		global $cfg, $ses;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		if (is_numeric($gallery_id)) {
			
			// Get the total number of images in this gallery that can be seen by this user
			$sql	 = "SELECT COUNT(DISTINCT ". PIX_TABLE_IMGS .".image_id) FROM `". PIX_TABLE_GORD ."`
			
						LEFT JOIN `". PIX_TABLE_IMGS ."` ON ". PIX_TABLE_GORD .".image_id = ". PIX_TABLE_IMGS .".image_id
						
						LEFT JOIN `". PIX_TABLE_IMVI ."` ON ". PIX_TABLE_IMGS .".image_id = ". PIX_TABLE_IMVI .".image_id
					
						WHERE ". PIX_TABLE_GORD .".gallery_id = '".$this->_dbl->escape($gallery_id)."'
						
						".$cfg['set']['image_access_sql']."";

			$this->gallery_image_count = $this->_dbl->sqlCountRows($sql);
		
			// Get an array of information about this gallery
			$gallery_info = $this->_dbl->sqlSelectRow("SELECT * FROM ". PIX_TABLE_GALL ." WHERE gallery_id = '".$this->_dbl->escape($gallery_id)."'");
			
			$this->gallery_id			= $gallery_info['gallery_id'];
			$this->gallery_active		= $gallery_info['gallery_active'];
			$this->gallery_archived		= $gallery_info['gallery_archived'];
			$this->gallery_date			= $gallery_info['gallery_date'];
			$this->gallery_parent		= $gallery_info['gallery_parent'];
			$this->gallery_type			= $gallery_info['gallery_type'];
			$this->gallery_userid		= $gallery_info['gallery_userid'];
			$this->gallery_title		= $gallery_info['gallery_title'];
			$this->gallery_short_title	= $gallery_info['gallery_short_title'];
			$this->gallery_description	= $gallery_info['gallery_description'];
			$this->gallery_password		= $gallery_info['gallery_password'];
			$this->gallery_permissions	= $gallery_info['gallery_permissions'];
			$this->gallery_key			= $gallery_info['gallery_key'];
			$this->gallery_created		= $gallery_info['gallery_created'];
			$this->gallery_edited		= $gallery_info['gallery_edited'];
			$this->gallery_counter		= $gallery_info['gallery_counter'];
			$this->gallery_rss			= $gallery_info['gallery_rss'];
			
			// Get the groups who can see this gallery
			$result = $this->_dbl->sqlSelectRows("SELECT userid FROM ".PIX_TABLE_GVIE." WHERE gallery_id = '".$this->_dbl->escape($gallery_id)."'");

			if (is_array($result)) {
				
				foreach ($result as $key => $value) {
				
					$this->gallery_viewers[] = $value['userid'];
				
				}
			
			} else {
			
				$this->gallery_viewers = array();
			
			}
			
			
		} else {
		
			return;
		
		}
		
	}
	
	
	/*
	*	Get the gallery ID
	*/
	function getGalleryId() {
	
		return $this->gallery_id;
	
	}

	/*
	*	Get the gallery date
	*/
	function getGalleryDate() {
	
		return $this->gallery_date;
	
	}

	/*
	*	Get the gallery parent ID
	*/
	function getGalleryParentId() {
	
		return $this->gallery_parent;
	
	}

	/*
	*	Get the gallery parent ID
	*/
	function getGalleryParentTitle() {
		
		if (is_numeric($this->gallery_parent)) {
			$result = $this->_dbl->sqlSelectRow("SELECT gallery_title FROM ".PIX_TABLE_GALL." WHERE gallery_id = '".$this->_dbl->escape($this->gallery_parent)."'");
			return $result['gallery_title'];
		} else {
			return false;
		}
	}

	/*
	*	Get the gallery type
	*/
	function getGalleryType() {
	
		return $this->gallery_type;
	
	}

	/*
	*	Get the gallery user ID
	*/
	function getGalleryUserId() {
	
		return $this->gallery_userid;
	
	}

	/*
	*	Get the gallery title
	*/
	function getGalleryTitle() {
	
		return $this->gallery_title;
	
	}

	/*
	*	Get the gallery active
	*/
	function getGalleryActive() {
	
		return $this->gallery_active;
	
	}

	/*
	*	Get the gallery archived
	*/
	function getGalleryArchived() {
	
		return $this->gallery_archived;
	
	}

	/*
	*	Get the gallery short title
	*/
	function getGalleryShortTitle() {
	
		return $this->gallery_short_title;
	
	}

	/*
	*	Get the gallery description
	*/
	function getGalleryDescription() {
	
		return $this->gallery_description;
	
	}

	/*
	*	Get the gallery password
	*/
	function getGalleryPassword() {
	
		return $this->gallery_password;
	
	}

	/*
	*	Get the gallery permissions
	*/
	function getGalleryPermissions() {
	
		return $this->gallery_permissions;
	
	}

	/*
	*	Get the gallery key image ID
	*/
	function getGalleryKeyImageId() {
	
		return $this->gallery_key;
	
	}

	/*
	*	Get the gallery creation date
	*/
	function getGalleryDateCreated() {
	
		return $this->gallery_created;
	
	}

	/*
	*	Get the gallery edit date
	*/
	function getGalleryDateEdited() {
	
		return $this->gallery_edited;
	
	}

	/*
	*	Get the gallery view count
	*/
	function getGalleryViewCount() {
	
		return $this->gallery_counter;
	
	}

	/*
	*	Get the gallery RSS setting
	*/
	function getGalleryRss() {
	
		return $this->gallery_rss;
	
	}

	/*
	*	Get the gallery image count
	*/
	function getGalleryImageCount() {
	
		return $this->gallery_image_count;
	
	}

	/*
	*	Get the number of child galleries of this one
	*/
	function getGalleryChildCount () {
		
		// Set global variables
		global $cfg, $ses;
		
		// If no gallery ID is set for this object, use a default of zero
		if ($this->gallery_id == ""): $gallery_id = "0"; else: $gallery_id = $this->gallery_id; endif;
		
		// Get the total number of child galleries for this gallery
		$sql = "SELECT COUNT(DISTINCT ". PIX_TABLE_GALL .".gallery_id) FROM `". PIX_TABLE_GALL ."`
		
				LEFT JOIN ".PIX_TABLE_GVIE." ON ".PIX_TABLE_GALL.".gallery_id = ".PIX_TABLE_GVIE.".gallery_id
				
				WHERE ".PIX_TABLE_GALL.".gallery_parent = '".$this->_dbl->escape($gallery_id)."'
				
				".$cfg['set']['gallery_access_sql']."";

		return $this->_dbl->sqlCountRows($sql);
		
	}
	
	/*
	*	Get array of gallery viewer groups
	*/
	function getGalleryViewers() {
		return $this->gallery_viewers;
	}
	
	/*
	*	Get array of gallery viewer group names
	*/
	function getGalleryViewerGroupNames() {
		if (is_array($this->gallery_viewers)) {
			foreach ($this->gallery_viewers as $key => $value) {
				$data[] = getGroupName($value);
			}
		}
		return $data;
	}
	
	/*
	*	Get whether or not the gallery information is valid
	*/
	function getGalleryValid() {
		return $this->gallery_valid;
	}
	
	/*
	*	Get any errors in the gallery information
	*/
	function getGalleryErrors() {
		return $this->gallery_errors;
	}
	
	/*
	*	Get gallery image display order
	*/
	function getGalleryImageOrder () {
	
		// Get the image sort order for this gallery from the database
		$gallery_info = $this->_dbl->sqlSelectRows("SELECT image_id FROM ".PIX_TABLE_GORD." WHERE gallery_id = '".$this->_dbl->escape($this->gallery_id)."' ORDER BY order_id ASC");
		
		if (is_array($gallery_info) && count($gallery_info) > 0) {
		
			foreach ($gallery_info as $key => $value) {
			
				$gallery_order[] = $value['image_id'];
			
			}
		
			return $gallery_order;
		
		} else {
		
			return false;
		
		}
		
	}
	
	/*
	*	Set the gallery date
	*/
	function setGalleryDate($var) {
	
		$this->gallery_date 	= trim($var);
		$this->_update_flag		= (bool)true;
	
	}

	/*
	*	Set the gallery parent
	*/
	function setGalleryParent($var) {
	
		$this->gallery_parent 	= $var;
		$this->_update_flag		= (bool)true;
	
	}

	/*
	*	Set the gallery type
	*/
	function setGalleryType($var) {
	
		$this->gallery_type 	= $var;
		$this->_update_flag		= (bool)true;
	
	}

	/*
	*	Set the gallery type
	*/
	function setGalleryUserId($var) {
		if (is_numeric($var)) {
			$this->gallery_userid	= $var;
			$this->_update_flag		= (bool)true;
		}
	}

	/*
	*	Set the gallery title
	*/
	function setGalleryTitle($var) {
	
		$this->gallery_title	= trim($var);
		$this->_update_flag		= (bool)true;
	
	}

	/*
	*	Set the gallery active
	*/
	function setGalleryActive($var) {
		
		if ($var == "on" || $var == 1) {
		
			$this->gallery_active	= 1;
			$this->_update_flag		= (bool)true;
		
		} else {
		
			$this->gallery_active	= 0;
			$this->_update_flag		= (bool)true;
			
		}
	
	}

	/*
	*	Set the gallery archived
	*/
	function setGalleryArchived($var) {
		
		if ($var == "on" || $var == 1) {
		
			$this->gallery_archived	= 1;
			$this->_update_flag		= (bool)true;
		
		} else {
		
			$this->gallery_archived	= 0;
			$this->_update_flag		= (bool)true;
			
		}
	
	}

	/*
	*	Set the gallery title
	*/
	function setGalleryShortTitle($var) {
	
		$this->gallery_short_title	= trim($var);
		$this->_update_flag			= (bool)true;
	
	}

	/*
	*	Set the gallery description
	*/
	function setGalleryDescription($var) {
	
		$this->gallery_description	= trim($var);
		$this->_update_flag			= (bool)true;
	
	}

	/*
	*	Set the gallery password
	*/
	function setGalleryPassword($var) {
	
		$this->gallery_password	= trim($var);
		$this->_update_flag		= (bool)true;
	
	}

	/*
	*	Set the gallery permissions
	*/
	function setGalleryPermissions($var) {
	
		$this->gallery_permissions	= $var;
		$this->_update_flag			= (bool)true;
	
	}

	/*
	*	Set the gallery key image ID
	*/
	function setGalleryKeyImageId($var) {
		if (is_numeric($var)) {
			$this->gallery_key	= trim($var);
			$this->_update_flag	= (bool)true;
		}
	}

	/*
	*	Set the gallery creation date
	*/
	function setGalleryDateCreated($var) {
	
		$this->gallery_created	= $var;
		$this->_update_flag		= (bool)true;
	
	}

	/*
	*	Set the gallery edit date
	*/
	function setGalleryDateEdited($var) {
	
		$this->gallery_edited	= $var;
		$this->_update_flag		= (bool)true;
	
	}

	/*
	*	Set the gallery view count
	*/
	function setGalleryViewCount($var) {
	
		$this->gallery_counter	= $var;
		$this->_update_flag		= (bool)true;
	
	}
	
	/*
	*	Set the gallery RSS setting
	*/
	function setGalleryRss($var) {
	
		$this->gallery_rss	= $var;
		$this->_update_flag	= (bool)true;
	
	}
	
	/*
	*	Set the gallery viewers
	*/
	function setGalleryViewers($var) {
	
		$this->gallery_viewers	= $var;
		$this->_update_flag		= (bool)true;
	
	}
	
	/*
	*	Create a new gallery
	*/
	function createGallery () {
		
		global $ses;
		
		if ($this->_update_flag) {
		
			// Variables defined by the system
			$gallery_userid			= $ses['psg_userid'];
			
			// Define the SQL string to create this gallery
			$sql = "INSERT INTO ".PIX_TABLE_GALL." VALUES (
				
					'0',
					'".$this->_dbl->escape($this->gallery_active)."',
					'".$this->_dbl->escape($this->gallery_archived)."',
					'".$this->_dbl->escape($this->gallery_date)."',
					'".$this->_dbl->escape($this->gallery_parent)."',
					'10',
					'".$this->_dbl->escape($this->gallery_userid)."',
					'".$this->_dbl->escape($this->gallery_title)."',
					'".$this->_dbl->escape($this->gallery_short_title)."',
					'".$this->_dbl->escape($this->gallery_description)."',
					'".$this->_dbl->escape($this->gallery_password)."',
					'".$this->_dbl->escape($this->gallery_permissions)."',
					'".$this->_dbl->escape($this->gallery_key)."',
					NOW(),
					NOW(),
					'',
					'',
					'',
					'',
					'0',
					'".$this->_dbl->escape($this->gallery_rss)."'
					
					)";
			
			$this->_dbl->sqlQuery($sql);
			
			$this->gallery_id = $this->_dbl->sqlInsertId();
			
			$this->applyGalleryViewerRules();
			
			return true;
			
		} else {
		
			return false;
		
		}
	
	}
	
	/*
	*	Save changes to a gallery
	*/
	function editGallery () {
		
		if ($this->_update_flag) {
		
			// Write SQL to update this gallery
			$sql = "UPDATE ".PIX_TABLE_GALL." SET
			
					gallery_parent			= '".$this->_dbl->escape($this->gallery_parent)."',
					gallery_active			= '".$this->_dbl->escape($this->gallery_active)."',
					gallery_archived		= '".$this->_dbl->escape($this->gallery_archived)."',
					gallery_title			= '".$this->_dbl->escape($this->gallery_title)."',
					gallery_short_title		= '".$this->_dbl->escape($this->gallery_short_title)."',
					gallery_description		= '".$this->_dbl->escape($this->gallery_description)."',
					gallery_key				= '".$this->_dbl->escape($this->gallery_key)."',
					gallery_permissions		= '".$this->_dbl->escape($this->gallery_permissions)."',
					gallery_password 		= '".$this->_dbl->escape($this->gallery_password)."',
					gallery_edited			= NOW(),
					gallery_date			= '".$this->_dbl->escape($this->gallery_date)."',
					gallery_rss				= '".$this->_dbl->escape($this->gallery_rss)."'
					
					WHERE gallery_id = '$this->gallery_id'";
			
			// Run the database update for this gallery
			$this->_dbl->sqlQuery($sql);
			
			$this->applyGalleryViewerRules();
			
			return true;
			
		} else {
		
			return false;
		
		}
 
 	}
	
	/*
	*	Delete a gallery ($gallery_parent is an optional directory to move sub galleries into)
	*/
	function deleteGallery ($gallery_parent) {
		
		if (is_numeric($this->gallery_id)) {
		
			// Delete the gallery
			$this->_dbl->sqlQuery("DELETE FROM ".PIX_TABLE_GALL." WHERE gallery_id = '$this->gallery_id'");
			
			// Delete the gallery order information
			$this->_dbl->sqlQuery("DELETE FROM ".PIX_TABLE_GORD." WHERE gallery_id = '$this->gallery_id'");
			
			// Delete the gallery group information
			$this->_dbl->sqlQuery("DELETE FROM ".PIX_TABLE_GVIE." WHERE gallery_id = '$this->gallery_id'");
			
			if (is_numeric($gallery_parent)) {
				// Move child galleries of this one into their new location
				$this->_dbl->sqlQuery("UPDATE ".PIX_TABLE_GALL." SET gallery_parent = '$gallery_parent' WHERE gallery_parent = '$this->gallery_id'");
			} else {
				// Move child galleries of this one into their new location
				$this->_dbl->sqlQuery("UPDATE ".PIX_TABLE_GALL." SET gallery_parent = '0' WHERE gallery_parent = '$this->gallery_id'");
			}
			
			return true;
			
		} else {
		
			return false;
		
		}
		
	}
	
	/*
	*	Apply group permissions
	*/
	function applyGalleryViewerRules () {
		
		if ($this->_update_flag && is_array($this->gallery_viewers) && $this->gallery_permissions == "12" && count($this->gallery_viewers) > 0) {
			
			// Delete any existing rules
			$this->_dbl->sqlQuery("DELETE FROM ".PIX_TABLE_GVIE." WHERE gallery_id = '$this->gallery_id';");
			
			foreach ($this->gallery_viewers as $key => $value) {
				
				// Insert new rules
				$this->_dbl->sqlQuery("INSERT into ".PIX_TABLE_GVIE." VALUES ('$this->gallery_id','$value')");
			
			}
		
		}
	
	}
	
	/*
	*	Add images with path $image_path to this gallery
	*/
	function addImagesByPath ($image_path) {
	
		if ($image_path != "" && is_numeric($this->gallery_id)) {
			
			// Load these images into an array
			$image_array = $this->_dbl->sqlSelectRows("SELECT image_id FROM ".PIX_TABLE_IMGS." WHERE image_path = '$image_path'");
			
			if (count($image_array) > 0 && is_array($image_array)) {
				
				foreach($image_array as $key => $value) {
				
					$image_id = $value['image_id'];
					$this->_dbl->sqlQuery("INSERT INTO ".PIX_TABLE_GORD." VALUES ('$key','$this->gallery_id','$image_id');");
					
				}
				
				return true;
			
			} else {
		
				return false;
		
			}
		
		} else {
		
			return false;
		
		}
	
	}
	
	/*
	*	Add an array $images of images to this gallery
	*/
	function addImagesById ($images, $insert_location="") {
		
		// If we are getting a string of comma separated image_ids, break into an array
		if ($images != "" && is_string($images)) {

			// Load these images into an array
			$image_order = explode(",",$images);

		}
		
		// If the $images variable is an array
		if (is_array($images)) {
		
			$image_order = $images;
		
		}
		
		if (!is_array($image_order)) {
		
			return false;
		
		}
		
		// Get the original gallery order
		$gallery_order = $this->getGalleryImageOrder();
		
		// If there is a gallery_order merge it with the new images
		if (is_array($gallery_order) && count($gallery_order) > 0 && $gallery_order[0] != "") {
				
			// Set where to insert the images into the gallery
			if ($insert_location == "start") {
			
				// Merge these arrays to get new gallery order
				$gallery_order	= array_merge($image_order,$gallery_order);
			
			} else {
			
				// Merge these arrays to get new gallery order
				$gallery_order	= array_merge($gallery_order,$image_order);
			
			}
			
		} else {
		
			// If there are no images in the gallery, use the list of images instead
			$gallery_order = $image_order;
		
		}
		
		if (is_array($gallery_order) && count($gallery_order) > 0 && is_numeric($this->gallery_id)) {
			
			// Remove existing images
			$this->_dbl->sqlQuery("DELETE FROM ".PIX_TABLE_GORD." WHERE gallery_id = '$this->gallery_id'");
			
			foreach(array_unique($gallery_order) as $key => $value) {
				
				// Add the combine old and new images
				$this->_dbl->sqlQuery("INSERT INTO ".PIX_TABLE_GORD." VALUES ('$key','$this->gallery_id','$value');");
				
			}
		
		} else {
		
			return false;
		
		}
	
	}
	
	/*
	*	Removed images from gallery (takes array of image_id values)
	*/
	function removeImagesById ($images) {
		
		if (count($images) > 0 && is_array($images) && is_numeric($this->gallery_id)) {
		
			foreach ($images as $key => $value) {
				
				$sql = "DELETE FROM ".PIX_TABLE_GORD." WHERE image_id = '".$this->_dbl->escape($value)."' AND gallery_id = '$this->gallery_id'";
				
				$this->_dbl->sqlQuery($sql);
				
			}
			
			return true;
			
		} else {
		
			return false;
		
		}
		
	}
	
	/*
	*	Validate that required information is present
	*/
	function validateGalleryInformation ($mode) {
	
		if ($this->gallery_title == "") {
		
			$problem = "1";
			$problem_status[] = "You need to give the gallery a title.";
			
		} else {
			
			if ($mode != "edit") {

				$result = $this->_dbl->sqlCountRows("SELECT COUNT(gallery_id) FROM ".PIX_TABLE_GALL." WHERE gallery_title = '$this->gallery_title' AND gallery_parent = '$this->gallery_parent'");
				if ($result > 0) {
					$problem = "1";
					$problem_status[] = "A gallery with the same name and parent gallery already exists.  Please choose a new name or parent gallery.";
				}

			}
			
		}
	
		if ($this->gallery_short_title == "") {
		
			$problem = "1";
			$problem_status[] = "You need to give your gallery a short title.";
			
		} else {
			
			if (eregi('[^a-zA-Z0-9_-]{1,}', $this->gallery_short_title)) {
				$problem = "1";
				$problem_status[] = "The gallery short title contains invalid characters.  You can only use letters, numbers and the hyphen (dash) and underscore signs.";
			}
			
			if ($this->gallery_short_title == "main") {
				$problem = "1";
				$problem_status[] = "The word 'main' is reserved for use by Pixaria and cannot be used as a short name.";
			}
			
			if ($mode != "edit") {

				$result = $this->_dbl->sqlCountRows("SELECT COUNT(gallery_id) FROM ".PIX_TABLE_GALL." WHERE gallery_short_title = '$this->gallery_short_title';");
				if ($result > 0) {
					$problem = "1";
					$problem_status[] = "The short title you gave the gallery already exists.  Please choose another.";
				}
			
			}
			
		}
		
		if ($this->gallery_permissions == "12" && (!is_array($this->gallery_viewers) || count($this->gallery_viewers) < 1)) {
			$problem = "1";
			$problem_status[] = "You are restricting access to this gallery by user group but have not granted any groups viewing privileges.  Please select some groups who can see this gallery.";
		}
		
		if ($problem == "1") {
			
			$this->gallery_valid	= (bool)false;
			$this->gallery_errors	= $problem_status;
			
		} else {
		
			$this->gallery_valid	= (bool)true;
			$this->gallery_errors	= '';
			
		}
		
	}
	
}


?>