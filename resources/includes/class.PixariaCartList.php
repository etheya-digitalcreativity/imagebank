<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

/*
*	
*	A class to handle working with the shopping cart
*	
*	Exposes the following methods:
*	
*/

/*
*	
*	Define the different cart status types
*	
*/

define ("PIX_CART_ACTIVE",1);
define ("PIX_CART_AWAIT_QUOTE","");
define ("PIX_CART_AWAIT_ACCEPTANCE",2);
define ("PIX_CART_AWAIT_PAYMENT",3);
define ("PIX_CART_AWAIT_SHIPPING","");
define ("PIX_CART_COMPLETE",4);

class PixariaCartList {
	
	// Private variables
	var $_dbl;
	
	// Public variables
	var $cart_id				= array();
	var $cart_store_type		= array();
	var $cart_status			= array();
	var $cart_userid			= array();
	var $cart_subtotal			= array();
	var $cart_tax				= array();
	var $cart_tax_total			= array();
	var $cart_shipping			= array();
	var $cart_total				= array();
	var $cart_transaction_id	= array();
	var $cart_remote_txn_id		= array();
	var $cart_paid				= array();
	var $cart_payment_method	= array();
	var $cart_usage_text		= array();
	var $cart_transaction_info	= array();
	var $cart_date_created		= array();
	var $cart_date_processed	= array();
	var $cart_date_completed	= array();
	
	var $email_address			= array();
	var $full_name				= array();
	var $formal_title			= array();
	var $other_title			= array();
	var $first_name				= array();
	var $middle_initial			= array();
	var $family_name			= array();
	var $telephone				= array();
	var $mobile					= array();
	var $fax					= array();
	var $addr1					= array();
	var $addr2					= array();
	var $addr3					= array();
	var $city					= array();
	var $region					= array();
	var $country				= array();
	var $postal_code			= array();
	
	var $cart_info				= array();
	
	// Error log for malformed data
	var $error 		= false;
	var $error_log 	= array();
	
	/*
	*	
	*	This is the class constructor for the PixariaCart class
	*	
	*	PixariaCartList -- Load data for a shopping cart
	*	
	*	class PixariaCartList()
	*
	*/
	
	function PixariaCartList() {
		
		// Localise globals
		global $ses, $cfg;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
	}
	
	/*
	*	List all carts within a specified date range
	*/
	function cartsByDateRange ($from_date,$to_date) {
		
		$sql = "SELECT *, CONCAT(".PIX_TABLE_USER.".first_name,' ',".PIX_TABLE_USER.".family_name) AS full_name FROM ".PIX_TABLE_CART."
				
				LEFT JOIN ".PIX_TABLE_USER." ON ".PIX_TABLE_CART.".userid = ".PIX_TABLE_USER.".userid
				
				WHERE status = '4' AND date_completed > '$from_date'
				
				AND date_completed < '$to_date'
				
				ORDER BY date_completed ASC";
		
		$cart_info 	= $this->_dbl->sqlSelectRows($sql);
		
		if (is_array($cart_info)) {
			
			foreach ($cart_info as $key => $value) {
				
				// Cart specific data
				$this->cart_id[]				= $value['cart_id'];
				$this->cart_store_type[]		= $value['store_type'];
				$this->cart_status[]			= $value['status'];
				$this->cart_userid[]			= $value['userid'];
				$this->cart_subtotal[]			= $value['subtotal'];
				$this->cart_tax[]				= $value['tax'];
				$this->cart_tax_total[]			= $value['tax_total'];
				$this->cart_shipping[]			= $value['shipping'];
				$this->cart_total[]				= $value['total'];
				$this->cart_transaction_id[]	= $value['transaction_id'];
				$this->cart_remote_txn_id[]		= $value['remote_txn_id'];
				$this->cart_paid[]				= $value['paid'];
				$this->cart_payment_method[]	= $value['method'];
				$this->cart_usage_text[]		= $value['usage_text'];
				$this->cart_transaction_info[]	= $value['transaction_info'];
				$this->cart_date_created[]		= $value['date_created'];
				$this->cart_date_processed[]	= $value['date_processed'];
				$this->cart_date_completed[]	= $value['date_completed'];
				
				// User specific data
				$this->email_address[]			= $value['email_address'];
				$this->full_name[]				= $value['first_name'] . " " . $value['family_name'];
				$this->formal_title[]			= $value['formal_title'];
				$this->other_title[]			= $value['other_title'];
				$this->first_name[]				= $value['first_name'];
				$this->middle_initial[]			= $value['middle_initial'];
				$this->family_name[]			= $value['family_name'];
				$this->telephone[]				= $value['telephone'];
				$this->fax[]					= $value['fax'];
				$this->addr1[]					= $value['addr1'];
				$this->addr2[]					= $value['addr2'];
				$this->addr3[]					= $value['addr3'];
				$this->city[]					= $value['city'];
				$this->region[]					= $value['region'];
				$this->country[]				= $value['country'];
				$this->postal_code[]			= $value['postal_code'];
				
				// The raw SQL output
				$this->cart_info				= $cart_info;
			
			}

		} else {
		
			return;
		
		}
		
	}
	
	/*
	*	Return the cart_store_type
	*/
	function getStoreType () {
	
		return $this->cart_store_type;
	
	}
		
	/*
	*	Return the cart_status
	*/
	function getStatus () {
	
		return $this->cart_status;
	
	}
		
	/*
	*	Return the cart_userid
	*/
	function getUserId () {
	
		return $this->cart_userid;
	
	}
		
	/*
	*	Return the cart_subtotal
	*/
	function getSubTotal () {
	
		return $this->cart_subtotal;
	
	}
		
	/*
	*	Return the cart_tax
	*/
	function getTax () {
	
		return $this->cart_tax;
	
	}
		
	/*
	*	Return the cart_tax_total
	*/
	function getTaxTotal () {
	
		return $this->cart_tax_total;
	
	}
		
	/*
	*	Return the cart_shipping
	*/
	function getShipping () {
	
		return $this->cart_shipping;
	
	}
		
	/*
	*	Return the cart_total
	*/
	function getTotal () {
	
		return $this->cart_total;
	
	}
		
	/*
	*	Return the cart_transaction_id
	*/
	function getTransactionId () {
	
		return $this->cart_transaction_id;
	
	}
		
	/*
	*	Return the cart_remote_txn_id
	*/
	function getRemoteTransactionId () {
	
		return $this->cart_remote_txn_id;
	
	}
		
	/*
	*	Return the cart_paid
	*/
	function getPaid () {
	
		return $this->cart_paid;
	
	}
		
	/*
	*	Return the cart_payment_method
	*/
	function getPaymentMethod () {
	
		return $this->cart_payment_method;
	
	}
		
	/*
	*	Return the cart_usage_text
	*/
	function getUsageText () {
	
		return $this->cart_usage_text;
	
	}
		
	/*
	*	Return the cart_transaction_info
	*/
	function getTransactionInfo () {
	
		return $this->cart_transaction_info;
	
	}
		
	/*
	*	Return the cart_date_created
	*/
	function getDateCreated () {
	
		return $this->cart_date_created;
	
	}
		
	/*
	*	Return the cart_date_processed
	*/
	function getDateProcessed () {
	
		return $this->cart_date_processed;
	
	}
		
	/*
	*	Return the cart_date_completed
	*/
	function getDateCompleted () {
	
		return $this->cart_date_completed;
	
	}
		
	function getEmail() {
	
		return 	$this->email_address;
		
	}
	
	function getSalutation() {
	
		return 	$this->formal_title;
		
	}
	
	function getOtherSalutation() {
	
		return 	$this->other_title;
		
	}
	
	function getFirstName() {
	
		return 	$this->first_name;
		
	}
	
	function getName() {
	
		return 	$this->full_name;
		
	}
	
	function getInitial() {

		return 	$this->middle_initial;

	}
	
	function getFamilyName() {

		return 	$this->family_name;

	}
	
	function getTelephone() {

		return 	$this->telephone;

	}
	
	function getMobile() {

		return 	$this->mobile;

	}
	
	function getFax() {

		return 	$this->fax;

	}
	
	function getAddress1() {

		return 	$this->addr1;

	}
	
	function getAddress2() {

		return 	$this->addr2;

	}
	
	function getAddress4() {

		return 	$this->addr3;

	}
	
	function getCity() {

		return 	$this->city;

	}
	
	function getRegion() {

		return 	$this->region;

	}
	
	function getCountry() {

		return 	$this->country;

	}
	
	function getPostCode() {

		return 	$this->postal_code;

	}
	
}


?>