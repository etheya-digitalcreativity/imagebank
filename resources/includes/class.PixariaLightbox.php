<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

// Load the main Smarty class
require_once ("class.PixariaMultiImage.php");

class PixariaLightbox extends PixariaMultiImage {
	
	var $_dbl;
	var $_update_flag		= false;
	
	// Core lightbox properties
	var $lightbox_id;
	var $lightbox_image;
	var $lightbox_image_data;
	var $lightbox_image_count;
	
	function PixariaLightbox ($lightbox_id, $multi_image_page = "", $lightbox_image = "", $submitted_access_key = "") {
		
		global $cfg, $ses;
		
		$this->multi_image_type		= "lightbox";
		$this->multi_image_page		= $multi_image_page;
		$this->multi_image_image	= $lightbox_image;
		$this->submitted_access_key	= $submitted_access_key;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		// If there is a numeric lightbox ID set then we will use that
		if (is_numeric($lightbox_id)) {
			
			$this->lightbox_id = $lightbox_id;
		
		} else {
		
			$this->lightbox_id = $ses['psg_lightbox_id'];
			
		}
		
		// Load the lightbox data access object
		require_once ('class.DAO.PixariaLightbox.php');
		
		// Create the lightbox data access object
		$objPixariaLightbox = new DAO_PixariaLightbox($this->lightbox_id);
		
		$this->lightbox_date			= $objPixariaLightbox->getLightboxDate();
		$this->lightbox_userid			= $objPixariaLightbox->getLightboxUserId();
		$this->lightbox_type			= $objPixariaLightbox->getLightboxType();
		$this->lightbox_status			= $objPixariaLightbox->getLightboxStatus();
		$this->lightbox_name			= $objPixariaLightbox->getLightboxName();
		$this->lightbox_image_count 	= $objPixariaLightbox->getLightboxImageCount();
		$this->multi_image_count 		= $objPixariaLightbox->getLightboxImageCount();
		$this->lightbox_access_key		= strtoupper(substr(md5($cfg['sys']['encryption_key'] . $this->lightbox_id),0,6));
		
		if (is_numeric($this->multi_image_image)) {
			
			// If we're looking at a single image page, load required information
			$this->lightbox_image_data = $this->processLightboxImageDetail();
		
		}
		
	}
		
	/*
	*	Get the lightbox ID
	*	
	*	@return string $this->lightbox_id the ID of the current lightbox
	*/
	function getLightboxId() {
	
		return $this->lightbox_id;
	
	}

	/*
	*	Get the lightbox status
	*/
	function getLightboxStatus() {
	
		return $this->lightbox_status;
	
	}

	/*
	*	Get the lightbox userid
	*/
	function getLightboxUserId() {
	
		return $this->lightbox_userid;
	
	}

	/*
	*	Get the lightbox type
	*/
	function getLightboxType() {
	
		return $this->lightbox_type;
	
	}

	/*
	*	Get the lightbox name
	*/
	function getLightboxName() {
	
		return $this->lightbox_name;
	
	}

	/*
	*	Get detailed information about an image and it's neighbours in this lightbox
	*
	*	@return array $this->lightbox_image_data an array of info about an image and its neighbours in a lightbox
	*/
	function getLightboxImageData() {
	
		return $this->lightbox_image_data;
	
	}

	/*
	*	Get the lightbox access key
	*/
	function getLightboxAccessKey() {
		
		return $this->lightbox_access_key;
	
	}

	/*
	*	Get the lightbox PDF contact sheet URL
	*/
	function getPDFContactSheetURL() {
		
		return $cfg['sys']['base_url'] . FILE_PUB_LIGHTBOX . "?lid=" . $this->lightbox_id . "&amp;key=" . $this->submitted_access_key . "&amp;cmd=downloadContactSheet";
	
	}

	/*
	*	Get the boolean of whether the lightbox is the logged in users or now
	*/
	function getIsMyLightbox() {
		
		global $ses;
		
		if ($ses['psg_userid'] == $this->lightbox_userid) {
			return true;
		} else {
			return false;
		}
		
	}

	/*
	*	Get the gallery URL
	*	
	*	@return string $gallery_url link to the current gallery main page
	*/
	function getGalleryURL() {
	
		global $cfg;
		
		// Set the URL of this page
		if ($cfg['set']['func_mod_rewrite']) {
		
			$gallery_url = $cfg['sys']['base_url'] . "gallery/" . $this->gallery_id;
		
		} else {
		
			$gallery_url = $cfg['sys']['base_url'] . FILE_PUB_GALLERY . "?gid=" . $this->gallery_id;
		
		}

		return $gallery_url;
	
	}

	/*
	*	Get data for the images in this lightbox
	*/
	function getLightboxMultiImageData() {
		
		global $cfg, $ses;
		
		// Get the gallery ID property
		$lightbox_id = $this->lightbox_id;
		
		if ($lightbox_id != "") { // If there is a lightbox ID then load information about sub-galleries for this one
		
			// Build SQL to select all the necessary image information from the database
			$sql = "SELECT *, ".$cfg['sys']['table_imgs'].".image_id AS image_id FROM `".$cfg['sys']['table_imgs']."`
			
							LEFT JOIN `".$cfg['sys']['table_lbme']."` ON ".$cfg['sys']['table_imgs'].".image_id = ".$cfg['sys']['table_lbme'].".image_id
							
							LEFT JOIN `".$cfg['sys']['table_imvi']."` ON ".$cfg['sys']['table_imgs'].".image_id = ".$cfg['sys']['table_imvi'].".image_id
						
							WHERE ".$cfg['sys']['table_lbme'].".lightbox_id = '$lightbox_id'
							
							".$cfg['set']['image_access_sql']."
							
							".$cfg['set']['image_visibility_sql']."
							
							GROUP by ".$cfg['sys']['table_imgs'].".image_id
								
							ORDER by ".$cfg['sys']['table_lbme'].".order ASC " . 
					
							$this->getMultiImagePageLimitSQL();
			
			$lightbox_images = $this->_dbl->sqlSelectRows($sql);
			
			//print_r($lightbox_images);exit;
			
			// Runs the method in the parent class to process a list of the images in this gallery
			return $this->processMultiImageData($lightbox_images);
		
		} else { // If no gallery ID then return false
		
			return false;
		
		}
	
	}
		
	function processNeighbourURLs($start, $number) {
		
		global $cfg;
		
		for ($i=($start + 1); $i <= ($start + $number + 1); $i++) {
	
			$data[]	= $cfg['sys']['base_url'] . FILE_PUB_LIGHTBOX . "?lid=" . $this->lightbox_id . "&amp;key=" . $this->submitted_access_key . "&amp;img=" . $i;
				
		}
		
		return $data;
		
	}
	
	/*
	*	Outputs an array of information about a single image in a lightbox and limited detail of those near it
	*	
	*	@return array $images_data information about an image in a lightbox and the five images nearest to it
	*/
	function processLightboxImageDetail () {
	
		global $cfg, $ses, $smarty;
		
		$results_view		= $this->getBrowsingLimitData ('results_view');
		$results_lim_sta	= $this->getBrowsingLimitData ('results_lim_sta');
		$results_lim_end	= $this->getBrowsingLimitData ('results_lim_end');
		$image_key			= $this->getBrowsingLimitData ('image_key');
		
		//print("Image number: $results_image<br>Total images:$results_total<br>$results_view - <br>SQL limit start:$results_lim_sta<br>SQL limit end:$results_lim_end<br>Current image:$image_key ");
		
		// Build SQL to select all the necessary gallery information from the database
		$results_sql = "SELECT *, ".$cfg['sys']['table_imgs'].".image_id AS image_id FROM `".$cfg['sys']['table_imgs']."`
			
							LEFT JOIN `".$cfg['sys']['table_lbme']."` ON ".$cfg['sys']['table_imgs'].".image_id = ".$cfg['sys']['table_lbme'].".image_id
							
							LEFT JOIN `".$cfg['sys']['table_imvi']."` ON ".$cfg['sys']['table_imgs'].".image_id = ".$cfg['sys']['table_imvi'].".image_id
						
							WHERE ".$cfg['sys']['table_lbme'].".lightbox_id = '$this->lightbox_id'
							
							".$cfg['set']['image_access_sql']."
							
							".$cfg['set']['image_visibility_sql']."
							
							GROUP by ".$cfg['sys']['table_imgs'].".image_id
								
							ORDER by ".$cfg['sys']['table_lbme'].".order ASC
							
							LIMIT $results_lim_sta,$results_lim_end";
		
		// Retrieve gallery information from database
		$image_data 	= $this->_dbl->sqlSelectRows($results_sql);
		
		// Load the PixariaImage class 
		require_once ('class.PixariaImage.php');
		
		// Create the image object
		$objPixariaImage = new PixariaImage($image_data[$image_key]['image_id']);
		
		// Update the view counter for this image
		$objPixariaImage->updateImageViewCounter();
		
		// Add the image_id to the array of data
		$images_data['image_id']			= $image_data[$image_key]['image_id'];
		
		// Add detailed information about this image to the array
		$images_data['image_detail']		= $objPixariaImage->getImageDataComplete();
		
		// Add basic information about the neighbouring images to the array
		$images_data['image_neighbours']	= $this->processMultiImageData($image_data);
		
		$images_data['image_count']			= count($images_data['image_neighbours']);
		
		$images_data['image_urls']			= $this->processNeighbourURLs($results_lim_sta,$results_lim_end);
			
		$images_data['image_numbers']		= $this->processNeighbourImageNumbers($results_lim_sta,$results_lim_end);
			
		if ($this->multi_image_image == 1) {
			$images_data['image_prev_url'] = $cfg['sys']['base_url'] . FILE_PUB_LIGHTBOX . "?lid=" . $this->lightbox_id . "&amp;key=" . $this->submitted_access_key;
		} else {
			$images_data['image_prev_url'] = $cfg['sys']['base_url'] . FILE_PUB_LIGHTBOX . "?lid=" . $this->lightbox_id . "&amp;key=" . $this->submitted_access_key . "&amp;img=" . ($this->multi_image_image - 1);
		}
		
		if ($this->multi_image_image == $this->multi_image_count) {
			$images_data['image_next_url'] = $cfg['sys']['base_url'] . FILE_PUB_LIGHTBOX . "?lid=" . $this->lightbox_id . "&amp;key=" . $this->submitted_access_key;
		} else {
			$images_data['image_next_url'] = $cfg['sys']['base_url'] . FILE_PUB_LIGHTBOX . "?lid=" . $this->lightbox_id . "&amp;key=" . $this->submitted_access_key . "&amp;img=" . ($this->multi_image_image + 1);
		}
				
		// Return the image data as an array
		return $images_data;
		
	}
	
	function removeImageFromLightbox ($image_id) {
		
		global $cfg;
		
		if (isset($image_id)) {
		
			// Delete any existing items with this image_id, userid and lightbox_id to avoid duplicate entries in one lightbox
			@mysql_query("DELETE FROM " . PIX_TABLE_LBME . " WHERE lightbox_id = '$this->lightbox_id' AND image_id = '$image_id';");
						
			return true;
			
		} else {
		
			return false;
		
		}
	
	}
	
	function addImageToLightbox ($image_id) {
		
		global $cfg;
		
		if (isset($image_id)) {
		
			// Delete any existing items with this image_id, userid and lightbox_id to avoid duplicate entries in one lightbox
			@mysql_query("DELETE FROM " . PIX_TABLE_LBME . " WHERE lightbox_id = '$this->lightbox_id' AND image_id = '$image_id';");
						
			// Add a new entry for this image_id ($image_id)
			@mysql_query("INSERT INTO " . PIX_TABLE_LBME . " VALUES ('0','$this->lightbox_id','$this->lightbox_userid','$image_id','');");
				
			return true;
			
		} else {
		
			return false;
		
		}
	
	}
	
	/*
	*	
	*	Set the lightbox order
	*	
	*/
	function setLightboxOrder ($lightbox_order) {
		
		if ($lightbox_order != "") {
		
			$lightbox_order = explode("|",$lightbox_order);
			
			if (is_array($lightbox_order)) {
			
				$this->lightbox_order 	= $lightbox_order;
				$this->_update_flag  	= (bool)true;
				
			}
			
		}
		
	}
	
	/*
	*	
	*	Set the lightbox name
	*	
	*/
	function setLightboxName ($lightbox_name) {
		
		if ($lightbox_name != "") {
			$this->lightbox_name 	= addslashes($lightbox_name);
			$this->_update_flag  	= (bool)true;
		}
		
	}
	
	/*
	*	
	*	Set the lightbox default value
	*	
	*/
	function setLightboxDefault ($lightbox_default) {
		
		if ($lightbox_default == "on") {
			$this->lightbox_default = (bool)true;
			$this->_update_flag  	= (bool)true;
		}
		
	}
	
	/*
	*	
	*	Make the lightbox with ID $lightbox_id the active one for the current user
	*	
	*/
	function makeLightboxActive ($lightbox_id) {
	
		global $cfg, $ses;
		
		$userid = $ses['psg_userid'];
			
		if (isset($lightbox_id)) {
			
			$this->_dbl->sqlQuery("UPDATE ".$cfg['sys']['table_user']." SET active_image_box = '$lightbox_id' WHERE userid = '$userid'");
		
		}
	
	}
	
	/*
	*	
	*	Delete the lightbox with ID $lightbox_id
	*	
	*/
	function deleteLightbox ($lightbox_id) {
	
		global $cfg, $ses;
		
		$userid = $ses['psg_userid'];
		
		if (isset($lightbox_id) && $lightbox_id != $ses['psg_lightbox_id']) {
			
			// Delete this entry from the main lightbox table
			$this->_dbl->sqlQuery("DELETE FROM ".$cfg['sys']['table_lbox']." WHERE id = '$lightbox_id' AND userid = '$userid' AND status = '10' AND type = '10'");
			
			// Delete the items from the lightbox members table
			$this->_dbl->sqlQuery("DELETE FROM ".$cfg['sys']['table_lbme']." WHERE lightbox_id = '$lightbox_id' AND userid = '$userid'");
			
		}
	
	}
	
	/*
	*	
	*	Creates a new lightbox
	*	
	*/
	function createLightbox () {
	
		global $cfg, $ses;
		
		$userid = $ses['psg_userid'];
			
		if ($this->_update_flag && $this->lightbox_name != "" && is_numeric($userid)) {
			
			$sql = "INSERT INTO ".$cfg['sys']['table_lbox']." VALUES ('0','$this->lightbox_name',NOW(),'$userid','10','10');";
			
				$this->_dbl->sqlQuery($sql);
			
			if ($this->lightbox_default) {
			
				$lightbox_id = $this->_dbl->sqlInsertId();
				
				$sql = "UPDATE ".$cfg['sys']['table_user']." SET active_image_box = '$lightbox_id' WHERE userid = '$userid'";
				
				$this->_dbl->sqlQuery($sql);
				
			}
		
		}
	
	}
	
	/*
	*	
	*	Saves changes to the lightbox
	*	
	*/
	function updateLightbox () {
		
		global $cfg;
		
		if ($this->_update_flag) {
		
			// If the user's submitting lightbox order information
			if (is_array($this->lightbox_order)) {
							
				// Delete any existing items with this image_id, userid and lightbox_id to avoid duplicate entries in one lightbox
				$this->_dbl->sqlQuery("DELETE FROM ".$cfg['sys']['table_lbme']." WHERE lightbox_id = '$this->lightbox_id' AND userid = '$this->lightbox_userid';");
				
				
				// Write the new order of images into the gallery_order table
				foreach ($this->lightbox_order as $key => $value) {
					
					// The script will pass an array of div id's some of which are not needed
					// We will only need ones that begin with the prefix 'image_'.
					if (eregi("image_",$value)) {
					
						$image_id = eregi_replace("image_","",$value);
					
						// Add a new entry for this image_id ($value)
						$this->_dbl->sqlQuery("INSERT INTO ".$cfg['sys']['table_lbme']." VALUES ('0','$this->lightbox_id','$this->lightbox_userid','$image_id','$key');");
					
					}
				}
		
			}
			
			if ($this->lightbox_default) {
				
				// Set this lightbox to be active if requested
				$this->_dbl->sqlQuery("UPDATE ".$cfg['sys']['table_user']." SET active_image_box = '$this->lightbox_id' WHERE userid = '$this->lightbox_userid'");
				
			}
		
			if ($this->lightbox_name != "") {
				
				// Set this lightbox to be active if requested
				$this->_dbl->sqlQuery("UPDATE ".$cfg['sys']['table_lbox']." SET name = '$this->lightbox_name' WHERE userid = '$this->lightbox_userid' AND id = '$this->lightbox_id'");
				
			}
		
		} else {
		
			return;
		
		}
	
	}
	
}


?>