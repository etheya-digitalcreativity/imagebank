<?php if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

class Database {

	var $dbl;
	
	function Database() {
	
		global $cfg;
	
		$this->dbl = mysql_connect($cfg['sys']['db_host'],$cfg['sys']['db_uname'],$cfg['sys']['db_pword']) or die(mysql_error());
		
		$db = mysql_select_db($cfg['sys']['db_name'], $this->dbl) or die(mysql_error());
		
		return $db;
		
	}
	
	/*
	*
	*	RETURN THE RESULT OF A QUERY ON A SINGLE DATABASE ROW AS AN ARRAY
	*
	*/
	function row ($sql) {
		
		$result = @mysql_query($sql);
	
		if (($numrow = @mysql_num_rows($result)) < 1) { return(FALSE); }
	
		$array = @mysql_fetch_array($result);
		
		return($array);
	
	}
	
	/*
	*
	*	RETURN THE RESULT OF A QUERY ON MULTIPLE DATABASE ROWS AS AN ARRAY
	*
	*/
	function rows ($sql, $type=MYSQL_BOTH) {
				
		$result = @mysql_query($sql);
	
		if(($numrow = @mysql_num_rows($result)) < 1){return(FALSE);}
	
		$i=0;while($i < $numrow){
	
			$array[$i] = @mysql_fetch_array($result,$type);
	
			$i++;
		}
	
		@mysql_free_result($result);
		
		return $array;
	
	}

	/*
	*
	*	RETURN MULTIPLE ROWS AS COLUMNS OF DATA
	*
	*/
	function rowsAsColumns ($sql) {
	
		$result = $this->sqlSelectRows($sql, MYSQL_NUM);
		
		if (is_array($result)) {
			
			foreach ($result as $key => $value) {
			
				$items = count($value);
				
				for ($i = 0; $i < $items; $i++) {
					
					$data_{$i}[] = $value[$i];
				
				}
				
			}
			
			for ($i = 0; $i < $items; $i++) {
					
				$array[] = $data_{$i};
				
			}
			
			return $array;
	
		} else {
		
			return false;
		
		}

	}
	
	/*
	*
	*	COUNT ROWS FROM MYSQL COUNT() RESULTS SET
	*
	*/
	function count ($sql) {
	
		$result = @mysql_query($sql);
		
		$numrow = @mysql_num_rows($result);
		
		if ($numrow < 1 || $numrow == "") { return(0); }
	
		$array = @mysql_fetch_array($result);
		
		return($array[0]);
	
	}
	
	/*
	*
	*	COUNT ROWS FROM STANDARD MYSQL RESULTS SET
	*
	*/
	function countRows ($sql) {
	
		$result = @mysql_query($sql);
	
		$numrow = @mysql_num_rows($result);
		
		if ($numrow < 1 || $numrow == "") { return('0'); } else { return $numrow; } 
	}
	
	/*
	*
	*	ESCAPE MYSQL DATA BASED ON MAGIC QUOTES SETTINGS
	*
	*/
	function escape ($string) {
		
	    $string = str_replace ("\0",'',$string);
	    
	    if (get_magic_quotes_gpc()) $string = stripslashes($string);
	    
	    return mysql_real_escape_string($string);
		
	}
	
	/*
	*
	*	EXECUTE A MYSQL COMMAND
	*
	*/
	function query ($sql) {
		
		return @mysql_query($sql);
	
	}
	
	/*
	*
	*	GET THE ID OF THE LAST AUTO INCREMENT INSERT
	*
	*/
	function insertId () {
	
		return @mysql_insert_id();
		
	}
	
	/*
	*	
	*	sqlCloseConnection -- Close connection to database
	*
	*	mixed sqlCloseConnection(void)
	*
	*/
	
	function sqlSelectRow($sql) {
		return $this->row($sql);
	}
	
	function sqlSelectRows($sql) {
		return $this->rows($sql);
	}
	
	function sqlParseRows($sql) {
		return $this->rowsAsColumns($sql);
	}
	
	function sqlCloseConnection() {
		return @mysql_close($this->dbl);
	}
	
	function sqlQuery($sql) {
		return $this->query($sql);
	}
	
	function sqlInsertId() {
		return $this->insertId();
	}
	
	function sqlCountRows($sql) {
		return $this->count($sql);
	}

	function sqlCountSelectRows ($sql) {
		return $this->countRows($sql);
	}
	
	function mysqlEscape($string) {
	    return $this->escape($string);
	}
	
}

?>