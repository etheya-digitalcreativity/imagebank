<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

class PixariaMultiImage {
	
	var $multi_image_page;
	var $multi_image_type;
	var $multi_image_count;
	var $multi_image_sid;
	
	// If this is set to true, then do not limit images
	var $multi_image_show_all = false;
	
	/*
	*
	*	Get SQL to limit the number of image thumbs to the current page
	*
	*/
	function getMultiImagePageLimitSQL () {
		
		global $cfg;
		
		$this->_cfg =& $cfg;
		
		// If $this->multi_image_show_all is true then don't limit images
		if ($this->multi_image_show_all) {
		
			return;
			
		// This statement ensure we only load image data for the page we're on
		} elseif (!$this->multi_image_page) {
			
			$results_lim_sta 	= 0;
			$results_lim_end	= $cfg['set']['thumb_per_page'];
			
		} else {
			
			$results_lim_sta 	= ($this->multi_image_page - 1) * $cfg['set']['thumb_per_page'];
			$results_lim_end	= $cfg['set']['thumb_per_page'];
			
		}
		
		return " LIMIT $results_lim_sta, $results_lim_end ";
	
	}
	
	/*
	*
	*	Get the size of an image from the image's full file path
	*
	*/
	function getImageDimensions($width, $height) {
		
		if (is_numeric($width) && is_numeric($height) && $width >= 1 && $height >= 1) {
		
			$result[0]	= $width;									// Width in pixels
			$result[1]	= $height;									// Height in pixels
			$result[2]	= "width=\"$width\" height=\"$height\"";	// HTML width and height attributes
			$result[3]	= $width / $height;							// The aspect ratio of the image
		
			return $result;
		
		} else {
		
			return false;
		
		}
		
	}

	/*
	*	
	*	Generates links and titles for multiple image nested navigation
	*
	*/
	function getMultiImageNavigation() {
		
		global $cfg, $smarty;
		
		switch ($this->multi_image_type) {
		
			case "gallery":
			
				$this->getGalleryHierarchy($this->gallery_id);
				
				$gallery_titles = $this->gallery_hierarchy['title'];
				$gallery_ids	= $this->gallery_hierarchy['id'];
				
				// Check to see we have arrays to work with
				if (is_array($gallery_titles) && is_array($gallery_ids) ) {
				
					$gallery_titles	= array_reverse($gallery_titles);
					$gallery_ids	= array_reverse($gallery_ids);
					
					foreach ($gallery_ids as $key => $value) {
					
						if ($cfg['set']['func_mod_rewrite']) { // We need to generate standard URLs
						
							$gallery_href[] = $cfg['sys']['base_url'] . "gallery/" . $value . "/";
						
						} else { // We need to generate Mod_Rewrite comaptible URLs
						
							$gallery_href[] = $cfg['sys']['base_url'] . $cfg['fil']['index_gallery'] . "?gid=$value";
						
						}
						
					}
				
				}
				
				return array($gallery_titles, $gallery_href);
						
			break;
		
			case "search":
				
				$titles = array("Search");
				$links	= array(SYS_BASE_PATH.$cfg['fil']['index_search']);
				
				return array($titles, $links);
						
			break;
		
			case "admin_search": case "admin_search_popup":
				
				$titles = array("Search");
				$links	= array(SYS_BASE_PATH.FILE_ADM_LIBRARY_SEARCH);
				
				return array($titles, $links);
						
			break;
		
		}
		
	}

	/*
	*	
	*	Generates links to navigate between pages of a thumbnail view
	*
	*/
	function getMultiImagePageNavigation() {
		
		global $cfg, $smarty, $objEnvData;
		
		switch ($this->multi_image_type) {
		
			case "gallery":
				
				$current_page	= $this->multi_image_page;
				
				if ($current_page == ""): $current_page = 1; endif;
				
				$images 		= $this->gallery_image_count;
				$items			= $cfg['set']['thumb_per_page'];
				$pages			= ceil($images / $items);
				$this_page		= $this->multi_image_page;
				
				/*
				*	Set the short title as the gallery path if it's present
				*/
				if ($this->gallery_short_title != "") {
					$gallery_path = $this->gallery_short_title;
				} else {
					$gallery_path = $this->gallery_id;
				}
				
				// Get the page number of the next and previous pages
				if ($current_page == 1) { $previous_page = 1; } else { $previous_page = $current_page - 1; }
				if ($current_page == $pages) { $next_page = $pages; } else { $next_page = $current_page + 1; }
				
				/*
				*	Set the 'base' page text and link
				*/
				if ($cfg['set']['func_mod_rewrite']) {
					$base_page = $cfg['sys']['base_url']."gallery/".$gallery_path;
				} else {
					$base_page = $cfg['sys']['base_url'].$cfg['fil']['index_gallery']."?gid=".$this->gallery_id;
				}
				
				/*
				*	Set the 'previous' page text and link
				*/
				$page[] = "&laquo; ".$GLOBALS['_STR_']['IM_THUMBS_34'];
				if ($cfg['set']['func_mod_rewrite']) {
					$href[] = $cfg['sys']['base_url']."gallery/".$gallery_path."/pages/".$previous_page . "/";
				} else {
					$href[] = $cfg['sys']['base_url'].$cfg['fil']['index_gallery']."?gid=".$this->gallery_id."&amp;ipg=".$previous_page;
				}
				
				/*
				*	Loop through the pages and generate text and links for each page
				*/
				for ($i = 1; $i <= $pages; $i++) {
				
					$page[] = $i;
					
					if ($cfg['set']['func_mod_rewrite']) {
						
						$href[] = $cfg['sys']['base_url']."gallery/".$gallery_path."/pages/".$i . "/";
					
					} else {
					
						$href[] = $cfg['sys']['base_url'].$cfg['fil']['index_gallery']."?gid=".$this->gallery_id."&amp;ipg=".$i;
					
					}
					
				}
				
				/*
				*	Set the 'next' page text and link
				*/
				$page[] = $GLOBALS['_STR_']['IM_THUMBS_33']." &raquo;";
				if ($cfg['set']['func_mod_rewrite']) {
					$href[] = $cfg['sys']['base_url']."gallery/".$gallery_path."/pages/".$next_page . "/";
				} else {
					$href[] = $cfg['sys']['base_url'].$cfg['fil']['index_gallery']."?gid=".$this->gallery_id."&amp;ipg=".$next_page;
				}
				
				return paginationTidy($current_page, $page, $href, $pages, $base_page);
				
			break;
			
			case "lightbox":
				
				$current_page	= $this->multi_image_page;
				
				if ($current_page == ""): $current_page = 1; endif;
				
				$images 		= $this->lightbox_image_count;
				$items			= $cfg['set']['thumb_per_page'];
				$pages			= ceil($images / $items);
				$this_page		= $this->multi_image_page;
				
				// Get the page number of the next and previous pages
				if ($current_page == 1) { $previous_page = 1; } else { $previous_page = $current_page - 1; }
				if ($current_page == $pages) { $next_page = $pages; } else { $next_page = $current_page + 1; }
				
				/*
				*	Set the 'base' page text and link
				*/
				$base_page = $cfg['sys']['base_url'].$cfg['fil']['index_lightbox']."?lid=".$this->lightbox_id;
				
				/*
				*	Set the 'previous' page text and link
				*/
				$page[] = "&laquo; ".$GLOBALS['_STR_']['IM_THUMBS_34'];
				$href[] = $cfg['sys']['base_url'].$cfg['fil']['index_lightbox']."?lid=$this->lightbox_id" . "&amp;key=" . $this->submitted_access_key . "&amp;ipg=".$previous_page;
				
				for ($i = 1; $i <= $pages; $i++) {
				
					$page[] = $i;
					$href[] = $cfg['sys']['base_url'].$cfg['fil']['index_lightbox']."?lid=$this->lightbox_id" . "&amp;key=" . $this->submitted_access_key . "&amp;ipg=".$i;
					
				}
				
				/*
				*	Set the 'next' page text and link
				*/
				$page[] = $GLOBALS['_STR_']['IM_THUMBS_33']." &raquo;";
				$href[] = $cfg['sys']['base_url'].$cfg['fil']['index_lightbox']."?lid=$this->lightbox_id" . "&amp;key=" . $this->submitted_access_key . "&amp;ipg=".$next_page;
				
				return paginationTidy($current_page, $page, $href, $pages, $base_page);
				
			break;
			
			case "search":
				
				$current_page	= $this->multi_image_page;
				
				if ($current_page == ""): $current_page = 1; endif;
				
				$images 		= $this->multi_image_count;
				$items			= $cfg['set']['thumb_per_page'];
				$pages			= ceil($images / $items);
				$this_page		= $this->multi_image_page;
				$sid			= $this->multi_image_sid;
				
				// Get the page number of the next and previous pages
				if ($current_page == 1) { $previous_page = 1; } else { $previous_page = $current_page - 1; }
				if ($current_page == $pages) { $next_page = $pages; } else { $next_page = $current_page + 1; }
				
				/*
				*	Set the 'base' page text and link
				*/
				$base_page = $cfg['sys']['base_url'].$cfg['fil']['index_search']."?sid=".$sid;
				
				/*
				*	Set the 'previous' page text and link
				*/
				$page[] = "&laquo; ".$GLOBALS['_STR_']['IM_THUMBS_34'];
				$href[] = $cfg['sys']['base_url'].$cfg['fil']['index_search']."?sid=$sid&amp;ipg=".$previous_page;
				
				for ($i = 1; $i <= $pages; $i++) {
				
					$page[] = $i;
					$href[] = $cfg['sys']['base_url'].$cfg['fil']['index_search']."?sid=$sid&amp;ipg=".$i;
					
				}
				
				/*
				*	Set the 'next' page text and link
				*/
				$page[] = $GLOBALS['_STR_']['IM_THUMBS_33']." &raquo;";
				$href[] = $cfg['sys']['base_url'].$cfg['fil']['index_search']."?sid=$sid&amp;ipg=".$next_page;
				
				return paginationTidy($current_page, $page, $href, $pages, $base_page);
				
			break;
			
			case "admin_search":
				
				$current_page	= $this->multi_image_page;
				
				if ($current_page == ""): $current_page = 1; endif;
				
				$images 		= $this->multi_image_count;
				$items			= $cfg['set']['thumb_per_page'];
				$pages			= ceil($images / $items);
				$this_page		= $this->multi_image_page;
				$sid			= $this->multi_image_sid;
				
				// Get the page number of the next and previous pages
				if ($current_page == 1) { $previous_page = 1; } else { $previous_page = $current_page - 1; }
				if ($current_page == $pages) { $next_page = $pages; } else { $next_page = $current_page + 1; }
				
				/*
				*	Set the 'previous' page text and link
				*/
				$page[] = "&laquo; Previous";
				$href[] = $cfg['sys']['base_url'].FILE_ADM_LIBRARY_SEARCH."?sid=$sid&amp;ipg=".$previous_page."&amp;cmd=doSearch";
				
				for ($i = 1; $i <= $pages; $i++) {
				
					$page[] = $i;
					$href[] = $cfg['sys']['base_url'].FILE_ADM_LIBRARY_SEARCH."?sid=$sid&amp;ipg=".$i."&amp;cmd=doSearch";
					
				}
				
				/*
				*	Set the 'next' page text and link
				*/
				$page[] = "Next &raquo;";
				$href[] = $cfg['sys']['base_url'].FILE_ADM_LIBRARY_SEARCH."?sid=$sid&amp;ipg=".$next_page."&amp;cmd=doSearch";
				
				return paginationTidy($current_page, $page, $href, $pages, "");
				
			break;
			
			case "admin_search_popup":
				
				$current_page	= $this->multi_image_page;
				
				if ($current_page == ""): $current_page = 1; endif;
				
				$images 		= $this->multi_image_count;
				$items			= $cfg['set']['thumb_per_page'];
				$pages			= ceil($images / $items);
				$this_page		= $this->multi_image_page;
				$sid			= $this->multi_image_sid;
				
				// Get the page number of the next and previous pages
				if ($current_page == 1) { $previous_page = 1; } else { $previous_page = $current_page - 1; }
				if ($current_page == $pages) { $next_page = $pages; } else { $next_page = $current_page + 1; }
				
				/*
				*	Set the 'previous' page text and link
				*/
				$page[] = "&laquo; Previous";
				$href[] = $cfg['sys']['base_url'].FILE_ADM_LIBRARY_SEARCH."?sid=$sid&amp;ipg=".$previous_page."&amp;mode=popup&amp;cmd=doSearch&amp;name=" . $objEnvData->fetchGlobal('name');
				
				for ($i = 1; $i <= $pages; $i++) {
				
					$page[] = $i;
					$href[] = $cfg['sys']['base_url'].FILE_ADM_LIBRARY_SEARCH."?sid=$sid&amp;ipg=".$i."&amp;mode=popup&amp;cmd=doSearch&amp;name=" . $objEnvData->fetchGlobal('name');
					
				}
				
				/*
				*	Set the 'next' page text and link
				*/
				$page[] = "Next &raquo;";
				$href[] = $cfg['sys']['base_url'].FILE_ADM_LIBRARY_SEARCH."?sid=$sid&amp;ipg=".$next_page."&amp;mode=popup&amp;cmd=doSearch&amp;name=" . $objEnvData->fetchGlobal('name');
				
				return paginationTidy($current_page, $page, $href, $pages, "");
				
			break;
			
		}
		
	}

	/*
	*
	*	Turn a combined array of image data into information needed
	*	for showing a series of thumbnail images in the gallery, lightbox and search pages
	*	
	*	This class takes an array of raw image data from the images table of Pixaria
	*
	*/
	function processMultiImageData ($data) {
		
		global $cfg, $ses;
		
		$sid = $this->multi_image_sid;
		
		if (is_array($data) && $data[0][0] != "") {
			
			foreach ($data as $key => $value) {
				
				if ($this->multi_image_page < 2) {
				
					$index = $key + 1;
				
				} else {
				
					$index = (($this->multi_image_page - 1) * $cfg['set']['thumb_per_page']) + $key + 1;
				
				}
				
				$image_base_path = $cfg['sys']['base_library'].$value['image_path']."/";
				
				$image_icon_path 		= $image_base_path . "32x32/" . $value['image_filename'];
				$image_small_path 		= $image_base_path . "80x80/" . $value['image_filename'];
				$image_large_path 		= $image_base_path . "160x160/" . $value['image_filename'];
				$image_comp_path 		= $image_base_path . COMPING_DIR."/" . $value['image_filename'];
				$image_original_path 	= $image_base_path . "original/" . $value['image_filename'];
				
				$image_icon_dims 		= $this->getImageDimensions($value['icon_w'],$value['icon_h']);
				$image_small_dims 		= $this->getImageDimensions($value['smal_w'],$value['smal_h']);
				$image_large_dims 		= $this->getImageDimensions($value['larg_w'],$value['larg_h']);
				$image_comp_dims 		= $this->getImageDimensions($value['comp_w'],$value['comp_h']);
				$image_original_dims 	= $this->getImageDimensions($value['orig_w'],$value['orig_h']);
				
				$image_icon_encoded 		= base64_encode($image_icon_path);
				$image_small_encoded		= base64_encode($image_small_path);
				$image_large_encoded		= base64_encode($image_large_path);
				$image_comp_encoded			= base64_encode($image_comp_path);
				$image_original_encoded		= base64_encode($image_original_path);
				
				$image_icon_filesize 		= @filesize($image_icon_path);
				$image_small_filesize		= @filesize($image_small_path);
				$image_large_filesize		= @filesize($image_large_path);
				$image_comp_filesize		= @filesize($image_comp_path);
				$image_original_filesize	= @filesize($image_original_path);
				
				$image_icon_url				= $cfg['sys']['base_url'] . $cfg['fil']['psg_image_thumbnail']."?file=".$image_icon_encoded;
				$image_small_url			= $cfg['sys']['base_url'] . $cfg['fil']['psg_image_thumbnail']."?file=".$image_small_encoded;
				$image_large_url			= $cfg['sys']['base_url'] . $cfg['fil']['psg_image_thumbnail']."?file=".$image_large_encoded;
				$image_comp_url				= $cfg['sys']['base_url'] . $cfg['fil']['psg_image_comping']."?file=".$image_comp_encoded;
				$image_original_url			= $cfg['sys']['base_url'] . $cfg['fil']['psg_image_comping']."?file=".$image_original_encoded;
				
				$admin_edit_url 			= $cfg['sys']['base_url'] . $cfg['fil']['admin_image'] . "?cmd=edit&amp;image_id=".$value['image_id'];
				$admin_preview_url 			= "javascript:imagePreviewWindow('". $cfg['sys']['base_url'] . $cfg['fil']['admin_image_output'] . "?html=".$image_comp_encoded."&amp;','".$image_comp_dims[0]."}','".$image_comp_dims[1]."');";
				
				switch ($this->multi_image_type) {
				
					case "gallery":
					
						if ($cfg['set']['func_mod_rewrite']) {
							
							if ($this->gallery_short_title != "") {
								$gallery_path = $this->gallery_short_title;
							} else {
								$gallery_path = $this->gallery_id;
							}
							
							$inline_url				= $cfg['sys']['base_url'] . "gallery/$gallery_path/image/$index/";
							$image_url				= $cfg['sys']['base_url'] . "image/" . $value['image_id'] . "/";
							
						} else {
						
							$inline_url				= $cfg['sys']['base_url'] . FILE_PUB_GALLERY . "?gid=" . $this->gallery_id . "&amp;img=" . $index;
							$image_url				= $cfg['sys']['base_url'] . FILE_PUB_IMAGE . "?image_id=" . $value['image_id'];
						
						}
						
					break;
					
					case "search":
					
						$inline_url				= $cfg['sys']['base_url'] . FILE_PUB_SEARCH . "?sid=$sid&amp;img=" . $index;
						$image_url				= $cfg['sys']['base_url'] . FILE_PUB_IMAGE . "?sid=$sid&amp;image_id=" . $value['image_id'];
						
					break;
				
					case "lightbox":
					
						$inline_url				= $cfg['sys']['base_url'] . FILE_PUB_LIGHTBOX . "?lid=" . $this->lightbox_id . "&amp;key=" . $this->submitted_access_key . "&amp;img=" . $index;
						$image_url				= $cfg['sys']['base_url'] . FILE_PUB_IMAGE . "?image_id=" . $value['image_id'];
						
					break;
				
				}
				
				
				// Set the URL to open this image in a popup window
				$popup_url = "javascript:openPop('".$cfg['sys']['base_url']. FILE_PUB_IMAGE ."?cmd=pop&amp;image_id=".$value['image_id']."','" . $cfg['set']['image_popup_width'] . "','" . $cfg['set']['image_popup_height'] . "','yes','yes','no','no','yes','no','no');";
				
				
				// Set the three types of URL for a thumbnail
				$images[$key]['url_inline']		= $inline_url;
				$images[$key]['url_popup']		= $popup_url;
				$images[$key]['url_image']		= $image_url;
				
				
				// Choose how the default click on thumbnail action should be handled
				if ($cfg['set']['image_href'] == "inline") {
				
					$images[$key]['url_default'] = $inline_url;
				
				} else {
				
					$images[$key]['url_default'] = $popup_url;
				
				}
				
				$this->multi_image_image;
				
				$images[$key]['basic']['index']				= $index;
				$images[$key]['basic']['id']				= $value['image_id'];
				$images[$key]['basic']['path']				= $value['image_path'];
				$images[$key]['basic']['userid']			= $value['image_userid'];
				$images[$key]['basic']['file_name']			= $this->getImageFileNameClean ($value['image_filename']);
				$images[$key]['basic']['file_name_full']	= $value['image_filename'];
				$images[$key]['basic']['title']				= stripslashes($value['image_title']);
				$images[$key]['basic']['caption']			= stripslashes($value['image_caption']);
				$images[$key]['basic']['date']				= $value['image_date'];
				$images[$key]['basic']['width']				= $value['image_width'];
				$images[$key]['basic']['height']			= $value['image_height'];
				$images[$key]['basic']['aspect_ratio']		= $image_comp_dims[3];
				$images[$key]['basic']['copyright']			= stripslashes($value['image_copyright']);
				$images[$key]['basic']['cart_link']			= $this->getImageCartLink($value['image_price'], $value['image_sale']);
				$images[$key]['basic']['cart_add']			= $this->getImageCartURL($value['image_id']);
				$images[$key]['basic']['lightbox_add']		= $this->getImageLightboxURL($value['image_id']);
				$images[$key]['basic']['lightbox_remove']	= $this->getImageLightboxRemoveURL($value['image_id']);
				
				$images[$key]['files']['icon_size']			= $image_icon_dims;
				$images[$key]['files']['icon_filesize']		= $image_icon_filesize;
				$images[$key]['files']['icon_path']			= $image_icon_encoded;
				$images[$key]['files']['icon_url']			= $image_icon_url;
				$images[$key]['files']['small_size']		= $image_small_dims;
				$images[$key]['files']['small_filesize']	= $image_small_filesize;
				$images[$key]['files']['small_path']		= $image_small_encoded;
				$images[$key]['files']['small_url']			= $image_small_url;
				$images[$key]['files']['large_size']		= $image_large_dims;
				$images[$key]['files']['large_filesize']	= $image_large_filesize;
				$images[$key]['files']['large_path']		= $image_large_encoded;
				$images[$key]['files']['large_url']			= $image_large_url;
				$images[$key]['files']['comp_size']			= $image_comp_dims;
				$images[$key]['files']['comp_filesize']		= $image_comp_filesize;
				$images[$key]['files']['comp_path']			= $image_comp_encoded;
				$images[$key]['files']['comp_url']			= $image_comp_url;
				$images[$key]['files']['original_size']		= $image_original_dims;
				$images[$key]['files']['original_filesize']	= $image_original_filesize;
				$images[$key]['files']['original_path']		= $image_large_encoded;
				$images[$key]['files']['original_url']		= $image_original_url;
				$images[$key]['files']['admin_preview_url']	= $admin_preview_url;
				$images[$key]['files']['admin_edit_url']	= $admin_edit_url;
				$images[$key]['files']['download_comp'] 	= $this->getImageDownloadComp($image_comp_encoded);
			
			}
			
			return $images;
			
		} else {
		
			return false;
		
		}
	
	}

	/*
	*
	*
	*
	*/
	function getImageFileNameClean ($filename) {
		
		if (!$this->_cfg['set']['filename_noext'] && preg_match("/\./",$filename)) {
			
			$filename = explode(".", $filename);
			
			array_pop($filename);
			
			return implode(".",$filename);
			
		} else {
		
			return $filename;
			
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function processNeighbourImageNumbers($start, $number) {
		
		global $cfg;
		
		for ($i=($start + 1); $i <= ($start + $number + 1); $i++) {
				
			$data[]	= $i;
				
		}
		
		return $data;
		
	}
	
	/*
	*	Return the download URL of the comping image
	*	
	*	This function is duplicated in class.PixariaImage.php
	*/
	function getImageDownloadComp ($image_comp_encoded) {

		global $cfg;
		return $cfg['sys']['base_url'] . $cfg['fil']['psg_image_comping']."?file=". $image_comp_encoded . "&amp;dl=1";
	
	}

	/*
	*	Return boolean for cart link display
	*	
	*	This function is duplicated in class.PixariaImage.php
	*/
	function getImageCartLink ($image_price, $image_sale) {

		global $cfg;

		if ($cfg['set']['func_store'] == 0 || $cfg['set']['store_images_forsale'] == 10) {
		
			return FALSE;
		
		} elseif ($cfg['set']['store_images_forsale'] == 11 && $image_price > 0) {
		
			return TRUE;
		
		} elseif ($cfg['set']['store_images_forsale'] == 12 && $image_sale == 1) {
			
			return TRUE;
		
		} elseif ($cfg['set']['store_images_forsale'] == 13) {
		
			return TRUE;
		
		} else {
		
			return FALSE;
		
		}
	
	}

	/*
	*	Return the URL of the add to cart link
	*	
	*	This function is duplicated in class.PixariaImage.php
	*/
	function getImageCartURL ($image_id) {

		global $cfg;
		
		// URL for popup window
		$url_popup	= "javascript:openPop('".$cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=purchaseOptionsPopup&amp;image_id=$image_id','" . $cfg['set']['cart_popup_width'] . "','" . $cfg['set']['cart_popup_height'] . "','yes','yes','no','no','yes','no','no');";
		
		// URL for current window
		$url_inline	= $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=purchaseOptionsInline&amp;image_id=$image_id";

		// Set the default value
		if ($cfg['set']['cart_href'] == "popup") {
		
			$url_default = $url_popup;
		
		} else {
		
			$url_default = $url_inline;
		
		}

		$cart_add['url_default']	= $url_default;
		$cart_add['url_popup']		= $url_popup;
		$cart_add['url_inline']		= $url_inline;
	
		return $cart_add;
	
	}

	/*
	*	Return the URL of the add to lightbox link
	*	
	*	This function is duplicated in class.PixariaImage.php
	*/
	function getImageLightboxURL ($image_id) {

		global $cfg, $ses;
		
		// URL for popup window
		$url_popup	= "javascript:openPop('" . $cfg['sys']['base_url'] . $cfg['fil']['index_lightbox'] . "?cmd=addImageToLightbox&amp;image_id=$image_id','" . $cfg['set']['lbox_popup_width'] . "','" . $cfg['set']['lbox_popup_height'] . "','yes','yes','no','no','yes','no','no');";
		
		// URL for current window
		$url_inline	= $cfg['sys']['base_url'] . $cfg['fil']['index_lightbox'] . "?cmd=addImageToLightbox&amp;image_id=$image_id";
		
		// Set the default value
		if ($cfg['set']['lightbox_href'] == "popup") {
		
			$url_default = $url_popup;
		
		} else {
		
			$url_default = $url_inline;
		
		}

		$lightbox_add['url_default']	= $url_default;
		$lightbox_add['url_popup']		= $url_popup;
		$lightbox_add['url_inline']		= $url_inline;
	
		return $lightbox_add;
	
	}
	
	/*
	*
	*/
	function getImageLightboxRemoveURL ($image_id) {
	
		global $cfg, $ses;
		
		return $cfg['sys']['base_url'] . $cfg['fil']['index_lightbox'] . "?cmd=removeImageFromLightbox&amp;image_id=$image_id";
		
	}
	
	/*
	*	Returns information needed for the following classes in lightbox, search and gallery views
	*	
	*	processGalleryImageDetail, processLightboxImageDetail, processSearchImageDetail
	*	
	*/
	function getBrowsingLimitData ($switch="") {
		
		global $cfg;
		
		$number 		= $cfg['set']['browse_thumb_count'];
		
		$numplusone		= $number + 1;
		$numminusone	= $number - 1;
		$mid			= ceil($number / 2);
		$midminusone	= $mid - 1;
		
		$results_image = $this->multi_image_image;
		$results_total = $this->multi_image_count;

		if ($results_total < $number) { // There are fewer images than the number of thumbnails
		
			$results_view		= $results_image;
			$results_lim_sta	= 0;
			$results_lim_end	= $number;
			$image_key			= $results_view - 1;
		
		} elseif ($results_image < $mid) { // The image we're viewing is before position five in the list
			
			$results_view		= $results_image;
			$results_lim_sta	= 0;
			$results_lim_end	= $number;
			$image_key			= $results_view - 1;
			
		} elseif ($results_image > ($results_total - $mid)) { // The image is one of the last three at the end of the list
		
			$results_view		= $numminusone - ($results_total - $results_image);
			$results_lim_sta	= $results_total - $number;
			$results_lim_end	= $numplusone;
			$image_key			= $results_view;
		
		} else { // The image is somewhere in the middle of the list
		
			$results_view		= $midminusone;
			$results_lim_sta	= $results_image - $mid;
			$results_lim_end	= $number;
			$image_key			= $results_view;
			
		}
		
		/*		
		if ($results_image < 5) { // The image we're viewing is before position five in the list
			
			$results_view		= $results_image;
			$results_lim_sta	= 0;
			$results_lim_end	= 5;
			$image_key			= $results_view -1;
			
		} elseif (($results_image + 1) >= $results_total - 3) { // The image is one of the last three at the end of the list
			
			$results_view		= 4 - ($results_total - $results_image);
			$results_lim_sta	= $results_total - 5;
			$results_lim_end	= 6;
			$image_key			= $results_view;
		
		} else { // The image is somewhere in the middle of the list
		
			$results_view		= 2;
			$results_lim_sta	= $results_image - 3;
			$results_lim_end	= 5;
			$image_key			= $results_view;
			
		}
		*/
		
		switch ($switch) {
			
			case "results_view":
				return $results_view;
			break;
		
			case "results_lim_sta":
				return $results_lim_sta;
			break;
		
			case "results_lim_end":
				return $results_lim_end;
			break;
		
			case "image_key":
				return $image_key;
			break;
		
		}

	}
	
	/*
	*
	*	This method increments the view counter for this gallery by one
	*
	*/
	function updateImageViewCounter () {
		
		global $ses, $cfg;
		
		if (!$ses['psg_administrator']) { // Only update counter if user is not an admin
		
			// Update the number of times this gallery's been viewed
			$this->_dbl->sqlQuery("UPDATE `".PIX_TABLE_IMGS."` SET image_counter = image_counter + 1 WHERE image_id = '$image_id';");
		
		}
			
	}
		
}

?>