<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*	
*	A class to handle working with forms and environment variables
*	
*	Exposes the following methods:
*	
*	public	fetchPost()				Return the value of a posted form item named $name
*	public	fetchGet()				Return the value of a query string (GET request) form item named $name
*	public	fetchGlobal()			Return the value of any (Get/Post) form item named $name
*	public	fetchCookie()			Return the value of a cookie variable named $name
*	public	fetchServer()			Return the value of a server variable named $name
*	public	unregisterGlobals()		Clear the super global $GLOBALS array and re-instate safe values
*	
*/

class EnvData {
	
	var $browserAppVersion;
	var $browserAppName;
	var $browserSystemName;
	var $browserSystemVersion;
	
	// Define HTML tags allowed by default
	var $allowed_html_tags = "";
	
	/*
	*	
	*	This is the class constructor for the Form class
	*	
	*	EnvData -- Unregisters global variables and intialises local properties
	*	
	*	class EnvData(void)
	*
	*/
	
	function EnvData() {
		
		// Clean up global scope
		$this->unregisterGlobals(); 	
		
		// Load browser detection script
		require_once ('pixaria.Useragent.php');
		
		$this->browserAppVersion	= browser_detection('number');
		$this->browserAppName		= browser_detection('browser');
		$this->browserSystemName	= browser_detection('os');
		$this->browserSystemVersion	= browser_detection('os_number');
				
	}
	
	/*
	*	Returns the browser application version
	*/
	function getBrowserAppVersion () {
	
		return ($this->browserAppVersion);
	
	}
	
	/*
	*	Returns the browser application name as an abbreviated string 
	*/
	function getBrowserAppName () {
	
		return ($this->browserAppName);
	
	}
	
	/*
	*	Returns the browser application version
	*/
	function getBrowserSystemName () {
	
		return ($this->browserSystemName);
	
	}
	
	/*
	*	Returns the browser application version
	*/
	function getBrowserSystemVersion () {
	
		return ($this->browserSystemVersion);
	
	}
	
	/*
	*
	*	Pushes $value with the key $key into the superglobal array $_POST
	*
	*/
	function pushPost ($key, $value) {
	
		if (isset($value) && isset($key)) {
		
			$_POST[$key] = $value;
			
			return true;
			
		} else {
		
			return false;
		
		}
	
	}
	
	/*
	*	Return the value of a posted form item named $name
	*	
	*	string  fetchPost(string key)
	*	
	*	If key matches key in $_POST array, Returns string which is value of key from $_POST array 
	*	
	*	if key does NOT match key in $_POST array, Returns EMPTY STRING.
	*
	*/
	
	function fetchPost($name,$quotes=false) {
	
		foreach($_POST as $key=>$value){
	
			if ($name == $key) {
			
				if ($value !== '') { $output = $value; }
				
			}
			
		}
	
		if (!isset($output)) {
		
			$output = '';
		
		} elseif (is_string($output)) {
		
			// Remove poison null bytes
			$output = str_replace("\0","",$output);
			
			$output = strip_tags(substr(trim($output),0,10000),$this->allowed_html_tags);
		
		}
		
		if ($quotes) { return stripslashes($output); } else { return $output; }
		
	}
	
	
	/*
	*	Return the value of a query string (GET request) form item named $name
	*	
	*	string  fetchGet(string key)
	*	
	*	If key matches key in $_GET array, Returns string which is value of key from $_GET array 
	*	
	*	if key does NOT match key in $_POST array, Returns EMPTY STRING.
	*
	*/
	
	function fetchGet($name,$quotes=false) {
	
		foreach($_GET as $key=>$value){
	
			if ($name == $key) {
			
				if ($value !== '') { $output = $value; }
				
			}
			
		}
	
		if (!isset($output)) {
		
			$output = '';
		
		} elseif (is_string($output)) {
		
			// Remove poison null bytes
			$output = str_replace("\0","",$output);
			
			$output = strip_tags(substr(trim($output),0,5000),$this->allowed_html_tags);
		
		}
		
		if ($quotes) { return stripslashes($output); } else { return $output; }
		
	}
	
	/*
	*	Return the value of any (Get/Post) form item named $name
	*
	*	string  fetchGlobal(string key)
	*	
	*	If key matches key in $_POST or $_GET array, Returns string which is value of key from $_POST or $_GET array.
	*	
	*	if key does NOT match key in $_POST or $_GET array, Returns EMPTY STRING.
	*	
	*	by order of execution, precedence is given to $_POST.
	*
	*/
	
	function fetchGlobal($name,$quotes=false) {
	
		foreach($_GET as $key=>$value){
	
			if ($name == $key) {
			
				if ($value !== '') { $output = $value; }
				
			}
			
		}
		
		foreach($_POST as $key=>$value){
	
			if ($name == $key) {
			
				if ($value !== '') { $output = $value; }
				
			}
			
		}
		
		if (!isset($output)) {
		
			$output = '';
		
		} elseif (is_string($output)) {
			
			// Remove poison null bytes
			$output = str_replace("\0","",$output);
			
			$output = strip_tags(substr(trim($output),0,10000),$this->allowed_html_tags);
		
		}
		
		if ($quotes) { return stripslashes($output); } else { return $output; }
		
	}
	
	/*
	*	Get post data without cleaning up quotes
	*/
	function fetchRawPost ($name) {
		
		if (trim($_POST[$name]) != "") {
			
			// Get data from superglobal post array
			$output = $_POST[$name];
			
			// Remove poison null bytes
			$output = str_replace("\0","",$output);
			
			// Return data
			return $output;
		
		}
	
	}
	
	/*
	*
	*	Return values based on whether a checkbox is checked or not
	*	
	*	The $if_on and $if_off parameters allow custom return based on
	*	whether the checkbox is ticked or not.  The defaults are 1 and
	*	0 respectively returned as string values NOT boolean.
	*	
	*	This assumes the checkbox has no HTML value attribute set
	*
	*/
	function fetchCheckbox ($name, $if_on="", $if_off="") {
	
		$data = $this->fetchGlobal($name);
		
		if ($data == "on") 	{
		
			if ($if_on != "") 	{ return $if_on; } else { return "1"; }
		
		} else {
		
			if ($if_off != "") 	{ return $if_off; } else { return "0"; }
		
		}
		
	}
	
	/*
	*	Return the value of a cookie variable named $name
	*	
	*	string  fetchCookie(string key)
	*	
	*	If key matches key in $_COOKIE array, Returns string which is value of key from $_COOKIE array 
	*	
	*	if key does NOT match key in $_COOKIE array, Returns EMPTY STRING.
	*
	*/
	
	function fetchCookie($name,$quotes = "") {
	
		foreach($_COOKIE as $key=>$value){
	
			if ($name == $key) {
			
				if ($value !== '') { $output = $value; }
				
			}
			
		}
	
		if (!isset($output)) {
		
			$output = '';
		
		} elseif (is_string($output)) {
		
			// Remove poison null bytes
			$output = str_replace("\0","",$output);
			
			$output = strip_tags(substr(trim($output),0,5000));
		
		}
		
		if ($quotes) { return stripslashes($output); } else { return $output; }
		
	}

	/*
	*	Return the value of a server variable named $name
	*	
	*	string  fetchServer(string key)
	*	
	*	If key matches key in $_SERVER array, Returns string which is value of key from $_COOKIE array 
	*	
	*	if key does NOT match key in $_SERVER array, Returns EMPTY STRING.
	*
	*/
	
	function fetchServer($name) {
	
		foreach($_SERVER as $key=>$value){
	
			if ($name == $key) {
			
				if ($value !== '') { $output = $value; }
				
			}
			
		}
	
		if (!isset($output)) {
		
			$output = '';
		
		} elseif (is_string($output)) {
		
			$output = eregi_replace("\"","&quot;",$output);
			
			// Remove poison null bytes
			$output = str_replace("\0","",$output);
			
			$output = strip_tags(substr(trim($output),0,5000));
		
		}
		
		return($output);
	
	}

	/*
	*
	*	
	*
	*/
	function formGet ($name) {
	
		if (array_key_exists($name,$_GET)) {
			$output = $_GET[$name];
		}
		
		if (is_array($output)) {
			
			return $output;
		
		} elseif (!isset($output)) {
		
			$output = '';
		
		} elseif (is_string($output)) {
		
			// Remove poison null bytes
			$output = str_replace("\0","",$output);
			
			$output = strip_tags(substr(trim($output),0,5000),$this->allowed_html_tags);
		
		}
		
		// If magic_quotes is on then strip the slashes
		if (ini_get('magic_quotes_gpc')) { return stripslashes($output); } else { return $output; }
		
	}
	
	/*
	*
	*	
	*
	*/
	function formPost ($name) {
	
		if (array_key_exists($name,$_POST)) {
			$output = $_POST[$name];
		}
		
		if (is_array($output)) {
			
			return $output;
		
		} elseif (!isset($output)) {
		
			$output = '';
		
		} elseif (is_string($output)) {
		
			// Remove poison null bytes
			$output = str_replace("\0","",$output);
			
			$output = strip_tags(substr(trim($output),0,5000),$this->allowed_html_tags);
		
		}
		
		// If magic_quotes is on then strip the slashes
		if (ini_get('magic_quotes_gpc')) { return stripslashes($output); } else { return $output; }
		
	}
	
	/*
	*	Get post data without cleaning up quotes or HTML
	*/
	function formPostRaw ($name) {
		
		if (trim($_POST[$name]) != "") {
			
			// Get data from superglobal post array
			$output = $_POST[$name];
			
			// Remove poison null bytes
			$output = str_replace("\0","",$output);
			
			// Return data
			return $output;
		
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function formGlobal ($name) {
	
		if (array_key_exists($name,$_POST)) {
			$output = $_POST[$name];
		} elseif (array_key_exists($name,$_GET)) {
			$output = $_GET[$name];
		}
		
		if (is_array($output)) {
			
			return $output;
		
		} elseif (!isset($output)) {
		
			$output = '';
		
		} elseif (is_string($output)) {
		
			// Remove poison null bytes
			$output = str_replace("\0","",$output);
			
			$output = strip_tags(substr(trim($output),0,5000),$this->allowed_html_tags);
		
		}
		
		// If magic_quotes is on then strip the slashes
		if (ini_get('magic_quotes_gpc')) { return stripslashes($output); } else { return $output; }
		
	}

	/*
	*
	*	
	*
	*/
	function formCheckbox ($name) {
		
		return $this->fetchCheckbox($name);
	
	}
	
	/*
	*
	*	
	*
	*/
	function formHtml ($name) {
		
		$output = $this->html($name);
		
		// If magic_quotes is on then strip the slashes
		if (ini_get('magic_quotes_gpc')) { return stripslashes($output); } else { return $output; }
		
	}
	
	/*
	*
	*	GET HTML FORM DATA AND SANITISE IT BY REMOVING JAVASCRIPT
	*
	*/
	function html ($name) {
		
		// Get default allowed HTML tags
		$original_tags = $this->allowed_html_tags;
		
		// Override the allowed HTML tags which can be received in formdata
		$this->allowed_html_tags = "<div><b><i><a><img><h1><h2><h3><h4><h5><h6><p><ul><ol><li><em><strong><emphasis><blockquote><code><pre><script>";
		
		// Get form data leaving double quotes intact
		$data = $this->fetchGlobal($name, false);
		
		// Restore original allowed HTML tags
		$this->allowed_html_tags = $original_tags;
		
		// Return cleaned up data
		return preg_replace_callback('/^<([a-z])\s.*?>/i',array($this,'removeChars'),$data);
		
	}
	
	/*
	*
	*	REMOVE JAVASCRIPT/SQL DANGEROUS CHARACTERS
	*
	*/
	function removeChars ($string) {
		
		// Strip out the following characters: ' ; ( ) ` { } +
		return preg_replace("/'|;|\(|\)|`|\{|\}|\+/",'',$string[0]);
	
	}

	/*
	*	Clear out the entire global scope and reinstate safe values
	*	
	*	null unregisterGlobals(void)
	*	
	*/
	function unregisterGlobals() {
	
	   // Save the existing superglobals first 
	   $REQUEST		= $_REQUEST;
	   $GET			= $_GET;
	   $POST		= $_POST;
	   $COOKIE		= $_COOKIE;
	   $FILES		= $_FILES;
	   $ENV			= $_ENV;
	   $SERVER		= $_SERVER;
	
	   if (isset($_SESSION)) { 
	      $SESSION = $_SESSION;
	   }
	
	   // Unset the $GLOBALS array (clear all)
	   foreach($GLOBALS as $key => $value) {
	
	      if ($key != 'GLOBALS') {
	
	         unset($GLOBALS[$key]);
	
	      }
	
	   }
	
	   // Re-assign the saved superglobals again
	   $_REQUEST	= $REQUEST;
	   $_GET		= $GET;
	   $_POST		= $POST;
	   $_COOKIE		= $COOKIE;
	   $_FILES		= $FILES;
	   $_ENV		= $ENV;
	   $_SERVER		= $SERVER;
	
	   if (isset($SESSION)) {
	      $_SESSION = $SESSION;
	   }
	
	}
	
}


?>