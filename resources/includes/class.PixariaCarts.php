<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

/*
*	
*	A class to handle working with multiple shopping carts
*	
*	Exposes the following methods:
*	
*/

/*
*	
*	Define the different cart status types
*	
*/

define ("PIX_CART_ACTIVE",1);
define ("PIX_CART_AWAIT_QUOTE","");
define ("PIX_CART_AWAIT_ACCEPTANCE",2);
define ("PIX_CART_AWAIT_PAYMENT",3);
define ("PIX_CART_AWAIT_SHIPPING","");
define ("PIX_CART_COMPLETE",4);

class PixariaCarts {
	
	// Private variables
	var $_dbl;
	var $_update_flag = false;
	
	// Public variables
	var $method;
	var $status;
	var $status_description;
	var $carts_present;
	var $cart_id;
	var $cart_items;
	var $cart_status;
	var $cart_status_icon;
	var $cart_view_url;
	var $transaction_id;
	var $date_processed;
	var $date_completed;
	var $cart_user_id;
	var $cart_user_name;
	var $cart_user_email;
		
	// Error log for malformed data
	var $error 		= false;
	var $error_log 	= array();
	
	/*
	*	
	*	This is the class constructor for the PixariaCarts class
	*	
	*	PixariaCarts -- Load data for all shopping cart transactions
	*	
	*	class PixariaCarts(int status, int sort, string method)
	*
	*/
	
	function PixariaCarts($status, $sort, $method) {
		
		// Localise globals
		global $ses, $cfg;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		switch ($status) {
		
			case 1: // Status is open
			
				// Set the status description for this view option
				$this->status_description = "Open shopping carts";
				
				$sub_select = "WHERE ".$cfg['sys']['table_cart'].".status = '1'";
			
			break;
		
			case 2: // Status is pending further acton
			
				// Set the status description for this view option
				$this->status_description = "Transactions in progress";
				
				$sub_select = "WHERE ".$cfg['sys']['table_cart'].".status = '2'";
			
			break;
		
			case 3: // Status is complete
			
				// Set the status description for this view option
				$this->status_description = "Transactions awaiting payment";
				
				$sub_select = "WHERE ".$cfg['sys']['table_cart'].".status = '3'";
			
			break;
		
			case 4: // Status is complete
			
				// Set the status description for this view option
				$this->status_description = "Completed transactions";
				
				$sub_select = "WHERE ".$cfg['sys']['table_cart'].".status = '4'";
			
			break;
		
			default: // No status (show everything)
			
				// Set the status description for this view option
				$this->status_description = "All stored sales";
				
				$sub_select = "WHERE ".$cfg['sys']['table_cart'].".status != '1'";
			
			break;
		
		}
		
		switch ($sort) {
		
			case family_name:
			
				if ($method == "ASC" || !$method) {
				
					$sql_sort = "ORDER BY ".$cfg['sys']['table_user'].".family_name ASC";
					$this->method = "DESC";
				
				} else {
					
					$sql_sort = "ORDER BY ".$cfg['sys']['table_user'].".family_name DESC";
					$this->method = "ASC";
				
				}
			
			break;
			
			case cart_items:
			
				if ($method == "ASC" || !$method) {
				
					$sql_sort = "ORDER BY cart_items ASC";
					$this->method = "DESC";
				
				} else {
					
					$sql_sort = "ORDER BY cart_items DESC";
					$this->method = "ASC";
				
				}
			
			break;
			
			case date_processed:
			
				if ($method == "ASC" || !$method) {
				
					$sql_sort = "ORDER BY ".$cfg['sys']['table_cart'].".date_processed ASC";
					$this->method = "DESC";
				
				} else {
					
					$sql_sort = "ORDER BY ".$cfg['sys']['table_cart'].".date_processed DESC";
					$this->method = "ASC";
				
				}
			
			break;
			
			case date_completed:
			
				if ($method == "ASC" || !$method) {
				
					$sql_sort = "ORDER BY ".$cfg['sys']['table_cart'].".date_completed ASC";
					$this->method = "DESC";
				
				} else {
					
					$sql_sort = "ORDER BY ".$cfg['sys']['table_cart'].".date_completed DESC";
					$this->method = "ASC";
				
				}
			
			break;
			
			default:
				
				$sql_sort = "ORDER BY ".$cfg['sys']['table_cart'].".cart_id DESC";
				$this->method = "ASC";
			
			break;
			
		}
		
		// Get an array of all shopping carts this user has
		$sql	 = "SELECT ".PIX_TABLE_CART.".*,
					
					".PIX_TABLE_USER.".userid AS cart_user_id,
					
					CONCAT(".PIX_TABLE_USER.".family_name,', ',".PIX_TABLE_USER.".first_name) AS cart_user_name,
					
					".PIX_TABLE_USER.".email_address AS cart_user_email,
					
					COUNT(id) AS cart_items FROM ".PIX_TABLE_CART."
		
					LEFT JOIN ".PIX_TABLE_CRME." ON ".PIX_TABLE_CRME.".cart_id = ".PIX_TABLE_CART.".cart_id
						
					LEFT JOIN ".PIX_TABLE_USER." ON ".PIX_TABLE_CART.".userid = ".PIX_TABLE_USER.".userid
					
					$sub_select
					
					GROUP BY ".PIX_TABLE_CART.".cart_id
					
					$sql_sort";
		
		// Retrieve image information from database
		$cart_data = $this->_dbl->sqlSelectRows($sql);
		
		if (is_array($cart_data)) {
		
			foreach ($cart_data as $key => $value) {
				
				if ($value['cart_items'] != "") {
				
					$this->cart_id[]			= $value['cart_id'];
					$this->cart_user_id[]		= $value['cart_user_id'];
					$this->cart_items[]			= $value['cart_items'];
					$this->cart_status[]		= $value['status'];
					$this->transaction_id[]		= $value['transaction_id'];
					$this->date_processed[]		= $value['date_processed'];
					$this->date_completed[]		= $value['date_completed'];
					$this->cart_user_name[]		= stripslashes($value['cart_user_name']);
					$this->cart_user_email[]	= $value['cart_user_email'];
					
					switch ($value['status']) {
					
						case 1: // Open cart
						
							$this->cart_status_icon[] = "open";
							$this->cart_status_text[] = "In progress";
							$this->cart_view_url[]	= $cfg['sys']['base_url'].$cfg['fil']['admin_transaction']."?cmd=30&amp;cart_id=".$value['cart_id'];
							
						break;
					
						case 2: // Transactions in progress
						
							$this->cart_status_icon[] = "awaits";
							$this->cart_status_text[] = "Awaits quote";
							$this->cart_view_url[]	= $cfg['sys']['base_url'].$cfg['fil']['admin_transaction']."?cmd=10&amp;cart_id=".$value['cart_id'];
							
						break;
					
						case 3: // Awaits payment
						
							$this->cart_status_icon[] = "pending";
							$this->cart_status_text[] = "Awaits payment";
							$this->cart_view_url[]	= $cfg['sys']['base_url'].$cfg['fil']['admin_transaction']."?cmd=10&amp;cart_id=".$value['cart_id'];
						
						break;
					
						case 4: // Completed transaction
						
							$this->cart_status_icon[] = "complete";
							$this->cart_status_text[] = "Completed transaction";
							$this->cart_view_url[]	= $cfg['sys']['base_url'].$cfg['fil']['admin_transaction']."?cmd=10&amp;cart_id=".$value['cart_id'];
						
						break;
						
						default:
						
							$this->cart_status_icon[] = "awaits";
							$this->cart_status_text[] = "Awaits quote";
							$this->cart_view_url[]	= $cfg['sys']['base_url'].$cfg['fil']['admin_transaction']."?cmd=10&amp;cart_id=".$value['cart_id'];

						break;
						
					}
					
				}
				
			}
			
			// Indicate there are carts to view
			$this->carts_present = (bool)TRUE;
			
		} else {
		
			return;
			
		}
		
	}
	
	/*
	*	Return the method
	*/
	function getMethod () {
	
		return $this->method;
	
	}
		
	/*
	*	Return the status
	*/
	function getStatus () {
	
		return $this->status;
	
	}
		
	/*
	*	Return the status_description
	*/
	function getStatusDescription () {
	
		return $this->status_description;
	
	}
		
	/*
	*	Return the carts_present
	*/
	function getCartsPresent () {
	
		return $this->carts_present;
	
	}
		
	/*
	*	Return the cart_id
	*/
	function getCartId () {
	
		return $this->cart_id;
	
	}
		
	/*
	*	Return the cart_items
	*/
	function getCartItems () {
	
		return $this->cart_items;
	
	}
		
	/*
	*	Return the cart_status
	*/
	function getCartStatus () {
	
		return $this->cart_status;
	
	}
		
	/*
	*	Return the cart_status_text
	*/
	function getCartStatusText () {
	
		return $this->cart_status_text;
	
	}
		
	/*
	*	Return the cart_status_icon
	*/
	function getCartStatusIcon () {
	
		return $this->cart_status_icon;
	
	}
		
	/*
	*	Return the cart_view_url
	*/
	function getCartViewUrl () {
	
		return $this->cart_view_url;
	
	}
		
	/*
	*	Return the transaction_id
	*/
	function getTransactionId () {
	
		return $this->transaction_id;
	
	}
		
	/*
	*	Return the date_created
	*/
	function getDateCreated () {
	
		return $this->date_created;
	
	}
		
	/*
	*	Return the date_processed
	*/
	function getDateProcessed () {
	
		return $this->date_processed;
	
	}
		
	/*
	*	Return the date_completed
	*/
	function getDateCompleted () {
	
		return $this->date_completed;
	
	}
	
	/*
	*	Return the cart_user_name
	*/
	function getCartUserName() {
	
			return $this->cart_user_name;
	
	}
		
	/*
	*	Return the cart_user_email
	*/
	function getCartUserEmail() {
	
			return $this->cart_user_email;
	
	}
		
	/*
	*	Return the cart_user_id
	*/
	function getCartUserId() {
	
			return $this->cart_user_id;
	
	}
		
}


?>