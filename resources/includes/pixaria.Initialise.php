<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

/*
*
* 	Do not edit any of the values below this line
* 	Doing so may break your installation!
*
*/

// 	Set error reporting to minor errors only
error_reporting(E_ERROR | E_WARNING | E_PARSE);

//error_reporting(E_ALL);
//ini_set("display_errors","on");

define("PIXGALL","ON");

// Load the class file for handling forms and environment variables
require_once ("class.EnvData.php");

// Initiliase the class for handling forms and environment variables and clear all variables
$objEnvData = new EnvData();

//	Load the configuration file for Pixaria
require_once (dirname(dirname(dirname(__FILE__)))."/"."pixaria.config.php");


// Create a safe working base path constant
define ("SYS_BASE_PATH",$cfg['sys']['base_path']);

// Work around REQUEST_URI bug under W2K/IIS/CGI/PHP 
if (!isset($_SERVER['REQUEST_URI']) and isset($_SERVER['SCRIPT_NAME'])) 
{ 
    $_SERVER['REQUEST_URI'] = $_SERVER['SCRIPT_NAME']; 
    if (isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING'])) {
        $_SERVER['REQUEST_URI'] .= '?' . $_SERVER['QUERY_STRING']; 
	}
}

// Tell MySQL to work with UTF-8 text
@mysql_query("SET NAMES 'utf8'");

// Tell MySQL to operate in 'traditional' SQL mode (http://bugs.mysql.com/bug.php?id=18551)
@mysql_query("SET sql_mode = ''");

//	Import the Pixaria Gallery user editable settings
require_once ("pixaria.Constants.php");

//	Import the Pixaria Gallery functions
require_once ("pixaria.Functions.php");

//	Load session management and authentication library
require_once ("pixaria.Authentication.php");

//	Load the database function library
require_once ("pixaria.Database.php");

//	Load the Pixaria extension function library
require_once ("functions.Pixaria.php");

// Load the Pixaria settings class
require_once ("class.PixariaSettings.php");

// Turn on big SQL select join capability to deal with large databases
// Needs to be replaced by better indexing in future updates
@mysql_query("SET SQL_BIG_SELECTS=1;");

// Initialise the Pixaria settings object
$objSettings = new PixariaSettings();

// Include the Pixaria Gallery main language file
if (file_exists(SYS_BASE_PATH . "resources/local/".LOCAL_LANGUAGE.".php")) {
	require_once (SYS_BASE_PATH . "resources/local/".LOCAL_LANGUAGE.".php");
} else {
	require_once (SYS_BASE_PATH . "resources/local/en_GB.php");
}

// Include the e-mail strings file
if (file_exists(SYS_BASE_PATH . "resources/local/".LOCAL_LANGUAGE."_email.php")) {
	require_once (SYS_BASE_PATH . "resources/local/".LOCAL_LANGUAGE."_email.php");
} else {
	require_once (SYS_BASE_PATH . "resources/local/en_GB_email.php");
}

// Move the localised strings into the global scope
$GLOBALS['_STR_'] = $strings;

// Destroy the $lan variable to free up memory
unset($strings);

// Get the user's session information and group membership information
if ($objEnvData->fetchCookie('psg_userid') != '') {
	$ses = pixGetSession($objEnvData->fetchCookie('psg_userid'));
} elseif ($objEnvData->fetchPost('psg_userid') != '') {
	$ses = pixGetSession(urldecode($objEnvData->fetchPost('psg_userid')));
}

// Add a config variable for the SQL fragment to limit access to images the user is authorised to view
pix_image_access_sql();

// Add a config variable for the SQL fragment to limit access to galleries the user is authorised to view
pix_gallery_access_sql();

// Set up user preferences
pix_user_preferences();

// Define the URL for the Divestock website
if ($cfg['sys']['base_url'] == 'http://pixaria2.dev') {
	define('DIVESTOCK_DOMAIN','www.divestock.com');
	define('DIVESTOCK_URL','http://www.divestock.com/');
} else {
	define('DIVESTOCK_DOMAIN','divestock.dev');
	define('DIVESTOCK_URL','http://www.divestock.com/');
}

// Import the main Smarty class for Pixaria
require_once (SYS_BASE_PATH . "resources/includes/class.Smarty.php");

?>