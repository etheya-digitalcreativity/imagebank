<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

// Load the PixariaImage class so we can extend it
require_once ('class.PixariaImage.php');
		
/*
*	
*	A class to handle working with multiple images.
*	
*	Extends the PixariaImage class
*	
*	Exposes the following methods:
*	
*	public	setChangeProperty()		Set the properties that must be updated
*	
*/

class PixariaImageAdmin extends PixariaImage {
	
	var $image_exif_date;
	var $image_exif_camera_make;
	var $image_exif_camera_model;
	var $image_iptc_keywords;
	var $image_iptc_caption;
	var $image_iptc_photographer;
	var $image_iptc_headline;
	var $image_iptc_byline;
	var $image_iptc_bltitle;
	var $image_iptc_credit;
	var $image_iptc_source;
	var $image_iptc_objname;
	var $image_iptc_reference;
	var $image_iptc_date;
	var $image_colr_average_hex;
	var $image_colr_average_dec;
	
	var $image_group_links;
	var $image_group_titles;
	var $image_group_ids;
	
	var $image_category_links;
	var $image_category_titles;
	var $image_category_ids;
	
	var $mode = "admin";
	
	/*
	*	
	*	This is the class constructor for the PixariaImageAdmin class
	*	
	*	PixariaImageAdmin -- Load data for an image
	*	
	*	mixed PixariaImageAdmin([int image_id])
	*
	*/
	
	function PixariaImageAdmin($image_id = "") {
		
		global $cfg, $ses;
		
		// Initialise the Pixaria Images class
		$this->PixariaImage($image_id);
	
		// Load EXIF information for the original image into an array
		$exif_data = $this->getExifMetaData();

		if ($exif_data[0] != false) {
		
			$this->image_exif_date			= $exif_data[1];
			$this->image_exif_camera_make	= $exif_data[2];
			$this->image_exif_camera_model	= $exif_data[3];
		
		}
	
		// Get IPTC header information for this image if needed
		$iptc_data = $this->getIptcMetaData();
		
		if (isset($iptc_data['keywords']))		{ $this->image_iptc_keywords		= $iptc_data['keywords'];	}
		if (isset($iptc_data['caption']))		{ $this->image_iptc_caption			= $iptc_data['caption'];	}
		if (isset($iptc_data['copyright']))		{ $this->image_iptc_photographer	= $iptc_data['copyright'];	}
		if (isset($iptc_data['headline']))		{ $this->image_iptc_headline		= $iptc_data['headline'];	}
		if (isset($iptc_data['byline']))		{ $this->image_iptc_byline			= $iptc_data['byline'];		}
		if (isset($iptc_data['bltitle']))		{ $this->image_iptc_bltitle			= $iptc_data['bltitle'];	}
		if (isset($iptc_data['credit']))		{ $this->image_iptc_credit			= $iptc_data['credit'];		}
		if (isset($iptc_data['source']))		{ $this->image_iptc_source			= $iptc_data['source'];		}
		if (isset($iptc_data['objname']))		{ $this->image_iptc_objname			= $iptc_data['objname'];	}
		if (isset($iptc_data['reference']))		{ $this->image_iptc_reference		= $iptc_data['reference'];	}
		if (isset($iptc_data['date']))			{ $this->image_iptc_date			= $iptc_data['date'];		}
		
		// Take a colour sample of the image to get the median colour
		if ($cfg['set']['gd_upload']) {
			
			// Generate a single pixel bitmap of the average colour
			// of the image which has just been uploaded
			$source = imagecreatefromjpeg($this->image_icon_path);

			// Set the image starting dimensions
			$imageX = imagesx($source);
			$imageY = imagesy($source);
			
			// Set the new image dimensions
			$thumbX = 1;
			$thumbY = 1;
			
			// Create a destination image object
			$dest   = imagecreatetruecolor($thumbX, $thumbY);
			imagecopyresampled ($dest, $source, 0, 0, 0, 0, $thumbX, $thumbY, $imageX, $imageY);
			
			// Set the image colour palette
			$rgb = ImageColorAt($dest, 0, 0);
			$r = ($rgb >> 16) & 0xFF;
			$g = ($rgb >> 8) & 0xFF;
			$b = $rgb & 0xFF;
			
			// Set values for the image colour properties
			$this->image_colr_average_hex = strtoupper(dechex($r).dechex($g).dechex($b));
			$this->image_colr_average_dec = "$r, $g, $b";
			
		}
		
		/*
		*	The following code gets information about categories
		*/
		
		// Load the PixariaCategory class so we can extend it
		require_once ('class.PixariaCategoryImage.php');
		
		// Initialise category-image linking object for this image
		$objCategory = new PixariaCategoryImage($this->image_id);
		
		// Set properties for categories
		$this->image_category_links 	= $objCategory->getImageCategoryLinks();
		$this->image_category_titles 	= $objCategory->getCategoryListTitles();
		$this->image_category_ids 		= $objCategory->getCategoryListIds();
		
		/*
		*	The following code gets information about groups
		*/
		
		// Load the PixariaGroupImage class so we can extend it
		require_once ('class.PixariaGroupImage.php');
		
		// Initialise group-image linking object for this image
		$objGroup = new PixariaGroupImage($this->image_id);
		
		// Set properties for groups
		$this->image_group_links 	= $objGroup->getImageGroupLinks();
		$this->image_group_titles 	= $objGroup->getGroupListTitles();
		$this->image_group_ids 		= $objGroup->getGroupListIds();
		
	}
	
	/*
	*	Return the IPTC date in MySQL date format
	*/
	function getIptcDate () {
	
		if (isset($this->image_iptc_date)) {
			
			$dd	= substr($this->image_iptc_date,6,2);
			$mm	= substr($this->image_iptc_date,4,2);
			$yy	= substr($this->image_iptc_date,0,4);
			
			return "$yy-$mm-$dd 00:00:00";
			
		}
	
	}
	
	/*
	*	Return the EXIF date in MySQL date format
	*/
	function getExifDate () {
	
		if (isset($this->image_exif_date)) {
			
			return $this->image_exif_date;
			
		}
	
	}
	
	/*
	*	Return the average image colour as a hexadecimal number
	*/
	function getImageColrAverageHex () {
	
		if (isset($this->image_colr_average_hex)) {
			
			return $this->image_colr_average_hex;
			
		} else {
			
			return false;
			
		}
	
	}
	
	/*
	*	Return the average image colour as decimal values
	*/
	function getImageColrAverageDec () {
	
		if (isset($this->image_colr_average_dec)) {
			
			return $this->image_colr_average_dec;
			
		} else {
			
			return false;
			
		}
	
	}
	
	/*
	*	Return the image_group_links
	*/
	function getImageGroupLinks () {
		
		if (is_array($this->image_group_links)) {
			
			return $this->image_group_links;
			
		} else {
			
			return false;
			
		}
		
	}

	/*
	*	Return the image_link_categories
	*/
	function getImageCategoryLinks () {
	
		return $this->image_category_links;
	
	}

	/*
	*	Return the image_category_titles
	*/
	function getCategoryListTitles () {
	
		return $this->image_category_titles;
	
	}

	/*
	*	Return the image_category_ids
	*/
	function getCategoryListIds () {
	
		return $this->image_category_ids;
	
	}

	/*
	*	Return the image_group_titles
	*/
	function getGroupListTitles () {
	
		return $this->image_group_titles;
	
	}

	/*
	*	Return the image_group_ids
	*/
	function getGroupListIds () {
	
		return $this->image_group_ids;
	
	}

	/*
	*
	*	getExifMetaData -- Load key EXIF data from an image
	*	
	*	string  getExifMetaData(string filepath)
	*	
	*	If EXIF data found returns an array of the data with the value of the first key set to true
	*	
	*	If EXIF data not found or and error occurs returns an array with the value of the first key set to false
	*
	*/
	
	function getExifMetaData() {
		
		$filepath = $this->image_comp_path;
		
		if (function_exists(exif_read_data)) {
		
			if (!@exif_read_data($filepath, 'IFD0')) {
			
				$exif_data[0] = false;
			
			} else {
			
				$exif = @exif_read_data($filepath, 0, true);
				
				## Get the camera make and model
				$exif_camr_mak	= $exif[IFD0][Make];
				$exif_camr_mod	= $exif[IFD0][Model];
				
				$exif_date_dat = explode(" ",$exif[EXIF][DateTimeOriginal]);
				
				$exif_date_ymd	= explode(":",$exif_date_dat[0]);
				$exif_date_hms	= explode(":",$exif_date_dat[1]);
				
				$exif_date_day	= $exif_date_ymd[2];
				$exif_date_mon	= $exif_date_ymd[1];
				$exif_date_yrs	= $exif_date_ymd[0];
				
				$exif_date_sec	= $exif_date_hms[2];
				$exif_date_min	= $exif_date_hms[1];
				$exif_date_hrs	= $exif_date_hms[0];
				
				## Create a Unix style timestamp for the image capture date
				$exif_date_unx	= @mktime($exif_date_hrs,$exif_date_min,$exif_date_sec,$exif_date_mon,$exif_date_day,$exif_date_yrs);
				
				$exif_data[0] = true;
				$exif_data[1] = $exif_date_unx;
				$exif_data[2] = $exif_camr_mak;
				$exif_data[3] = $exif_camr_mod;
			
			}
		
		} else {
		
			$exif_data[0] = false;
		
		}
		
		return $exif_data;
	
	}
	
	/*
	*
	*	getIptcMetaData -- Get IPTC header information from a JPEG file.
	*	
	*	string getIptcMetaData(string filepath)
	*	
	*	Requires 1 argument.
	*	
	*	string is a filepath provided at runtime.
	*	
	*	Returns array for IPTC header strings.
	*
	*/
	
	function getIptcMetaData() {
	
		global $cfg;
		
		$image_path = $this->image_comp_path;
		
		$size = getimagesize ($image_path, $info);
		   
			  if(is_array($info)) {    
				$iptc = iptcparse($info["APP13"]);
				
				if (is_array($iptc)) {
				foreach (array_keys($iptc) as $s) {              
					$c = count ($iptc[$s]);
					for ($i=0; $i <$c; $i++) 
					{
					
						## Get the IPTC Headline
						if ($s == "2#105") { $IPTC['headline']	 = utf8_encode($iptc[$s][$i]); }
					
						## Get the IPTC Byline
						if ($s == "2#080") { $IPTC['byline']	 = utf8_encode($iptc[$s][$i]); }
					
						## Get the IPTC Byline Title
						if ($s == "2#085") { $IPTC['bltitle']	 = utf8_encode($iptc[$s][$i]); }
					
						## Get the IPTC Credit
						if ($s == "2#110") { $IPTC['credit']	 = utf8_encode($iptc[$s][$i]); }
					
						## Get the IPTC Source
						if ($s == "2#115") { $IPTC['source']	 = utf8_encode($iptc[$s][$i]); }
					
						## Get the IPTC Object Name
						if ($s == "2#005") { $IPTC['objname']	 = utf8_encode($iptc[$s][$i]); }
					
						## Get the IPTC City
						if ($s == "2#090") { $IPTC['city']		 = utf8_encode($iptc[$s][$i]); }
					
						## Get the IPTC Province
						if ($s == "2#095") { $IPTC['province']	 = utf8_encode($iptc[$s][$i]); }
					
						## Get the IPTC Country
						if ($s == "2#101") { $IPTC['country']	 = utf8_encode($iptc[$s][$i]); }
					
						## Get the IPTC Reference
						if ($s == "2#103") { $IPTC['reference']	 = utf8_encode($iptc[$s][$i]); }
					
						## Get the IPTC Copyright
						if ($s == "2#116") { $IPTC['copyright']	 = utf8_encode($iptc[$s][$i]); }
					
						## Get the IPTC Caption
						if ($s == "2#120") { $IPTC['caption']	 = utf8_encode($iptc[$s][$i]); }
					
						## Get the IPTC Date
						if ($s == "2#055") { $IPTC['date']		 = utf8_encode($iptc[$s][$i]); }
					
						## Get the IPTC Keywords
						if ($s == "2#025") {
							if ($x == 0) {
								$IPTC['keywords']	.= utf8_encode($iptc[$s][$i]);
								$x = 1;
							} else {
								$IPTC['keywords']	.= " " . utf8_encode($iptc[$s][$i]);
							}
						}
			
					}
				} 
							   
			}
		
		}
		
		$IPTC['descriptors']['keywords']	= "Keywords";
		$IPTC['descriptors']['headline']	= "Headline";
		$IPTC['descriptors']['byline']		= "Byline";
		$IPTC['descriptors']['bltitle']		= "Byline title";
		$IPTC['descriptors']['credit']		= "Credit";
		$IPTC['descriptors']['source']		= "Source";
		$IPTC['descriptors']['objname']		= "Object Name";
		$IPTC['descriptors']['city']		= "City";
		$IPTC['descriptors']['province']	= "Province";
		$IPTC['descriptors']['country']		= "Country";
		$IPTC['descriptors']['reference']	= "Reference";
		$IPTC['descriptors']['copyright']	= "Copyright";
		$IPTC['descriptors']['caption']		= "Caption";
		$IPTC['descriptors']['date']		= "Date";
		
		return $IPTC;         
		
	}
	

}


?>