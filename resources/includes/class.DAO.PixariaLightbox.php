<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

class DAO_PixariaLightbox {
	
	var $_dbl;
	var $_update_flag		= false;
	
	var $lightbox_id;
	var $lightbox_image_count;

	function DAO_PixariaLightbox ($lightbox_id) {
	
		global $cfg, $ses;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		if (is_numeric($lightbox_id)) {
			
			// Get the total number of images in this lightbox that can be seen by this user
			$sql	 = "SELECT COUNT(DISTINCT ". PIX_TABLE_IMGS .".image_id) FROM `". PIX_TABLE_LBME ."`
			
						LEFT JOIN `". PIX_TABLE_IMGS ."` ON ". PIX_TABLE_LBME .".image_id = ". PIX_TABLE_IMGS .".image_id
						
						LEFT JOIN `". PIX_TABLE_IMVI ."` ON ". PIX_TABLE_IMGS .".image_id = ". PIX_TABLE_IMVI .".image_id
					
						WHERE ". PIX_TABLE_LBME .".lightbox_id = '$lightbox_id'
						
						".$cfg['set']['image_access_sql']."";

			$this->lightbox_image_count = $this->_dbl->sqlCountRows($sql);
		
			// Get an array of information about this lightbox
			$lightbox_info = $this->_dbl->sqlSelectRow("SELECT * FROM ". PIX_TABLE_LBOX ." WHERE id = '$lightbox_id'");
			
		} else {
		
			return;
		
		}
		
		$this->lightbox_id			= $lightbox_info['id'];
		$this->lightbox_date		= $lightbox_info['date'];
		$this->lightbox_name		= $lightbox_info['name'];
		$this->lightbox_type		= $lightbox_info['type'];
		$this->lightbox_userid		= $lightbox_info['userid'];
		$this->lightbox_status		= $lightbox_info['status'];
		
	}
	
	
	/*
	*	Get the lightbox ID
	*/
	function getLightboxId() {
	
		return $this->lightbox_id;
	
	}

	/*
	*	Get the lightbox date
	*/
	function getLightboxDate() {
	
		return $this->lightbox_date;
	
	}

	/*
	*	Get the lightbox name
	*/
	function getLightboxName() {
	
		return $this->lightbox_name;
	
	}

	/*
	*	Get the lightbox type
	*/
	function getLightboxType() {
	
		return $this->lightbox_type;
	
	}

	/*
	*	Get the lightbox user ID
	*/
	function getLightboxUserId() {
	
		return $this->lightbox_userid;
	
	}

	/*
	*	Get the lightbox status
	*/
	function getLightboxStatus() {
	
		return $this->lightbox_status;
	
	}
	
	/*
	*	Get the lightbox image count
	*/
	function getLightboxImageCount() {
	
		return $this->lightbox_image_count;
	
	}
	
	/*
	/*
	*	Set the lightbox date
	*/
	function setGalleryDate($var) {
	
		$this->gallery_date 	= $var;
		$this->_update_flag		= (bool)true;
	
	}

	/*
	*	Set the lightbox name
	*/
	function setGalleryName($var) {
	
		$this->gallery_name 	= $var;
		$this->_update_flag		= (bool)true;
	
	}

	/*
	*	Set the gallery type
	*/
	function setGalleryType($var) {
	
		$this->gallery_type 	= $var;
		$this->_update_flag		= (bool)true;
	
	}

	/*
	*	Set the gallery userid
	*/
	function setGalleryUserid($var) {
	
		$this->gallery_userid	= $var;
		$this->_update_flag		= (bool)true;
	
	}

	/*
	*	Set the gallery status
	*/
	function setGalleryStatus($var) {
	
		$this->gallery_status	= $var;
		$this->_update_flag		= (bool)true;
	
	}

}


?>