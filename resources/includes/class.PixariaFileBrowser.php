<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

class PixariaFileBrowser {
	
	var $files			= array();
	var $file_path		= array();
	var $file_type		= array();
	var $file_ext		= array();
	var $file_icon		= array();
	var $file_size		= array();
	var $file_modified	= array();
	var $last_modified	= array();
	var $import_dir		= array();
	var $new_file		= array();
	var $success		= true;
	var $parent_access	= false;
	var $parent_dir;
	var $current_dir;
	var $file_preview;
	var $file_dimensions;
	
	function PixariaFileBrowser ($path, $base = "") {
		
		global $objEnvData;
		
		// Base path is defined as a restriction on going to far up the directory structure
		if (realpath($base) == realpath($path)):
			$this->parent_dir		= $path;
			$this->parent_access	= false;
		else:
			$this->parent_dir 		= realpath($path."/..");
			$this->parent_access	= true;
		endif;
		
		// If there is a base path and we're outside of it, return error and exit
		if ($base != "" && !eregi(realpath($base), realpath($path)) && realpath($base) != realpath($path)):
			$this->success	= false;
			return;
		endif;
		
		// Get the current directroy path
		$this->current_dir 	= $path;
		
		if ($id_dir = opendir($path)) {
		
			while (false !== ($filename = readdir($id_dir))) {
			
				if (substr($filename,0,1) != ".") {
				
					$this->files[]				= $filename;
					$this->file_path[]			= $path.$filename;
					$this->base64_file_path[]	= base64_encode($path.$filename);
					$this->file_modified[]		= filemtime($path.$filename);
					$this->last_modified[]		= $this->lastModified(filemtime($path.$filename));
					
					$type_data = $this->getFileType($path.$filename);
					
					$this->file_type[]	= $type_data[0];
					$this->file_icon[]	= $type_data[1];
					$this->file_ext[]	= $type_data[2];
					
					if (is_dir($path.$filename)) { // If the item is a directory
						
						$this->file_size[]			= "-";
						$this->file_preview[]		= (bool)false;
						$this->image_comp_width[]		= (bool)false;
						$this->image_comp_height[]		= (bool)false;
						$this->image_thumb_width[]		= (bool)false;
						$this->image_thumb_height[]		= (bool)false;
						
					} else { // The item is a file
						
						$bytes		= filesize($path.$filename);
						$kilobytes	= $bytes / 1024;
						$megabytes	= $kilobytes / 1024;
						
						
						// Check whether we can show a preview of this image
						if ($kilobytes > 300  || $type_data[2] != "jpg") {
						
							$this->file_preview[] 			= (bool)false;
							$this->image_comp_width[]		= (bool)false;
							$this->image_comp_height[]		= (bool)false;
							$this->image_thumb_width[]		= (bool)false;
							$this->image_thumb_height[]		= (bool)false;
							
						} else {
							
							// This file is one we can show a preview for so get image dimensions
							$image_dimensions 				= $this->getImageProperties($path.$filename);
							
							// If the image is too large to be a comping image, then don't show a preview
							if ($image_dimensions[2] > 650 || $image_dimensions[3] > 650) {
							
								$this->file_preview[] 			= (bool)false;
								$this->image_comp_width[]		= (bool)false;
								$this->image_comp_height[]		= (bool)false;
								$this->image_thumb_width[]		= (bool)false;
								$this->image_thumb_height[]		= (bool)false;
								
							} else {
							
								$this->file_preview[] 			= (bool)true;
								$this->image_thumb_width[]		= $image_dimensions[0];
								$this->image_thumb_height[]		= $image_dimensions[1];
								$this->image_comp_width[]		= $image_dimensions[2];
								$this->image_comp_height[]		= $image_dimensions[3];
							
							}
							
						}
						
						// Set the file size to useful unit measurements
						if ($megabytes < 1) {
							
							if ($kilobytes < 1) {
							
								$this->file_size[] = $bytes . " B";
							
							} else {
							
								$this->file_size[] = round($kilobytes,0) . " KB";
							
							}
						
						} else {
						
							$this->file_size[] = round($megabytes,1) . " MB";
						
						}
						
					}
					
					$this->import_dir[]	= $this->isImportDir($path.$filename);
					
					if (is_array($objEnvData->fetchGet('fn'))) {
					
						if (in_array($filename,$objEnvData->fetchGet('fn'))) {
						
							$this->new_file[] = (bool)TRUE;
						
						} else {
						
							$this->new_file[] = (bool)FALSE;
						
						}
					
					} else {
						
						$this->new_file[] = (bool)FALSE;
						
					}
					
				}
			}
		
			closedir($id_dir);
		
		}
		
	}
	
	/*
	*	Get the status of the object
	*/
	function getStatus () {
	
		return $this->success;
		
	}
	
	/*
	*	Get an array of file and directory names
	*/
	function getFiles () {
	
		return $this->files;
		
	}
	
	/*
	*	Get an array of file and directory paths
	*/
	function getFilePaths () {
	
		return $this->file_path;
		
	}
	
	/*
	*	Get an array of file types
	*/
	function getFileTypes () {
	
		return $this->file_type;
		
	}
	
	/*
	*	Get an array of file extensions
	*/
	function getFileExt () {
	
		return $this->file_ext;
		
	}
	
	/*
	*	Get an array of file icon names
	*/
	function getFileIconNames () {
	
		return $this->file_icon;
		
	}
	
	/*
	*	Get an array of file sizes
	*/
	function getFileSizes () {
	
		return $this->file_size;
		
	}
	
	/*
	*	Get an array of file modification dates
	*/
	function getFileModDates () {
	
		return $this->file_modified;
		
	}
	
	/*
	*	Get an array of file last modified information
	*/
	function getFileLastModified () {
	
		return $this->last_modified;
		
	}
	
	/*
	*	Get an array of import status
	*/
	function getIsImportDir () {
	
		return $this->import_dir;
		
	}
	
	/*
	*	Get the path of the parent directory
	*/
	function getParentDir () {
	
		return $this->parent_dir;
		
	}
	
	/*
	*	Find out whether we are able to go into the parent directory (boolean)
	*/
	function getParentAccessible () {
	
		return $this->parent_access;
		
	}
	
	/*
	*	Get the path of the current directory
	*/
	function getCurrentDir () {
	
		return $this->current_dir;
		
	}
	
	/*
	*	Get the path of the current directory
	*/
	function getNewFile () {
	
		return $this->new_file;
		
	}
	
	/*
	*	Get the width of the comp image
	*/
	function getImageCompWidth () {
	
		return $this->image_comp_width;
		
	}
	
	/*
	*	Get the height of the comp image
	*/
	function getImageCompHeight () {
	
		return $this->image_comp_height;
		
	}
	
	/*
	*	Get the width of the thumbnail image
	*/
	function getImageThumbWidth () {
	
		return $this->image_thumb_width;
		
	}
	
	/*
	*	Get the height of the thumbnail image
	*/
	function getImageThumbHeight () {
	
		return $this->image_thumb_height;
		
	}
	
	/*
	*	Get the base64 encoded path of the image
	*/
	function getFilePath () {
	
		return $this->base64_file_path;
		
	}
	
	/*
	*	Get the file preview flags
	*/
	function getFilePreview () {
	
		return $this->file_preview;
		
	}
	
	/*
	*	Get the file preview flags
	*/
	function getImageProperties ($path) {
		
		$image_data = @getimagesize($path);
		
		$imageX 	= $image_data[0];
		$imageY 	= $image_data[1];
		
		if ($imageX <= 160 || $imageY <= 160) {
		
			$thumbX = $imageX;
			$thumbY = $imageY;
		
		} else {
		
			if ($imageX >= $imageY) {
			
				$thumbX = 160;
				$thumbY = (int)(($thumbX*$imageY) / $imageX );
				
			} else {
			
				$thumbY = 160;
				$thumbX = (int)(($thumbY*$imageX) / $imageY );
				
			}
		
		}
		
		$compX = $imageX;
		$compY = $imageY;
		
		return array($thumbX,$thumbY,$compX,$compY);
		
	}
	
	/*
	*	Get the type of the file
	*/
	function getFileType ($path) {
		
		$info = pathinfo($path);
		
		if (is_dir($path)) {
			
			return array("Directory","folder");
		
		} else {
		
			switch (strtolower($info['extension'])) {
			
				case 'gif':
					return array("GIF image","image-x-generic","gif");
				break;
			
				case 'jpg': case 'jpeg':
					return array("JPEG image","image-x-generic","jpg");
				break;
			
				case 'tif':
					return array("TIFF image","image-x-generic","tif");
				break;
			
				case 'png':
					return array("PNG image","image-x-generic","png");
				break;
			
				case 'dat':
					return array("Data file","text-x-generic","dat");
				break;
			
				case 'db':
					return array("Database file","text-x-generic","db");
				break;
			
				case 'log':
					return array("Log file","text-x-generic","log");
				break;
			
				case 'php':
					return array("PHP file","text-x-generic","php");
				break;
			
				case 'inc':
					return array("PHP include file","text-x-generic","inc");
				break;
			
				case 'DS_Store':
					return array("Mac OS X Finder data file","text-x-generic","dsstore");
				break;
			
				case 'xml':
					return array("XML data file","text-x-generic","xml");
				break;
			
				default:
					return array("File","text-x-generic","txt");
				break;
			
			}
		
		}
	
	}
	
	/*
	*
	*	Tells us how long ago a file was modified
	*	
	*/
	function lastModified($date) {
	
		$now = time();
		
		$diff = $now - $date;
		
		$hour = 60 * 60;
		
		$day = $hour * 24;
		
		$week = $day * 7;
		
		$month = $day * 28;
		
		$year = $day * 365.5;
		
		if ($diff < $hour) {
		
			$date_ago = round($diff / 60);
			
			if ($date_ago == 1): 	$date_text = " minute ago";
			else: 					$date_text = " minutes ago";
			endif;
			
		} elseif ($diff < $day) {
		
			$date_ago = round($diff / $hour);
			
			if ($date_ago == 60):	$date_ago  = 1; endif;
			if ($date_ago == 60):	$date_text = " hour ago"; endif;
			
			if ($date_ago == 1): 	$date_text = " hour ago";
			else: 					$date_text = " hours ago";
			endif;
			
		} elseif ($diff < $week) {
		
			$date_ago = round($diff / $day);
			
			if ($date_ago == 7):	$date_ago  = 1; endif;
			if ($date_ago == 7):	$date_text = " week ago"; endif;
			
			if ($date_ago == 1): 	$date_text = " day ago";
			else: 					$date_text = " days ago";
			endif;
			
		} elseif ($diff < $month) {
		
			$date_ago = round($diff / $week);
			
			if ($date_ago == 4):	$date_ago  = 1; endif;
			if ($date_ago == 4):	$date_text = " month ago"; endif;
			
			if ($date_ago == 1): 	$date_text = " week ago";
			else: 					$date_text = " weeks ago";
			endif;
			
		} elseif ($diff < $year) {
		
			$date_ago = round($diff / $month);
			
			if ($date_ago == 12):	$date_ago  = 1; endif;
			if ($date_ago == 12):	$date_text = " year ago"; endif;
			
			if ($date_ago == 1): 	$date_text = " month ago";
			else: 					$date_text = " months ago";
			endif;
			
		} else {
		
			$date_ago = round($diff / $year);
			
			if ($date_ago == 1): 	$date_text = " year ago";
			else: 					$date_text = " years ago";
			endif;
			
		}
		
		return $date_ago . $date_text;
		
	}
	
	/*
	*
	*	Tells us whether a directory can be imported
	*	
	*/
	function isImportDir($path) {
		
		$icon_dir		= $path . "/32x32";
		$small_dir		= $path . "/80x80";
		$large_dir		= $path . "/160x160";
		$comp_dir		= $path . "/".COMPING_DIR;
		$original_dir	= $path . "/original";
		
		if (file_exists($comp_dir)) {
		
			$file_count = countDirectoryItems($comp_dir);
			
			if (is_dir($icon_dir) && countDirectoryItems($icon_dir) != $file_count) {
			
				return FALSE;
			
			} elseif (is_dir($small_dir) && countDirectoryItems($small_dir) != $file_count) {
			
				return FALSE;
			
			} elseif (is_dir($large_dir) && countDirectoryItems($large_dir) != $file_count) {
			
				return FALSE;
			
			} else {
			
				return TRUE;
			
			}
			
		} else {
		
			return FALSE;
			
		}
	
	}

}

?>