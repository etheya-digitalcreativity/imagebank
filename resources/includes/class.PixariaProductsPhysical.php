<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

/*
*	
*	A class to handle working with physical product options
*	
*	Exposes the following methods:
*	
*/

class PixariaProductsPhysical {
	
	// Private variables
	var $_dbl;
	var $_update_flag = false;
	
	// Public variables
	var $prod_id			= array();
	var $prod_active		= array();
	var $prod_type			= array();
	var $prod_name			= array();
	var $prod_description	= array();
	var $prod_price			= array();
	var $products_physical;
	
	// Error log for malformed data
	var $error 		= false;
	var $error_log 	= array();
	
	/*
	*	
	*	This is the class constructor for the PixariaProductsPhysical class
	*	
	*	PixariaProductsPhysical -- Load data for all physical product options
	*	
	*	class PixariaProductsPhysical()
	*
	*/
	
	function PixariaProductsPhysical() {
		
		// Localise globals
		global $ses, $cfg;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		$rules	= $this->_dbl->sqlSelectRows("SELECT * FROM ".PIX_TABLE_PROD." WHERE prod_type != 'IND' AND prod_active = '1' ORDER BY prod_name ASC");
		
		if (is_array($rules)) {
		
			foreach ($rules as $key => $value) {
			
				$this->prod_id[]			= $value['prod_id'];
				$this->prod_type[]			= $value['prod_type'];
				$this->prod_name[]			= $value['prod_name'];
				$this->prod_description[]	= $value['prod_description'];
				$this->prod_price[]			= $value['prod_price'];
				$this->prod_dnl_size[]		= $value['prod_dnl_size'];
								
			}
			
			$this->products_physical = true;
			
		} else {
		
			return;
			
		}
		
	}
	
	/*
	*	Return the prod_id
	*/
	function getProductId () {
	
		return $this->prod_id;
	
	}		
		
	/*
	*	Return the prod_type
	*/
	function getProductType () {
	
		return $this->prod_type;
	
	}		
		
	/*
	*	Return the prod_name
	*/
	function getProductName () {
	
		return $this->prod_name;
	
	}		
		
	/*
	*	Return the prod_description
	*/
	function getProductDescription () {
	
		return $this->prod_description;
	
	}		
		
	/*
	*	Return the prod_price
	*/
	function getProductPrice () {
	
		return $this->prod_price;
	
	}		
		
	/*
	*	Return the prod_dnl_size
	*/
	function getDownloadSize () {
	
		return $this->prod_dnl_size;
	
	}		
		
	/*
	*	Return the products_physical
	*/
	function getProductsPhysical () {
	
		return $this->products_physical;
	
	}
		
}


?>