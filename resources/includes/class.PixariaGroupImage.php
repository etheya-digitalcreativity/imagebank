<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

/*
*	
*	A class to handle working with categories
*	
*	Exposes the following methods:
*	
*	public	getGroupListTitles()		Get an array of all the groups in the system
*	public	getGroupListIds()			A mapped list of all the groups ids to match the list of group titles
*	
*/

class PixariaGroupImage {
	
	// Private variables
	var $_dbl;
	var $_update_flag 				= false;
	
	// Public variables
	var $group_list_titles;
	var $group_list_ids;
	var $image_link_group;
	
	function PixariaGroupImage($image_id = "") {
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		// Load an array of nested categories for the category listbox
		$result = $this->_dbl->sqlSelectRows("SELECT * FROM ".PIX_TABLE_GRPS." ORDER BY group_name ASC");
		
		if (is_array($result)) {
			
			$group_id = array();
			$group_name	= array();
			
			foreach ($result as $key => $value) {
			
				$group_id[] 	= $value['group_id'];
				$group_name[]	= $value['group_name'];
			
			}
		
		}
	
		$group_info[] = $group_name;
		$group_info[] = $group_id;
		
		// Set properties for the list all categories and their IDs
		$this->group_list_titles 	= $group_info[0];
		$this->group_list_ids 		= $group_info[1];
			
		if (isset($image_id)) {
		
			// Load list of users who can view this gallery
			$viewers_result = $this->_dbl->sqlSelectRows("SELECT * FROM ".PIX_TABLE_IMVI." WHERE image_id ='".$this->_dbl->escape($image_id)."'");
			
			// Clean the list of viewers and sort into an array
			if (count($viewers_result) > 0 && is_array($viewers_result)) {
				foreach($viewers_result as $key => $value) {
					$viewers[] = $viewers_result[$key]['group_id'];
				}
			}
			
			// Get information on which items in the group list should be selected
			if (is_array($viewers)) {
			
				// Check which groups are selected for this gallery
				foreach ($group_info[1] as $key => $value) {
				
					if (in_array($value,$viewers)) { $viewer_id[] = "$value"; } else { $viewer_id[] = ""; }
				
				}
			
			}
			
			$this->image_link_group = $viewer_id;
		
		}
		
	}

	/*
	*	Return the group_list_titles
	*/
	function getGroupListTitles () {
	
		return $this->group_list_titles;
	
	}
	
	/*
	*	Return the group_list_ids
	*/
	function getGroupListIds () {
	
		return $this->group_list_ids;
	
	}
	
	/*
	*	Return the image_link_group
	*/
	function getImageGroupLinks () {
	
		return $this->image_link_group;
	
	}
	
}


?>