<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

/*
*	
*	A class to handle working with the shopping cart
*	
*	Exposes the following methods:
*	
*/

/*
*	
*	Define the different cart status types
*	
*/

define ("PIX_CART_ACTIVE",1);
define ("PIX_CART_AWAIT_QUOTE","");
define ("PIX_CART_AWAIT_ACCEPTANCE",2);
define ("PIX_CART_AWAIT_PAYMENT",3);
define ("PIX_CART_AWAIT_SHIPPING","");
define ("PIX_CART_COMPLETE",4);

class PixariaCart {
	
	// Private variables
	var $_dbl;
	var $_update_flag = false;
	
	// Public variables
	var $cart_id;
	var $cart_store_type;
	var $cart_status;
	var $cart_user_name;
	var $cart_user_email;
	var $cart_userid;
	var $cart_subtotal;
	var $cart_tax;
	var $cart_tax_total;
	var $cart_shipping;
	var $cart_total;
	var $cart_transaction_id;
	var $cart_remote_txn_id;
	var $cart_paid;
	var $cart_payment_method;
	var $cart_usage_text;
	var $cart_transaction_info;
	var $cart_date_created;
	var $cart_date_processed;
	var $cart_date_completed;
	var $cart_message_id;
	var $cart_message_username;
	var $cart_message_user_email;
	var $cart_message_text;
	var $cart_message_time;
	var $cart_message_on = false;
	
	// Error log for malformed data
	var $error 		= false;
	var $error_log 	= array();
	
	/*
	*	
	*	This is the class constructor for the PixariaCart class
	*	
	*	PixariaCart -- Load data for a shopping cart
	*	
	*	class PixariaCart([int cart_id])
	*
	*/
	
	function PixariaCart($cart_id = "") {
		
		// Localise globals
		global $ses, $cfg;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		// Get the user ID
		$user_id = $ses['psg_userid'];
		
		if (is_numeric($cart_id)) { // There is a cart_id to view
			
			$sql = "SELECT *,
											
					CONCAT(".PIX_TABLE_USER.".first_name,' ',".PIX_TABLE_USER.".family_name) AS user_name,
					
					".PIX_TABLE_USER.".email_address AS user_email
					
					FROM ". PIX_TABLE_CART ."
					
					LEFT JOIN ". PIX_TABLE_USER ." ON ". PIX_TABLE_USER .".userid = ". PIX_TABLE_CART .".userid
					
					WHERE cart_id = '$cart_id'";
			
			$cart_info 	= $this->_dbl->sqlSelectRow($sql);
			
		} else { // There is no cart_id to view
			
			// Find whether this user has a cart yet
			$cart_count = $this->_dbl->sqlCountRows("SELECT COUNT(userid) FROM ". PIX_TABLE_CART ." WHERE userid = '$user_id' AND status = '". PIX_CART_ACTIVE ."'");
		
			if ($cart_count > 0) { // Load user's current active cart
				
				$cart_info 	= $this->_dbl->sqlSelectRow("SELECT * FROM ". PIX_TABLE_CART ." WHERE userid = '$user_id' AND status = '". PIX_CART_ACTIVE ."'");
				
			} else { // There is no cart, so create one
			
				// If there is no cart id specified, try to create one
				$this->createCart ($user_id);
				return;
			
			}
		}
		
		if (is_array($cart_info)) {
			
			// Get a list of all the messages associated with this cart
			$sql = "SELECT ".PIX_TABLE_TMSG.".*, ".PIX_TABLE_USER.".email_address AS message_user_email,
			
					CONCAT(".PIX_TABLE_USER.".first_name,' ',".PIX_TABLE_USER.".family_name) AS message_username FROM ".PIX_TABLE_TMSG."
			
					LEFT JOIN ".PIX_TABLE_USER." ON ".PIX_TABLE_USER.".userid = ".PIX_TABLE_TMSG.".message_userid
					
					WHERE ".PIX_TABLE_TMSG.".message_cart_id = '$cart_id'
					
					ORDER BY ".PIX_TABLE_TMSG.".message_time DESC";
			
			$messages = $this->_dbl->sqlSelectRows($sql);
			
			$this->cart_id					= $cart_info['cart_id'];
			$this->cart_store_type			= $cart_info['store_type'];
			$this->cart_status				= $cart_info['status'];
			$this->cart_userid				= $cart_info['userid'];
			$this->cart_user_name			= $cart_info['user_name'];
			$this->cart_user_email			= $cart_info['user_email'];
			$this->cart_subtotal			= $cart_info['subtotal'];
			$this->cart_tax					= $cart_info['tax'];
			$this->cart_tax_total			= $cart_info['tax_total'];
			$this->cart_shipping			= $cart_info['shipping'];
			$this->cart_total				= $cart_info['total'];
			$this->cart_transaction_id		= $cart_info['transaction_id'];
			$this->cart_remote_txn_id		= $cart_info['remote_txn_id'];
			$this->cart_paid				= $cart_info['paid'];
			$this->cart_payment_method		= $cart_info['method'];
			$this->cart_usage_text			= $cart_info['usage_text'];
			$this->cart_transaction_info	= $cart_info['transaction_info'];
			$this->cart_date_created		= $cart_info['date_created'];
			$this->cart_date_processed		= $cart_info['date_processed'];
			$this->cart_date_completed		= $cart_info['date_completed'];
			
			if (is_array($messages)) {
			
				foreach ($messages as $value) {
				
					$this->cart_message_id[]			= $value['message_id'];
					$this->cart_message_username[]		= $value['message_username'];
					$this->cart_message_user_email[]	= $value['message_user_email'];
					$this->cart_message_text[]			= $value['message_text'];
					$this->cart_message_time[]			= $value['message_time'];
					$this->cart_message_on				= (bool)true;
				
				}
			
			}
			
		} else {
		
			return;
		
		}
		
	}
	
	/*
	*	Return the cart_id
	*/
	function getCartId () {
	
		return $this->cart_id;
	
	}
		
	/*
	*	Return the cart_store_type
	*/
	function getStoreType () {
	
		return $this->cart_store_type;
	
	}
		
	/*
	*	Return the cart_status
	*
	*	1 = In progress
	*	2 = Awaits a quotation response
	*	3 = Awaits payment by the buyer
	*	4 = Transaction is paid for and complete
	*	
	*/
	function getStatus () {
	
		return $this->cart_status;
	
	}
		
	/*
	*	Return the cart_userid
	*/
	function getUserId () {
	
		return $this->cart_userid;
	
	}
		
	/*
	*	Return the cart_user_name
	*/
	function getUserName () {
	
		return $this->cart_user_name;
	
	}
		
	/*
	*	Return the cart_user_email
	*/
	function getUserEmail () {
	
		return $this->cart_user_email;
	
	}
		
	/*
	*	Return the cart_subtotal
	*/
	function getSubTotal () {
	
		return $this->cart_subtotal;
	
	}
		
	/*
	*	Return the cart_tax
	*/
	function getTax () {
	
		return $this->cart_tax;
	
	}
		
	/*
	*	Return the cart_tax_total
	*/
	function getTaxTotal () {
	
		return $this->cart_tax_total;
	
	}
		
	/*
	*	Return the cart_shipping
	*/
	function getShipping () {
	
		return $this->cart_shipping;
	
	}
		
	/*
	*	Return the cart_total
	*/
	function getTotal () {
	
		return $this->cart_total;
	
	}
		
	/*
	*	Return the cart_transaction_id
	*/
	function getTransactionId () {
	
		return $this->cart_transaction_id;
	
	}
		
	/*
	*	Return the cart_remote_txn_id
	*/
	function getRemoteTransactionId () {
	
		return $this->cart_remote_txn_id;
	
	}
		
	/*
	*	Return the cart_paid
	*/
	function getPaid () {
	
		return $this->cart_paid;
	
	}
		
	/*
	*	Return the cart_payment_method
	*	
	*	10 = PayPal
	*	11 = Cheque
	*	12 = Postal Order
	*	13 = Money Order
	*	14 = Bank Transfer
	*	15 = 2Checkout
	*	
	*/
	function getPaymentMethod () {
	
		return $this->cart_payment_method;
	
	}
		
	/*
	*	Return the cart_usage_text
	*/
	function getUsageText () {
	
		return $this->cart_usage_text;
	
	}
		
	/*
	*	Return the cart_transaction_info
	*/
	function getTransactionInfo () {
	
		return $this->cart_transaction_info;
	
	}
		
	/*
	*	Return the cart_date_created
	*/
	function getDateCreated () {
	
		return $this->cart_date_created;
	
	}
		
	/*
	*	Return the cart_date_processed
	*/
	function getDateProcessed () {
	
		return $this->cart_date_processed;
	
	}
		
	/*
	*	Return the cart_date_completed
	*/
	function getDateCompleted () {
	
		return $this->cart_date_completed;
	
	}
	
	/*
	*	Get the message ID array for this cart
	*/
	function getMessagesId() {
	
		if (is_array($this->cart_message_id)) {
		
			return $this->cart_message_id;
			
		}
	
	}
	
	/*
	*	Get the message user name array for this cart
	*/
	function getMessagesUserName() {
	
		if (is_array($this->cart_message_username)) {
		
			return $this->cart_message_username;
			
		}
	
	}
	
	/*
	*	Get the message email array for this cart
	*/
	function getMessagesEmail() {
	
		if (is_array($this->cart_message_user_email)) {
		
			return $this->cart_message_user_email;
			
		}
	
	}
	
	/*
	*	Get the message text array for this cart
	*/
	function getMessagesText() {
	
		if (is_array($this->cart_message_text)) {
		
			return $this->cart_message_text;
			
		}
	
	}
	
	/*
	*	Get the message time array for this cart
	*/
	function getMessagesTime() {
	
		if (is_array($this->cart_message_time)) {
		
			return $this->cart_message_time;
			
		}
	
	}
	
	/*
	*	Get the message status for this cart
	*/
	function getMessagesOn() {
	
		if ($this->cart_message_on) {
		
			return $this->cart_message_on;
			
		}
	
	}
	
	/*
	*	Set the value of cart_store_type
	*/
	function setStoreType ($cart_store_type) {
	
		if ($cart_store_type == "10" || $cart_store_type == "11" || $cart_store_type == "12") {
		
			$this->cart_store_type 	= $cart_store_type;
			$this->_update_flag = (bool)true;
		
		} else {
			
			$this->error		= (bool)true;
			$this->error_log[] 	= "The selected store type is not valid for this version of Pixaria.";
		
		}
	
	}
	
	/*
	*	Set the value of cart_cart_status
	*/
	function setStatus ($cart_status) {
	
		if (is_numeric($cart_status)) {
		
			$this->cart_status 	= $cart_status;
			$this->_update_flag = (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_userid
	*/
	function setUserId ($cart_userid) {
	
		if ($cart_userid != "") {
		
			$this->cart_userid 	= $cart_userid;
			$this->_update_flag = (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_subtotal
	*/
	function setSubTotal ($cart_subtotal) {
	
		if (is_numeric($cart_subtotal)) {
		
			$this->cart_subtotal 	= $this->significantFigure($cart_subtotal);
			$this->_update_flag 	= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_tax
	*/
	function setTax ($cart_tax) {
	
		if (is_numeric($cart_tax)) {
		
			$this->cart_tax 	= $cart_tax;
			$this->_update_flag = (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_tax_total
	*/
	function setTaxTotal ($cart_tax_total) {
	
		if (is_numeric($cart_tax_total)) {
		
			$this->cart_tax_total 	= $this->significantFigure($cart_tax_total);
			$this->_update_flag 	= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_shipping
	*/
	function setShipping ($cart_shipping) {
		
		global $cfg;
		
		if (is_numeric($cart_shipping)) {
		
			$this->cart_shipping 	= $this->significantFigure($cart_shipping);
			$this->_update_flag 	= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_total
	*/
	function setTotal ($cart_total) {
	
		if (is_numeric($cart_total)) {
		
			$this->cart_total 	= $this->significantFigure($cart_total);
			$this->_update_flag = (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_remote_txn_id
	*/
	function setRemoteTransactionId ($cart_remote_txn_id) {
	
		if ($cart_remote_txn_id != "") {
		
			$this->cart_remote_txn_id 	= $cart_remote_txn_id;
			$this->_update_flag 		= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_paid
	*/
	function setPaid ($cart_paid) {
	
		if ($cart_paid != "") {
		
			$this->cart_paid 	= $cart_paid;
			$this->_update_flag = (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_payment_method
	*	
	*	10 = PayPal
	*	11 = Cheque
	*	12 = Postal Order
	*	13 = Money Order
	*	14 = Bank Transfer
	*	
	*/
	function setPaymentMethod ($cart_payment_method) {
	
		if ($cart_payment_method != "") {
		
			$this->cart_payment_method 	= $cart_payment_method;
			$this->_update_flag 		= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_usage_text
	*/
	function setUsageText ($cart_usage_text) {
	
		if ($cart_usage_text != "") {
		
			$this->cart_usage_text 	= $cart_usage_text;
			$this->_update_flag 	= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_transaction_info
	*/
	function setTransactionInfo ($cart_transaction_info) {
	
		if ($cart_transaction_info != "") {
		
			$this->cart_transaction_info 	= $cart_transaction_info;
			$this->_update_flag 			= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_date_processed
	*/
	function setDateProcessed ($cart_date_processed) {
	
		if ($cart_date_processed == "NOW") {
			
			$this->cart_date_processed 	= date("Y-m-d H:i:s");
			$this->_update_flag 		= (bool)true;
		
		} elseif ($cart_date_processed != "") {
		
			$this->cart_date_processed 	= $cart_date_processed;
			$this->_update_flag 		= (bool)true;
		
		} else {
		
			return;
		
		}
	
	}
	
	/*
	*	Set the value of cart_date_completed
	*/
	function setDateCompleted ($cart_date_completed) {
	
		if ($cart_date_completed == "NOW") {
			
			$this->cart_date_completed 	= date("Y-m-d H:i:s");
			$this->_update_flag 		= (bool)true;
		
		} elseif ($cart_date_completed != "") {
		
			$this->cart_date_completed 	= $cart_date_completed;
			$this->_update_flag 		= (bool)true;
		
		} else {
		
			return;
		
		}
	
	}
	
	/*
	*	
	*	Create a new empty cart for a user
	*	
	*	bool createCart(int userid)
	*	
	*/
	
	function createCart ($userid) {
		
		global $cfg;
		
		// If there are no properties to insert don't run the insert command
		if (!is_numeric($userid)) {
			return false;
		}
		
		$cart_tax = $cfg['set']['store_default_tax'];
		
		// If there isn't a shopping cart create one for the user
		$cart_count = $this->_dbl->sqlCountRows("SELECT COUNT(userid) FROM ". PIX_TABLE_CART ." WHERE userid = '$userid' AND status = '". PIX_CART_ACTIVE ."'");
		
		if ($cart_count == 0) {
			
			// Generate a 128 bit hexadecimal random number for the transaction id
			$transaction_id = strtoupper(md5(uniqid(rand(), true)));
			
			// Get the store operation mode from the settings
			$store_type		= $cfg['set']['store_type'];
		
			// Create a new empty cart for this user
			$this->_dbl->sqlQuery("INSERT INTO ". PIX_TABLE_CART ." VALUES (	
																			'0',
																			'$store_type',
																			'". PIX_CART_ACTIVE ."',
																			'$userid',
																			'',
																			'$cart_tax',
																			'',
																			'',
																			'',
																			'$transaction_id',
																			'',
																			'',
																			'',
																			'',
																			'',
																			NOW(),
																			'',
																			''
																			)");
			// Get the new cart ID
			$this->cart_id = $this->_dbl->sqlInsertId();
			
			// Reinitialise the class
			$this->PixariaCart();
		
		}

	}
	
	/*
	*	
	*	Update the details of a single product overwriting blank entries
	*	
	*	bool updateCart(void)
	*	
	*/
	
	function updateCart () {
		
		// If there are no properties to change or there is no cart ID, don't run the update
		if (!$this->_update_flag || $this->cart_id == "" || $this->error) {
			return false;
		}
		
		// Load cart properties for inclusion in the database
		$cart_id				= $this->cart_id;
		$cart_store_type		= $this->cart_store_type;
		$cart_status			= $this->cart_status;
		$cart_subtotal			= $this->cart_subtotal;
		$cart_tax				= $this->cart_tax;
		$cart_tax_total			= $this->cart_tax_total;
		$cart_shipping			= $this->cart_shipping;
		$cart_total				= $this->cart_total;
		$cart_remote_txn_id		= $this->cart_remote_txn_id;
		$cart_paid				= $this->cart_paid;
		$cart_payment_method	= $this->cart_payment_method;
		$cart_usage_text		= $this->cart_usage_text;
		$cart_transaction_info	= $this->cart_transaction_info;
		$cart_date_processed	= $this->cart_date_processed;
		$cart_date_completed	= $this->cart_date_completed;
		
		// Construct a SQL statement for updating this image
		$sql = "UPDATE ". PIX_TABLE_CART ." SET 	
								
											store_type			= '$cart_store_type',
											status				= '$cart_status',
											subtotal			= '$cart_subtotal',
											tax					= '$cart_tax',
											tax_total			= '$cart_tax_total',
											shipping			= '$cart_shipping',
											total				= '$cart_total',
											remote_txn_id		= '$cart_remote_txn_id',
											method				= '$cart_payment_method',
											usage_text			= '$cart_usage_text',
											transaction_info	= '$cart_transaction_info',
											date_processed		= '$cart_date_processed',
											date_completed		= '$cart_date_completed'
					
											WHERE cart_id = '$cart_id' LIMIT 1;";

		
		// Pass the SQL command to the database object
		return $this->_dbl->sqlQuery($sql);

	}
	
	/*
	*	
	*	Delete a cart from the system
	*	
	*	bool deleteCart(void)
	*	
	*/
	
	function deleteCart () {
	
		// If there is no cart_id don't do anything
		if ($this->cart_id == "") {
		
			return false;
		
		} else {
		
			// Get the product ID
			$cart_id = $this->cart_id;
			
			// Create SQL command to delete this product
			$sql = "DELETE FROM ". PIX_TABLE_CART ." WHERE cart_id = '$cart_id' LIMIT 1";
			
			// Pass the SQL command to the database object
			$this->_dbl->sqlQuery($sql);
			
			// Delete items in the cart
			$sql = "DELETE FROM ". PIX_TABLE_CRME ." WHERE cart_id = '$cart_id'";
			
			// Pass the SQL command to the database object
			$this->_dbl->sqlQuery($sql);
			
			// Delete messages associated with this cart
			$sql = "DELETE FROM ". PIX_TABLE_TMSG ." WHERE message_cart_id = '$cart_id'";
			
			// Pass the SQL command to the database object
			$this->_dbl->sqlQuery($sql);
		
		}
			
	}
	
	/*
	*	
	*	Empty the contents of a cart
	*	
	*	bool emptyCart(void)
	*	
	*/
	
	function emptyCart () {
		
		// If there is no cart_id don't do anything
		if ($this->cart_id == "" && is_int($this->cart_userid)) {
		
			return false;
		
		} else {
		
			// Delete the old cart for this user
			$this->deleteCart ($this->cart_id);
			
			// Create a new cart for this user
			$this->createCart ($this->cart_userid);
			
		}
			
	}
	
	/*
	*	
	*	Add a message to a cart
	*	
	*	bool createCartMessage(void)
	*	
	*/
	
	function createCartMessage ($sMessage) {
		
		global $cfg, $ses;
		
		// If there is no cart_id don't do anything
		if ($this->cart_id == "") {
		
			return false;
		
		} else {
		
			// Get the cart ID and user ID
			$iCartId = $this->cart_id;
			$iUserId = $ses['psg_userid'];
			
			$sql = "INSERT INTO ".PIX_TABLE_TMSG." VALUES ('0','$sMessage',NOW(),'$iUserId','$iCartId','')";
		
			// Pass the SQL command to the database object
			return $this->_dbl->sqlQuery($sql);
		
		}
			
	}
	
	/*
	*
	* 	Convert a numeric decimal string into a two sig fig decimal string
	*	e.g. 120.0 -> 120.00
	*
	*/
	
	function significantFigure($var) {
	
		$var = round($var,2);
		
		list ($base,$decimal) = split('[/.-]', $var);
		
		$newvar = $base . "." . str_pad($decimal, 2, "0", STR_PAD_RIGHT);
		
		return($newvar);
	
	}
	
}


?>