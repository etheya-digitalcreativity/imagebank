<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

/*
*	
*	A class to handle working with categories
*	
*	Exposes the following methods:
*	
*	public	getCategoryListTitles()		Get an array of all the categories in the system, nested with indenting to indicate hierarchy
*	public	getCategoryListIds()		A mapped list of all the category ids to match the list of category titles
*	
*/

class PixariaCategoryImage {
	
	// Private variables
	var $_dbl;
	var $_update_flag 				= false;
	
	// Public variables
	var $category_list_titles;
	var $category_list_ids;
	var $image_link_categories;
	
	// Private properties
	var $category_parents;
	var $category_name;
	var $category_id;
	var $category_array;
	var $indent;
	var $subcategory_array;
	
	
	function PixariaCategoryImage($image_id = "") {
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		// Load an array of nested categories for the category listbox
		$category_list = $this->getCategoryNestedList();
		
		// Set properties for the list all categories and their IDs
		$this->category_list_titles = $category_list[0];
		$this->category_list_ids 	= $category_list[1];
		
		if (isset($image_id)) {

			// Initialise the arrays
			$categories			= array();
			$image_category_id	= array();
			
			// Load list of categories that this image is in
			$categories_result = $this->_dbl->sqlSelectRows("SELECT * FROM ".PIX_TABLE_CAME." WHERE image_id ='".$this->_dbl->escape($image_id)."'");
			
			// Clean the list of viewers and sort into an array
			if (count($categories_result) > 0 && is_array($categories_result)) {
				foreach($categories_result as $key => $value) {
					$categories[] = $categories_result[$key]['category_id'];
				}
			}
			
			// Get information about which items in the category list should be selected
			if (is_array($categories)) {
			
				// Check which groups are selected for this gallery
				foreach ($this->category_list_ids as $key => $value) {
				
					if (in_array($value,$categories)) { $image_category_id[] = "$value"; } else { $image_category_id[] = ""; }
				
				}
				
				// Set the property for the whole object
				$this->image_link_categories = $image_category_id;
				
			}
		
		}
			
	}

	/*
	*	Return the category_list_titles
	*/
	function getCategoryListTitles () {
	
		return $this->category_list_titles;
	
	}
	
	/*
	*	Return the category_list_ids
	*/
	function getCategoryListIds () {
	
		return $this->category_list_ids;
	
	}
	
	/*
	*	Return the image_link_categories
	*/
	function getImageCategoryLinks () {
	
		return $this->image_link_categories;
	
	}
	
	/*
	*
	*	This function creates an array of nested categories
	*
	*/
	function getCategoryNestedArray($id) {
	
		global $cfg;
	
		$this->category_array[]	= $this->indent.$this->category_name[$id];
		
		$this->category_id[]	= $id;
		
		if(isset($this->category_parents[$id]) && is_array($this->category_parents[$id])) {
		
		
			foreach($this->category_parents[$id] as $kind) {
				$this->indent .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				$this->getCategoryNestedArray($kind);
			}
			
		}
		$this->indent = substr($this->indent,0,-36);
	
		
	}
	
	
	
	/*
	*
	*	This function returns a two dimensional array of nested category names
	*	and the id number of each category.  Output is sorted alphabetically.
	*
	*/
	function getCategoryNestedList() {
	
		global $cfg;
		
		$this->category_array		= array();
		$this->category_id		= array();
		
		$result = $this->_dbl->sqlSelectRows("SELECT * FROM ".PIX_TABLE_CATG);
		
		if (is_array($result) && count($result) > 0) {
			
			foreach ($result as $key => $value) {
			
				$this->category_parents[$value["category_parent"]][] 	= $value["category_id"];
				$this->category_name[$value["category_id"]]			= $value["category_title"];
			
			}
			
			ksort($this->category_parents);
			
			$parents	= $this->_dbl->sqlSelectRows("SELECT * FROM ".PIX_TABLE_CATG." WHERE category_parent = '0' ORDER by category_title ASC");
			
			foreach ($parents as $key => $value) {
			
				$this->getCategoryNestedArray($value['category_id']);
			
			}
			
		}
		
		$category_info[] = $this->category_array;
		$category_info[] = $this->category_id;
		
		return $category_info;
	
	}
	
	/*
	*
	*	Load an array with the category_ids of all subcategories of a category with category_id of $category_id
	*
	*/
	
	function getSubCategoryList($category_id) {
	
		global $cfg;
		
		$subcategory_array = array();
		
		$this->getSubCategoryId($category_id);
			
		return $subcategory_array;
	
	}
	
	/*
	*
	*	Load an array with the category_ids of all subcategories of a category with category_id of $category_id
	*
	*/
	
	function getSubCategoryId($id) {
		
		global $cfg;
		
		$result = $this->_dbl->sqlSelectRows("SELECT category_id FROM ".PIX_TABLE_CATG." WHERE category_parent = '".$this->_dbl->escape($id)."'");
		
		if (is_array($result)) {
			
			foreach($result as $key => $value) {
				
				$this->subcategory_array[] = $value['category_id'];
				
				$this->getSubCategoryId($value['category_id']);
			
			}
			
		}
	
	}
	
}


?>