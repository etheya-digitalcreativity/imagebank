<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

/*
*	
*	A class to handle working with the cart and items in it
*	
*	Exposes the following methods:
*	
*/

// Load the shopping cart class
require_once ('class.PixariaCart.php');
		
class PixariaCartObjects extends PixariaCart {
	
	/* Inherited variables from parent
	*
	
	// Private variables
	var $_dbl;
	var $_update_flag = false;
	
	// PixariaCart variables from parent class
	var $cart_id;
	var $cart_store_type;
	var $cart_status;
	var $cart_userid;
	var $cart_subtotal;
	var $cart_tax;
	var $cart_tax_total;
	var $cart_shipping;
	var $cart_total;
	var $cart_transaction_id;
	var $cart_remote_txn_id;
	var $cart_paid;
	var $cart_payment_method;
	var $cart_usage_text;
	var $cart_transaction_info;
	var $cart_date_created;
	var $cart_date_processed;
	var $cart_date_completed;
	
	// Error log for malformed data
	var $error 		= false;
	var $error_log 	= array();
	
	*
	*/
	
	// Public variables from this class
	var $cart_items;
	var $cart_contents;
	var $cart_item_id;
	var $cart_item_type;
	var $cart_item_type_text;
	var $cart_item_image_comp_path;
	var $cart_item_image_comp_size;
	var $cart_item_image_icon_path;
	var $cart_item_image_icon_dims;
	var $cart_item_image_small_path;
	var $cart_item_image_small_dims;
	var $cart_item_image_large_path;
	var $cart_item_image_large_dims;
	var $cart_item_calculator_href;
	var $cart_item_image_id;
	var $cart_item_image_path;
	var $cart_item_usage_text;
	var $cart_item_photographer_userid;
	var $cart_item_price;
	var $cart_item_shipping;
	var $cart_item_shipping_multiple;
	var $cart_item_quantity;
	var $cart_item_image_filename;
	var $cart_item_image_basename;
	var $cart_item_image_extension;
	var $cart_item_image_title;
	var $cart_item_quote_text;
	var $cart_item_download_counter;
	var $cart_item_download_link;
	var $cart_item_download_jpg;
	var $cart_item_download_jp2;
	var $cart_item_download_tif;
	var $cart_item_download_psd;
	var $cart_item_download_remaining;
	var $cart_quote_complete;
	var $cart_price_complete;
	
	var $calculated_subtotal;
	var $calculated_total;
	var $download_link;
	var $download_remaining;
	
	/*
	*	
	*	This is the class constructor for the PixariaCart class
	*	
	*	PixariaCartObjects -- Load data for a shopping cart
	*	
	*	class PixariaCartObjects([int cart_id])
	*
	*/
	
	function PixariaCartObjects($cart_id = "") {
		
		// Localise globals
		global $cfg, $ses;
		
		// Initialise the Pixaria Cart class
		$this->PixariaCart($cart_id);
		
		// Get the cart id
		$cart_id = $this->cart_id;
		
		if ($ses['psg_administrator']) { // If the user is an administrator we don't need to check that the  user ID matches cart ID
			
			$sql = "SELECT ".PIX_TABLE_CRME.".*, ".PIX_TABLE_IMGS.".image_userid AS photographer_userid FROM ".PIX_TABLE_CRME."
					
					LEFT JOIN ".PIX_TABLE_IMGS." ON ".PIX_TABLE_CRME.".image_id = ".PIX_TABLE_IMGS.".image_id
					
					WHERE ".PIX_TABLE_CRME.".cart_id = '$cart_id'";
			
			$cart_items = $this->_dbl->sqlSelectRows($sql);
		
		} else { // The user is not an administrator so we need to check that the  user ID matches cart ID
		
			$sql = "SELECT ".PIX_TABLE_CRME.".*, ".PIX_TABLE_IMGS.".image_userid AS photographer_userid FROM ".PIX_TABLE_CRME."
					
					LEFT JOIN ".PIX_TABLE_IMGS." ON ".PIX_TABLE_CRME.".image_id = ".PIX_TABLE_IMGS.".image_id
					
					WHERE ".PIX_TABLE_CRME.".cart_id = '$cart_id' AND ".PIX_TABLE_CRME.".userid = '" . $ses['psg_userid'] . "'";
			
			$cart_items = $this->_dbl->sqlSelectRows($sql);
		
		}
		
		if (is_array($cart_items)) {
			
			// This cart has items in it
			$this->cart_contents = true;
			
			$cart_item_type 				= array();
			$cart_item_image_id 			= array();
			$cart_item_image_path 			= array();
			$cart_item_usage_text 			= array();
			$cart_item_photographer_userid 	= array();
			$cart_item_price 				= array();
			$cart_item_shipping 			= array();
			$cart_item_shipping_multiple	= array();
			$cart_item_quantity 			= array();
			$cart_item_image_filename 		= array();
			$cart_item_image_title			= array();
			$cart_item_quote_text			= array();
			$cart_item_download_counter		= array();
			$cart_item_image_icon_dims		= array();
			$cart_item_image_small_dims		= array();
			$cart_item_image_large_dims 	= array();
			
			// This value allows us to check that all required information is present in an advanced quote
			$quote_complete	= (bool)TRUE;
			$price_complete	= (bool)TRUE;
			
			$calculated_subtotal = 0;
			
			foreach ($cart_items as $key => $value) {
				
				$icon_path 		= $cfg['sys']['base_library'].$value['image_path']."/32x32/".$value['image_filename'];
				$small_path 	= $cfg['sys']['base_library'].$value['image_path']."/80x80/".$value['image_filename'];
				$large_path 	= $cfg['sys']['base_library'].$value['image_path']."/160x160/".$value['image_filename'];
				$comp_path 		= $cfg['sys']['base_library'].$value['image_path']."/".COMPING_DIR."/".$value['image_filename'];
				$original_path 	= $cfg['sys']['base_library'].$value['image_path']."/original/".$value['image_filename'];
				$orig_jp2_path 	= $cfg['sys']['base_library'].$value['image_path']."/original_JP2/".$value['image_filename'];
				$orig_tif_path 	= $cfg['sys']['base_library'].$value['image_path']."/original_TIF/".$value['image_filename'];
				$orig_psd_path 	= $cfg['sys']['base_library'].$value['image_path']."/original_PSD/".$value['image_filename'];
								
				if (file_exists($icon_path) && file_exists($comp_path)) {
				
					$cart_item_image_comp_path[]	= base64_encode($comp_path);
					$cart_item_image_comp_size[] 	= getimagesize($comp_path);
									
				} else {
				
					$icon_path 			= $cfg['sys']['base_path']."resources/themes/".$cfg['set']['theme']."/images/32x32_missing.jpg";
					$small_path 		= $cfg['sys']['base_path']."resources/themes/".$cfg['set']['theme']."/images/80x80_missing.jpg";
					$large_path 		= $cfg['sys']['base_path']."resources/themes/".$cfg['set']['theme']."/images/160x160_missing.jpg";
									
					$cart_item_image_comp_path[]	= (bool)FALSE;
					$cart_item_image_comp_size[] 	= (bool)FALSE;
									
				}
				
				if (($value['cart_download_counter'] < $cfg['set']['store_download_attempts']) && $cfg['set']['func_store_download'] == 1) {
					
					$download_link[]		= (bool)TRUE;
					$download_remaining[]	= $cfg['set']['store_download_attempts'] - $value['cart_download_counter'];
					
				} else {
				
					$download_link[]		= (bool)FALSE;
					$download_remaining[]	= 0;
				
				}
				
				$cart_item_image_icon_path[]	= base64_encode($icon_path);
				$cart_item_image_icon_size 		= getimagesize($icon_path);
				$cart_item_image_icon_dims[]	= $image_icon_size[3];					
				
				$cart_item_image_small_path[]	= base64_encode($small_path);
				$cart_item_image_small_size 	= getimagesize($small_path);
				$cart_item_image_small_dims[]	= $image_small_size[3];					
				
				$cart_item_image_large_path[]	= base64_encode($large_path);
				$cart_item_image_large_size 	= getimagesize($large_path);
				$cart_item_image_large_dims[]	= $image_large_size[3];					
				
				if ($cfg['set']['cart_href'] == "popup") {
				
					$cart_item_calculator_href[]	= "javascript:openPop('".$cfg['sys']['base_url'].$cfg['fil']['index_store']."?cmd=recalculateOptionsPopup&amp;cart_item_id=".$value['id']."', '".$cfg['set']['popup_cart_width']."', '".$cfg['set']['popup_cart_height']."', 'yes', 'yes', 'no', 'no', 'yes', 'no', 'no');";
				
				} else {
				
					$cart_item_calculator_href[]	= "".$cfg['sys']['base_url'].$cfg['fil']['index_store']."?cmd=recalculateOptionsInline&amp;cart_item_id=".$value['id']."";
				
				}
				
				$cart_item_id[]						= $value['id'];
				$cart_item_type[]					= $value['item_type'];
				$cart_item_image_id[]				= $value['image_id'];
				$cart_item_image_path[]				= $value['image_path'];
				$cart_item_usage_text[]				= $value['usage_text'];
				$cart_item_photographer_userid[]	= $value['photographer_userid'];
				$cart_item_price[]					= $value['price'];
				$cart_item_shipping[]				= $value['shipping'];
				$cart_item_shipping_multiple[]		= $value['shipping_multiple'];
				$cart_item_quantity[]				= $value['quantity'];
				$cart_item_image_filename[]			= $value['image_filename'];
				$cart_item_image_title[]			= $value['image_title'];
				$cart_item_quote_text[]				= $value['quote_text'];
				$cart_item_download_counter[]		= $value['cart_download_counter'];
				$cart_item_prod_code[]				= $value['item_prod_code'];
				$cart_item_dnl_size[]				= $value['item_dnl_size'];
				
				$download_jpg[] = $this->getDownloadURL($value['image_path'], $value['image_filename'], "JPG", $value['image_id'], $value['id'],$value['item_prod_code'],$value['item_dnl_size']);
				$download_jp2[] = $this->getDownloadURL($value['image_path'], $value['image_filename'], "JP2", $value['image_id'], $value['id'],$value['item_prod_code'],$value['item_dnl_size']);
				$download_tif[] = $this->getDownloadURL($value['image_path'], $value['image_filename'], "TIF", $value['image_id'], $value['id'],$value['item_prod_code'],$value['item_dnl_size']);
				$download_psd[] = $this->getDownloadURL($value['image_path'], $value['image_filename'], "PSD", $value['image_id'], $value['id'],$value['item_prod_code'],$value['item_dnl_size']);
								
				// Update the calculated subtotal
				$calculated_subtotal = $calculated_subtotal + $value['price'];
				
				// Establish whether all items have quote text
				if ($value['usage_text'] == "") {
					$quote_complete	= (bool)FALSE;
				}
				
				// Establish whether all items have a price set for them
				if ($value['price'] == 0) {
					$price_complete	= (bool)FALSE;
				}
				
			}
			
			$this->calculated_subtotal = $this->significantFigure($calculated_subtotal);
			if ($this->cart_subtotal == 0) {
				$this->setSubTotal($this->calculated_subtotal);
			}
			
			$this->calculated_tax_total = $this->significantFigure(($this->cart_subtotal / 100) * $this->cart_tax);
			if ($this->cart_tax_total == 0) {
				$this->setTaxTotal($this->calculated_tax_total);
			}
			
			// Initialise array to hold cart items
			$cart_items = array();
			
			$cart_items['cart_item_id']				= $cart_item_id;
			$this->cart_item_id						= $cart_item_id;
			
			$cart_items['cart_item_type']			= $cart_item_type;
			$this->cart_item_type					= $cart_item_type;
			
			$cart_items['image_comp_path']			= $cart_item_image_comp_path;
			$this->cart_item_image_comp_path		= $cart_item_image_comp_path;
			
			$cart_items['image_comp_size']			= $cart_item_image_comp_size;
			$this->cart_item_image_comp_size		= $cart_item_image_comp_size;
			
			$cart_items['image_icon_path']			= $cart_item_image_icon_path;
			$this->cart_item_image_icon_path		= $cart_item_image_icon_path;
			
			$cart_items['image_icon_dims']			= $cart_item_image_icon_dims;
			$this->cart_item_image_icon_dims		= $cart_item_image_icon_dims;
			
			$cart_items['image_small_path']			= $cart_item_image_small_path;
			$this->cart_item_image_small_path		= $cart_item_image_small_path;
			
			$cart_items['image_small_dims']			= $cart_item_image_small_dims;
			$this->cart_item_image_small_dims		= $cart_item_image_small_dims;
			
			$cart_items['image_large_path']			= $cart_item_image_large_path;
			$this->cart_item_image_large_path		= $cart_item_image_large_path;
			
			$cart_items['image_large_dims']			= $cart_item_image_large_dims;
			$this->cart_item_image_large_dims		= $cart_item_image_large_dims;
			
			$cart_items['calculator_href']			= $cart_item_calculator_href;
			$this->cart_item_calculator_href		= $cart_item_calculator_href;
			
			$cart_items['image_id'] 				= $cart_item_image_id;
			$this->cart_item_image_id				= $cart_item_image_id;
			
			$cart_items['image_path'] 				= $cart_item_image_path;
			$this->cart_item_image_path				= $cart_item_image_path;
			
			$cart_items['usage_text'] 				= $cart_item_usage_text;
			$this->cart_item_usage_text				= $cart_item_usage_text;
			
			$cart_items['photographer_userid'] 		= $cart_item_photographer_userid;
			$this->cart_item_photographer_userid	= $cart_item_photographer_userid;
			
			$cart_items['price'] 					= $cart_item_price;
			$this->cart_item_price					= $cart_item_price;
			
			$cart_items['shipping'] 				= $cart_item_shipping;
			$this->cart_item_shipping				= $cart_item_shipping;
			
			$cart_items['shipping_multiple'] 		= $cart_item_shipping_multiple;
			$this->cart_item_shipping_multiple		= $cart_item_shipping_multiple;
			
			$cart_items['quantity'] 				= $cart_item_quantity;
			$this->cart_item_quantity				= $cart_item_quantity;
			
			$cart_items['image_filename'] 			= $cart_item_image_filename;
			$this->cart_item_image_filename			= $cart_item_image_filename;
			
			$cart_items['image_title'] 				= $cart_item_image_title;
			$this->cart_item_image_title			= $cart_item_image_title;
			
			$cart_items['quote_text'] 				= $cart_item_quote_text;
			$this->cart_item_quote_text				= $cart_item_quote_text;
			
			$cart_items['download_counter'] 		= $cart_item_download_counter;
			$this->cart_item_download_counter		= $cart_item_download_counter;
			
			$cart_items['item_prod_code'] 			= $cart_item_prod_code;
			$this->cart_item_prod_code				= $cart_item_prod_code;
			
			$cart_items['item_dnl_size'] 			= $cart_item_dnl_size;
			$this->cart_item_dnl_size				= $cart_item_dnl_size;
			
			$cart_items['quote_complete'] 			= $quote_complete;
			$this->cart_quote_complete				= $quote_complete;
			
			$cart_items['price_complete'] 			= $price_complete;
			$this->cart_price_complete				= $price_complete;
			
			$cart_items['download_link'] 			= $download_link;
			$this->cart_item_download_link			= $download_link;
			
			$cart_items['download_jpg'] 			= $download_jpg;
			$this->cart_item_download_jpg			= $download_jpg;
			
			$cart_items['download_jp2'] 			= $download_jp2;
			$this->cart_item_download_jp2			= $download_jp2;
			
			$cart_items['download_tif'] 			= $download_tif;
			$this->cart_item_download_tif			= $download_tif;
			
			$cart_items['download_psd'] 			= $download_psd;
			$this->cart_item_download_psd			= $download_psd;
			
			$cart_items['download_remaining'] 		= $download_remaining;
			$this->cart_item_download_remaining		= $download_remaining;
			
			// Get any shipping (if necessary)
			$shipping	= $cfg['set']['store_default_ship_val'];
			
			// Update cart object with shipping information
			if ($this->cart_shipping > 0) {
			
				// Calculate the grand total
				$this->setTotal($this->cart_subtotal + $this->cart_tax_total + $this->cart_shipping);
				
			} elseif ($cfg['set']['store_default_ship'] == 1) {
			
				// Assign shipping value to the cart object
				$this->setShipping($shipping);
				
				// Calculate the grand total
				$this->setTotal($this->cart_subtotal + $this->cart_tax_total + $this->cart_shipping);
				
			} else {
			
				// Assign shipping value to the cart object
				$this->setShipping("0.00");
				
				// Calculate the grand total
				$this->setTotal($this->cart_subtotal + $this->cart_tax_total);
				
			}
						
			if (is_array($this->cart_item_type)) {
				
				foreach($this->cart_item_type as $key => $value) {
				
					switch ($value) {
					
						case digital:
						
							$this->cart_item_type_text[] = "Digital image";
						
						break;
						
						case physical:
							
							if ($this->cart_item_prod_code[$key] == "DNL") {
								$this->cart_item_type_text[] = "Instant image download";
							} else {
								$this->cart_item_type_text[] = "Product derived from an image";
							}
							
						break;
					
					}
				
				}
			
			}
			
		} else {
		
			return false;
		
		}

	}
	
	function getDownloadURL ($directory, $filename, $mime_type, $image_id, $cart_item_id, $prod_code, $dnl_size) {
		
		global $cfg, $ses, $objEnvData, $smarty;
		
		$filename = substr($filename, 0, -4);
		
		if ($prod_code == "DNL") {
		
			if ($dnl_size == "comping") {
			
				$subdir = COMPING_DIR;
				
			} else {
			
				$subdir = "original";
				
			}
		
		} else {
		
			$subdir = "original";
		
		}
		
		$directory = $cfg['sys']['base_library'] . $directory . "/" . $subdir;
		
		switch ($mime_type) {
		
			case "JPG":
				
				if (file_exists("$directory/$filename.jpg")) {
					$url = $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=downloadImage&amp;cart_item_id=$cart_item_id&amp;cart_id=$this->cart_id&amp;extension=jpg";
				} elseif (file_exists("$directory/$filename.JPG")) {
					$url = $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=downloadImage&amp;cart_item_id=$cart_item_id&amp;cart_id=$this->cart_id&amp;extension=JPG";
				} else {
					$url = (bool)FALSE;
				}
				
			break;
		
			case "JP2":
				
				if (file_exists("$directory/$filename.jp2")) {
					$url = $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=downloadImage&amp;cart_item_id=$cart_item_id&amp;cart_id=$this->cart_id&amp;extension=jp2";
				} elseif (file_exists("$directory/$filename.JP2")) {
					$url = $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=downloadImage&amp;cart_item_id=$cart_item_id&amp;cart_id=$this->cart_id&amp;extension=JP2";
				} else {
					$url = (bool)FALSE;
				}
				
			break;
		
			case "TIF":
				
				if (file_exists("$directory/$filename.tif")) {
					$url = $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=downloadImage&amp;cart_item_id=$cart_item_id&amp;cart_id=$this->cart_id&amp;extension=tif";
				} elseif (file_exists("$directory/$filename.TIF")) {
					$url = $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=downloadImage&amp;cart_item_id=$cart_item_id&amp;cart_id=$this->cart_id&amp;extension=TIF";
				} else {
					$url = (bool)FALSE;
				}
				
			break;
		
			case "PSD":
				
				if (file_exists("$directory/$filename.psd")) {
					$url = $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=downloadImage&amp;cart_item_id=$cart_item_id&amp;cart_id=$this->cart_id&amp;extension=psd";
				} elseif (file_exists("$directory/$filename.PSD")) {
					$url = $cfg['sys']['base_url'] . $cfg['fil']['index_store'] . "?cmd=downloadImage&amp;cart_item_id=$cart_item_id&amp;cart_id=$this->cart_id&amp;extension=PSD";
				} else {
					$url = (bool)FALSE;
				}
				
			break;
		
		}
		
		return $url;
		
	}
	
	/*
	*	
	*	getCartItemType ()
	*	
	*	Returns the type of cart item 
	*	
	*/
	function getCartItemType() {

		return $this->cart_item_type;

	}
	
	/*
	*	
	*	getCartItemTypeText ()
	*	
	*	Returns the type of cart item 
	*	
	*/
	function getCartItemTypeText() {

		return $this->cart_item_type_text;

	}
	
	/*
	*	
	*	getDownloadLink ()
	*	
	*	Returns the array of download URLs 
	*	
	*/
	function getDownloadLink() {

		return $this->cart_item_download_link;

	}
	
	/*
	*	
	*	getItemDownloadJPG ()
	*	
	*	Returns the array of JPEG download URLs 
	*	
	*/
	function getItemDownloadJPG() {

		return $this->cart_item_download_jpg;

	}
	
	/*
	*	
	*	getItemDownloadJP2 ()
	*	
	*	Returns the array of JPEG download URLs 
	*	
	*/
	function getItemDownloadJP2() {

		return $this->cart_item_download_jp2;

	}
	
	/*
	*	
	*	getItemDownloadTIF ()
	*	
	*	Returns the array of TIFF download URLs 
	*	
	*/
	function getItemDownloadTIF() {

		return $this->cart_item_download_tif;

	}
	
	/*
	*	
	*	getDownloadPSD ()
	*	
	*	Returns the array of PSD download URLs 
	*	
	*/
	function getItemDownloadPSD() {

		return $this->cart_item_download_psd;

	}
	
	/*
	*	
	*	getDownloadRemaining ()
	*	
	*	Returns the array of remaining downloads
	*	
	*/
	function getDownloadRemaining() {

		return $this->cart_item_download_remaining;

	}
	
	/*
	*	
	*	getCartItemShipping ()
	*	
	*	Returns the cart item shipping unit price
	*	
	*/
	function getCartItemShipping() {

		return $this->cart_item_shipping;

	}
	
	/*
	*	
	*	getCartItemShippingMultiple ()
	*	
	*	Returns the cart item shipping price for each subsequent item after the first
	*	
	*/
	function getCartItemShippingMultiple() {

		return $this->cart_item_shipping_multiple;

	}
	
	/*
	*	
	*	getCartItemDownloadSize ()
	*	
	*/
	function getCartItemDownloadSize() {

		return $this->cart_item_download_size;

	}
	
	/*
	*	
	*	getCartItemProductCode ()
	*	
	*/
	function getCartItemProductCode() {

		return $this->cart_item_prod_code;

	}
	
	/*
	*	
	*	getCartItemQuantity ()
	*	
	*	Returns the quantity of items required
	*	
	*/
	function getCartItemQuantity() {

		return $this->cart_item_quantity;

	}
	
	/*
	*	
	*	getCartItemPhotographerUserId ()
	*	
	*	Returns the userid of the photographer who owns this image
	*	
	*/
	function getCartItemPhotographerUserId() {

		return $this->cart_item_photographer_userid;

	}
	
	/*
	*	
	*	getCartItemPhotographerUserDetails ()
	*	
	*	Returns the name and e-mail of the photographer as an array
	*	
	*/
	function getCartItemPhotographerUserDetails() {
		
		if (is_array($this->cart_item_photographer_userid)) {
			
			foreach ($this->cart_item_photographer_userid as $key => $value) {
				
				$user_details[] = getUserContactDetails($value);
				
			}
			
		}
		
		return $user_details;

	}
	
}


?>