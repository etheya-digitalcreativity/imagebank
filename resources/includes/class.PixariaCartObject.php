<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

/*
*	
*	A class to handle working with the cart and items in it
*	
*	Exposes the following methods:
*	
*/

// Load the shopping cart class
require_once ('class.PixariaCart.php');
		
class PixariaCartObject extends PixariaCart {
	
	/* Inherited variables from parent
	*
	
	// Private variables
	var $_dbl;
	var $_update_flag = false;
	
	// PixariaCart variables from parent class
	var $cart_id;
	var $cart_store_type;
	var $cart_status;
	var $cart_userid;
	var $cart_subtotal;
	var $cart_tax;
	var $cart_tax_total;
	var $cart_shipping;
	var $cart_total;
	var $cart_transaction_id;
	var $cart_remote_txn_id;
	var $cart_paid;
	var $cart_payment_method;
	var $cart_usage_text;
	var $cart_transaction_info;
	var $cart_date_created;
	var $cart_date_processed;
	var $cart_date_completed;
	
	// Error log for malformed data
	var $error 		= false;
	var $error_log 	= array();
	
	*
	*/
	
	// Private variables
	var $_dbl;

	// Public variables from this class
	var $cart_items;
	var $cart_item_id;
	var $cart_item_cart_id;
	var $cart_item_type;
	var $cart_item_image_id;
	var $cart_item_image_path;
	var $cart_item_usage_text;
	var $cart_item_price_original;
	var $cart_item_price;
	var $cart_item_shipping;
	var $cart_item_shipping_multiple;
	var $cart_item_quantity_original;
	var $cart_item_quantity;
	var $cart_item_image_filename;
	var $cart_item_image_title;
	var $cart_item_quote_text;
	var $cart_item_download_counter;
	var $cart_item_prod_code;
	var $cart_item_dnl_size;
	var $cart_quote_complete;
	var $cart_price_complete;
	
	var $calculated_subtotal;
	var $calculated_total;
	
	/*
	*	
	*	This is the class constructor for the PixariaCartObject class
	*	
	*	PixariaCartObject -- Load data for a shopping cart item
	*	
	*	class PixariaCartObject([int cart_id])
	*
	*/
	
	function PixariaCartObject($cart_item_id = "") {
		
		// Localise globals
		global $cfg, $ses;
		
		if (is_numeric($cart_item_id)) { // There is a cart_id to view
		
			// Create the database object
			$this->_dbl = new Database();
		
			$cart_item_info = $this->_dbl->sqlSelectRow("SELECT * FROM ". PIX_TABLE_CRME ." WHERE id = '$cart_item_id'");
			
			$this->cart_item_id					= $cart_item_info['id'];
			$this->cart_item_cart_id			= $cart_item_info['cart_id'];
			$this->cart_item_type				= $cart_item_info['item_type'];
			$this->cart_item_user_id			= $cart_item_info['userid'];
			$this->cart_item_image_id			= $cart_item_info['image_id'];
			$this->cart_item_image_path 		= $cart_item_info['image_path'];
			$this->cart_item_usage_text 		= $cart_item_info['usage_text'];
			$this->cart_item_price				= $cart_item_info['price'];
			$this->cart_item_price_original		= $cart_item_info['price'];
			$this->cart_item_shipping			= $cart_item_info['shipping'];
			$this->cart_item_shipping_multiple	= $cart_item_info['shipping_multiple'];
			$this->cart_item_quantity_original	= $cart_item_info['quantity'];
			$this->cart_item_quantity			= $cart_item_info['quantity'];
			$this->cart_item_image_filename		= $cart_item_info['image_filename'];
			$this->cart_item_image_title		= $cart_item_info['image_title'];
			$this->cart_item_quote_text			= $cart_item_info['quote_text'];
			$this->cart_item_download_counter	= $cart_item_info['cart_download_counter'];
			$this->cart_item_prod_code			= $cart_item_info['item_prod_code'];
			$this->cart_item_dnl_size			= $cart_item_info['item_dnl_size'];
			
			$file_info = pathinfo($cfg['sys']['base_library'].$this->cart_item_image_path."/".COMPING_DIR."/".$this->cart_item_image_filename);
			
			$this->cart_item_image_extension	= $file_info['extension'];
			$this->cart_item_image_basename		= substr($file_info['basename'], 0, -(strlen($file_info['extension']) + ($file_info['extension'] == '' ? 0 : 1)));

			// Initialise the Pixaria Cart class with the cart_id of the item we're looking at
			$this->PixariaCart($cart_item_info['cart_id']);
		
		} else {
		
			// Initialise the Pixaria Cart class with the current user's cart_id
			$this->PixariaCart();
		
		}
		
	}
	
	/*
	*	Get the value of cart_item_image_id
	*/
	function getImageId () {
	
			return $this->cart_item_image_id;
	
	}
	
	/*
	*	Get the value of cart_item_image_path
	*/
	function getImagePath () {
	
			return $this->cart_item_image_path;
	
	}
	
	/*
	*	Get the value of cart_item_image_filename
	*/
	function getImageFileName () {
	
			return $this->cart_item_image_filename;
	
	}
	
	/*
	*	Get the value of cart_item_price
	*/
	function getImagePrice () {
	
			return $this->cart_item_price;
	
	}
	
	/*
	*	Get the value of cart_item_type
	*/
	function getItemType () {
	
			return $this->cart_item_type;
	
	}
	
	/*
	*	Get the value of cart_item_shipping
	*/
	function getItemShipping () {
	
			return $this->cart_item_shipping;
	
	}
	
	/*
	*	Get the value of cart_item_shipping_multiple
	*/
	function getItemShippingMultiple () {
	
			return $this->cart_item_shipping_multiple;
	
	}
	
	/*
	*	Get the value of cart_item_quantity
	*/
	function getItemQuantity () {
	
			return $this->cart_item_quantity;
	
	}
	
	/*
	*	Get the value of cart_item_download_counter
	*/
	function getItemDownloadCount () {
	
			return $this->cart_item_download_counter;
	
	}
	
	/*
	*	Get the value of cart_item_prod_code
	*/
	function getItemProductCode () {
	
			return $this->cart_item_prod_code;
	
	}
	
	/*
	*	Get the value of cart_item_dnl_size
	*/
	function getItemDownloadSize () {
	
			return $this->cart_item_dnl_size;
	
	}
	
	/*
	*	
	*	getImageBaseName ()
	*	
	*	Returns the quantity of items required
	*	
	*/
	function getImageBaseName() {

		return $this->cart_item_image_basename;

	}
	
	/*
	*	
	*	getImageExtension ()
	*	
	*	Returns the quantity of items required
	*	
	*/
	function getImageExtension() {

		return $this->cart_item_image_extension;

	}
	
	/*
	*	Set the value of cart_item_type
	*/
	function setItemType ($cart_item_type) {
	
		if ($cart_item_type == "digital") {
		
			$this->cart_item_type 	= "digital";
			$this->_update_flag 	= (bool)true;
		
		} elseif ($cart_item_type == "physical") {
		
			$this->cart_item_type 	= "physical";
			$this->_update_flag 	= (bool)true;
		
		} else {
		
			return;
		
		}
	
	}
	
	/*
	*	Set the value of cart_item_image_id
	*/
	function setImageId ($cart_item_image_id) {
	
		if ($cart_item_image_id != "") {
		
			$this->cart_item_image_id = $cart_item_image_id;
			$this->_update_flag 	= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_item_image_path
	*/
	function setImagePath ($cart_item_image_path) {
	
		if ($cart_item_image_path != "") {
		
			$this->cart_item_image_path = $cart_item_image_path;
			$this->_update_flag 		= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_item_usage_text
	*/
	function setUsageText ($cart_item_usage_text) {
	
		if ($cart_item_usage_text != "") {
		
			$this->cart_item_usage_text = $cart_item_usage_text;
			$this->_update_flag 		= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_item_price
	*/
	function setPrice ($cart_item_price) {
	
		if ($cart_item_price != "") {
		
			$this->cart_item_price 	= $cart_item_price;
			$this->_update_flag 	= (bool)true;
		
		} else {
		
			$this->cart_item_price 	= "0";
			$this->_update_flag 	= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_item_shipping
	*/
	function setCartShipping ($cart_item_shipping) {
		
		global $cfg;
		
		if ($cart_item_shipping != "" && !$cfg['set']['store_flatrate_ship']) {
		
			$this->cart_item_shipping 	= $cart_item_shipping;
			$this->_update_flag 		= (bool)true;
		
		} else {
		
			$this->cart_item_shipping 	= "0.00";
			$this->_update_flag 		= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_item_shipping_multiple
	*/
	function setCartShippingMultiple ($cart_item_shipping_multiple) {
	
		global $cfg;
		
		if ($cart_item_shipping_multiple != "" && !$cfg['set']['store_flatrate_ship']) {
		
			$this->cart_item_shipping_multiple 	= $cart_item_shipping_multiple;
			$this->_update_flag 				= (bool)true;
		
		} else {
		
			$this->cart_item_shipping_multiple 	= "0.00";
			$this->_update_flag 				= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_item_quantity
	*/
	function setCartQuantity ($cart_item_quantity) {
	
		if ($cart_item_quantity != "") {
		
			$this->cart_item_quantity 	= $cart_item_quantity;
			$this->_update_flag 		= (bool)true;
		
		} else {
		
			$this->cart_item_quantity 	= "1";
			$this->_update_flag 		= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_item_image_filename
	*/
	function setImageFileName ($cart_item_image_filename) {
	
		if ($cart_item_image_filename != "") {
		
			$this->cart_item_image_filename = $cart_item_image_filename;
			$this->_update_flag 			= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_item_image_title
	*/
	function setImageTitle ($cart_item_image_title) {
	
		if ($cart_item_image_title != "") {
		
			$this->cart_item_image_title = $cart_item_image_title;
			$this->_update_flag 		= (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of cart_item_quote_text
	*/
	function setQuoteText ($cart_item_quote_text) {
	
		if ($cart_item_quote_text != "") {
		
			$this->cart_item_quote_text = $cart_item_quote_text;
			$this->_update_flag 		= (bool)true;
		
		}
	
	}
		
	/*
	*	Set the value of cart_download_counter
	*/
	function setCartItemDownloadCounter ($cart_item_download_counter) {
	
		$this->cart_item_download_counter 	= $cart_item_download_counter;
		$this->_update_flag 				= (bool)true;
	
	}
		
	/*
	*	Set the value of cart_item_prod_code
	*/
	function setCartProductCode ($cart_item_prod_code) {
	
		$this->cart_item_prod_code 	= $cart_item_prod_code;
		$this->_update_flag 		= (bool)true;
	
	}
		
	/*
	*	Set the value of cart_item_dnl_size
	*/
	function setCartDownloadSize ($cart_item_dnl_size) {
	
		$this->cart_item_dnl_size 	= $cart_item_dnl_size;
		$this->_update_flag 		= (bool)true;
	
	}
		
	/*
	*	
	*	Put a new item into the shopping cart
	*	
	*	bool createCartObject()
	*	
	*/
	
	function createCartObject () {
		
		global $cfg;
		
		// If there are no properties to insert don't run the insert command
		if (!$this->_update_flag || $this->cart_id == "" || $this->error) {
			return false;
		}
		
		if ($this->cart_item_quantity == 0) {
			$this->setCartQuantity(1);
		}
		
		// Get internal variables
		$cart_id						= $this->cart_id;
		$cart_item_type					= $this->cart_item_type;
		$cart_userid 					= $this->cart_userid;
		$cart_item_image_id				= $this->cart_item_image_id;
		$cart_item_image_path			= $this->cart_item_image_path;
		$cart_item_usage_text			= $this->cart_item_usage_text;
		$cart_item_price				= $this->cart_item_price;
		$cart_item_shipping				= $this->cart_item_shipping;
		$cart_item_shipping_multiple	= $this->cart_item_shipping_multiple;
		$cart_item_quantity				= $this->cart_item_quantity;
		$cart_item_image_filename		= $this->cart_item_image_filename;
		$cart_item_image_title			= $this->cart_item_image_title;
		$cart_item_quote_text			= $this->cart_item_quote_text;
		$cart_item_prod_code			= $this->cart_item_prod_code;
		$cart_item_dnl_size				= $this->cart_item_dnl_size;
		
		// Add a new item to this cart
		$sql = "INSERT INTO ". PIX_TABLE_CRME ." VALUES (	
															'0',
															'".mysql_real_escape_string($cart_id)."',
															'".mysql_real_escape_string($cart_item_type)."',
															'".mysql_real_escape_string($cart_userid)."',
															'".mysql_real_escape_string($cart_item_image_id)."',
															'".mysql_real_escape_string($cart_item_image_path)."',
															'".mysql_real_escape_string($cart_item_usage_text)."',
															'".mysql_real_escape_string($cart_item_price)."',
															'".mysql_real_escape_string($cart_item_shipping)."',
															'".mysql_real_escape_string($cart_item_shipping_multiple)."',
															'".mysql_real_escape_string($cart_item_quantity)."',
															'".mysql_real_escape_string($cart_item_image_filename)."',
															'".mysql_real_escape_string($cart_item_image_title)."',
															'".mysql_real_escape_string($cart_item_quote_text)."',
															'0',
															'".mysql_real_escape_string($cart_item_prod_code)."',
															'".mysql_real_escape_string($cart_item_dnl_size)."'
														);";
		
		$this->_dbl->sqlQuery($sql);
		
		// Now we need to add the cost of this item to the cart and update it
		$current_cart_subtotal 	= $this->cart_subtotal;
		$current_cart_tax		= $this->cart_tax;
		$current_cart_tax_total	= $this->cart_tax_total;
		$current_cart_shipping	= $this->cart_shipping;
		$current_cart_total		= $this->cart_total;
		
		// Calculate the shipping
		if ($cart_item_quantity == 1) {

			// There is only one item to add
			$new_cart_shipping = $current_cart_shipping + $cart_item_shipping; 
			
		} elseif ($cart_item_quantity > 1) {
			
			// The standard shipping rate for a single item
			$rate_norm = $cart_item_shipping;
			
			// The rate for all the additional items
			$rate_redu = ($cart_item_quantity - 1) * $cart_item_shipping_multiple;
			
			// Add the two shipping values to the current value
			$new_cart_shipping = $current_cart_shipping + $rate_redu + $rate_norm;
			
		}
		
		// Calculate new amounts for the cart
		$new_cart_subtotal		= $current_cart_subtotal + ($cart_item_price * $cart_item_quantity); // Add the price of the new item to the cart subtotal
		$new_cart_tax_total		= ($new_cart_subtotal / 100) * $current_cart_tax; // Calculate the tax on this item
		$new_total				= $new_cart_subtotal + $new_cart_tax_total + $new_cart_shipping; // Calculate the new grand total
		
		// Update the cart object with the new subtotal, tax and total price information
		$this->setSubTotal($new_cart_subtotal);
		$this->setTaxTotal($new_cart_tax_total);
		
		if ($this->cart_item_type == "physical" && $cfg['set']['store_flatrate_ship']) {
		
			$this->setShipping($cfg['set']['store_flatrate_ship_val']);
		
		} else {
		
			$this->setShipping($new_cart_shipping);
		
		}
		
		$this->setTotal($new_total);
		
		// Update the cart to reflect the cost of the new item
		$this->updateCart();
		
	}
	
	/*
	*	
	*	Update an item that is already in the shopping cart
	*	
	*	bool updateCartObject()
	*	
	*/

	function updateCartObject () {
	
		global $cfg;
		
		// If there are no properties to insert don't run the insert command
		if (!$this->_update_flag || $this->cart_id == "" || $this->error) {
			return false;
		}
		
		// If the cart quantity has been set to zero, delete the item from the cart
		if ($this->cart_item_quantity == 0) {
			$this->deleteCartObject();
			return true;
		}
		
		// Get internal variables
		$cart_id						= $this->cart_id;
		$cart_item_type					= $this->cart_item_type;
		$cart_item_id					= $this->cart_item_id;
		$cart_userid 					= $this->cart_userid;
		$cart_item_image_id				= $this->cart_item_image_id;
		$cart_item_image_path			= $this->cart_item_image_path;
		$cart_item_usage_text			= $this->cart_item_usage_text;
		$cart_item_price_original		= $this->cart_item_price_original;
		$cart_item_price				= $this->cart_item_price;
		$cart_item_shipping				= $this->cart_item_shipping;
		$cart_item_shipping_multiple	= $this->cart_item_shipping_multiple;
		$cart_item_quantity_original	= $this->cart_item_quantity_original;
		$cart_item_quantity				= $this->cart_item_quantity;
		$cart_item_image_filename		= $this->cart_item_image_filename;
		$cart_item_image_title			= $this->cart_item_image_title;
		$cart_item_quote_text			= $this->cart_item_quote_text;
		$cart_item_download_counter		= $this->cart_item_download_counter;
		$cart_item_prod_code			= $this->cart_item_prod_code;
		$cart_item_dnl_size				= $this->cart_item_dnl_size;
		
		// Add a new item to this cart
		$this->_dbl->sqlQuery("UPDATE ". PIX_TABLE_CRME ." SET	image_id				= '".mysql_real_escape_string($cart_item_image_id)."',
																item_type				= '".mysql_real_escape_string($cart_item_type)."',
																image_path				= '".mysql_real_escape_string($cart_item_image_path)."',
																usage_text				= '".mysql_real_escape_string($cart_item_usage_text)."',
																price					= '".mysql_real_escape_string($cart_item_price)."',
																shipping				= '".mysql_real_escape_string($cart_item_shipping)."',
																shipping_multiple		= '".mysql_real_escape_string($cart_item_shipping_multiple)."',
																quantity				= '".mysql_real_escape_string($cart_item_quantity)."',
																image_filename			= '".mysql_real_escape_string($cart_item_image_filename)."',
																image_title				= '".mysql_real_escape_string($cart_item_image_title)."',
																quote_text				= '".mysql_real_escape_string($cart_item_quote_text)."',
																cart_download_counter	= '".mysql_real_escape_string($cart_item_download_counter)."',
																item_prod_code			= '".mysql_real_escape_string($cart_item_prod_code)."',
																item_dnl_size			= '".mysql_real_escape_string($cart_item_dnl_size)."'
																
															WHERE cart_id = '$cart_id'
															
															AND id = '$cart_item_id'
															
															LIMIT 1;");
		
		// Now we need to add the cost of this item to the cart and update it
		$current_cart_subtotal 	= $this->cart_subtotal;
		$current_cart_tax		= $this->cart_tax;
		$current_cart_tax_total	= $this->cart_tax_total;
		$current_cart_shipping	= $this->cart_shipping;
		$current_cart_total		= $this->cart_total;
		
		// Update the shipping based on the quantity of items
		if ($cart_item_quantity != $cart_item_quantity_original) {
		
			// The standard shipping rate for a single item
			$rate_norm = $cart_item_shipping;
			
			// The rate for all the additional items
			$rate_redu_old = ($cart_item_quantity_original - 1) * $cart_item_shipping_multiple;
			
			// The rate for all the additional items for the new quantity
			$rate_redu_new = ($cart_item_quantity - 1) * $cart_item_shipping_multiple;
			
			
			// Calculate the shipping for the original quantity
			if ($cart_item_quantity_original == 1) {
				
				// The shipping for the old quantity
				$shipping_item_old = $rate_norm;
				
			} elseif ($cart_item_quantity_original > 1) {
			
				// The shipping for the old quantity
				$shipping_item_old = $rate_norm + $rate_redu_old;
				
			}
			
			// Calculate the shipping for the new quantity
			if ($cart_item_quantity == 1) {
			
				$new_cart_shipping = ($current_cart_shipping - $shipping_item_old) + $rate_norm;
			
			} elseif ($cart_item_quantity > 1) {
			
				$new_cart_shipping = ($current_cart_shipping - $shipping_item_old) + $rate_norm + $rate_redu_new;
			
			}
			
			// Calculate the change in price for the new quantities
			$items_price_old = $cart_item_price * $cart_item_quantity_original;
			$items_price_new = $cart_item_price * $cart_item_quantity;
			
			$price_change = $items_price_new - $items_price_old;
			
		} else {
			
			// The quantity hasn't changes and therefore the shipping hasn't either
			$new_cart_shipping = $current_cart_shipping;
			
			// Calculate the change in price if there is any
			$price_change = $this->cart_item_price - $this->cart_item_price_original;
			
		}
		
		
		
		// Calculate new amounts for the cart
		$new_cart_subtotal		= $current_cart_subtotal + $price_change; // Add the change in price between the new item and old item to the cart subtotal
		$new_cart_tax_total		= ($new_cart_subtotal / 100) * $current_cart_tax; // Calculate the tax on this item
		$new_total				= $new_cart_subtotal + $new_cart_tax_total + $new_cart_shipping; // Calculate the new grand total
		
		// Update the cart object with the new subtotal, tax and total price information
		$this->setSubTotal($new_cart_subtotal);
		$this->setTaxTotal($new_cart_tax_total);
		$this->setShipping($new_cart_shipping);
		$this->setTotal($new_total);
		
		// Update the cart to reflect the cost of the new item
		$this->updateCart();
		
	}

	/*
	*	
	*	Delete an item from the cart
	*	
	*	bool deleteCartObject(void)
	*	
	*/
	
	function deleteCartObject () {
	
		// If there is no cart_id don't do anything
		if ($this->cart_id == "" || $this->cart_item_id == "") {
			return false;
		}
		
		// Get the cart ID
		$cart_id = $this->cart_id;
		
		// Get the cart item ID
		$cart_item_id = $this->cart_item_id;
		
		// Create SQL command to delete this product
		$sql = "DELETE FROM ". PIX_TABLE_CRME ." WHERE id = '$cart_item_id' AND cart_id = '$cart_id' LIMIT 1;";
		
		// Pass the SQL command to the database object
		$this->_dbl->sqlQuery($sql);
		
		// Now we need to add the cost of this item to the cart and update it
		$current_cart_subtotal 	= $this->cart_subtotal;
		$current_cart_tax		= $this->cart_tax;
		$current_cart_tax_total	= $this->cart_tax_total;
		$current_cart_shipping	= $this->cart_shipping;
		$current_cart_total		= $this->cart_total;
		
		// Calculate the shipping
		if ($this->cart_item_quantity == 1) {

			// There is only one item to add
			$new_cart_shipping = $current_cart_shipping - $this->cart_item_shipping; 
			
		} elseif ($this->cart_item_quantity > 1) {
			
			// The standard shipping rate for a single item
			$rate_norm = $this->cart_item_shipping;
			
			// The rate for all the additional items
			$rate_redu = ($this->cart_item_quantity - 1) * $this->cart_item_shipping_multiple;
			
			// Add the two shipping values to the current value
			$new_cart_shipping = $current_cart_shipping - $rate_redu - $rate_norm;
			
		}
		
		// Calculate new amounts for the cart
		$new_cart_subtotal		= $current_cart_subtotal - ($this->cart_item_price_original *  $this->cart_item_quantity); // Add the price of the new item to the cart subtotal
		$new_cart_tax_total		= ($new_cart_subtotal / 100) * $current_cart_tax; // Calculate the tax on this item
		$new_total				= $new_cart_subtotal + $new_cart_tax_total + $new_cart_shipping; // Calculate the new grand total
		
		// Update the cart object with the new subtotal, tax and total price information
		$this->setSubTotal($new_cart_subtotal);
		$this->setTaxTotal($new_cart_tax_total);
		$this->setShipping($new_cart_shipping);
		$this->setTotal($new_total);
		
		//print_r($this);exit;
		
		// Update the cart to reflect the cost of the new item
		$this->updateCart();
		
	}
	
	/*
	*
	* 	Calculate a price for an image based on the pricing rules
	*	
	*	Takes values:
	*	
	*	$options		(ARRAY)		Array of the options selected by the user
	*	$price			(FLOAT)		Starting price of the image
	*
	*/
	
	function calculatePrice($options,$price) {
		
		global $cfg, $ses;
		
		$sym			= $cfg['set']['store_symbol'];
		
		$old_price		= $price;
		
		if (is_array($options)) {
		
			foreach ($options as $key=> $value) {
			
				$calculation		= $this->_dbl->sqlSelectRow("SELECT * FROM ".PIX_TABLE_CAAR." WHERE ar_id = '$value';");
				
				$operand 			= $calculation['ar_operand'];
				$factor				= $calculation['ar_factor'];
				$ar_id[]			= $calculation['ar_id'];
				$ar_name[]			= $calculation['ar_name'];
				$ar_description[]	= $calculation['ar_description'];
				
				switch ($operand) {
				
					case 0:
						$arithmetic[]	= "Price = $sym$price";
						$price			= $price;
					break;
					
					case 1:
						$arithmetic[]	= "Price = $sym$price + $sym$factor";
						$price			= ($price + $factor);
					break;
					
					case 2:
						$arithmetic[]	= "Price = $sym$price - $sym$factor";
						$price			= ($price - $factor);
					break;
					
					case 3:
						$arithmetic[]	= "Price = $sym$price &times; $factor";
						$price			= ($price * $factor);
					break;
					
					case 4:
						$arithmetic[]	= "Price = $sym$price &divide; $factor";
						$price			= ($price / $factor);
					break;
					
					case 5:
						$arithmetic[]	= "Price = $sym$factor";
						$price			= $factor;
					break;
					
				}
		
			}
			
			$output['new_price']		= getSignificantFigure($price);
			$output['old_price']		= $old_price;
			$output['arithmetic']		= $arithmetic;
			$output['ar_id']			= $ar_id;
			$output['ar_name']			= $ar_name;
			$output['ar_description']	= $ar_description;
			
			return $output;
			
		} else {
	
			return false;	
		
		}
	
	}

}


?>