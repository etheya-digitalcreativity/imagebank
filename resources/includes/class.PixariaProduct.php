<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

/*
*	
*	A class to handle working with products
*	
*	Exposes the following methods:
*	
*	PixariaProduct()
*	getProdActive()
*	getProdType()
*	getProdName()
*	getProdDescription()
*	getProdPrice()
*	getProdShipping()
*	getProdShippingMultiple()
*	getProducts()
*	setProdActive()
*	setProdType()
*	setProdName()
*	setProdDescription()
*	setProdPrice()
*	setProdShipping()
*	setProdShippingMultiple()
*	createProduct()
*	updateProduct()
*	deleteProduct()
*	
*/

class PixariaProduct {
	
	// Private variables
	var $_dbl;
	var $_update_flag = false;
	
	// Public variables
	var $prod_id;
	var $prod_active;
	var $prod_type;
	var $prod_name;
	var $prod_description;
	var $prod_price;
	var $prod_shipping;
	var $prod_shipping_multiple;
	
	// Error log for malformed data
	var $error 		= false;
	var $error_log 	= array();
	
	/*
	*	
	*	This is the class constructor for the PixariaProduct class
	*	
	*	PixariaProduct -- Load data for a product
	*	
	*	class PixariaProduct([int prod_id])
	*
	*/
	
	function PixariaProduct($prod_id = "") {
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		if (is_numeric($prod_id)) {
		
			$result = $this->_dbl->sqlSelectRow("SELECT * FROM ". PIX_TABLE_PROD ." WHERE prod_id = '$prod_id'");
		
		} else {
		
			return;
		
		}
		
		if (is_array($result)) {
			
			$this->prod_id					= $result['prod_id'];
			$this->prod_active				= $result['prod_active'];
			$this->prod_type				= $result['prod_type'];
			$this->prod_name				= $result['prod_name'];
			$this->prod_description			= $result['prod_description'];
			$this->prod_price				= $result['prod_price'];
			$this->prod_shipping			= $result['prod_shipping'];
			$this->prod_shipping_multiple	= $result['prod_shipping_multiple'];
			$this->prod_dnl_size			= $result['prod_dnl_size'];
				
		} else {
		
			return;
		
		}
		
	}
	
	/*
	*	Return the prod_active
	*/
	function getProdActive () {
	
		return $this->prod_active;
	
	}
	
	/*
	*	Return the prod_type
	*/
	function getProdType () {
	
		return $this->prod_type;
	
	}
	
	/*
	*	Return the prod_name
	*/
	function getProdName () {
	
		return $this->prod_name;
	
	}
	
	/*
	*	Return the prod_description
	*/
	function getProdDescription () {
	
		return $this->prod_description;
	
	}
	
	/*
	*	Return the prod_price
	*/
	function getProdPrice () {
	
		return $this->prod_price;
	
	}
	
	/*
	*	Return the prod_shipping
	*/
	function getProdShipping () {
	
		return $this->prod_shipping;
	
	}
	
	/*
	*	Return the prod_shipping_multiple
	*/
	function getProdShippingMultiple () {
	
		return $this->prod_shipping_multiple;
	
	}
	
	/*
	*	Return the prod_dnl_size
	*/
	function getProdDownloadSize () {
	
		return $this->prod_dnl_size;
	
	}
	
	/*
	*	
	*	Get details of all the products in the database
	*	
	*	Returns an array of information for all products in the database of false on failure
	*	
	*	array getProducts([string sort_column][, string sort_order])
	*	
	*/
	
	function getProducts ($sort_column="", $sort_order="") {
	
		$sql = "SELECT * FROM ". PIX_TABLE_PROD ."";
		
		$result  = $this->_dbl->sqlSelectRows($sql);
		
		if (is_array($result)) {
			
			$prod_id 				= array();
			$prod_active 			= array();
			$prod_type 				= array();
			$prod_name 				= array();
			$prod_description 		= array();
			$prod_price				= array();
			$prod_shipping			= array();
			$prod_shipping_multiple	= array();
			
			foreach ($result as $key => $value) {
			
				$prod_id[]					= $value['prod_id'];
				$prod_active[]				= $value['prod_active'];
				$prod_type[]				= $value['prod_type'];
				$prod_name[]				= $value['prod_name'];
				$prod_description[]			= $value['prod_description'];
				$prod_price[]				= $value['prod_price'];
				$prod_shipping[]			= $value['prod_shipping'];
				$prod_shipping_multiple[]	= $value['prod_shipping_multiple'];
				$prod_dnl_size[]			= $value['prod_dnl_size'];
			
			}
			
			// Initilialise output
			$output 					= array();
			
			// Parse data into output
			$output['prod_id'] 					= $prod_id;
			$output['prod_active'] 				= $prod_active;
			$output['prod_type'] 				= $prod_type;
			$output['prod_name'] 				= $prod_name;
			$output['prod_description'] 		= $prod_description;
			$output['prod_price'] 				= $prod_price;
			$output['prod_shipping'] 			= $prod_shipping;
			$output['prod_shipping_multiple'] 	= $prod_shipping_multiple;
			$output['prod_dnl_size'] 			= $prod_dnl_size;
			
			return $output;
		
		} else {
		
			return false;
		
		}
	
	}
	
	/*
	*	Set the value of prod_active
	*/
	function setProdActive ($prod_active) {
	
		if ($prod_active == "on" || $prod_active == "1") {
		
			$this->prod_active 	= "1";
			$this->_update_flag = (bool)true;
		
		} else {
		
			$this->prod_active 	= "0";
			$this->_update_flag = (bool)true;
		
		}
	
	}
	
	/*
	*	Set the value of prod_type
	*/
	function setProdType ($prod_type) {
		
		if ($prod_type == "IMG" || $prod_type == "IND" || $prod_type == "DNL") {
			
			$this->prod_type 	= $prod_type;
			$this->_update_flag	= (bool)true;
		
		} else {
			
			$this->error		= (bool)true;
			$this->error_log[] 	= "The product type is not valid for this version of Pixaria.";
		
		}
		
	}
	
	/*
	*	Set the value of prod_name
	*/
	function setProdName ($prod_name) {
		
		if ($prod_name != "") {
			
			$this->prod_name 	= addslashes($prod_name);
			$this->_update_flag	= (bool)true;
		
		} else {
			
			$this->error		= (bool)true;
			$this->error_log[] 	= "All products on your website must have a name.";
		
		}
		
	}
	
	/*
	*	Set the value of prod_description
	*/
	function setProdDescription ($prod_description) {
	
		if ($prod_description != "") {
			
			$this->prod_description = addslashes($prod_description);
			$this->_update_flag		= (bool)true;
		
		} else {
			
			$this->error		= (bool)true;
			$this->error_log[] 	= "All products on your website must have a description.";
		
		}
	
	}
	
	/*
	*	Set the value of prod_price
	*/
	function setProdPrice ($prod_price) {
		
		if (is_numeric($prod_price)) {
		
			$this->prod_price 	= $prod_price;
			$this->_update_flag	= (bool)true;
		
		} else {
			
			$this->error		= (bool)true;
			$this->error_log[] 	= "You must provide a price for all products on your website.";
		
		}
		
	}
	
	/*
	*	Set the value of prod_shipping
	*/
	function setProdShipping ($prod_shipping) {
		
		if (is_numeric($prod_shipping)) {
		
			$this->prod_shipping 	= $prod_shipping;
			$this->_update_flag		= (bool)true;
		
		} else {
			
			$this->error			= (bool)true;
			$this->error_log[] 		= "You must provide a price for all products on your website.";
		
		}
		
	}
	
	/*
	*	Set the value of prod_shipping_multiple
	*/
	function setProdShippingMultiple ($prod_shipping_multiple) {
		
		if (is_numeric($prod_shipping_multiple)) {
		
			$this->prod_shipping_multiple 	= $prod_shipping_multiple;
			$this->_update_flag				= (bool)true;
		
		} else {
			
			$this->error					= (bool)true;
			$this->error_log[] 				= "You must provide a price for all products on your website.";
		
		}
		
	}
	
	/*
	*	Set the value of prod_shipping_multiple
	*/
	function setProdDownloadSize ($prod_dnl_size) {
		
		if ($prod_dnl_size != "") {
		
			$this->prod_dnl_size 	= $prod_dnl_size;
			$this->_update_flag		= (bool)true;
		
		}
		
	}
	
	/*
	*	
	*	Create a new product entry in the database
	*	
	*	bool createProduct(void)
	*	
	*/
	
	function createProduct () {
		
		// If there are no properties to insert don't run the insert command
		if (!$this->_update_flag || $this->error) {
			return false;
		}
		
		// Load image properties for inclusion in the database and escape dangerous items
		$prod_id				= $this->prod_id;
		$prod_active			= $this->prod_active;
		$prod_type				= $this->prod_type;
		$prod_name				= $this->prod_name;
		$prod_description		= $this->prod_description;
		$prod_price				= $this->prod_price;
		$prod_shipping			= $this->prod_shipping;
		$prod_shipping_multiple	= $this->prod_shipping_multiple;
		$prod_dnl_size			= $this->prod_dnl_size;
		
		// If the image isn't a download product, then set the download size to null
		if ($prod_type != "DNL") {
			$prod_dnl_size = "";
		}
		
		// Construct a SQL statement for updating this image
		$sql = "INSERT INTO ". PIX_TABLE_PROD ." VALUES (	
															'0',
															'$prod_active',
															'$prod_type',
															'$prod_name',
															'$prod_description',
															'$prod_price',
															'$prod_shipping',
															'$prod_shipping_multiple',
															'$prod_dnl_size'
														);";
		// Pass the SQL command to the database object
		return $this->_dbl->sqlQuery($sql);
		
	}
	
	/*
	*	
	*	Update the details of a single product overwriting blank entries
	*	
	*	bool updateProduct(void)
	*	
	*/
	
	function updateProduct () {
		
		// If there are no properties to change or there is no product ID, don't run the update
		if (!$this->_update_flag || $this->prod_id == "" || $this->error) {
			return false;
		}
		
		// Load image properties for inclusion in the database and escape dangerous items
		$prod_id				= $this->prod_id;
		$prod_active			= $this->prod_active;
		$prod_type				= $this->prod_type;
		$prod_name				= $this->prod_name;
		$prod_description		= $this->prod_description;
		$prod_price				= $this->prod_price;
		$prod_shipping			= $this->prod_shipping;
		$prod_shipping_multiple	= $this->prod_shipping_multiple;
		$prod_dnl_size			= $this->prod_dnl_size;
		
		// If the image isn't a download product, then set the download size to null
		if ($prod_type != "DNL") {
			$prod_dnl_size = "";
		}
		
		// Construct a SQL statement for updating this image
		$sql = "UPDATE ". PIX_TABLE_PROD ." SET 	
								
								prod_active				= '$prod_active',
								prod_type				= '$prod_type',
								prod_name				= '$prod_name',
								prod_description		= '$prod_description',
								prod_price				= '$prod_price',
								prod_shipping			= '$prod_shipping',
								prod_shipping_multiple	= '$prod_shipping_multiple',
								prod_dnl_size			= '$prod_dnl_size'
		
								WHERE prod_id = '$prod_id' LIMIT 1;";
		
		// Pass the SQL command to the database object
		return $this->_dbl->sqlQuery($sql);
		
	}
	
	/*
	*	
	*	Delete a product from the system
	*	
	*	bool deleteProduct(void)
	*	
	*/
	
	function deleteProduct () {
	
		// If there is no product_id don't do anything
		if ($this->prod_id == "") {
			return false;
		}
		
		// Get the product ID
		$prod_id = $this->prod_id;
		
		// Create SQL command to delete this product
		$sql = "DELETE FROM ". PIX_TABLE_PROD ." WHERE prod_id = '$prod_id' LIMIT 1";
		
		// Pass the SQL command to the database object
		return $this->_dbl->sqlQuery($sql);
		
	}
	
}


?>