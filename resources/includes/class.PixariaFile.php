<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/* ## @@ZEND@@ ## */

class PixariaFile {
	
	var $file_name;
	var $file_path;
	var $file_type;
	var $file_icon;
	var $file_size;
	var $file_modified;
	var $file_image_size;
	var $file_image_width;
	var $file_image_height;
	
	function PixariaFile ($path) {
		
		$pathinfo = pathinfo($path);
		
		$this->file_name		= $pathinfo['basename'];
		$this->file_path		= $path;
		$this->file_modified	= filemtime($path);
		
		$type_data = $this->parseFileType($path);
		
		$this->file_type		= $type_data[0];
		$this->file_icon		= $type_data[1];
		
		if (is_dir($path)) {
		
			$this->file_size = "Dir";
			
		} else {
		
			$bytes		= filesize($path);
			$kilobytes	= $bytes / 1000;
			$megabytes	= $kilobytes / 1000;
			
			if ($megabytes < 1) {
				
				if ($kilobytes < 1) {
				
					$this->file_size = $bytes . " B";
				
				} else {
				
					$this->file_size = round($kilobytes,0) . " KB";
				
				}
			
			} else {
			
				$this->file_size = round($megabytes,1) . " MB";
			
			}
			
			if ($this->file_image_size = @getimagesize($path)) {
			
				$this->file_image_width 	= $this->file_image_size[0];
				$this->file_image_height 	= $this->file_image_size[1];
			
			}
			
		}
	
	}
	
	/*
	*	Get file and directory name
	*/
	function getFileName () {
	
		return $this->file_name;
		
	}
	
	/*
	*	Get file and directory paths
	*/
	function getFilePath () {
	
		return $this->file_path;
		
	}
	
	/*
	*	Get file type
	*/
	function getFileType () {
	
		return $this->file_type;
		
	}
	
	/*
	*	Get file icon name
	*/
	function getFileIconName () {
	
		return $this->file_icon;
		
	}
	
	/*
	*	Get file size
	*/
	function getFileSize () {
	
		return $this->file_size;
		
	}
	
	/*
	*	Get file modification date
	*/
	function getFileModDate () {
	
		return $this->file_modified;
		
	}
	
	/*
	*	Get the width of this file if it's an image
	*/
	function getFileImageWidth () {
	
		return $this->file_image_width;
		
	}
	
	/*
	*	Get the width of this file if it's an image
	*/
	function getFileImageHeight () {
	
		return $this->file_image_height;
		
	}
	
	/*
	*	Get the type of the file
	*/
	function parseFileType ($path) {
		
		$info = pathinfo($path);
		
		if (is_dir($path)) {
			
			return array("Directory","folder");
		
		} else {
		
			switch ($info['extension']) {
			
				case 'gif':
					return array("GIF image","image-x-generic");
				break;
			
				case 'jpg':
					return array("JPEG image","image-x-generic");
				break;
			
				case 'jpeg':
					return array("JPEG image","image-x-generic");
				break;
			
				case 'tif':
					return array("TIFF image","image-x-generic");
				break;
			
				case 'png':
					return array("PNG image","image-x-generic");
				break;
			
				case 'dat':
					return array("Data file","text-x-generic");
				break;
			
				case 'db':
					return array("Database file","text-x-generic");
				break;
			
				case 'log':
					return array("Log file","text-x-generic");
				break;
			
				case 'php':
					return array("PHP file","text-x-generic");
				break;
			
				case 'inc':
					return array("PHP include file","text-x-generic");
				break;
			
				case 'DS_Store':
					return array("Mac OS X Finder data file","text-x-generic");
				break;
			
				case 'xml':
					return array("XML data file","text-x-generic");
				break;
			
				default:
					return array("File","text-x-generic");
				break;
			
			}
		
		}
	
	}
	
}

?>