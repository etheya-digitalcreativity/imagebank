<?php /* Smarty version 2.6.25, created on 2010-01-14 11:27:12
         compiled from index.layout/main.header.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'load_pixie', 'index.layout/main.header.tpl', 84, false),array('function', 'include_template', 'index.layout/main.header.tpl', 204, false),)), $this); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/2000/REC-xhtml1-20000126/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	
	<!-- Query String: <?php echo $this->_tpl_vars['query_string']; ?>
 -->
	<!-- Base URI: <?php echo $this->_tpl_vars['base_uri']; ?>
 -->

	<!-- Browser Name: <?php echo $this->_tpl_vars['browser_name']; ?>
 -->
	<!-- Browser Version: <?php echo $this->_tpl_vars['browser_version']; ?>
 -->
	
	<!-- OS Name: <?php echo $this->_tpl_vars['browser_OS_name']; ?>
 -->
	<!-- OS Version: <?php echo $this->_tpl_vars['browser_OS_version']; ?>
 -->
	
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	
	<title><?php echo $this->_tpl_vars['set_site_name']; ?>
 &raquo; <?php echo $this->_tpl_vars['page_title']; ?>
</title>
	
	<meta http-equiv="imagetoolbar" content="no" />

	<?php if ($this->_tpl_vars['image']['basic']['keywords'] != ""): ?><meta name="keywords" content="<?php echo $this->_tpl_vars['image']['basic']['keywords']; ?>
" /><?php else: ?><meta name="keywords" content="gallery, pixaria, web, php, album, photography" /><?php endif; ?>
	
	<?php if ($this->_tpl_vars['image']['files']['canonical_url'] != ""): ?><link rel="canonical" href="<?php echo $this->_tpl_vars['image']['files']['canonical_url']; ?>
" /><?php endif; ?>


	<?php if ($this->_tpl_vars['image']['basic']['caption'] != ""): ?><meta name="description" content="<?php echo $this->_tpl_vars['image']['basic']['caption']; ?>
" /><?php else: ?>
	<meta name="description" content="This photo gallery website is powered by Pixaria Gallery software." /><?php endif; ?>
	
	
	<?php if ($this->_tpl_vars['image']['files']['large_url'] != ""): ?><meta name="thumbnail_url" content="<?php echo $this->_tpl_vars['image']['files']['large_url']; ?>
" /><?php endif; ?>
	
	
	<link rel="stylesheet" href="<?php echo $this->_tpl_vars['base_url']; ?>
resources/themes/<?php echo $this->_tpl_vars['set_theme']; ?>
/css/pixaria.css" />
	<link rel="stylesheet" href="<?php echo $this->_tpl_vars['base_url']; ?>
resources/themes/<?php echo $this->_tpl_vars['set_theme']; ?>
/css/pixaria.3.0.css" />
	
	<?php if ($this->_tpl_vars['js_lightbox']): ?><link rel="stylesheet" href="<?php echo $this->_tpl_vars['base_url']; ?>
resources/themes/<?php echo $this->_tpl_vars['set_theme']; ?>
/css/pixaria.lightbox.css" /><?php endif; ?>
	
	<?php if ($this->_tpl_vars['set_func_rss'] == '1'): ?><link href="<?php echo $this->_tpl_vars['base_url']; ?>
<?php echo $this->_tpl_vars['fil_index_rss']; ?>
" rel="alternate" type="application/rss+xml" title="<?php echo $this->_tpl_vars['set_site_name']; ?>
" /><?php endif; ?>
	
	<?php if ($this->_tpl_vars['gallery_media_rss'] != ''): ?><?php echo $this->_tpl_vars['gallery_media_rss']; ?>
<?php endif; ?>
	
	<script type="text/javascript" language="javascript">
	<!--
	var baseUrl 	= '<?php echo $this->_tpl_vars['base_url']; ?>
';
	var baseTheme 	= '<?php echo $this->_tpl_vars['set_theme']; ?>
';
	-->
	</script>
	
	<script type="text/javascript" language="javascript" src="<?php echo $this->_tpl_vars['base_url']; ?>
resources/javascript/scriptaculous/prototype.js"></script>
	<script type="text/javascript" language="javascript" src="<?php echo $this->_tpl_vars['base_url']; ?>
resources/javascript/scriptaculous/scriptaculous.js"></script>

	<script type="text/javascript" language="javascript" src="<?php echo $this->_tpl_vars['base_url']; ?>
resources/themes/<?php echo $this->_tpl_vars['set_theme']; ?>
/javascript/pixaria.global.js"></script>
	
	<?php if ($this->_tpl_vars['js_lightbox']): ?><script type="text/javascript" language="javascript" src="<?php echo $this->_tpl_vars['base_url']; ?>
resources/themes/<?php echo $this->_tpl_vars['set_theme']; ?>
/javascript/pixaria.lightbox.js"></script><?php endif; ?>
	<?php if ($this->_tpl_vars['js_multifile']): ?><script type="text/javascript" language="javascript" src="<?php echo $this->_tpl_vars['base_url']; ?>
resources/themes/<?php echo $this->_tpl_vars['set_theme']; ?>
/javascript/pixaria.multifile.js"></script><?php endif; ?>
	
	<!--[if lt IE 7]>
	<script defer type="text/javascript" src="<?php echo $this->_tpl_vars['base_url']; ?>
resources/themes/<?php echo $this->_tpl_vars['set_theme']; ?>
/javascript/pixaria.pngfix.js"></script>
	<![endif]-->
		
</head>

<body>

<div id="content">

	<?php if (! $this->_tpl_vars['ses_psg_full_user'] && $this->_tpl_vars['set_login_default']): ?>
	
		<!-- IF THE USER IS NOT LOGGED IN AND THE SETTING TO LIMIT ACCESS FOR LOGGED OUT USERS IS ON THEN SHOW A LIMITED NAVIGATION BAR -->
		<div id="header">
	
			<div id="logo"><img src="<?php echo $this->_tpl_vars['base_url']; ?>
resources/themes/<?php echo $this->_tpl_vars['set_theme']; ?>
/images/logo.gif" alt="<?php echo $this->_tpl_vars['set_site_name']; ?>
" width="293" height="52" border="0" /></div>
			
			<?php echo smarty_load_pixie(array('name' => 'cart_items'), $this);?>

			
			<div id="navigation">
			
			<ul id="nav">
			
				<li><a href="<?php echo $this->_tpl_vars['base_url']; ?>
<?php echo $this->_tpl_vars['fil_index_account']; ?>
?cmd=userRegistrationStart" class="navigation-link">Register</a></li>
			
				<li><a href="<?php echo $this->_tpl_vars['base_url']; ?>
<?php echo $this->_tpl_vars['fil_index_login']; ?>
" class="navigation-link">Sign In</a></li>

				<?php if ($this->_tpl_vars['set_func_user_language_select']): ?>
					
					<li><a href="javascript:void(0);" class="navigation-link" onclick="javascript:Effect.toggle('lang-picker', 'blind', <?php echo '{ duration: 0.1 }'; ?>
);" style="padding-top:3px;"><img src="<?php echo $this->_tpl_vars['base_url']; ?>
/resources/images/flags/<?php echo @LOCAL_LANGUAGE; ?>
_small.png" alt="" width="24" height="24" border="0"></a></li>
				
				<?php endif; ?>

			</ul>
			
			</div>
			
		</div>
	
	<?php else: ?>

		<!-- SHOW THE NORMAL NAVIGATION BAR -->
		<div id="header">
		
			<div id="logo"><img src="<?php echo $this->_tpl_vars['base_url']; ?>
resources/themes/<?php echo $this->_tpl_vars['set_theme']; ?>
/images/logo.gif" alt="<?php echo $this->_tpl_vars['set_site_name']; ?>
" width="293" height="52" border="0" /></div>
			
			<?php echo smarty_load_pixie(array('name' => 'cart_items'), $this);?>

			
			<div id="navigation">
			
			<ul id="nav">
				
				<li><a href="<?php echo $this->_tpl_vars['base_url']; ?>
<?php echo $this->_tpl_vars['fil_index']; ?>
" class="navigation-link">Home</a></li>
				
				<?php if ($this->_tpl_vars['set_func_news']): ?>
					
					<!-- IF THE NEWS FUNCTIONS ARE ON -->
					<li><a href="<?php echo $this->_tpl_vars['base_url']; ?>
<?php echo $this->_tpl_vars['fil_index_news']; ?>
" class="navigation-link">News</a></li>
					
				<?php endif; ?>
				
				<?php if ($this->_tpl_vars['set_func_search']): ?>
					
					<!-- IF THE SEARCH FUNCTIONS ARE ON -->
					<li><a href="<?php echo $this->_tpl_vars['base_url']; ?>
<?php echo $this->_tpl_vars['fil_index_search']; ?>
" class="navigation-link">Search</a></li>
					
				<?php endif; ?>
				
				<li><a href="<?php echo $this->_tpl_vars['base_url']; ?>
<?php echo $this->_tpl_vars['fil_index_account']; ?>
" class="navigation-link">Account</a></li>
				
				<?php if ($this->_tpl_vars['set_func_lightbox']): ?>
					
					<!-- IF THE LIGHTBOX FUNCTIONS ARE ON -->
					<li><a href="<?php echo $this->_tpl_vars['base_url']; ?>
<?php echo $this->_tpl_vars['fil_index_lightbox']; ?>
" class="navigation-link">Lightbox</a></li>
				
				<?php endif; ?>
				
				<?php if ($this->_tpl_vars['set_func_store'] && $this->_tpl_vars['sys_store_enabled']): ?>
					
					<!-- IF THE E-COMMERCE FUNCTIONS ARE ON -->
					<li><a href="<?php echo $this->_tpl_vars['base_url']; ?>
<?php echo $this->_tpl_vars['fil_index_store']; ?>
" class="navigation-link">Cart</a></li>
				
				<?php endif; ?>
				
				<?php if ($this->_tpl_vars['set_func_contact']): ?>
					
					<!-- IF THE CONTACT FORM FUNCTIONS ARE ON -->
					<li><a href="<?php echo $this->_tpl_vars['base_url']; ?>
<?php echo $this->_tpl_vars['fil_index_contact']; ?>
" class="navigation-link">Contact</a></li>
				
				<?php endif; ?>
				
				<?php if ($this->_tpl_vars['ses_psg_administrator']): ?>
					
					<!-- IF THE USER IS AN ADMINISTRATOR -->
					<li><a href="<?php echo $this->_tpl_vars['base_url']; ?>
<?php echo $this->_tpl_vars['fil_admin_dashboard']; ?>
" class="navigation-link">Admin</a></li>
					
				<?php endif; ?>
				
				
				<?php if ($this->_tpl_vars['ses_psg_full_user']): ?>
				
					<!-- IF LOGGED IN -->
					<li><a href="<?php echo $this->_tpl_vars['base_url']; ?>
<?php echo $this->_tpl_vars['fil_index_login']; ?>
?cmd=signout" class="navigation-link">Sign Out</a></li>
				
				
				<?php else: ?>
					
					<!-- IF NOT LOGGED IN -->
					<li><a href="<?php echo $this->_tpl_vars['base_url']; ?>
<?php echo $this->_tpl_vars['fil_index_account']; ?>
?cmd=userRegistrationStart" class="navigation-link">Register</a></li>
				
					<li><a href="<?php echo $this->_tpl_vars['base_url']; ?>
<?php echo $this->_tpl_vars['fil_index_login']; ?>
" class="navigation-link">Sign In</a></li>
				
				<?php endif; ?>
				
				<?php if ($this->_tpl_vars['set_func_user_language_select']): ?>
					
					<li><a href="javascript:void(0);" class="navigation-link" onclick="javascript:Effect.toggle('lang-picker', 'blind', <?php echo '{ duration: 0.1 }'; ?>
);" style="padding-top:3px;"><img src="<?php echo $this->_tpl_vars['base_url']; ?>
/resources/images/flags/<?php echo @LOCAL_LANGUAGE; ?>
_small.png" alt="" width="24" height="24" border="0"></a></li>
				
				<?php endif; ?>
				
			</ul>
			
			<div id="search"><?php echo smarty_load_pixie(array('name' => 'search_form'), $this);?>
</div>
			
			</div>
		
		</div>
	
	<?php endif; ?>


<div>
	
<div id="front-outerdiv">

<div id="front-innerdiv">
	
<?php echo smarty_include_template(array('file' => "index.snippets/language.picker.html"), $this);?>