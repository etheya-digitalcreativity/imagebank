<?php /* Smarty version 2.6.25, created on 2010-01-14 11:27:12
         compiled from index.gallery/gallery.thumbnails.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', 'index.gallery/gallery.thumbnails.tpl', 85, false),array('modifier', 'date_format', 'index.gallery/gallery.thumbnails.tpl', 89, false),)), $this); ?>
<?php if ($this->_tpl_vars['galleries'] != false): ?>

	<table class="table-thumbnail">
	
	<thead>
	<tr>
	<th>Browse galleries:</th>
	</tr>
	</thead>
	
	<tbody>
	<tr>
	<td class="options">
		
		<div class="option-title">Thumbnail options: </div>
			
		<?php if ($this->_tpl_vars['set_default_thumb_size'] == 'both'): ?>
		<div class="pagination">
			
			<a href="javascript:toggleLarge();updateCookie(3,'large','1|<?php echo $this->_tpl_vars['set_thumb_per_page']; ?>
|<?php echo $this->_tpl_vars['set_gallery_per_page']; ?>
|<?php echo $this->_tpl_vars['set_view_thumb_view']; ?>
');">Large</a>
			
			<a href="javascript:toggleSmall();updateCookie(3,'small','1|<?php echo $this->_tpl_vars['set_thumb_per_page']; ?>
|<?php echo $this->_tpl_vars['set_gallery_per_page']; ?>
|<?php echo $this->_tpl_vars['set_view_thumb_view']; ?>
');">Small</a>
			
		</div>
		<?php endif; ?>
		
		<div class="pagination">
			
			<form action="<?php echo $this->_tpl_vars['gallery_url']; ?>
" method="post" onsubmit="return false;">
			<input type="text" size="3" maxlength="3" id="gallery_per_page" name="gallery_per_page" value="<?php echo $this->_tpl_vars['set_gallery_per_page']; ?>
" class="page-button" />
			<a href="<?php echo $this->_tpl_vars['gallery_url']; ?>
" onclick="javascript:if (document.getElementById('gallery_per_page').value != '0')<?php echo ' { '; ?>
updateCookie(2,document.getElementById('gallery_per_page').value,'1|<?php echo $this->_tpl_vars['set_thumb_per_page']; ?>
|<?php echo $this->_tpl_vars['set_gallery_per_page']; ?>
|<?php echo $this->_tpl_vars['set_view_thumb_view']; ?>
'); <?php echo '} else {'; ?>
 alert('You must choose a value greater than zero'); return false; <?php echo '}'; ?>
">Galleries per page</a>
			</form>
			
		</div>			
	
	</td>
	
	</tr>

	<tr>
	<td class="options">

		<div class="option-title">Page number: </div>
			
		<div class="pagination">
		
			<?php unset($this->_sections['gpages']);
$this->_sections['gpages']['name'] = 'gpages';
$this->_sections['gpages']['loop'] = is_array($_loop=$this->_tpl_vars['gpage_numbers']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['gpages']['show'] = true;
$this->_sections['gpages']['max'] = $this->_sections['gpages']['loop'];
$this->_sections['gpages']['step'] = 1;
$this->_sections['gpages']['start'] = $this->_sections['gpages']['step'] > 0 ? 0 : $this->_sections['gpages']['loop']-1;
if ($this->_sections['gpages']['show']) {
    $this->_sections['gpages']['total'] = $this->_sections['gpages']['loop'];
    if ($this->_sections['gpages']['total'] == 0)
        $this->_sections['gpages']['show'] = false;
} else
    $this->_sections['gpages']['total'] = 0;
if ($this->_sections['gpages']['show']):

            for ($this->_sections['gpages']['index'] = $this->_sections['gpages']['start'], $this->_sections['gpages']['iteration'] = 1;
                 $this->_sections['gpages']['iteration'] <= $this->_sections['gpages']['total'];
                 $this->_sections['gpages']['index'] += $this->_sections['gpages']['step'], $this->_sections['gpages']['iteration']++):
$this->_sections['gpages']['rownum'] = $this->_sections['gpages']['iteration'];
$this->_sections['gpages']['index_prev'] = $this->_sections['gpages']['index'] - $this->_sections['gpages']['step'];
$this->_sections['gpages']['index_next'] = $this->_sections['gpages']['index'] + $this->_sections['gpages']['step'];
$this->_sections['gpages']['first']      = ($this->_sections['gpages']['iteration'] == 1);
$this->_sections['gpages']['last']       = ($this->_sections['gpages']['iteration'] == $this->_sections['gpages']['total']);
?>
			
				<?php if ($this->_tpl_vars['gpage_numbers'][$this->_sections['gpages']['index']] == $this->_tpl_vars['gpage_current']): ?>
				
					<a href="<?php echo $this->_tpl_vars['gpage_links'][$this->_sections['gpages']['index']]; ?>
" class="current"><?php echo $this->_tpl_vars['gpage_numbers'][$this->_sections['gpages']['index']]; ?>
</a>
				
				<?php else: ?>
				
					<a href="<?php echo $this->_tpl_vars['gpage_links'][$this->_sections['gpages']['index']]; ?>
"><?php echo $this->_tpl_vars['gpage_numbers'][$this->_sections['gpages']['index']]; ?>
</a>
				
				<?php endif; ?>
			
			<?php endfor; endif; ?>
		
		</div>
	
	</td>
	</tr>

	<tr>
	<td>

		<?php if ($this->_tpl_vars['set_default_thumb_size'] == 'both' || $this->_tpl_vars['set_default_thumb_size'] == 'large'): ?>
		<div id="gallery-large"<?php if ($this->_tpl_vars['set_view_thumb_view'] == 'small' && $this->_tpl_vars['set_default_thumb_size'] == 'both'): ?> style="display:none;"<?php endif; ?>>
			
		<?php unset($this->_sections['tn']);
$this->_sections['tn']['name'] = 'tn';
$this->_sections['tn']['loop'] = is_array($_loop=$this->_tpl_vars['galleries']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['tn']['show'] = true;
$this->_sections['tn']['max'] = $this->_sections['tn']['loop'];
$this->_sections['tn']['step'] = 1;
$this->_sections['tn']['start'] = $this->_sections['tn']['step'] > 0 ? 0 : $this->_sections['tn']['loop']-1;
if ($this->_sections['tn']['show']) {
    $this->_sections['tn']['total'] = $this->_sections['tn']['loop'];
    if ($this->_sections['tn']['total'] == 0)
        $this->_sections['tn']['show'] = false;
} else
    $this->_sections['tn']['total'] = 0;
if ($this->_sections['tn']['show']):

            for ($this->_sections['tn']['index'] = $this->_sections['tn']['start'], $this->_sections['tn']['iteration'] = 1;
                 $this->_sections['tn']['iteration'] <= $this->_sections['tn']['total'];
                 $this->_sections['tn']['index'] += $this->_sections['tn']['step'], $this->_sections['tn']['iteration']++):
$this->_sections['tn']['rownum'] = $this->_sections['tn']['iteration'];
$this->_sections['tn']['index_prev'] = $this->_sections['tn']['index'] - $this->_sections['tn']['step'];
$this->_sections['tn']['index_next'] = $this->_sections['tn']['index'] + $this->_sections['tn']['step'];
$this->_sections['tn']['first']      = ($this->_sections['tn']['iteration'] == 1);
$this->_sections['tn']['last']       = ($this->_sections['tn']['iteration'] == $this->_sections['tn']['total']);
?>
		
			<div class="thumb-lrg" style="width:200px; height:260px;">
			
			<table>
			<thead>
			<tr>
			<th colspan="2"><a href="<?php echo $this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['url']; ?>
"><img src="<?php echo $this->_tpl_vars['base_url']; ?>
<?php echo $this->_tpl_vars['fil_psg_image_thumbnail']; ?>
?file=<?php echo $this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['key_large_path']; ?>
" <?php echo $this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['key_large_dims']['2']; ?>
 class="thumbnail" alt="<?php echo $this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['title']; ?>
" title="<?php echo $this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['title']; ?>
" /></a></th>
			</tr>
			</thead>
			<tbody>
			<tr>
			<th>Title:</th>
			<td><a href="<?php echo $this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['url']; ?>
" title="<?php echo $this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['title']; ?>
" class="plain"><?php echo ((is_array($_tmp=$this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['title'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 20, "...", true) : smarty_modifier_truncate($_tmp, 20, "...", true)); ?>
</a></td>
			</tr>
			<tr>
			<th>Date:</th>
			<td><?php echo ((is_array($_tmp=$this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%B %e, %Y") : smarty_modifier_date_format($_tmp, "%B %e, %Y")); ?>
</td>
			</tr>
			<tr>
			<th>Contains:</th>
			<td><?php echo $this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['image_count']; ?>
 images</td>
			</tr>
			</tbody>
			</table>
			
			</div>
			
		<?php endfor; endif; ?>
				
		<br clear="all" />
				
		</div>
		<?php endif; ?>
		
		<?php if ($this->_tpl_vars['set_default_thumb_size'] == 'both' || $this->_tpl_vars['set_default_thumb_size'] == 'small'): ?>
		<div id="gallery-small"<?php if ($this->_tpl_vars['set_view_thumb_view'] == 'large' && $this->_tpl_vars['set_default_thumb_size'] == 'both'): ?> style="display:none;"<?php endif; ?>>
			
		<?php unset($this->_sections['tn']);
$this->_sections['tn']['name'] = 'tn';
$this->_sections['tn']['loop'] = is_array($_loop=$this->_tpl_vars['galleries']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['tn']['show'] = true;
$this->_sections['tn']['max'] = $this->_sections['tn']['loop'];
$this->_sections['tn']['step'] = 1;
$this->_sections['tn']['start'] = $this->_sections['tn']['step'] > 0 ? 0 : $this->_sections['tn']['loop']-1;
if ($this->_sections['tn']['show']) {
    $this->_sections['tn']['total'] = $this->_sections['tn']['loop'];
    if ($this->_sections['tn']['total'] == 0)
        $this->_sections['tn']['show'] = false;
} else
    $this->_sections['tn']['total'] = 0;
if ($this->_sections['tn']['show']):

            for ($this->_sections['tn']['index'] = $this->_sections['tn']['start'], $this->_sections['tn']['iteration'] = 1;
                 $this->_sections['tn']['iteration'] <= $this->_sections['tn']['total'];
                 $this->_sections['tn']['index'] += $this->_sections['tn']['step'], $this->_sections['tn']['iteration']++):
$this->_sections['tn']['rownum'] = $this->_sections['tn']['iteration'];
$this->_sections['tn']['index_prev'] = $this->_sections['tn']['index'] - $this->_sections['tn']['step'];
$this->_sections['tn']['index_next'] = $this->_sections['tn']['index'] + $this->_sections['tn']['step'];
$this->_sections['tn']['first']      = ($this->_sections['tn']['iteration'] == 1);
$this->_sections['tn']['last']       = ($this->_sections['tn']['iteration'] == $this->_sections['tn']['total']);
?>
		
			<div class="thumb-sml" style="width:190px; height:170px;">
			
			<table>
			<thead>
			<tr>
			<th colspan="2"><a href="<?php echo $this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['url']; ?>
"><img src="<?php echo $this->_tpl_vars['base_url']; ?>
<?php echo $this->_tpl_vars['fil_psg_image_thumbnail']; ?>
?file=<?php echo $this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['key_small_path']; ?>
" <?php echo $this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['key_small_dims']['2']; ?>
 class="thumbnail" alt="<?php echo $this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['title']; ?>
" title="<?php echo $this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['title']; ?>
" /></a></th>
			</tr>
			</thead>
			<tbody>
			<tr>
			<th>Title:</th>
			<td><a href="<?php echo $this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['url']; ?>
" title="<?php echo $this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['title']; ?>
" class="plain"><?php echo ((is_array($_tmp=$this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['title'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 20, "...", true) : smarty_modifier_truncate($_tmp, 20, "...", true)); ?>
</a></td>
			</tr>
			<tr>
			<th>Date:</th>
			<td><?php echo ((is_array($_tmp=$this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%B %e, %Y") : smarty_modifier_date_format($_tmp, "%B %e, %Y")); ?>
</td>
			</tr>
			<tr>
			<th>Contains:</th>
			<td><?php echo $this->_tpl_vars['galleries'][$this->_sections['tn']['index']]['image_count']; ?>
 images</td>
			</tr>
			</tbody>
			</table>
			
			</div>
			
		<?php endfor; endif; ?>
				
		<br clear="all" />
				
		</div>
		<?php endif; ?>
		
	</td>
	</tr>
	</tbody>
	
	</table>

<?php endif; ?>