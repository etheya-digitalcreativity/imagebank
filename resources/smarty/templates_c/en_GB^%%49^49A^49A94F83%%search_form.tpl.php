<?php /* Smarty version 2.6.25, created on 2010-01-14 11:27:12
         compiled from /home/etheya/imagebank.etheya.com/resources/pixies/search_form/search_form.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'html_select_date', '/home/etheya/imagebank.etheya.com/resources/pixies/search_form/search_form.tpl', 249, false),array('function', 'include_template', '/home/etheya/imagebank.etheya.com/resources/pixies/search_form/search_form.tpl', 353, false),)), $this); ?>

<?php if ($this->_tpl_vars['EA1_form_type'] == 'minimal'): ?>
	
	<form action="<?php echo $this->_tpl_vars['base_url']; ?>
<?php echo $this->_tpl_vars['fil_index_search']; ?>
" method="post">
	
	<input type="hidden" name="cmd" value="doSearch" />
	
	<input type="hidden" name="new" value="1" />
	
		<table border="0" cellpadding="3" cellspacing="0" width="280" style="height:30px;">
		<tr>
		<td align="right"><input type="text" value="<?php echo $this->_tpl_vars['keywords_simple_boolean']; ?>
" name="keywords_simple_boolean" class="forminput" style="width:150px; font-size:9pt; font-weight:bold;" /></td>
		<td><input type="submit" name="search" value="Search" style="text-align:center; width:60px; border:2px outset #888; color:#FFF; background:#888; font-family:Helvetica; font-size:10pt; font-weight:bold;" /></td>
		</tr>
		</table>
	
	</form>
	
<?php elseif ($this->_tpl_vars['EA1_form_type'] == 'sidepanel'): ?>
	
	<form action="<?php echo $this->_tpl_vars['base_url']; ?>
<?php echo $this->_tpl_vars['fil_index_search']; ?>
" method="post" name="search_advanced">
	
	<input type="hidden" name="cmd" value="doSearch" />
	
	<input type="hidden" name="new" value="1" />
	
	<table class="table-search">
	
	<tr>
	<th>Search for images</th>
	</tr>
	
	<tr>
	<td style="padding:4px;">
	
		<?php if ($this->_tpl_vars['set_search_opt_keywords']): ?>
			
			<span class="bold">Enter a keyword search:</span><br />
			<input type="text" value="<?php echo $this->_tpl_vars['keywords_simple_boolean']; ?>
" name="keywords_simple_boolean" class="forminput" style="width:245px;" />
			
			<br /><br />
			
		<?php endif; ?>
	
		<?php if ($this->_tpl_vars['set_search_opt_galleries']): ?>
		
				<span class="bold">Search for images in galleries:</span><br />
				
				<select name="gallery_id" class="forminput" style="width:245px;" size="10">
				<option value="0" <?php if ($this->_tpl_vars['EA1_menu_gallery_id'][$this->_sections['gallery']['index']] == ""): ?> selected="selected"<?php endif; ?>>Entire library</option>
				<option value="0">-------------------------</option>
				<?php unset($this->_sections['gallery']);
$this->_sections['gallery']['name'] = 'gallery';
$this->_sections['gallery']['loop'] = is_array($_loop=$this->_tpl_vars['EA1_menu_gallery_title']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['gallery']['show'] = true;
$this->_sections['gallery']['max'] = $this->_sections['gallery']['loop'];
$this->_sections['gallery']['step'] = 1;
$this->_sections['gallery']['start'] = $this->_sections['gallery']['step'] > 0 ? 0 : $this->_sections['gallery']['loop']-1;
if ($this->_sections['gallery']['show']) {
    $this->_sections['gallery']['total'] = $this->_sections['gallery']['loop'];
    if ($this->_sections['gallery']['total'] == 0)
        $this->_sections['gallery']['show'] = false;
} else
    $this->_sections['gallery']['total'] = 0;
if ($this->_sections['gallery']['show']):

            for ($this->_sections['gallery']['index'] = $this->_sections['gallery']['start'], $this->_sections['gallery']['iteration'] = 1;
                 $this->_sections['gallery']['iteration'] <= $this->_sections['gallery']['total'];
                 $this->_sections['gallery']['index'] += $this->_sections['gallery']['step'], $this->_sections['gallery']['iteration']++):
$this->_sections['gallery']['rownum'] = $this->_sections['gallery']['iteration'];
$this->_sections['gallery']['index_prev'] = $this->_sections['gallery']['index'] - $this->_sections['gallery']['step'];
$this->_sections['gallery']['index_next'] = $this->_sections['gallery']['index'] + $this->_sections['gallery']['step'];
$this->_sections['gallery']['first']      = ($this->_sections['gallery']['iteration'] == 1);
$this->_sections['gallery']['last']       = ($this->_sections['gallery']['iteration'] == $this->_sections['gallery']['total']);
?>
					
					<option value="<?php echo $this->_tpl_vars['EA1_menu_gallery_id'][$this->_sections['gallery']['index']]; ?>
" <?php if ($this->_tpl_vars['EA1_menu_gallery_id'][$this->_sections['gallery']['index']] == $this->_tpl_vars['gallery_id']): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['EA1_menu_gallery_title'][$this->_sections['gallery']['index']]; ?>
</option>
				
				<?php endfor; endif; ?>
				
				</select>
				
				<br /><br />
		
		<?php endif; ?>
		
		<?php if ($this->_tpl_vars['set_search_opt_model_release']): ?>
	
			<input type="checkbox" name="model_release" class="forminput" <?php if ($this->_tpl_vars['model_release'] == 'on'): ?>checked="checked"<?php endif; ?> /> <span class="bold">With model release</span><br />
			
		<?php endif; ?>
	
		<?php if ($this->_tpl_vars['set_search_opt_property_release']): ?>
		
			<input type="checkbox" name="property_release" class="forminput" <?php if ($this->_tpl_vars['property_release'] == 'on'): ?>checked="checked"<?php endif; ?> /> <span class="bold">With property release</span><br />
			
		<?php endif; ?>		
		
		<?php if ($this->_tpl_vars['set_search_opt_rights_managed']): ?>
			
			<br />
			<span class="bold">Rights management type:</span><br />
			<input type="radio" name="rights_managed" value="2" class="forminput" <?php if ($this->_tpl_vars['rights_managed'] == '2'): ?>checked="checked"<?php endif; ?> /> Rights managed only<br />
			<input type="radio" name="rights_managed" value="3" class="forminput" <?php if ($this->_tpl_vars['rights_managed'] == '3'): ?>checked="checked"<?php endif; ?> /> Royalty free only<br />
			<input type="radio" name="rights_managed" value="1" class="forminput" <?php if ($this->_tpl_vars['rights_managed'] == '1' || ! $this->_tpl_vars['rights_managed']): ?>checked="checked"<?php endif; ?> /> All images	
			
		<?php endif; ?>		
		
		<?php if ($this->_tpl_vars['set_search_opt_property_release'] || $this->_tpl_vars['set_search_opt_model_release'] || $this->_tpl_vars['set_search_opt_rights_managed']): ?>
		
			<br /><br />
			
		<?php endif; ?>
		
		<?php if ($this->_tpl_vars['set_search_orientation']): ?>
		
			<span class="bold">Image orientation:</span><br />
			<input type="radio" name="image_orientation" class="forminput" value="ne" <?php if ($this->_tpl_vars['image_orientation'] == 'ne' || ! $this->_tpl_vars['image_orientation']): ?>checked="checked"<?php endif; ?> /> Any orientation<br />
			<input type="radio" name="image_orientation" class="forminput" value="ls" <?php if ($this->_tpl_vars['image_orientation'] == 'ls'): ?>checked="checked"<?php endif; ?> /> Landscape<br />
			<input type="radio" name="image_orientation" class="forminput" value="po" <?php if ($this->_tpl_vars['image_orientation'] == 'po'): ?>checked="checked"<?php endif; ?> /> Portrait<br />
			<input type="radio" name="image_orientation" class="forminput" value="sq" <?php if ($this->_tpl_vars['image_orientation'] == 'sq'): ?>checked="checked"<?php endif; ?> /> Square<br />
			
			<br />
		
		<?php endif; ?>

		<input type="submit" class="formbutton" style="width:245px;" value="Search for images" />
		
	
	</td>
	</tr>
	
	</table>
	
	</form>


<?php elseif ($this->_tpl_vars['EA1_form_type'] == 'advanced'): ?>
	
	
	<form action="<?php echo $this->_tpl_vars['base_url']; ?>
<?php echo $this->_tpl_vars['fil_index_search']; ?>
" method="post" name="search_advanced">
	
	<input type="hidden" name="cmd" value="doSearch" />
	
	<input type="hidden" name="new" value="1" />
	
	<table class="table-search">
	
	<tr>
	<th>Search for images</th>
	</tr>
	
	<tr>
	<td>
	
	<div class="search-box" style="border-right:1px solid #BBB; min-height:255px;">
	
	<input type="radio" name="search_within" value="false" checked="checked" class="forminput" /> <b>New Search</b>
	
	<?php if ($this->_tpl_vars['sid'] != ""): ?>&nbsp; &nbsp; <input type="radio" name="search_within" value="<?php echo $this->_tpl_vars['sid']; ?>
" class="forminput" /> <b>Search within</b><?php endif; ?>
	
	<br /><br />
	
		<?php if ($this->_tpl_vars['set_search_opt_keywords']): ?>
			
			<span class="bold">Enter a keyword search:</span><br />
			<input type="text" value="<?php echo $this->_tpl_vars['keywords_simple_boolean']; ?>
" name="keywords_simple_boolean" class="forminput" style="width:100%;" />
			
			<br /><br />
			
		<?php endif; ?>
	
		<?php if ($this->_tpl_vars['set_search_opt_model_release']): ?>
	
			<input type="checkbox" name="model_release" class="forminput" <?php if ($this->_tpl_vars['model_release'] == 'on'): ?>checked="checked"<?php endif; ?> /> <span class="bold">With model release</span><br />
			
		<?php endif; ?>
	
		<?php if ($this->_tpl_vars['set_search_opt_property_release']): ?>
		
			<input type="checkbox" name="property_release" class="forminput" <?php if ($this->_tpl_vars['property_release'] == 'on'): ?>checked="checked"<?php endif; ?> /> <span class="bold">With property release</span><br />
			
		<?php endif; ?>		
		
		<?php if ($this->_tpl_vars['set_search_opt_rights_managed']): ?>
		
			<input type="radio" name="rights_managed" value="2" class="forminput" <?php if ($this->_tpl_vars['rights_managed'] == '2'): ?>checked="checked"<?php endif; ?> /> <span class="bold">Rights managed only</span><br />
			<input type="radio" name="rights_managed" value="3" class="forminput" <?php if ($this->_tpl_vars['rights_managed'] == '3'): ?>checked="checked"<?php endif; ?> /> <span class="bold">Royalty free only</span><br />
			<input type="radio" name="rights_managed" value="1" class="forminput" <?php if ($this->_tpl_vars['rights_managed'] == '1' || ! $this->_tpl_vars['rights_managed']): ?>checked="checked"<?php endif; ?> /> <span class="bold">All images</span>		
			
		<?php endif; ?>		
		
		<?php if ($this->_tpl_vars['set_search_opt_property_release'] || $this->_tpl_vars['set_search_opt_model_release'] || $this->_tpl_vars['set_search_opt_rights_managed']): ?>
		
			<br /><br />
			
		<?php endif; ?>
		
		
		<input type="submit" class="formbutton" style="width:100%;" value="Search for images" />
		
	</div>

	<?php if ($this->_tpl_vars['set_search_opt_galleries']): ?>
	
		<div class="search-box" style="border-right:1px solid #BBB; min-height:255px;">
		
			<span class="bold">Search for images in galleries:</span><br />
			
			<select name="gallery_id" class="forminput" style="width:100%;" size="11">
			<option value="0" <?php if ($this->_tpl_vars['EA1_menu_gallery_id'][$this->_sections['gallery']['index']] == ""): ?> selected="selected"<?php endif; ?>>Entire library</option>
			<option value="0">-------------------------</option>
			<?php unset($this->_sections['gallery']);
$this->_sections['gallery']['name'] = 'gallery';
$this->_sections['gallery']['loop'] = is_array($_loop=$this->_tpl_vars['EA1_menu_gallery_title']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['gallery']['show'] = true;
$this->_sections['gallery']['max'] = $this->_sections['gallery']['loop'];
$this->_sections['gallery']['step'] = 1;
$this->_sections['gallery']['start'] = $this->_sections['gallery']['step'] > 0 ? 0 : $this->_sections['gallery']['loop']-1;
if ($this->_sections['gallery']['show']) {
    $this->_sections['gallery']['total'] = $this->_sections['gallery']['loop'];
    if ($this->_sections['gallery']['total'] == 0)
        $this->_sections['gallery']['show'] = false;
} else
    $this->_sections['gallery']['total'] = 0;
if ($this->_sections['gallery']['show']):

            for ($this->_sections['gallery']['index'] = $this->_sections['gallery']['start'], $this->_sections['gallery']['iteration'] = 1;
                 $this->_sections['gallery']['iteration'] <= $this->_sections['gallery']['total'];
                 $this->_sections['gallery']['index'] += $this->_sections['gallery']['step'], $this->_sections['gallery']['iteration']++):
$this->_sections['gallery']['rownum'] = $this->_sections['gallery']['iteration'];
$this->_sections['gallery']['index_prev'] = $this->_sections['gallery']['index'] - $this->_sections['gallery']['step'];
$this->_sections['gallery']['index_next'] = $this->_sections['gallery']['index'] + $this->_sections['gallery']['step'];
$this->_sections['gallery']['first']      = ($this->_sections['gallery']['iteration'] == 1);
$this->_sections['gallery']['last']       = ($this->_sections['gallery']['iteration'] == $this->_sections['gallery']['total']);
?>
				
				<option value="<?php echo $this->_tpl_vars['EA1_menu_gallery_id'][$this->_sections['gallery']['index']]; ?>
" <?php if ($this->_tpl_vars['EA1_menu_gallery_id'][$this->_sections['gallery']['index']] == $this->_tpl_vars['gallery_id']): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['EA1_menu_gallery_title'][$this->_sections['gallery']['index']]; ?>
</option>
			
			<?php endfor; endif; ?>
			
			</select>
			
			<br /><br />
		
		<span class="bold">Display results in order:</span><br />
		
		<select name="results_order" class="forminput" style="width:100%;">
		<option value="1"<?php if ($this->_tpl_vars['results_order'] == 1): ?> selected="selected"<?php elseif (! $this->_tpl_vars['results_order'] && $this->_tpl_vars['set_search_default_order'] == 1): ?> selected="selected"<?php endif; ?>>Most recent images first</option>
		<option value="3"<?php if ($this->_tpl_vars['results_order'] == 3): ?> selected="selected"<?php elseif (! $this->_tpl_vars['results_order'] && $this->_tpl_vars['set_search_default_order'] == 3): ?> selected="selected"<?php endif; ?>>File name A - Z</option>
		<option value="4"<?php if ($this->_tpl_vars['results_order'] == 4): ?> selected="selected"<?php elseif (! $this->_tpl_vars['results_order'] && $this->_tpl_vars['set_search_default_order'] == 4): ?> selected="selected"<?php endif; ?>>File name Z - A</option>
		<option value="5"<?php if ($this->_tpl_vars['results_order'] == 5): ?> selected="selected"<?php elseif (! $this->_tpl_vars['results_order'] && $this->_tpl_vars['set_search_default_order'] == 5): ?> selected="selected"<?php endif; ?>>Image Title A - Z</option>
		<option value="6"<?php if ($this->_tpl_vars['results_order'] == 6): ?> selected="selected"<?php elseif (! $this->_tpl_vars['results_order'] && $this->_tpl_vars['set_search_default_order'] == 6): ?> selected="selected"<?php endif; ?>>Image Title Z - A</option>
		</select>


		<br /><br />
		
		</div>
	
	<?php endif; ?>
	
	<?php if ($this->_tpl_vars['set_search_opt_date']): ?>
		
		<div class="search-box" style="border-right:1px solid #BBB; width:220px; min-height:255px;">
			
			<span class="bold">Search for images by date</span>
			
			<br /><br />
			
			Specify an exact date or range of dates to refine your image search.
			
			<br /><br />
			
			<input type="radio" name="date_srch" value="10"<?php if ($this->_tpl_vars['date_srch'] == 10 || ! $this->_tpl_vars['date_srch']): ?> checked="checked"<?php endif; ?> />
			
			<span class="bold">Don't search by date</span>
				
			<br /><br />
			
			<input type="radio" name="date_srch" value="11"<?php if ($this->_tpl_vars['date_srch'] == 11): ?> checked="checked"<?php endif; ?> /> <span class="bold">Search by exact date:</span><br />
			
			<?php if ($this->_tpl_vars['set_date_format'] == 'dmy'): ?>
			
				<?php echo smarty_function_html_select_date(array('time' => $this->_tpl_vars['date_is_time'],'field_order' => 'DMY','prefix' => 'date_is_','start_year' => '1970','reverse_years' => 'TRUE'), $this);?>

				
			<?php else: ?>
			
				<?php echo smarty_function_html_select_date(array('time' => $this->_tpl_vars['date_is_time'],'field_order' => 'MDY','prefix' => 'date_is_','start_year' => '1970','reverse_years' => 'TRUE'), $this);?>

				
			<?php endif; ?>
			
			<br /><br />
			
			<input type="radio" name="date_srch" value="12"<?php if ($this->_tpl_vars['date_srch'] == 12): ?> checked="checked"<?php endif; ?> /> <span class="bold">Search by date range:</span><br />
			
			
			<?php if ($this->_tpl_vars['set_date_format'] == 'dmy'): ?>
			
				<?php echo smarty_function_html_select_date(array('time' => $this->_tpl_vars['date_st_time'],'field_order' => 'DMY','prefix' => 'date_st_','start_year' => '1970','reverse_years' => 'TRUE'), $this);?>

		
			<?php else: ?>
			
				<?php echo smarty_function_html_select_date(array('time' => $this->_tpl_vars['date_st_time'],'field_order' => 'MDY','prefix' => 'date_st_','start_year' => '1970','reverse_years' => 'TRUE'), $this);?>

		
			<?php endif; ?>
			
			<br />
		
			<?php if ($this->_tpl_vars['set_date_format'] == 'dmy'): ?>
			
				<?php echo smarty_function_html_select_date(array('time' => $this->_tpl_vars['date_en_time'],'field_order' => 'DMY','prefix' => 'date_en_','start_year' => '1970','reverse_years' => 'TRUE'), $this);?>

		
			<?php else: ?>
			
				<?php echo smarty_function_html_select_date(array('time' => $this->_tpl_vars['date_en_time'],'field_order' => 'MDY','prefix' => 'date_en_','start_year' => '1970','reverse_years' => 'TRUE'), $this);?>

		
			<?php endif; ?>
		
			<br /><br />
			
		</div>
		
	<?php endif; ?>

	<div class="search-box" style="min-height:255px;">
	
		<?php if ($this->_tpl_vars['set_search_orientation']): ?>
		
			<span class="bold">Image orientation:</span><br />
			<input type="radio" name="image_orientation" class="forminput" value="ne" <?php if ($this->_tpl_vars['image_orientation'] == 'ne' || ! $this->_tpl_vars['image_orientation']): ?>checked="checked"<?php endif; ?> /> Any orientation<br />
			<input type="radio" name="image_orientation" class="forminput" value="ls" <?php if ($this->_tpl_vars['image_orientation'] == 'ls'): ?>checked="checked"<?php endif; ?> /> Landscape<br />
			<input type="radio" name="image_orientation" class="forminput" value="po" <?php if ($this->_tpl_vars['image_orientation'] == 'po'): ?>checked="checked"<?php endif; ?> /> Portrait<br />
			<input type="radio" name="image_orientation" class="forminput" value="sq" <?php if ($this->_tpl_vars['image_orientation'] == 'sq'): ?>checked="checked"<?php endif; ?> /> Square<br />
			
			<br />
		
		<?php endif; ?>


	<?php if ($this->_tpl_vars['set_search_image_color']): ?>
	
		<span class="bold">Find images by colour:</span><br />
		
		<input type="checkbox" name="image_colr_enable" class="forminput" <?php if ($this->_tpl_vars['image_colr_enable']): ?> checked="checked"<?php endif; ?> /> Enable color search tool<br />
			
			<div id="color_picker" style="display:inline;">
			
			<?php echo '<script language="javascript" type="text/javascript">function openHUD() { Effect.Appear(\'hud-color\',{duration:0.5}); }</script>'; ?>

			
			<br />
			
			<b>Colour palette selection:</b>
			
			<div style="width:190px;">
			
			<div style="float:left; width:30px; height:30px; background:#<?php echo $this->_tpl_vars['image_colr_hex']['3']; ?>
; margin:5px; cursor:pointer;" id="selected-color-form" onclick="javascript:openHUD();"></div>
			<div style="float:left; width:130px; margin:5px;">
			
				<div id="color-form-hex">Hex: <?php echo $this->_tpl_vars['image_colr_hex']['3']; ?>
</div>
				<div id="color-form-rgb">RGB: <?php echo $this->_tpl_vars['image_colr_dec']['3']; ?>
</div>
			
			</div>
			
			</div>
			
			<input type="hidden" name="image_colr_r" id="image_colr_r" value="<?php echo $this->_tpl_vars['image_colr_mod']['0']; ?>
" />
			<input type="hidden" name="image_colr_g" id="image_colr_g" value="<?php echo $this->_tpl_vars['image_colr_mod']['1']; ?>
" />
			<input type="hidden" name="image_colr_b" id="image_colr_b" value="<?php echo $this->_tpl_vars['image_colr_mod']['2']; ?>
" />
				
			<br clear="all" />
			
			<a href="javascript:openHUD();" title="Show the colour palette and colour selection heads up display.">Show colour palette HUD</a>
			
			</div>
		
	<?php endif; ?>
	
	</div>
	
	
	</td>
	</tr>
	
	</table>
	
	</form>
	
	<?php if ($this->_tpl_vars['set_search_image_color']): ?><?php echo smarty_include_template(array('file' => "index.search/image.hud.color.palette.tpl"), $this);?>
<?php endif; ?>

<?php endif; ?>
