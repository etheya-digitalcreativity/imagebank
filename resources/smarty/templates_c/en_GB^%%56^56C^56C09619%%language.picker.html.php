<?php /* Smarty version 2.6.25, created on 2010-01-14 11:27:12
         compiled from index.snippets/language.picker.html */ ?>
<?php if ($this->_tpl_vars['set_func_user_language_select']): ?>

<div style="display:none;" id="lang-picker">
	
	<form name="language-picker" id="language-picker" action="" onsubmit="javascript:return false;">
	
	<div class="form-h">
		
		<div class="header">Choose your preferred language</div>
		
		<div class="body">
			
			<div class="box">
			
				<p>Choose the language you would prefer to use when browsing this website.</p>
			
				<table style="float:left; width:250px;">
				
				<?php $_from = $this->_tpl_vars['set_optional_languages']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['local'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['local']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
        $this->_foreach['local']['iteration']++;
?>
				
				<tr>
				
					<td style="padding:0; width:32px;"><img src="<?php echo $this->_tpl_vars['base_url']; ?>
resources/images/flags/<?php echo $this->_tpl_vars['v']; ?>
.png" alt="<?php echo $this->_tpl_vars['v']; ?>
" width="32" height="32" border="0" /><td>
					
					<td>
					<input type="radio" id="lang_<?php echo ($this->_foreach['local']['iteration']-1); ?>
" name="default_language" class="forminput" value="<?php echo $this->_tpl_vars['v']; ?>
" <?php if (@LOCAL_LANGUAGE == $this->_tpl_vars['v']): ?>checked="checked"<?php endif; ?> />
					<label for="lang_<?php echo ($this->_foreach['local']['iteration']-1); ?>
"><?php echo $this->_tpl_vars['set_installed_languages'][$this->_tpl_vars['v']]; ?>
</label>
					</td>
				
				</tr>
				
				<?php if (( ($this->_foreach['local']['iteration']-1)+1 ) % 5 == 0): ?>
				
				</table>
				
				<table style="float:left; width:250px;">
				
				<?php endif; ?>
				
				<?php endforeach; endif; unset($_from); ?>
				
				</table>
				
				<br clear="all" />
				
				<input type="button" class="button" name="" value="Save language selection" onclick="javascript:saveDefaultLanguage('language-picker','default_language');" />
				
			</div>
			
			<br clear="all" />
			
		</div>
	
	</div>
	
	</form>
	
</div>

<?php endif; ?>