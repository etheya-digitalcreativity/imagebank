/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/


function setPointer(theRow, theAction, theColor)
{
    var theCells = null;
	
    // 1.1 Sets the mouse pointer to pointer on mouseover and back to normal otherwise.
    if (theAction == "over" || theAction == "click") {
        theRow.style.cursor='pointer';
        theRow.style.background = theColor;
    } else {
        theRow.style.cursor='default';
        theRow.style.background = theColor;
    }
    
    
    
    return true;
} // end of the 'setPointer()' function


function confirmLink(theLink, theCommand)
{
    // Confirmation is not required in the configuration file
    if (theCommand == '') {
        return true;
    }

    var is_confirmed = confirm('Are you sure you want to:\n'+theCommand);
    
    if (is_confirmed) {
    
        document.location.href = theLink + '&js_confirmed=1';
        
    }

    return is_confirmed;
} // end of the 'confirmLink()' function

function confirmSubmit(theCommand)
{
    var msg=theCommand;
    return confirm(msg);
}

    
/*
*	This function opens a popup window
*/
function openPop(strFile,nWidth,nHeight,bStatus,bscroll,btoolbar,blocation,bresize,bdirectories,bmenubar) {

window.open(strFile, "help","toolbar=" + btoolbar + ",location=" + blocation + ",directories=" + bdirectories + ",menubar=" + bmenubar + ",status=" + bStatus + ",scrollbars=" + bscroll +",resizable=" + bresize + ",width=" + nWidth + ",height=" + nHeight);

}

/*
*	This function opens a popup window to view an image preview
*/
function imagePreviewWindow(strFile,nWidth,nHeight) {
	
	width 	= parseInt (nWidth) - 2;
	height 	= parseInt (nHeight) - 2;
	
	window.open(strFile, "Image","toolbar=no,location=no,directories=no,menubar=no,status=yes,scrollbars=no,resizable=no,width=" + width + ",height=" + height);

}


/*
*	This function counts the number of characters in a text box and trims if it overruns an allowed maximum value
*/
function textCounter(field, countfield, maxlimit) {

	if (field.value.length > maxlimit) { // if too long...trim it!
	
		field.value = field.value.substring(0, maxlimit);
	
	} else { // otherwise, update 'characters left' counter
	
		countfield.value = maxlimit - field.value.length;
	
	}
}


var DHTML = (document.getElementById || document.all || document.layers);

function getObj(name)
{
  if (document.getElementById)
  {
        this.obj = document.getElementById(name);
        this.style = document.getElementById(name).style;
  }
  else if (document.all)
  {
        this.obj = document.all[name];
        this.style = document.all[name].style;
  }
  else if (document.layers)
  {
        this.obj = document.layers[name];
        this.style = document.layers[name];
  }
}

function show_permisions(flag)
{
        if (!DHTML) return;
        var permissions_1 = new getObj('permissions_1');
        var permissions_2 = new getObj('permissions_2');
        var permissions_3 = new getObj('permissions_3');
        var permissions_4 = new getObj('permissions_4');
        var permissions_5 = new getObj('permissions_5');
        var permissions_6 = new getObj('permissions_6');
        
        permissions_1.style.display = (flag) ? 'none'	: 'inline'
        permissions_2.style.display = (flag) ? 'inline' : 'none'
        permissions_3.style.display = (flag) ? 'none'	: 'inline'
        permissions_4.style.display = (flag) ? 'inline' : 'none'
        permissions_5.style.display = (flag) ? 'none'	: 'inline'
        permissions_6.style.display = (flag) ? 'inline' : 'none'
        
}

function show_postcards_settings(flag)
{
        if (!DHTML) return;
        var postcards_settings = new getObj('postcards_settings');
        postcards_settings.style.display = (flag) ? 'inline' : 'none'
        
}

function show_downloads_settings(flag)
{
        if (!DHTML) return;
        var downloads_settings = new getObj('downloads_settings');
        downloads_settings.style.display = (flag) ? 'inline' : 'none'
        
}



/*
*	Selects all thumbnail checkboxes with name formElement
*/
function selectAllImages(formElement)   { setAllSelectNew(true,formElement); }

/*
*	Deselects all thumbnail checkboxes with name formElement
*/
function deselectAllImages(formElement) { setAllSelectNew(false,formElement); }
	
/*
*	Toggles selection state (boolean) for all checkboxes formElement
*/
function setAllSelectNew(state,formElement) {
	
	var formName	= getThumbnailFormId();
	var elFrm		= document.getElementById(formName);
	
	for (var i = 0; i < elFrm.elements.length; i++) {
		var ourEl = elFrm.elements[i];
		if (ourEl.type == "checkbox")
			if (ourEl.name.indexOf(formElement) != -1)
				ourEl.checked = state;
		}
		
}



/*
*	Selects all thumbnail checkboxes with name formElement
*/
function selectAll(formName,formElement)   { setAllSelect(true,formName,formElement); }

/*
*	Deselects all thumbnail checkboxes with name formElement
*/
function deselectAll(formName,formElement) { setAllSelect(false,formName,formElement); }
	
/*
*	Toggles selection state (boolean) for all checkboxes formElement
*/
function setAllSelect(state,formName,formElement) {
	
	var elFrm = document.getElementById(formName);
	
	for (var i = 0; i < elFrm.elements.length; i++) {
		var ourEl = elFrm.elements[i];
		if (ourEl.type == "checkbox")
			if (ourEl.name.indexOf(formElement) != -1)
				ourEl.checked = state;
		}
		
}

/*
*	Returns array of all checkboxes with the name photoid[]
*/
function getSelectedImages() {
	
	var formName	= getThumbnailFormId();
	var elFrm		= document.getElementById(formName);
	var strParams	= "";	
	
	//If selected, add image Id to the list
	for (var i = 0; i < elFrm.elements.length; i++) {
		var ourEl = elFrm.elements[i];
		if (ourEl.type == "checkbox")
			if (ourEl.checked && (ourEl.name.indexOf("photoid[]") != -1))				 
				strParams = strParams + ourEl.value + ",";					
	}
	//Remove Last delimiter.
	strParams = strParams.substr(0,strParams.length-1);	
	return strParams;
	
}

/*
*	Returns the id of the thumbnail form being viewed
*/
function getThumbnailFormId() {
	
	var thumbsFormSmall = new getObj('image-small');
	var thumbsFormLarge = new getObj('image-large');
	
	if (thumbsFormSmall.style.display == 'none') {
		return 'thumb_grid_large';
	} else {
		return 'thumb_grid_small';
	}
	
}

/*
*	Submits the thumbnail grid add to lightbox form
*/
function addSelectionToLightbox () {

	var formName	= getThumbnailFormId();
	var elFrm		= document.getElementById(formName);
	elFrm.submit();
	
}

/*
*	Submits the thumbnail grid remove from lightbox form
*/
function removeSelectionFromLightbox () {

	var formName	= getThumbnailFormId();
	var elFrm		= document.getElementById(formName);
	elFrm.submit();
	
}

/*
*	Returns the id of the thumbnail form being viewed
*/
function getThumbnailFormId() {
	
	var thumbsFormSmall = new getObj('image-small');
	var thumbsFormLarge = new getObj('image-large');
	
	if (thumbsFormSmall.style.display == 'none') {
		return 'thumb_grid_large';
	} else {
		return 'thumb_grid_small';
	}
	
}

function saveLightbox() {

		var list = document.getElementById('lightbox-cells');
		var items = list.getElementsByTagName('div');
		var ids = '';
			
		for (var i = 0; i < items.length; i++)  {
			if (i > 0) ids += '|';
			var id = items[i].getAttribute('id');
			ids += id;
		}
		
		document.lightbox_edit.lightbox_order.value = ids;
		return true;

}

function getSelectedCheckbox(buttonGroup) {
   // Go through all the check boxes. return an array of all the ones
   // that are selected (their position numbers). if no boxes were checked,
   // returned array will be empty (length will be zero)
   var retArr = new Array();
   var lastElement = 0;
   if (buttonGroup[0]) { // if the button group is an array (one check box is not an array)
      for (var i=0; i<buttonGroup.length; i++) {
         if (buttonGroup[i].checked) {
            retArr.length = lastElement;
            retArr[lastElement] = i;
            lastElement++;
         }
      }
   } else { // There is only one check box (it's not an array)
      if (buttonGroup.checked) { // if the one check box is checked
         retArr.length = lastElement;
         retArr[lastElement] = 0; // return zero as the only array value
      }
   }
   return retArr;
} // Ends the "getSelectedCheckbox" function

function getSelectedCheckboxValue(buttonGroup) {
   // return an array of values selected in the check box group. if no boxes
   // were checked, returned array will be empty (length will be zero)
   var retArr = new Array(); // set up empty array for the return values
   var selectedItems = getSelectedCheckbox(buttonGroup);
   if (selectedItems.length != 0) { // if there was something selected
      retArr.length = selectedItems.length;
      for (var i=0; i<selectedItems.length; i++) {
         if (buttonGroup[selectedItems[i]]) { // Make sure it's an array
            retArr[i] = buttonGroup[selectedItems[i]].value;
         } else { // It's not an array (there's just one check box and it's selected)
            retArr[i] = buttonGroup.value;// return that value
         }
      }
   }
   return retArr;
} // Ends the "getSelectedCheckBoxValue" function


function saveRules() {

	var list = document.getElementById('rules');
	var items = list.getElementsByTagName('div');
	var ids = '';
		
	for (var i = 0; i < items.length; i++)  {
		if (i > 0) ids += '|';
		var id = items[i].getAttribute('id');
		ids += id;
	}
	
	document.rules_edit.rules_order.value = ids;
	return true;

}

function submitActionForm (frm) {
	
	frm = $(frm);
	
	var formAction = $F('form_action').evalJSON();
	
	frm.action	= formAction.url;
	$('cmd').value	= formAction.cmd;
	frm.submit();
}

