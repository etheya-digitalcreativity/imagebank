if(!RJIS) var RJIS = {};

RJIS.settings = {
	
	offset: { x: 2, y: -20 },
	stem: true,
	delay: 300,
	styles: {
		
		border: 5,
		borderColor: '#BFBE77',
		backgroundColor: '#FFFDCE',
		width: 220,
		height: 220
		
	}

};

RJIS.Tooltips = Class.create({

	timeoutId:   	undefined,
	objTip:   		undefined,
	objTipContent:	undefined,
	tipsArray:  [],

	/*
	*	Constructor runs on completion of the DOM loading. 
	*/
	initialize: function (attachClass, objOptions) {
		
		this.attachClass	= attachClass;
		this.objOptions		= objOptions;
		
		this.tipsArray = [];		
		objBody = $$('body')[0];

		objBody.appendChild(Builder.node('div',{id:'dlog',style: 'display:none;' }));
		this.objTip 		= $('dlog');
		
		this.objTip.appendChild(Builder.node('div',{id:'dlog-content'}));
		this.objTipContent 	= $('dlog-content');
		
		this.objTip.appendChild(Builder.node('div',{id:'dlog-stem'}));
		this.objTipStem 	= $('dlog-stem');
				
		this.setupTooltips();
		this.configureSettings();
	},	
	
	/*
	*	Scans the DOM to find all elements with an 'anatips' class attribute and bind them to the 3 events handlers
	*/
	setupTooltips: function() {

		if (!this.objOptions.stem) { this.objTipStem.style.display = 'none'; }

		me = this;
		cnt = 0;
		$$('.'+this.attachClass).each(function(el){				
			if(el.title && el.title.length > 0) {				
				el.observe('mouseover', (function(event) { event.stop; me.handleMouseOver(event) }).bind(this));
				el.observe('mousemove', (function(event) { event.stop; me.handleMouseMove(event) }).bind(this));
				el.observe('mouseout',  (function(event) { event.stop; me.handleMouseOut(event) }).bind(this));
				me.tipsArray[cnt] = el.title;
				el.tip_num = cnt++;
				el.removeAttribute('title');
			}
		})		
	},		
	
	/*
	*	Apply styles to the tooltip
	*/
	configureSettings: function () {
		
		var settings	= this.objOptions;
		var tip			= this.objTip;
		var content		= this.objTipContent;
		
		if (Object.isNumber(settings.styles.border)) {
			tip.style.borderWidth = settings.styles.border + 'px';
		} else {
			tip.style.borderWidth = RJIS.settings.styles.borderWidth + 'px';
		}
		
		if (Object.isString(settings.styles.borderColor)) {
			tip.style.borderColor = settings.styles.borderColor;
		} else {
			tip.style.borderColor = RJIS.settings.styles.borderColor;
		}
		
		if (Object.isString(settings.styles.backgroundColor)) {
			content.style.backgroundColor = settings.styles.backgroundColor;
		} else {
			tip.style.backgroundColor = RJIS.settings.styles.backgroundColor;
		}
		
		if (Object.isNumber(settings.styles.width)) {
			content.style.width = settings.styles.width + 'px';
		} else {
			tip.style.width = RJIS.settings.styles.width + 'px';
		}
		
		if (Object.isNumber(settings.styles.height)) {
			content.style.height = settings.styles.height + 'px';
		} else {
			tip.style.height = RJIS.settings.styles.height + 'px';
		}
		
	},
	
	/*
	*	Called when mouse enters the target : setup and show container
	*/
	handleMouseOver: function(event){
		target = event.findElement('.'+this.attachClass);
		if (target) {
			this.objTipContent.innerHTML = this.tipsArray[target.tip_num];
			this.showTooltip();
			image = event.findElement('.'+this.attachClass+' img'); // Disable inner images's alt attribute (prevents IE auto alt tooltips)
			if(image) image.alt = '';
		}
	},
	
	/*
	*	Show the tooltip
	*/
	showTooltip: function () {
		
		this.timeoutID = setTimeout(function () {
			
			//me.objTip.style.visibility = 'visible';
			Effect.Appear('dlog', {duration:0});
			
			
		} ,'200');
	
	},
	
	/*
	*	Called when mouse moves over the target : move the tip container
	*/
	handleMouseMove: function(event) {
	
		target = event.findElement('.'+this.attachClass);
		
		var cursorOffsetX = (this.objOptions.offset.x > 0) ? this.objOptions.offset.x : 20;
		var cursorOffsetY = (this.objOptions.offset.y > 0) ? this.objOptions.offset.y : 20;
		
		if (target) {
		
			var viewWidth	= document.viewport.getWidth();
			var viewHeight	= document.viewport.getHeight();
			
			var dlogWidth	= $('dlog').getWidth();
			var dlogHeight	= $('dlog').getHeight();
			
			var overflowWidth 	= viewWidth - cursorOffsetX;
			var overflowHeight 	= viewHeight - cursorOffsetY;
			
			var dlogRightMargin = event.pointerX() + dlogWidth + cursorOffsetX;
			var dlogLeftMargin	= event.pointerX() - dlogWidth - cursorOffsetX;
			
			var dlogBottomMargin	= event.pointerY() + dlogHeight + cursorOffsetY;
			var dlogTopMargin 		= event.pointerY() - dlogHeight - cursorOffsetY;
			
			if (dlogRightMargin >= overflowWidth && dlogLeftMargin > 0) {
				this.objTip.style.left	= (event.pointerX() - dlogWidth - cursorOffsetX) + 'px';
			} else {
				this.objTip.style.left	= (event.pointerX() + cursorOffsetX) + 'px';
			}
			
			if (dlogBottomMargin >= overflowHeight && dlogTopMargin > 0) {
				this.objTip.style.top	= (event.pointerY() - dlogHeight - cursorOffsetY) + 'px';
			} else {
				this.objTip.style.top	= (event.pointerY() + cursorOffsetY) + 'px';
			}
		
		}
		
	},
	
	/*
	*	Called when mouse leaves the target : hide the tip container
	*/
	handleMouseOut: function(event){
		clearTimeout(this.timeoutID);
		Effect.Fade(this.objTip, {duration:0});
	}
	
});


