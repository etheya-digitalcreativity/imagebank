var cur;

var checkEvent = function (e)
{
	
	// Add event handler to the label tag
	if (e.target.tagName == "LABEL") {
		if (e.target.id && e.altKey){
			cur = document.getElementById(e.target.id);
			selectAll(cur);
		}

	}
	
	// Add event handler to the checkbox input tag
	if (e.target.tagName == "INPUT" && e.target.type == "checkbox" && (e.button == 0 || e.keyCode == 32))
	  {
		if (cur && e.shiftKey) 
		  selectRange(cur, e.target);
		 else if (e.target && e.altKey)
		  selectAllCheckboxes(e.target);
		cur = e.target;
		
	  }
};
  
var selectAllCheckboxes = function (from) 
{

	var status;
	var inputName 	= from.name;
	var inputs 		= document.getElementsByTagName('input');
	var checkboxes 	= [];
	var i;
	
	for (i = 0; i < inputs.length; i++)
		if (inputs[i].getAttribute("type") == "checkbox")
		checkboxes.push(inputs[i]);
	
	for (i = 0; i < checkboxes.length; i++)
		if (checkboxes[i].getAttribute("name") == inputName) checkboxes[i].checked = from.checked;

}
  
var selectRange = function (from, to)
{
	var inputs 		= document.getElementsByTagName("input");
	var fromName 	= from.name;
	var toName 		= to.name;
	var checkboxes 	= [];
	var last;
	var i;
	for (i = 0; i < inputs.length; i++)
	  if (inputs[i].getAttribute("type") == "checkbox" && inputs[i].getAttribute("name") == fromName && inputs[i].getAttribute("name") == toName)
		checkboxes.push(inputs[i]);
	for (i = 0; i < checkboxes.length; i++)
	  {
		if (checkboxes[i] == to && checkboxes[i].getAttribute("name") == fromName && checkboxes[i].getAttribute("name") == toName)
		  {
			last = from;
			break;
		  }
		if (checkboxes[i] == from && checkboxes[i].getAttribute("name") == fromName && checkboxes[i].getAttribute("name") == toName)
		  {
			last = to;
			break;
		  }
	  }
	for (; i < checkboxes.length; i++)
	  {
		if (checkboxes[i] != from && checkboxes[i] != to && checkboxes[i].getAttribute("name") == fromName && checkboxes[i].getAttribute("name") == toName)
		  {
			if (checkboxes[i].checked != from.checked)
			  {
				var e = document.createEvent("MouseEvents");
				e.initEvent("click", true, false);
				checkboxes[i].dispatchEvent(e);
			  }
		  }
		if (checkboxes[i] == last)
		  break;
	  }
};

var init = function ()
{	
	document.documentElement.addEventListener("click", checkEvent, true);
	document.documentElement.addEventListener("keyup", checkEvent, true);
};

init();