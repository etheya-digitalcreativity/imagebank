/*
*
* DATATABLE OBJECT
*
*/

var dataTableObject = Class.create ( {

	initialize: function() {
		
		this.mode = '';
		this.rowListener();
		this.filterListener();
		
	},
	
	/*
	*
	* 
	*
	*/
	submit: function () {
		
		switch (this.mode) {
		
		case 'add':
			this.saveItem();
		break;
		
		case 'update':
			this.updateItem();
		break;
		
		}
	
	},
	
	/*
	*
	* 
	*
	*/
	addItem: function () {
		
		this.mode = 'add';
		
		new Ajax.Request(this.rpcAddUrl, {
			
			method: 'post',
			onSuccess: function(transport) {
				var dialog = $('dialog-content');
				dialog.update(transport.responseText);
				YAHOO.editor.container.dialog1.show();
			},
			parameters: { 
				cmd: this.rpcAddCmd
			}
		
		});
			
	},
	
	/*
	*
	* 
	*
	*/
	saveItem: function () {
		
		var formDataObject = $(this.dlogFormId).serialize(true);
		
		new Ajax.Request(this.rpcAddUrl, {
			
			method: 'post',
			onSuccess: function(transport) {
			
				if (transport.responseText == 'true') {
					
					YAHOO.editor.container.dialog1.hide();
					
					// Reload the page to show the new record
					window.location.reload( false );
				
				} else {
					
					var dialog = $('dialog-content');
					dialog.update(transport.responseText);
					YAHOO.editor.container.dialog1.show();
				
				}
			},
			parameters: formDataObject
		
		});
		
	},
	
	/*
	*
	* 
	*
	*/
	editItem: function (itemId) {
		
		this.mode = 'update';
		
		new Ajax.Request(this.rpcEditUrl, {
			
			method: 'post',
			onSuccess: function(transport) {
				var dialog = $('dialog-content');
				dialog.update(transport.responseText);
				YAHOO.editor.container.dialog1.show();
			},
			parameters: { 
				cmd: this.rpcEditCmd,
				id: itemId
			}
		
		});
		
	},
	
	/*
	*
	* 
	*
	*/
	updateItem: function () {
		
		var rowId 			= 'row-' + $(this.rowPrimaryKey).value;
		var rowCells 		= $(rowId).childElements();
		var updateCellName 	= new Array();
		var updateCell 		= new Array();
		
		for (i = 0; i < rowCells.length; i = i+1) {
			
			var rel 	= rowCells[i].readAttribute('rel');
			
			if (rel != null) {
			
				updateCell[i] 		= rowCells[i];
				updateCellName[i] 	= rel;
				
			}
			
		}
		
		var formDataObject = $(this.dlogFormId).serialize(true);
		
		new Ajax.Request(this.rpcUpdateUrl, {
			
			method: 'post',
			onSuccess: function(transport) {
				
				if (transport.responseText == 'true') {
					
					YAHOO.editor.container.dialog1.hide();
					
					for (i = 0; i < updateCellName.length; i = i+1) {
						$(updateCell[i]).innerHTML = formDataObject[updateCellName[i]];
					}
					
					new Effect.Highlight(rowId, { startcolor: '#FFFF99' } );
				
				} else {
					
					var dialog = $('dialog-content');
					dialog.update(transport.responseText);
					YAHOO.editor.container.dialog1.show();
				
				}
			},
			parameters: formDataObject
			
		});
		
	},
	
	/*
	*
	* 
	*
	*/
	deleteItem: function (itemId) {
		
		this.deleteItemId = itemId;
		
		YAHOO.namespace("confirm.container");

		// Define various event handlers for Dialog
		var handleYes = function() {
			dt.actionDeleteItem();
			YAHOO.confirm.container.simpledialog1.destroy();
		};
		
		var handleNo = function() {
			YAHOO.confirm.container.simpledialog1.destroy();
		};
				
		// Instantiate the Dialog
		YAHOO.confirm.container.simpledialog1 = new YAHOO.widget.SimpleDialog("simpledialog1", 
																				 { width: "300px",
																				   fixedcenter: true,
																				   visible: false,
																				   draggable: false,
																				   close: true,
																				   modal: true,
																				   text: "Are you sure you want to delete this item?",
																				   constraintoviewport: true,
																				   buttons: [ { text:"Yes", handler:handleYes, isDefault:true },
																							  { text:"No",  handler:handleNo } ]
																				 } );
		YAHOO.confirm.container.simpledialog1.setHeader("Are you sure?");
		
		// Render the Dialog
		YAHOO.confirm.container.simpledialog1.render("html-body");
	
		YAHOO.confirm.container.simpledialog1.show();
	
	},
	
	/*
	*
	* 
	*
	*/
	actionDeleteItem: function () {
		
		var itemId = this.deleteItemId;
		
		new Ajax.Request(this.rpcDeleteUrl, {
			
			method: 'post',
			onSuccess: function(transport) {
			
				var row = 'row-' + itemId;
				var container = $('row-'+itemId).up();
				
				$(row).remove();
				dt.rowHighlighting(container);
				
			},
			parameters: { 
				cmd: this.rpcDeleteCmd,
				id: itemId
			}
		
		});			
		
	},
	
	/*
	*
	* 
	*
	*/
	rowHighlighting: function (container) {
		
		var rows = container.childElements();
		
		for (i = 0; i < rows.length; i = i+1) {
			
			var row = $(rows[i]);
			
			row.className = '';
			
			if ((i % 2) > 0) {
				row.addClassName('row1');
			} else {
				row.addClassName('row2');
			}
			
	    } 
	
	},
	
	/*
	*
	* 
	*
	*/
	rowListener: function () {
		
		var rows = $(this.tableId).down().next().childElements();
		
		for (i = 0; i < rows.length; i = i+1) {
			
			var row = $(rows[i]);
			
			row.observe('dblclick',this.rowClickHandler);
			
	    } 
		
	},
	
	/*
	*
	* 
	*
	*/
	rowClickHandler: function (event) {
		
		var element = event.element();
		
		var rel = element.up().readAttribute('rel');
		
		dt.editItem(rel);
		
		return false;
		
	},
	
	/*
	*
	* 
	*
	*/
	filterListener: function () {
		
		var headerCells = $(this.tableId).down().down().childElements();
		
		for (i = 0; i < headerCells.length; i = i+1) {
			
			if ($(headerCells[i]).down() != null) {
			
				var filterDiv = $(headerCells[i]).down();
				
				filterDiv.observe('click',this.filterHandler);
				
			}
			
	    } 
		
	},
	
	/*
	*
	* 
	*
	*/
	filterHandler: function (event) {
		
		var attrRel 	= Event.element(event).up().readAttribute('rel');
		var dbColumn	= Event.element(event).readAttribute('rel');
		
		var cells 		= $$('#list td[rel]');
		var cellList	= new Array();
		
		for (i = 0; i < cells.length; i = i+1) {
			
			if ($(cells[i]).readAttribute('rel') == attrRel) {
				
				cellList.push(cells[i].innerHTML);
			
			}
		
		}
		
		var cellList = cellList.sort().uniq();

		var filterOptions = '<option value="">Select filter</option>';
		
		for (i = 0; i < cellList.length; i = i+1) {
			
			filterOptions += '<option value="' + cellList[i] + '">' + cellList[i] + '</option>';
			
	    } 
		
		htmlInsert = '<div class="datatable-dialog-filter">';
		htmlInsert += '<form><select name="" onchange="javascript:dt.doFilter(\''+dbColumn+'\',this.options[this.selectedIndex].value);" style="width:100%">'+ filterOptions +'</select></form>';
		htmlInsert += '<br /><a href="javascript:location.href=window.location.pathname;">Clear all filters</a>';
		htmlInsert += '</div>';
		
		YAHOO.namespace("confirm.container");

		var handleNo = function() {
			YAHOO.confirm.container.simpledialog1.hide();
			YAHOO.confirm.container.simpledialog1.destroy();
		};
				
		// Instantiate the Dialog
		YAHOO.confirm.container.simpledialog1 = new YAHOO.widget.SimpleDialog("simpledialog1",  {
			
			width: "300px",
			context:[$(Event.element(event)), "tl", "bl"],
			visible: false,
			draggable: false,
			close: true,
			modal: false,
			constraintoviewport: true
			
		 });
		 
		YAHOO.confirm.container.simpledialog1.setHeader("Filter table by this column");
		YAHOO.confirm.container.simpledialog1.setBody(htmlInsert);
		
		// Render the Dialog
		YAHOO.confirm.container.simpledialog1.render("html-body");
	
		YAHOO.confirm.container.simpledialog1.show();
	
	},
	
	/*
	*
	* 
	*
	*/
	doFilter: function (filterColumn, filterString) {
		
		YAHOO.confirm.container.simpledialog1.hide();
		YAHOO.confirm.container.simpledialog1.destroy();
		
		var queryString = window.location.search.substring(1);
		
		if (queryString == '') { queryString = 'filter=1'; }
		
		queryString += '&fc[]='+filterColumn+'&fs[]='+filterString;
		
		location.href = window.location.pathname + '?' + queryString;
	
	},
	
	/*
	*
	* 
	*
	*/
	liveSearch: function (formId, updateContainer) {
		
		var ajaxRequestUrl = $(formId).readAttribute('action');
		var formDataObject = $(formId).serialize(true);
		
		var queryString = window.location.search.substring(1);
		
		
		// Send request
		new Ajax.Request(ajaxRequestUrl + '?' + queryString, {
			method: 'post',
			onSuccess: function(transport) {
			
				// Reset the list
				$(updateContainer).innerHTML = transport.responseText;
				dt.filterListener();
				
			},
			parameters: formDataObject
			
		});
	
	}
	
});


