function toggleLarge(set_thumb_per_page,set_gallery_per_page) {

	if (document.getElementById('gallery-large')) {
	
		Effect.Appear('gallery-large',{ duration: 0.0 });
		Effect.Fade('gallery-small',{ duration: 0.0 });
	
	}
	
	if (document.getElementById('image-large')) {
	
		Effect.Appear('image-large',{ duration: 0.0 });
		Effect.Fade('image-small',{ duration: 0.0 });
	
	}
	
}

function toggleSmall(set_thumb_per_page,set_gallery_per_page) {

	if (document.getElementById('gallery-large')) {
	
		Effect.Appear('gallery-small',{ duration: 0.0 });
		Effect.Fade('gallery-large',{ duration: 0.0 });
	
	}
	
	if (document.getElementById('image-large')) {
	
		Effect.Appear('image-small',{ duration: 0.0 });
		Effect.Fade('image-large',{ duration: 0.0 });
	
	}

}

/*
*
*	Function to update the preferences cookie
*
*	cookieKey		= integer:	value of item in the prefs to change
*	cookieValue 	= string:	value of the item to be changed
*	cookieDefault 	= string:	defualt cookie data if preferences don't exist yet
*
*/
function updateCookie (cookieKey,cookieValue,cookieDefault) {
	
	cookieKey = parseInt(cookieKey);
	
	var pixariaCookie = readCookie('psg_prefs');
	
	if (!pixariaCookie) {
		
		createCookie('psg_prefs',cookieDefault,360);
		
		var pixariaCookie = cookieDefault;

	}
	
	var cookieElements = pixariaCookie.split("|");
	
	var cookieVersion		= cookieElements[0];
	var imagesPerPage		= cookieElements[1];
	var galleriesPerPage	= cookieElements[2];
	var viewThumbView		= cookieElements[3];
	
	if (cookieKey == 1) {
		cookieData = '1|' + cookieValue + '|' + galleriesPerPage + '|' + viewThumbView;
	}
	
	if (cookieKey == 2) {
		cookieData = '1|' + imagesPerPage + '|' + cookieValue + '|' + viewThumbView;
	}
	
	if (cookieKey == 3) {
		cookieData = '1|' + imagesPerPage + '|' + galleriesPerPage + '|' + cookieValue;
	}
	
	createCookie('psg_prefs',cookieData,360)
	
}

/*
*
*	Function to create a cookie
*
*/
function createCookie(name,value,days) {

	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else var expires = "";
	document.cookie = name+"="+value+expires+"; path=/";

}

/*
*
*	Function to read data from a cookie
*
*/
function readCookie(name) {

	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;

}

/*
*
*	Function to erase a cookie
*
*/
function eraseCookie(name) {

	createCookie(name,"",-1);
	
}
