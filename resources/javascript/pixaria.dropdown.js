// JavaScript Document

startSelect = function() {

	if (document.all&&document.getElementById) {
		
		if (document.getElementById("itsel")) {
		
			imgRoot = document.getElementById("itsel");
		
			for (i=0; i<imgRoot.childNodes.length; i++) {
			
				node = imgRoot.childNodes[i];
				
				if (node.nodeName=="LI") {
				
					node.onmouseover=function() {
					
						this.className+=" over";
					
					}
					
					node.onmouseout=function() {
					
						this.className=this.className.replace(" over", "");
					
					}
				}
	
			}
			
		}
		
		if (document.getElementById("gtsel")) {
		
			gallRoot = document.getElementById("gtsel");
			
			for (i=0; i<gallRoot.childNodes.length; i++) {
			
				node = gallRoot.childNodes[i];
				
				if (node.nodeName=="LI") {
				
					node.onmouseover=function() {
					
						this.className+=" over";
					
					}
					
					node.onmouseout=function() {
					
						this.className=this.className.replace(" over", "");
					
					}
				}
	
			}
			
		}
		
	}

}
window.onload=startSelect;