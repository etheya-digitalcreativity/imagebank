<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "settings";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Check that the user is a SuperUser
pix_authorise_user("administrator");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

$objSettings = new Admin_Settings ();

class Admin_Settings {
	
	var $tab;
	var $cmd;
	
	var $input;
	var $view;
	var $json;
	var $db;
	
	function Admin_Settings () {
	
		global $objEnvData, $smarty;
		
		// Localise global objects
		$this->input	=& $objEnvData;
		$this->view		=& $smarty;
		
		// Load an initialise the database class
		require_once ('class.Database.php');
		$this->db = new Database();
		
		// Load and initialise the JSON class
		require_once (SYS_BASE_PATH . 'resources/classes/class.JSON.php');
		$this->json = new Pixaria_JSON();
		
		// Get the page and tab to view
		list ($this->cmd, $this->tab) = explode ("/", $this->input->formGlobal('cmd'));
		
		/*
		*	Choose which method to run
		*/
		switch ($this->cmd) {
			
			case 'addBannedDomainNames':
				$this->addBannedDomainNames();
			break;
			
			case 'removeBannedDomainNames':
				$this->removeBannedDomainNames();
			break;
			
			case 'applyDataCaptureSettings':
				$this->applyDataCaptureSettings();
			break;
			
			case "applySettings":
				$this->applySettings();
			break;
			
			case "appearance":
				$this->showAppearanceSettings();
			break;
			
			case "login":
				$this->showLoginRegistrationSettings();
			break;
			
			case "regban":
				$this->showBanningSettings();
			break;
			
			case "regdata":
				$this->showDataCaptureSettings();
			break;
			
			case "search":
				$this->showSearchSettings();
			break;
			
			case "general": default:
				$this->showGeneralSettings();
			break;
		
		}
	
	}
	
	/*
	*	
	*/
	function showGeneralSettings () {
		
		require_once ('class.XmlParse.php');
		
		$plistData = file_get_contents(SYS_BASE_PATH . 'resources/local/localisations.xml');
		
		$objXmlParser = new parseXmlData($plistData,array("language"));
		
		$languages = $objXmlParser->xmlarray['localisations']['language'];
		
		if (is_array($languages)) {
			
			$this->view->assign("languages",true);
			
			foreach ($languages as $key => $value) {
				
				if ($value['enabled']) {
				
					$local_name[] = $value['name'];
					$local_code[] = $value['code'];
				
				}
				
			}
			
			$this->view->assign("local_name",$local_name);
			$this->view->assign("local_code",$local_code);
			
		}
		
		// Define the navigation section for the admin toolbar
		$this->view->assign("toolbar","settings");
		
		// Define the active tab to show
		$this->view->assign("active_tab",$this->tab);
		
		// Set the page title
		$this->view->assign("page_title","General Settings");
		
		// Display the html header
		$this->view->display('settings/general.html');
		
	}
	
	/*
	*	
	*/
	function showSearchSettings () {
		
		// Define the navigation section for the admin toolbar
		$this->view->assign("toolbar","settings");
		
		// Define the active tab to show
		$this->view->assign("active_tab",$this->tab);
		
		// Set the page title
		$this->view->assign("page_title","Search Settings");
		
		// Display the html header
		$this->view->display('settings/search.html');
		
	}
	
	/*
	*	
	*/
	function showLoginRegistrationSettings () {
		
		$sql = "SELECT id, domain_name FROM ".PIX_TABLE_DOMN." ORDER BY domain_name ASC";
		
		list ($domain_id, $domain_name) = $this->db->rowsAsColumns($sql);
				
		// Assign the list of domain names to Smarty
		$this->view->assign("domain_id",$domain_id);
		$this->view->assign("domain_name",$domain_name);
		
		// Define the navigation section for the admin toolbar
		$this->view->assign("toolbar","settings");
		
		// Define the active tab to show
		$this->view->assign("active_tab",$this->tab);
		
		// Set the page title
		$this->view->assign("page_title","Registration Settings");
		
		// Display the html header
		$this->view->display('settings/registration.html');
		
	}
	
	/*
	*	
	*/
	function showDataCaptureSettings () {
	
		$result = $this->db->rows("SELECT * FROM ". PIX_TABLE_USDA, MYSQL_ASSOC);
		
		if (is_array($result)) {
		
			//	Feed data into Smarty
			for ($i=0; $i<count($result); $i++) {
			
				$name = $result[$i]['name'];
				
				$this->view->assign($name,$result[$i]['value']);
			
			}
		
		}
		
		// Set the page title
		$this->view->assign("page_title","User information management");
		
		// Display the html header
		$this->view->display('settings/datacapture.html');
		
	}
	
	/*
	*	
	*/
	function showBanningSettings () {
		
		$sql = "SELECT id, domain_name FROM ".PIX_TABLE_DOMN." ORDER BY domain_name ASC";
		
		list ($domain_id, $domain_name) = $this->db->rowsAsColumns($sql);
				
		// Assign the list of domain names to Smarty
		$this->view->assign("domain_id",$domain_id);
		$this->view->assign("domain_name",$domain_name);
		
		// Define the navigation section for the admin toolbar
		$this->view->assign("toolbar","settings");
		
		// Set the page title
		$this->view->assign("page_title","Registration Blocking Settings");
		
		// Display the html header
		$this->view->display('settings/regban.html');
		
	}
	
	/*
	*	
	*/
	function showAppearanceSettings () {
		
		global $cfg;
		
		// Define the navigation section for the admin toolbar
		$this->view->assign("toolbar","settings");
		
		// Define the active tab to show
		$this->view->assign("active_tab",$this->tab);
		
		// Set the page title
		$this->view->assign("page_title","Appearance Settings");
		
		$this->view->assign("theme_html",pix_select_menu(SYS_BASE_PATH . "resources/themes/","theme",$cfg['set']['theme'],FALSE,"select"));
		$this->view->assign("image_watermark_textleft",(50-(strlen(stripslashes($cfg['set']['image_watermark_text'])))));

		// Display the html header
		$this->view->display('settings/appearance.html');
		
	}
	
	/*
	*	
	*/
	function applySettings () {
	
		$settings = $this->input->formGlobal('settings');
		
		foreach ($settings as $key => $value) {
			
			switch ($value) {
				
				case 'checkbox':
					$data = $this->input->formCheckbox($key);
				break;
				
				case 'html':
					$data = $this->input->html($key);
				break;
				
				case 'array':
					$data = base64_encode(serialize($this->input->fetchGlobal($key)));
				break;
				
				default:
					$data = $this->input->fetchRawPost($key);
				break;
			
			}
			
			if ($this->settingsProcessor($key, $data)) {
				
				$changed[] = $key;
				
			}
			
		}
		
		if (is_array($changed)) {
			
			$changed = $this->json->encodeArrayAsJson ($changed);
			
			$this->json->sendJson($changed);
			
		} else {
		
			return false;
			
		}
		
	}
	
	/*
	*	Apply updated settings
	*/
	function settingsProcessor ($name, $value) {
		
		global $cfg;
		
		// SQL to update the database
		$sql = "UPDATE ".PIX_TABLE_SETT." SET value = '".$this->db->mysqlEscape($value)."' WHERE name = '".$this->db->mysqlEscape($name)."'";
		
		// Update the settings in the database
		$this->db->sqlQuery($sql);
		
		// Update settings in the configuration data array
		$cfg['set'][$name] = $value;
		
		// Update settings in the template object
		$this->view->assign("set_" . $name,$value);
		
		return true;

	}
	
	/*
	*	ADD ONE OR MORE DOMAIN NAMES TO BE BANNED
	*/
	function addBannedDomainNames () {
	
		$emails = explode(",",$this->input->fetchGlobal('user_domain'));
		
		if (is_array($emails)) {
		
			foreach ($emails as $value) {
				
				$value = trim($value);
				
				if (preg_match("/@/i",$value)) {
				
					list ($name, $domain) = explode("@",$value);
					
				} else {
				
					$domain = $value;
				
				}
						
				if ($domain != "") {
				
					$this->db->query("INSERT INTO ".PIX_TABLE_DOMN." VALUES('0','".$this->db->escape($domain)."')");
				
				}
			
			}
		
		}
		
		header ("Location: ". SYS_BASE_URL . FILE_ADM_SETTINGS . "?cmd=regban");
	
	}
	
	/*
	*	REMOVE ONE OR MORE DOMAIN NAMES FROM THE BANNED LIST
	*/
	function removeBannedDomainNames () {
		
		$domain_list = $this->input->fetchGlobal('domain_list');
		
		if (is_array($domain_list)) {
		
			foreach ($domain_list as $value) {
			
				$this->db->query("DELETE FROM ".PIX_TABLE_DOMN." WHERE id = '".$this->db->escape($value)."'");
			
			}
		
		}
		
		header ("Location: ". SYS_BASE_URL . FILE_ADM_SETTINGS . "?cmd=regban");
	
	}
	
	/*
	*	SAVE USER REGISTRATION DATA CAPTURE SETTINGS
	*/
	function applyDataCaptureSettings () {
	
		if ($this->input->fetchPost('salutation') == 0) {
			
			$salutation = 0;
			$salutation_req = 0;
			
		} elseif ($this->input->fetchPost('salutation') == 1) {
			
			$salutation = 1;
			$salutation_req = 0;
			
		} elseif ($this->input->fetchPost('salutation') == 2) {
		
			$salutation = 1;
			$salutation_req = 1;
		
		}
		
		if ($this->input->fetchPost('telephone') == 0) {
			
			$telephone = 0;
			$telephone_req = 0;
			
		} elseif ($this->input->fetchPost('telephone') == 1) {
			
			$telephone = 1;
			$telephone_req = 0;
			
		} elseif ($this->input->fetchPost('telephone') == 2) {
		
			$telephone = 1;
			$telephone_req = 1;
		
		}
		
		if ($this->input->fetchPost('mobile') == 0) {
			
			$mobile = 0;
			$mobile_req = 0;
			
		} elseif ($this->input->fetchPost('mobile') == 1) {
			
			$mobile = 1;
			$mobile_req = 0;
			
		} elseif ($this->input->fetchPost('mobile') == 2) {
		
			$mobile = 1;
			$mobile_req = 1;
		
		}
		
		if ($this->input->fetchPost('facsimile') == 0) {
			
			$facsimile = 0;
			$facsimile_req = 0;
			
		} elseif ($this->input->fetchPost('facsimile') == 1) {
			
			$facsimile = 1;
			$facsimile_req = 0;
			
		} elseif ($this->input->fetchPost('facsimile') == 2) {
		
			$facsimile = 1;
			$facsimile_req = 1;
		
		}
		
		if ($this->input->fetchPost('address') == 0) {
			
			$address = 0;
			$address_req = 0;
			
		} elseif ($this->input->fetchPost('address') == 1) {
			
			$address = 1;
			$address_req = 0;
			
		} elseif ($this->input->fetchPost('address') == 2) {
		
			$address = 1;
			$address_req = 1;
		
		}
		
		if ($this->input->fetchPost('business_type') == 0) {
			
			$business_type = 0;
			$business_type_req = 0;
			
		} elseif ($this->input->fetchPost('business_type') == 1) {
			
			$business_type = 1;
			$business_type_req = 0;
			
		} elseif ($this->input->fetchPost('business_type') == 2) {
		
			$business_type = 1;
			$business_type_req = 1;
		
		}
		
		if ($this->input->fetchPost('business_position') == 0) {
			
			$business_position = 0;
			$business_position_req = 0;
			
		} elseif ($this->input->fetchPost('business_position') == 1) {
			
			$business_position = 1;
			$business_position_req = 0;
			
		} elseif ($this->input->fetchPost('business_position') == 2) {
		
			$business_position = 1;
			$business_position_req = 1;
		
		}
		
		if ($this->input->fetchPost('image_interest') == 0) {
			
			$image_interest = 0;
			$image_interest_req = 0;
			
		} elseif ($this->input->fetchPost('image_interest') == 1) {
			
			$image_interest = 1;
			$image_interest_req = 0;
			
		} elseif ($this->input->fetchPost('image_interest') == 2) {
		
			$image_interest = 1;
			$image_interest_req = 1;
		
		}
		
		if ($this->input->fetchPost('frequency') == 0) {
			
			$frequency = 0;
			$frequency_req = 0;
			
		} elseif ($this->input->fetchPost('frequency') == 1) {
			
			$frequency = 1;
			$frequency_req = 0;
			
		} elseif ($this->input->fetchPost('frequency') == 2) {
		
			$frequency = 1;
			$frequency_req = 1;
		
		}
		
		if ($this->input->fetchPost('circulation') == 0) {
			
			$circulation = 0;
			$circulation_req = 0;
			
		} elseif ($this->input->fetchPost('circulation') == 1) {
			
			$circulation = 1;
			$circulation_req = 0;
			
		} elseif ($this->input->fetchPost('circulation') == 2) {
		
			$circulation = 1;
			$circulation_req = 1;
		
		}
		
		if ($this->input->fetchPost('territories') == 0) {
			
			$territories = 0;
			$territories_req = 0;
			
		} elseif ($this->input->fetchPost('territories') == 1) {
			
			$territories = 1;
			$territories_req = 0;
			
		} elseif ($this->input->fetchPost('territories') == 2) {
		
			$territories = 1;
			$territories_req = 1;
		
		}
		
		if ($this->input->fetchPost('website') == 0) {
			
			$website = 0;
			$website_req = 0;
			
		} elseif ($this->input->fetchPost('website') == 1) {
			
			$website = 1;
			$website_req = 0;
			
		} elseif ($this->input->fetchPost('website') == 2) {
		
			$website = 1;
			$website_req = 1;
		
		}
		
		if ($this->input->fetchPost('company_name') == 0) {
			
			$company_name = 0;
			$company_name_req = 0;
			
		} elseif ($this->input->fetchPost('company_name') == 1) {
			
			$company_name = 1;
			$company_name_req = 0;
			
		} elseif ($this->input->fetchPost('company_name') == 2) {
		
			$company_name = 1;
			$company_name_req = 1;
		
		}
		
		if ($this->input->fetchPost('message') == 0) {
			
			$message = 0;
			$message_req = 0;
			
		} elseif ($this->input->fetchPost('message') == 1) {
			
			$message = 1;
			$message_req = 0;
			
		} elseif ($this->input->fetchPost('message') == 2) {
		
			$message = 1;
			$message_req = 1;
		
		}
		
		// Update requested items
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$salutation' WHERE name = 'salutation';";
		$this->db->query($sql);	
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$telephone' WHERE name = 'telephone';";
		$this->db->query($sql);	
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$mobile' WHERE name = 'mobile';";
		$this->db->query($sql);	
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$facsimile' WHERE name = 'facsimile';";
		$this->db->query($sql);	
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$address' WHERE name = 'address';";
		$this->db->query($sql);	
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$business_type' WHERE name = 'business_type';";
		$this->db->query($sql);	
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$business_position' WHERE name = 'business_position';";
		$this->db->query($sql);	
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$image_interest' WHERE name = 'image_interest';";
		$this->db->query($sql);	
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$frequency' WHERE name = 'frequency';";
		$this->db->query($sql);	

		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$circulation' WHERE name = 'circulation';";
		$this->db->query($sql);	

		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$territories' WHERE name = 'territories';";
		$this->db->query($sql);	

		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$website' WHERE name = 'website';";
		$this->db->query($sql);
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$message' WHERE name = 'message';";
		$this->db->query($sql);
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$company_name' WHERE name = 'company_name';";
		$this->db->query($sql);
		
		// Update required items
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$salutation_req' WHERE name = 'salutation_req';";
		$this->db->query($sql);	
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$telephone_req' WHERE name = 'telephone_req';";
		$this->db->query($sql);	
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$mobile_req' WHERE name = 'mobile_req';";
		$this->db->query($sql);	
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$facsimile_req' WHERE name = 'facsimile_req';";
		$this->db->query($sql);	
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$address_req' WHERE name = 'address_req';";
		$this->db->query($sql);	
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$business_type_req' WHERE name = 'business_type_req';";
		$this->db->query($sql);	
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$business_position_req' WHERE name = 'business_position_req';";
		$this->db->query($sql);	
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$image_interest_req' WHERE name = 'image_interest_req';";
		$this->db->query($sql);	
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$frequency_req' WHERE name = 'frequency_req';";
		$this->db->query($sql);	

		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$circulation_req' WHERE name = 'circulation_req';";
		$this->db->query($sql);	

		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$territories_req' WHERE name = 'territories_req';";
		$this->db->query($sql);	

		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$website_req' WHERE name = 'website_req';";
		$this->db->query($sql);
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$message_req' WHERE name = 'message_req';";
		$this->db->query($sql);
		
		$sql = "UPDATE ".PIX_TABLE_USDA." SET value = '$company_name_req' WHERE name = 'company_name_req';";
		$this->db->query($sql);
		
		header ("Location: ". SYS_BASE_URL . FILE_ADM_SETTINGS . "?cmd=regdata");
	
	}
	
}

?>