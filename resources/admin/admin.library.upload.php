<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "images";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Define the navigation section for the admin toolbar
$smarty->assign("toolbar","library");

// Check that the user is a SuperUser
pix_authorise_user("administrator");

// Set the page title
$smarty->assign("page_title","Upload images");

// Catch safe mode problems
if ($cfg['sys']['safe_mode']) {
	$smarty->assign("page_title","Safe mode warning");
	$smarty->pixDisplay('admin.snippets/admin.html.error.safemode.tpl');
	exit;
}
		
switch ($objEnvData->fetchGlobal('cmd')) {
		
	case 1: // Process incoming files and bounce user to the import page
		
		
	break;
	
	default: // Show the image upload form
		
		$process		= aglobal(process);
		$uploadtype		= aglobal(uploadtype);
		
		// We need to make sure that there are no lingering bits from previous uploads:
		if (file_exists(SYS_BASE_PATH.$cfg['set']['temporary']."icon_".$ses[USERID].".jpg")) {
		@unlink(SYS_BASE_PATH.$cfg['set']['temporary']."lthum_".$ses[USERID].".jpg");
		}
		
		if (file_exists(SYS_BASE_PATH.$cfg['set']['temporary']."sthum_".$ses[USERID].".jpg")) {
		@unlink(SYS_BASE_PATH.$cfg['set']['temporary']."lthum_".$ses[USERID].".jpg");
		}
		
		if (file_exists(SYS_BASE_PATH.$cfg['set']['temporary']."lthum_".$ses[USERID].".jpg")) {
		@unlink(SYS_BASE_PATH.$cfg['set']['temporary']."lthum_".$ses[USERID].".jpg");
		}
		
		if (file_exists(SYS_BASE_PATH.$cfg['set']['temporary']."comp_".$ses[USERID].".jpg")) { 
		@unlink(SYS_BASE_PATH.$cfg['set']['temporary']."comp_".$ses[USERID].".jpg");
		}
		
		if (file_exists(SYS_BASE_PATH.$cfg['set']['temporary']."original_".$ses[USERID].".jpg")) { 
		@unlink(SYS_BASE_PATH.$cfg['set']['temporary']."original_".$ses[USERID].".jpg");
		}
		
		
		$php_upload_limit = rtrim(get_cfg_var(upload_max_filesize),'M');
		$php_memory_limit = rtrim(get_cfg_var(memory_limit),'M');
		
		if ($php_upload_limit < ($php_memory_limit/3)) {
		
			$smarty->assign("popp_upload_limit",round($php_upload_limit,0)*1024000);
		
		} else {
		
			$smarty->assign("popp_upload_limit",round(($php_memory_limit/3),0)*1024000);
		
		}
		
		if ($objEnvData->fetchGet('type') == "inline") {
		
			// DISPLAY HTML
			$smarty->pixDisplay('admin.library/library.add.upload.tpl');
			
		} else {
		
			// DISPLAY HTML
			$smarty->pixDisplay('admin.library/library.upload.01.tpl');
			
		}
		
	break;
	
}


?>