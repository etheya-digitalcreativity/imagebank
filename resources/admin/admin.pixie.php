<?php 

/*
*
*	Pixaria Gallery 1.0
*	Copyright 2002 - 2006 Jamie Longstaff
*
*	Script author:		Jamie Longstaff
*	Script version:		1.0	
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "site";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Check that the user is a SuperUser
pix_authorise_user("administrator");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

$objAdminPixie = new AdminPixie();

class AdminPixie {
	
	function AdminPixie () {
		
		global $objEnvData;
		
		switch ($objEnvData->fetchGlobal('cmd')) {
		
			case "showPluginInfo":
				$this->showPluginInfo();
			break;
			
			default:
				$this->showPluginList();
			break;
		
		}
	
	}

	function showPluginList() {
	
		global $smarty, $cfg, $XML_LIST_ELEMENTS;
		
		// Find out what Pixies are installed
		$handle=@opendir(SYS_BASE_PATH."resources/pixies/");
		
		if($handle != FALSE) {
		
			while (false !== ($directory = readdir($handle))) { 
			
				if (substr($directory,0,1) != "." && is_dir(SYS_BASE_PATH."resources/pixies/".$directory)) {
					
					$xml_file		= SYS_BASE_PATH."resources/pixies/".$directory."/".$directory.".xml";
					
					if (file_exists($xml_file)) {
							
						$XML_LIST_ELEMENTS	= array( "Option","OptionAllowedValue","PixieExample" );
						$pixie_data			= pix_xml_tree($xml_file);
						
						$pixie_list_name		= $pixie_data['PixieDefinition']['PixieName'];
						$pixie_list_version		= $pixie_data['PixieDefinition']['PixieVersion'];
					
						$installed_pixie_name[] 	= $pixie_list_name;
						$installed_pixie_version[] 	= $pixie_list_version;
						$installed_pixie_dir[]		= $directory;
					
					}
					
				}
				
			}
			
		}
		@closedir($handle);
		
		// Assign variables to Smarty object
		$smarty->assign("installed_pixie_name",$installed_pixie_name);
		$smarty->assign("installed_pixie_dir",$installed_pixie_dir);
		$smarty->assign("installed_pixie_version",$installed_pixie_version);
		
		// Set the page title
		$smarty->assign("page_title","Pixie Manager");
		
		// Output the library page
		smartyPixDisplay('admin.pixie/pixies.list.tpl');
		
	}
	
	function showPluginInfo() {
	
		global $smarty, $cfg, $objEnvData, $XML_LIST_ELEMENTS;
		
		$pixie_view			= $objEnvData->fetchGlobal('pixie_view');
		
		$pixie_directory	= SYS_BASE_PATH."resources/pixies/".$pixie_view;
		
		// We must have a comp image directory, the others can be made manually
		if (file_exists(SYS_BASE_PATH."resources/pixies/".$pixie_view."/pixie.logic.php")) {
		
			$xml_file		= SYS_BASE_PATH."resources/pixies/".$pixie_view."/".$pixie_view.".xml";
			
			if (file_exists($xml_file)) {
					
				$XML_LIST_ELEMENTS	= array( "Option","OptionAllowedValue","PixieExample" );
				$pixie_data			= pix_xml_tree($xml_file);
				
				$pixie_name			= $pixie_data['PixieDefinition']['PixieName'];
				$pixie_version		= $pixie_data['PixieDefinition']['PixieVersion'];
				$pixie_description	= $pixie_data['PixieDefinition']['PixieDescription'];
				$pixie_warning		= $pixie_data['PixieDefinition']['PixieNotice'];
				$pixie_examples		= $pixie_data['PixieDefinition']['PixieExamples'];
				$pixie_options		= $pixie_data['PixieDefinition']['PixieOptions']['Option'];
							
				// Tell Smarty whether or not to show a pixie
				$smarty->assign("display",(bool)TRUE);
			
			}
		
		}
		
		
		//print_array($pixie_data);exit;
		
		// Assign variables to Smarty object
		$smarty->assign("pixie_view",$pixie_view);
		$smarty->assign("pixie_name",$pixie_name);
		$smarty->assign("pixie_version",$pixie_version);
		$smarty->assign("pixie_description",$pixie_description);
		$smarty->assign("pixie_warning",$pixie_warning);
		$smarty->assign("pixie_examples",$pixie_examples);
		$smarty->assign("pixie_options",$pixie_options);
		
		// Set the page title
		$smarty->assign("page_title","Pixie Manager &raquo; " . $pixie_name);
		
		// Output the library page
		smartyPixDisplay('admin.pixie/pixies.detail.tpl');
		
	}
	
}



?>