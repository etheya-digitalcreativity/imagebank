<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "sales";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Check that the user is a SuperUser
pix_authorise_user("administrator");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Initialise the store administration class
$AdminStore = new AdminStore();

// The store administration class
class AdminStore {
	
	// The class constructor for store administration
	function AdminStore () {
		
		global $objEnvData;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		// Run the correct method for this page
		switch ($objEnvData->fetchGlobal('cmd')) {
		
			case "salesView":
				// Run the method for viewing a list of all transactions
				$this->salesView();
			
			break;
			
			case "salesReportForm":
				// Run the method for showing a sales report builder form
				$this->salesReportForm();
			
			break;
			
			case "salesReportOuput":
				// Run the method for outputting a sales report
				$this->salesReportOuput();
			
			break;
			
			case "cartDelete":
				// Run the method to delete a cart
				$this->cartDelete();
			
			break;
			
			case "cartClear":
				// Run the method to empty a cart
				$this->cartClear();
			
			break;
			
			case "settingsUpdate":
			
				// Run the settings updater method
				$this->settingsUpdate();
			
			break;
			
			case "listImageDownloads":
			
				// List image downloads by user
				$this->listImageDownloads();
			
			break;
			
			case "viewImageDownloadDetail":
			
				// View details of image downloads by a user
				$this->viewImageDownloadDetail();
			
			break;
			
			case "listPhotographerSales":
			
				// List photographers who've made sales
				$this->listPhotographerSales();
			
			break;
			
			case "settingsView": default:
			
				// Run the settings view method
				$this->settingsView();
			
			break;
		
		}
	
	}
	
	/*
	*	Show a list of all transactions
	*/
	function salesView () {
	
		global $objEnvData, $cfg, $ses, $smarty;
	
		$punctuation	= "/[\\.\\,\\-\\+\\(\\)\\~\\!\\$\\%\\^\\&\\*\\@\\#\\<\\>\\?\\=]/";
		
		// Clean up the SQL usable strings
		$sort			= preg_replace($punctuation,"",$objEnvData->fetchGlobal('sort'));
		$method			= preg_replace($punctuation,"",$objEnvData->fetchGlobal('method'));
		$status			= preg_replace($punctuation,"",$objEnvData->fetchGlobal('status'));
		
		// Load the class file
		require_once ('class.PixariaCarts.php');
		
		// Initialise the class
		$objCarts = new PixariaCarts($status, $sort, $method);
		
		// Set the page title
		$smarty->assign("page_title",$objCarts->getStatusDescription());
		
		// Pass the status back to Smarty for column header sorting links
		$smarty->assign("status",$objCarts->getStatus());
		$smarty->assign("status_description",$objCarts->getCartStatusText());
		$smarty->assign("method",$objCarts->getMethod());
		$smarty->assign("carts_present",$objCarts->getCartsPresent());
		$smarty->assign("cart_id",$objCarts->getCartId());
		$smarty->assign("cart_items",$objCarts->getCartItems());
		$smarty->assign("cart_status",$objCarts->getCartStatus());
		$smarty->assign("cart_status_icon",$objCarts->getCartStatusIcon());
		$smarty->assign("cart_view_url",$objCarts->getCartViewUrl());
		$smarty->assign("transaction_id",$objCarts->getTransactionId());
		$smarty->assign("date_processed",$objCarts->getDateProcessed());
		$smarty->assign("date_completed",$objCarts->getDateCompleted());
		$smarty->assign("cart_username",$objCarts->getCartUserName());
		$smarty->assign("cart_user_email",$objCarts->getCartUserEmail());
		$smarty->assign("cart_user_id",$objCarts->getCartUserId());
		
		// Output html from template file
		$smarty->pixDisplay('admin.sales/admin.sales.tpl');
	
	}
	
	/*
	*	Output a CSV export of all the sales
	*/
	function listPhotographerSales () {
	
		global $smarty, $cfg, $objEnvData;
		
		/*
		*	First we need to get a list of photographers
		*/
		
		$sql = "SELECT ".PIX_TABLE_USER.".userid, ".PIX_TABLE_USER.".email_address, ".PIX_TABLE_USER.".first_name, ".PIX_TABLE_USER.".family_name
		
				FROM ".PIX_TABLE_USER." 
		
				LEFT JOIN ".PIX_TABLE_SGME." ON ".PIX_TABLE_USER.".userid = ".PIX_TABLE_SGME.".sys_userid
				
				LEFT JOIN ".PIX_TABLE_IMGS." ON ".PIX_TABLE_USER.".userid = ".PIX_TABLE_IMGS.".image_userid
				
				LEFT JOIN ".PIX_TABLE_CAME." ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_CAME.".image_id
				
				WHERE ".PIX_TABLE_SGME.".sys_group_id = '3'
				
				GROUP BY ".PIX_TABLE_USER.".userid
				
				ORDER BY ".PIX_TABLE_USER.".family_name";
		
		$result = $this->_dbl->sqlSelectRows($sql);
		
		if (is_array($result)) {
		
			foreach ($result as $key => $value) {
			
				$photographer_userid[]	= $value['userid'];
				$photographer_email[]	= $value['email_address'];
				$photographer_name[]	= $value['family_name'] . ", " . $value['first_name'];
			
			}
		
		}
		
		$date_month 	= $objEnvData->fetchGlobal('date_Month');
		$date_year		= $objEnvData->fetchGlobal('date_Year');
		$userid			= $objEnvData->fetchGlobal('userid');
		$show_all		= $objEnvData->fetchGlobal('show_all');
		
		if (is_numeric($date_month) && is_numeric($date_year)) {
			
			$date_str 		= "$date_year-$date_month";
			
		} else {
		
			$date_str 		= strftime('%Y-%m');
			$date_month		= strftime('%m');
			$date_year		= strftime('%Y');
		
		}
		
		if ($show_all != "on") {
			$date_sql = "AND DATE_FORMAT(".PIX_TABLE_CART.".date_completed,'%Y-%m') = '$date_str'";
		} else {
			$date_sql = "";
		}
		
		$sql = "SELECT ".PIX_TABLE_IMGS.".*,
		
				".PIX_TABLE_CRME.".item_type,
				
				".PIX_TABLE_CRME.".price,
				
				".PIX_TABLE_CRME.".shipping,
				
				".PIX_TABLE_CRME.".shipping_multiple,
				
				".PIX_TABLE_CRME.".quantity, 
				
				".PIX_TABLE_CRME.".quote_text, 
				
				".PIX_TABLE_CART.".cart_id,
				
				".PIX_TABLE_CART.".date_completed
				
				FROM ".PIX_TABLE_CRME."
				
				LEFT JOIN ".PIX_TABLE_IMGS." ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_CRME.".image_id
				
				LEFT JOIN ".PIX_TABLE_CART." ON ".PIX_TABLE_CRME.".cart_id = ".PIX_TABLE_CART.".cart_id
				
				LEFT JOIN ".PIX_TABLE_USER." ON ".PIX_TABLE_USER.".userid = ".PIX_TABLE_IMGS.".image_userid
				
				WHERE ".PIX_TABLE_USER.".userid = '$userid' 
				
				AND ".PIX_TABLE_CART.".status = '4'
				
				$date_sql
				
				ORDER BY ".PIX_TABLE_CART.".date_completed DESC";
		
		$result = $this->_dbl->sqlSelectRows($sql);
		
		$total 			= 0;
		$total_physical = 0;
		$total_digital 	= 0;
		
		if (is_array($result)) {
		
			foreach ($result as $key => $row) {
			
				$image_id[]			= $row['image_id'];
				$image_active[]		= $row['image_active'];
				$image_filename[] 	= $row['image_filename'];
				$image_path[]		= $row['image_path'];
				$image_date[]		= $row['image_date'];
				
				$icon_path[]		= base64_encode($cfg['sys']['base_library'].$row['image_path']."/32x32/".$row['image_filename']);
				$icon_size			= @getimagesize($cfg['sys']['base_library'].$row['image_path']."/32x32/".$row['image_filename']);
				$icon_width[]		= $icon_size[0];
				$icon_height[]		= $icon_size[1];
				
				$comp_path[]		= base64_encode($cfg['sys']['base_library'].$row['image_path']."/".COMPING_DIR."/".$row['image_filename']);
				$comp_size			= @getimagesize($cfg['sys']['base_library'].$row['image_path']."/".COMPING_DIR."/".$row['image_filename']);
				$comp_width[]		= $comp_size[0];
				$comp_height[]		= $comp_size[1];
				
				$image_width[]		= $row['image_width'];
				$image_height[]		= $row['image_height'];
				
				$image_title[]		= $row['image_title'];
				
				$price[]				= $row['price'];
				$item_type[]			= $row['item_type'];
				$date_completed[]		= $row['date_completed'];
				$shipping[]				= $row['shipping'];
				$shipping_multiple[]	= $row['shipping_multiple'];
				$quantity[]				= $row['quantity'];
				$cart_id[]				= $row['cart_id'];
				$quote_text[]			= $row['quote_text'];
				$item_total[]			= $row['quantity'] * $row['price'];
				
				if ($row['item_type'] == "physical") { $total_physical = $total_physical + ($row['quantity'] * $row['price']); }
				if ($row['item_type'] == "digital") { $total_physical = $total_physical + ($row['quantity'] * $row['price']); }
				$total = $total + ($row['quantity'] * $row['price']);
				
			}
			
			$smarty->assign("sales",true);
			
		}
		
		// Assign image information to Smarty
		$smarty->assign("image_id",$image_id);
		$smarty->assign("image_active",$image_active);
		$smarty->assign("image_filename",$image_filename);
		$smarty->assign("image_path",$image_path);
		$smarty->assign("image_date",$image_date);
		$smarty->assign("icon_path",$icon_path);
		$smarty->assign("icon_width",$icon_width);
		$smarty->assign("icon_height",$icon_height);
		$smarty->assign("comp_path",$comp_path);
		$smarty->assign("comp_width",$comp_width);
		$smarty->assign("comp_height",$comp_height);
		$smarty->assign("image_width",$image_width);
		$smarty->assign("image_height",$image_height);
		$smarty->assign("image_title",$image_title);
		
		$smarty->assign("total",number_format($total));
		$smarty->assign("total_physical",number_format($total_physical));
		$smarty->assign("total_digital",number_format($total_digital));
		$smarty->assign("price",$price);
		$smarty->assign("date_completed",$date_completed);
		$smarty->assign("item_type",$item_type);
		$smarty->assign("shipping",$shipping);
		$smarty->assign("shipping_multiple",$shipping_multiple);
		$smarty->assign("quantity",$quantity);
		$smarty->assign("cart_id",$cart_id);
		$smarty->assign("quote_text",$quote_text);
		$smarty->assign("item_total",$item_total);
		
		$smarty->assign("photographer_userid",$photographer_userid);
		$smarty->assign("userid",$userid);
		$smarty->assign("email_address",$photographer_email);
		$smarty->assign("show_all",$show_all);
		$smarty->assign("photographer_name",$photographer_name);
		$smarty->assign("time","$date_year-$date_month-01");

		
		// Set the page title
		$smarty->assign("page_title","Sales by photographers");
		
		// Display the html header
		$smarty->pixDisplay('admin.sales/sales.photographers.list.tpl');
		
		// Stop running script
		exit;
	
	}
	
	/*
	*	Output a CSV export of all the sales
	*/
	function salesReportOuput () {
	
		global $objEnvData, $cfg, $ses, $smarty;
	
		$from_year 	= $objEnvData->fetchPost('from_Year');
		$from_month = $objEnvData->fetchPost('from_Month');
		$from_day 	= $objEnvData->fetchPost('from_Day');
		
		$to_year 	= $objEnvData->fetchPost('to_Year');
		$to_month	= $objEnvData->fetchPost('to_Month');
		$to_day 	= $objEnvData->fetchPost('to_Day');
		
		$from_date 	= "$from_year-$from_month-$from_day 00:00:00";
		$to_date	= "$to_year-$to_month-$to_day 23:59:59";
		
		// Load class file
		require_once ("class.PixariaCartList.php");
		
		// Instantiate class
		$objCarts	= new PixariaCartList();
		
		// Get cart items
		$objCarts->cartsByDateRange($from_date,$to_date);
		
		foreach ($objCarts->cart_info as $key => $value) {
			
			if ($objEnvData->fetchPost('inc_userid')) {
				$csv_data	.= "\"".$objCarts->cart_info[$key]['userid']."\",";
			}
			
			if ($objEnvData->fetchPost('inc_name')) {
				$csv_data	.= "\"".$objCarts->cart_info[$key]['full_name']."\",";
			}
			
			if ($objEnvData->fetchPost('inc_first_name')) {
				$csv_data	.= "\"".$objCarts->cart_info[$key]['first_name']."\",";
			}
			
			if ($objEnvData->fetchPost('inc_family_name')) {
				$csv_data	.= "\"".$objCarts->cart_info[$key]['family_name']."\",";
			}
			
			if ($objEnvData->fetchPost('inc_address')) {
				$csv_data	.= "\"".$objCarts->cart_info[$key]['addr1']." ".
								$objCarts->cart_info[$key]['addr2']." ".
								$objCarts->cart_info[$key]['addr3']."\",";
			}
			
			if ($objEnvData->fetchPost('inc_country')) {
				$csv_data	.= "\"".$objCarts->cart_info[$key]['country']."\",";
			}
			
			if ($objEnvData->fetchPost('inc_post_code')) {
				$csv_data	.= "\"".$objCarts->cart_info[$key]['postal_code']."\",";
			}
			
			if ($objEnvData->fetchPost('inc_telephone')) {
				$csv_data	.= "\"".$objCarts->cart_info[$key]['telephone']."\",";
			}
			
			if ($objEnvData->fetchPost('inc_fax')) {
				$csv_data	.= "\"".$objCarts->cart_info[$key]['fax']."\",";
			}
			
			if ($objEnvData->fetchPost('inc_email')) {
				$csv_data	.= "\"".$objCarts->cart_info[$key]['email_address']."\",";
			}
			
			if ($objEnvData->fetchPost('inc_sub_total')) {
				$csv_data	.= "\"".$objCarts->cart_info[$key]['subtotal']."\",";
			}
			
			if ($objEnvData->fetchPost('inc_tax_total')) {
				$csv_data	.= "\"".$objCarts->cart_info[$key]['tax_total']."\",";
			}
			
			if ($objEnvData->fetchPost('inc_shippng')) {
				$csv_data	.= "\"".$objCarts->cart_info[$key]['shipping']."\",";
			}
			
			if ($objEnvData->fetchPost('inc_total')) {
				$csv_data	.= "\"".$objCarts->cart_info[$key]['total']."\",";
			}
			
			if ($objEnvData->fetchPost('inc_txn_id')) {
				$csv_data	.= "\"".$objCarts->cart_info[$key]['remote_txn_id']."\",";
			}
			
			if ($objEnvData->fetchPost('inc_date_completed')) {
				$csv_data	.= "\"".$objCarts->cart_info[$key]['date_completed']."\",";
			}
			
			// Trim off the trailing comma
			$csv_data  = substr($csv_data,0,-1);
			
			// Append a semi-colon to the end of each line
			$csv_data .= "\n";
		
		}
		
		// Send forced download headers
		header("Content-Description: File Transfer");
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=transaction_data.csv");
		
		// Output the CSV data
		print($csv_data);
		
		// Exit
		exit;

	}
	
	/*
	*	Show the form to build a sales report
	*/
	function salesReportForm () {
	
		global $objEnvData, $cfg, $ses, $smarty;
	
		// Set the page title
		$smarty->assign("page_title","Sales reports");

		// Output html from template file
		$smarty->pixDisplay('admin.sales/admin.sales.report.tpl');
	
	}
	
	/*
	*	Delete an entire cart and all the items in it
	*/
	function cartDelete () {
		
		global $objEnvData, $cfg, $ses, $smarty;
	
		$cart_id = $objEnvData->fetchGlobal('cart_id');
		
		if ($objEnvData->fetchGlobal('js_confirmed') == 1) {

			// Load the class file
			require_once ('class.PixariaCart.php');
			
			// Initialise the class
			$objCart = new PixariaCart($cart_id);
		
			// Delete the cart
			$objCart->deleteCart();
		
		}
		
		// Display the sales viewer again
		$this->salesView();
		
		// Stop running script
		exit;
	
	}
	
	/*
	*	Clear the contents of a cart
	*/
	function cartClear () {
	
		global $objEnvData, $cfg, $ses, $smarty;
	
		$cart_id = $objEnvData->fetchGlobal('cart_id');
		
		if ($objEnvData->fetchGlobal('js_confirmed') == 1) {

			// Load the class file
			require_once ('class.PixariaCart.php');
			
			// Initialise the class
			$objCart = new PixariaCart($cart_id);
		
			// Empty the cart
			$objCart->emptyCart();
		
		}
		
		// Display the sales viewer again
		$this->salesView();
		
		// Stop running script
		exit;
	
	}
	
	/*
	*	Apply updated settings
	*/
	function settingsProcessor ($name, $value) {
		
		global $smarty, $cfg;
		
		// Update the settings in the database
		@mysql_query ("UPDATE ".$cfg['sys']['table_sett']." SET value = '$value' WHERE name = '$name';");
		
		// Update settings in the configuration data array
		$cfg['set'][$name] = $value;
		
		// Update settings in Smarty object
		$smarty->assign("set_" . $name,$value);

	}
	
	/*
	*	Process updated settings information
	*/
	function settingsUpdate () {
		
		global $objEnvData, $cfg, $ses, $smarty;
	
		$func_store						= $objEnvData->fetchPost('func_store');
		$func_store_download			= $objEnvData->fetchPost('func_store_download');
		$store_download_attempts		= $objEnvData->fetchPost('store_download_attempts');
		$store_type						= $objEnvData->fetchPost('store_type');
		$store_images_forsale			= $objEnvData->fetchPost('store_images_forsale');
		$store_flatrate_ship			= $objEnvData->fetchPost('store_flatrate_ship');
		$store_flatrate_ship_val		= getSignificantFigure($objEnvData->fetchPost('store_flatrate_ship_val'));
		$store_default_price			= getSignificantFigure($objEnvData->fetchPost('store_default_price'));
		$store_default_tax				= $objEnvData->fetchPost('store_default_tax');
		$store_currency					= $objEnvData->fetchPost('store_currency');
		$store_func_paypal				= $objEnvData->fetchPost('store_func_paypal');
		$store_paypal_email				= $objEnvData->fetchPost('store_paypal_email');
		$store_func_2co					= $objEnvData->fetchPost('store_func_2co');
		$store_2co_sid					= $objEnvData->fetchPost('store_2co_sid');
		$store_2co_demo					= $objEnvData->fetchPost('store_2co_demo');
		$store_2co_secret				= $objEnvData->fetchPost('store_2co_secret');
		$store_quote_option				= $objEnvData->fetchPost('store_quote_option');
		$store_shipping_message			= addslashes($objEnvData->fetchPost('store_shipping_message'));
		$store_tax_number				= addslashes($objEnvData->fetchPost('store_tax_number'));
		$store_calculate_login			= addslashes($objEnvData->fetchPost('store_calculate_login'));
		$store_offline_payment			= addslashes($objEnvData->fetchPost('store_offline_payment'));
		$func_downloads					= $objEnvData->fetchPost('func_downloads');
		$func_downloads_user			= $objEnvData->fetchPost('func_downloads_user');
		$func_download_comp				= $objEnvData->fetchPost('func_download_comp');
		$store_func_cheque				= $objEnvData->fetchPost('store_func_cheque');
		$store_func_postalorder			= $objEnvData->fetchPost('store_func_postalorder');
		$store_func_moneyorder			= $objEnvData->fetchPost('store_func_moneyorder');
		$store_func_banktransfer		= $objEnvData->fetchPost('store_func_banktransfer');
		$store_func_terms_conditions	= $objEnvData->fetchPost('store_func_terms_conditions');
		$store_terms_conditions			= $objEnvData->fetchPost('store_terms_conditions');
		
		// If 2Checkout is on and we have an incompatible currency, don't change the setting
		if ($store_func_2co == "on" && ($store_currency == "PLN" || $store_currency == "CZK" || $store_currency == "SGD" || $store_currency == "HUF")) {
			
			// Check the currency isn't already set to one that 2Checkout doesn't support
			if ($cfg['set']['store_currency'] == "PLN" || $cfg['set']['store_currency'] == "CZK" || $cfg['set']['store_currency'] == "SGD" || $cfg['set']['store_currency'] == "HUF") {
				// Change the currency to US Dollars
				$store_currency = "USD";
			} else {
				// Don't change the store currency
				$store_currency = $cfg['set']['store_currency'];
			}
			
		}
		
		if ($func_store == "on") 				{ $func_store = 1; 				} else { $func_store = 0; }
		if ($func_store_download == "on") 		{ $func_store_download = 1; 	} else { $func_store_download = 0; }
		if ($store_func_paypal == "on") 		{ $store_func_paypal = 1; 		} else { $store_func_paypal = 0; }
		if ($store_flatrate_ship == "on") 		{ $store_flatrate_ship = 1; 	} else { $store_flatrate_ship = 0; }
		if ($store_quote_option == "on") 		{ $store_quote_option = 1; 		} else { $store_quote_option = 0; }
		if ($store_calculate_login == "on") 	{ $store_calculate_login = 1; 	} else { $store_calculate_login = 0; }	
		if ($store_func_2co == "on") 			{ $store_func_2co = 1;			} else { $store_func_2co = 0; }
		if ($store_2co_demo == "on") 			{ $store_2co_demo = 1;			} else { $store_2co_demo = 0; }
		if ($func_downloads == "on") 			{ $func_downloads = 1; 			} else { $func_downloads = 0; }
		if ($func_download_comp == "on") 		{ $func_download_comp = 1; 		} else { $func_download_comp = 0; }
		
		if ($store_func_cheque == "on"):			$store_func_cheque = 1;				else:	$store_func_cheque = 0;				endif;
		if ($store_func_postalorder == "on"):		$store_func_postalorder = 1;		else:	$store_func_postalorder = 0;		endif;
		if ($store_func_moneyorder == "on"):		$store_func_moneyorder = 1;			else:	$store_func_moneyorder = 0;			endif;
		if ($store_func_banktransfer == "on"):		$store_func_banktransfer = 1;		else:	$store_func_banktransfer = 0;		endif;
		if ($store_func_terms_conditions == "on"):	$store_func_terms_conditions = 1;	else:	$store_func_terms_conditions = 0;	endif;
		
		if (isset($store_type)) {
			$this->settingsProcessor("store_type",$store_type);
		}
		
		if (isset($store_images_forsale)) {
			$this->settingsProcessor("store_images_forsale",$store_images_forsale);
		}
		
		if (isset($store_flatrate_ship)) {
			$this->settingsProcessor("store_flatrate_ship",$store_flatrate_ship);
		}
		
		if (isset($store_flatrate_ship_val) && is_numeric($store_flatrate_ship_val) && $store_flatrate_ship_val >= 0) {
			$this->settingsProcessor("store_flatrate_ship_val",$store_flatrate_ship_val);
		}
		
		if (isset($store_default_price) && is_numeric($store_default_price) && $store_default_price >= 0) {
			$this->settingsProcessor("store_default_price",$store_default_price);
		}
		
		if (isset($store_default_tax) && is_numeric($store_default_tax)) {
			$this->settingsProcessor("store_default_tax",$store_default_tax);
		}
		
		if (isset($func_store)) {
			$this->settingsProcessor("func_store",$func_store);
		}
				
		if (isset($func_store_download)) {
			$this->settingsProcessor("func_store_download",$func_store_download);
		}
				
		if (isset($store_currency)) {
			$this->settingsProcessor("store_currency",$store_currency);
		}
				
		if (isset($store_func_paypal)) {
			$this->settingsProcessor("store_func_paypal",$store_func_paypal);
		}
				
		if (isset($store_paypal_email)) {
			$this->settingsProcessor("store_paypal_email",$store_paypal_email);
		}
				
		if (isset($store_func_2co)) {
			$this->settingsProcessor("store_func_2co",$store_func_2co);
		}
				
		if (isset($store_2co_sid)) {
			$this->settingsProcessor("store_2co_sid",$store_2co_sid);
		}
				
		if (isset($store_2co_demo)) {
			$this->settingsProcessor("store_2co_demo",$store_2co_demo);
		}
				
		if (isset($store_2co_secret)) {
			$this->settingsProcessor("store_2co_secret",$store_2co_secret);
		}
				
		if (isset($store_download_attempts)) {
			$this->settingsProcessor("store_download_attempts",$store_download_attempts);
		}
				
		if (isset($store_shipping_message)) {
			$this->settingsProcessor("store_shipping_message",$store_shipping_message);
		}
				
		if (isset($store_quote_option)) {
			$this->settingsProcessor("store_quote_option",$store_quote_option);
		}
		
		if (isset($store_tax_number)) {
			$this->settingsProcessor("store_tax_number",$store_tax_number);
		}
				
		if (isset($store_calculate_login)) {
			$this->settingsProcessor("store_calculate_login",$store_calculate_login);
		}
				
		if (isset($store_offline_payment)) {
			$this->settingsProcessor("store_offline_payment",$store_offline_payment);
		}
		
		if (isset($store_func_cheque)) {
			$this->settingsProcessor("store_func_cheque",$store_func_cheque);
		}
		
		if (isset($store_func_cheque)) {
			$this->settingsProcessor("store_func_postalorder",$store_func_postalorder);
		}
		
		if (isset($store_func_cheque)) {
			$this->settingsProcessor("store_func_moneyorder",$store_func_moneyorder);
		}
		
		if (isset($store_func_cheque)) {
			$this->settingsProcessor("store_func_banktransfer",$store_func_banktransfer);
		}
		
		if (isset($func_downloads_user)) {
			$this->settingsProcessor("func_downloads_user",$func_downloads_user);
		}
		
		if (isset($func_downloads)) {
			$this->settingsProcessor("func_downloads",$func_downloads);
		}
		
		if (isset($func_downloads)) {
			$this->settingsProcessor("func_download_comp",$func_download_comp);
		}
		
		if (isset($store_func_terms_conditions)) {
			$this->settingsProcessor("store_func_terms_conditions",$store_func_terms_conditions);
		}
		
		if (isset($store_terms_conditions)) {
			$this->settingsProcessor("store_terms_conditions",$store_terms_conditions);
		}
		
		// Now display the settings page again
		$this->settingsView();
		
	}
	
	/*
	*	Show store settings
	*/
	function settingsView () {
	
		global $objEnvData, $cfg, $ses, $smarty;
	
		// Define the navigation section for the admin toolbar
		$smarty->assign("toolbar","settings");
		
		// Set the url of the page to send the form data to
		$smarty->assign("output_next_href",$cfg['sys']['base_url'] . $cfg['fil']['admin_store']."?cmd=1");	
			
		// Set the page title
		$smarty->assign("page_title","Store control panel");
		
		$smarty->assign("store_shipping_messageleft",255 - strlen($cfg['set']['store_shipping_message']));
		
		$smarty->assign("store_tax_number_left",255 - strlen($cfg['set']['store_tax_number']));
		
		$smarty->assign("store_offline_paymentleft",1000 - strlen($cfg['set']['store_offline_payment']));
		
		// Display the html header
		$smarty->pixDisplay('settings/store.tpl');
		
		// Stop running script
		exit;
		
	}
	
	function listImageDownloads () {
		
		global $smarty, $cfg, $objEnvData;
		
		$date_month 	= $objEnvData->fetchGlobal('date_Month');
		$date_year		= $objEnvData->fetchGlobal('date_Year');
		$transaction	= $objEnvData->fetchGlobal('transaction');
		
		if (is_numeric($date_month) && is_numeric($date_year)) {
			
			$date_sql 		= "$date_year-$date_month";
			
		} else {
		
			$date_sql 		= strftime('%Y-%m');
			$date_month		= strftime('%m');
			$date_year		= strftime('%Y');
		
		}
		
		if ($transaction == "free") {
			$transaction_sql = "AND transaction = 'free'";
		} else {
			$transaction_sql = "AND transaction = 'pay'";
		}
		
		$sql = "SELECT *, COUNT(".PIX_TABLE_USER.".userid) AS 'download_count'
				
				FROM ".PIX_TABLE_DLOG."
				
				LEFT JOIN ".PIX_TABLE_USER." ON ".PIX_TABLE_USER.".userid = ".PIX_TABLE_DLOG.".userid
				
				WHERE DATE_FORMAT(time,'%Y-%m') = '$date_sql'
				
				AND ".PIX_TABLE_USER.".email_address != ''

				$transaction_sql
				
				GROUP BY ".PIX_TABLE_USER.".userid
				
				ORDER BY ".PIX_TABLE_USER.".userid ASC";
		
		$result = $this->_dbl->sqlSelectRows($sql);
		
		if (is_array($result)) {
		
			foreach ($result as $key => $value) {
			
				$userid[]			= $value['userid'];
				$download_count[]	= $value['download_count'];
				$username[]			= $value['family_name'] . ", " . $value['first_name'];
				$detail_url[]		= $cfg['sys']['base_url'].$cfg['fil']['admin_store'] . "?cmd=viewImageDownloadDetail&amp;transaction=$transaction&amp;time=".mktime(0,0,0,$date_month,"1",$date_year)."&amp;userid=" . $value['userid'];
			
			}
		
		}
		
		// Load variables into Smarty
		$smarty->assign("month",strftime("%B %Y",mktime(0,0,0,$date_month,"1",$date_year)));
		$smarty->assign("time","$date_year-$date_month-01");
		$smarty->assign("transaction",$transaction);
		$smarty->assign("userid",$userid);
		$smarty->assign("detail_url",$detail_url);
		$smarty->assign("download_count",$download_count);
		$smarty->assign("username",$username);
		
		// Set the page title
		$smarty->assign("page_title","Image Download Log");
		
		// Display the html header
		$smarty->pixDisplay('admin.sales/admin.downloads.list.tpl');
		
		// Stop running script
		exit;
		
	}
	
	function viewImageDownloadDetail () {
	
		global $smarty, $cfg, $objEnvData;
		
		$time 			= $objEnvData->fetchGlobal('time');
		$userid			= $objEnvData->fetchGlobal('userid');
		$transaction	= $objEnvData->fetchGlobal('transaction');
		
		if (is_numeric($time)) {
			
			$date_sql 		= strftime("%Y",$time)."-".strftime("%m",$time);
			$date_month		= strftime('%m',$time);
			$date_year		= strftime('%Y',$time);
			
		} else {
		
			$date_sql 		= strftime('%Y-%m');
			$date_month		= strftime('%m');
			$date_year		= strftime('%Y');
		
		}
		
		if ($transaction == "free") {
			$transaction_sql = "AND transaction = 'free'";
		} else {
			$transaction_sql = "AND transaction = 'pay'";
		}
		
		$sql = "SELECT *, COUNT(".PIX_TABLE_IMGS.".image_id) AS 'download_count'
				
				FROM ".PIX_TABLE_DLOG."
				
				LEFT JOIN ".PIX_TABLE_IMGS." ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_DLOG.".image_id
				
				LEFT JOIN ".PIX_TABLE_USER." ON ".PIX_TABLE_USER.".userid = ".PIX_TABLE_DLOG.".userid
				
				WHERE DATE_FORMAT(time,'%Y-%m') = '$date_sql'
				
				AND ".PIX_TABLE_USER.".email_address != ''

				AND ".PIX_TABLE_DLOG.".userid = '$userid'
				
				$transaction_sql
				
				GROUP BY ".PIX_TABLE_IMGS.".image_id
				
				ORDER BY ".PIX_TABLE_DLOG.".time ASC";
		
		$result = $this->_dbl->sqlSelectRows($sql);
		
		if (count($result) > 0 && is_array($result)) {
		
			foreach ($result as $row) {
			
				$image_id[]			= $row['image_id'];
				$image_filename[] 	= $row['image_filename'];
				$image_path[]		= $row['image_path'];
				$image_date[]		= $row['image_date'];
				$icon_path[]		= base64_encode($cfg['sys']['base_library'].$row[image_path]."/32x32/".$row[image_filename]);
				$icon_size			= getimagesize($cfg['sys']['base_library'].$row[image_path]."/32x32/".$row[image_filename]);
				$icon_width[]		= $icon_size[0];
				$icon_height[]		= $icon_size[1];
				$comp_path[]		= base64_encode($cfg['sys']['base_library'].$row[image_path]."/".COMPING_DIR."/".$row[image_filename]);
				$comp_size			= getimagesize($cfg['sys']['base_library'].$row[image_path]."/".COMPING_DIR."/".$row[image_filename]);
				$comp_width[]		= $comp_size[0];
				$comp_height[]		= $comp_size[1];
				$image_width[]		= $row[image_width];
				$image_height[]		= $row[image_height];
				$image_title[]		= $row[image_title];
				$download_count[]	= $row['download_count'];
				$image_price[]		= $row['image_price'];
				$format_type[]		= $row['type'];
				
			}
			
			// Assign image information to Smarty
			$smarty->assign("image_id",$image_id);
			$smarty->assign("image_filename",$image_filename);
			$smarty->assign("image_path",$image_path);
			$smarty->assign("image_date",$image_date);
			$smarty->assign("icon_path",$icon_path);
			$smarty->assign("icon_width",$icon_width);
			$smarty->assign("icon_height",$icon_height);
			$smarty->assign("comp_path",$comp_path);
			$smarty->assign("comp_width",$comp_width);
			$smarty->assign("comp_height",$comp_height);
			$smarty->assign("image_width",$image_width);
			$smarty->assign("image_height",$image_height);
			$smarty->assign("image_title",$image_title);
			$smarty->assign("download_count",$download_count);
			$smarty->assign("image_price",$image_price);
			$smarty->assign("format_type",$format_type);
		
		}
		
		// Load the user controller class
		require_once ("class.PixariaUser.php");
		
		// Initiate the user object
		$objUser = new PixariaUser($userid);
		
		// Load data into Smarty
		$smarty->assign("username",$objUser->getName());
		$smarty->assign("email",$objUser->getEmail());
		$smarty->assign("userid",$userid);
		$smarty->assign("month",strftime("%B %Y",mktime(0,0,0,$date_month,"1",$date_year)));
		
		
		// Set the page title
		$smarty->assign("page_title","Image Download Log");
		
		// Display the html header
		$smarty->pixDisplay('admin.sales/admin.downloads.detail.tpl');
		
		// Stop running script
		exit;
		
	}
	
}

?>