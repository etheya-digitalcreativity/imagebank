<?php 

/*
*
*	Pixaria Gallery 1.0
*	Copyright 2002 - 2006 Jamie Longstaff
*
*	Script author:		Jamie Longstaff
*	Script version:		1.0	
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "content";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;
		
switch($objEnvData->fetchGlobal('cmd')) {
	
	case 2:
	
		// Load form field name into Smarty
		$smarty->assign("name",aglobal(name));
		
		// Get the directory to search for
		$directory = aglobal(directory);
	
		$sql = "SELECT * FROM ".$cfg['sys']['table_imgs']." WHERE image_path = '$directory'";
		
		$result = sql_select_rows($sql);
			
		if (is_array($result) && count($result) > 0) {
		
			foreach($result as $key => $value) {
			
				$small_path[]	= base64_encode($cfg['sys']['base_library'].$value['image_path']."/80x80/".$value['image_filename']);
				$small_size		= getimagesize($cfg['sys']['base_library'].$value['image_path']."/80x80/".$value['image_filename']);
				$small_width[]	= $small_size[0];
				$small_height[]	= $small_size[1];
				
				$large_path[]	= base64_encode($cfg['sys']['base_library'].$value['image_path']."/160x160/".$value['image_filename']);
				$large_size		= getimagesize($cfg['sys']['base_library'].$value['image_path']."/160x160/".$value['image_filename']);
				$large_width[]	= $large_size[0];
				$large_height[]	= $large_size[1];
								
				$thumb_name[]	= $value['image_filename'];
				$thumb_id[]		= $value['image_id'];
				
			}
		
		}
		
		// Set some text to show as the results title
		$results_count = count($thumb_id);
		
		if ($results_count < 1) {
		$smarty->assign("results_title","Your search did not match any results");
		$smarty->assign("results_fail",TRUE);
		} elseif ($results_count == 1) {
		$smarty->assign("results_title","Your search found one image");
		} else {
		$smarty->assign("results_title","Your search found ".$results_count." images");
		}		
		
		$smarty->assign("small_path",$small_path);
		$smarty->assign("small_name",$small_name);
		$smarty->assign("small_width",$small_width);
		$smarty->assign("small_height",$small_height);

		$smarty->assign("large_path",$large_path);
		$smarty->assign("large_name",$large_name);
		$smarty->assign("large_width",$large_width);
		$smarty->assign("large_height",$large_height);

		$smarty->assign("thumb_id",$thumb_id);
		
		// Display the html header
		smartyPixDisplay('admin.image/image.find.02.tpl');
	
	break;
	
	case 1:
	
		// Load form field name into Smarty
		$smarty->assign("name",aglobal(name));
		
		// Get the gallery_id to search for
		$gallery_id = aglobal(gallery_id);
	
		// Get date search parameters
		$keywords_any		= aglobal(keywords_any);
		$keywords_all		= aglobal(keywords_all);
		$keywords_not		= aglobal(keywords_not);
		$srch_date_st			= aglobal(srch_date_st);
		$srch_date_en			= aglobal(srch_date_en);
		$date_srch				= aglobal(date_srch);
		$srch_date_is_dd		= str_pad(aglobal(srch_date_is_dd),2,"00",STR_PAD_LEFT);
		$srch_date_is_mm		= str_pad(aglobal(srch_date_is_mm),2,"00",STR_PAD_LEFT);
		$srch_date_is_yy		= aglobal(srch_date_is_yy);
		$srch_date_st_dd		= str_pad(aglobal(srch_date_st_dd),2,"00",STR_PAD_LEFT);
		$srch_date_st_mm		= str_pad(aglobal(srch_date_st_mm),2,"00",STR_PAD_LEFT);
		$srch_date_st_yy		= aglobal(srch_date_st_yy);
		$srch_date_en_dd		= str_pad(aglobal(srch_date_en_dd),2,"00",STR_PAD_LEFT);
		$srch_date_en_mm		= str_pad(aglobal(srch_date_en_mm),2,"00",STR_PAD_LEFT);
		$srch_date_en_yy		= aglobal(srch_date_en_yy);
		
		// Search the database
		$result = pix_image_search();
		
		if (is_array($result) && count($result) > 0) {
		
			foreach($result as $key => $value) {
			
				$small_path[]	= base64_encode($cfg['sys']['base_library'].$value['image_path']."/80x80/".$value['image_filename']);
				$small_size		= getimagesize($cfg['sys']['base_library'].$value['image_path']."/80x80/".$value['image_filename']);
				$small_width[]	= $small_size[0];
				$small_height[]	= $small_size[1];
				
				$large_path[]	= base64_encode($cfg['sys']['base_library'].$value['image_path']."/160x160/".$value['image_filename']);
				$large_size		= getimagesize($cfg['sys']['base_library'].$value['image_path']."/160x160/".$value['image_filename']);
				$large_width[]	= $large_size[0];
				$large_height[]	= $large_size[1];
								
				$thumb_name[]	= $value['image_filename'];
				$thumb_id[]		= $value['0'];
				
			}
		
		}
		
		// Set some text to show as the results title
		$results_count = count($thumb_id);
		
		if ($results_count < 1) {
		$smarty->assign("results_title","Your search did not match any results");
		$smarty->assign("results_fail",TRUE);
		} elseif ($results_count == 1) {
		$smarty->assign("results_title","Your search found one image");
		} else {
		$smarty->assign("results_title","Your search found ".$results_count." images");
		}		
		
		$smarty->assign("small_path",$small_path);
		$smarty->assign("small_name",$small_name);
		$smarty->assign("small_width",$small_width);
		$smarty->assign("small_height",$small_height);

		$smarty->assign("large_path",$large_path);
		$smarty->assign("large_name",$large_name);
		$smarty->assign("large_width",$large_width);
		$smarty->assign("large_height",$large_height);

		$smarty->assign("thumb_id",$thumb_id);
		
		// Display the html header
		smartyPixDisplay('admin.image/image.find.02.tpl');
	
	break;
	
	default:
	
		// Load form field name into Smarty
		$smarty->assign("name",aglobal(name));
		
		$sql = "SELECT image_id, image_path FROM ".$cfg['sys']['table_imgs']." GROUP BY image_path";
		
		$result = sql_select_rows($sql);
		
		if (is_array($result)) {
			
			foreach ($result as $key => $value) {
			
				$image_path[]	= $value['image_path'];
				$image_id[]		= $value['image_id'];
				
			}
			
		}
		
		$smarty->assign("image_path",$image_path);
		$smarty->assign("image_id",$image_id);
		
		// Load an array of nested categories
		$gallery_list = galleryListingArray();
		
		// Load the category list data into Smarty
		$smarty->assign("menu_gallery_title",$gallery_list[0]);
		$smarty->assign("menu_gallery_id",$gallery_list[1]);

		// Get array of date menu information
		$date_menus = pix_date_menu();
		
		// Load date information into Smarty for the menus
		$smarty->assign("years",$date_menus[0]);
		$smarty->assign("months",$date_menus[1]);
		$smarty->assign("days",$date_menus[2]);
		$smarty->assign("this_year",$date_menus[3]);
		$smarty->assign("this_month",$date_menus[4]);
		$smarty->assign("this_day",$date_menus[5]);
		
		// Display the html header
		smartyPixDisplay('admin.image/image.find.01.tpl');
	
	break;

}

/*
*
*	This function searches the database for images and returns an array of image data
*	The function takes two optional arguments to limit the number of results returned
*
*	$results_lim_sta = Point at which to start saving the results
*	$results_lim_num = Number of results to return in this query
*
*/

function pix_image_search ($results_lim_sta="",$results_lim_num="") {
	
	// The following variables are all required for this function
	global $keywords_any, $keywords_all, $keywords_not, $gallery_id, $cfg;
	global $srch_date_st, $srch_date_en, $date_srch, $srch_date_is_dd, $srch_date_is_mm, $srch_date_is_yy;
	global $srch_date_st_dd, $srch_date_st_mm, $srch_date_st_yy, $srch_date_en_dd, $srch_date_en_mm, $srch_date_en_yy;
	
	// For boolean searches, set which indices should be searched
	
	$match_sql = "MATCH(image_keywords";
	
	if ($cfg['set']['search_caption']) { // Include image captions
	
		$match_sql .= ", image_caption";
	
	}
	
	if ($cfg['set']['search_title']) { // Include image titles
	
		$match_sql .= ", image_title";
	
	}
	
	if ($cfg['set']['search_filename']) { // Include image filenames
	
		$match_sql .= ", image_filename";
	
	}
	
	$match_sql .= ")";
	
	// Add 'QQ' prefix to any three letter keywords
	$keywords_any = keywordsEncode($keywords_any);
	$keywords_all = keywordsEncode($keywords_all);
	$keywords_not = keywordsEncode($keywords_not);
	
	if ($results_lim_sta || $results_lim_num) { $limit = "LIMIT $results_lim_sta,$results_lim_num"; }
	
	// Create an array of punctuation to remove from the keyword fields
	$punctuation = array(",","'","\"","\\","/",";","\$","%","&","(",")","{","}","[","]","+","*","<",">");
	
	// Get the optional keywords to search for
	$keywords_any_local = str_replace($punctuation,"",$keywords_any);
	
	// Get the mendatory keywords to search for
	$keywords_all_local = str_replace($punctuation,"",$keywords_all);
	
	// Get the forbidden keywords to search for
	$keywords_not_local = str_replace($punctuation,"",$keywords_not);
	
	if ($gallery_id != "0") {
	
		$gallery_sql = " AND ( gallery_id = '$gallery_id'";
		
		// Get an array of sub galleries of the one being searched for
		$gallery_array = galleryListSubgalleries($gallery_id);
		
		if (is_array($gallery_array)) {
			
			foreach($gallery_array as $key => $value) {
			
				$gallery_sql .= " OR gallery_id = '$value'";
			
			}
			
		}
		
		$gallery_sql .= " ) ";
	
	}
	
	if ($model_release == "on") {
		
		$gallery_sql .= " AND image_model_release = '1' ";
		
	}
	
	if ($property_release == "on") {
		
		$gallery_sql .= " AND image_property_release = '1' ";
		
	}
	
	/*
	
	Date search types:
	
	$date_srch = 10 - Don't search by date
	$date_srch = 11 - Search for a single date
	$date_srch = 12 - Search a range of dates
	
	*/
	
	// If the user is entering a date search, build some SQL to insert into the query
	if (!$srch_date_st && !$srch_date_en) {
		
		if ($date_srch == 11) {
		
		$srch_date_query	= " AND image_date >= '$srch_date_is_yy-$srch_date_is_mm-$srch_date_is_dd 00:00:00'  AND image_date <= '$srch_date_is_yy-$srch_date_is_mm-$srch_date_is_dd 23:59:59' ";
		
		} elseif ($date_srch == 12) {

		$srch_date_query	= " AND image_date >= '$srch_date_st_yy-$srch_date_st_mm-$srch_date_st_dd 00:00:00' AND image_date <= '$srch_date_en_yy-$srch_date_en_mm-$srch_date_en_dd 00:00:00' ";
		
		}
	
	}
	
	if ($keywords_all_local == "" && $keywords_any_local == "" && $keywords_not_local == "") {  // NO KEYWORDS
		
		if ($gallery_id != "0" && $date_srch != "10") {
			
			// Get the SQL
			$sql = "SELECT ".$cfg['sys']['table_imgs'].".*,".$cfg['sys']['table_gord'].".gallery_id as gallery_id
					
					FROM ".$cfg['sys']['table_imgs']."
					
					LEFT JOIN ".$cfg['sys']['table_gord']." ON ".$cfg['sys']['table_gord'].".image_id = ".$cfg['sys']['table_imgs'].".image_id
					
					LEFT JOIN `".$cfg['sys']['table_imvi']."` ON ".$cfg['sys']['table_imgs'].".image_id = ".$cfg['sys']['table_imvi'].".image_id
				
					WHERE gallery_id
					
					$gallery_sql
					
					$srch_date_query
					
					".$cfg['set']['image_access_sql']."
					
					GROUP BY ".$cfg['sys']['table_imgs'].".image_id
					
					ORDER BY image_date ASC
					
					$limit";
			
			// Date search within a gallery (NO KEYWORDS)
			$result = sql_select_rows($sql);
		
		} elseif ($date_srch != "10") {
			
			$sql = "SELECT * FROM ".$cfg['sys']['table_imgs']."
			
					LEFT JOIN `".$cfg['sys']['table_imvi']."` ON ".$cfg['sys']['table_imgs'].".image_id = ".$cfg['sys']['table_imvi'].".image_id
				
					WHERE ".$cfg['sys']['table_imgs'].".image_id != '' $srch_date_query
					
					".$cfg['set']['image_access_sql']."
					
					GROUP BY ".$cfg['sys']['table_imgs'].".image_id
					
					ORDER BY image_date ASC
					
					$limit";
			
			// Date search whole library (NO KEYWORDS)
			$result = sql_select_rows($sql);
		
		} else {
		
			// Don't do a search (NO KEYWORDS)
			
		}
				
	} elseif (!$cfg['set']['search_boolean']) { // We can't do fulltext Boolean searches but we have keywords
				
		if((!$keywords_any_local) || ($keywords_any_local == "")) { $keywords_any_local = ""; }  
		
		if ($gallery_id == 0) {
		
			$sql = "SELECT ".$cfg['sys']['table_imgs'].".*,
			
					$match_sql
			
					AGAINST ('$keywords_any_local')
					
					AS score FROM ".$cfg['sys']['table_imgs']."
			
					WHERE $match_sql
					
					AGAINST ('$keywords_any_local')
					
					$srch_date_query
					
					".$cfg['set']['image_access_sql']."
					
					GROUP BY ".$cfg['sys']['table_imgs'].".image_id
					
					ORDER BY score DESC
					
					$limit";
		
			// Search for images (need to replace with more powerful search tool)
			$result = sql_select_rows($sql);
		
		} else {
		
			$sql = "SELECT ".$cfg['sys']['table_imgs'].".*,".$cfg['sys']['table_gord'].".gallery_id
			
					AS gallery_id,
					
					$match_sql
					
					AGAINST ('$keywords_any_local')
					
					AS score FROM ".$cfg['sys']['table_imgs']."
					
					LEFT JOIN ".$cfg['sys']['table_gord']." ON ".$cfg['sys']['table_gord'].".image_id = ".$cfg['sys']['table_imgs'].".image_id
					
					LEFT JOIN `".$cfg['sys']['table_imvi']."` ON ".$cfg['sys']['table_imgs'].".image_id = ".$cfg['sys']['table_imvi'].".image_id
				
					WHERE $match_sql
					
					AGAINST ('$keywords_any_local') $srch_date_query
					
					$gallery_sql
					
					".$cfg['set']['image_access_sql']."
					
					GROUP BY ".$cfg['sys']['table_imgs'].".image_id
					
					ORDER BY score DESC
					
					$limit";
			
			// Search for results in one gallery only
			$result = sql_select_rows($sql);
		
		}
		
	} else { // Keywords present and in Boolean mode
	
		// Set up the required keyword syntax for MySQL
		if((!$keywords_any_local) || ($keywords_any_local == "")) { $keywords_any_local = ""; }  
		
		if((!$keywords_all_local) || ($keywords_all_local == "")) {
		
			$keywords_all_local = "";
		
		} else {
		
			// Load keywords into an array
			$keywords_all_array = explode(" ",$keywords_all_local);
		
			if (is_array($keywords_all_array)) {
				
				$keywords_all_local = "";
				
				foreach ($keywords_all_array as $key => $value) {
				
					$keywords_all_local .= "+".$value." ";
					
				}
				
				rtrim($keywords_all_local);
				
			} else {
			
				$keywords_all_local = "+".$keywords_all_local;
			
			}
			
		} 
			
		if((!$keywords_not_local) || ($keywords_not_local == "")) {
		
			$keywords_not_local = "";
		
		} else {
		
			// Load keywords into an array
			$keywords_not_array = explode(" ",$keywords_not_local);
		
			if (is_array($keywords_not_array)) {
				
				$keywords_not_local = "";
				
				foreach ($keywords_not_array as $key => $value) {
				
					$keywords_not_local .= "-".$value." ";
					
				}
				
				rtrim($keywords_not_local);
				
			} else {
			
				$keywords_not_local = "-".$keywords_not_local;
			
			}
			
		} 
		
		if ($gallery_id == 0) {
			
			$sql = "SELECT ".$cfg['sys']['table_imgs'].".*, $match_sql
					
					AGAINST ('$keywords_all_local $keywords_not_local $keywords_any_local' IN BOOLEAN MODE)
					
					AS score FROM ".$cfg['sys']['table_imgs']."
					
					LEFT JOIN `".$cfg['sys']['table_imvi']."` ON ".$cfg['sys']['table_imgs'].".image_id = ".$cfg['sys']['table_imvi'].".image_id
				
					WHERE $match_sql
					
					AGAINST ('$keywords_all_local $keywords_not_local $keywords_any_local' IN BOOLEAN MODE)
					
					$srch_date_query
					
					".$cfg['set']['image_access_sql']."
					
					GROUP BY ".$cfg['sys']['table_imgs'].".image_id
					
					ORDER BY score DESC
					
					$limit";
			
			// Search for images (need to replace with more powerful search tool)
			$result = sql_select_rows($sql);
		
		} else {
			
			$sql = "SELECT ".$cfg['sys']['table_imgs'].".*,".$cfg['sys']['table_gord'].".gallery_id
			
					AS gallery_id,
					
					$match_sql
					
					AGAINST ('$keywords_all_local $keywords_not_local $keywords_any_local' IN BOOLEAN MODE)
					
					AS score FROM ".$cfg['sys']['table_imgs']."
					
					LEFT JOIN ".$cfg['sys']['table_gord']." ON ".$cfg['sys']['table_gord'].".image_id = ".$cfg['sys']['table_imgs'].".image_id
					
					LEFT JOIN `".$cfg['sys']['table_imvi']."` ON ".$cfg['sys']['table_imgs'].".image_id = ".$cfg['sys']['table_imvi'].".image_id
				
					WHERE $match_sql
					
					AGAINST ('$keywords_all_local $keywords_not_local $keywords_any_local' IN BOOLEAN MODE) $srch_date_query
					
					$gallery_sql
					
					".$cfg['set']['image_access_sql']."
					
					GROUP BY ".$cfg['sys']['table_imgs'].".image_id
					
					ORDER BY score DESC
					
					$limit";
			
			// Search for results in one gallery only
			$result = sql_select_rows($sql);
		
		}
	
	}
		
	// Pass back the array of results
	return $result;
	
}

?>