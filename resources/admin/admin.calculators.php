<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "products";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Check that the user is a SuperUser
pix_authorise_user("administrator");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Initialise the page
$Admin_Calculators = new Admin_Calculators();

/*
*
*/
class Admin_Calculators {
	
	var $_dbl;
	var $action_complete = false;
	
	/*
	*
	*/
	function Admin_Calculators () {
	
		global $objEnvData, $cfg, $ses, $smarty;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		switch ($objEnvData->fetchGlobal('cmd')) {
			
			case "deletePricingRule":
				$this->deletePricingRule();
			break;
			
			case "actionEditPricingRule":
				$this->actionEditPricingRule();
			break;
			
			case "showFormEditPricingRule":
				$this->showFormEditPricingRule();
			break;
			
			case "showFormAddNewRuleOption":
				$this->showFormAddNewRuleOption();
			break;
			
			case "actionAddNewRuleOption":
				$this->actionAddNewRuleOption();
			break;
			
			/*
			*	Editing options
			*/
			case "showFormEditOption":
				$this->showFormEditOption();
			break;
			case "formPreviewOptionEdits":
				$this->formPreviewOptionEdits();
			break;
			case "actionSaveOptionEdits":
				$this->actionSaveOptionEdits();
			break;
			
			/*
			*	Delete a pricing option
			*/
			case "actionDeletePricingOption":
				$this->actionDeletePricingOption();
			break;
			
			/*
			*	Creating and previewing new rules
			*/
			case "showFormPreviewNewRule":
				$this->showFormPreviewNewRule();
			break;
			
			case "actionSaveNewRule":
				$this->actionSaveNewRule();
			break;
			
			/*
			*	Test price rules
			*/
			case "showFormPriceRuleTest":
				$this->showFormPriceRuleTest();
			break;
			
			case "actionPriceRuleTest":
				$this->actionPriceRuleTest();
			break;
			
			/*
			*	Change order
			*/
			case "saveRuleOrder":
				$this->saveRuleOrder();
			break;
			
			/*
			*	Rule and option list view (Default)
			*/
			default:
				$this->showCalculationRules();
			break;
		
		}
		
	}
	
	/*
	*
	*
	*
	*/
	function showCalculationRules () {
		
		global $smarty, $cfg, $ses, $objEnvData;
		
		$view	= $objEnvData->fetchGlobal('view');
		
		$rules	= sql_select_rows("SELECT * FROM ".PIX_TABLE_CAOB." ORDER BY calc_order ASC");
		
		// Tell Smarty if we're going to show the list after a completed action
		$smarty->assign("action_complete",$this->action_complete);
		
		if (is_array($rules)) {
		
			foreach ($rules as $key => $value) {
			
				$calc_id[]			= $value['calc_id'];
				$calc_name[]		= $value['calc_name'];
				$calc_description[] = $value['calc_description'];
				$calc_active[]		= (bool)$value['calc_active'];
				
				$options = array();
				
				$options = $this->_dbl->sqlSelectRows("SELECT * FROM ".PIX_TABLE_CAAR." WHERE ar_calc_id = '".$value['calc_id']."' ORDER BY ar_name ASC");
				
				$ar_id			= array();
				$ar_name		= array();
				$ar_operand		= array();
				$ar_factor		= array();
				$ar_function	= array();
					
				if (is_array($options)) {
					
					foreach($options as $opt_key => $opt_value) {
					
						$ar_id[]		= $opt_value['ar_id']; 
						$ar_name[]		= $opt_value['ar_name']; 
						$ar_operand[]	= $opt_value['ar_operand']; 
						$ar_factor[]	= $opt_value['ar_factor']; 
					
						switch ($opt_value['ar_operand']) {
						
							case 0:
								$ar_function[] = "Price = Previous Price";
							break;
							
							case 1:
								$ar_function[] = "Price = Previous Price + " . $cfg['set']['store_symbol'] . $opt_value['ar_factor'];
							break;
							
							case 2:
								$ar_function[] = "Price = Previous Price - " . $cfg['set']['store_symbol'] . $opt_value['ar_factor'];
							break;
							
							case 3:
								$ar_function[] = "Price = Previous Price &times; " . $opt_value['ar_factor'];
							break;
							
							case 4:
								$ar_function[] = "Price = Previous Price &divide; " . $opt_value['ar_factor'];
							break;
							
							case 5:
								$ar_function[] = "Price = " . $cfg['set']['store_symbol'] . $opt_value['ar_factor'];
							break;
							
						}
					
					}
				
					$calc_options_show[] = (bool)TRUE;
				
				} else {
				
					$calc_options_show[] = (bool)FALSE;
					
				}
				
				$calc_ar_id[]		= $ar_id;
				$calc_ar_name[]		= $ar_name;
				$calc_ar_operand[]	= $ar_operand;
				$calc_ar_factor[]	= $ar_factor;
				$calc_ar_function[]	= $ar_function;
				
			}
			
			// Tell Smarty whether or not to show the lsit of rules
			$smarty->assign("show_list",(bool)TRUE);
			
			// Assign list informaiton to Smarty
			$smarty->assign("calc_description",$calc_description);
			$smarty->assign("calc_active",$calc_active);
			$smarty->assign("calc_name",$calc_name);
			$smarty->assign("calc_id",$calc_id);
			$smarty->assign("calc_options_show",$calc_options_show);

			$smarty->assign("calc_ar_id",$calc_ar_id);
			$smarty->assign("calc_ar_name",$calc_ar_name);
			$smarty->assign("calc_ar_operand",$calc_ar_operand);
			$smarty->assign("calc_ar_factor",$calc_ar_factor);
			$smarty->assign("calc_ar_function",$calc_ar_function);
			
		}
		
		if ($view == "") {
		
			// Set the page title
			$smarty->assign("page_title","Price calculators");
			
			// Display the category editor page
			$smarty->pixDisplay('admin.calculators/calculators.list.tpl');
		
		} elseif ($view == "sort") {
		
			// Set the page title
			$smarty->assign("page_title","Re-order price rules");
			
			// Display the category editor page
			$smarty->pixDisplay('admin.calculators/calculators.reorder.tpl');
		
		}
	
	}
	
	/*
	*
	*
	*
	*/
	function showFormPreviewNewRule () {
		
		global $smarty, $cfg, $ses, $objEnvData;
		
		// Process form items
		$calc_name			= stripslashes($objEnvData->fetchGlobal('calc_name'));
		$calc_description	= stripslashes($objEnvData->fetchGlobal('calc_description'));
		$calc_download_size	= $objEnvData->fetchGlobal('calc_download_size');
		
		// Check for missing name
		if ($calc_name == "") {
		
			$problem			= (bool)TRUE;
			$problem_message[]	= "You must enter a short name for this rule which will be used in the add to cart form.";
		
		}
		
		// Check for missing description
		if ($calc_description == "") {
		
			$problem			= (bool)TRUE;
			$problem_message[]	= "You must enter a description for this rule.";
		
		}
		
		$smarty->assign("calc_name",$calc_name);
		$smarty->assign("calc_description",$calc_description);
		$smarty->assign("calc_download_size",$calc_download_size);
		
		// Assign any error messages to Smarty
		$smarty->assign("problem",$problem);
		$smarty->assign("problem_message",$problem_message);
		
		// Set the page title
		$smarty->assign("page_title","Check the details of this question");
		
		// Display the category editor page
		smartyPixDisplay('admin.calculators/calculator.rule.preview.tpl');
	
	}
	
	/*
	*
	*
	*
	*/
	function actionSaveNewRule () {
	
		global $smarty, $cfg, $ses, $objEnvData;
		
		// Process form items
		$calc_name			= stripslashes($objEnvData->fetchGlobal('calc_name'));
		$calc_description	= stripslashes($objEnvData->fetchGlobal('calc_description'));
		$calc_download_size	= $objEnvData->fetchGlobal('calc_download_size');
		
		// If the calc_name value isn't empty add this new rule to the system
		if ($calc_name != "") {
			
			if ($calc_download_size == "on") { $type = "1"; } else { $type = "0"; }
			
			// SQL for the new item
			$sql = "INSERT INTO ".PIX_TABLE_CAOB." VALUES (	'0',
															'".mysql_real_escape_string($calc_name)."',
															'".mysql_real_escape_string($calc_description)."',
															'0',
															'100',
															'".mysql_real_escape_string($type)."');";
			
			// Insert the new item into the database
			$this->_dbl->sqlQuery($sql);
			
			$this->action_complete = true;
			
		}
		
		$this->showCalculationRules();

	}
	
	/*
	*
	*
	*
	*/
	function deletePricingRule () {
	
		global $smarty, $cfg, $ses, $objEnvData;
		
		// Get the id of the rule to delete
		$calc_id = $objEnvData->fetchGlobal('calc_id');
		
		if ($objEnvData->fetchGlobal('js_confirmed') == 1) {
		
			// Delete this rule
			$this->_dbl->sqlQuery("DELETE FROM ".PIX_TABLE_CAOB." WHERE calc_id = '$calc_id'");
			
			// Delete the options inside this rule
			$this->_dbl->sqlQuery("DELETE FROM ".PIX_TABLE_CAAR." WHERE ar_calc_id = '$calc_id'");
		
			$this->action_complete = true;
			
		}
		
		$this->showCalculationRules();
		
	}
	
	/*
	*
	*
	*
	*/
	function showFormEditPricingRule () {
	
		global $smarty, $cfg, $ses, $objEnvData;
		
		// Tell Smarty if we're going to show the list after a completed action
		$smarty->assign("action_complete",$this->action_complete);
		
		// Process form items
		$calc_id			= $objEnvData->fetchGlobal('calc_id');
		
		// Get details of this calculation
		$sql				= "SELECT * FROM ".PIX_TABLE_CAOB." WHERE calc_id = '$calc_id'";
		$result				= $this->_dbl->sqlSelectRow($sql);
		
		// Load information from MySQL
		$calc_name			= $result['calc_name'];
		$calc_description	= $result['calc_description'];
		$calc_type			= $result['calc_type'];
		
		$sql				= "SELECT * FROM ".PIX_TABLE_CAAR." WHERE ar_calc_id = '$calc_id' ORDER BY ar_name ASC";
		$result				= $this->_dbl->sqlSelectRows($sql);
		
		$ar_id			= array();
		$ar_name		= array();
		$ar_description = array();
		$ar_active		= array();
		$ar_operand		= array();
		$ar_factor		= array();
		
		if (is_array($result)) {
		
			foreach ($result as $key => $value) {
			
				$ar_id[]			= $value['ar_id'];
				$ar_name[]			= $value['ar_name'];
				$ar_description[]	= $value['ar_description'];
				$ar_active[]		= $value['ar_active'];
				
				switch ($value['ar_operand']) {
				
					case 0:
						$ar_operand[]	= "Makes no change";
						$ar_factor[]	= $cfg['set']['store_symbol'].$value['ar_factor'];
						$ar_function[] 	= "Price = Previous Price";
					break;
					
					case 1:
						$ar_operand[]	= "Adds";
						$ar_factor[]	= $cfg['set']['store_symbol'].$value['ar_factor'];
						$ar_function[] 	= "Price = Previous Price + " . $cfg['set']['store_symbol'] . $value['ar_factor'];
					break;
					
					case 2:
						$ar_operand[]	= "Subtracts";
						$ar_factor[]	= $cfg['set']['store_symbol'].$value['ar_factor'];
						$ar_function[] 	= "Price = Previous Price - " . $cfg['set']['store_symbol'] . $value['ar_factor'];
					break;
					
					case 3:
						$ar_operand[]	= "Multiplies by";
						$ar_factor[]	= $value['ar_factor'];
						$ar_function[] 	= "Price = Previous Price &times; " . $value['ar_factor'];
					break;
					
					case 4:
						$ar_operand[]	= "Divides by";
						$ar_factor[]	= $value['ar_factor'];
						$ar_function[] 	= "Price = Previous Price &divide; " . $value['ar_factor'];
					break;
					
					case 5:
						$ar_operand[]	= "Sets price to";
						$ar_factor[]	= $cfg['set']['store_symbol'].$value['ar_factor'];
						$ar_function[] 	= "Price = " . $cfg['set']['store_symbol'] . $value['ar_factor'];
					break;
					
				}
				
				if ($value['ar_id'] != "") {
				$smarty->assign("show_options",(bool)TRUE);
				}
				
			}
		
		}
		
		$smarty->assign("calc_id",$calc_id);
		$smarty->assign("calc_name",$calc_name);
		$smarty->assign("calc_description",$calc_description);
		$smarty->assign("calc_type",$calc_type);
		$smarty->assign("ar_id",$ar_id);
		$smarty->assign("ar_operand",$ar_operand);
		$smarty->assign("ar_factor",$ar_factor);
		$smarty->assign("ar_function",$ar_function);
		$smarty->assign("ar_name",$ar_name);
		$smarty->assign("ar_description",$ar_description);
		$smarty->assign("ar_active",$ar_active);
		
		// Set the page title
		$smarty->assign("page_title","Edit the options for this rule");
		
		// Display the category editor page
		$smarty->pixDisplay('admin.calculators/calculator.rule.edit.tpl');
	
	}
	
	/*
	*
	*
	*
	*
	*/
	function actionEditPricingRule () {
	
		global $smarty, $cfg, $ses, $objEnvData;
		
		// Process form items
		$calc_id			= $objEnvData->fetchGlobal('calc_id');
		$calc_name			= addslashes($objEnvData->fetchGlobal('calc_name'));
		$calc_description	= addslashes($objEnvData->fetchGlobal('calc_description'));
		
		if ($calc_name != "" && $calc_description != "") {
		
			$this->_dbl->sqlQuery("UPDATE ".PIX_TABLE_CAOB." SET calc_name = '$calc_name', calc_description = '$calc_description' WHERE calc_id = '$calc_id'");
			
			$this->action_complete = true;
			
		}
		
		$this->showFormEditPricingRule();
		
	}
	
	/*
	*
	*
	*
	*
	*/
	function showFormAddNewRuleOption () {
	
		global $smarty, $cfg, $ses, $objEnvData;
		
		// Process form items
		$ar_name		= stripslashes($objEnvData->fetchGlobal('ar_name'));
		$ar_description	= stripslashes($objEnvData->fetchGlobal('ar_description'));
		$ar_operand		= $objEnvData->fetchGlobal('ar_operand');
		$ar_factor		= $objEnvData->fetchGlobal('ar_factor');
		$calc_id		= $objEnvData->fetchGlobal('calc_id');
		
		$problem = (bool)FALSE;
		
		// Check for missing name
		if ($ar_name == "") {
		
			$problem			= (bool)TRUE;
			$problem_message[]	= "You must enter a short name for this rule which will be used in the add to cart form.";
		
		}
		
		// Check for missing description
		if ($ar_description == "") {
		
			$problem			= (bool)TRUE;
			$problem_message[]	= "You must enter a description for this rule.";
		
		}
		
		// Check for missing numeric factor
		if (!is_numeric($ar_factor) && $ar_factor != "" && $ar_operand != 0) {
		
			$problem			= (bool)TRUE;
			$problem_message[]	= "You must enter a numeric value for the arithmetic calcualation of this rule.";
		
		}
		
		// Prevent dividing by zero
		if ($ar_operand == 2 && $ar_factor == 0) {
		
			$problem			= (bool)TRUE;
			$problem_message[]	= "You cannot divide numbers by zero.";
		
		}
		
		// Prevent the use of negative numbers
		if ($ar_factor <= 0 && $ar_operand != "0") {
		
			$problem			= (bool)TRUE;
			$problem_message[]	= "You cannot use negative numbers in your arithmetic calculations.";
		
		}
		
		// Assign any error messages to Smarty
		$smarty->assign("problem",$problem);
		$smarty->assign("problem_message",$problem_message);
		
		// Show a user friendly version of the arithmetic for this option
		switch ($ar_operand) {
		
			case 0:
				$ar_function 	= "Price = Previous Price";
			break;
			
			case 1:
				$ar_function 	= "Price = Previous Price + " . $cfg['set']['store_symbol'] . $ar_factor;
			break;
			
			case 2:
				$ar_function 	= "Price = Previous Price - " . $cfg['set']['store_symbol'] . $ar_factor;
			break;
			
			case 3:
				$ar_function 	= "Price = Previous Price &times; " . $ar_factor;
			break;
			
			case 4:
				$ar_function 	= "Price = Previous Price &divide; " . $ar_factor;
			break;
			
			case 5:
				$ar_function 	= "Price = " . $cfg['set']['store_symbol'] . $ar_factor;
			break;
			
		}
		
		// Assign variables back to Smarty for error page
		$smarty->assign("ar_name",$ar_name);
		$smarty->assign("ar_description",$ar_description);
		$smarty->assign("ar_operand",$ar_operand);
		$smarty->assign("ar_factor",$ar_factor);
		$smarty->assign("ar_function",$ar_function);
		$smarty->assign("calc_id",$calc_id);

		// Set the page title
		$smarty->assign("page_title","Preview new rule");
			
		// Display the category editor page
		$smarty->pixDisplay('admin.calculators/calculator.option.create.tpl');
	
	}
	
	/*
	*
	*
	*
	*
	*/
	function actionAddNewRuleOption () {
	
		global $smarty, $cfg, $ses, $objEnvData;
		
		// Process form items
		$ar_name		= stripslashes($objEnvData->fetchGlobal('ar_name'));
		$ar_description	= stripslashes($objEnvData->fetchGlobal('ar_description'));
		$ar_operand		= $objEnvData->fetchGlobal('ar_operand');
		$ar_factor		= $objEnvData->fetchGlobal('ar_factor');
		$calc_id		= $objEnvData->fetchGlobal('calc_id');
		
		$sql = "INSERT INTO ".PIX_TABLE_CAAR." VALUES ('0','$calc_id','$ar_operand','$ar_factor','$ar_name','$ar_description','0');";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->action_complete = true;
		
		$this->showFormEditPricingRule();
		
	}
	
	/*
	*
	*
	*
	*
	*/
	function showFormEditOption () {
	
		global $smarty, $cfg, $ses, $objEnvData;
		
		$ar_id 		= $objEnvData->fetchGlobal('ar_id');
		$calc_id 	= $objEnvData->fetchGlobal('calc_id');
		
		$sql = "SELECT * FROM ".PIX_TABLE_CAAR." WHERE ar_id = '$ar_id'";
		
		$result = $this->_dbl->sqlSelectRow($sql);
		
		$ar_name			= $result['ar_name'];
		$ar_description		= $result['ar_description'];
		$ar_operand			= $result['ar_operand'];
		$ar_factor			= $result['ar_factor'];
		
		$smarty->assign("calc_id",$calc_id);
		$smarty->assign("ar_id",$ar_id);
		$smarty->assign("ar_operand",$ar_operand);
		$smarty->assign("ar_factor",$ar_factor);
		$smarty->assign("ar_name",$ar_name);
		$smarty->assign("ar_description",$ar_description);
		$smarty->assign("ar_active",$ar_active);
		
		// Set the page title
		$smarty->assign("page_title","Edit rule option");
		
		// Display the category editor page
		smartyPixDisplay('admin.calculators/calculator.option.edit.01.tpl');
		
	}
	
	/*
	*
	*
	*
	*
	*/
	function formPreviewOptionEdits () {
	
		global $smarty, $cfg, $ses, $objEnvData;
		
		// Process form items
		$calc_id 			= $objEnvData->fetchGlobal('calc_id');
		$ar_id 				= $objEnvData->fetchGlobal('ar_id');
		$ar_operand			= $objEnvData->fetchGlobal('ar_operand');
		$ar_factor			= addslashes($objEnvData->fetchGlobal('ar_factor'));
		$ar_name			= addslashes($objEnvData->fetchGlobal('ar_name'));
		$ar_description		= addslashes($objEnvData->fetchGlobal('ar_description'));
		
		$problem = (bool)FALSE;
		
		// Check for missing name
		if ($ar_name == "") {
		
			$problem			= (bool)TRUE;
			$problem_message[]	= "You must enter a short name for this rule which will be used in the add to cart form.";
		
		}
		
		// Check for missing description
		if ($ar_description == "") {
		
			$problem			= (bool)TRUE;
			$problem_message[]	= "You must enter a description for this rule.";
		
		}
		
		// Check for missing numeric factor
		if (!is_numeric($ar_factor) && $ar_factor != "" && $ar_operand != 0) {
		
			$problem			= (bool)TRUE;
			$problem_message[]	= "You must enter a numeric value for the arithmetic calcualation of this rule.";
		
		}
		
		// Prevent dividing by zero
		if ($ar_operand == 2 && $ar_factor == 0) {
		
			$problem			= (bool)TRUE;
			$problem_message[]	= "You cannot divide numbers by zero.";
		
		}
		
		// Prevent the use of negative numbers
		if ($ar_factor <= 0) {
		
			$problem			= (bool)TRUE;
			$problem_message[]	= "You cannot use negative numbers in your arithmetic calculations.";
		
		}
		
		if ($problem) {
		
			// Assign any error messages to Smarty
			$smarty->assign("problem",$problem);
			$smarty->assign("problem_message",$problem_message);
				
		}
		
		switch ($ar_operand) {
		
			case 0:
				$ar_function 	= "Price = Previous Price";
			break;
			
			case 1:
				$ar_function 	= "Price = Previous Price + " . $cfg['set']['store_symbol'] . $ar_factor;
			break;
			
			case 2:
				$ar_function 	= "Price = Previous Price - " . $cfg['set']['store_symbol'] . $ar_factor;
			break;
			
			case 3:
				$ar_function 	= "Price = Previous Price &times; " . $ar_factor;
			break;
			
			case 4:
				$ar_function 	= "Price = Previous Price &divide; " . $ar_factor;
			break;
			
			case 5:
				$ar_function 	= "Price = " . $cfg['set']['store_symbol'] . $ar_factor;
			break;
			
		}
		
		$smarty->assign("calc_id",$calc_id);
		$smarty->assign("ar_id",$ar_id);
		$smarty->assign("ar_function",$ar_function);
		$smarty->assign("ar_operand",$ar_operand);
		$smarty->assign("ar_factor",$ar_factor);
		$smarty->assign("ar_name",$ar_name);
		$smarty->assign("ar_description",$ar_description);
		$smarty->assign("ar_active",$ar_active);
		
		// Set the page title
		$smarty->assign("page_title","Confirm settings for this option");
		
		// Display the category editor page
		$smarty->pixDisplay('admin.calculators/calculator.option.edit.02.tpl');
	
	}
	
	/*
	*
	*
	*
	*
	*/
	function actionSaveOptionEdits () {
		
		global $smarty, $cfg, $ses, $objEnvData;
		
		// Process form items
		$calc_id 			= $objEnvData->fetchGlobal('calc_id');
		$ar_id 				= $objEnvData->fetchGlobal('ar_id');
		$ar_operand			= $objEnvData->fetchGlobal('ar_operand');
		$ar_factor			= addslashes($objEnvData->fetchGlobal('ar_factor'));
		$ar_name			= addslashes($objEnvData->fetchGlobal('ar_name'));
		$ar_description		= addslashes($objEnvData->fetchGlobal('ar_description'));
		
		if ($ar_id != "") {
			
			$sql	= 	"UPDATE ".PIX_TABLE_CAAR."
						
						SET ar_name			= '$ar_name',
							ar_description	= '$ar_description',
							ar_operand		= '$ar_operand',
							ar_factor		= '$ar_factor'
																
						WHERE ar_id			= '$ar_id'";
			
			@mysql_query($sql);
			
			$this->action_complete = true;
		
		}
		
		// Show the rule editor for this rule
		$this->showFormEditPricingRule();
		
	}
	
	/*
	*
	*
	*
	*
	*/
	function saveRuleOrder () {
	
		global $smarty, $cfg, $ses, $objEnvData;
		
		// Load the rules order information
		$rules_order = $objEnvData->fetchGlobal('rules_order');
		
		// Turn the rules order into an array
		$rules_order = explode("|",$rules_order);
		
		// If the rules order is an array we can continue
		if (is_array($rules_order)) {
			
			// Loop through the array
			foreach ($rules_order as $key => $value) {
				
				// If the value is authentic
				if (eregi("rule_",$value)) {
					
					// Get the calc_id value for this item
					$calc_id = eregi_replace("rule_","",$value);
					
					// Update the database
					$this->_dbl->sqlQuery("UPDATE ".PIX_TABLE_CAOB." SET calc_order = '$key' WHERE calc_id = '$calc_id';");
					
				}
			
			}
			
			$this->action_complete = true;
			
		}
		
		// Show the pricing rules page
		$this->showCalculationRules();
		
	}
	
	/*
	*
	*
	*
	*
	*/
	function actionDeletePricingOption () {
	
		global $smarty, $cfg, $ses, $objEnvData;
		
		// Get the id of the option to delete
		$ar_id 		= $objEnvData->fetchGlobal('ar_id');
		$calc_id 	= $objEnvData->fetchGlobal('calc_id');
		
		if ($objEnvData->fetchGlobal('js_confirmed') == 1) {
		
			// Delete this option
			$this->_dbl->sqlQuery("DELETE FROM ".PIX_TABLE_CAAR." WHERE ar_id = '$ar_id'");
			
			// Let Smarty know this action is complete so we can show a message
			$this->action_complete = true;
			
		}
		
		// Show the rule editor for this rule
		$this->showFormEditPricingRule();
		
	}
	
	/*
	*
	*
	*
	*
	*/
	function showFormPriceRuleTest () {
	
		global $smarty, $cfg, $ses, $objEnvData;
		
		// Add price calculator options to Smarty
		$this->calculatorOptions();
		
		// Set the page title
		$smarty->assign("page_title","Price calculator test");
			
		// Display the category editor page
		$smarty->pixDisplay('admin.calculators/calculators.test.01.tpl');
	
	}
	
	/*
	*
	*
	*
	*
	*/
	function actionPriceRuleTest () {
	
		global $smarty, $cfg, $ses, $objEnvData;
		
		$options 		= $objEnvData->fetchGlobal('options');
		$price			= $objEnvData->fetchGlobal('img_price');
		
		// Calculate the price of the image
		$this->calculatePrice($options,$price);
		
		$smarty->assign("page_title","Price calculator test");
			
		// Display the category editor page
		$smarty->pixDisplay('admin.calculators/calculators.test.02.tpl');
			
	}
	
	/*
	*
	*
	*
	*
	*/
	function calculatorOptions() {
		
		global $cfg, $ses, $smarty;
		
		$rules	= $this->_dbl->sqlSelectRows("SELECT * FROM ".PIX_TABLE_CAOB." ORDER BY calc_order ASC");
		
		if (is_array($rules)) {
		
			foreach ($rules as $key => $value) {
			
				$calc_id[]			= $value['calc_id'];
				$calc_name[]		= $value['calc_name'];
				$calc_description[] = $value['calc_description'];
				$calc_active[]		= (bool)$value['calc_active'];
				
				$options = sql_select_rows("SELECT * FROM ".$cfg['sys']['table_caar']." WHERE ar_calc_id = '".$value['calc_id']."' ORDER BY ar_name ASC");
				
				$ar_id			= array();
				$ar_name		= array();
				$ar_operand		= array();
				$ar_factor		= array();
				$ar_function	= array();
					
				if (is_array($options)) {
					
					foreach($options as $opt_key => $opt_value) {
					
						$ar_id[]		= $opt_value['ar_id']; 
						$ar_name[]		= $opt_value['ar_name']; 
						$ar_operand[]	= $opt_value['ar_operand']; 
						$ar_factor[]	= $opt_value['ar_factor']; 
					
					}
					
				}
				
				$calc_ar_id[]		= $ar_id;
				$calc_ar_name[]		= $ar_name;
				$calc_ar_operand[]	= $ar_operand;
				$calc_ar_factor[]	= $ar_factor;
				$calc_ar_function[]	= $ar_function;
				
			}
			
			// Tell Smarty whether or not to show the list of rules
			$smarty->assign("show_list",(bool)TRUE);
			
			// Assign list informaiton to Smarty
			$smarty->assign("calc_description",$calc_description);
			$smarty->assign("calc_active",$calc_active);
			$smarty->assign("calc_name",$calc_name);
			$smarty->assign("calc_id",$calc_id);
	
			$smarty->assign("calc_ar_id",$calc_ar_id);
			$smarty->assign("calc_ar_name",$calc_ar_name);
			$smarty->assign("calc_ar_operand",$calc_ar_operand);
			$smarty->assign("calc_ar_factor",$calc_ar_factor);
			$smarty->assign("calc_ar_function",$calc_ar_function);
			
		}
	
	}

	/*
	*
	* 	Calculate a price for an image based on the pricing rules
	*	
	*	Takes values:
	*	
	*	$options		(ARRAY)		Array of the options selected by the user
	*	$price			(FLOAT)		Starting price of the image
	*	
	*	$smarty			(OBJECT)	Pointer to the Smarty object to laod results into [optional]
	*
	*/
	function calculatePrice($options,$price) {
		
		global $cfg, $ses, $smarty;
		
		$sym			= $cfg['set']['store_symbol'];
		
		$old_price		= $price;
		
		if (is_array($options)) {
		
			foreach ($options as $key=> $value) {
			
				$calculation		= sql_select_row("SELECT * FROM ".$cfg['sys']['table_caar']." WHERE ar_id = '$value';");
				
				$operand 			= $calculation['ar_operand'];
				$factor				= $calculation['ar_factor'];
				$ar_id[]			= $calculation['ar_id'];
				$ar_name[]			= $calculation['ar_name'];
				$ar_description[]	= $calculation['ar_description'];
				
				switch ($operand) {
				
					case 0:
						$arithmetic[]	= "Price = $sym$price";
						$price			= $price;
					break;
					
					case 1:
						$arithmetic[]	= "Price = $sym$price + $sym$factor";
						$price			= ($price + $factor);
					break;
					
					case 2:
						$arithmetic[]	= "Price = $sym$price - $sym$factor";
						$price			= ($price - $factor);
					break;
					
					case 3:
						$arithmetic[]	= "Price = $sym$price &times; $factor";
						$price			= ($price * $factor);
					break;
					
					case 4:
						$arithmetic[]	= "Price = $sym$price &divide; $factor";
						$price			= ($price / $factor);
					break;
					
					case 5:
						$arithmetic[]	= "Price = $sym$factor";
						$price			= $factor;
					break;
					
				}
		
			}
			
			if (is_object($smarty)) {
			
				$smarty->assign("new_price",getSignificantFigure($price));
				$smarty->assign("old_price",$old_price);
				$smarty->assign("arithmetic",$arithmetic);
				$smarty->assign("ar_id",$ar_id);
				$smarty->assign("ar_name",$ar_name);
				$smarty->assign("ar_description",$ar_description);
			
			}
			
			$output['new_price']		= getSignificantFigure($price);
			$output['old_price']		= $old_price;
			$output['arithmetic']		= $arithmetic;
			$output['ar_id']			= $ar_id;
			$output['ar_name']			= $ar_name;
			$output['ar_description']	= $ar_description;
			
			return $output;
			
		} else {
	
			return false;	
		
		}
	
	}
	
}




?>