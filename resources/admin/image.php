<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "images";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Initialise the Smarty object
$smarty = new Smarty_Pixaria;

if ($objEnvData->fetchPost('cmd') == "12") {

	// Check that the user is an image_editor
	pix_authorise_user("image_editor");
	
} else {

	// Check that the user is a administrator
	pix_authorise_user("administrator");
	
}

$objImage = new Admin_Image();

class Admin_Image {

	/*
	*
	*
	*
	*/
	function Admin_Image () {
	
		global 	$objEnvData,
				$smarty,
				$ses,
				$cfg;

		// Load an initialise the database class
		require_once ('class.Database.php');
		require_once (SYS_BASE_PATH . 'resources/classes/class.InputData.php');

		$this->db		= new Database();
		$this->input	= new InputData();
		$this->view 	= $smarty;
		$this->config	= $cfg;
		$this->session 	= $ses;
		
		// Assign an array of form actions to the view object
		$this->view->assign('form_actions', array (

			'images_delete' 		=> "{url:'".SYS_BASE_URL.FILE_ADM_IMAGE."',cmd:'formDeleteFromLibrary'}",
			'images_batch_edit' 	=> "{url:'".SYS_BASE_URL.FILE_ADM_IMAGE."',cmd:'editMultiple'}",
			'gallery_images_create' => "{url:'".SYS_BASE_URL.FILE_ADM_GALLERY."',cmd:'formCreateNewGallery'}",
			'gallery_images_add' 	=> "{url:'".SYS_BASE_URL.FILE_ADM_GALLERY."',cmd:'formAddImagesToGallery'}"
		
		));
		
		switch ($this->input->name('cmd')) {
			
			case 'saveFrontEnd':
				$this->saveFrontEnd();
			break;
			
			case 'editMultiple':
				$this->formEditMultiple();
			break;
			
			case 'saveMultiple':
				$this->saveMultiple();
			break;
			
			case 'edit':
				$this->formEditSingle();
			break;
		
			case 'save':
				$this->saveSingle();
			break;
			
			case 'formDeleteFromLibrary':
				$this->formDeleteFromLibrary();
			break;
			
			case 'actionDeleteFromLibrary':
				$this->actionDeleteFromLibrary();
			break;
			
			default:
				$this->index();
			break;
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function index () {
		
		global $cfg;
		
		$page = $this->input->name('page');
		
		$sql = "SELECT ".PIX_TABLE_IMGS.".*,
				
				CONCAT(".PIX_TABLE_USER.".family_name, ', ', ".PIX_TABLE_USER.".first_name) AS image_owner
		
				FROM ".PIX_TABLE_IMGS."
				
				LEFT JOIN ".PIX_TABLE_GORD." ON  ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_GORD.".image_id
				
				LEFT JOIN ".PIX_TABLE_USER." ON  ".PIX_TABLE_IMGS.".image_userid = ".PIX_TABLE_USER.".userid
				
				WHERE ".PIX_TABLE_GORD.".image_id IS NULL
				
				ORDER BY ".PIX_TABLE_IMGS.".image_filename";
		
		// Count the total number of orphan images
		$total	= $this->db->countRows($sql);
		
		// Get the total number of galleries
		list ($total_galleries) = $this->db->count("SELECT COUNT(gallery_id) FROM ".PIX_TABLE_GALL."");
		
		// Select all images that are no in the gallery_order table (i.e. not yet in any galleries)
		$result = $this->db->rows($sql . getMultiImagePageLimitSQL($this->db->escape($page)));
		
		$ipages = getMultiImagePageNavigation ("orphans", $this->db->escape($page), $total, '');
		
		if (is_array($result)) {
		
			// Initialise arrays to hold image data
			$image_id	 		= array();
			$image_userid		= array();
			$image_active		= array();
			$image_filename		= array();
			$image_path			= array();
			$image_date			= array();
			$icon_path			= array();
			$icon_width			= array();
			$icon_height		= array();
			$comp_path			= array();
			$comp_width			= array();
			$comp_height		= array();
			$image_width		= array();
			$image_height		= array();
			$image_title		= array();
		
			foreach ($result as $key => $value) {
				
				$image_id[]			= $value['image_id'];
				$image_userid[]		= $value['image_userid'];
				$image_owner[]		= $value['image_owner'];
				$image_active[]		= $value['image_active'];
				$image_filename[] 	= $value['image_filename'];
				$image_path[]		= $value['image_path'];
				$image_date[]		= $value['image_date'];
				
				$icon_path[]		= base64_encode($cfg['sys']['base_library'].$value[image_path]."/32x32/".$value[image_filename]);
				$icon_size			= @getimagesize($cfg['sys']['base_library'].$value[image_path]."/32x32/".$value[image_filename]);
				$icon_width[]		= $icon_size[0];
				$icon_height[]		= $icon_size[1];
				
				$comp_path[]		= base64_encode($cfg['sys']['base_library'].$value[image_path]."/".COMPING_DIR."/".$value[image_filename]);
				$comp_size			= @getimagesize($cfg['sys']['base_library'].$value[image_path]."/".COMPING_DIR."/".$value[image_filename]);
				$comp_width[]		= $comp_size[0];
				$comp_height[]		= $comp_size[1];
				
				$image_width[]		= $value[image_width];
				$image_height[]		= $value[image_height];
				
				$image_title[]		= $value[image_title];
				
			}
			
			// Assign image information to Smarty
			$this->view->assign("image_id",$image_id);
			$this->view->assign("image_owner",$image_owner);
			$this->view->assign("image_userid",$image_userid);
			$this->view->assign("image_active",$image_active);
			$this->view->assign("image_filename",$image_filename);
			$this->view->assign("image_path",$image_path);
			$this->view->assign("image_date",$image_date);
			$this->view->assign("icon_path",$icon_path);
			$this->view->assign("icon_width",$icon_width);
			$this->view->assign("icon_height",$icon_height);
			$this->view->assign("comp_path",$comp_path);
			$this->view->assign("comp_width",$comp_width);
			$this->view->assign("comp_height",$comp_height);
			$this->view->assign("image_width",$image_width);
			$this->view->assign("image_height",$image_height);
			$this->view->assign("image_title",$image_title);
			
			$this->view->assign("total_galleries",$total_galleries);
			
			$this->view->assign("ipage_current",$ipages[0]);
			$this->view->assign("ipage_numbers",$ipages[1]);
			$this->view->assign("ipage_links",$ipages[2]);
			
		}	
		
		// The number of orphaned images
		$this->view->assign("orph_count",count($image_id));
		
		// Set the page title
		$this->view->assign("page_title","List images not in a gallery");
		
		// Output the orphan images page
		$this->view->display('image/images.orphans.html');
	
	}
	
	/*
	*
	*
	*
	*/
	function formEditSingle () {
	
		// Set the page title
		$this->view->assign("page_title","Edit an image");
		
		// Get the image_id
		$image_id 	= $this->input->name('image_id');
		$images		= $this->input->name('images');
		
		// Check for image_id in array (only use the first item
		if (is_array($images)) {
			if (is_numeric($images[0])) {
				$image_id = (int)$images[0];
			} else {
				$this->index();
			}
		}
		
		// Load the PixariaImage class so we can extend it
		require_once ('class.PixariaImageAdmin.php');
		
		// Initialise image object
		$objImage = new PixariaImageAdmin($image_id);
		
		// Load information into Smarty for the edit image screen
		$this->view->assign("image_id",$objImage->getImageId());
		$this->view->assign("image_active",$objImage->getImageActive());
		$this->view->assign("image_userid",$objImage->getImageUserId());
		$this->view->assign("image_title",$objImage->getImageTitle());				
		$this->view->assign("image_caption",$objImage->getImageCaption());	
		$this->view->assign("image_keywords",$objImage->getImageKeywords());
		$this->view->assign("image_width",$objImage->getImageWidth());				
		$this->view->assign("image_height",$objImage->getImageHeight());				
		$this->view->assign("image_copyright",$objImage->getImageCopyright());
		$this->view->assign("image_permissions",$objImage->getImagePermissions());	
		$this->view->assign("image_rights_type",$objImage->getImageRightsType());				
		$this->view->assign("image_rights_text",$objImage->getImageRightsText());				
		$this->view->assign("image_model_release",$objImage->getImagemodelRelease());				
		$this->view->assign("image_property_release",$objImage->getImagePropertyRelease());				
		$this->view->assign("image_price",$objImage->getImagePrice());				
		$this->view->assign("image_mysql_date",$objImage->getImageDate());				
		$this->view->assign("image_sale",$objImage->getImageSale());				
		$this->view->assign("image_product_link",$objImage->getImageProductLink());				
		$this->view->assign("image_extra_01",$objImage->getImageExtra1());				
		$this->view->assign("image_extra_02",$objImage->getImageExtra2());				
		$this->view->assign("image_extra_03",$objImage->getImageExtra3());				
		$this->view->assign("image_extra_04",$objImage->getImageExtra4());				
		$this->view->assign("image_extra_05",$objImage->getImageExtra5());								
		$this->view->assign("image_colr_enable",$objImage->getImageColrEnable());				
		$this->view->assign("image_colr_mod",$objImage->getImageModRGB());				
		$this->view->assign("image_colr_hex",$objImage->getImageHexRGB());				
		$this->view->assign("image_colr_dec",$objImage->getImageDecRGB());				
		$this->view->assign("image_exif_date",$objImage->getExifDate());
		$this->view->assign("image_iptc_date",$objImage->getIptcDate());
		$this->view->assign("image_colr_average_hex",$objImage->getImageColrAverageHex());
		$this->view->assign("image_colr_average_dec",$objImage->getImageColrAverageDec());
		$this->view->assign("image_large_encoded",$objImage->getImageLargeEncoded());
		$this->view->assign("image_comp_encoded",$objImage->getImageCompEncoded());
		$this->view->assign("image_large_dims",$objImage->getImageLargeDimensions());
		$this->view->assign("image_comp_dims",$objImage->getImageCompDimensions());
		$this->view->assign("category_title",$objImage->getCategoryListTitles());
		$this->view->assign("category_id",$objImage->getCategoryListIds());
		$this->view->assign("group_title",$objImage->getGroupListTitles());
		$this->view->assign("group_id",$objImage->getGroupListIds());		
		$this->view->assign("image_category_links",$objImage->getImageCategoryLinks());
		$this->view->assign("image_group_links",$objImage->getImageGroupLinks());
		$this->view->assign("image_product_array",$objImage->getImageProducts());
		$this->view->assign("gps_longitude",$objImage->getGpsLongitude());
		$this->view->assign("gps_latitude",$objImage->getGpsLatitude());

		$this->view->assign("return",$this->input->name('return'));
		$this->view->assign("referer",$_SERVER['HTTP_REFERER']);
		
		require_once('class.PixariaProduct.php');
		
		$objProducts = new PixariaProduct();
		
		$product_data = $objProducts->getProducts();
		
		$this->view->assign("product_id",$product_data['prod_id']);
		$this->view->assign("product_name",$product_data['prod_name']);
		
		// Get a list of all the photographers in the system
		list ( $user_id, $user_name ) = photographerListArray();
		
		// Load the photographer data
		$this->view->assign("user_id",$user_id);
		$this->view->assign("user_name",$user_name);
		
		// Output the HTML page
		$this->view->display('image/form.edit.single.html');
	
	}
	
	/*
	*
	*	
	*
	*/
	function saveSingle () {
	
		// Load the image management class
		require_once ('class.PixariaImage.php');
		
		// Create a new image object
		$objImage = new PixariaImage($this->input->post('image_id'));
		
		// Update the image date
		$objImage->setImageDate($this->input->post('toda_Year'),
								$this->input->post('toda_Month'),
								$this->input->post('toda_Day'),
								$this->input->post('toda_Hour'),
								$this->input->post('toda_Minute'),
								$this->input->post('toda_Second'));
	
		
		// Update the properties
		$objImage->setImageActive($this->input->checkbox('image_active'));
		$objImage->setImageUserId($this->input->post('image_userid'));
		$objImage->setImageTitle($this->input->post('image_title'));
		$objImage->setImageCaption($this->input->post('image_caption'));
		$objImage->setImageKeywords($this->input->post('image_keywords'));
		$objImage->setImageWidth($this->input->post('image_width'));
		$objImage->setImageHeight($this->input->post('image_height'));
		$objImage->setImageCopyright($this->input->post('image_copyright'));
		$objImage->setImagePrice($this->input->post('image_price'));
		$objImage->setImageSale($this->input->checkbox('image_sale'));
		$objImage->setImageProductLink($this->input->post('image_product_link'));
		$objImage->setImagePermissions($this->input->post('image_permissions'));
		$objImage->setImageViewers($this->input->post('image_viewers'));
		$objImage->setImageRightsType($this->input->post('image_rights_type'));
		$objImage->setImageRightsText($this->input->post('image_rights_text'));
		$objImage->setImageColrEnable($this->input->checkbox('image_colr_enable'));
		$objImage->setImageColrR($this->input->post('image_colr_r'));
		$objImage->setImageColrG($this->input->post('image_colr_g'));
		$objImage->setImageColrB($this->input->post('image_colr_b'));
		$objImage->setImageModelRelease($this->input->checkbox('image_model_release'));
		$objImage->setImagePropertyRelease($this->input->checkbox('image_property_release'));
		$objImage->setImageExtra1($this->input->post('image_extra_01'));
		$objImage->setImageExtra2($this->input->post('image_extra_02'));
		$objImage->setImageExtra3($this->input->post('image_extra_03'));
		$objImage->setImageExtra4($this->input->post('image_extra_04'));
		$objImage->setImageExtra5($this->input->post('image_extra_05'));
		$objImage->setImageProducts($this->input->post('image_products'));
		
		// Update the details of this image
		$objImage->updateImage();		
		
		print "{'message':'The changes to the selected images have been saved!'}";
		
	}
	
	/*
	*
	*	
	*
	*/
	function formEditMultiple () {
		
		$prefix	= "toda_date";
		$type	= "Today";
		
		$ss	= date("s",time());
		$mi	= date("i",time());
		$hh	= date("H",time());
		$dd	= date("d",time());
		$mm	= date("m",time());
		$yy	= date("Y",time());
		
		// Get array of date menu information
		$date_menus = pix_date_menu();
		
		// Load date information into Smarty for the menus
		$this->view->assign("toda_years",$date_menus[0]);
		$this->view->assign("toda_months",$date_menus[1]);
		$this->view->assign("toda_days",$date_menus[2]);
		$this->view->assign("toda_this_year",$yy);
		$this->view->assign("toda_this_month",$mm);
		$this->view->assign("toda_this_day",$dd);
		$this->view->assign("toda_this_hour",$hh);
		$this->view->assign("toda_this_minute",$mi);
		$this->view->assign("toda_this_second",$ss);
					
		// Load an array of the user groups
		$group_list = groupListArray();
		
		// Load the category list data into Smarty
		$this->view->assign("group_name",$group_list[0]);
		$this->view->assign("group_id",$group_list[1]);
		
		// Load an array of all photographers in the system
		$user_list = photographerListArray();
		
		// Load the photographer data into Smarty
		$this->view->assign("user_id",$user_list[0]);
		$this->view->assign("user_name",$user_list[1]);
		
		// Get variables for Smarty
		$this->view->assign("image_id",$this->input->name('images'));
		
		require_once('class.PixariaProduct.php');
		
		$objProducts = new PixariaProduct();
		
		$product_data = $objProducts->getProducts();
		
		$this->view->assign("product_id",$product_data['prod_id']);
		$this->view->assign("product_name",$product_data['prod_name']);
		
		// Set the page title
		$this->view->assign("page_title","Edit Multiple Images");
		
		// Output the HTML page
		$this->view->display('image/form.edit.multiple.html');	
			
	}
	
	/*
	*
	*	
	*
	*/
	function saveMultiple () {
		
		// Get an array of the image_id
		$image_ids = $this->input->post('image_id');
		
		// Load the image management class
		require_once ('class.PixariaImage.php');
		
		if (is_array($image_ids)) {
			
			foreach ($image_ids as $key => $value) {
				
				$objPixariaImage = new PixariaImage($value);
				
				// Load information about which image data to change
				if ($this->input->checkbox('change_date')) { 
					$objPixariaImage->setImageDate(	$this->input->post('toda_Year'),
												$this->input->post('toda_Month'),
												$this->input->post('toda_Day'),
												$this->input->post('toda_Hour'),
												$this->input->post('toda_Minute'),
												$this->input->post('toda_Second'));
				}

				if ($this->input->checkbox('change_active')) { 
					$objPixariaImage->setImageActive($this->input->post('image_active'));
				}
				
				if ($this->input->checkbox('change_title')) { 
					$objPixariaImage->setImageTitle($this->input->post('image_title'));
				}
				
				if ($this->input->checkbox('change_caption')) {
					$objPixariaImage->setImageCaption($this->input->post('image_caption'));
				}
				
				if ($this->input->checkbox('change_keywords')) {
					if ($this->input->checkbox('append_keywords')) {
						$objPixariaImage->setImageKeywords($this->input->post('image_keywords'),true);
					} else {
						$objPixariaImage->setImageKeywords($this->input->post('image_keywords'));
					}
				}
				
				if ($this->input->checkbox('change_width')) {
					$objPixariaImage->setImageWidth($this->input->post('image_width'));
				}
				
				if ($this->input->checkbox('change_height')) {
					$objPixariaImage->setImageHeight($this->input->post('image_height'));
				}
				
				if ($this->input->checkbox('change_userid')) { $this->input->post('image_userid');
					$objPixariaImage->setImageUserid($this->input->post('image_userid'));
				}
				
				if ($this->input->checkbox('change_copyright')) {
					$objPixariaImage->setImageCopyright($this->input->post('image_copyright'));
				}
				
				if ($this->input->checkbox('change_price')) {
					$objPixariaImage->setImagePrice($this->input->post('image_price'));
				}
				
				if ($this->input->checkbox('change_image_product_link')) 			{
					$objPixariaImage->setImageProductLink($this->input->post('image_product_link'));
					$objPixariaImage->setImageProducts($this->input->post('image_products'));
				}
				
				if ($this->input->checkbox('change_sale')) {
					$objPixariaImage->setImageSale($this->input->post('image_sale'));
				}
				
				if ($this->input->checkbox('change_permissions')) {
					$objPixariaImage->setImagePermissions($this->input->post('image_permissions'));
				}
				
				if ($this->input->checkbox('change_viewers')) {
					$objPixariaImage->setImageViewers($this->input->post('image_viewers'));
				}
				
				if ($this->input->checkbox('change_rights_type')) {
				$objPixariaImage->setImageRightsType($this->input->post('image_rights_type'));
				}
				
				if ($this->input->checkbox('change_rights_text')) {
					$objPixariaImage->setImageRightsText($this->input->post('image_rights_text'));
				}
				
				if ($this->input->checkbox('change_model_release')) {
					$objPixariaImage->setImageModelRelease($this->input->post('image_model_release'));
				}
				
				if ($this->input->checkbox('change_property_release')) {
					$objPixariaImage->setImagePropertyRelease($this->input->post('image_property_release'));
				}
				
				if ($this->input->checkbox('change_image_extra_01')) {
					$objPixariaImage->setImageExtra1($this->input->post('image_extra_01'));
				}
				
				if ($this->input->checkbox('change_image_extra_02')) {
					$objPixariaImage->setImageExtra2($this->input->post('image_extra_02'));
				}
				
				if ($this->input->checkbox('change_image_extra_03')) {
					$objPixariaImage->setImageExtra3($this->input->post('image_extra_03'));
				}
				
				if ($this->input->checkbox('change_image_extra_04')) {
					$objPixariaImage->setImageExtra4($this->input->post('image_extra_04'));
				}
				
				if ($this->input->checkbox('change_image_extra_05')) {
					$objPixariaImage->setImageExtra5($this->input->post('image_extra_05'));
				}
						
				// Update the database
				$objPixariaImage->updateImage();		
				
			}
		
		}
		
		print "{'message':'The changes to this image have been saved!'}";
		
	}
	
	/*
	*
	*	
	*
	*/
	function formDeleteFromLibrary () {

		global $cfg;
		
		// Get images
		$images = $this->input->name('images');
		
		// Only continue if we've got an array of images (as image_id)
		if (is_array($images)) {
		
			// Initialise arrays to hold image data
			$image_id	 	= array();
			$image_filename = array();
			$image_path		= array();
			$image_date		= array();
			$icon_path		= array();
			$icon_width		= array();
			$icon_height	= array();
			$comp_path		= array();
			$comp_width		= array();
			$comp_height	= array();
			$comp_filesize	= array();
			$image_width	= array();
			$image_height	= array();
			$image_title	= array();
			
			foreach($images as $key => $value) {
				
				// Get the name of the file from the database
				$result = $this->db->row("SELECT * FROM ".PIX_TABLE_IMGS." WHERE image_id = '".$this->db->escape($value)."'");
				
				$image_filename[]	= $result['image_filename'];
				$image_path[]		= $result['image_path'];
				
				$image_id[]			= $result['image_id'];
				$image_date[]		= $result['image_date'];
				
				$icon_path[]		= base64_encode($cfg['sys']['base_library'].$result[image_path]."/32x32/".$result[image_filename]);
				$icon_size			= getimagesize($cfg['sys']['base_library'].$result[image_path]."/32x32/".$result[image_filename]);
				$icon_width[]		= $icon_size[0];
				$icon_height[]		= $icon_size[1];
				
				$comp_path[]		= base64_encode($cfg['sys']['base_library'].$result[image_path]."/".COMPING_DIR."/".$result[image_filename]);
				$comp_size			= getimagesize($cfg['sys']['base_library'].$result[image_path]."/".COMPING_DIR."/".$result[image_filename]);
				$comp_width[]		= $comp_size[0];
				$comp_height[]		= $comp_size[1];
				$comp_filesize[]	= round((filesize($cfg['sys']['base_library'].$result[image_path]."/".COMPING_DIR."/".$result[image_filename]) / 1024),0);
				
				$image_width[]		= $result['image_width'];
				$image_height[]		= $result['image_height'];
							
			}
	
			// Assign image information to Smarty
			$this->view->assign("image_id",$image_id);
			$this->view->assign("image_filename",$image_filename);
			$this->view->assign("image_path",$image_path);
			$this->view->assign("image_date",$image_date);
			$this->view->assign("icon_path",$icon_path);
			$this->view->assign("icon_width",$icon_width);
			$this->view->assign("icon_height",$icon_height);
			$this->view->assign("comp_path",$comp_path);
			$this->view->assign("comp_width",$comp_width);
			$this->view->assign("comp_height",$comp_height);
			$this->view->assign("comp_filesize",$comp_filesize);
			$this->view->assign("image_width",$image_width);
			$this->view->assign("image_height",$image_height);
			
			$this->view->assign("display",(bool)TRUE);
			
		}
		
		$this->view->assign("referer",$_SERVER['HTTP_REFERER']);
		
		$this->view->assign("page_title","Delete images");
		
		// Load the HTML page
		$this->view->display('image/images.delete.01.html');
	
	}
	
	/*
	*
	*	This method deletes images in array $images from the library
	*
	*/
	function actionDeleteFromLibrary () {
	
		// Create an array to hold the errors in
		$errors = array();
		
		// Get form data
		$referer 		= $this->input->name('referer');
		$images 		= $this->input->name('images');
		$delete_option 	= $this->input->name('delete_option');
				
		// This loop deletes the real files for images in this gallery if the user has chosen to do this
		if (count($images) > 0 && is_array($images) && $delete_option != "off") {
			
			foreach ($images as $key => $value) {
				
				// Delete the image from the database
				deleteImageFromLibrary($value,$delete_option);			
							
			}
			
			// SQL for optimising image list tables
			$sql = "OPTIMIZE TABLE 	`".PIX_TABLE_GORD."` ,
									`".PIX_TABLE_IMGS."` ,
									`".PIX_TABLE_LBME."` ,
									`".PIX_TABLE_SRCS."` ";
			
			// Run SQL
			$this->db->query($sql);
			
		}
		
		// Count number of images that cound not be deleted
		$this->view->assign("error_count",count($errors));
		$this->view->assign("errors",$errors);
		
		// Set the page title
		$this->view->assign("page_title","Image(s) deleted");
		
		// Assign the referring page to take the user back to
		$this->view->assign("referer",$referer);
		
		// Display the main HTML page
		$this->view->display('image/images.delete.02.html');

	
	}
	
}

?>