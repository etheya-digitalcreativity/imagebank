<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "logs";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Check that the user is a SuperUser
pix_authorise_user("administrator");

// Initialise objects
$smarty 		= new Smarty_Pixaria;
$objPixariaLog 	= new PixariaLog();

class PixariaLog {

	function PixariaLog () {
	
		global $objEnvData;
		
		switch ($objEnvData->fetchGlobal('cmd')) {
		
			case "showPaymentScriptLogItem":
				$this->showPaymentScriptLogItem();
			break;
			
			case "showPaymentScriptLog": default:
				$this->showPaymentScriptLog();
			break;
		
		}
	
	}

	/*
	*	Show a list of attempts to access the payment scripts
	*	
	*	@return void
	*/
	function showPaymentScriptLog () {
		
		global $objEnvData, $smarty;
		
		require_once('class.PixariaPaymentLog.php');
		
		$objPaymentLog = new PixariaPaymentLog();
		
		$sort_column 	= $objEnvData->fetchGlobal('sort_column');
		$sort_order		= $objEnvData->fetchGlobal('sort_order');
		
		$data = $objPaymentLog->getMultipleEntries("","",$sort_column,$sort_order);
		
		$smarty->assign("id",$data['id']);
		$smarty->assign("ip_address",$data['ip_address']);
		$smarty->assign("host_name",$data['host_name']);
		$smarty->assign("post_count",$data['post_count']);
		$smarty->assign("get_count",$data['get_count']);
		$smarty->assign("cookie_count",$data['cookie_count']);
		$smarty->assign("event_time",$data['event_time']);
		
		// Set the correct sort order value for this page
		if ($sort_order == "ASC") { $smarty->assign("sort_order","DESC"); } else { $smarty->assign("sort_order","ASC"); }
		
		// Set page title
		$smarty->assign("page_title","Payment Script Access Log");
		
		// Output HTML
		$smarty->pixDisplay('admin.log/log.payscripts.tpl');
		
	}

	/*
	*	Show a list of attempts to access the payment scripts
	*	
	*	@return void
	*/
	function showPaymentScriptLogItem () {
		
		global $objEnvData, $smarty;
		
		require_once('class.PixariaPaymentLog.php');
		
		$objPaymentLog = new PixariaPaymentLog($objEnvData->fetchGlobal('id'));
		
		$smarty->assign("ip_address",$objPaymentLog->getIPAddress());
		$smarty->assign("host_name",$objPaymentLog->getHostName());
		$smarty->assign("post_vars",$objPaymentLog->getPostVars());
		$smarty->assign("get_vars",$objPaymentLog->getGetVars());
		$smarty->assign("cookie_vars",$objPaymentLog->getCookieVars());
		$smarty->assign("post_count",$objPaymentLog->getPostCount());
		$smarty->assign("get_count",$objPaymentLog->getGetCount());
		$smarty->assign("cookie_count",$objPaymentLog->getCookieCount());
		$smarty->assign("event_time",$objPaymentLog->getEventTime());
		
		if (count($data['ip_address']) > 0) : $smarty->assign("data_result",TRUE); endif;
		
		// Set page title
		$smarty->assign("page_title","Payment Script Access Log");
		
		//print_r($objPaymentLog->getCookieVars());exit;
		
		// Output HTML
		$smarty->pixDisplay('admin.log/log.payscripts.view.tpl');
		
	}

}


?>