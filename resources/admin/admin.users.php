<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "user";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Check that the user is a SuperUser
pix_authorise_user("administrator");

// Instantiate the object
$objAdminUsers = new AdminUsers();


class AdminUsers {

	/*
	*
	*	
	*
	*/
	function AdminUsers () {
	
		global $cfg, $ses, $objEnvData;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		switch ($objEnvData->fetchGlobal('cmd')) {
		
			case "showUserLightboxes":
				$this->showUserLightboxes();
			break;
		
			case "actionEmptyLightbox":
				$this->actionEmptyLightbox();
			break;
		
			case "actionDeleteLightbox":
				$this->actionDeleteLightbox();
			break;
		
			case "createNewUserForm":
				$this->createNewUserForm();
			break;
		
			case "createNewPartialUser":
				$this->createNewPartialUser();
			break;
		
		}
	
	}
	
	/*
	*
	*	Shows a form allowing administrator to create a new user invitation
	*
	*/
	function createNewUserForm () {
	
		global $smarty, $cfg;
		
		// Get the lists of group names and ids
		$group_list = generateUserGroupsArray();

		// Load the group list data into Smarty
		$smarty->assign("group_name",$group_list[0]);
		$smarty->assign("group_id",$group_list[1]);
		
		// Define html page title
		$smarty->assign("page_title","Create a new user");
		
		// Output html from template file
		$smarty->pixDisplay('admin.users/new.user.tpl');
	
		// Stop running the script here
		exit;
		
	}
	
	/*
	*
	*	Process the user invitation
	*
	*/
	function createNewPartialUser () {
		
		global $objEnvData, $cfg, $smarty;
		
		require_once ("class.PixariaUser.php");	
		
		$objPixariaUser = new PixariaUser();
		
		$objPixariaUser->setFirstName($objEnvData->fetchGlobal('first_name'));
		$objPixariaUser->setFamilyName($objEnvData->fetchGlobal('family_name'));
		$objPixariaUser->setEmail($objEnvData->fetchGlobal('email_address'));
		$objPixariaUser->setIsAdministrator($objEnvData->fetchGlobal('is_admin'));
		$objPixariaUser->setIsEditor($objEnvData->fetchGlobal('is_editor'));
		$objPixariaUser->setIsPhotographer($objEnvData->fetchGlobal('is_photo'));
		$objPixariaUser->setGroups($objEnvData->fetchGlobal('groups'));
		
		$success = $objPixariaUser->createNewUserMinimal();
		
		// Check whether the create user function worked or not
		
		if ($success) { // Yes, it worked and we can send the invite
			
			// Define html page title
			$smarty->assign("page_title","Invitation complete");
			
			// Output html from template file
			$smarty->pixDisplay('admin.users/new.user.success.tpl');
		
			// Stop running the script here
			exit;
			
		} else { // The user was not created because there was a problem with the data submitted
			
			// Load the errror information from the PixariaUsers object
			$smarty->assign("profile_errors",$objPixariaUser->getProfileErrors());
			
			// Define html page title
			$smarty->assign("page_title","Correct errors");
			
			// Output html from template file
			$smarty->pixDisplay('admin.users/new.user.error.tpl');
		
			// Stop running the script here
			exit;
			
		}
		
	}
	
}

?>