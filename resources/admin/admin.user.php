<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "user";

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Check that the user is a SuperUser
pix_authorise_user("administrator");

$objAdminUser = new AdminUser();

class AdminUser {
	
	var $action_complete;
	var $_dbl;
	var $_tpl;
	
	/*
	*
	*	
	*
	*/
	function AdminUser() {
		
		global $objEnvData, $smarty;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		$this->db 	=& $this->_dbl;
		
		$this->_tpl =& $smarty;
		
		// Perform the correct command for this page
		switch ($objEnvData->fetchGlobal('cmd')) {
			
			case "formConfirmDeleteUsers":
				$this->formConfirmDeleteUsers();
			break;
			
			case "actionDeleteLightbox":
				$this->actionDeleteLightbox();
			break;
			
			case "actionDeleteUsers":
				$this->actionDeleteUsers();
			break;
			
			case "viewUserImages":
				$this->viewUserImages();
			break;
			
			case "promoteAdmin":
				$this->actionChangePrivileges('promoteAdmin');
			break;
			
			case "promoteActive":
				$this->actionChangePrivileges('promoteActive');
			break;
			
			case "promotePhotographer":
				$this->actionChangePrivileges('promotePhotographer');
			break;
			
			case "promoteDownload":
				$this->actionChangePrivileges('promoteDownload');
			break;
			
			case "demoteAdmin":
				$this->actionChangePrivileges('demoteAdmin');
			break;
			
			case "demoteActive":
				$this->actionChangePrivileges('demoteActive');
			break;
			
			case "demotePhotographer":
				$this->actionChangePrivileges('demotePhotographer');
			break;
			
			case "demoteDownload":
				$this->actionChangePrivileges('demoteDownload');
			break;
			
			default:
				$this->listAllUsers();
			break;
		
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function listAllUsers () {
		
		global $smarty, $objEnvData, $ses;
		
		$page = $objEnvData->fetchGlobal('page');
		
		$is_administrator	= $objEnvData->fetchGlobal('is_administrator');
		$is_photographer	= $objEnvData->fetchGlobal('is_photographer');
		$is_download		= $objEnvData->fetchGlobal('is_download');
		$is_disabled		= $objEnvData->fetchGlobal('is_disabled');
		$download			= $objEnvData->fetchGlobal('download');
		$photographer		= $objEnvData->fetchGlobal('photographer');
		$administrator		= $objEnvData->fetchGlobal('administrator');
		$inactive			= $objEnvData->fetchGlobal('inactive');
		$sort				= $objEnvData->fetchGlobal('sort');
		$method				= $objEnvData->fetchGlobal('method');
		$filter_email		= $objEnvData->fetchGlobal('filter_email');
		$filter_name		= $objEnvData->fetchGlobal('filter_name');
		
		// Tell Smarty if we're going to show the list after a completed action
		$this->_tpl->assign("action_complete",$this->action_complete);
		
		switch ($sort) {
		
			case "name1":
			
				if ($method == "a" || !$method) {
				
					$sql_order = "ORDER BY ".PIX_TABLE_USER.".family_name ASC";
					$this->_tpl->assign("new_method","d");
				
				} else {
					
					$sql_order = "ORDER BY ".PIX_TABLE_USER.".family_name DESC";
					$this->_tpl->assign("new_method","a");
				
				}
			
			break;
			
			case "name2":
			
				if ($method == "a" || !$method) {
				
					$sql_order = "ORDER BY ".PIX_TABLE_USER.".first_name ASC";
					$this->_tpl->assign("new_method","d");
				
				} else {
					
					$sql_order = "ORDER BY ".PIX_TABLE_USER.".first_name DESC";
					$this->_tpl->assign("new_method","a");
				
				}
			
			break;
			
			case "enabled":
			
				if ($method == "a" || !$method) {
				
					$sql_order = "ORDER BY ".PIX_TABLE_USER.".account_status ASC";
					$this->_tpl->assign("new_method","d");
				
				} else {
					
					$sql_order = "ORDER BY ".PIX_TABLE_USER.".account_status DESC";
					$this->_tpl->assign("new_method","a");
				
				}
			
			break;
			
			case "email":
			
				if ($method == "a" || !$method) {
				
					$sql_order = "ORDER BY ".PIX_TABLE_USER.".email_address ASC";
					$this->_tpl->assign("new_method","d");
				
				} else {
					
					$sql_order = "ORDER BY ".PIX_TABLE_USER.".email_address DESC";
					$this->_tpl->assign("new_method","a");
				
				}
			
			break;
			
			case "date":
				
				if ($method == "a" || !$method) {
				
					$sql_order = "ORDER BY ".PIX_TABLE_USER.".date_registered ASC";
					$this->_tpl->assign("new_method","d");
				
				} else {
					
					$sql_order = "ORDER BY ".PIX_TABLE_USER.".date_registered DESC";
					$this->_tpl->assign("new_method","a");
				
				}
			
			break;
			
			default:
				
				$sort 	= "name1";
				$method = "a";
				$this->_tpl->assign("new_method","d");
				
				if ($method == "a" || !$method) {
				
					$sql_order = "ORDER BY ".PIX_TABLE_USER.".family_name ASC";
				
				} else {
					
					$sql_order = "ORDER BY ".PIX_TABLE_USER.".family_name DESC";
				
				}
			
			break;
			
		}
				
		if ($inactive == 1 || $is_disabled) {
			
			$sql_where .= " AND ".PIX_TABLE_USER.".account_status = '0' ";
			
			$this->_tpl->assign("type","inactive=".$inactive);
			$this->_tpl->assign("is_disabled",true);
		
		}
		
		if ($administrator == 1 || $is_administrator) {
			
			$sql_filter_1 = " HAVING ";
			$sql_filter_2[] = " groups LIKE '%1%' ";
			
			$this->_tpl->assign("type","is_administrator=1");
			$this->_tpl->assign("is_administrator",true);
		
		}
		
		if ($photographer == 1 || $is_photographer) {
			
			$sql_filter_1 = " HAVING ";
			$sql_filter_2[] = " groups LIKE '%3%' ";
			
			$this->_tpl->assign("type","is_photographer=1");
			$this->_tpl->assign("is_photographer",true);
		
		}
		
		if ($download == 1 || $is_download) {
			
			$sql_filter_1 = " HAVING ";
			$sql_filter_2[] = " groups LIKE '%5%' ";
			
			$this->_tpl->assign("type","is_download=1");
			$this->_tpl->assign("is_download",true);
		
		}
		
		if (is_array($sql_filter_2)) {
			
			$sql_having = $sql_filter_1 . implode(" AND ",$sql_filter_2);
		
		}
		
		if ($filter_email != "") {
			
			$sql_where .= " AND ".PIX_TABLE_USER.".email_address LIKE '%$filter_email%' ";
			
			$this->_tpl->assign("filter_email",$filter_email);
		
		}
		
		if ($filter_name != "") {
			
			$sql_where .= " AND (".PIX_TABLE_USER.".first_name LIKE '%$filter_name%' OR ".PIX_TABLE_USER.".family_name LIKE '%$filter_name%') ";
			
			$this->_tpl->assign("filter_name",$filter_name);
		
		}
		
		
		$sql = "SELECT ".PIX_TABLE_USER.".*
											
				,CONCAT(".PIX_TABLE_USER.".family_name,', ',".PIX_TABLE_USER.".first_name) AS user_name
				
				,GROUP_CONCAT(".PIX_TABLE_SGME.".sys_group_id) AS `groups`
				
				FROM ".PIX_TABLE_USER." 
											
				LEFT JOIN ".PIX_TABLE_GRME." ON ".PIX_TABLE_USER.".userid = ".PIX_TABLE_GRME.".userid
				
				LEFT JOIN ".PIX_TABLE_SGME." ON ".PIX_TABLE_USER.".userid = ".PIX_TABLE_SGME.".sys_userid
				
				WHERE ".PIX_TABLE_USER.".userid AND ".PIX_TABLE_USER.".email_address != ''
				
				$sql_where
				
				GROUP BY ".PIX_TABLE_USER.".userid
				
				$sql_having
				
				$sql_order";

		// Count the total number of orphan images
		$total	= $this->_dbl->sqlCountSelectRows($sql);
		
		// Get an array of users from the database
		$user_data = $this->_dbl->sqlSelectRows($sql . getMultiImagePageLimitSQL($page));
		
		$query_string = "char=$char&amp;sort=$sort&amp;administrator=$administrator&amp;inactive=$inactive&amp;photograher=$photographer&amp;download=$download&amp;filter_name=$filter_name&amp;filter_email=$filter_email";
		
		$ipages = getMultiImagePageNavigation ("users", $page, $total, $query_string);
		
		if (is_array($user_data) && count($user_data) > 0) {
		
			foreach($user_data as $key => $value) {
				
				$userid[]				= $value['userid'];
				$email_address[]		= $value['email_address'];
				$user_name[]			= $value['user_name'];
				$first_name[]			= $value['first_name'];
				$family_name[]			= $value['family_name'];
				$date_registered[]		= strtotime($value['date_registered']);
				$date_edited[]			= strtotime($value['date_edited']);
				$date_expiration[]		= strtotime($value['date_expiration']);
				$account_status[]		= $value['account_status'];
				$addr1[]				= $value['addr1'];
				$addr2[]				= $value['addr2'];
				$addr3[]				= $value['addr3'];
				$country[]				= $value['country'];
				$postal_code[]			= $value['postal_code'];
				$telephone[]			= $value['telephone'];
				$fax[]					= $value['fax'];
				$city[]					= $value['city'];
				$region[]				= $value['region'];
				$password[]				= $value['password'] == "" ? 0:1;
				
			}
		
			// Assign the number of users to Smarty
			$this->_tpl->assign("user_count",count($user_data));
		
		} else {
		
			// Assign the number of users to Smarty
			$this->_tpl->assign("user_count",0);
		
		}
		
		// Assign users to Smarty
		$this->_tpl->assign("userid",$userid);
		$this->_tpl->assign("email_address",$email_address);
		$this->_tpl->assign("user_name",$user_name);
		$this->_tpl->assign("family_name",$family_name);
		$this->_tpl->assign("first_name",$first_name);
		$this->_tpl->assign("date_registered",$date_registered);
		$this->_tpl->assign("date_edited",$date_edited);
		$this->_tpl->assign("date_expiration",$date_expiration);
		$this->_tpl->assign("account_status",$account_status);
		$this->_tpl->assign("addr1",$addr1);
		$this->_tpl->assign("addr2",$addr2);
		$this->_tpl->assign("addr3",$addr3);
		$this->_tpl->assign("country",$country);
		$this->_tpl->assign("postal_code",$postal_code);
		$this->_tpl->assign("telephone",$telephone);
		$this->_tpl->assign("fax",$fax);
		$this->_tpl->assign("city",$city);
		$this->_tpl->assign("region",$region);
		$this->_tpl->assign("password",$password);
		$this->_tpl->assign("query_string",$query_string);
		$this->_tpl->assign("ipage_current",$ipages[0]);
		$this->_tpl->assign("ipage_numbers",$ipages[1]);
		$this->_tpl->assign("ipage_links",$ipages[2]);
		$this->_tpl->assign("sort",$sort);
		$this->_tpl->assign("method",$method);
			
		
		if ($objEnvData->fetchGlobal('action') == "exportUsersAsCSV") {
			
			// Send CSV force download headers
			pix_http_headers("csv","","userdata.csv");
			
			// Output the users page
			$this->_tpl->pixDisplay('admin.user/user.export.csv.txt');
		
		} else {
			
			// Send HTML content HTTP header and don't cache
			pix_http_headers("html","");
			
			// Set the page title
			$this->_tpl->assign("page_title","User management");
			
			// Output the users page
			$this->_tpl->pixDisplay('admin.user/user.tpl');
		
		}
		
	}
	
	/*
	*
	*	This method lists the images owned by a user with id $userid
	*
	*/
	function viewUserImages () {
	
		global $objEnvData, $ses, $cfg;
		
		$page = $objEnvData->fetchGlobal('page');
		
		// Get the userid for the person whose images we want to look at
		$userid = $objEnvData->fetchGlobal('userid');
		
		// Load the user class
		require_once('class.PixariaUser.php');
		
		// Create the user object
		$objUser = new PixariaUser($userid);
		
		// Generate SQL to view the user's images
		$sql = "SELECT * FROM ".PIX_TABLE_IMGS."
				
				WHERE image_userid = '$userid'
				
				ORDER BY image_filename";
		
		// Count the total number of orphan images
		$total	= $this->_dbl->sqlCountSelectRows($sql);
		
		// Select all images that are no in the gallery_order table (i.e. not yet in any galleries)
		$result = $this->_dbl->sqlSelectRows($sql . getMultiImagePageLimitSQL($page));
		
		$query_string = "cmd=viewUserImages&amp;userid=$userid";
		
		$ipages = getMultiImagePageNavigation ("user_images", $page, $total, $query_string);
		
		if (is_array($result)) {
		
			// Initialise arrays to hold image data
			$image_id	 	= array();
			$image_active	= array();
			$image_filename = array();
			$image_path		= array();
			$image_date		= array();
			$icon_path		= array();
			$icon_width		= array();
			$icon_height	= array();
			$comp_path		= array();
			$comp_width		= array();
			$comp_height	= array();
			$image_width	= array();
			$image_height	= array();
			$image_title	= array();
		
			foreach ($result as $key => $value) {
				
				$image_id[]			= $value['image_id'];
				$image_active[]		= $value['image_active'];
				$image_filename[] 	= $value['image_filename'];
				$image_path[]		= $value['image_path'];
				$image_date[]		= $value['image_date'];
				
				$icon_path[]		= base64_encode($cfg['sys']['base_library'].$value[image_path]."/32x32/".$value[image_filename]);
				$icon_size			= @getimagesize($cfg['sys']['base_library'].$value[image_path]."/32x32/".$value[image_filename]);
				$icon_width[]		= $icon_size[0];
				$icon_height[]		= $icon_size[1];
				
				$comp_path[]		= base64_encode($cfg['sys']['base_library'].$value[image_path]."/".COMPING_DIR."/".$value[image_filename]);
				$comp_size			= @getimagesize($cfg['sys']['base_library'].$value[image_path]."/".COMPING_DIR."/".$value[image_filename]);
				$comp_width[]		= $comp_size[0];
				$comp_height[]		= $comp_size[1];
				
				$image_width[]		= $value[image_width];
				$image_height[]		= $value[image_height];
				
				$image_title[]		= $value[image_title];
				
			}
			
			// Assign image information to Smarty
			$this->_tpl->assign("image_id",$image_id);
			$this->_tpl->assign("image_active",$image_active);
			$this->_tpl->assign("image_filename",$image_filename);
			$this->_tpl->assign("image_path",$image_path);
			$this->_tpl->assign("image_date",$image_date);
			$this->_tpl->assign("icon_path",$icon_path);
			$this->_tpl->assign("icon_width",$icon_width);
			$this->_tpl->assign("icon_height",$icon_height);
			$this->_tpl->assign("comp_path",$comp_path);
			$this->_tpl->assign("comp_width",$comp_width);
			$this->_tpl->assign("comp_height",$comp_height);
			$this->_tpl->assign("image_width",$image_width);
			$this->_tpl->assign("image_height",$image_height);
			$this->_tpl->assign("image_title",$image_title);
			$this->_tpl->assign("query_string",$query_string);
			$this->_tpl->assign("ipage_current",$ipages[0]);
			$this->_tpl->assign("ipage_numbers",$ipages[1]);
			$this->_tpl->assign("ipage_links",$ipages[2]);
			
			$this->_tpl->assign("images_present",(bool)true);
			
		}	
		
		$this->_tpl->assign("user_name",$objUser->getName());
		
		// Send HTML content HTTP header and don't cache
		pix_http_headers("html","");
		
		// Set the page title
		$this->_tpl->assign("page_title","Images owned by ".$objUser->getName());
		
		// Output the library page
		$this->_tpl->pixDisplay('admin.user/user.images.list.tpl');
		
	}
	
	/*
	*
	*	This method deletes one or more users with ids in array $users
	*
	*/
	function actionDeleteUsers () {
	
		global $objEnvData, $cfg, $ses;
		
		$users = $objEnvData->fetchGlobal('users');
		
		if (is_array($users)) {
		
			foreach ($users as $key => $value) {
				
				if ($value != $ses['psg_userid'] && is_numeric($value)) {
					
					// Delete the user from the users table
					@mysql_query("DELETE FROM ".PIX_TABLE_USER." WHERE userid = '".$this->db->escape($value)."'");
					
					// Delete the user from the groups table
					@mysql_query("DELETE FROM ".PIX_TABLE_GRPS." WHERE userid = '".$this->db->escape($value)."'");
								
					// Delete the user from the lightbox table
					@mysql_query("DELETE FROM ".PIX_TABLE_LBOX." WHERE userid = '".$this->db->escape($value)."'");
								
					// Delete the user from the lightbox_members table
					@mysql_query("DELETE FROM ".PIX_TABLE_LBME." WHERE userid = '".$this->db->escape($value)."'");
								
					// Delete the user from the cart table
					@mysql_query("DELETE FROM ".PIX_TABLE_CART." WHERE userid = '".$this->db->escape($value)."'");
								
					// Delete the user from the cart_members table
					@mysql_query("DELETE FROM ".PIX_TABLE_CRME." WHERE userid = '".$this->db->escape($value)."'");
								
					// Delete the user from the transaction_messages table
					@mysql_query("DELETE FROM ".PIX_TABLE_TMSG." WHERE userid = '".$this->db->escape($value)."'");						
				
					// Delete the user from the user preferences table
					@mysql_query("DELETE FROM ".PIX_TABLE_UPRF." WHERE user_id = '".$this->db->escape($value)."'");						
				
				}
				
			}
		
		}
	
		// Display the user list 
		$this->listAllUsers();			
				
	}
	
	/*
	*
	*	Method shows a confirmation page for deleting one or more users from the system
	*
	*/
	function formConfirmDeleteUsers () {
		
		global $objEnvData, $cfg, $ses;
		
		$users = $objEnvData->fetchGlobal('users');
		
		if (is_array($users)) {
		
			$this->_tpl->assign("users",$users);
			
			foreach ($users as $key => $value) {
			
				$userinfo			= getUserContactDetails($value);
				$user_name[]		= $userinfo[0];
				$email_address[]	= $userinfo[1];
				
			}
			
			$this->_tpl->assign("user_count","1");
			$this->_tpl->assign("email_address",$email_address);
			$this->_tpl->assign("user_name",$user_name);
			
			// Send HTML content HTTP header and don't cache
			pix_http_headers("html","");
		
			// Set the page title
			$this->_tpl->assign("page_title","Confirm delete users");
		
			// Output a page to confirm deleting of these user(s)
			$this->_tpl->pixDisplay('admin.user/user.delete.multi.confirm.tpl');
				
		} else {
		
			// Display the user list 
			$this->listAllUsers();			
		
		}
	
	}
	
	/*
	*
	*
	*
	*/
	function actionChangePrivileges ($cmd) {
	
		global $objEnvData, $cfg;
		
		$users = $objEnvData->fetchGlobal('users');
		
		if (is_array($users)) {
		
			foreach ($users as $key => $value) {
			
				switch ($cmd) {
				
					case "promoteAdmin":
						$sql = "INSERT INTO ".PIX_TABLE_SGME." VALUES ('1','".$this->db->escape($value)."');";
						$_POST['administrator'] = "1";
					break;
				
					case "promoteActive":
						$sql = "UPDATE ".PIX_TABLE_USER." SET account_status = '1' WHERE userid = '".$this->db->escape($value)."';";
						$_POST['inactive'] = "1";
					break;
				
					case "promotePhotographer":
						$sql = "INSERT INTO ".PIX_TABLE_SGME." VALUES ('3','".$this->db->escape($value)."');";
						$_POST['photographer'] = "1";
					break;
				
					case "promoteDownload":
						$sql = "INSERT INTO ".PIX_TABLE_SGME." VALUES ('5','".$this->db->escape($value)."');";
						$_POST['download'] = "1";
					break;
				
					case "demoteAdmin":
						$sql = "DELETE FROM ".PIX_TABLE_SGME." WHERE sys_group_id = '1' AND sys_userid = '".$this->db->escape($value)."';";
						$_POST['administrator'] = "1";
					break;
				
					case "demoteActive":
						$sql = "UPDATE ".PIX_TABLE_USER." SET account_status = '0' WHERE userid = '".$this->db->escape($value)."';";
						$_POST['inactive'] = "1";
					break;
				
					case "demotePhotographer":
						$sql = "DELETE FROM ".PIX_TABLE_SGME." WHERE sys_group_id = '3' AND sys_userid = '$value';";
						$_POST['photographer'] = "1";
					break;
				
					case "demoteDownload":
						$sql = "DELETE FROM ".PIX_TABLE_SGME." WHERE sys_group_id = '5' AND sys_userid = '$value';";
						$_POST['download'] = "1";
					break;
				
				}
			
				$this->_dbl->sqlQuery($sql);
				
			}
			
			$this->action_complete = true;
			
			$this->listAllUsers();
			
			exit;
			
		} else {
		
			$this->listAllUsers();
			
			exit;
			
		}
		
	}
	
}

?>