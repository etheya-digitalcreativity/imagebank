<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "user";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Check that the user is a SuperUser
//pix_authorise_user("administrator");

switch (aglobal('cmd')) {
	
	case 31:
	
		$sys_group_id 	= apost('sys_group_id');
		$users			= $_POST['users'];
		
		if ($sys_group_id == "1") { // We're editing the administrators group
		
			if (is_array($users)) {
				
				// First delete all users in this group except the admin user making the request (administrators can't remove themselves from that group)
				@mysql_query("DELETE FROM ".$cfg['sys']['table_sgme']." WHERE sys_group_id = '$sys_group_id' AND userid != '".$ses['psg_userid']."';");

				foreach ($users as $key => $value) {
					
					if ($value != $ses['psg_userid']) { // If the user isn't an admin
					
						// Now add users back into this group
						@mysql_query("INSERT INTO ".$cfg['sys']['table_sgme']." VALUES ('$sys_group_id','$value');");

					}
					
				}
			
			} else {
			
				// First delete all users in this group except the admin user making the request (administrators can't remove themselves from that group)
				@mysql_query("DELETE FROM ".$cfg['sys']['table_sgme']." WHERE sys_group_id = '$sys_group_id' AND userid != '".$ses['psg_userid']."';");

			}
		
		} else { // We're editing one of the other groups
		
			if (is_array($users)) {
				
				// First delete all users in this group except the admin user making the request
				@mysql_query("DELETE FROM ".$cfg['sys']['table_sgme']." WHERE sys_group_id = '$sys_group_id';");

				foreach ($users as $key => $value) {
					
					// Now add users back into this group
					@mysql_query("INSERT INTO ".$cfg['sys']['table_sgme']." VALUES ('$sys_group_id','$value');");

				
				}
			
			} else {
			
				// First delete all users in this group except the admin user making the request
				@mysql_query("DELETE FROM ".PIX_TABLE_GRME." WHERE group_id = '$group_id';");

			}
		
		}
		
		// Generate a redirection URL 
		$meta_url	= $cfg['sys']['base_url'] . $cfg['fil']['admin_groups'];
					
		// Show redirect page
		$smarty->pixWaiting($meta_url,"1");
	
	break;
	
	case 30:
		
		$sys_group_id = aget('sys_group_id');
		
		// Get an array of blogs from the database
		$sys_group_info = sql_select_row("SELECT * FROM ".PIX_TABLE_SGRP." WHERE sys_group_id = '$sys_group_id'");
		
		$sys_group_id			= $sys_group_info['sys_group_id'];
		$sys_group_name			= $sys_group_info['sys_group_name'];
		$sys_group_description	= $sys_group_info['sys_group_description'];
		
		// Get an array of the users in this group
		$sys_group_members	= sql_select_rows("SELECT * FROM ".$cfg['sys']['table_sgme']." WHERE sys_group_id = '$sys_group_id'");

		// Clean the list of viewers and sort into an array
		if (count($sys_group_members) > 0 && is_array($sys_group_members)) {
			foreach($sys_group_members as $key => $value) {
				$members[] = $sys_group_members[$key]['sys_userid'];
			}
		}
				
		$sql = "SELECT ".PIX_TABLE_USER.".*,
											
				CONCAT(".PIX_TABLE_USER.".family_name,', ',".PIX_TABLE_USER.".first_name) AS user_name
											
				FROM ".PIX_TABLE_USER." 
											
				LEFT JOIN ".PIX_TABLE_SGME." ON ".PIX_TABLE_USER.".userid = ".PIX_TABLE_SGME.".sys_userid
				
				WHERE ".PIX_TABLE_USER.".email_address != ''
				
				GROUP BY ".PIX_TABLE_USER.".userid
				
				ORDER BY ".PIX_TABLE_USER.".family_name ASC";
		
		// Get an array of users from the database
		$user_data = sql_select_rows($sql);
		
		if (is_array($user_data) && count($user_data) > 0) {
		
			foreach($user_data as $key => $value) {
				
				$userid[]				= $value['userid'];
				$email_address[]		= $value['email_address'];
				$user_name[]			= $value['user_name'];
				
				// Check if the user is in this group or not
				if (is_array($members) && in_array($value[userid],$members)) { $user_member[] = $value['userid']; } else { $user_member[] = 0; }
				
			}
		
		}
		
		// Assign users to Smarty
		$smarty->assign("userid",$userid);
		$smarty->assign("user_name",$user_name);
		$smarty->assign("email_address",$email_address);
		$smarty->assign("user_member",$user_member);
		
		// Set the page title
		$smarty->assign("page_title","Editing group: $sys_group_name");
		
		// Assign blog items to Smarty
		$smarty->assign("sys_group_id",$sys_group_id);
		$smarty->assign("sys_group_name",$sys_group_name);
		$smarty->assign("sys_group_description",$sys_group_description);
		
		// Output the library page
		smartyPixDisplay('admin.groups/sys.groups.01.tpl');
	
	break;
	
	case 21:
	
		$group_id 		= apost('group_id');
		
		if ($group_id != "1" && $group_id != "2") {
		
			// Delete this entry from the groups table (holds the group information)
			@mysql_query("DELETE FROM ".PIX_TABLE_GRPS." WHERE group_id = '$group_id'");
			
			// Delete this entry from the group_members table (hold user to group links)
			@mysql_query("DELETE FROM ".PIX_TABLE_GRME." WHERE group_id = '$group_id'");
			
			// Delete this entry from the image_viewers table (holds image to group links)
			@mysql_query("DELETE FROM ".$cfg['sys']['table_imvi']." WHERE userid = '$group_id'");
			
			// Delete this entry from the gallery_view table (holds gallery to group links)
			@mysql_query("DELETE FROM ".$cfg['sys']['table_gvie']." WHERE group_id = '$group_id'");
			
		
		}
		
		// Generate a redirection URL 
		$meta_url	= $cfg['sys']['base_url'] . $cfg['fil']['admin_groups'];
					
		// Show redirect page
		$smarty->pixWaiting($meta_url,"1");
	
	break;
	
	case 20:
		
		$group_id 		= aget('group_id');
		
		if ($group_id != "1" && $group_id != "2") {
		
			// Get an array of blogs from the database
			$group_info = sql_select_row("SELECT * FROM ".PIX_TABLE_GRPS." WHERE group_id = '$group_id'");
			
			// Set the page title
			$smarty->assign("page_title","Delete a group");
			
			// Set the page title
			$smarty->assign("group_name",$group_info['group_name']);
			
			// Set the group_id
			$smarty->assign("group_id",$group_id);
			
			// Display the html header
			smartyPixDisplay('admin.snippets/admin.html.header.tpl');
			
			// Output the library page
			smartyPixDisplay('admin.groups/groups.03.tpl');
			
			## Display the html footer
			smartyPixDisplay('admin.snippets/admin.html.footer.tpl');
		
		} else { // User is somehow trying to delete one of the built in groups (administrators or image editors)
		
			// Generate a redirection URL 
			$meta_url	= $cfg['sys']['base_url'] . $cfg['fil']['admin_groups'];
					
			// Show redirect page
			$smarty->pixWaiting($meta_url,"1");
		
		}
		
	break;
	
	case 11: // This case handles editing the title and members of a group
	
		$group_id 		= apost('group_id');
		$group_name		= addslashes(apost('group_name'));
		$users			= $_POST['users'];
		
		if ($group_id == "1") { // We're editing the administrators group
		
			if (is_array($users)) {
				
				// First delete all users in this group except the admin user making the request (administrators can't remove themselves from that group)
				@mysql_query("DELETE FROM ".PIX_TABLE_GRME." WHERE group_id = '$group_id' AND userid != '".$ses['psg_userid']."';");

				foreach ($users as $key => $value) {
					
					if ($value != $ses['psg_userid']) { // If the user isn't an admin
					
						// Now add users back into this group
						@mysql_query("INSERT INTO ".PIX_TABLE_GRME." VALUES ('$group_id','$value');");

					}
					
				}
			
			} else {
			
				// First delete all users in this group except the admin user making the request (administrators can't remove themselves from that group)
				@mysql_query("DELETE FROM ".PIX_TABLE_GRME." WHERE group_id = '$group_id' AND userid != '".$ses['psg_userid']."';");

			}
		
		} else { // We're editing one of the other groups
		
			if (is_array($users)) {
				
				// First delete all users in this group except the admin user making the request
				@mysql_query("DELETE FROM ".PIX_TABLE_GRME." WHERE group_id = '$group_id';");

				foreach ($users as $key => $value) {
					
					// Now add users back into this group
					@mysql_query("INSERT INTO ".PIX_TABLE_GRME." VALUES ('$group_id','$value');");

				
				}
			
			} else {
			
				// First delete all users in this group except the admin user making the request
				@mysql_query("DELETE FROM ".PIX_TABLE_GRME." WHERE group_id = '$group_id';");

			}
		
		}
		
		// Update the group name
		@mysql_query("UPDATE ".PIX_TABLE_GRPS." SET group_name = '$group_name' WHERE group_id = '$group_id'");
		
		// Generate a redirection URL 
		$meta_url	= $cfg['sys']['base_url'] . $cfg['fil']['admin_groups'];
					
		// Show redirect page
		$smarty->pixWaiting($meta_url,"1");
		
	break;
	
	case 10:
		
		$group_id = aget('group_id');
		
		// Get an array of blogs from the database
		$group_info = sql_select_row("SELECT * FROM ".PIX_TABLE_GRPS." WHERE group_id = '$group_id'");
		
		$group_id		= $group_info['group_id'];
		$group_name		= $group_info['group_name'];
		
		// Get an array of the users in this group
		$group_members	= sql_select_rows("SELECT * FROM ".PIX_TABLE_GRME." WHERE group_id = '$group_id'");

		// Clean the list of viewers and sort into an array
		if (count($group_members) > 0 && is_array($group_members)) {
			foreach($group_members as $key => $value) {
				$members[] = $group_members[$key]['userid'];
			}
		}
				
		$sql = "SELECT ".PIX_TABLE_USER.".*,
											
				CONCAT(".PIX_TABLE_USER.".family_name,', ',".PIX_TABLE_USER.".first_name) AS user_name
											
				FROM ".PIX_TABLE_USER." 
											
				LEFT JOIN ".PIX_TABLE_GRME." ON ".PIX_TABLE_USER.".userid = ".PIX_TABLE_GRME.".userid
				
				WHERE ".PIX_TABLE_USER.".email_address != ''
				
				GROUP BY ".PIX_TABLE_USER.".userid
				
				ORDER BY ".PIX_TABLE_USER.".family_name ASC";
		
		// Get an array of users from the database
		$user_data = sql_select_rows($sql);
		
		if (is_array($user_data) && count($user_data) > 0) {
		
			foreach($user_data as $key => $value) {
				
				$userid[]				= $value['userid'];
				$email_address[]		= $value['email_address'];
				$user_name[]			= $value['user_name'];
				
				// Check if the user is in this group or not
				if (is_array($members) && in_array($value[userid],$members)) { $user_member[] = $value['userid']; } else { $user_member[] = 0; }
				
			}
		
		}
		
		// Assign users to Smarty
		$smarty->assign("userid",$userid);
		$smarty->assign("user_name",$user_name);
		$smarty->assign("email_address",$email_address);
		$smarty->assign("user_member",$user_member);
		
		// Set the page title
		$smarty->assign("page_title","Editing group: $group_name");
		
		// Assign blog items to Smarty
		$smarty->assign("group_id",$group_id);
		$smarty->assign("group_name",$group_name);
		
		// Output the library page
		smartyPixDisplay('admin.groups/groups.02.tpl');
				
	break;
	
	case 01:
		
		// This case deals with creating a new group entry
		// Function replicated in admin.gallery.php for new blog when creating gallery
		
		// Laod information from form
		$group_name		= addslashes(apost('group_name'));
		
		$sql = "INSERT INTO ".PIX_TABLE_GRPS." VALUES('0','$group_name');";
		@mysql_query($sql);
		
		// Generate a redirection URL 
		$meta_url	= $cfg['sys']['base_url'] . $cfg['fil']['admin_groups'];
					
		// Show redirect page
		$smarty->pixWaiting($meta_url,"1");			

	break;
	
	
	default:
		
		// Get an array of blogs from the database
		$group_info = sql_select_rows("SELECT * FROM ".PIX_TABLE_GRPS." ORDER BY group_name ASC");
		
		if (is_array($group_info) && count($group_info) > 0) {
		
			foreach($group_info as $key => $value) {
				
				$group_id[]		= $value['group_id'];
				$group_name[]	= $value['group_name'];
				
			}
		
		}
		
		// Get an array of blogs from the database
		$sys_group_info = sql_select_rows("SELECT * FROM ".PIX_TABLE_SGRP." ORDER BY sys_group_name ASC");
		
		if (is_array($sys_group_info) && count($sys_group_info) > 0) {
		
			foreach($sys_group_info as $key => $value) {
				
				$sys_group_id[]				= $value['sys_group_id'];
				$sys_group_name[]			= $value['sys_group_name'];
				$sys_group_description[]	= $value['sys_group_description'];
				
			}
		
		}
		
		// Set the page title
		$smarty->assign("page_title","User groups");
		
		// Assign the number of groups to Smarty
		$smarty->assign("group_count",count($group_info));
		
		// Assign normal group items to Smarty
		$smarty->assign("group_id",$group_id);
		$smarty->assign("group_name",$group_name);
		
		// Assign the number of system groups to Smarty
		$smarty->assign("sys_group_count",count($sys_group_info));
		
		// Assign system group items to Smarty
		$smarty->assign("sys_group_id",$sys_group_id);
		$smarty->assign("sys_group_name",$sys_group_name);
		$smarty->assign("sys_group_description",$sys_group_description);
		
		// Output the library page
		smartyPixDisplay('admin.groups/groups.01.tpl');
		
	break;

}


?>