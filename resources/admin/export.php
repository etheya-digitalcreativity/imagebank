<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Check that the user is a SuperUser
pix_authorise_user("administrator");

// Set the site section
$admin_page_section = "tools";

// Initialise the class for this page
$export = new Export();

class Export {
	
	/*
	*	DATABASE EXPORTER
	*/
	function Export() {
		
		// Load the database class
		require_once ('class.Database.php');
		require_once (SYS_BASE_PATH . 'resources/classes/class.InputData.php');
		
		// Create required objects
		$this->db		= new Database();
		$this->input	= new InputData();
		$this->view		= new Smarty_Pixaria;
	
		switch ($this->input->post('cmd')) {
				
			case "exportDatabase":
				$this->exportDatabase();
			break;
		
			default:
				$this->index();
			break;
			
		}
	
	}
	
	/*
	*
	*
	*
	*/
	function index () {
		
		global $cfg;
		
		$this->view->assign('page_title','Database Exporter');
		$this->view->assign("dbname",$cfg['sys']['db_name']);
		
		$this->view->display("export/index.html");
	
	}
	
	/*
	*
	*
	*
	*/
	function exportDatabase () {
		
		global $cfg;
		
		require_once (SYS_BASE_PATH . "resources/classes/class.MySQLDump.php");
		
		if ($this->input->checkbox('compress')) {
			$extension = "sql.gz";
		} else {
			$extension = "sql";
		}
		
		$file_path = $cfg['set']['temporary']. time() . ".sql";
		
		//Instantiate the MySQL dump class
		$dump = new MySQLDump($cfg['sys']['db_name'], $file_path, $this->input->checkbox('compress'));
		
		// Dump the database into a file
		$dump->doDump();
		
		//send file to standard output
		header ('Content-Type: application/octet-stream');
		header ("Content-Disposition: attachment; filename=\"Pixaria_Gallery_Dump.$extension\"");
		
		readfile($file_path);
		
		//delete temporary files
		@unlink($file_path);
			
	}
	
}