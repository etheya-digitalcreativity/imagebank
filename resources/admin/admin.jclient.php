<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "images";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Check that the user is a SuperUser
pix_authorise_user("administrator");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Define the navigation section for the admin toolbar
$smarty->assign("toolbar","library");

$objAdminJclient = new Admin_JClient();

/*
*
*	
*
*/
class Admin_JClient {
	
	var $jclient_folder;
	var $jclient_path;
	
	/*
	*
	*	
	*
	*/
	function Admin_JClient() {
		
		global $objEnvData, $cfg, $ses;
		
		$this->jclient_folder 	= $ses['psg_userid'];
		$this->jclient_path 	= $cfg['sys']['base_path'] . "resources/javaupload/" . $this->jclient_folder . "/";
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		switch ($objEnvData->fetchGlobal('cmd')) {

			case "showFormSettings":
				$this->showFormSettings();
			break;

			case "actionSaveSettings":
				$this->actionSaveSettings();
			break;

			case "showUploadedFiles":
				$this->showUploadedFiles();
			break;

			case "processUploadedFiles":
				$this->processUploadedFiles();
			break;

			case "formDeleteImageFiles":
				$this->formDeleteImageFiles();
			break;

			default:
				$this->jFileUpload();
			break;

		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function jFileUpload () {
		
		global $smarty, $objEnvData, $cfg, $ses;
		
		$smarty->assign("page_title","Java FTP upload with resize");
		$smarty->assign("jclient_folder",$this->jclient_folder);
		$smarty->assign("account_path",preg_replace("/\/+/","/",$cfg['set']['ftp_path']."/".$ses['psg_userid']."/"));
		
		$smarty->pixDisplay('admin.jclient/upload.view.tpl');
		
		exit;
	
	}
	
	/*
	*
	*	
	*
	*/
	function showUploadedFiles () {
	
		global $cfg, $smarty, $objEnvData;
				
		// Find out what valid photo albums are on the system and present them as options to the user
		$handle=@opendir($this->jclient_path);
		if($handle != FALSE) {
			$fn=1;
			while (false!==($filename = readdir($handle))) { 
				
				if (substr($filename,0,1) != ".") { // Exclude filenames starting with a period
					
					if (substr($filename,0,9) == "original_") {
						
						$pathinfo = pathinfo($this->jclient_path.$filename);
						
						$original_filename = eregi_replace("^original_","",$filename);
						
						if (strtolower($pathinfo['extension'])==("tif"||"jpg"||"mov"||"gif"||"png"||"tiff"||"avi")) { // Only include image files
						
							$imageinfo = @getimagesize($this->jclient_path.$filename);
							
							$image_data[$fn]['comp_valid']	= file_exists($this->jclient_path."comp_".$original_filename);
							$image_data[$fn]['orig_valid']	= file_exists($this->jclient_path."original_".$original_filename);
							$image_data[$fn]['filepath']	= base64_encode($this->jclient_path.$filename);
							$image_data[$fn]['filename']	= $original_filename;
							$image_data[$fn]['filesize']	= round((filesize($this->jclient_path.$filename) / 1024),2);
							$image_data[$fn]['width']		= $imageinfo[0];
							$image_data[$fn]['height']		= $imageinfo[1];
							$image_data[$fn]['filemtime']	= filemtime($this->jclient_path.$filename);
							$fn++;
							
						}
					
					}
					
				}
				
			}
			
		}
		
		@closedir($handle);
		
		if (is_array($image_data)) {
		
			// Sort the items into alphabetical order (not all servers do it automatically)
			@natsort($image_data);
			
			foreach ($image_data as $key => $value) {
			
				$comp_valid[]	= $value['comp_valid'];
				$orig_valid[]	= $value['orig_valid'];
				$filepath[]		= $value['filepath'];
				$filename[]		= $value['filename'];
				$filesize[]		= $value['filesize'];
				$width[]		= $value['width'];
				$height[]		= $value['height'];
				$filemtime[]	= $value['filemtime'];
			
			}
		
		}
		
		/*
		*	List directories in the incoming directory
		*/
		$handle=@opendir($cfg['sys']['base_incoming']);
		if($handle != FALSE) {
		while (false!==($file = readdir($handle))) { 
			if (is_dir($cfg['sys']['base_incoming'] . $file) && substr($file,0,1) != ".") { 
				$show_directories 	= (bool)TRUE;
				$incoming[]			= $file;
			}
		}
		}
		@closedir($handle);
		
		
		/*
		*	List directories in the library directory
		*/
		$handle=@opendir($cfg['sys']['base_library']);
		if($handle != FALSE) {
		while (false!==($file = readdir($handle))) { 
			if (is_dir($cfg['sys']['base_library'] . $file) && substr($file,0,1) != ".") { 
				$show_directories 	= (bool)TRUE;
				$library[]			= $file;
			}
		}
		}
		@closedir($handle);
		
		/*
		*	Input directory information into Smarty
		*/
		$smarty->assign("show_directories",$show_directories);
		$smarty->assign("incoming",$incoming);
		$smarty->assign("library",$library);
		
		// Tell Smarty whether or not to show a table of items
		if (is_array($filepath)) { $smarty->assign("display","TRUE"); }
		
		// Assign variables to Smarty object
		$smarty->assign("comp_valid",$comp_valid);
		$smarty->assign("orig_valid",$orig_valid);
		$smarty->assign("filepath",$filepath);
		$smarty->assign("filename",$filename);
		$smarty->assign("filesize",$filesize);
		$smarty->assign("width",$width);
		$smarty->assign("height",$height);
		$smarty->assign("filemtime",$filemtime);
		
		// The page title
		$smarty->assign("page_title","Images uploaded by Java FTP");

		// Display the html header
		$smarty->pixDisplay('admin.jclient/upload.images.list.tpl');
		
		exit;
		
	}
	
	/*
	*
	*	
	*
	*/
	function processUploadedFiles () {
	
		global $cfg, $smarty, $objEnvData;
		
		$directory_name = $objEnvData->fetchGlobal('directory_name');		
		$images 		= $objEnvData->fetchGlobal('images');
		
		$error			= false;
		
		$storage_path	= $cfg['sys']['base_incoming'] . $directory_name;
		$storage_comp	= $cfg['sys']['base_incoming'] . $directory_name . "/" . COMPING_DIR;
		$storage_orig	= $cfg['sys']['base_incoming'] . $directory_name . "/original";
		
		// Check there is a directory name
		if (trim($directory_name) == "") {
			$error 				= true;
			$error_message[] 	= "You must specify a directory to place the image files into [".__LINE__."]";
		}
		
		// If the directory doesn't exist, we need to create it
		if (!is_dir($storage_path)) {
			if (!$this->createDirectory($storage_path)) {
				$error 				= true;
				$error_message[] 	= "Unable to create a directory to put the image files into [".__LINE__."]";
			}
		}
		
		// If the directory doesn't exist, we need to create it
		if (!is_dir($storage_orig)) {
			if (!$this->createDirectory($storage_orig)) {
				$error 				= true;
				$error_message[] 	= "Unable to create the 'original' directory to put the full size image files into [".__LINE__."]";
			}
		}
		
		// If the directory doesn't exist, we need to create it
		if (!is_dir($storage_comp)) {
			if (!$this->createDirectory($storage_comp)) {
				$error 				= true;
				$error_message[] 	= "Unable to create the '".COMPING_DIR."' directory to put the full size image files into [".__LINE__."]";
			}
		}
		
		if (!$error) {
		
			// Loop through the selected images and move them
			foreach ($images as $key => $value) {
				
				// Move the comping image
				if (file_exists($this->jclient_path."comp_".$value) && is_writable($this->jclient_path."comp_".$value)) {
					@rename($this->jclient_path."comp_".$value , $storage_comp."/".$value);
				} else {
					$error 				= true;
					$error_message[] 	= "Unable to move the image 'comp_".$value."' [".__LINE__."]";
				}
				
				// Move the original image
				if (file_exists($this->jclient_path."original_".$value) && is_writable($this->jclient_path."original_".$value)) {
					@rename($this->jclient_path."original_".$value , $storage_orig."/".$value);
				} else {
					$error 				= true;
					$error_message[] 	= "Unable to move the image 'original_".$value."' [".__LINE__."]";
				}
				
			}
		
		}
		
		$smarty->assign("directory_name",$directory_name);
		$smarty->assign("error",$error);
		$smarty->assign("error_message",$error_message);
		
		// The page title
		$smarty->assign("page_title","Upload complete");

		// Display the html header
		$smarty->pixDisplay('admin.jclient/upload.complete.tpl');
		
	}
	
	/*
	*
	*	Delete uploaded images
	*
	*/
	function formDeleteImageFiles () {
		
		global $objEnvData, $smarty;
		
		$images = $objEnvData->fetchGlobal('images');
		
		if (is_array($images)) {
			
			foreach ($images as $key => $value) {
				
				if (file_exists($this->jclient_path."comp_".$value)) {
					@unlink($this->jclient_path."comp_".$value);
				}
				
				if (file_exists($this->jclient_path."original_".$value)) {
					@unlink($this->jclient_path."original_".$value);
				}
				
			}
		
			$smarty->assign("action_complete",true);
		
		}
		
		$this->showUploadedFiles();
		
	}
	
	/*
	*
	*	Show the FTP upload settings
	*
	*/
	function showFormSettings () {
	
		global $cfg, $smarty, $objEnvData;
		
		// The page title
		$smarty->assign("page_title","FTP Upload Settings");

		// Display the html header
		$smarty->pixDisplay('admin.jclient/settings.tpl');
		
	}
	
	/*
	*
	*	Save changes to the FTP settings
	*
	*/
	function actionSaveSettings () {
	
		global $cfg, $smarty, $objEnvData;
		
		$ftp_address	= $objEnvData->fetchPost('ftp_address');
		$ftp_path		= $objEnvData->fetchPost('ftp_path');
		$ftp_username	= $objEnvData->fetchPost('ftp_username');
		$ftp_password	= $objEnvData->fetchPost('ftp_password');

		if (isset($ftp_address)) {
			$sql = "UPDATE ".$cfg['sys']['table_sett']." SET value = '$ftp_address' WHERE name = 'ftp_address';";
			$this->_dbl->sqlQuery($sql);	
		}
		
		if (isset($ftp_path)) {
			$sql = "UPDATE ".$cfg['sys']['table_sett']." SET value = '$ftp_path' WHERE name = 'ftp_path';";
			$this->_dbl->sqlQuery($sql);	
		}
		
		if (isset($ftp_username)) {
			$sql = "UPDATE ".$cfg['sys']['table_sett']." SET value = '$ftp_username' WHERE name = 'ftp_username';";
			$this->_dbl->sqlQuery($sql);	
		}
		
		if (isset($ftp_password)) {
			$sql = "UPDATE ".$cfg['sys']['table_sett']." SET value = '$ftp_password' WHERE name = 'ftp_password';";
			$this->_dbl->sqlQuery($sql);	
		}
		
		$smarty->assign("set_ftp_address",$ftp_address);
		$smarty->assign("set_ftp_path",$ftp_path);
		$smarty->assign("set_ftp_username",$ftp_username);
		$smarty->assign("set_ftp_password",$ftp_password);
		
		$smarty->assign("action_complete",true);
		
		// Show the applet form
		$this->jFileUpload();
		
	}
	
	/*
	*
	*	Check if a directory exists and if it doesn't make it
	*
	*/
	function createDirectory($path) {
		
		global $cfg;
		
		if (!file_exists($path)) {
			
			$umask_old = umask(0);
			mkdir($path, 0777);
			$directory_created = TRUE;
			umask($old);
			
		} elseif (substr(sprintf('%o', fileperms($path)), -4) != 0777) {
			
			$path = umask(0);
			@chmod($path,0777);
			umask($old);
			
			$directory_created = FALSE;
			
		}
		
		return $directory_created;
	
	}
	
}

?>