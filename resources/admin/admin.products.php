<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "products";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Check that the user is a SuperUser
pix_authorise_user("administrator");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

switch($objEnvData->fetchGlobal('cmd')) {
	
	case 5:
	
		// Load the product management class
		require_once ('class.PixariaProduct.php');
		
		// Create a new product object
		$objProduct = new PixariaProduct($objEnvData->fetchGlobal('prod_id'));
		
		// If the user has agreed to delete this product
		if ($objEnvData->fetchGlobal('js_confirmed') == 1) {
		
			// Delete the product from the database
			$objProduct->deleteProduct();
		
		}
		
		// Redirect back to the main products page 
		$meta_url	= $cfg['sys']['base_url'] . FILE_ADM_PRODUCTS;
					
		// Display a redirection screen
		$smarty->pixWaiting($meta_url,"1");
		
	break;
	
	case 4: // Save changes to a product
	
		// Load the product management class
		require_once ('class.PixariaProduct.php');
		
		// Create a new product object
		$objProduct = new PixariaProduct($objEnvData->fetchPost('prod_id'));
		
		// Pass the form data into the product object
		$objProduct->setProdActive($objEnvData->fetchPost('prod_active'));
		$objProduct->setProdType($objEnvData->fetchPost('prod_type'));
		$objProduct->setProdName($objEnvData->fetchPost('prod_name'));
		$objProduct->setProdDescription($objEnvData->fetchPost('prod_description'));
		$objProduct->setProdPrice($objEnvData->fetchPost('prod_price'));
		$objProduct->setProdShipping($objEnvData->fetchPost('prod_shipping'));
		$objProduct->setProdShippingMultiple($objEnvData->fetchPost('prod_shipping_multiple'));
		$objProduct->setProdDownloadSize($objEnvData->fetchPost('prod_dnl_size'));
		
		// Check to see if there are any errors in the data
		if ($objProduct->error) {
			
			// Assign errors to Smarty
			$smarty->assign("error",$objProduct->error);
			$smarty->assign("error_log",$objProduct->error_log);
			
			// Pass variables back to Smarty
			$smarty->assign("prod_id",$objEnvData->fetchPost('prod_id'));
			$smarty->assign("prod_active",$objEnvData->fetchPost('prod_active'));
			$smarty->assign("prod_type",$objEnvData->fetchPost('prod_type'));
			$smarty->assign("prod_name",$objEnvData->fetchPost('prod_name'));
			$smarty->assign("prod_description",$objEnvData->fetchPost('prod_description'));
			$smarty->assign("prod_price",$objEnvData->fetchPost('prod_price'));
			$smarty->assign("prod_shipping",$objEnvData->fetchPost('prod_shipping'));
			$smarty->assign("prod_shipping_multiple",$objEnvData->fetchPost('prod_shipping_multiple'));
			$smarty->assign("prod_dnl_size",$objEnvData->fetchPost('prod_dnl_size'));
			$smarty->assign("comping_dir",COMPING_DIR);
			
			// Set the page title
			$smarty->assign("page_title","Product manager");
			
			// Display the product creation form
			$smarty->pixDisplay('admin.products/product.edit.tpl');
			
		} else { // There are no errors - create a new entry
			
			// Add this product to the database
			$objProduct->updateProduct();
			
			// Redirect back to the main products page 
			$meta_url	= $cfg['sys']['base_url'] . FILE_ADM_PRODUCTS;
						
			// Display a redirection screen
			$smarty->pixWaiting($meta_url,"1");
			
		}
	
	break;
	
	case 2: // Show the product editing form
	
		// Load the product management class
		require_once ('class.PixariaProduct.php');
		
		// Create a new product object
		$objProduct = new PixariaProduct($objEnvData->fetchGlobal('prod_id'));
		
		// Assign the product ID to Smarty
		$smarty->assign("prod_id",$objEnvData->fetchGlobal('prod_id'));
		
		$smarty->assign("prod_type",$objProduct->getProdType());
		$smarty->assign("prod_active",$objProduct->getProdActive());
		$smarty->assign("prod_name",$objProduct->getProdName());
		$smarty->assign("prod_description",$objProduct->getProdDescription());
		$smarty->assign("prod_price",$objProduct->getProdPrice());
		$smarty->assign("prod_shipping",$objProduct->getProdShipping());
		$smarty->assign("prod_shipping_multiple",$objProduct->getProdShippingMultiple());
		$smarty->assign("prod_dnl_size",$objProduct->getProdDownloadSize());
		$smarty->assign("comping_dir",COMPING_DIR);
		
		// Set the page title
		$smarty->assign("page_title","Edit product");
			
		// Display the product editor form
		$smarty->pixDisplay('admin.products/product.edit.tpl');
	
	break;
	
	case 1: // Add a new product to the database
		
		// Load the product management class
		require_once ('class.PixariaProduct.php');
		
		// Create a new product object
		$objProduct = new PixariaProduct();
		
		// Pass the form data into the product object
		$objProduct->setProdType($objEnvData->fetchPost('prod_type'));
		$objProduct->setProdName($objEnvData->fetchPost('prod_name'));
		$objProduct->setProdDescription($objEnvData->fetchPost('prod_description'));
		$objProduct->setProdPrice($objEnvData->fetchPost('prod_price'));
		$objProduct->setProdShipping($objEnvData->fetchPost('prod_shipping'));
		$objProduct->setProdShippingMultiple($objEnvData->fetchPost('prod_shipping_multiple'));
		$objProduct->setProdDownloadSize($objEnvData->fetchPost('prod_dnl_size'));
		$objProduct->setProdActive($objEnvData->fetchPost('prod_active'));
		
		// Check to see if there are any errors in the data
		if ($objProduct->error) {
			
			// Assign errors to Smarty
			$smarty->assign("error",$objProduct->error);
			$smarty->assign("error_log",$objProduct->error_log);
		
			// Pass variables back to Smarty to show errors in the form
			$smarty->assign("prod_active",$objEnvData->fetchPost('prod_active'));
			$smarty->assign("prod_type",$objEnvData->fetchPost('prod_type'));
			$smarty->assign("prod_name",$objEnvData->fetchPost('prod_name'));
			$smarty->assign("prod_description",$objEnvData->fetchPost('prod_description'));
			$smarty->assign("prod_price",$objEnvData->fetchPost('prod_price'));
			$smarty->assign("prod_shipping",$objEnvData->fetchPost('prod_shipping'));
			$smarty->assign("prod_shipping_multiple",$objEnvData->fetchPost('prod_shipping_multiple'));
			$smarty->assign("prod_dnl_size",$objEnvData->fetchPost('prod_dnl_size'));
			$smarty->assign("comping_dir",COMPING_DIR);
			
			// Set the page title
			$smarty->assign("page_title","Product manager");
			
			// Display the product creation form
			$smarty->pixDisplay('admin.products/product.create.tpl');
			
		} else { // There are no errors - create a new entry
			
			// Add this product to the database
			$objProduct->createProduct();
			
			// Redirect back to the main products page
			$meta_url	= $cfg['sys']['base_url'] . FILE_ADM_PRODUCTS;
						
			// Display a redirection screen
			$smarty->pixWaiting($meta_url,"1");
			
		}
		
	break;
	
	default: // Display a page to view all calculators
		
		// Load the product management class
		require_once ('class.PixariaProduct.php');
		
		// Create a new product object
		$objProduct = new PixariaProduct();
		
		$products = $objProduct->getProducts();
		
		if (!$products) {
		
			$smarty->assign("show_list",(bool)false);
		
		} else {
		
			$smarty->assign("show_list",(bool)true);
			$smarty->assign("prod_id",$products['prod_id']);
			$smarty->assign("prod_active",$products['prod_active']);
			$smarty->assign("prod_type_list",$products['prod_type']);
			$smarty->assign("prod_name",$products['prod_name']);
			$smarty->assign("prod_description",$products['prod_description']);
			$smarty->assign("prod_price",$products['prod_price']);
			$smarty->assign("prod_shipping",$products['prod_shipping']);
			$smarty->assign("prod_shipping_multiple",$products['prod_shipping_multiple']);
			$smarty->assign("comping_dir",COMPING_DIR);
		
		}
		
		// Set the page title
		$smarty->assign("page_title","Product manager");
		
		// Display the main products page
		$smarty->pixDisplay('admin.products/products.list.tpl');
		
	break;

}


?>