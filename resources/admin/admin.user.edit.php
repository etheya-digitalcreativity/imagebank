<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "user";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Check that the user is a SuperUser
pix_authorise_user("administrator");

// Instantiate the class
$objUsers = new PixariaUsers();

/*
*
*	
*
*/
class PixariaUsers {

	function PixariaUsers () {
	
		global $cfg, $ses, $objEnvData;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		switch ($objEnvData->fetchGlobal('cmd')) {
			
			case "actionDeleteUser":
			
				$this->actionEditUser();
			
			break;
			
			case "formConfirmDeleteUser":
			
				$this->formConfirmDeleteUser();
			
			break;
			
			case "actionEditUser":
			
				$this->actionEditUser();
			
			break;
			
			case "showFormUserEdit": default:
				
				$this->showFormUserEdit();
			
			break;
		
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function actionDeleteUser () {
	
		global $cfg, $smarty, $ses, $lang, $objEnvData;
		
		$userid = $objEnvData->fetchPost('userid');
		
		if ($userid != $ses['psg_userid'] && is_numeric($userid)) { // If this user is not trying to delete themselves and the userid is a valid integer
		
			// Delete the user from the users table
			$this->_dbl->sqlQuery("DELETE FROM ".PIX_TABLE_USER." WHERE userid = '$userid'");
			
			// Delete the user from the groups table
			$this->_dbl->sqlQuery("DELETE FROM ".PIX_TABLE_GRPS." WHERE userid = '$userid'");
						
			// Delete the user from the lightbox table
			$this->_dbl->sqlQuery("DELETE FROM ".PIX_TABLE_LBOX." WHERE userid = '$userid'");
						
			// Delete the user from the lightbox_members table
			$this->_dbl->sqlQuery("DELETE FROM ".PIX_TABLE_LBME." WHERE userid = '$userid'");
						
			// Delete the user from the cart table
			$this->_dbl->sqlQuery("DELETE FROM ".PIX_TABLE_CART." WHERE userid = '$userid'");
						
			// Delete the user from the cart_members table
			$this->_dbl->sqlQuery("DELETE FROM ".PIX_TABLE_CRME." WHERE userid = '$userid'");
						
			// Delete the user from the transaction_messages table
			$this->_dbl->sqlQuery("DELETE FROM ".PIX_TABLE_TMSG." WHERE userid = '$userid'");
						
		}
	
		// Take the user back to the user manager screen
		
		// Generate a redirection URL 
		$meta_url	= $cfg['sys']['base_url'] . $cfg['fil']['admin_user'];
					
		// Show redirect page
		$smarty->pixWaiting($meta_url,"1");			
		
	}
	
	/*
	*
	*	
	*
	*/
	function formConfirmDeleteUser () {
	
		global $cfg, $smarty, $ses, $lang, $objEnvData;
		
		$userid = $objEnvData->fetchGlobal('userid');
		
		$sql	= "	SELECT *, CONCAT(first_name,' ',family_name) AS user_name
		
					FROM " . PIX_TABLE_USER . "
					
					WHERE userid = '$userid'";
		
		$result = $this->_dbl->sqlSelectRow($sql);
		
		$userid 			= $result['userid'];
		$user_name 			= $result['user_name'];
		
		// Assign user variables to smarty
		$smarty->assign("userid",$userid);
		$smarty->assign("user_name",$user_name);

		// Define html page title
		$smarty->assign("page_title","Delete user");
		
		// Output html from template file
		$smarty->pixDisplay('admin.user/user.delete.01.tpl');
	
	}
	
	/*
	*
	*	
	*
	*/
	function actionEditUser () {
	
		global $cfg, $smarty, $ses, $lang, $objEnvData;
		
		require_once ("class.PixariaUser.php");	
		
		$objPixariaUser = new PixariaUser($objEnvData->fetchGlobal('userid'));
		
		$objPixariaUser->setFirstName($objEnvData->fetchGlobal('first_name'));
		$objPixariaUser->setFamilyName($objEnvData->fetchGlobal('family_name'));
		$objPixariaUser->setEmail($objEnvData->fetchGlobal('email_address'));
		$objPixariaUser->setIsAdministrator($objEnvData->fetchGlobal('is_admin'));
		$objPixariaUser->setIsEditor($objEnvData->fetchGlobal('is_editor'));
		$objPixariaUser->setIsPhotographer($objEnvData->fetchGlobal('is_photo'));
		$objPixariaUser->setIsDelete($objEnvData->fetchGlobal('is_delete'));
		$objPixariaUser->setIsDownload($objEnvData->fetchGlobal('is_download'));
		$objPixariaUser->setGroups($objEnvData->fetchGlobal('groups'));

		$objPixariaUser->setPassword1($objEnvData->fetchGlobal('password_1'));
		$objPixariaUser->setPassword2($objEnvData->fetchGlobal('password_2'));
		$objPixariaUser->setReminderQuestion($objEnvData->fetchGlobal('password_rem_que'));
		$objPixariaUser->setReminderAnswer($objEnvData->fetchGlobal('password_rem_ans'));
		$objPixariaUser->setAccountStatus($objEnvData->fetchGlobal('account_status'));
		$objPixariaUser->setTelephone($objEnvData->fetchGlobal('telephone'));
		$objPixariaUser->setMobile($objEnvData->fetchGlobal('mobile'));
		$objPixariaUser->setFax($objEnvData->fetchGlobal('fax'));
		$objPixariaUser->setAddress1($objEnvData->fetchGlobal('addr1'));
		$objPixariaUser->setAddress2($objEnvData->fetchGlobal('addr2'));
		$objPixariaUser->setAddress3($objEnvData->fetchGlobal('addr3'));
		$objPixariaUser->setCity($objEnvData->fetchGlobal('city'));
		$objPixariaUser->setRegion($objEnvData->fetchGlobal('region'));
		$objPixariaUser->setCountry($objEnvData->fetchGlobal('country'));
		$objPixariaUser->setPostCode($objEnvData->fetchGlobal('postal_code'));
		$objPixariaUser->setBusinessType($objEnvData->fetchGlobal('other_business_type'));
		$objPixariaUser->setPosition($objEnvData->fetchGlobal('other_business_position'));
		$objPixariaUser->setInterest($objEnvData->fetchGlobal('other_image_interest'));
		$objPixariaUser->setPubFrequency($objEnvData->fetchGlobal('other_frequency'));
		$objPixariaUser->setPubCirculation($objEnvData->fetchGlobal('other_circulation'));
		$objPixariaUser->setTerritories($objEnvData->fetchGlobal('other_territories'));
		$objPixariaUser->setWebsite($objEnvData->fetchGlobal('other_website'));
		$objPixariaUser->setCompanyName($objEnvData->fetchGlobal('other_company_name'));
		$objPixariaUser->setMessage($objEnvData->fetchGlobal('other_message'));

		// Assign form data back to smarty
		$smarty->assign("userid",$objEnvData->fetchGlobal('userid'));
		$smarty->assign("is_admin",$objEnvData->fetchGlobal('is_admin'));
		$smarty->assign("is_editor",$objEnvData->fetchGlobal('is_editor'));
		$smarty->assign("formal_title",$objEnvData->fetchGlobal('formal_title'));
		$smarty->assign("other_title",$objEnvData->fetchGlobal('other_title'));
		$smarty->assign("first_name",$objEnvData->fetchGlobal('first_name'));
		$smarty->assign("middle_initial",$objEnvData->fetchGlobal('middle_initial'));
		$smarty->assign("family_name",$objEnvData->fetchGlobal('family_name'));
		$smarty->assign("email_address",$objEnvData->fetchGlobal('email_address'));
		$smarty->assign("account_status",$objEnvData->fetchGlobal('account_status'));
		$smarty->assign("telephone",$objEnvData->fetchGlobal('telephone'));
		$smarty->assign("mobile",$objEnvData->fetchGlobal('mobile'));
		$smarty->assign("fax",$objEnvData->fetchGlobal('fax'));
		$smarty->assign("addr1",$objEnvData->fetchGlobal('addr1'));
		$smarty->assign("addr2",$objEnvData->fetchGlobal('addr2'));
		$smarty->assign("addr3",$objEnvData->fetchGlobal('addr3'));
		$smarty->assign("city",$objEnvData->fetchGlobal('city'));
		$smarty->assign("region",$objEnvData->fetchGlobal('region'));
		$smarty->assign("country",$objEnvData->fetchGlobal('country'));
		$smarty->assign("postal_code",$objEnvData->fetchGlobal('postal_code'));
		$smarty->assign("password_1",$objEnvData->fetchGlobal('password_1'));
		$smarty->assign("password_2",$objEnvData->fetchGlobal('password_2'));
		$smarty->assign("password_rem_que",$objEnvData->fetchGlobal('password_rem_que'));
		$smarty->assign("password_rem_ans",$objEnvData->fetchGlobal('password_rem_ans'));
		$smarty->assign("other_company_name",$objEnvData->fetchGlobal('other_company_name'));
		$smarty->assign("other_business_type",$objEnvData->fetchGlobal('other_business_type'));
		$smarty->assign("other_business_position",$objEnvData->fetchGlobal('other_business_position'));
		$smarty->assign("other_image_interest",$objEnvData->fetchGlobal('other_image_interest'));
		$smarty->assign("other_frequency",$objEnvData->fetchGlobal('other_frequency'));
		$smarty->assign("other_circulation",$objEnvData->fetchGlobal('other_circulation'));
		$smarty->assign("other_territories",$objEnvData->fetchGlobal('other_territories'));
		$smarty->assign("other_website",$objEnvData->fetchGlobal('other_website'));

		// Edit the user's profile information
		// Apply data validation in mode = 'user' and type = 'edit'
		$success = $objPixariaUser->editUser('admin','edit',false);
		
		if ($success) { // The new user account is now complete

			// Define html page title
			$smarty->assign("page_title","Profile Saved");
			
			// Output html from template file
			$smarty->pixDisplay('admin.user/user.edit.02.tpl');
						
		} else { // The validation failed and the user needs to edit some bits

			$smarty->assign("profile_errors",$objPixariaUser->getProfileErrors());
			$smarty->assign("problem",true);
			
			// Assign Smarty data for required registration information
			$this->smartyAssignRegistrationFields();
			
			// Assign Smarty data for country drop down menu
			$this->smartyAssignCountryData();
			
			// Tell smarty object that there were errors
			$smarty->assign("problem",(bool)1);
			
			// Assign problem message to smarty object
			$smarty->assign("problem_output",$problem_output);
				
			// Define html page title
			$smarty->assign("page_title","Correct mistakes");
			
			// Output html from template file
			$smarty->pixDisplay('admin.user/user.edit.error.tpl');
		
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function showFormUserEdit () {
		
		global $cfg, $smarty, $ses, $lang, $objEnvData;
		
		// Get the userid
		$userid = $objEnvData->fetchGlobal('userid');
		
		// Get the list of system groups this user is a member of
		$result = $this->_dbl->sqlSelectRows("SELECT * FROM " . PIX_TABLE_SGME . " WHERE sys_userid = '$userid' ORDER by sys_group_id ASC");
		
		$is_admin	= (bool)FALSE;
		$is_editor	= (bool)FALSE;
		$is_photo	= (bool)FALSE;
		
		if (is_array($result)) {
			
			foreach ($result as $key => $value) {
				
				// Set whether or not this user is a an admin or image editor
				if ($value['sys_group_id']==1) { $is_admin		= (bool)TRUE; }
				if ($value['sys_group_id']==2) { $is_editor		= (bool)TRUE; }
				if ($value['sys_group_id']==3) { $is_photo		= (bool)TRUE; }
				if ($value['sys_group_id']==4) { $is_delete		= (bool)TRUE; }
				if ($value['sys_group_id']==5) { $is_download	= (bool)TRUE; }
			
			}
			
			$smarty->assign("is_admin",$is_admin);
			$smarty->assign("is_editor",$is_editor);
			$smarty->assign("is_photo",$is_photo);
			$smarty->assign("is_delete",$is_delete);
			$smarty->assign("is_download",$is_download);
			
		}
		
		
		// Get the list of groups this user is a member of
		$result = $this->_dbl->sqlSelectRows("SELECT * FROM " . PIX_TABLE_GRME . " WHERE userid = '$userid' ORDER by group_id ASC");
		
		if (is_array($result)) {
		
			// Clean the list of groups and sort into an array
			if (count($result) > 0 && is_array($result)) {
				foreach($result as $key => $value) {
					if ($result[$key]['group_id'] > 2) { $member_of[] = $result[$key]['group_id']; }
				}
			}
				
			
		}
		
		// Get the lists of group names and ids
		$group_list = groupListArray();
		
		if (is_array($member_of)) {
		
			// Check which groups are selected for this gallery
			foreach ($group_list[1] as $key => $value) {
			
				if (in_array($value,$member_of)) { $group_member_id[] = "$value"; } else { $group_member_id[] = ""; }
			
			}
		
		}
		
		// Get information about required registration details
		$result = $this->_dbl->sqlSelectRows("SELECT * FROM ".PIX_TABLE_USDA);
		if (is_array($result)) {
		
			//	Feed data into Smarty
			for ($i=0; $i<count($result); $i++) {
			
				$name = $result[$i]['name'];
				
				$smarty->assign("data_".$name,$result[$i]['value']);
			
			}
		
		}
		
		// Load the group list data into Smarty
		$smarty->assign("group_name",$group_list[0]);
		$smarty->assign("group_id",$group_list[1]);
		$smarty->assign("group_member_id",$group_member_id);
		
		require_once('class.PixariaUser.php');
		
		$objPixariaUser = new PixariaUser($userid);
		
		$email_address 				= $objPixariaUser->getEmail();
		$formal_title 				= $objPixariaUser->getSalutation();
		$other_title 				= $objPixariaUser->getOtherSalutation();
		$first_name 				= $objPixariaUser->getFirstName();
		$middle_initial 			= $objPixariaUser->getInitial();
		$family_name				= $objPixariaUser->getFamilyName();
		$password_rem_que			= $objPixariaUser->getReminderQuestion();
		$password_rem_ans			= $objPixariaUser->getReminderAnswer();
		$date_registered 			= $objPixariaUser->getDateRegistered();
		$date_edited 				= $objPixariaUser->getDateEdited();
		$date_expiration 			= $objPixariaUser->getDateExpires();
		$account_status 			= $objPixariaUser->getAccountStatus();
		$telephone 					= $objPixariaUser->getTelephone();
		$mobile 					= $objPixariaUser->getMobile();
		$fax 						= $objPixariaUser->getFax();
		$addr1 						= $objPixariaUser->getAddress1();
		$addr2 						= $objPixariaUser->getAddress2();
		$addr3 						= $objPixariaUser->getAddress3();
		$city 						= $objPixariaUser->getCity();
		$region 					= $objPixariaUser->getRegion();
		$country 					= $objPixariaUser->getCountry();
		$postal_code 				= $objPixariaUser->getPostCode();
		$other_business_type 		= $objPixariaUser->getBusinessType();
		$other_business_position 	= $objPixariaUser->getBusinessPosition();
		$other_image_interest 		= $objPixariaUser->getInterest();
		$other_frequency 			= $objPixariaUser->getPubFrequency();
		$other_circulation 			= $objPixariaUser->getPubCirculation();
		$other_territories 			= $objPixariaUser->getTerritories();
		$other_website 				= $objPixariaUser->getWebsite();
		$other_company_name 		= $objPixariaUser->getCompanyName();
		$other_message 				= $objPixariaUser->getMessage();
		
		// Assign user variables to smarty
		$smarty->assign("userid",$userid);
		$smarty->assign("formal_title",stripslashes($formal_title));
		$smarty->assign("other_title",stripslashes($other_title));
		$smarty->assign("first_name",stripslashes($first_name));
		$smarty->assign("middle_initial",stripslashes($middle_initial));
		$smarty->assign("family_name",stripslashes($family_name));
		$smarty->assign("email_address",stripslashes($email_address));
		$smarty->assign("telephone",stripslashes($telephone));
		$smarty->assign("mobile",stripslashes($mobile));
		$smarty->assign("fax",stripslashes($fax));
		$smarty->assign("addr1",stripslashes($addr1));
		$smarty->assign("addr2",stripslashes($addr2));
		$smarty->assign("addr3",stripslashes($addr3));
		$smarty->assign("city",stripslashes($city));
		$smarty->assign("region",stripslashes($region));
		$smarty->assign("country",stripslashes($country));
		$smarty->assign("postal_code",stripslashes($postal_code));
		$smarty->assign("password_rem_que",stripslashes($password_rem_que));
		$smarty->assign("password_rem_ans",stripslashes($password_rem_ans));
		$smarty->assign("date_registered",strtotime($date_registered));
		$smarty->assign("date_edited",strtotime($date_edited));
		$smarty->assign("account_status",$account_status);
		$smarty->assign("other_business_type",stripslashes($other_business_type));
		$smarty->assign("other_business_position",stripslashes($other_business_position));
		$smarty->assign("other_image_interest",stripslashes($other_image_interest));
		$smarty->assign("other_frequency",stripslashes($other_frequency));
		$smarty->assign("other_circulation",stripslashes($other_circulation));
		$smarty->assign("other_territories",stripslashes($other_territories));
		$smarty->assign("other_website",stripslashes($other_website));
		$smarty->assign("other_company_name",stripslashes($other_company_name));
		$smarty->assign("other_message",stripslashes($other_message));
		
		// Load the country class
		require_once ("class.PixariaCountry.php");
		
		// Initialise the country class
		$objCountry = new PixariaCountry();
		
		// Assign country data to Smarty
		$smarty->assign("iso_codes",$objCountry->getIsoCodes());
		$smarty->assign("printable_names",$objCountry->getPrintableNames());
		
		// Define html page title
		$smarty->assign("page_title","Edit Account Profile");
		
		// Output html from template file
		$smarty->pixDisplay('admin.user/user.edit.01.tpl');
	
	}

	/*
	*
	*	
	*
	*/
	function smartyAssignCountryData () {
	
		global $objEnvData, $cfg, $smarty;
	
		// Load the country class
		require_once ("class.PixariaCountry.php");
		
		// Initialise the country class
		$objCountry = new PixariaCountry();
		
		// Assign country data to Smarty
		$smarty->assign("iso_codes",$objCountry->getIsoCodes());
		$smarty->assign("printable_names",$objCountry->getPrintableNames());
		
	}
	
	/*
	*
	*	
	*
	*/
	function smartyAssignRegistrationFields () {
	
		global $objEnvData, $cfg, $smarty;
	
		// Get information about required registration details
		$result = $this->_dbl->sqlSelectRows("SELECT * FROM " . PIX_TABLE_USDA);
		
		if (is_array($result)) {
		
			//	Feed data into Smarty
			for ($i=0; $i<count($result); $i++) {
			
				$name = $result[$i]['name'];
				
				$smarty->assign("data_".$name,$result[$i]['value']);
			
			}
		
		}
		
	}
	
}

?>