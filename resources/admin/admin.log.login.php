<?php 

/*
*
*	Pixaria Gallery 1.0
*	Copyright 2002 - 2006 Jamie Longstaff
*
*	Script author:		Jamie Longstaff
*	Script version:		1.0	
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "logs";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Check that the user is a SuperUser
pix_authorise_user("administrator");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Pass the cmd variable through to Smarty (controls what bits we see)
$smarty->assign("cmd",aglobal(cmd));

switch (aglobal('cmd')) {
	
	case 1: // Empty the login log
		
		// Empty the log table
		@mysql_query("DELETE FROM ".$cfg['sys']['table_ulog']);
		
		// Update the last emptied date
		@mysql_query("UPDATE ".$cfg['sys']['table_ulog']." SET value = NOW() WHERE name = 'login_log_date'");
		
		// Generate a redirection URL 
		$meta_url	= $cfg['sys']['base_url'] . $cfg['fil']['admin_log_keywords'];
					
		// Show redirect page
		$smarty->pixWaiting($meta_url,"1");			
		
	break;
	
	default: // Show a list of the search keywords
	
		$method = aglobal('method');
		
		switch (aglobal('sort')) {
		
			case username:
			
				if ($method == "ASC" || !$method) {
				
					$sql_sort = "ORDER BY 'log_username' ASC";
					$smarty->assign("method","DESC");
				
				} else {
					
					$sql_sort = "ORDER BY 'log_username' DESC";
					$smarty->assign("method","ASC");
				
				}
			
			break;
			
			case ipaddr:
			
				if ($method == "ASC" || !$method) {
				
					$sql_sort = "ORDER BY 'log_ip' ASC";
					$smarty->assign("method","DESC");
				
				} else {
					
					$sql_sort = "ORDER BY 'log_ip' DESC";
					$smarty->assign("method","ASC");
				
				}
			
			break;
			
			case emailaddr:
			
				if ($method == "ASC" || !$method) {
				
					$sql_sort = "ORDER BY 'log_email' ASC";
					$smarty->assign("method","DESC");
				
				} else {
					
					$sql_sort = "ORDER BY 'log_email' DESC";
					$smarty->assign("method","ASC");
				
				}
			
			break;
			
			case hostname:
			
				if ($method == "ASC" || !$method) {
				
					$sql_sort = "ORDER BY 'log_host' ASC";
					$smarty->assign("method","DESC");
				
				} else {
					
					$sql_sort = "ORDER BY 'log_host' DESC";
					$smarty->assign("method","ASC");
				
				}
			
			break;
			
			case date:
			
				if ($method == "ASC" || !$method) {
				
					$sql_sort = "ORDER BY 'log_date' ASC";
					$smarty->assign("method","DESC");
				
				} else {
					
					$sql_sort = "ORDER BY 'log_date' DESC";
					$smarty->assign("method","ASC");
				
				}
			
			break;
			
			default:
			
				if ($method == "ASC") {
				
					$sql_sort = "ORDER BY log_date ASC";
					$smarty->assign("method","DESC");
				
				} else {
					
					$sql_sort = "ORDER BY log_date DESC";
					$smarty->assign("method","ASC");
				
				}
			
			break;
			
		}
		
		if (aglobal('show') == "ano") {
		
			$extra_sql_1 = " GROUP BY 'log_ip' ";
			
			$smarty->assign("show","anomalous");
		
		}
		
		if (aglobal('show') == "fail") {
		
			$extra_sql_2 = " WHERE log_success = '0' ";
			
			$smarty->assign("show","failed");
		
		} else {
		
			$extra_sql_2 = " WHERE log_success = '1' ";
			
		}
		
		$sql = "SELECT ".$cfg['sys']['table_ulog'].".*,
				
				CONCAT(".$cfg['sys']['table_user'].".family_name,', ',".$cfg['sys']['table_user'].".first_name) AS log_username
				
				FROM ".$cfg['sys']['table_ulog']."
				
				LEFT JOIN ".$cfg['sys']['table_user']." ON ".$cfg['sys']['table_ulog'].".log_userid = ".$cfg['sys']['table_user'].".userid
				
				$extra_sql_2
				
				$extra_sql_1
				
				$sql_sort
				
				LIMIT 0,50";
		
		$result = @sql_select_rows($sql);
		
		if (is_array($result)) {
		
			foreach ($result as $key => $value) {
			
				$log_username[]	= $value['log_username'];
				$log_userid[]	= $value['log_userid'];
				$log_email[]	= $value['log_email'];
				$log_ip[]		= $value['log_ip'];
				$log_date[]		= $value['log_date'];
				$log_host[]		= $value['log_host'];
				
			}
			
			// Tell Smarty there were entries to show
			$smarty->assign("login_result",(bool)TRUE);
			
		}
		
		if (aglobal('show') == "ano") {
			
			// Count the number of times each userid occurs in the result
			$log_number = array_count_values($log_userid);
			
			if (is_array($log_number)) {
			
				foreach ($log_userid as $key => $value) {
					
					if ($log_number[$value] > 4) {
					
						$log_warning[] = (bool)TRUE;
					
					} else {
					
						$log_warning[] = (bool)FALSE;
					
					}
					
				}
			
			}
			
			// Load the number of unique IP addresses used
			$smarty->assign("log_warning",$log_warning);
			
		}
		
		// Load the userid
		$smarty->assign("log_userid",$log_userid);
		
		// Load the user name
		$smarty->assign("log_username",$log_username);
		
		// Load the IP address
		$smarty->assign("log_ip",$log_ip);
		
		// Load the e-mail address
		$smarty->assign("log_email",$log_email);
		
		// Load the date
		$smarty->assign("log_date",$log_date);
		
		// Load the host name
		$smarty->assign("log_host",$log_host);
		
		// Set the page title
		$smarty->assign("page_title","User login log");
		
		// Display the admin status page
		smartyPixDisplay('admin.log/log.login.tpl');
		
	break;

}
?>