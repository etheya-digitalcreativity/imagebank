<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "logs";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Check that the user is a SuperUser
pix_authorise_user("administrator");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Instantiate the class object
$objAdminKeywords = new Admin_Keywords();


class Admin_Keywords {

	var $_dbl;
	
	/*
	*
	*	Class constructor
	*
	*/
	function Admin_Keywords () {
	
		global $objEnvData, $cfg;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		switch ($objEnvData->fetchGlobal('cmd')) {
		
			case "clearSearchLog":
				$this->clearSearchLog();
			break;
			
			case "showSearchLog": default:
				$this->showSearchLog();
			break;
		
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function showSearchLog () {
		
		global $smarty, $objEnvData, $cfg;
		
		// Pass the cmd variable through to Smarty (controls what bits we see)
		$smarty->assign("cmd",aglobal(cmd));
		
		// Get the sort method
		$method = $objEnvData->fetchGlobal('method');
		
		switch ($objEnvData->fetchGlobal('show')) {
			
			case exc:
			
				$sql_show = "WHERE type = 'NOT'";
				
			break;
			
			case inc:
			
				$sql_show = "WHERE type != 'NOT'";
				
			break;
			
			default:
			
				$sql_show = "";
				
			break;
		
		}
		
		switch ($objEnvData->fetchGlobal('sort')) {
		
			case keyword:
			
				if ($method == "ASC" || !$method) {
				
					$sql_sort = "ORDER BY keyword ASC";
					$smarty->assign("method","DESC");
				
				} else {
					
					$sql_sort = "ORDER BY keyword DESC";
					$smarty->assign("method","ASC");
				
				}
			
			break;
			
			case frequency:
			
				if ($method == "ASC" || !$method) {
				
					$sql_sort = "ORDER BY frequency ASC";
					$smarty->assign("method","DESC");
				
				} else {
					
					$sql_sort = "ORDER BY frequency DESC";
					$smarty->assign("method","ASC");
				
				}
			
			break;
			
			default:
			
				if ($method == "ASC") {
				
					$sql_sort = "ORDER BY frequency ASC";
					$smarty->assign("method","DESC");
				
				} else {
					
					$sql_sort = "ORDER BY frequency DESC";
					$smarty->assign("method","ASC");
				
				}
			
			break;
			
		}
		
		$sql = "SELECT *, COUNT(keyword) AS frequency FROM ".PIX_TABLE_SLOG." $sql_show GROUP BY keyword $sql_sort LIMIT 0,50";
		
		$result = $this->_dbl->sqlSelectRows($sql);
		
		if (is_array($result)) {
		
			foreach ($result as $key => $value) {
			
				$keyword[]	= $value['keyword'];
				$type[]		= $value['type'];
				$count[]	= $value['frequency'];
				
				if (eregi(" ",$value['keyword'])) {
					$escape[] = (bool)true;
				} else {
					$escape[] = (bool)false;
				}
				
			}
			
			// Tell Smarty there were entries to show
			$smarty->assign("keyword_result",(bool)TRUE);
			
		}
		
		// Load data into Smarty
		$smarty->assign("keyword",$keyword);
		$smarty->assign("type",$type);
		$smarty->assign("count",$count);
		$smarty->assign("escape",$escape);
		
		// Set the page title
		$smarty->assign("page_title","Search keyword log");
		
		// Display the admin status page
		smartyPixDisplay('admin.log/log.keywords.tpl');
	
	}
	
	/*
	*
	*	
	*
	*/
	function clearSearchLog () {
		
		global $cfg;
		
		// Empty the log table
		@mysql_query("DELETE FROM ".PIX_TABLE_SLOG);
		
		// Update the last emptied date
		@mysql_query("UPDATE ".PIX_TABLE_SETT." SET value = NOW() WHERE name = 'search_log_date'");
		
		$cfg['set']['search_log_date'] = date("Y-m-d h:i:s");
		
		// Show the search log page 
		$this->showSearchLog();		
		
	}

}

?>