<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "tools";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Check that the user is a administrator
pix_authorise_user("administrator");

new Divestock();

class Divestock {
	
	var $db;
	var $input;
	var $view;
	var $config;
	var $session;
	var $json;
	
	var $image_access_sql;
	var $gallery_access_sql;
	
	/*
	*
	*
	*
	*/
	function Divestock () {
	
		global 	$objEnvData,
				$ses,
				$cfg;

		// Load required files
		require_once ('class.Database.php');
		require_once (SYS_BASE_PATH . 'resources/classes/class.InputData.php');
		require_once (SYS_BASE_PATH . 'resources/classes/class.JSON.php');
		
		$this->db		= new Database();
		$this->input	= new InputData();
		$this->view 	= new Smarty_Pixaria();
		$this->json 	= new Pixaria_JSON();
		$this->config	= $cfg;
		$this->session 	= $ses;
		
		// Define the internal properties for image access control
		$this->image_access_sql		= " AND imgs.image_permissions = '10' ";
		$this->gallery_access_sql	= " AND gall.gallery_permissions = '10' ";

		switch ($this->input->name('cmd')) {
			
			case 'countTotalImages':
				$this->countTotalImages();
			break;
			
			case 'getProfileData':
				$this->getProfileData();
			break;
			
			case 'applySettings':
				$this->applySettings();
			break;
			
			default:
				$this->index();
			break;
			
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function index () {
		
		list ($divestock_gallery_id, $published_gallery_id) = $this->db->rowsAsColumns("SELECT * FROM ".PIX_TABLE_DGAL."");
		
		$this->view->assign("published_gallery_id",$published_gallery_id);
		
		// Load Gallery Core class
		require_once (SYS_BASE_PATH . 'resources/classes/class.Core.Gallery.php');
		
		$GalleryCore = new GalleryCore();
		
		// Load an array of nested categories
		list ($menu_gallery_title, $menu_gallery_id) = $GalleryCore->galleryListingArray();
		
		// Load the category list data into Smarty
		$this->view->assign("menu_gallery_title",$menu_gallery_title);
		$this->view->assign("menu_gallery_id",$menu_gallery_id);
		
		// Load the number of images in the selection
		$this->view->assign('image_count',$this->getTotalImages($published_gallery_id));
		
		// Define the navigation section for the admin toolbar
		$this->view->assign("toolbar","status");
		
		// Set the page title
		$this->view->assign("page_title","Divestock Publishing Manager");
		
		// Display the html header
		$this->view->display('divestock/index.html');
		
		exit;
				
	}
	
	/*
	*
	*
	*
	*/
	function getTotalImages ($divestock_galleries) {
		
		if (is_array($divestock_galleries) && count($divestock_galleries) > 0) {
		
			$constraint = "gord.gallery_id = '" . implode("' OR gord.gallery_id = '",$divestock_galleries) . "'";
			
			$sql = "SELECT gord.image_id
					
					FROM ".PIX_TABLE_GORD." gord
					
					LEFT JOIN ".PIX_TABLE_IMGS." imgs ON gord.image_id = imgs.image_id
					
					LEFT JOIN ".PIX_TABLE_GALL." gall ON gord.gallery_id = gall.gallery_id
					
					WHERE ($constraint)
					
					".$this->gallery_access_sql."
					
					".$this->image_access_sql."";
			
			return $this->db->countRows($sql);
		
		} else {
		
			return '0';
		
		}
		
	}
	
	/*
	*
	*
	*
	*/
	function countTotalImages () {
		
		$divestock_galleries = $this->input->name('divestock_galleries');
		
		if (is_array($divestock_galleries) && count($divestock_galleries) > 0) {
		
			array_walk($divestock_galleries,array($this->db,"escape"));
			
			$constraint = "gord.gallery_id = '" . implode("' OR gord.gallery_id = '",$divestock_galleries) . "'";
			
			$sql = "SELECT gord.image_id
					
					FROM ".PIX_TABLE_GORD." gord
					
					LEFT JOIN ".PIX_TABLE_IMGS." imgs ON gord.image_id = imgs.image_id
					
					LEFT JOIN ".PIX_TABLE_GALL." gall ON gord.gallery_id = gall.gallery_id
					
					WHERE ($constraint)
					
					".$this->gallery_access_sql."
					
					".$this->image_access_sql."";
			
			$result = $this->db->countRows($sql);
			
		} else {
			
			$result = '0';
		
		}
		
		header("Content-type: application/json");
		
		print "{image_count:$result}";
		
		exit;
		
	}
	
	/*
	*
	*	
	*
	*/
	function getProfileData () {
		
		global $cfg;
		
		$json = $this->getRemoteFile(DIVESTOCK_DOMAIN,'POST','/api/profiledata','api_key='.$cfg['set']['divestock_api_key'].'&base_url='.BASE_URL);
		
		$data = $this->json->json_decode($json);
		
		$api_key				= $data['api_key'];
		$image_allowance 		= $data['image_allowance'];
		$image_allowance_used 	= $data['image_allowance_used'];
		
		$this->view->assign('api_key',$api_key);
		$this->view->assign('image_allowance',$image_allowance);
		$this->view->assign('image_allowance_used',$image_allowance_used);
		
		$this->view->display('divestock/profiledata.html');
		
	}
	
	/*
	*
	*	
	*
	*/
	function applySettings () {
		
		$divestock_galleries = $this->input->post('divestock_galleries');
		
		$this->db->query("DELETE FROM " . PIX_TABLE_DGAL);
		
		while ( list($key, $gallery_id) = @each($divestock_galleries) ) {
		
			$sql = $this->db->query("INSERT INTO ".PIX_TABLE_DGAL." VALUES ('0', '".$this->db->escape($gallery_id)."')");
		
		}
		
		//header("Content-type: application/json");
		
		print "{success:true}";
	
		exit;
		
	}
		
	/*
	*
	*	GET A REMOTE FILE
	*	
	*/
	function getRemoteFile ($host, $method, $path, $data) {
	
		$method = strtoupper($method);        
		
		if ($method == "GET") {
			$path.= '?'.$data;
		}    
		
		$filePointer = fsockopen($host, 80, $errorNumber, $errorString, 10);
		
		if (!$filePointer) {
			logEvent('debug', 'Failed opening http socket connection: '.$errorString.' ('.$errorNumber.')<br/>\n');
			return false;
		}
		
		$requestHeader = $method." ".$path."  HTTP/1.1\r\n";
		$requestHeader.= "Host: ".$host."\r\n";
		$requestHeader.= "User-Agent:      Mozilla/6.0 (Pixaria ".$cfg['set']['software_version'].")\r\n";
		$requestHeader.= "Content-Type: application/x-www-form-urlencoded\r\n";
		
		if ($method == "POST")
		{
		$requestHeader.= "Content-Length: ".strlen($data)."\r\n";
		}
		
		$requestHeader.= "Connection: close\r\n\r\n";
		
		if ($method == "POST")
		{
		$requestHeader.= $data;
		}            
		
		fwrite($filePointer, $requestHeader);
		
		$responseHeader = '';
		$responseContent = '';
		
		do 
		{
		$responseHeader.= fread($filePointer, 1); 
		}
		while (!preg_match('/\\r\\n\\r\\n$/', $responseHeader));
		
		
		if (!strstr($responseHeader, "Transfer-Encoding: chunked"))
		{
		while (!feof($filePointer))
		{
		   $responseContent.= fgets($filePointer, 128);
		}
		}
		else 
		{
		
		while ($chunk_length = hexdec(fgets($filePointer))) 
		{
		   $responseContentChunk = '';
		
		  
		   $read_length = 0;
		   
		   while ($read_length < $chunk_length) 
		   {
			   $responseContentChunk .= fread($filePointer, $chunk_length - $read_length);
			   $read_length = strlen($responseContentChunk);
		   }
		
		   $responseContent.= $responseContentChunk;
		   
		   fgets($filePointer);
		   
		}
		
		}
		
		return chop($responseContent);
		
	}
	
}

?>