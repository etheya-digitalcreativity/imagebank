<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "images";

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Tell Smarty this is a Pixaria 3.0 admin page
$smarty->assign('pixaria_version','3');

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Check that the user is a SuperUser
pix_authorise_user("administrator");

$library = new library();

class library {

	/*
	*
	*	
	*
	*/
	function library () {
	
		global $objEnvData, $smarty, $ses, $cfg;

		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->db		= new Database();
		$this->view		=& $smarty;
		$this->input	=& $objEnvData;
		
		switch ($this->input->name('cmd')) {
						
			case 'incoming':
				$this->incoming();
			break;
		
			default:
				$this->index();
			break;
		
		}
		
	}

}

?>