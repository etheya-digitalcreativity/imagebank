<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "images";

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Tell Smarty this is a Pixaria 3.0 admin page
$smarty->assign('pixaria_version','3');

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Check that the user is a SuperUser
pix_authorise_user("administrator");


$Import = new Import();

class Import {
	
	var $directory_regexp = '/[^a-z-A-Z-0-9-_]+/i';
	
	/*
	*
	*	
	*
	*/
	function Import () {
	
		global $objEnvData, $ses, $cfg;

		// Load the database class
		require_once ('class.Database.php');
		require_once (SYS_BASE_PATH . 'resources/classes/class.InputData.php');
		
		// Create the database object
		$this->db 		= new Database();
		$this->view		= new Smarty_Pixaria();
		$this->input	= new InputData();
		$this->session 	= $ses;
		
		switch ($this->input->name('cmd')) {
						
			case 'galleryColumnView':
				$this->galleryColumnView();
			break;
		
			case 'getImageList':
				$this->getImageList();
			break;
		
			case 'updateImportSettings':
				$this->updateImportSettings();
			break;
		
			case 'importImages':
				$this->importImages();
			break;
		
			default:
				$this->index();
			break;
		
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function index () {	
		
		// Load the filesystem manipulation library
		require_once (SYS_BASE_PATH . 'resources/classes/class.Core.File.php');
		
		// Setup the filesystem manipulation object
		$objFileCore = new FileCore();
		
		/*
		*	List directories in the incoming directory
		*/
		$handle = @opendir(SYS_BASE_INCOMING);
		
		if($handle != FALSE) {
			
			$directory_name[]	= 'resources/incoming/';
			$directory_path[]	= '0';
			
			while (false!==($file = readdir($handle))) {
				
				if (is_dir(SYS_BASE_INCOMING . $file) && substr($file,0,1) != "." && !preg_match($this->directory_regexp,$file)) {
					
					if (!$objFileCore->isEmptyDirectory(SYS_BASE_INCOMING . $file . '/' . LARGE_COMPING_DIR) || !$objFileCore->isEmptyDirectory(SYS_BASE_INCOMING . $file . '/original')) {
					
						$directory_name[]	= 'resources/incoming/'.$file;
						$directory_path[]	= $file;
					
					}
					
				}
				
			}
		
		}
		
		@closedir($handle);
		
		/*
		*	List directories in the library directory
		*/
		$handle = @opendir(SYS_BASE_LIBRARY);
		
		if($handle != FALSE) {
			
			while (false!==($file = readdir($handle))) {
			
				if (is_dir(SYS_BASE_LIBRARY . $file) && substr($file,0,1) != "." && !preg_match($this->directory_regexp,$file)) {
				
					$lib_directory[]	= $file;
					
				}
				
			}
		
		}
		
		@closedir($handle);
		
		// Load an array of nested galleries
		$gallery_list = galleryListingArray();
		
		// Load the gallery list data into Smarty
		$this->view->assign("menu_gallery_title",$gallery_list[0]);
		$this->view->assign("menu_gallery_id",$gallery_list[1]);
	
		// Load data for gallery column view
		$this->galleryColumnView();
		
		// Load the PixariaGroupImage class so we can extend it
		require_once ('class.PixariaGroupImage.php');
		
		// Initialise group-image linking object for this image
		$objGroup = new PixariaGroupImage();
		
		// Set properties for groups
		$this->view->assign("group_title",$objGroup->getGroupListTitles());
		$this->view->assign("group_id",$objGroup->getGroupListIds());

		// Get information about available products
		require_once('class.PixariaProduct.php');
		
		$objProducts = new PixariaProduct();
		
		$product_data = $objProducts->getProducts();
		
		$this->view->assign("product_id",$product_data['prod_id']);
		$this->view->assign("product_name",$product_data['prod_name']);

		// Get a list of all the photographers in the system
		list ( $user_id, $user_name ) = photographerListArray();
		
		// Load the photographer data
		$this->view->assign("user_id",$user_id);
		$this->view->assign("user_name",$user_name);
		
		// Get the total number of galleries
		list ($total_galleries) = $this->db->count("SELECT COUNT(gallery_id) FROM ".PIX_TABLE_GALL."");
		$this->view->assign("total_galleries",$total_galleries);

		/*
		*	Input directory information into Smarty
		*/
		$this->view->assign("directory_name",$directory_name);
		$this->view->assign("directory_path",$directory_path);
		$this->view->assign("lib_directory",$lib_directory);
		$this->view->assign("default_image_list",$this->getImageList('return'));
		
		$this->view->assign("page_title","Import");
		
		$this->view->display('import/index.html');
		
	}
	
	/*
	*
	*
	*
	*/
	function importImages () {
	
		$incoming_directory	= $this->input->post('incoming_directory');
		$library_directory 	= $this->input->post('library_directory');
		$image				= $this->input->post('image');
		
		// Directory names can only contain alphanumeric chars, underscore and hyphen
		if (preg_match($this->directory_regexp,$library_directory) || $library_directory == '') {
			
			header ('Content-type: application/json');
			print "{error:true, error_code:'Invalid directory name'}";
			exit;
			
		}
		
		$uploads_dir = SYS_BASE_LIBRARY . '/' . $library_directory;
		
		if ($incoming_directory != '0') {
		
			$directory_base 			= '/' . $incoming_directory . '/';
			$directory_original 		= '/' . $incoming_directory . '/original/';
			$directory_comping_large	= '/' . $incoming_directory . '/' . LARGE_COMPING_DIR . '/';
			$directory_comping_small	= '/' . $incoming_directory . '/' . SMALL_COMPING_DIR . '/';
			$directory_thumb_large		= '/' . $incoming_directory . '/160x160/';
			$directory_thumb_small		= '/' . $incoming_directory . '/80x80/';
			$directory_icon				= '/' . $incoming_directory . '/32x32/';
		
		} else {
			
			$directory_original = '/';
		
		}
		
		if (file_exists(SYS_BASE_INCOMING . $directory_original . $image)) {
		
			$file_name		= $image;
			$file_path 		= SYS_BASE_INCOMING . $directory_original . $image;
			$file_original	= true;
			
		} elseif (file_exists(SYS_BASE_INCOMING . $directory_comping_large . $image)) {
			
			$file_name 		= $image;
			$file_path 		= SYS_BASE_INCOMING . $directory_comping_large . $image;
			$file_original	= false;
			
		} else {
			
			header ('Content-type: application/json');
			print "{error:true, error_code:'Image file cannot be found.'}";
			exit;
		
		}

		// Load the image manipulation library
		require_once (SYS_BASE_PATH . 'resources/classes/class.Core.Image.php');
		
		// Setup the image manipulation object
		$objImageCore = new ImageCore($file_path);
		
		if (!$objImageCore->isInit()) {
			print "{error:true, error_code:'Could not initialise GD or ImageMagick.'}";
			exit;
		}
		
		// Load the filesystem manipulation library
		require_once (SYS_BASE_PATH . 'resources/classes/class.Core.File.php');
		
		// Setup the filesystem manipulation object
		$objFileCore = new FileCore();
		
		// Load the Pixaria library manipulation library
		require_once (SYS_BASE_PATH . 'resources/classes/class.Core.Library.php');
		
		// Setup the Pixaria library manipulation object
		$objLibraryCore = new LibraryCore();
		
		// Load the Pixaria IPTC metadata library
		require_once (SYS_BASE_PATH . 'resources/classes/class.Core.IPTC.php');
		
		// Setup the Pixaria IPTC object
		$objIPTCCore = new IPTCCore($file_path);
		
		// Check that an image with this filename doesn't already exist
		if ($image_id = $objLibraryCore->checkImageExists('image_filename',$file_name)) {
			
			// Load the Pixaria Image library
			require_once (SYS_BASE_PATH . 'resources/includes/class.PixariaImage.php');
			
			$objImage = new PixariaImage($image_id);
			
			$icon_path 		= $objImage->getImageIconEncoded();
			$comp_path 		= $objImage->getImageCompEncoded();
			$icon_width		= $objImage->getImageIconWidth();
			$icon_height	= $objImage->getImageIconHeight();
			$comp_width		= $objImage->getImageCompingWidth();
			$comp_height	= $objImage->getImageCompingHeight();
			
			header ('Content-type: application/json');
			print "{name:'$name',
					iconWidth:'".$icon_width."',
					iconHeight:'".$icon_height."',
					compWidth:'".$comp_width."',
					compHeight:'".$comp_height."',
					id:'".$image_id."',
					compPath:'".$comp_path."',
					iconPath:'".$icon_path."',
					error:true,
					error_code:'An image with the same file name already exists in your library.'}";
			exit;
			
		}
		
		// Create upload directory
		$objFileCore->createDirectory("$uploads_dir");
		
		//  Create original image directory
		$objFileCore->createDirectory("$uploads_dir/original");
		
		//  Create large comping image directory
		$objFileCore->createDirectory("$uploads_dir/" . LARGE_COMPING_DIR);
		
		//  Create small comping image directory
		$objFileCore->createDirectory("$uploads_dir/" . SMALL_COMPING_DIR);
		
		//  Create large thumbnail image directory
		$objFileCore->createDirectory("$uploads_dir/160x160");
		
		//  Create small thumbnail image directory
		$objFileCore->createDirectory("$uploads_dir/80x80");
		
		//  Create icon thumbnail image directory
		$objFileCore->createDirectory("$uploads_dir/32x32");
		
		/*
		*	If the file is a valid image
		*/
		if (list($w, $h) = @getimagesize($file_path)) {
			
			/*
			*	Move the original image to the library
			*/
			if ($file_original) {
				
				$rename = @rename(SYS_BASE_INCOMING . $directory_original . $file_name, $uploads_dir . '/original/' . $file_name);
			
			} else {
				
				$rename = @rename(SYS_BASE_INCOMING . $directory_comping_large . $file_name, $uploads_dir . '/' . LARGE_COMPING_DIR . '/' . $file_name);
			
			}
			
			/*
			*	Move large comping image or generate it if not present
			*/
			if ($file_original && @getimagesize(SYS_BASE_INCOMING . $directory_comping_large . $file_name)) {
			
				@rename(SYS_BASE_INCOMING . $directory_comping_large . $file_name, $uploads_dir . '/' . LARGE_COMPING_DIR . '/' . $file_name);
				
			} else {
				
				// Resize to 630 x 630 if larger than the desginated comp image size
				if ($w > LARGE_COMPING_SIZE || $h > LARGE_COMPING_SIZE) {
					$objImageCore->resize(LARGE_COMPING_SIZE,LARGE_COMPING_SIZE,'keep_aspect');
				}
				
				$objImageCore->save("$uploads_dir/".LARGE_COMPING_DIR."/$file_name");
				$objFileCore->chmod("$uploads_dir/".LARGE_COMPING_DIR."/$file_name",0777);
	
			}
			
			/*
			*	Move small comping image or generate it if not present
			*/
			if (@getimagesize(SYS_BASE_INCOMING . $directory_comping_small . $file_name)) {
			
				@rename(SYS_BASE_INCOMING . $directory_comping_small . $file_name, $uploads_dir . '/' .SMALL_COMPING_DIR . '/' . $file_name);
				
			} else {
			
				// Resize to 320 x 320
				$objImageCore->resize(SMALL_COMPING_SIZE,SMALL_COMPING_SIZE,'keep_aspect');
				$objImageCore->save("$uploads_dir/".SMALL_COMPING_DIR."/$file_name");
				$objFileCore->chmod("$uploads_dir/".SMALL_COMPING_DIR."/$file_name",0777);
				
			}
			
			/*
			*	Move large thumbnail image or generate it if not present
			*/
			if (@getimagesize(SYS_BASE_INCOMING . $directory_thumb_large . $file_name)) {
			
				@rename(SYS_BASE_INCOMING . $directory_thumb_large . $file_name, $uploads_dir . '/160x160/' . $file_name);
				
			} else {
			
				// Resize to 160 x 160
				$objImageCore->resize(160,160,'keep_aspect');
				$objImageCore->save("$uploads_dir/160x160/$file_name");
				$objFileCore->chmod("$uploads_dir/160x160/$file_name",0777);
				
			}
			
			/*
			*	Move large thumbnail image or generate it if not present
			*/
			if (@getimagesize(SYS_BASE_INCOMING . $directory_thumb_small . $file_name)) {
			
				@rename(SYS_BASE_INCOMING . $directory_thumb_small . $file_name, $uploads_dir . '/80x80/' . $file_name);
				
			} else {
			
				// Resize to 80 x 80
				$objImageCore->resize(80,80,'keep_aspect');
				$objImageCore->save("$uploads_dir/80x80/$file_name");
				$objFileCore->chmod("$uploads_dir/80x80/$file_name",0777);
				
			}
			
			/*
			*	Move icon image or generate it if not present
			*/
			if (@getimagesize(SYS_BASE_INCOMING . $directory_icon . $file_name)) {
			
				@rename(SYS_BASE_INCOMING . $directory_icon . $file_name, $uploads_dir . '/32x32/' . $file_name);
				
			} else {
			
				// Resize to 32 x 32
				$objImageCore->resize(32,32,'keep_aspect');
				$objImageCore->save("$uploads_dir/32x32/$file_name");
				$objFileCore->chmod("$uploads_dir/32x32/$file_name",0777);
	
			}
		
			$objImageCore->clean();
			
			/*
			*	Clean up directories to remove empty ones
			*/
			if ($objFileCore->isEmptyDirectory(SYS_BASE_INCOMING . $directory_original)) {
				$objFileCore->removeDirectory(SYS_BASE_INCOMING . $directory_original);
			}
			
			if ($objFileCore->isEmptyDirectory(SYS_BASE_INCOMING . $directory_comping_large)) {
				$objFileCore->removeDirectory(SYS_BASE_INCOMING . $directory_comping_large);
			}
			
			if ($objFileCore->isEmptyDirectory(SYS_BASE_INCOMING . $directory_comping_small)) {
				$objFileCore->removeDirectory(SYS_BASE_INCOMING . $directory_comping_small);
			}
			
			if ($objFileCore->isEmptyDirectory(SYS_BASE_INCOMING . $directory_thumb_large)) {
				$objFileCore->removeDirectory(SYS_BASE_INCOMING . $directory_thumb_large);
			}
			
			if ($objFileCore->isEmptyDirectory(SYS_BASE_INCOMING . $directory_thumb_small)) {
				$objFileCore->removeDirectory(SYS_BASE_INCOMING . $directory_thumb_small);
			}
			
			if ($objFileCore->isEmptyDirectory(SYS_BASE_INCOMING . $directory_icon)) {
				$objFileCore->removeDirectory(SYS_BASE_INCOMING . $directory_icon);
			}
						
			if (!is_dir(SYS_BASE_INCOMING . $directory_original) &&
				!is_dir(SYS_BASE_INCOMING . $directory_comping_large) &&
				!is_dir(SYS_BASE_INCOMING . $directory_comping_small) &&
				!is_dir(SYS_BASE_INCOMING . $directory_thumb_large) &&
				!is_dir(SYS_BASE_INCOMING . $directory_thumb_small) &&
				!is_dir(SYS_BASE_INCOMING . $directory_icon) &&
				$directory_base != 'jclient' &&
				realpath(SYS_BASE_INCOMING . $directory_base) != realpath(SYS_BASE_INCOMING)) {
				
				$objFileCore->removeDirectory(SYS_BASE_INCOMING . $directory_base);
				
			}
			
			$icon = @getimagesize("$uploads_dir/32x32/$file_name");
			$comp = @getimagesize("$uploads_dir/".LARGE_COMPING_DIR."/$file_name");
			
			$comp_path = base64_encode("$uploads_dir/".LARGE_COMPING_DIR."/$file_name");
			$icon_path = base64_encode("$uploads_dir/32x32/$file_name");
			
			$image_id = $this->addImageToLibrary($library_directory, $file_name);
			
			header ('Content-type: application/json');
			print "{name:'$name', iconWidth:'".$icon[0]."', iconHeight:'".$icon[1]."', compWidth:'".$comp[0]."', compHeight:'".$comp[1]."', imageId:'".$image_id."', compPath:'".$comp_path."', iconPath:'".$icon_path."'}";
			exit;			
			
		} else {
			
			header ('Content-type: application/json');
			print "{error:true, error_code:'Not a valid JPEG image file.'}";
			exit;
			
		}
		
	}
	
	/*
	*
	*
	*
	*/
	function getImageList ($mode='') {
		
		$directory = $this->input->name('directory');
		
		$this->view->assign('import_directory',$directory);
		
		$image_file_name = array();
		$image_file_size = array();
		$image_file_width = array();
		$image_file_height = array();
		
		if ($directory == '0') {
			
			$handle = @opendir(SYS_BASE_INCOMING);

			if ($handle != FALSE) {
				
				while (false!==($file = readdir($handle))) {
				
					if (!is_dir(SYS_BASE_INCOMING . $file) && substr($file,0,1) != ".") {
					
						$image_file_name[]	= $file;
						$image_file_size[]	= $this->convertKiloBytes(@filesize(SYS_BASE_INCOMING . $file) / 1024);
						
						list (
							
							$image_file_width[],
							$image_file_height[]
						
						) = @getimagesize(SYS_BASE_INCOMING . $file);

					}
					
				}
			
			}
			
			@closedir($handle);
		
		} elseif (is_string($directory)) {
			
			if (is_dir(SYS_BASE_INCOMING . $directory)) {
				
				$directory = SYS_BASE_INCOMING . $directory;
				
				// Load array of original images
				if (is_dir($directory . '/original/')) {
					
					$directory_original = $directory . '/original/';
					
					$handle = @opendir($directory_original);
		
					if ($handle != FALSE) {
						
						while (false!==($file = readdir($handle))) {
						
							if (!is_dir($directory_original . $file) && substr($file,0,1) != ".") {
							
								$image_file_name[]	= $file;
								$image_file_size[]	= $this->convertKiloBytes(@filesize($directory_original . $file) / 1024);
								
								list (
									
									$image_file_width[],
									$image_file_height[]
								
								) = @getimagesize($directory_original . $file);
								
							}
							
						}
					
					}
					
					@closedir($handle);
				
				}
				
				// Load array of comping images
				if (is_dir($directory . '/' . COMPING_DIR . '/')) {
				
					$directory_comping = $directory . '/' . COMPING_DIR . '/';
					
					$handle = @opendir($directory_comping);
		
					if ($handle != FALSE) {
						
						while (false!==($file = readdir($handle))) {
						
							if (!is_dir($directory_comping . $file) && substr($file,0,1) != "." && !in_array($file, $image_file_name)) {
							
								$image_file_name[]	= $file;
								$image_file_size[]	= $this->convertKiloBytes(@filesize($directory_comping . $file) / 1024);

								list (
									
									$image_file_width[],
									$image_file_height[]
								
								) = @getimagesize($directory_comping . $file);
								
							}
							
						}
					
					}
					
					@closedir($handle);
				
				}
				
			}
		
		}
		
		$this->view->assign('image_file_name',$image_file_name);
		$this->view->assign('image_file_size',$image_file_size);
		$this->view->assign('image_file_width',$image_file_width);
		$this->view->assign('image_file_height',$image_file_height);
		
		if ($mode == 'return') {
			
			return $this->view->fetch('import/images.html');
			
		} else {
			
			$this->view->display('import/images.html');
		
		}
	
	}
	
	/*
	*
	*	CONVERT BYTES INTO A MEANINGFUL UNIT
	*
	*/
	function convertKiloBytes ($kilobytes) {
		
		$bytes 		= $kilobytes * 1024;
		$megabytes 	= $kilobytes / 1024;
		
		if ($megabytes < 1) {
			
			if ($kilobytes < 1) {
			
				return $bytes . " bytes";
			
			} else {
			
				return round($kilobytes,0) . " KB";
			
			}
		
		} else {
		
			return round($megabytes,1) . " MB";
		
		}
	
	}
	
	/*
	*
	*	UPDATE THE ACTIVE USER'S IMAGE IMPORT SETTINGS
	*
	*/
	function updateImportSettings () {
		
		$hour 	= $this->input->post('time_Hour');
		$minute	= $this->input->post('time_Minute');
		$second = $this->input->post('time_Second');
		$year 	= $this->input->post('date_Year');
		$month 	= $this->input->post('date_Month');
		$day	= $this->input->post('date_Day');
		
		$data['image_active'] 			= $this->input->post('image_active');
		$data['image_title'] 			= $this->input->post('image_title');
		$data['image_caption'] 			= $this->input->post('image_caption');
		$data['image_keywords'] 		= implode(', ',$this->input->post('image_keywords'));
		$data['image_userid'] 			= $this->input->post('image_userid');
		$data['image_copyright'] 		= $this->input->post('image_copyright');
		$data['image_date'] 			= "$year-$month-$day $hour:$minute:$second";
		$data['image_sale'] 			= $this->input->post('image_sale');
		$data['image_price'] 			= $this->input->post('image_price');
		$data['image_product_link'] 	= $this->input->post('image_product_link');
		$data['image_products'] 		= $this->input->post('image_products');
		$data['image_rights_type'] 		= $this->input->post('image_rights_type');
		$data['image_rights_text'] 		= $this->input->post('image_rights_text');
		$data['image_model_release'] 	= $this->input->post('image_model_release');
		$data['image_property_release'] = $this->input->post('image_property_release');
		$data['image_permissions'] 		= $this->input->post('image_permissions');
		$data['image_viewers'] 			= $this->input->post('image_viewers');
		$data['image_extra_01'] 		= $this->input->post('image_extra_01');
		$data['image_extra_02'] 		= $this->input->post('image_extra_02');
		$data['image_extra_03'] 		= $this->input->post('image_extra_03');
		$data['image_extra_04'] 		= $this->input->post('image_extra_04');
		$data['image_extra_05'] 		= $this->input->post('image_extra_05');
		
		foreach ($data as $key => $value) {
			
			if (is_array($value) && count($value) > 0) {
				
				$insert_val = base64_encode(serialize($value));
				
			} else {
				
				$insert_val = $value;
				
			}
			
			list (
				
				$preference_id
			
			) = $this->db->row("SELECT preference_id FROM ".PIX_TABLE_PREF." WHERE name = 'import_".$this->db->escape($key)."'");
			
			$this->db->query("DELETE FROM ".PIX_TABLE_UPRF." WHERE preference_id = '".$preference_id."' AND user_id = '".$this->session['psg_userid']."'");
			
			$this->db->query("INSERT INTO ".PIX_TABLE_UPRF." VALUES ('0', '".$preference_id."', '".$this->session['psg_userid']."', '".$insert_val."')");
			
		}
		
	}
	
	/*
	*
	*	ADD AN IMAGE TO THE LIBRARY - RETURNS (int) $image_id
	*
	*/
	function addImageToLibrary ($image_path, $file_name) {
		
		global $ses;
		
		$original_file = SYS_BASE_PATH . 'resources/library/' . $image_path . '/original/' . $file_name;
		
		// Load the metadata libraries
		require_once (SYS_BASE_PATH . 'resources/classes/class.Core.IPTC.php');
		require_once (SYS_BASE_PATH . 'resources/classes/class.Core.EXIF.php');
		require_once (SYS_BASE_PATH . 'resources/classes/class.Core.XMP.php');
		require_once (SYS_BASE_PATH . 'resources/classes/class.Core.File.php');
		require_once (SYS_BASE_PATH . 'resources/includes/class.PixariaImage.php');
			
		// Setup the metadata objects
		$objIPTCCore = new IPTCCore($original_file);
		$objEXIFCore = new EXIFCore($original_file);
		$objXMPCore  = new XMPCore($original_file);
		$objFileCore = new FileCore();
		
		
		// Get IPTC metadata
		$title		= $objIPTCCore->get('005'); // Same as headline
		$caption	= $objIPTCCore->get('120');
		$copyright	= $objIPTCCore->get('116');
		$headline	= $objIPTCCore->get('105');
		$keywords	= $objIPTCCore->get('025');
		$creator	= $objIPTCCore->get('080'); // Same as byline
		$jobtitle	= $objIPTCCore->get('085');
		$city		= $objIPTCCore->get('090');
		$country	= $objIPTCCore->get('101');
		$credit		= $objIPTCCore->get('110');
		$source		= $objIPTCCore->get('115');
		$object		= $objIPTCCore->get('005');
		$byline		= $objIPTCCore->get('080');
		
		// Get EXIF geo tag data
		list (
			$latitude,
			$longitude
		) = $objEXIFCore->getGeoTagCoordinates();
		
		
		// Get date and time from EXIF and IPTC
		$exif_date_time = $objEXIFCore->getDateTime();
		$iptc_date_time = $objIPTCCore->getDateTime();
		$file_date_time = $objFileCore->getDateTimeModified($original_file);
		
		// Get image dimensions
		list($icon_w, $icon_h) = $this->getImageSize($file_name, $image_path, '32x32');
		list($smal_w, $smal_h) = $this->getImageSize($file_name, $image_path, '80x80');
		list($larg_w, $larg_h) = $this->getImageSize($file_name, $image_path, '160x160');
		list($scom_w, $scom_h) = $this->getImageSize($file_name, $image_path, SMALL_COMPING_DIR);
		list($comp_w, $comp_h) = $this->getImageSize($file_name, $image_path, LARGE_COMPING_DIR);
		list($orig_w, $orig_h) = $this->getImageSize($file_name, $image_path, 'original');
		
		if (!file_exists($original_file)) {
			$orig_w = $comp_w;
			$orig_h = $comp_h;
		}
		
		// Create the Pixaria image class
		$objImage = new PixariaImage();
		
		// Set the image date
		if ($iptc_date_time) {
			$objImage->setImageMySQLDate($iptc_date_time);
		} elseif ($exif_date_time) {
			$objImage->setImageMySQLDate($exif_date_time);
		} else {
			$objImage->setImageMySQLDate($this->session['pref_import_image_date']);
		}
		
		// Set the image owner
		if (trim($this->session['pref_import_image_owner']) != '') {
			$image_owner = $this->session['pref_import_image_owner'];
		} else {
			$image_owner = $this->session['psg_userid'];
		}
		
		// Set the image title
		if (trim($title) != '') {
			$im_title = $title;
		} elseif (trim($title) == '' && trim($headline) != '') {
			$im_title = $headline;
		} elseif ($objXMPCore->getValue('title') != '') {
			$im_title = $objXMPCore->getValue('title');
		} else {
			$im_title = $this->session['pref_import_image_title'];
		}
		
		// Set the image caption
		if (trim($caption) == '' && $objXMPCore->getValue('description') != '') {
			$caption = $objXMPCore->getValue('description');
		} elseif (trim($caption) != '') {
			$caption = $caption;
		} else {
			$caption = $this->session['pref_import_image_caption'];
		}
		
		// Set the image copyright
		if (trim($copyright) == '') {
			$copyright = $this->session['pref_import_image_copyright'];
		}
		
		// Set the image keywords information
		if (trim($keywords) == '' && $objXMPCore->getValue('keywords') != '') {
			$keywords = $objXMPCore->getValue('keywords');
		} elseif (trim($keywords) != '') {
			$keywords = $keywords;
		} else {
			$keywords = $this->session['pref_import_image_keywords'];
		}
		
		$objImage->setImageActive(1);
		$objImage->setImagePath($image_path);
		$objImage->setImageUserId($image_owner);
		$objImage->setImageFileName($file_name);
		$objImage->setImageTitle($im_title);
		$objImage->setImageCaption($caption);
		$objImage->setImageKeywords($keywords);
		$objImage->setImageWidth($orig_w);
		$objImage->setImageHeight($orig_h);
		$objImage->setImageCopyright($copyright);
		$objImage->setImagePermissions($this->session['pref_import_image_permissions']);
		$objImage->setImageViewers($this->session['pref_import_image_viewers']);
		$objImage->setImageRightsType($this->session['pref_import_image_rights_type']);
		$objImage->setImageRightsText($this->session['pref_import_image_rights_text']);
		$objImage->setImagePrice($this->session['pref_import_image_price']);
		$objImage->setImageSale($this->session['pref_import_image_sale']);
		$objImage->setImageProductLink($this->session['pref_import_image_product_link']);
		$objImage->setImageProducts($this->session['pref_import_image_products']);
		$objImage->setImageModelRelease($this->session['pref_import_image_model_release']);
		$objImage->setImagePropertyRelease($this->session['pref_import_image_property_release']);
		$objImage->setImageExtra1($this->session['pref_import_image_extra_01']);
		$objImage->setImageExtra2($this->session['pref_import_image_extra_02']);
		$objImage->setImageExtra3($this->session['pref_import_image_extra_03']);
		$objImage->setImageExtra4($this->session['pref_import_image_extra_04']);
		$objImage->setImageExtra5($this->session['pref_import_image_extra_05']);
		
		$objImage->setIconWidth($icon_w);
		$objImage->setIconHeight($icon_h);
		$objImage->setSmallWidth($smal_w);
		$objImage->setSmallHeight($smal_h);
		$objImage->setLargeWidth($larg_w);
		$objImage->setLargeHeight($larg_h);
		$objImage->setSmallCompingWidth($scom_w);
		$objImage->setSmallCompingHeight($scom_h);
		$objImage->setCompingWidth($comp_w);
		$objImage->setCompingHeight($comp_h);
		$objImage->setOriginalWidth($orig_w);
		$objImage->setOriginalHeight($orig_h);
		
		$objImage->setIPTCCreator($creator);
		$objImage->setIPTCJobtitle($jobtitle);
		$objImage->setIPTCCity($city);
		$objImage->setIPTCCountry($country);
		$objImage->setIPTCCredit($credit);
		$objImage->setIPTCSource($source);
		$objImage->setIPTCObject($object);
		$objImage->setIPTCByline($byline);

		$objImage->setGpsLongitude($longitude);
		$objImage->setGpsLatitude($latitude);
		
		$objImage->createImage();
		
		return $objImage->getImageId();
		
	}
	
	/*
	*
	*	
	*
	*/
	function getImageSize ($file_name, $image_path, $size='') {
		
		if ($size != '') {
		
			return @getimagesize(SYS_BASE_PATH . 'resources/library/' . $image_path . '/' . $size . '/' . $file_name);
			
		} else {
			
			return @getimagesize(SYS_BASE_PATH . 'resources/library/' . $image_path . '/' . LARGE_COMPING_DIR . '/' . $file_name);
		
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function galleryColumnView () {
		
		require_once('class.PixariaImage.php');
		
		$parent_gallery_id = $this->input->name('gallery_id');
		
		$sql = "SELECT gallery_id, gallery_parent FROM ".PIX_TABLE_GALL."";
		
		list ($all_gallery_id, $all_gallery_parent_id) = $this->db->rowsAsColumns($sql);
		
		if ($parent_gallery_id == '') {
			
			$sql = "SELECT gallery_id, gallery_title, gallery_key FROM ".PIX_TABLE_GALL." WHERE gallery_parent = '0'";
			
		} else {
			
			$sql = "SELECT gallery_id, gallery_title, gallery_key FROM ".PIX_TABLE_GALL." WHERE gallery_parent = '".$this->db->escape($parent_gallery_id)."'";
			
		}
		
		list ($gallery_id, $gallery_title, $gallery_key) = $this->db->rowsAsColumns($sql);
		
		while ( list($key, $value) = @each($gallery_id) ) {
			
			if (in_array($gallery_id[$key], $all_gallery_parent_id)) {
				$gallery_url[] = SYS_BASE_URL.FILE_ADM_IMPORT."?cmd=galleryColumnView&amp;gallery_id=" . $gallery_id[$key];
			} else {
				$gallery_url[] = '';
			}
			
		}
		
		if ($parent_gallery_id == '') {
			
			$this->view->assign('url',$gallery_url);
			$this->view->assign('title',$gallery_title);
			$this->view->assign('data',$gallery_id);
			$this->view->assign('input_name','gallery_id');
			
		} else {
		
			$this->view->assign('fcv_url',$gallery_url);
			$this->view->assign('fcv_title',$gallery_title);
			$this->view->assign('fcv_data',$gallery_id);
			$this->view->assign('fcv_input_name','gallery_id');
			
			$this->view->display('plugins/fancy.column.html');
			
		}
		
	}
	
}

?>