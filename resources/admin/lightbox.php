<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "users";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Initialise the Smarty object
$smarty = new Smarty_Pixaria;

if ($objEnvData->fetchPost('cmd') == "12") {

	// Check that the user is an image_editor
	pix_authorise_user("image_editor");
	
} else {

	// Check that the user is a administrator
	pix_authorise_user("administrator");
	
}

$objLightBox = new Lightbox();

class Lightbox {

	/*
	*
	*
	*
	*/
	function Lightbox () {
	
		global 	$objEnvData,
				$smarty,
				$ses,
				$cfg;

		// Load an initialise the database class
		require_once ('class.Database.php');
		require_once (SYS_BASE_PATH . 'resources/classes/class.InputData.php');

		$this->db		= new Database();
		$this->input	= new InputData();
		$this->view 	= $smarty;
		$this->config	= $cfg;
		$this->session 	= $ses;
		
		// Assign an array of form actions to the view object
		$this->view->assign('form_actions', array (

			'images_delete' 		=> "{url:'".SYS_BASE_URL.FILE_ADM_IMAGE."',cmd:'formDeleteFromLibrary'}",
			'images_batch_edit' 	=> "{url:'".SYS_BASE_URL.FILE_ADM_IMAGE."',cmd:'editMultiple'}",
			'gallery_images_create' => "{url:'".SYS_BASE_URL.FILE_ADM_GALLERY."',cmd:'formCreateNewGallery'}",
			'gallery_images_add' 	=> "{url:'".SYS_BASE_URL.FILE_ADM_GALLERY."',cmd:'formAddImagesToGallery'}"
		
		));
		
		switch ($this->input->name('cmd')) {
			
			case 'listLightboxImages':
				$this->listLightboxImages();
			break;
			
			case 'actionEmptyLightbox':
				$this->actionEmptyLightbox();
			break;
			
			case 'actionDeleteLightbox':
				$this->actionDeleteLightbox();
			break;
			
			default:
				$this->index();
			break;
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function index () {
		
		global $cfg, $ses, $objEnvData, $smarty;
		
		// Load active lightboxes for this user
		$sql = "SELECT ".PIX_TABLE_LBOX.".*, ".PIX_TABLE_USER.".*, COUNT(lightbox_id) AS items FROM ".PIX_TABLE_USER."
				
				LEFT JOIN ".PIX_TABLE_LBOX." ON ".PIX_TABLE_USER.".userid = ".PIX_TABLE_LBOX.".userid
				
				LEFT JOIN ".PIX_TABLE_LBME." ON ".PIX_TABLE_LBME.".lightbox_id = ".PIX_TABLE_LBOX.".id
				
				WHERE ".PIX_TABLE_LBME.".id != ''
				
				AND ".PIX_TABLE_USER.".email_address != ''

				GROUP BY ".PIX_TABLE_LBOX.".id
				
				ORDER BY ".PIX_TABLE_USER.".family_name";

		$result = $this->db->rows($sql);
		
		if (is_array($result)) {
			
			$this->view->assign("lightbox_count",count($result));
			
			foreach ($result as $key => $value) {
				
				$lightbox_id[]			= $value['id'];
				$lightbox_items[]		= $value['items'];
				$lightbox_name[]		= stripslashes($value['name']);
				$lightbox_date[]		= $value['date'];
				$lightbox_type[]		= $value['type'];
				$lightbox_status[]		= $value['status'];
				$lightbox_access_key[]	= strtoupper(substr(md5($cfg['sys']['encryption_key'].$value['id']),0,6));
				$lightbox_user_name[]	= $value['family_name'] . ", " . $value['first_name'];
				$lightbox_userid[]		= $value['userid'];
				
			}
			
		} else {
			
			$this->view->assign("lightbox_count",0);
		
		}
		
		// Load lightbox information into Smarty
		$this->view->assign("active_lightbox",$result[0]['active_image_box']);
		$this->view->assign("lightbox_id",$lightbox_id);
		$this->view->assign("lightbox_items",$lightbox_items);
		$this->view->assign("lightbox_name",$lightbox_name);
		$this->view->assign("lightbox_date",$lightbox_date);
		$this->view->assign("lightbox_type",$lightbox_type);
		$this->view->assign("lightbox_status",$lightbox_status);
		$this->view->assign("lightbox_access_key",$lightbox_access_key);
		$this->view->assign("lightbox_user_name",$lightbox_user_name);
		$this->view->assign("lightbox_userid",$lightbox_userid);
		
		// Define html page title
		$this->view->assign("page_title","List active lightboxes");
		
		// Output html from template file
		$this->view->display('lightbox/lightbox.list.html');
	
		// Stop running the script here
		exit;
		
	}

	/*
	*
	*	
	*
	*/
	function listLightboxImages () {
		
		$lightbox_id = $this->input->get('lid');
		
		$sql = "SELECT ".PIX_TABLE_IMGS.".*, ".PIX_TABLE_LBME.".userid
		
				FROM `".PIX_TABLE_IMGS."`
				
				LEFT JOIN `".PIX_TABLE_LBME."` ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_LBME.".image_id
				
				WHERE ".PIX_TABLE_LBME.".lightbox_id = '".$this->db->escape($lightbox_id)."'
				
				GROUP by ".PIX_TABLE_IMGS.".image_id
				
				ORDER by ".PIX_TABLE_IMGS.".image_id";
		
		// Load an array of image data for this gallery
		$images = $this->db->rows($sql);
		
		$image_id		= array();
		$image_name		= array();
		$image_filename	= array();
		$asset_iconpath	= array();
		
		if (count($images) > 0 && is_array($images)) {
		
			foreach ($images as $row) {
			
				$userid				= $row['userid'];
				$image_id[]			= $row['image_id'];
				$image_active[]		= $row['image_active'];
				$image_filename[] 	= $row['image_filename'];
				$image_path[]		= $row['image_path'];
				$image_date[]		= $row['image_date'];
				
				$icon_path[]		= base64_encode(SYS_BASE_LIBRARY.$row[image_path]."/32x32/".$row[image_filename]);
				$icon_size			= getimagesize(SYS_BASE_LIBRARY.$row[image_path]."/32x32/".$row[image_filename]);
				$icon_width[]		= $icon_size[0];
				$icon_height[]		= $icon_size[1];
				
				$comp_path[]		= base64_encode(SYS_BASE_LIBRARY.$row[image_path]."/".COMPING_DIR."/".$row[image_filename]);
				$comp_size			= getimagesize(SYS_BASE_LIBRARY.$row[image_path]."/".COMPING_DIR."/".$row[image_filename]);
				$comp_width[]		= $comp_size[0];
				$comp_height[]		= $comp_size[1];
				
				$image_width[]		= $row[image_width];
				$image_height[]		= $row[image_height];
				
				$image_title[]		= stripslashes($row[image_title]);

			}
			
			// Assign image information to Smarty
			$this->view->assign("userid",$userid);
			$this->view->assign("image_id",$image_id);
			$this->view->assign("image_active",$image_active);
			$this->view->assign("image_filename",$image_filename);
			$this->view->assign("image_path",$image_path);
			$this->view->assign("image_date",$image_date);
			$this->view->assign("icon_path",$icon_path);
			$this->view->assign("icon_width",$icon_width);
			$this->view->assign("icon_height",$icon_height);
			$this->view->assign("comp_path",$comp_path);
			$this->view->assign("comp_width",$comp_width);
			$this->view->assign("comp_height",$comp_height);
			$this->view->assign("image_width",$image_width);
			$this->view->assign("image_height",$image_height);
			$this->view->assign("image_title",$image_title);

			require_once ("class.PixariaUser.php");	
		
			$objPixariaUser = new PixariaUser($userid);
			
			$this->view->assign("first_name",$objPixariaUser->getFirstName());
			$this->view->assign("family_name",$objPixariaUser->getFamilyName());
			$this->view->assign("email_address",$objPixariaUser->getEmail());
			$this->view->assign("address1",$objPixariaUser->getAddress1());
			$this->view->assign("address2",$objPixariaUser->getAddress2());
			$this->view->assign("address3",$objPixariaUser->getAddress3());
			$this->view->assign("city",$objPixariaUser->getCity());
			$this->view->assign("region",$objPixariaUser->getRegion());
			$this->view->assign("country",$objPixariaUser->getCountryName());
			$this->view->assign("postal_code",$objPixariaUser->getPostCode());
		
		}
		
		// Count number of images in the library
		$this->view->assign("image_id_count",count($image_id));
		
		// Set the page title
		$this->view->assign("page_title","Images in this lightbox");
		
		// Display the main HTML page
		$this->view->display('lightbox/images.list.html');
		
	}
	
		
	/*
	*
	*	
	*
	*/
	function actionDeleteLightbox () {
	
		$userid 		= $this->input->name('userid');
		$lightbox_id 	= $this->input->name('lid');
		
		// Delete the user from the lightbox table
		$sql = "DELETE FROM ".PIX_TABLE_LBOX." WHERE userid = '".$this->db->escape($userid)."' AND id = '".$this->db->escape($lightbox_id)."'";
		$this->db->query($sql);
					
		// Delete the user from the lightbox_members table
		$sql = "DELETE FROM ".PIX_TABLE_LBME." WHERE userid = '".$this->db->escape($userid)."' AND lightbox_id = '".$this->db->escape($lightbox_id)."'";
		$this->db->query($sql);
		
		// Get the ID of another lightbox the user has created (if any)
		$sql = "SELECT id FROM ".PIX_TABLE_LBOX." WHERE userid = '".$this->db->escape($userid)."'";
		list($lightbox_id) = $this->db->row($sql);
		
		if (is_numeric($lightbox_id)) {
			
			// Reset the user's default lightbox
			$sql = "UPDATE ".PIX_TABLE_USER." SET active_image_box = '".$this->db->escape($lightbox_id)."' WHERE userid = '".$this->db->escape($userid)."'";
			$this->db->query($sql);
		
		}
		
		// Back to the lightbox list
		$this->index();
		
	}
	
	/*
	*
	*	
	*
	*/
	function actionEmptyLightbox () {
	
		$userid 		= $this->input->name('userid');
		$lightbox_id 	= $this->input->name('lid');
		
		// Delete the user from the lightbox_members table
		$sql = "DELETE FROM ".PIX_TABLE_LBME." WHERE userid = '".$this->db->escape($userid)."' AND lightbox_id = '".$this->db->escape($lightbox_id)."'";
		$this->db->query($sql);
		
		// Back to the lightbox list
		$this->index();
		
	}
	
}

?>