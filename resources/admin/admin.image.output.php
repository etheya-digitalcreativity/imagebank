<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Check that the user is a SuperUser
pix_authorise_user("administrator");

if ($objEnvData->fetchGlobal('file')) {

	$file_path = base64_decode($objEnvData->fetchGlobal('file'));
	
	// Send JPG content HTTP header and cache for two days
	pix_http_headers("jpg","2");
	
	echo fread(fopen($file_path, 'r'), filesize($file_path));
	
} else {
	
	// Initialise the smarty object
	$smarty = new Smarty_Pixaria;

	// Set the image filepath as base 64 string
	$smarty->assign("filepath",$objEnvData->fetchGlobal('html'));
	
	// Get the image path
	$image_path = base64_decode($objEnvData->fetchGlobal('html'));
	
	// Load the file properties class
	require_once('class.PixariaFile.php');
	
	// Create a file property object
	$objImageFile = new PixariaFile($image_path);
	
	// Get image properties
	$smarty->assign("image_filesize",$objImageFile->getFileSize());
	$smarty->assign("image_filename",$objImageFile->getFileName());
	$smarty->assign("image_filetype",$objImageFile->getFileType());
	$smarty->assign("image_width",$objImageFile->getFileImageWidth());
	$smarty->assign("image_height",$objImageFile->getFileImageHeight());
	
	$smarty->assign("image_width_half",ceil($objImageFile->getFileImageWidth() / 2));
	$smarty->assign("image_height_half",ceil($objImageFile->getFileImageHeight() / 2));

	// Send HTML content HTTP header and don't cache
	pix_http_headers("html","");
	
	// Display the html header
	$smarty->pixDisplay('admin.image/image.output.tpl');

}

?>