<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "site";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Initialise the class for this page
$objAdminUpgrade = new AdminUpgrade();

// The admin diagnostics class
class AdminUpgrade {
	
	// Define internal properties
	var $current_version;
	var $database_modified	= false;
	var $database_report	= array();
	var $_dbl;
	
	/*
	*	Class constructor for the AdminDiagnostics class
	*	
	*	@return void
	*/
	function AdminUpgrade () {
		
		global $ses, $cfg, $objEnvData;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		// Authenticate the user
		$this->authenticateUser();

	}
	
	/*
	*
	*	Authenticate the user
	*
	*/
	function authenticateUser () {
		
		global $cfg, $objEnvData, $ses;
		
		$email_address	= $objEnvData->fetchGlobal('email');
		$password		= $objEnvData->fetchGlobal('password');
		
		if ($email_address != "" && $password != "") {
		
			// Check that the user's e-mail address exists on the system
			$result = $this->_dbl->sqlSelectRow("SELECT * FROM " . PIX_TABLE_USER . " WHERE email_address = '$email_address' AND account_status = '1'");
			
			$userid			= $result['userid'];
			$user_name		= $result['first_name'] . " " . $result['family_name'];
			$password		= $result['password'];
			$account_status	= $result['account_status'];
		
		}

		if ($ses['psg_userid'] != "" || (md5($objEnvData->fetchGlobal('password')) === $password) ) {
		
			$this->runDatabaseUpgrade();
		
		} else {
		
			$this->showFormAuthenticate();
		
		}
	
	}
	
	/*
	*
	*	Run the database upgrades
	*
	*/
	function runDatabaseUpgrade () {
		
		global $cfg;
		
		// Get the current database version
		$this->current_version = $cfg['set']['database_version'];
		
		// Find which method we need to call to begin upgrading the database
		$version_method = (string)"upgrade_" . eregi_replace("\.","_",$this->current_version);
		
		// Check the method we've identified exists
		if (method_exists($this, $version_method)) {
			
			// If it does exist, then call it
			$this->{$version_method}();
			
		}
		
		// Clear the template cache
		pix_clear_cache();

		// Show the database status
		$this->showDatabaseStatus();
		
	}
	
	/*
	*
	*	Show the authentication form
	*
	*/
	function showFormAuthenticate () {
	
		global $cfg, $smarty;
		
		// Set the page title
		$smarty->assign("page_title","Authenticate to continue");
		
		// Display the database status
		$smarty->pixDisplay('upgrade/upgrade.authenticate.tpl');
		
		exit;
		
	}
	
	/*
	*
	*	Show the user a results page of changes made
	*
	*/
	function showDatabaseStatus () {
		
		global $cfg, $smarty;
		
		$result = $this->_dbl->sqlSelectRow("SELECT value FROM ".PIX_TABLE_SETT." WHERE name = 'database_version'");
		
		$smarty->assign("database_version",$result[0]);
		$smarty->assign("software_version",$cfg['set']['software_version']);
		
		$smarty->assign("database_modified",$this->database_modified);
		$smarty->assign("database_report",$this->database_report);
		
		// Set the page title
		$smarty->assign("page_title","Database Updater");
		
		// Display the database status
		$smarty->pixDisplay('upgrade/upgrade.result.tpl');
		
		exit;
		
	}
	
	/*
	*
	*	Upgrade 2.0.0 to current release
	*
	*/
	function upgrade_2_0_0 () {
		
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.0.1' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1;";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.0.0.";
		
		/*
		*	Change download settings
		*/
		if ($cfg['set']['func_downloads_user'] == "1") {
		
			$this->_dbl->sqlQuery("UPDATE ".PIX_TABLE_SETT." SET `value` = '10' WHERE name = 'func_downloads_user' LIMIT 1;");
		
		} elseif ($cfg['set']['func_downloads_user'] == "0") {
			
			$this->_dbl->sqlQuery("UPDATE ".PIX_TABLE_SETT." SET `value` = '11' WHERE name = 'func_downloads_user' LIMIT 1;");
		
		}
		
		$this->database_report[] = "Image download settings updated to accomodate a new setting for 'by user' download preferences.";
		
		/*
		*	Add new system level user groups
		*/
		$this->_dbl->sqlQuery("INSERT INTO ".PIX_TABLE_SGRP." VALUES ('4', 'Allowed to delete images'), ('5', 'Allowed to download high-res images');");
		
		$this->database_report[] = "Add new system level user groups for image deletion and download.";
		
		/*
		*	Add 2Checkout support
		*/
		$this->_dbl->sqlQuery("INSERT INTO ".PIX_TABLE_SETT." VALUES ('store_func_2co', '0'), ('store_2co_sid', ''), ('store_2co_secret',''), ('store_2co_demo','0');");
		
		$this->database_report[] = "Add settings for 2Checkout payment support.";
		
		/*
		*	Add index
		*/
		$this->_dbl->sqlQuery("ALTER TABLE ".PIX_TABLE_IMVI." ADD PRIMARY KEY ( `image_id` , `group_id` )");
		
		$this->database_report[] = "Add a new index to the image viewers table to speed up performance.";
		
		// Go to the next upgrade method
		$this->upgrade_2_0_1();
		
	}
	
	/*
	*
	*	Upgrade 2.0.1 to current release
	*
	*/
	function upgrade_2_0_1 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.0.2' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1;";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.0.2.";
		
		/*
		*	Add localisation support
		*/
		$sql = "CREATE TABLE `psg_localisations` (
				`id` INT( 7 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
				`template_name` VARCHAR( 100 ) NOT NULL ,
				`template_ref` VARCHAR( 50 ) NOT NULL ,
				`iso_lang_code` VARCHAR( 5 ) NOT NULL ,
				`deprecated` INT( 1 ) NOT NULL DEFAULT '0',
				`string` TEXT NOT NULL
				);";

		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "New table added to contain language strings for template localisation.";
		
		/*
		*	Add Divestock support
		*/
		$this->_dbl->sqlQuery("INSERT INTO ".PIX_TABLE_SETT." VALUES ('func_divestock', '0'), ('divestock_username',''), ('divestock_api_key','');");
		
		$this->database_report[] = "Add settings for Divestock integration support.";
		
		// Go to the next upgrade method
		$this->upgrade_2_0_2();
		
	}
	
	/*
	*
	*	Upgrade 2.0.2 to current release
	*
	*/
	function upgrade_2_0_2 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.1.0' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1;";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.1.0.";
		
		/*
		*	Add rights managed search support
		*/
		$this->_dbl->sqlQuery("INSERT INTO ".PIX_TABLE_SETT." VALUES ('search_opt_rights_managed', '1');");
		
		$this->database_report[] = "Add settings for rights management support in searches.";
		
		// Go to the next upgrade method
		$this->upgrade_2_1_0();
		
	}
	
	/*
	*
	*	Upgrade 2.1.0 to current release
	*
	*/
	function upgrade_2_1_0 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.1.1' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1;";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.1.1.";
		
		// Go to the next upgrade method
		$this->upgrade_2_1_1();
		
	}
	
	/*
	*
	*	Upgrade 2.1.1 to current release
	*
	*/
	function upgrade_2_1_1 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.2.0' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1;";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.2.0.";
		
		$sql = "INSERT INTO ".PIX_TABLE_SETT." VALUES (`name` ,`value`)VALUES ('store_func_terms_conditions', '0'), ('store_terms_conditions', '');";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Added rows for storing terms and conditions of sale settings.";
		
		$sql = "INSERT INTO ".PIX_TABLE_SETT." VALUES (`name` ,`value`)VALUES ('ftp_address', ''), ('ftp_path', '');";
		
		$this->_dbl->sqlQuery($sql);
		
		$sql = "INSERT INTO ".PIX_TABLE_SETT." VALUES (`name` ,`value`)VALUES ('ftp_username', ''), ('ftp_password', '');";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Added rows for storing FTP settings.";
		
		$sql = "INSERT INTO ".PIX_TABLE_SETT." VALUES (`name` ,`value`)VALUES ('func_disable_saving', '1'), ('func_insert_metadata', '1');";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Added settings for disabling right click and drag and drop.";
		
		$this->database_report[] = "Added settings for controlling how metadata is added to downloaded comping images";
		
		$sql = "ALTER TABLE `".PIX_TABLE_SGRP."` ADD `sys_group_description` VARCHAR( 255 ) NOT NULL ;";
		
		$this->_dbl->sqlQuery($sql);
		
		$sql = "UPDATE `".PIX_TABLE_SGRP."` SET sys_group_name = 'Administrator', sys_group_description = 'Users with permission to access admin control panels' WHERE `sys_group_id` = '1';";
		
		$this->_dbl->sqlQuery($sql);
		
		$sql = "UPDATE `".PIX_TABLE_SGRP."` SET sys_group_name = 'Image editor', sys_group_description = 'Users with permission to edit images view the front end' WHERE `sys_group_id` = '2';";
		
		$this->_dbl->sqlQuery($sql);
		
		$sql = "UPDATE `".PIX_TABLE_SGRP."` SET sys_group_name = 'Photographer', sys_group_description = 'Users with permission to upload images to the library' WHERE `sys_group_id` = '3';";
		
		$this->_dbl->sqlQuery($sql);
		
		$sql = "UPDATE `".PIX_TABLE_SGRP."` SET sys_group_name = 'Delete images', sys_group_description = 'Users with permission to delete images from the library' WHERE `sys_group_id` = '4';";
		
		$this->_dbl->sqlQuery($sql);
		
		$sql = "UPDATE `".PIX_TABLE_SGRP."` SET sys_group_name = 'High resolution download', sys_group_description = 'Users with permission to download high resolution images' WHERE `sys_group_id` = '5';";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Added descriptions to built in user permission groups and updated group names.";
		
		// Go to the next upgrade method
		$this->upgrade_2_2_0();
		
	}
	
	/*
	*
	*	Upgrade 2.2.0 to current release
	*
	*/
	function upgrade_2_2_0 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.2.1' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1;";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.2.1.";
		
		/*
		*	
		*/
		if ($cfg['set']['store_func_terms_conditions'] == "" || $cfg['set']['store_terms_conditions'] == "") {
			
			$sql = "DELETE FROM ".PIX_TABLE_SETT." WHERE name = 'store_func_terms_conditions'; DELETE FROM ".PIX_TABLE_SETT." WHERE name = 'store_terms_conditions';";
			
			$this->_dbl->sqlQuery($sql);
		
			$sql = "INSERT INTO ".PIX_TABLE_SETT." VALUES ('store_func_terms_conditions', '0'), ('store_terms_conditions', '');";
		
			$this->_dbl->sqlQuery($sql);
		
			$this->database_report[] = "Added rows for storing terms and conditions of sale settings.";
		
		}
		
		
		/*
		*	
		*/
		if ($cfg['set']['ftp_address'] == "" || $cfg['set']['ftp_path'] == "" || $cfg['set']['ftp_username'] == "" || $cfg['set']['ftp_password'] == "") {
		
			$sql = "DELETE FROM ".PIX_TABLE_SETT." WHERE name = 'ftp_address'; DELETE FROM ".PIX_TABLE_SETT." WHERE name = 'ftp_path';";
			
			$this->_dbl->sqlQuery($sql);
		
			$sql = "DELETE FROM ".PIX_TABLE_SETT." WHERE name = 'ftp_username'; DELETE FROM ".PIX_TABLE_SETT." WHERE name = 'ftp_password';";
			
			$this->_dbl->sqlQuery($sql);
		
			$sql = "INSERT INTO ".PIX_TABLE_SETT." VALUES ('ftp_address', ''), ('ftp_path', '');";
			
			$this->_dbl->sqlQuery($sql);
			
			$sql = "INSERT INTO ".PIX_TABLE_SETT." VALUES ('ftp_username', ''), ('ftp_password', '');";
			
			$this->_dbl->sqlQuery($sql);
			
			$this->database_report[] = "Added rows for storing FTP settings.";

		}
		
		/*
		*	
		*/
		if ($cfg['set']['func_disable_saving'] == "" || $cfg['set']['func_insert_metadata'] == "") {
		
			$sql = "DELETE FROM ".PIX_TABLE_SETT." WHERE name = 'func_disable_saving'; DELETE FROM ".PIX_TABLE_SETT." WHERE name = 'func_insert_metadata';";
			
			$this->_dbl->sqlQuery($sql);
		
			$sql = "INSERT INTO ".PIX_TABLE_SETT." VALUES ('func_disable_saving', '1'), ('func_insert_metadata', '1');";
			
			$this->_dbl->sqlQuery($sql);
			
			$this->database_report[] = "Added settings for disabling right click and drag and drop.";
			
			$this->database_report[] = "Added settings for controlling how metadata is added to downloaded comping images";
		
		}
		
		$sql = "ALTER TABLE ".PIX_TABLE_SETT." CHANGE `value` `value` TEXT NOT NULL";
		
		$this->_dbl->sqlQuery($sql);
			
		$this->database_report[] = "Updated settings table value length.";

		// Go to the next upgrade method
		$this->upgrade_2_2_1();
		
	}
	
	/*
	*
	*	Upgrade 2.2.1 to current release
	*
	*/
	function upgrade_2_2_1 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.2.2' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1;";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.2.2.";
		
		/*
		*	Add search storage table
		*/
		$this->_dbl->sqlQuery("CREATE TABLE ".PIX_TABLE_SRCH." (`id` INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY ,`type` INT(1) NOT NULL ,`string` TEXT NOT NULL);");
		
		$this->database_report[] = "Add new database table to store searches.";
		
		/*
		*	Add userid to search storage
		*/
		$this->_dbl->sqlQuery("ALTER TABLE ".PIX_TABLE_SRCH." ADD `userid` INT( 15 ) NOT NULL AFTER `id` ;");
		
		/*
		*	Add search storage table
		*/
		$this->_dbl->sqlQuery("CREATE TABLE ".PIX_TABLE_SRCS." (`userid` INT(15) NOT NULL ,`search_id` INT(15) NOT NULL ,`image_id` INT(15) NOT NULL);");
		
		$this->_dbl->sqlQuery("ALTER TABLE ".PIX_TABLE_SRCS." ADD `date` INT( 16 ) NOT NULL;");
		
		$this->database_report[] = "Add new database table to store search indices.";
		
		// Go to the next upgrade method
		$this->upgrade_2_2_2();
		
	}
	
	/*
	*
	*	Upgrade 2.2.2 to current release
	*
	*/
	function upgrade_2_2_2 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.2.3' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1;";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.2.3.";
		
		/*
		*	Change database version
		*/
		$sql = "INSERT INTO ".PIX_TABLE_CTRY." VALUES ('GG', 'GUERNSEY', 'Guernsey', 'GGY', NULL), ('JE', 'JERSEY', 'Jersey', 'JEY', NULL);";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Added the Channel Islands States of Jersey and Guernsey as new countries.";
		
		/*
		*	Add new column to calculation objects table
		*/
		$sql = "ALTER TABLE ".PIX_TABLE_CAOB." ADD `calc_type` INT( 1 ) NOT NULL DEFAULT '1';";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Added a new column to the calculation table to reflect the ";
		
		// Go to the next upgrade method
		$this->upgrade_2_2_3();
		
	}
	
	/*
	*
	*	Upgrade 2.2.3 to current release
	*
	*/
	function upgrade_2_2_3 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.2.4' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.2.4.";
		
		/*
		*	Update database indices and primary keys
		*/				
		$sql = "ALTER TABLE `".PIX_TABLE_SRCS."` ADD `id` INT( 15 ) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST";
		$this->_dbl->sqlQuery($sql);
		
		$sql = "ALTER TABLE `".PIX_TABLE_GORD."` ADD INDEX ( `gallery_id` )";
		$this->_dbl->sqlQuery($sql);
		
		$sql = "ALTER TABLE `".PIX_TABLE_GALL."` ADD PRIMARY KEY ( `gallery_id` )";
		$this->_dbl->sqlQuery($sql);
		
		$sql = "ALTER TABLE `".PIX_TABLE_GALL."` DROP INDEX `ID`";
		$this->_dbl->sqlQuery($sql);
		
		$sql = "ALTER TABLE `".PIX_TABLE_SRCS."` ADD INDEX ( `image_id` )";
		$this->_dbl->sqlQuery($sql);
		
		$sql = "ALTER TABLE `".PIX_TABLE_SRCS."` ADD INDEX ( `search_id` )";
		$this->_dbl->sqlQuery($sql);
		
		$sql = "ALTER TABLE `".PIX_TABLE_IMGS."` DROP INDEX `image_filename`";
		$this->_dbl->sqlQuery($sql);
		
		$sql = "ALTER TABLE `".PIX_TABLE_IMGS."` ADD INDEX ( `image_filename` )";
		$this->_dbl->sqlQuery($sql);
		
		$sql = "ALTER TABLE `".PIX_TABLE_IMGS."` ADD FULLTEXT (`image_keywords`)";
		$this->_dbl->sqlQuery($sql);
		
		$sql = "ALTER TABLE `".PIX_TABLE_IMGS."` ADD FULLTEXT (`image_caption`)";
		$this->_dbl->sqlQuery($sql);
		
		$sql = "ALTER TABLE `".PIX_TABLE_IMGS."` ADD FULLTEXT (`image_title`)";
		$this->_dbl->sqlQuery($sql);
		
		$sql = "ALTER TABLE `".PIX_TABLE_IMGS."` ADD INDEX ( `image_title` )";
		$this->_dbl->sqlQuery($sql);
		
		$sql = "ALTER TABLE `".PIX_TABLE_GALL."` ADD INDEX ( `gallery_password` )";
		$this->_dbl->sqlQuery($sql);
				
		$this->database_report[] = "Database indices and primary keys updated to improve performance.";
		
		// Go to the next upgrade method
		$this->upgrade_2_2_4();
		
	}
	
	/*
	*
	*	Upgrade 2.2.4 to current release
	*
	*/
	function upgrade_2_2_4 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.2.5' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.2.5.";
		
		/*
		*
		*/
		$sql = "INSERT INTO ".PIX_TABLE_SETT." VALUES ('func_download_comp', '1'), ('func_tinymce', '1')";
			
		$this->_dbl->sqlQuery($sql);
			
		$this->database_report[] = "Added settings for controlling free comp downloads.";
			
		$this->database_report[] = "Added settings for enabling and disabling TinyMCE.";
		
		/*
		*	Capture mechanism
		*/
		$sql = "CREATE TABLE `".PIX_TABLE_CAPT."` (
				`id` INT( 5 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
				`key` VARCHAR( 50 ) NOT NULL ,
				`code` VARCHAR( 5 ) NOT NULL ,
				`date` DATETIME NOT NULL
				)";
				
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Added database storage for the CAPTCHA verification system.";
				
		/*
		*
		*/
		$sql = "INSERT INTO ".PIX_TABLE_SETT." VALUES ('func_captcha_registration', '1'), ('func_captcha_contact', '1')";
			
		$this->_dbl->sqlQuery($sql);
			
		$this->database_report[] = "Added settings for controlling where image based CAPTCHA is used";
		
		// Go to the next upgrade method
		$this->upgrade_2_2_5();
		
	}
	
	/*
	*
	*	Upgrade 2.2.5 to current release
	*
	*/
	function upgrade_2_2_5 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.2.6' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1;";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.2.6.";
		
		$sql = "DROP TABLE `PIX_TABLE_TIMG`";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Removing deprecated table.";
		
		$sql = "DROP TABLE `PIX_TABLE_TAGS`";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Removing deprecated table.";
		
		// Go to the next upgrade method
		$this->upgrade_2_2_6();
		
	}
	
	/*
	*
	*	Upgrade 2.2.6 to current release
	*
	*/
	function upgrade_2_2_6 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.3.0' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1;";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.3.0.";
		
		$sql = "DROP TABLE `".PIX_TABLE_KTAG."`";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Removing deprecated table.";
		
		$sql = "DROP TABLE `".PIX_TABLE_KIMG."`";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Removing deprecated table.";
		
		$sql = "ALTER TABLE `".PIX_TABLE_PROD."` ADD `prod_dnl_size` VARCHAR( 50 ) NOT NULL AFTER `prod_shipping_multiple` ;";
		
		$this->_dbl->sqlQuery($sql);
		
		$sql = "ALTER TABLE `".PIX_TABLE_PROD."` CHANGE `prod_type` `prod_type` ENUM( 'IMG', 'IND', 'DNL' ) NOT NULL DEFAULT 'IMG'";
		
		$this->_dbl->sqlQuery($sql);
		
		$sql = "ALTER TABLE `".PIX_TABLE_CAME."` ADD `item_prod_code` VARCHAR( 50 ) NOT NULL ;";
		
		$this->_dbl->sqlQuery($sql);
		
		$sql = "ALTER TABLE `".PIX_TABLE_CAME."` ADD `item_dnl_size` VARCHAR( 50 ) NOT NULL ;";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Adding support for image download products.";
		
		$sql = "ALTER TABLE `".PIX_TABLE_GALL."` ADD INDEX `gallery_structure` ( `gallery_id` , `gallery_parent` )";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Adding index to gallery table to speed up reading of large nested gallery lists.";		
		
		$sql = "INSERT INTO `".PIX_TABLE_SETT."` VALUES ('search_max_limit', '1000')";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Adding new setting for limiting the maximum number of search results.";		
		
		$sql = "ALTER TABLE `".PIX_TABLE_SRCH."` ADD INDEX `userid` ( `userid` )";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Adding new setting for limiting the maximum number of search results.";		
		
		$sql = "ALTER TABLE `".PIX_TABLE_IMGS."` 
				ADD `icon_w` INT( 5 ) NOT NULL DEFAULT '0',
				ADD `icon_h` INT( 5 ) NOT NULL DEFAULT '0',
				ADD `smal_w` INT( 5 ) NOT NULL DEFAULT '0',
				ADD `smal_h` INT( 5 ) NOT NULL DEFAULT '0',
				ADD `larg_w` INT( 5 ) NOT NULL DEFAULT '0',
				ADD `larg_h` INT( 5 ) NOT NULL DEFAULT '0',
				ADD `comp_w` INT( 5 ) NOT NULL DEFAULT '0',
				ADD `comp_h` INT( 5 ) NOT NULL DEFAULT '0',
				ADD `orig_w` INT( 5 ) NOT NULL DEFAULT '0',
				ADD `orig_h` INT( 5 ) NOT NULL DEFAULT '0';";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Adding columns for image dimensions to the images table.";		
		
		$sql = "INSERT INTO `".PIX_TABLE_SETT."` VALUES ('db_update_230', '0')";

		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Adding flag for database upgrade prompt.";		
		
		// Go to the next upgrade method
		$this->upgrade_2_3_0();
		
	}
	
	/*
	*
	*	Upgrade 2.3.0 to current release
	*
	*/
	function upgrade_2_3_0 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.3.1' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1;";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.3.1.";
		
		$sql = "ALTER TABLE `".PIX_TABLE_CRME."` ADD `item_prod_code` VARCHAR( 50 ) NOT NULL ;";
		
		$this->_dbl->sqlQuery($sql);
		
		$sql = "ALTER TABLE `".PIX_TABLE_CRME."` ADD `item_dnl_size` VARCHAR( 50 ) NOT NULL ;";
		
		$this->_dbl->sqlQuery($sql);

		// Go to the next upgrade method
		$this->upgrade_2_3_1();
		
	}
	
	/*
	*
	*	Upgrade 2.3.1 to current release
	*
	*/
	function upgrade_2_3_1 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.3.2' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1;";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.3.2.";
		
		$sql = "ALTER TABLE `".PIX_TABLE_IMGS."`
				ADD `iptc_creator` VARCHAR( 32 ) NULL ,
				ADD `iptc_jobtitle` VARCHAR( 32 ) NULL ,
				ADD `iptc_city` VARCHAR( 32 ) NULL ,
				ADD `iptc_country` VARCHAR( 64 ) NULL ,
				ADD `iptc_credit` VARCHAR( 32 ) NULL ,
				ADD `iptc_source` VARCHAR( 32 ) NULL ,
				ADD `iptc_object` VARCHAR( 64 ) NULL ,
				ADD `iptc_byline` VARCHAR( 32 ) NULL ";
		
		$this->_dbl->sqlQuery($sql);

		$sql = "INSERT INTO `".PIX_TABLE_SETT."` VALUES ('default_thumb_size', 'both')";

		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Adding setting to control thumbnail size.";		
		
		// Go to the next upgrade method
		$this->upgrade_2_3_2();
		
	}
	
	/*
	*
	*	Upgrade 2.3.2 to current release
	*
	*/
	function upgrade_2_3_2 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.3.3' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1;";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.3.3.";
				
		// Go to the next upgrade method
		$this->upgrade_2_3_3();
		
	}
	
	/*
	*
	*	Upgrade 2.3.3 to current release
	*
	*/
	function upgrade_2_3_3 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.3.4' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.3.4.";
				
		$sql = "ALTER TABLE `".PIX_TABLE_GALL."` ADD `gallery_archived` INT( 1 ) NOT NULL DEFAULT '0' AFTER `gallery_active` ";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Added new column to galleries table to manage archiving.";
				
		$sql = "INSERT INTO `".PIX_TABLE_SETT."` VALUES ('filter_gallery_active', '0')";

		$this->_dbl->sqlQuery($sql);
		
		$sql = "INSERT INTO `".PIX_TABLE_SETT."` VALUES ('filter_gallery_archived', '0')";

		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Adding setting storage for gallery view filters.";		
		
		// Go to the next upgrade method
		$this->upgrade_2_3_4();
		
	}
	
	/*
	*
	*	Upgrade 2.3.4 to current release
	*
	*/
	function upgrade_2_3_4 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.3.5' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1;";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.3.5.";
				
		// Go to the next upgrade method
		$this->upgrade_2_3_5();
		
	}
	
	/*
	*
	*	Upgrade 2.3.5 to current release
	*
	*/
	function upgrade_2_3_5 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.5.0' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1;";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.5.0.";
				
		$sql = "INSERT INTO ".PIX_TABLE_SETT." VALUES ('local_language', 'en_GB');";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Added settings to control the interface language.";
						
		// Go to the next upgrade method
		$this->upgrade_2_5_0();
		
	}
	
	/*
	*
	*	Upgrade 2.5.0 to current release
	*
	*/
	function upgrade_2_5_0 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.5.1' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.5.1.";
						
		// Go to the next upgrade method
		$this->upgrade_2_5_1();
		
	}
	
	/*
	*
	*	Upgrade 2.5.1 to current release
	*
	*/
	function upgrade_2_5_1 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.5.2' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.5.2.";
				
		// Go to the next upgrade method
		$this->upgrade_2_5_2();
		
	}
	
	/*
	*
	*	Upgrade 2.5.2 to current release
	*
	*/
	function upgrade_2_5_2 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.5.3' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.5.3.";
				
		// Go to the next upgrade method
		$this->upgrade_2_5_3();
		
	}
	
	/*
	*
	*	Upgrade 2.5.3 to current release
	*
	*/
	function upgrade_2_5_3 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.5.4' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.5.4.";
				
		// Go to the next upgrade method
		$this->upgrade_2_5_4();
		
	}
		
	/*
	*
	*	Upgrade 2.5.4 to current release
	*
	*/
	function upgrade_2_5_4 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.5.5' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.5.5.";
				
		/*
		*	Insert new setting for slideshow
		*/
		$sql = "INSERT INTO ".PIX_TABLE_SETT." ( `name` , `value` ) VALUES ('func_simple_slideshow', '1')";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "New settings for slideshow.";
				
		// Go to the next upgrade method
		$this->upgrade_2_5_5();
		
	}
		
	/*
	*
	*	Upgrade 2.5.5 to current release
	*
	*/
	function upgrade_2_5_5 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.5.6' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.5.6.";
				
		// Go to the next upgrade method
		$this->upgrade_2_5_6();
		
	}
		
	/*
	*
	*	Upgrade 2.5.6 to 2.6.0
	*
	*/
	function upgrade_2_5_6 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Fixing multiple slideshow settings bug
		*/
		$sql = "DELETE FROM ".PIX_TABLE_SETT." WHERE name = 'func_simple_slideshow'";
		$this->_dbl->sqlQuery($sql);
		
		/*
		*	Insert new setting for slideshow
		*/
		$sql = "INSERT INTO ".PIX_TABLE_SETT." ( `name` , `value` ) VALUES ('func_simple_slideshow', '1')";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Fixing settings for slideshow.";
		
		/*
		*	Add storage for small comp image dimensions
		*/
		$sql = "ALTER TABLE `".PIX_TABLE_IMGS."` ADD `scom_w` INT( 5 ) DEFAULT '0' NOT NULL AFTER `larg_h`,
				ADD `scom_h` INT( 5 ) DEFAULT '0' NOT NULL AFTER `scom_w`";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Adding storage for small comp image dimensions to images table.";
		
		/*
		*	Add GPS Geo-tagging storage
		*/
		$sql = "ALTER TABLE `".PIX_TABLE_IMGS."` ADD `gps_longitude` DOUBLE( 15, 8 ) NOT NULL ,
				ADD `gps_latitude` DOUBLE( 15, 8 ) NOT NULL";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Adding storage for GPS coordinates to images table.";
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.6.0' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.6.0.";
				
		// Go to the next upgrade method
		$this->upgrade_2_6_0();
		
	}
		
	/*
	*
	*	Upgrade 2.6.0 to 2.6.1
	*
	*/
	function upgrade_2_6_0 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.6.1' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.6.1.";
				
		/*
		*	Settings
		*/
		$this->_dbl->sqlQuery("INSERT INTO `".PIX_TABLE_SETT."` ( `name` , `value` ) VALUES ('filename_noext', '0')");
		
		$this->database_report[] = "Adding new settings defaults.";
				
		// Go to the next upgrade method
		$this->upgrade_2_6_1();
				
	}
		
	/*
	*
	*	Upgrade 2.6.1 to 2.6.2
	*
	*/
	function upgrade_2_6_1 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.6.2' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.6.2.";
				
		// Go to the next upgrade method
		$this->upgrade_2_6_2();
				
	}
		
	/*
	*
	*	Upgrade 2.6.2 to 2.6.3
	*
	*/
	function upgrade_2_6_2 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.6.3' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.6.3.";
				
		// Go to the next upgrade method
		$this->upgrade_2_6_3();
				
	}
		
	/*
	*
	*	Upgrade 2.6.3 to 2.7.0
	*
	*/
	function upgrade_2_6_3 () {
	
		global $cfg;
		
		$this->database_modified = (bool)true;
		
		/*
		*	Change database version
		*/
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.7.0' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.7.0.";
		
		$sql = "INSERT INTO `".PIX_TABLE_SETT."` ( `name` , `value` ) VALUES ('api_google_maps', ''), ('func_mapping', '1')";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Adding settings fields for GPS mapping and Google Maps API.";
		
		$sql = "ALTER TABLE `".PIX_TABLE_GALL."` ADD `gallery_rss` INT( 1 ) DEFAULT '1' NOT NULL";
		$this->_dbl->sqlQuery($sql);
		
		$sql = "INSERT INTO `".PIX_TABLE_SETT."` ( `name` , `value` )
				
				VALUES 	('user_default_priv_photographer', '0'),
						('user_default_priv_imgedit', '0'),
						('user_default_priv_imgdelete', '0'),
						('user_default_priv_hires', '0')";
		
		$this->database_report[] = "Adding column to gallery table to store RSS setting.";
		
		// Create preference type table
		$sql = "CREATE TABLE `".PIX_TABLE_PREF."` (
				  `preference_id` int(11) NOT NULL auto_increment,
				  `name` varchar(50) NOT NULL,
				  `type` varchar(10) NOT NULL,
				  `default_value` TEXT,
				  PRIMARY KEY  (`preference_id`)
				) ENGINE=MyISAM";
		$this->_dbl->sqlQuery($sql);
		
		// Create preference types
		$sql = "INSERT INTO `".PIX_TABLE_PREF."` ( `preference_id` , `name` , `type` , `default_value` ) VALUES
				('0', 'import_image_active', 'integer', '1'),
				('0', 'import_image_title', 'string', ''),
				('0', 'import_image_caption', 'string', ''),
				('0', 'import_image_keywords', 'string', ''),
				('0', 'import_image_userid', 'integer', ''),
				('0', 'import_image_copyright', 'string', ''),
				('0', 'import_image_date', 'string', ''),
				('0', 'import_image_time', 'string', ''),
				('0', 'import_image_sale', 'integer', ''),
				('0', 'import_image_price', 'string', ''),
				('0', 'import_image_product_link', 'integer', ''),
				('0', 'import_image_rights_type', 'integer', ''),
				('0', 'import_image_rights_text', 'string', ''),
				('0', 'import_image_model_release', 'integer', ''),
				('0', 'import_image_property_release', 'integer', ''),
				('0', 'import_image_permissions', 'integer', ''),
				('0', 'import_image_viewers', 'array', ''),
				('0', 'import_image_extra_01', 'string', ''),
				('0', 'import_image_extra_02', 'string', ''),
				('0', 'import_image_extra_03', 'string', ''),
				('0', 'import_image_extra_04', 'string', ''),
				('0', 'import_image_extra_05', 'string', ''),
				('0', 'import_image_iptc_charset', 'string', 'ISO-8859-1'),
				('0', 'import_image_products', 'array', '')";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Adding table for user specific preferences.";
		
		// Create table for user preferences
		$sql = "CREATE TABLE `".PIX_TABLE_UPRF."` (
				  `user_preference_id` int(10) NOT NULL auto_increment,
				  `preference_id` int(10) NOT NULL,
				  `user_id` int(10) NOT NULL,
				  `value` varchar(255) NOT NULL,
				  PRIMARY KEY  (`user_preference_id`)
				)";
		$this->_dbl->sqlQuery($sql);
		
		// Create table for character sets
		$this->database_report[] = "Adding table to store user preferences in.";
		
		$sql = "CREATE TABLE `".PIX_TABLE_CHAR."` (
				`charset_id` INT NOT NULL AUTO_INCREMENT ,
				`name` VARCHAR( 50 ) NOT NULL ,
				`code` VARCHAR( 50 ) NOT NULL ,
				PRIMARY KEY ( `charset_id` )
				) TYPE = MYISAM ";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Adding lookup table for character sets.";
		
		// Adding list of IPTC conversion character sets
		$sql = "INSERT INTO `psg_charset` ( `charset_id` , `name` , `code` ) VALUES
				('0', 'Western Latin (ISO 8859-1)', 'ISO-8859-1'),
				('0', 'Western (Windows-1252)', 'Windows-1252'),
				('0', 'Central European Latin 2 (ISO 8859-2)', 'ISO-8859-2'),
				('0', 'Central European (Windows-1250)', 'Windows-1250'),
				('0', 'South European (ISO 8859-3)', 'ISO-8859-3'),
				('0', 'North European (ISO 8859-4)', 'ISO-8859-4'),
				('0', 'Cyrillic (ISO 8859-5)', 'ISO-8859-5'),
				('0', 'Cyrillic (Windows-1251)', 'Windows-1251'),
				('0', 'Arabic (ISO 8859-6)', 'ISO-8859-6'),
				('0', 'Arabic (Windows-1256)', 'Windows-1256'),
				('0', 'Greek (ISO 8859-7)', 'ISO-8859-7'),
				('0', 'Greek (Windows-1253)', 'Windows-1253'),
				('0', 'Hebrew (ISO 8859-8)', 'ISO-8859-8'),
				('0', 'Hebrew (Windows-1255)', 'Windows-1255'),
				('0', 'Turkish (ISO 8859-9)', 'ISO-8859-9'),
				('0', 'Turkish (Windows-1254)', 'Windows-1254'),
				('0', 'Baltic (Windows-1257)', 'Windows-1257'),
				('0', 'Vietnamese (Windows-1258)', 'Windows-1258'),
				('0', 'Thai (Windows-874)', 'Windows-874')";
				
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Adding list of IPTC conversion character sets.";
		
		// Create a new setting record for whether Divestock integration is turned on
		$sql = "INSERT INTO `".PIX_TABLE_SETT."` ( `name` , `value` ) VALUES ('func_divestock', '0')";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Adding setting for Divestock integration status.";
		
		// Create table to store Divestock galleries
		$sql = "CREATE TABLE `".PIX_TABLE_DGAL."` (
				`divestock_gallery_id` INT NOT NULL AUTO_INCREMENT ,
				`gallery_id` INT NOT NULL ,
				PRIMARY KEY ( `divestock_gallery_id` )
				) TYPE = MYISAM ";
				
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Create a new table to hold galleries pubished to Divestock.";
		
		$sql = "INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('search_opt_wildcard_keywords', '0')";
		
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Create new setting for wildcard keyword support.";
		
		// Go to the next upgrade method
		$this->upgrade_2_7_0();
				
	}
	
	/*
	*
	*	Upgrade 2.7.0 to 2.7.1
	*
	*/
	function upgrade_2_7_0 () {
		
		$this->database_modified = (bool)true;
		
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.7.1' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.7.1.";
		
		$sql = "INSERT INTO `".PIX_TABLE_SETT."` ( `name` , `value` ) VALUES ('func_user_language_select', '0')";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Add default setting for user language choice.";
		
		$sql = "INSERT INTO `".PIX_TABLE_SETT."` ( `name` , `value` ) VALUES ('optional_languages', '')";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Add default setting for user optional languages.";
		
		$sql = "ALTER TABLE `".PIX_TABLE_SETT."` ADD `type` VARCHAR( 20 ) NOT NULL";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Add type column for settings table.";
		
		$sql = "UPDATE `".PIX_TABLE_SETT."` SET `type` = 'array' WHERE name = 'optional_languages'";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Set optional_languages setting type to array.";
		
		$sql = "UPDATE `".PIX_TABLE_SETT."` SET `type` = 'string' WHERE name != 'optional_languages'";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Set type to string for all non array settings.";
		
		$sql = "INSERT INTO `".PIX_TABLE_SETT."` ( `name` , `value` , `type` ) VALUES ('installed_languages', '', 'array')";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Add setting row to hold list of installed languages.";
		
		$sql = "ALTER TABLE `".PIX_TABLE_IMGS."` CHANGE `image_path` `image_path` VARCHAR( 255 ) NULL DEFAULT NULL";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Change length of image path.";
		
		// Go to the next upgrade method
		$this->upgrade_2_7_1();
				
	}
	
	/*
	*
	*	Upgrade 2.7.1 to 2.7.2
	*
	*/
	function upgrade_2_7_1 () {
		
		$this->database_modified = (bool)true;
		
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.7.2' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.7.2.";
		
		$sql = "INSERT INTO `psg_preference` ( `preference_id` , `name` , `type` , `default_value` ) VALUES ('', 'admin_navigation_style', 'string', 'icons')";
		
		$this->database_report[] = "Added new personal preference options for admin navigation menu style.";
		
		// Go to the next upgrade method
		$this->upgrade_2_7_2();
				
	}
	
	/*
	*
	*	Upgrade 2.7.2 to 2.7.3
	*
	*/
	function upgrade_2_7_2 () {
		
		$this->database_modified = (bool)true;
		
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.7.3' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.7.3.";
				
		// Go to the next upgrade method
		$this->upgrade_2_7_3();
				
	}
	
	/*
	*
	*	Upgrade 2.7.3 to 2.7.4
	*
	*/
	function upgrade_2_7_3 () {
		
		$this->database_modified = (bool)true;
		
		$sql = "UPDATE ".PIX_TABLE_SETT." SET `value` = '2.7.4' WHERE ".PIX_TABLE_SETT.".`name` = 'database_version' LIMIT 1";
		$this->_dbl->sqlQuery($sql);
		
		$this->database_report[] = "Database version number updated to 2.7.4.";
				
	}
	
}

?>


