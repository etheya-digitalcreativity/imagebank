<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

/*
*
*	admin.library.import.php
*
*	This page controls all the administration functions for adding,
*	editing and deleting image galleries in Pixaria Gallery.
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "images";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Check that the user is a SuperUser
pix_authorise_user("administrator");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Define the navigation section for the admin toolbar
$smarty->assign("toolbar","library");

$objAdminLibraryImport = new Admin_Import();

/*
*
*	
*
*/
class Admin_Import {

	/*
	*
	*	
	*
	*/
	function Admin_Import() {
		
		global $objEnvData, $cfg;
		
		// Catch safe mode problems
		$this->checkSafeMode();
	
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		switch ($objEnvData->fetchGlobal('cmd')) {
			
			case "formDeleteImageFiles":
				$this->formDeleteImageFiles();
			break;
			
			case "confirmDeleteFromIncoming":
				$this->confirmDeleteFromIncoming();
			break;
			
			case "directoryFromIncoming":
				$this->directoryFromIncoming();
			break;
			
			case "checkAllImagesPresent":
				$this->checkAllImagesPresent();
			break;
			
			case "addImageMetaData":
				$this->addImageMetaData();
			break;
			
			case "approveImageMetaData":
				$this->approveImageMetaData();
			break;
			
			case "importImages":
				$this->importImages();
			break;
			
			case "processImportedImages":
				$this->processImportedImages();
			break;
			
			case "deleteCompleteDirectory":
				$this->deleteCompleteDirectory();
			break;
			
			case "processArchive":
				$this->processArchive();
			break;
			
			case "processJpeg":
				$this->processJpeg();
			break;
			
			case "showUploadTypeChooser":
				$this->showUploadTypeChooser();
			break;
			
			case "showArchiveUploader":
				$this->showArchiveUploader();
			break;
			
			case "showJpegUploader":
				$this->showJpegUploader();
			break;
			
			case "showJavaUploader":
				$this->showJavaUploader();
			break;
			
			case "importFromIncoming":
				$this->importFromIncoming();
			break;
			
			case "importDirectories":
				$this->importDirectories();
			break;
			
			case "importUploaded":
				$this->importUploaded();
			break;
			
			case "viewImageMetaData":
				$this->viewImageMetaData();
			break;
			
			case "showFormUploadImages":
				$this->showFormUploadImages();
			break;
			
			case "showFormImportImages": default:
				$this->showFormImportImages();
			break;
			
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function deleteCompleteDirectory () {
	
		global $smarty, $objEnvData, $cfg;
		
		$directory 	= realpath($cfg['sys']['base_incoming'] . $objEnvData->fetchGlobal('directory'));
		$incoming	= realpath($cfg['sys']['base_incoming']);
		
		
		// Check we're in the right directory and that we're not going to delete the incoming directory
		if ($directory != $incoming && eregi($incoming,$directory)) {
			
			// Delete the directory
			$this->deleteDirectory($directory);
		
		}

		$meta_url	= $_SERVER['HTTP_REFERER'];
				
		// Print out a holding page telling the user
		// that their request is being processed
		$smarty->pixWaiting($meta_url,"1");
	
	}
	
	/*
	*
	*	
	*
	*/
	function processImportedImages () {
		
		global $smarty, $objEnvData, $cfg;
		
		switch ($objEnvData->fetchGlobal('action')) {
		
			case 1:
					
				$meta_url	= $cfg['sys']['base_url'] . $cfg['fil']['admin_gallery'] . "?cmd=addImagesToGallery&amp;gallery_id=" . $objEnvData->fetchGlobal('gallery_id') . "&amp;image_path=" . $objEnvData->fetchGlobal('image_path');
				
				// Print out a holding page telling the user
				// that their request is being processed
				$smarty->pixWaiting($meta_url,"1");
					
			break;
			
			case 2:
					
				$meta_url	= $cfg['sys']['base_url'] . $cfg['fil']['admin_gallery'] . "?image_path=" . $objEnvData->fetchGlobal('image_path');
				
				// Print out a holding page telling the user
				// that their request is being processed
				$smarty->pixWaiting($meta_url,"1");
					
			break;
			
			case 3:
					
				$this->addImagesToLibrary();
					
			break;
		
		}
	
	
	}
	
	/*
	*
	*	
	*
	*/
	function viewImageMetaData () {
	
		global $objEnvData, $cfg, $smarty, $ses;
		
		$filepath = base64_decode($objEnvData->fetchGlobal('filepath'));
		
		require_once ('class.Library.MetaData.php');
		
		$objMetaData = new PixariaMetaData();
		
		if (file_exists($filepath)) {
		
			$image_size[] 	= @getimagesize($filepath);
			$image_exif		= $objMetaData->extractEXIFMetaData($filepath);
			$image_iptc		= $objMetaData->extractIPTCMetaData($filepath);
			
		}
		
		// First off, assume no metadata is present (it will change to true if any is found)
		$smarty->assign("image_metadata",(bool)false);
		
		if (is_array($image_exif['exif_data']['FILE'])) {
			$smarty->assign("exif_file",(bool)true);
			$smarty->assign("exif_file_keys",array_keys($image_exif['exif_data']['FILE']));
			$smarty->assign("exif_file_values",array_values($image_exif['exif_data']['FILE']));
			$smarty->assign("image_metadata",(bool)true);
		}
		
		if (is_array($image_exif['exif_data']['COMPUTED'])) {
			$smarty->assign("exif_computed",(bool)true);
			$smarty->assign("exif_computed_keys",array_keys($image_exif['exif_data']['COMPUTED']));
			$smarty->assign("exif_computed_values",array_values($image_exif['exif_data']['COMPUTED']));
			$smarty->assign("image_metadata",(bool)true);
		}
		
		if (is_array($image_exif['exif_data']['IFD0'])) {
			$smarty->assign("exif_ifd0",(bool)true);
			$smarty->assign("exif_ifd0_keys",array_keys($image_exif['exif_data']['IFD0']));
			$smarty->assign("exif_ifd0_values",array_values($image_exif['exif_data']['IFD0']));
			$smarty->assign("image_metadata",(bool)true);
		}
		/*
		if (is_array($image_exif['exif_data']['THUMBNAIL'])) {
			$smarty->assign("exif_thumbnail",(bool)true);
			$smarty->assign("exif_thumbnail_data",base64_encode($image_exif['exif_data']['THUMBNAIL']));
			$smarty->assign("image_metadata",(bool)true);
		}
		*/
		if (is_array($image_exif['exif_data']['EXIF'])) {
			$smarty->assign("exif_exif",(bool)true);
			$smarty->assign("exif_exif_keys",array_keys($image_exif['exif_data']['EXIF']));
			$smarty->assign("exif_exif_values",array_values($image_exif['exif_data']['EXIF']));
			$smarty->assign("image_metadata",(bool)true);
		}
		
		if (is_array($image_iptc) && count($image_iptc) > 1) {
			$smarty->assign("iptc_raw",(bool)true);
			$smarty->assign("image_iptc",$image_iptc);
			$smarty->assign("image_metadata",(bool)true);
		}
		
		$smarty->pixDisplay('admin.image/image.metadata.tpl');
		
		exit;
	
	}
	
	/*
	*
	*	
	*
	*/
	function importImages () {
		
		global $objEnvData, $cfg, $smarty, $ses;
		
		require_once ('class.PixariaImage.php');

		// Load variables
		$image_path				= $objEnvData->fetchPost('directory');
		$image_permissions		= stripslashes($objEnvData->fetchPost('image_permissions'));			
		$image_product_link		= stripslashes($objEnvData->fetchPost('image_product_link'));
		$image_products			= $objEnvData->fetchPost('image_products');
		$image_groups			= $objEnvData->fetchPost('image_groups');
		
		$image_filename 		= $objEnvData->fetchPost('image_filename');
		$image_width 			= $objEnvData->fetchPost('image_width');
		$image_height 			= $objEnvData->fetchPost('image_height');
		$image_active 			= $objEnvData->fetchPost('image_active');
		$image_title 			= $objEnvData->fetchPost('image_title');
		$image_caption 			= $objEnvData->fetchPost('image_caption');
		$image_copyright 		= $objEnvData->fetchPost('image_copyright');
		$image_keywords 		= $objEnvData->fetchPost('image_keywords');
		$image_sale 			= $objEnvData->fetchPost('image_sale');
		$image_price			= $objEnvData->fetchPost('image_price');
		$image_rights_type 		= $objEnvData->fetchPost('image_rights_type');
		$image_rights_text 		= $objEnvData->fetchPost('image_rights_text');
		$image_extra_01 		= $objEnvData->fetchPost('image_extra_01');
		$image_extra_02 		= $objEnvData->fetchPost('image_extra_02');
		$image_extra_03 		= $objEnvData->fetchPost('image_extra_03');
		$image_extra_04 		= $objEnvData->fetchPost('image_extra_04');
		$image_extra_05 		= $objEnvData->fetchPost('image_extra_05');
		$date_custom			= $objEnvData->fetchPost('date_custom');
		$image_import			= $objEnvData->fetchPost('image_import');
		$image_model_release	= $objEnvData->fetchPost('model_release');
		$image_property_release	= $objEnvData->fetchPost('property_release');
		$iptc_creator			= $objEnvData->fetchPost('iptc_creator');
		$iptc_jobtitle			= $objEnvData->fetchPost('iptc_jobtitle');
		$iptc_city				= $objEnvData->fetchPost('iptc_city');
		$iptc_country			= $objEnvData->fetchPost('iptc_country');
		$iptc_credit			= $objEnvData->fetchPost('iptc_credit');
		$iptc_source			= $objEnvData->fetchPost('iptc_source');
		$iptc_object			= $objEnvData->fetchPost('iptc_object');
		$iptc_byline			= $objEnvData->fetchPost('iptc_byline');
		
		// The total number of files to import
		$total_images 	= $objEnvData->fetchPost('total_images');
		$total_selected = count($objEnvData->fetchPost('image_import'));
		
		// Set up a variable to catch any failures
		$partial_failure = (bool)false;
		
		// If there are more files than the limit, then the total number
		// of files to be imported in this step is the same as import limit
		if (($total_images > $cfg['set']['import_limit']) || ($total_selected < $total_images)) {
		
			$this_import	= $cfg['set']['import_limit'];

			// Load global vars back into Smarty for the next round		
			$smarty->assign("more_to_do",(bool)true);
			$smarty->assign("directory",$objEnvData->fetchPost('directory'));
			$smarty->assign("image_active",$objEnvData->fetchPost('global_image_active'));
			$smarty->assign("image_title",$objEnvData->fetchPost('global_image_title'));
			$smarty->assign("image_caption",$objEnvData->fetchPost('global_image_caption'));
			$smarty->assign("image_keywords",$objEnvData->fetchPost('global_image_keywords'));
			$smarty->assign("image_keywords_append",$objEnvData->fetchPost('global_image_keywords_append'));
			$smarty->assign("mysql_format_time",$objEnvData->fetchPost('global_mysql_format_time'));
			$smarty->assign("date_option",$objEnvData->fetchPost('global_date_option'));
			$smarty->assign("toda_Month",$objEnvData->fetchPost('global_toda_Month'));
			$smarty->assign("toda_Day",$objEnvData->fetchPost('global_toda_Day'));
			$smarty->assign("toda_Year",$objEnvData->fetchPost('global_toda_Year'));
			$smarty->assign("image_copyright",$objEnvData->fetchPost('global_image_copyright'));
			$smarty->assign("image_price",$objEnvData->fetchPost('global_image_price'));
			$smarty->assign("image_sale",$objEnvData->fetchPost('global_image_sale'));
			$smarty->assign("image_product_link",$objEnvData->fetchPost('global_image_product_link'));
			$smarty->assign("image_products",$objEnvData->fetchPost('global_image_products'));
			$smarty->assign("image_permissions",$objEnvData->fetchPost('global_image_permissions'));
			$smarty->assign("image_groups",$objEnvData->fetchPost('global_image_groups'));
			$smarty->assign("image_rights_type",$objEnvData->fetchPost('global_image_rights_type'));
			$smarty->assign("image_rights_text",$objEnvData->fetchPost('global_image_rights_text'));
			$smarty->assign("image_model_release",$objEnvData->fetchPost('global_image_model_release'));
			$smarty->assign("image_property_release",$objEnvData->fetchPost('global_image_property_release'));
			$smarty->assign("image_extra_01",$objEnvData->fetchPost('global_image_extra_01'));
			$smarty->assign("image_extra_02",$objEnvData->fetchPost('global_image_extra_02'));
			$smarty->assign("image_extra_03",$objEnvData->fetchPost('global_image_extra_03'));
			$smarty->assign("image_extra_04",$objEnvData->fetchPost('global_image_extra_04'));
			$smarty->assign("image_extra_05",$objEnvData->fetchPost('global_image_extra_05'));
			
		} else { // The import limit is the same as the total number of images selected to be imported
		
			$this_import 	= $total_selected;
			$more_to_do		= (bool)false;
			
		}
		
		// Loop through each image and import it into the database
		foreach ($image_filename as $key => $value) {
			
			$dkey = $key . "Day";
			$mkey = $key . "Month";
			$ykey = $key . "Year";
			$hkey = $key . "Hour";
			$nkey = $key . "Minute";
			$skey = $key . "Second";
			
			//
			if ($key <= ($cfg['set']['import_limit'] - 1)) {
				
				if ($image_import[$key] == "on") {
				
					// Instantiate the image class
					$objImage = new PixariaImage();
					
					$objImage->setImageDate($date_custom[$ykey],$date_custom[$mkey],$date_custom[$dkey],$date_custom[$hkey],$date_custom[$nkey],$date_custom[$skey]);
						
					list($icon_w, $icon_h) = @getimagesize($cfg['sys']['base_incoming'] . $image_path . "/32x32/" . $image_filename[$key]);
					list($smal_w, $smal_h) = @getimagesize($cfg['sys']['base_incoming'] . $image_path . "/80x80/" . $image_filename[$key]);
					list($larg_w, $larg_h) = @getimagesize($cfg['sys']['base_incoming'] . $image_path . "/160x160/" . $image_filename[$key]);
					list($comp_w, $comp_h) = @getimagesize($cfg['sys']['base_incoming'] . $image_path . "/".COMPING_DIR."/" . $image_filename[$key]);
					list($orig_w, $orig_h) = @getimagesize($cfg['sys']['base_incoming'] . $image_path . "/original/" . $image_filename[$key]);

					$objImage->setImageActive($image_active[$key]);
					$objImage->setImagePath($image_path);
					$objImage->setImageUserId($ses['psg_userid']);
					$objImage->setImageFileName($image_filename[$key]);
					$objImage->setImageTitle($image_title[$key]);
					$objImage->setImageCaption($image_caption[$key]);
					$objImage->setImageKeywords($image_keywords[$key]);
					$objImage->setImageWidth($image_width[$key]);
					$objImage->setImageHeight($image_height[$key]);
					$objImage->setImageCopyright($image_copyright[$key]);
					$objImage->setImagePermissions($image_permissions);
					$objImage->setImageRightsType($image_rights_type[$key]);
					$objImage->setImageRightsText($image_rights_text[$key]);
					$objImage->setImagePrice($image_price[$key]);
					$objImage->setImageSale($image_sale[$key]);
					$objImage->setImageProductLink($image_product_link);
					$objImage->setImageProducts($image_products);
					$objImage->setImageModelRelease($image_model_release[$key]);
					$objImage->setImagePropertyRelease($image_property_release[$key]);
					$objImage->setImageExtra1($image_extra_01[$key]);
					$objImage->setImageExtra2($image_extra_02[$key]);
					$objImage->setImageExtra3($image_extra_03[$key]);
					$objImage->setImageExtra4($image_extra_04[$key]);
					$objImage->setImageExtra5($image_extra_05[$key]);
					$objImage->setImageViewers($image_groups);
					$objImage->setImageModelRelease($image_model_release[$key]);
					$objImage->setImagePropertyRelease($image_property_release[$key]);
					
					$objImage->setIconWidth($icon_w);
					$objImage->setIconHeight($icon_h);
					$objImage->setSmallWidth($smal_w);
					$objImage->setSmallHeight($smal_h);
					$objImage->setLargeWidth($larg_w);
					$objImage->setLargeHeight($larg_h);
					$objImage->setCompingWidth($comp_w);
					$objImage->setCompingHeight($comp_h);
					$objImage->setOriginalWidth($orig_w);
					$objImage->setOriginalHeight($orig_h);
					
					$objImage->setIPTCCreator($iptc_creator[$key]);
					$objImage->setIPTCJobtitle($iptc_jobtitle[$key]);
					$objImage->setIPTCCity($iptc_city[$key]);
					$objImage->setIPTCCountry($iptc_country[$key]);
					$objImage->setIPTCCredit($iptc_credit[$key]);
					$objImage->setIPTCSource($iptc_source[$key]);
					$objImage->setIPTCObject($iptc_object[$key]);
					$objImage->setIPTCByline($iptc_byline[$key]);
					
					// Move the image file into the library directory
					$move_success = $this->moveImageFileToLibrary($image_filename[$key], $image_path);
					
					if ($move_success) {
						
						$objImage->createImage();
						
						// Compile an array of imported image IDs
						$images[] = $objImage->getImageId();
						
					} else {
						
						$partial_failure = (bool)true;
						$failed_images[] = $image_filename[$key];
					
					}

				}
			
			}
			
		}
		
		if (count($this_import) == count($failed_images)) {
			$complete_failure = (bool)true;
		}
		
		// Check if the import was a success
		if ($partial_failure != true && $complete_failure != true) {
			
			// Set the path to this import directory
			$dirpath = $cfg['sys']['base_incoming'] . $image_path;
			
			// If the directories are empty, delete them
			if (is_dir($dirpath . "/original/") && isEmptyFolder($dirpath . "/original/")) 	{ @rmdir($dirpath . "/original/"); }
			if (is_dir($dirpath . "/".COMPING_DIR."/") 	&& isEmptyFolder($dirpath . "/".COMPING_DIR."/")) 	{ @rmdir($dirpath . "/".COMPING_DIR."/"); }
			if (is_dir($dirpath . "/160x160/") 	&& isEmptyFolder($dirpath . "/160x160/")) 	{ @rmdir($dirpath . "/160x160/"); }
			if (is_dir($dirpath . "/80x80/") 	&& isEmptyFolder($dirpath . "/80x80/")) 	{ @rmdir($dirpath . "/80x80/"); }
			if (is_dir($dirpath . "/32x32/") 	&& isEmptyFolder($dirpath . "/32x32/")) 	{ @rmdir($dirpath . "/32x32/"); }
			if (is_dir($dirpath) 				&& isEmptyFolder($dirpath."/")) 			{ @rmdir($dirpath); }
		
		}
		
		$gallery_list = galleryListingArray();
			
		// Load the gallery list data into Smarty
		$smarty->assign("gallery_title",$gallery_list[0]);
		$smarty->assign("gallery_id",$gallery_list[1]);
		$smarty->assign("partial_failure",$partial_failure);
		$smarty->assign("complete_failure",$complete_failure);
		$smarty->assign("failed_images",$failed_images);
		
		// For the purposes of creating a new gallery with these images
		// or for adding them to an existing gallery, we need to know the
		// directory name where these images are being stored
		$smarty->assign("directory",$image_path);
		
		// Load image IDs of imported files into Smarty
		$smarty->assign("images",$images);
		
		// Set the page title
		$smarty->assign("page_title","Import process complete");
		
		// Load the HTML page
		$smarty->pixDisplay('admin.library/library.import.06.tpl');
	
	}
	
	/*
	*
	*	
	*
	*/
	function approveImageMetaData () {
		
		global $objEnvData, $cfg, $smarty;
		
		require_once ('class.Library.MetaData.php');
		
		$objMetaData = new PixariaMetaData();
		
		// Define the directory of images we're going to use
		$directory = $cfg['sys']['base_incoming']."/".$objEnvData->fetchGlobal('directory');
		
		$images = array();
		
		$handle=@opendir($directory."/".COMPING_DIR."/");
		
		if($handle != FALSE) {
		
			while (false!==($file = readdir($handle))) { 
			
				if (substr($file,0,1) != "." && !eregi("thumbs.db",$file)) { 
				  
					$images[] = $file;

				}
			
			}
			
		}
		
		@closedir($handle);
	
		if (is_array($images)) {
			
			// Pass the number of images to Smarty
			$smarty->assign("total_images",count($images));
			
			// Sort the list alphabetically
			natsort($images);
		
			$icon_path			= array();
			$icon_width			= array();
			$icon_height		= array();
			$file_name			= array();
			$upload_arr_o		= array();
			$upload_arr_c		= array();
			$upload_arr_l		= array();
			$upload_arr_s		= array();
			$upload_arr_i		= array();
			
			$all_present		= "TRUE";
			
			foreach ($images as $key => $value) {
				
				if ($upload_arr_i[]	= file_exists($directory."/32x32/".$value)) 	{ $icon_size	= getimagesize($directory."/80x80/".$value); }
				if ($upload_arr_l[]	= file_exists($directory."/160x160/".$value)) 	{ $large_size	= getimagesize($directory."/160x160/".$value); }
				if ($upload_arr_c[]	= file_exists($directory."/".COMPING_DIR."/".$value)) 	{ $comp_size 	= getimagesize($directory."/".COMPING_DIR."/".$value); }
				
				$icon_path[] 	= base64_encode($directory."/80x80/".$value);
				$large_path[] 	= base64_encode($directory."/160x160/".$value);
				$comp_path[] 	= base64_encode($directory."/".COMPING_DIR."/".$value);
				
				$icon_width[]	= $icon_size[0];
				$icon_height[]	= $icon_size[1];
				
				$large_width[]	= $large_size[0];
				$large_height[]	= $large_size[1];
				
				$comp_width[]	= $comp_size[0];
				$comp_height[]	= $comp_size[1];
				
				$file_name[]	= $value;
				
				if (!$upload_arr_c[] = file_exists($directory."/".COMPING_DIR."/".$value)) 	{ $all_present = "FALSE"; }
				if (!$upload_arr_l[] = file_exists($directory."/160x160/".$value)) 	{ $all_present = "FALSE"; }
				if (!$upload_arr_s[] = file_exists($directory."/80x80/".$value)) 	{ $all_present = "FALSE"; }
				if (!$upload_arr_i[] = file_exists($directory."/32x32/".$value)) 	{ $all_present = "FALSE"; }
				
				if (file_exists($directory."/original/".$value)) {
					
					$image_mpath[]	= base64_encode($directory."/original/".$value);
					$image_size[] 	= @getimagesize($directory."/original/".$value);
					
					$file_fullpath	= $directory."/original/".$value;
					$image_exif		= $objMetaData->extractEXIFMetaData($directory."/original/".$value);
					$image_iptc		= $objMetaData->extractIPTCMetaData($directory."/original/".$value);
					
				} elseif (file_exists($directory."/".COMPING_DIR."/".$value)) {
				
					$image_mpath[]	= base64_encode($directory."/".COMPING_DIR."/".$value);
					$image_size[] 	= @getimagesize($directory."/".COMPING_DIR."/".$value);
					
					$file_fullpath	= $directory."/".COMPING_DIR."/".$value;
					$image_exif		= $objMetaData->extractEXIFMetaData($directory."/".COMPING_DIR."/".$value);
					$image_iptc		= $objMetaData->extractIPTCMetaData($directory."/".COMPING_DIR."/".$value);
					
				}
				
				/*
				*	If user is merging keywords, make sure there are no duplicates
				*/
				if ($objEnvData->fetchGlobal('image_keywords_append') == "on" && trim($image_iptc['keywords']) != "") {
					
					// Get user and local keywords
					$keywords_iptc 		= explode(", ",trim($image_iptc['keywords']));
					$keywords_user 		= explode(", ",$objEnvData->fetchGlobal('image_keywords'));
					
					// Combine user keywords and IPTC keywords
					$keywords_comb		= array_merge($keywords_iptc, $keywords_user);

					// Remove duplicate keywords
					$keywords_comb 		= implode(", ",array_unique($keywords_comb));
			
					$iptc_keywords[] 	= $keywords_comb;
				
				
				} else {
				
					$iptc_keywords[] 	= $image_iptc['keywords'];
				
				}
				
				/*
				*	Load IPTC metadata
				*/
				$iptc_copyright[]	= $image_iptc['copyright'];
				$iptc_objname[]		= $image_iptc['objname'];
				$iptc_headline[]	= $image_iptc['headline'];
				$iptc_creator[]		= $image_iptc['byline'];
				$iptc_jobtitle[]	= $image_iptc['bltitle'];
				$iptc_city[]		= $image_iptc['city'];
				$iptc_country[]		= $image_iptc['country'];
				$iptc_credit[]		= $image_iptc['credit'];
				$iptc_source[]		= $image_iptc['source'];
				$iptc_byline[]		= $image_iptc['byline'];
				$iptc_object[]		= $image_iptc['objname'];
				
				if ($image_iptc['date'] != "") {

					$iptc_date_yy	= substr($image_iptc['date'],0,4);
					$iptc_date_mm	= substr($image_iptc['date'],4,2);
					$iptc_date_dd	= substr($image_iptc['date'],6,2);
					$iptc_date_hh	= 0;
					$iptc_date_mi	= 0;
					$iptc_date_ss	= 0;
					
				} else {
				
					$iptc_date_yy	= 0;
					$iptc_date_mm	= 0;
					$iptc_date_dd	= 0;
					$iptc_date_hh	= 0;
					$iptc_date_mi	= 0;
					$iptc_date_ss	= 0;
					
				}
				
				// Get the image date, IPTC takes precedence
				if ($iptc_date_yy != "0") {
					
					$image_datetime[] = "$iptc_date_yy-$iptc_date_mm-$iptc_date_dd 00:00:00";
					
				} elseif ($image_exif['timestamp'] != "") {

					$image_datetime[] = date("Y-m-d H:i:s",$image_exif['timestamp']);
				
				} else {
				
					$image_datetime[] = date("Y-m-d H:i:s",filemtime($file_fullpath));
					
				}
				
				$iptc_caption[] 	= $image_iptc['caption'];
				$image_filename[]	= $value;
				
			}
							
			// Get category names and ids from the selection chosen
			if (is_array($objEnvData->fetchGlobal('image_groups'))) {
			
				foreach($objEnvData->fetchGlobal('image_groups') as $key=>$value) {
				
					$result 			= $this->_dbl->sqlSelectRow("SELECT * FROM ".PIX_TABLE_GRPS." WHERE group_id = '$value'");
					$image_group_name[] = $result['group_name'];
					$image_group_id[] 	= $result['group_id'];
				
				}
			
			}
			
			// Assign group information to Smarty
			$smarty->assign("image_group_name",$image_group_name);
			$smarty->assign("image_group_id",$image_group_id);
			
			// Load array of image information into Smarty
			$smarty->assign("directory",aglobal(directory));
			$smarty->assign("temp_name",$temp_name);
			$smarty->assign("icon_path",$icon_path);
			$smarty->assign("icon_width",$icon_width);
			$smarty->assign("icon_height",$icon_height);
			$smarty->assign("large_path",$large_path);
			$smarty->assign("large_width",$large_width);
			$smarty->assign("large_height",$large_height);
			$smarty->assign("comp_path",$comp_path);
			$smarty->assign("comp_width",$comp_width);
			$smarty->assign("comp_height",$comp_height);
			$smarty->assign("file_name",$file_name);
			$smarty->assign("upload_arr_o",$upload_arr_o);
			$smarty->assign("upload_arr_c",$upload_arr_c);
			$smarty->assign("upload_arr_l",$upload_arr_l);
			$smarty->assign("upload_arr_s",$upload_arr_s);
			$smarty->assign("upload_arr_i",$upload_arr_i);
			$smarty->assign("image_color",$image_color);
			$smarty->assign("image_filename",$image_filename);
			$smarty->assign("image_size",$image_size);
			$smarty->assign("image_datetime",$image_datetime);
			$smarty->assign("image_mpath",$image_mpath);
			
			// Load IPTC info into Smarty
			$smarty->assign("iptc_keywords",$iptc_keywords);
			$smarty->assign("iptc_caption",$iptc_caption);
			$smarty->assign("iptc_copyright",$iptc_copyright);
			$smarty->assign("iptc_objname",$iptc_objname);
			$smarty->assign("iptc_headline",$iptc_headline);
			$smarty->assign("iptc_creator",$iptc_creator);
			$smarty->assign("iptc_jobtitle",$iptc_jobtitle);
			$smarty->assign("iptc_city",$iptc_city);
			$smarty->assign("iptc_country",$iptc_country);
			$smarty->assign("iptc_credit",$iptc_credit);
			$smarty->assign("iptc_source",$iptc_source);
			$smarty->assign("iptc_object",$iptc_object);
			$smarty->assign("iptc_byline",$iptc_byline);
			
			// Load non-text fields into Smarty
			$smarty->assign("date_option",$objEnvData->fetchGlobal('date_option'));
			$smarty->assign("mysql_format_time",$objEnvData->fetchGlobal('toda_Year')."-".$objEnvData->fetchGlobal('toda_Month')."-".$objEnvData->fetchGlobal('toda_Day')." 00:00:00");
			$smarty->assign("toda_Day",$objEnvData->fetchGlobal('toda_Day'));
			$smarty->assign("toda_Month",$objEnvData->fetchGlobal('toda_Month'));
			$smarty->assign("toda_Year",$objEnvData->fetchGlobal('toda_Year'));
			$smarty->assign("image_groups",$objEnvData->fetchGlobal('image_groups'));
			$smarty->assign("image_permissions",$objEnvData->fetchGlobal('image_permissions'));
			$smarty->assign("image_rights_type",$objEnvData->fetchGlobal('image_rights_type'));
			
			// Load data fields into Smarty
			$smarty->assign("image_active",stripslashes($objEnvData->fetchGlobal('image_active')));
			$smarty->assign("image_title",stripslashes($objEnvData->fetchGlobal('image_title')));
			$smarty->assign("image_headline",stripslashes($objEnvData->fetchGlobal('image_headline')));
			$smarty->assign("image_caption",stripslashes($objEnvData->fetchGlobal('image_caption')));
			$smarty->assign("image_keywords",stripslashes($objEnvData->fetchGlobal('image_keywords')));
			$smarty->assign("image_keywords_append",stripslashes($objEnvData->fetchGlobal('image_keywords_append')));
			$smarty->assign("image_sale",stripslashes($objEnvData->fetchGlobal('image_sale')));
			$smarty->assign("image_price",stripslashes($objEnvData->fetchGlobal('image_price')));
			$smarty->assign("image_product_link",stripslashes($objEnvData->fetchGlobal('image_product_link')));
			$smarty->assign("image_products",$objEnvData->fetchGlobal('image_products'));
			$smarty->assign("image_copyright",stripslashes($objEnvData->fetchGlobal('image_copyright')));
			$smarty->assign("image_rights_text",stripslashes($objEnvData->fetchGlobal('image_rights_text')));
			$smarty->assign("image_model_release",stripslashes($objEnvData->fetchGlobal('image_model_release')));
			$smarty->assign("image_property_release",stripslashes($objEnvData->fetchGlobal('image_property_release')));
			
			// Tell Smarty if any images are missing
			$smarty->assign("all_present",$all_present);
				
		}
		
		// Load custom data into Smarty
		$smarty->assign("image_extra_01",addslashes($objEnvData->fetchPost('image_extra_01')));				
		$smarty->assign("image_extra_02",addslashes($objEnvData->fetchPost('image_extra_02')));				
		$smarty->assign("image_extra_03",addslashes($objEnvData->fetchPost('image_extra_03')));				
		$smarty->assign("image_extra_04",addslashes($objEnvData->fetchPost('image_extra_04')));				
		$smarty->assign("image_extra_05",addslashes($objEnvData->fetchPost('image_extra_05')));				

		// Load the product class
		require_once('class.PixariaProduct.php');
		
		// Get all products in the system
		$objProducts = new PixariaProduct();
		
		// Get an array of all product data
		$product_data = $objProducts->getProducts();
		
		// Assign product information to Smarty
		$smarty->assign("product_id",$product_data['prod_id']);
		$smarty->assign("product_name",$product_data['prod_name']);
		
		// Set the page title
		$smarty->assign("page_title","Import images into library");
		
		// Load the HTML page
		$smarty->pixDisplay('admin.library/library.import.05.tpl');

	}
	
	/*
	*
	*	
	*
	*/
	function addImageMetaData () {
	
		global $objEnvData, $cfg, $smarty;
		
		// Define the directory of images we're going to use
		$directory = $cfg['sys']['base_incoming']."/".aglobal(directory);
		
		// Set the page title
		$smarty->assign("directory",aglobal(directory));
		
		// Load an array of the groups
		$group_list = groupListArray();
		
		// Load the category list data into Smarty
		$smarty->assign("group_name",$group_list[0]);
		$smarty->assign("group_id",$group_list[1]);
		
		// Load the product class
		require_once('class.PixariaProduct.php');
		
		// Get all products in the system
		$objProducts = new PixariaProduct();
		
		// Get an array of all product data
		$product_data = $objProducts->getProducts();
		
		// Assign product information to Smarty
		$smarty->assign("product_id",$product_data['prod_id']);
		$smarty->assign("product_name",$product_data['prod_name']);
		
		// Load global vars back into Smarty for the next round		
		$smarty->assign("directory",$objEnvData->fetchPost('directory'));
		$smarty->assign("image_active",$objEnvData->fetchPost('image_active'));
		$smarty->assign("image_title",$objEnvData->fetchPost('image_title'));
		$smarty->assign("image_caption",$objEnvData->fetchPost('image_caption'));
		$smarty->assign("image_keywords",$objEnvData->fetchPost('image_keywords'));
		$smarty->assign("mysql_format_time",$objEnvData->fetchPost('mysql_format_time'));
		$smarty->assign("date_option",$objEnvData->fetchPost('date_option'));
		$smarty->assign("toda_Month",$objEnvData->fetchPost('toda_Month'));
		$smarty->assign("toda_Day",$objEnvData->fetchPost('toda_Day'));
		$smarty->assign("toda_Year",$objEnvData->fetchPost('toda_Year'));
		$smarty->assign("image_copyright",$objEnvData->fetchPost('image_copyright'));
		$smarty->assign("image_price",$objEnvData->fetchPost('image_price'));
		$smarty->assign("image_sale",$objEnvData->fetchPost('image_sale'));
		$smarty->assign("image_product_link",$objEnvData->fetchPost('image_product_link'));
		$smarty->assign("image_products",$objEnvData->fetchPost('image_products'));
		$smarty->assign("image_permissions",$objEnvData->fetchPost('image_permissions'));
		$smarty->assign("image_groups",$objEnvData->fetchPost('image_groups'));
		$smarty->assign("image_rights_type",$objEnvData->fetchPost('image_rights_type'));
		$smarty->assign("image_rights_text",$objEnvData->fetchPost('image_rights_text'));
		$smarty->assign("image_model_release",$objEnvData->fetchPost('image_model_release'));
		$smarty->assign("image_property_release",$objEnvData->fetchPost('image_property_release'));
		$smarty->assign("image_extra_01",$objEnvData->fetchPost('image_extra_01'));
		$smarty->assign("image_extra_02",$objEnvData->fetchPost('image_extra_02'));
		$smarty->assign("image_extra_03",$objEnvData->fetchPost('image_extra_03'));
		$smarty->assign("image_extra_04",$objEnvData->fetchPost('image_extra_04'));
		$smarty->assign("image_extra_05",$objEnvData->fetchPost('image_extra_05'));
		
		// Set the page title
		$smarty->assign("page_title","Add image metadata");
		
		// Load the HTML page
		$smarty->pixDisplay('admin.library/library.import.04.tpl');
				
	}
	
	/*
	*
	*	This method checks that required images are present, creates thumbnails and puts images into the correct locations
	*
	*/
	function checkAllImagesPresent () {
		
		global $objEnvData, $cfg, $smarty;
		
		// Define the directory of images we're going to use
		$directory = $cfg['sys']['base_incoming']."/".$objEnvData->fetchGlobal('directory');
		
		// Check that PHP can read and write to this directory
		if (is_writable($directory)) { $permission_ok = "TRUE"; }

		// Check minimum required directories are present and writable
		if ($permission_ok && (is_dir($directory."/".COMPING_DIR."/") || is_dir($directory."/original/"))) { 
			
			// Initialise array
			$images = array();
			
			// Open directory for reading contents
			$handle=@opendir($directory."/".COMPING_DIR."/");
			
			// If the directory is accessible
			if($handle != FALSE) {
			
				while (false!==($file = readdir($handle))) { 
					
					if (strtolower(substr($file,-4,4)) == ".jpg") { 
					  	
					  	// Put image file names into array
						$images[] = $file;
	
					}
				
				}
				
			}
			
			// Close the directory
			@closedir($handle);
			
			if (is_array($images)) {
			
				// Sort the list alphabetically
				natsort($images);
			
				$icon_path			= array();
				$icon_width			= array();
				$icon_height		= array();
				$file_name			= array();
				$upload_arr_o		= array();
				$upload_arr_c		= array();
				$upload_arr_l		= array();
				$upload_arr_s		= array();
				$upload_arr_i		= array();
				
				// Assume all images are present and correct
				$all_present = "TRUE";
				
				// If comp directory is missing, make it
				if (!is_dir($directory."/".COMPING_DIR."/") && is_writable($directory)) {
					// Make a new directory for the comp images
					$directory_created	= $this->createDirectory($directory."/".COMPING_DIR."/");
				}
				
				// If large thumbnail directory is missing, make it
				if (!is_dir($directory."/160x160/") && is_writable($directory)) {
					// Make a new directory for the comp images
					$directory_created	= $this->createDirectory($directory."/160x160/");
				}
				
				// If large thumbnail directory is missing, make it
				if (!is_dir($directory."/80x80/") && is_writable($directory)) {
					// Make a new directory for the comp images
					$directory_created	= $this->createDirectory($directory."/80x80/");
						
				}
				
				// If large thumbnail directory is missing, make it
				if (!is_dir($directory."/32x32/") && is_writable($directory)) {
					// Make a new directory for the comp images
					$directory_created	= $this->createDirectory($directory."/32x32/");
				}
				
				/*
				*	Loop through the array of image file names
				*/
				foreach ($images as $key => $value) {
					
					// If no large thumbnail image exists, make one
					if (!file_exists($directory."/160x160/".$value) && file_exists($directory."/".COMPING_DIR."/".$value) &&  is_writable($directory."/160x160/")) {
						// Create large thumbnail image 160 x 160 and save
						$this->resizeImageFile($directory."/".COMPING_DIR."/".$value, $directory."/160x160/".$value,"160");
					}
					
					// If no small thumbnail image exists, make one
					if (!file_exists($directory."/80x80/".$value) && file_exists($directory."/160x160/".$value) &&  is_writable($directory."/80x80/")) {
						// Create small thumbnail image 80 x 80 and save
						$this->resizeImageFile($directory."/160x160/".$value, $directory."/80x80/".$value,"80");
					}
					
					// If no icon sized image exists, make one
					if (!file_exists($directory."/32x32/".$value) && file_exists($directory."/80x80/".$value) &&  is_writable($directory."/32x32/")) {
						// Create icon sized image 32 x 32 and save
						$this->resizeImageFile($directory."/80x80/".$value, $directory."/32x32/".$value,"32");
					}
					
					if ($upload_arr_i[]		= file_exists($directory."/32x32/".$value)) 	{ $icon_size = @getimagesize($directory."/32x32/".$value); }
					if ($upload_arr_c[]		= file_exists($directory."/".COMPING_DIR."/".$value)) 	{ $comp_size = @getimagesize($directory."/".COMPING_DIR."/".$value); }
					
					$icon_path[] 			= base64_encode($directory."/32x32/".$value);
					$comp_path[] 			= base64_encode($directory."/".COMPING_DIR."/".$value);
					
					$icon_width[]			= $icon_size[0];
					$icon_height[]			= $icon_size[1];
					
					$comp_width[]			= $comp_size[0];
					$comp_height[]			= $comp_size[1];
					
					$file_name[]			= $value;
					
					$upload_arr_o[] 		= file_exists($directory."/original/".$value);
					
					if (!$upload_arr_c[] 	= file_exists($directory."/".COMPING_DIR."/".$value)) 	{ $all_present = "FALSE"; }
					if (!$upload_arr_l[] 	= file_exists($directory."/160x160/".$value)) 	{ $all_present = "FALSE"; }
					if (!$upload_arr_s[] 	= file_exists($directory."/80x80/".$value)) 	{ $all_present = "FALSE"; }
					if (!$upload_arr_i[] 	= file_exists($directory."/32x32/".$value)) 	{ $all_present = "FALSE"; }
				
				}			
	
				// Load array of image information into Smarty
				$smarty->assign("directory",aglobal(directory));
				$smarty->assign("temp_name",$temp_name);
				$smarty->assign("icon_path",$icon_path);
				$smarty->assign("icon_width",$icon_width);
				$smarty->assign("icon_height",$icon_height);
				$smarty->assign("comp_path",$comp_path);
				$smarty->assign("comp_width",$comp_width);
				$smarty->assign("comp_height",$comp_height);
				$smarty->assign("file_name",$file_name);
				$smarty->assign("upload_arr_o",$upload_arr_o);
				$smarty->assign("upload_arr_c",$upload_arr_c);
				$smarty->assign("upload_arr_l",$upload_arr_l);
				$smarty->assign("upload_arr_s",$upload_arr_s);
				$smarty->assign("upload_arr_i",$upload_arr_i);
				
				// Tell Smarty if any images are missing
				$smarty->assign("all_present",$all_present);
				
			}
			
			// Set the page title
			$smarty->assign("page_title","Import directory of images");
		
			// Output the library page
			$smarty->pixDisplay('admin.library/library.import.03.tpl');		

		} else {
		
			// Set the page title
			$smarty->assign("page_title","File permissions problem");
			
			// Display the library import page
			$smarty->pixDisplay('admin.library/library.import.02.tpl');
		
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function directoryFromIncoming () {
	
		global $objEnvData, $cfg, $smarty;
		
		// Load the directory name
		$directory_name = $objEnvData->fetchPost('directory_name');
		
		// Replace disallowed characters with hyphen characters
		$directory_name = preg_replace("/[\s\\.\\,\\+\\(\\)\\{\\}\[\]\\~\\!\\$\\%\\^\\&\\*\\@\\#\\<\\>\\?\\=]/","-",$directory_name);

		if ($directory_name != "" && !eregi('[^a-zA-Z0-9_-]{1,}', $directory_name)) {
			
			// Use the user's provided directory name
			$temp_name = $directory_name;
		
		} else {
		
			// Generate a username for the user
			$temp_name = date("Y-m-d-H-i-s-",time()) . substr(strtoupper(md5(microtime())),0,4);
		
		}
						
		$images = $objEnvData->fetchPost('images');
		
		if (is_array($images)) {
			
			$read_write 	= "TRUE";
			$image_writable	= array();
			
			foreach($images as $key => $value) {
			
				if (!is_writable($cfg['sys']['base_incoming'])) {
					
					$read_write 		= "FALSE";
				
				} elseif (!is_writable($cfg['sys']['base_incoming']."/".$value)) {
				
					$read_write			= "FALSE";
					$images_writable[]	= "FALSE";
					
				} else {
				
					$images_writable[]	= "TRUE";
					
				}
			
			}
			
			if ($read_write == "TRUE") {
					
				$src_path = $cfg['sys']['base_incoming'];
				
				$des_path = $cfg['sys']['base_incoming']."/".$temp_name;
				
				// Make a new directory for this import
				$directory_created	= $this->createDirectory($des_path);
				
				// Make a new directory for the original images
				$directory_created	= $this->createDirectory($des_path."/original/");
				
				// Make a new directory for the comp images
				$directory_created	= $this->createDirectory($des_path."/".COMPING_DIR."/");
				
				// Make a new directory for the large thumbnail images
				$directory_created	= $this->createDirectory($des_path."/160x160/");
				
				// Make a new directory for the small thumbnail images
				$directory_created	= $this->createDirectory($des_path."/80x80/");
				
				// Make a new directory for the icon images
				$directory_created	= $this->createDirectory($des_path."/32x32/");
				
				$icon_path			= array();
				$icon_width			= array();
				$icon_height		= array();
				$file_name			= array();
				$upload_arr_o		= array();
				$upload_arr_c		= array();
				$upload_arr_l		= array();
				$upload_arr_s		= array();
				$upload_arr_i		= array();
				
				foreach($images as $key => $value) {
					
					$image_size = @getimagesize($src_path.$value);
					
					if ($image_size[0] > COMPING_SIZE || $image_size[1] > COMPING_SIZE) {
					
						// Move original images into the new original directory
						if (@rename($src_path.$value, $des_path."/original/".$value)) {
						
							// Create comp image 630 x 630 and save
							$this->resizeImageFile($des_path."/original/".$value, $des_path."/".COMPING_DIR."/".$value,COMPING_SIZE);
							
							// Create large thumbnail image 160 x 160 and save
							$this->resizeImageFile($des_path."/".COMPING_DIR."/".$value, $des_path."/160x160/".$value,"160");
							
							// Create small thumbnail image 80 x 80 and save
							$this->resizeImageFile($des_path."/160x160/".$value, $des_path."/80x80/".$value,"80");
							
							// Create icon image 32 x 32 and save
							$this->resizeImageFile($des_path."/80x80/".$value, $des_path."/32x32/".$value,"32");
							
							$icon_path[] 	= base64_encode($des_path."/32x32/".$value);
							$icon_size		= @getimagesize($des_path."/32x32/".$value);
							$icon_width[]	= $icon_size[0];
							$icon_height[]	= $icon_size[1];
							$comp_path[] 	= base64_encode($des_path."/".COMPING_DIR."/".$value);
							$comp_size		= @getimagesize($des_path."/".COMPING_DIR."/".$value);
							$comp_width[]	= $comp_size[0];
							$comp_height[]	= $comp_size[1];
							$file_name[]	= $value;
							$upload_arr_o[]	= file_exists($des_path."/original/".$value);
							$upload_arr_c[]	= file_exists($des_path."/".COMPING_DIR."/".$value);
							$upload_arr_l[]	= file_exists($des_path."/160x160/".$value);
							$upload_arr_s[]	= file_exists($des_path."/80x80/".$value);
							$upload_arr_i[]	= file_exists($des_path."/32x32/".$value);
							
						}
						
					} else {
					
						// Move original images into the new original directory
						if (@rename($src_path.$value, $des_path."/".COMPING_DIR."/".$value)) {
						
							// Create large thumbnail image 160 x 160 and save
							$this->resizeImageFile($des_path."/".COMPING_DIR."/".$value, $des_path."/160x160/".$value,"160");
							
							// Create small thumbnail image 80 x 80 and save
							$this->resizeImageFile($des_path."/160x160/".$value, $des_path."/80x80/".$value,"80");
							
							// Create icon image 32 x 32 and save
							$this->resizeImageFile($des_path."/80x80/".$value, $des_path."/32x32/".$value,"32");
							
							$icon_path[] 	= base64_encode($des_path."/32x32/".$value);
							$icon_size		= @getimagesize($des_path."/32x32/".$value);
							$icon_width[]	= $icon_size[0];
							$icon_height[]	= $icon_size[1];
							$comp_path[] 	= base64_encode($des_path."/".COMPING_DIR."/".$value);
							$comp_size		= @getimagesize($des_path."/".COMPING_DIR."/".$value);
							$comp_width[]	= $comp_size[0];
							$comp_height[]	= $comp_size[1];
							$file_name[]	= $value;
							$upload_arr_o[]	= file_exists($des_path."/original/".$value);
							$upload_arr_c[]	= file_exists($des_path."/".COMPING_DIR."/".$value);
							$upload_arr_l[]	= file_exists($des_path."/160x160/".$value);
							$upload_arr_s[]	= file_exists($des_path."/80x80/".$value);
							$upload_arr_i[]	= file_exists($des_path."/32x32/".$value);
							
						}
						
					}
					
				}
				
				// If any of the thumbnail images could not be created, show a warning
				if (in_array(0,$upload_arr_l)) {
				$smarty->assign("jpeg_error",(bool)true);
				}
				
				// Load array of image information into Smarty
				$smarty->assign("temp_name",$temp_name);
				$smarty->assign("icon_path",$icon_path);
				$smarty->assign("icon_width",$icon_width);
				$smarty->assign("icon_height",$icon_height);
				$smarty->assign("comp_path",$comp_path);
				$smarty->assign("comp_width",$comp_width);
				$smarty->assign("comp_height",$comp_height);
				$smarty->assign("file_name",$file_name);
				$smarty->assign("upload_arr_o",$upload_arr_o);
				$smarty->assign("upload_arr_c",$upload_arr_c);
				$smarty->assign("upload_arr_l",$upload_arr_l);
				$smarty->assign("upload_arr_s",$upload_arr_s);
				$smarty->assign("upload_arr_i",$upload_arr_i);

				// Set the page title
				$smarty->assign("page_title","Import comping images");
								
				// Display the library import page
				$smarty->pixDisplay('admin.library/library.import.01.tpl');
			
			} else {
					
				// Set the page title
				$smarty->assign("page_title","File permissions problem");
				
				// Set the title to show
				$smarty->assign("error_title","File permissions problem");
				
				// Load the array of images' read/write status into Smarty
				$smarty->assign("images_writable",$images_writable);
				
				// Load the array of image names into Smarty
				$smarty->assign("images",$images);
			
				// Display the library import page
				$smarty->pixDisplay('admin.library/library.import.02.tpl');
			
			}
			
		}
					
	}
	
	/*
	*
	*	
	*
	*/
	function formDeleteImageFiles () {
	
		global $objEnvData, $cfg, $smarty;
		
		// Get an array of the images
		$images = $objEnvData->fetchGlobal('images');
		
		if (is_array($images)) {
		
			$fn=1;
			foreach($images as $key => $value) {
			
				$imageinfo = @getimagesize($cfg['sys']['base_incoming'].$value);
				
				$image_data[$fn]['readwrite']	= is_writable($cfg['sys']['base_incoming'].$value);
				
				$image_data[$fn]['filepath']	= base64_encode($cfg['sys']['base_incoming'].$value);
				$image_data[$fn]['filename']	= $value;
				$image_data[$fn]['filesize']	= round((filesize($cfg['sys']['base_incoming'].$value) / 1024),2);
				$image_data[$fn]['width']		= $imageinfo[0];
				$image_data[$fn]['height']		= $imageinfo[1];
				$fn++;
				
			}
		
		}

		// Tell Smarty whether or not to show a table of items
		if (is_array($image_data)) { $smarty->assign("display","TRUE"); }
		
		if (is_array($image_data)) {
		
			// Sort the items into alphabetical order (not all servers do it automatically)
			@natsort($image_data);
			
			foreach ($image_data as $key => $value) {
				
				$readwrite[]= $value['readwrite'];
				$filepath[]	= $value['filepath'];
				$filename[]	= $value['filename'];
				$filesize[]	= $value['filesize'];
				$width[]	= $value['width'];
				$height[]	= $value['height'];
			
			}
		
		}
				
		// Assign variables to Smarty object
		$smarty->assign("readwrite",$readwrite);
		$smarty->assign("filepath",$filepath);
		$smarty->assign("filename",$filename);
		$smarty->assign("filesize",$filesize);
		$smarty->assign("width",$width);
		$smarty->assign("height",$height);
		
		$smarty->assign("page_title","Delete images");
		
		// Load the HTML page
		$smarty->pixDisplay('admin.library/delete.incoming.01.tpl');

	}
	
	/*
	*
	*	
	*
	*/
	function confirmDeleteFromIncoming () {
	
		global $objEnvData, $cfg, $smarty;
		
		// Get an array of the images
		$images = $objEnvData->fetchGlobal('images');
		
		if (is_array($images)) {
		
			$fn=1;
			foreach($images as $key => $value) {
				
				if (!@unlink($cfg['sys']['base_incoming'].$value)) {
				
					$imageinfo = @getimagesize($cfg['sys']['base_incoming'].$value);
					
					$image_data[$fn]['readwrite']	= is_writable($cfg['sys']['base_incoming'].$value);
					
					$image_data[$fn]['filepath']	= base64_encode($cfg['sys']['base_incoming'].$value);
					$image_data[$fn]['filename']	= $value;
					$image_data[$fn]['filesize']	= round((@filesize($cfg['sys']['base_incoming'].$value) / 1024),2);
					$image_data[$fn]['width']		= $imageinfo[0];
					$image_data[$fn]['height']		= $imageinfo[1];
					$fn++;
				
				}
				
			}
		
		}

	
		// Tell Smarty whether or not to show a table of items
		if (is_array($image_data)) {
		
			// Sort the items into alphabetical order (not all servers do it automatically)
			@natsort($image_data);
			
			foreach ($image_data as $key => $value) {
				
				$readwrite[]= $value['readwrite'];
				$filepath[]	= $value['filepath'];
				$filename[]	= $value['filename'];
				$filesize[]	= $value['filesize'];
				$width[]	= $value['width'];
				$height[]	= $value['height'];
			
			}
		
			// Assign variables to Smarty object
			$smarty->assign("readwrite",$readwrite);
			$smarty->assign("filepath",$filepath);
			$smarty->assign("filename",$filename);
			$smarty->assign("filesize",$filesize);
			$smarty->assign("width",$width);
			$smarty->assign("height",$height);
			
			// Set the title for this page
			$smarty->assign("page_title","Delete images");
			
			// Load the HTML page
			$smarty->pixDisplay('admin.library/delete.incoming.02.tpl');
		
		} else {
			
			// The images were deleted succesfully
			$smarty->assign("page_title","Images deleted");
			
			// Load the HTML page
			$smarty->pixDisplay('admin.library/delete.incoming.03.tpl');
		
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function showFormImportImages () {

		global $cfg, $smarty, $objEnvData;
		
		$smarty->assign("page_title","Import images");
		
		// Show the add content form
		$smarty->pixDisplay('admin.library/form.import.tpl');

	}
	
	/*
	*
	*	
	*
	*/
	function showFormUploadImages () {

		global $cfg, $smarty, $objEnvData;
		
		$smarty->assign("page_title","Upload images");
		
		// Show the add content form
		$smarty->pixDisplay('admin.library/form.upload.tpl');

	}
	
	/*
	*
	*	
	*
	*/
	function showUploadTypeChooser () {
	
		global $cfg, $smarty, $objEnvData;
		
		$process		= aglobal(process);
		$uploadtype		= aglobal(uploadtype);
		
		// We need to make sure that there are no lingering bits from previous uploads:
		if (file_exists(SYS_BASE_PATH.$cfg['set']['temporary']."icon_".$ses[USERID].".jpg")) {
		@unlink(SYS_BASE_PATH.$cfg['set']['temporary']."lthum_".$ses[USERID].".jpg");
		}
		
		if (file_exists(SYS_BASE_PATH.$cfg['set']['temporary']."sthum_".$ses[USERID].".jpg")) {
		@unlink(SYS_BASE_PATH.$cfg['set']['temporary']."lthum_".$ses[USERID].".jpg");
		}
		
		if (file_exists(SYS_BASE_PATH.$cfg['set']['temporary']."lthum_".$ses[USERID].".jpg")) {
		@unlink(SYS_BASE_PATH.$cfg['set']['temporary']."lthum_".$ses[USERID].".jpg");
		}
		
		if (file_exists(SYS_BASE_PATH.$cfg['set']['temporary']."comp_".$ses[USERID].".jpg")) { 
		@unlink(SYS_BASE_PATH.$cfg['set']['temporary']."comp_".$ses[USERID].".jpg");
		}
		
		if (file_exists(SYS_BASE_PATH.$cfg['set']['temporary']."original_".$ses[USERID].".jpg")) { 
		@unlink(SYS_BASE_PATH.$cfg['set']['temporary']."original_".$ses[USERID].".jpg");
		}
		
		
		$php_upload_limit = rtrim(get_cfg_var(upload_max_filesize),'M');
		$php_memory_limit = rtrim(get_cfg_var(memory_limit),'M');
		
		if ($php_upload_limit < ($php_memory_limit/3)) {
		
			$smarty->assign("popp_upload_limit",round($php_upload_limit,0)*1024000);
		
		} else {
		
			$smarty->assign("popp_upload_limit",round(($php_memory_limit/3),0)*1024000);
		
		}
		
		// Output html
		$smarty->pixDisplay('admin.library/library.add.upload.tpl');
	
	}
	
	/*
	*
	*	
	*
	*/
	function showArchiveUploader () {
	
		global $cfg, $smarty, $objEnvData;
		
		// Check we're not running in safe mode
		$this->catchSafeMode();
		
		$smarty->pixDisplay('admin.library/library.add.archive.tpl');
		
	}
	
	function showJpegUploader () {
	
		global $cfg, $smarty, $objEnvData;
		
		// Check we're not running in safe mode
		$this->catchSafeMode();
		
		$smarty->pixDisplay('admin.library/library.add.jpeg.tpl');
		
	}
	
	function showJavaUploader () {
	
		global $cfg, $smarty, $objEnvData;
		
		// Check we're not running in safe mode
		$this->catchSafeMode();
		
		$smarty->pixDisplay('admin.library/library.add.java.uploader.tpl');
		
	}
	
	/*
	*
	*	
	*
	*/
	function catchSafeMode () {
		
		global $cfg, $smarty, $objEnvData;
		
		// Catch safe mode problems
		if ($cfg['sys']['safe_mode']) {
			$smarty->assign("page_title","Safe mode warning");
			$smarty->pixDisplay('admin.snippets/admin.html.error.safemode.tpl');
			exit;
		}
				
	}
	
	/*
	*
	*	
	*
	*/
	function processJpeg () {

		global $cfg, $smarty, $objEnvData;
		
		if (is_array($_FILES)) {
		
			foreach ($_FILES as $key => $value) {
				
				$file_name		= $value['name'];
				$file_type		= $value['type'];
				$file_temp_name	= $value['tmp_name'];
				$file_size		= $value['size'];
				$file_error		= $value['error'];
				
				if (file_exists($file_temp_name)) {
					
					if ($file_type == "image/jpeg" || $file_type == "image/jpg" || $file_type == "image/pjpeg") {
					
						$file_data = @getimagesize($file_temp_name);
						
						if ($file_data[2] == 2) {
						
							// The file is a JPEG file so we move it to the incoming directory
							rename($file_temp_name,$cfg['sys']['base_incoming'].$file_name);
							$old_umask = umask(0);
							@chmod($cfg['sys']['base_incoming'].$file_name,0777);
							umask($old_umask);
							
							$files[] = $file_name;
							
						} else {
						
							// The file is not a JPEG file so we delete it
							@unlink($file_temp_name);
							
						}
					
					} else {
						
						// The file is not a JPEG file so we delete it
						@unlink($file_temp_name);
							
					}
					
				}
				
			}
		
		}
		
		// Import the uploaded images
		$this->importUploaded($files);
	
	}
	
	/*
	*
	*	
	*
	*/
	function processArchive () {
	
		global $cfg, $smarty, $objEnvData;
		
		require_once (SYS_BASE_PATH.'resources/ext/archive_extractor/ArchiveExtractor.class.php');

		if (is_array($_FILES) && count($_FILES) == 1) { // There is a single archive file
		
			foreach ($_FILES as $key => $value) {
				
				$file_name		= $value['name'];
				$file_type		= $value['type'];
				$file_temp_name	= $value['tmp_name'];
				$file_size		= $value['size'];
				$file_error		= $value['error'];
				
				if (file_exists($file_temp_name)) {
					
					if ($file_type == "application/zip" || $file_type == "application/x-tar" || $file_type == "application/x-gzip" || $file_type == "application/x-zip-compressed") {
						
						$file_name_info = explode(".",$file_name);
						
						$file_extension = array_pop($file_name_info);
						
						if ($file_extension == ("zip" || "gz" || "gzip" || "tgz" || "tar")) {
						
							// The file is a valid archive
							rename($file_temp_name,$cfg['sys']['base_incoming'].$file_name);
							$old_umask = umask(0);
							@chmod($cfg['sys']['base_incoming'].$file_name,0777);
							umask($old_umask);
							
							// Init. ArchiveExtractor Object
							$archExtractor = new ArchiveExtractor();
							
							/* Extract */
							$extractedFileList = $archExtractor->extractArchive($cfg['sys']['base_incoming'].$file_name,$cfg['sys']['base_incoming']);
							
							// Delete the archive
							@unlink($cfg['sys']['base_incoming'].$file_name);
							
							foreach ($extractedFileList as $key => $value) {
								
								$files[] = $value['stored_filename'];
							
							}
							
							// Upload import form
							$this->importUploaded($files);
							
						} else {
						
							// The file is not an archive file so we delete it
							@unlink($file_temp_name);
							
							// Set an error message
							$error = "The file you uploaded was not a valid zip, tar or gzip archive - please try again making sure that the archive contains only JPEG images.";
							
						}
					
					} else {
						
						// The file is not an archive file so we delete it
						@unlink($file_temp_name);
						
						// Set an error message
						$error = "The file you uploaded was not a valid zip, tar or gzip archive - please try again making sure that the archive contains only JPEG images.";
						
					}
					
				} else { // Something went wrong during the upload as the file doesn't exist
				
					// Set an error message
					$error = "No valid archive was uploaded.  This may be because the file you attempted to upload was larger than PHP's upload limit or there is a problem with the configuration of your Pixaria website.";
						
				}

			}
			
		} else { // No file was uploaded or too many files were uploaded
		
			// Set an error message
			$error = "No valid archive was uploaded.  You can only upload a single zip, tar or gzip archive of JPEG images.";
						
		}
		
		// There was a problem with the archive
		if ($error != "") {
			
			$smarty->assign("error_message",$error);
			
			// Show the error message to the user
			$smarty->pixDisplay('admin.library/library.add.archive.error.tpl');
			
			// Exit the script
			exit;
			
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function importFromIncoming () {
	
		global $cfg, $smarty, $objEnvData;
		
		// Find out what valid photo albums are on the system and present them as options to the user
		$handle=@opendir($cfg['sys']['base_incoming']);
		if($handle != FALSE) {
			$fn=1;
			while (false!==($filename = readdir($handle))) { 
				
				if (substr($filename,0,1) != ".") { // Exclude filenames starting with a period
					
					$pathinfo = pathinfo($cfg['sys']['base_incoming']."/".$filename);
					if (strtolower($pathinfo['extension'])==("tif"||"jpg"||"mov"||"gif"||"png"||"tiff"||"avi")) { // Only include image files
					
						$imageinfo = @getimagesize($cfg['sys']['base_incoming']."/".$filename);
						
						$image_data[$fn]['filepath']	= base64_encode($cfg['sys']['base_incoming']."/".$filename);
						$image_data[$fn]['filename']	= $filename;
						$image_data[$fn]['filesize']	= round((filesize($cfg['sys']['base_incoming']."/".$filename) / 1024),2);
						$image_data[$fn]['width']		= $imageinfo[0];
						$image_data[$fn]['height']		= $imageinfo[1];
						$image_data[$fn]['filemtime']	= filemtime($cfg['sys']['base_incoming']."/".$filename);
						$image_data[$fn]['resizeable']	= imageIsResizeable($cfg['sys']['base_incoming']."/".$filename);
						$fn++;
						
					}
					
				}
				
			}
			
		}
		
		@closedir($handle);
		
		if (is_array($image_data)) {
		
			// Sort the items into alphabetical order (not all servers do it automatically)
			@natsort($image_data);
			
			foreach ($image_data as $key => $value) {
			
				$filepath[]		= $value['filepath'];
				$filename[]		= $value['filename'];
				$filesize[]		= $value['filesize'];
				$width[]		= $value['width'];
				$height[]		= $value['height'];
				$filemtime[]	= $value['filemtime'];
				$resizeable[]	= $value['resizeable'];
			
			}
		
		}
		
		/*
		*	List directories in the incoming directory
		*/
		$handle=@opendir($cfg['sys']['base_incoming']);
		if($handle != FALSE) {
		while (false!==($file = readdir($handle))) { 
			if (is_dir($cfg['sys']['base_incoming'] . $file) && substr($file,0,1) != ".") { 
				$show_directories 	= (bool)TRUE;
				$incoming[]			= $file;
			}
		}
		}
		@closedir($handle);
		
		
		/*
		*	List directories in the library directory
		*/
		$handle=@opendir($cfg['sys']['base_library']);
		if($handle != FALSE) {
		while (false!==($file = readdir($handle))) { 
			if (is_dir($cfg['sys']['base_library'] . $file) && substr($file,0,1) != ".") { 
				$show_directories 	= (bool)TRUE;
				$library[]			= $file;
			}
		}
		}
		@closedir($handle);
		
		/*
		*	Input directory information into Smarty
		*/
		$smarty->assign("show_directories",$show_directories);
		$smarty->assign("incoming",$incoming);
		$smarty->assign("library",$library);
		
		// Tell Smarty whether or not to show a table of items
		if (is_array($filepath)) { $smarty->assign("display","TRUE"); }
		
		// Assign variables to Smarty object
		$smarty->assign("filepath",$filepath);
		$smarty->assign("filename",$filename);
		$smarty->assign("filesize",$filesize);
		$smarty->assign("width",$width);
		$smarty->assign("height",$height);
		$smarty->assign("filemtime",$filemtime);
		$smarty->assign("resizeable",$resizeable);
		
		// Set the page title
		$smarty->assign("page_title","Import high resolution images");
		
		// Display the html header
		$smarty->pixDisplay('admin.library/library.add.images.tpl');
	
	}
	
	/*
	*
	*	
	*
	*/
	function importDirectories () {
	
		global $cfg, $smarty, $objEnvData;
		
		// Load the column titles and widths into arrays
		$column_titles	= array("Directory name","Number of images");
	
		// Find out what valid gallery directories are on the system and present them as options to the user
		$handle=@opendir($cfg['sys']['base_incoming']);
		
		if($handle != FALSE) {
		
			$fn=0;
			
			while (false!==($directory = readdir($handle))) {
			
				if (substr($directory,0,1) != "." && is_dir($cfg['sys']['base_incoming']."/".$directory)) {

					// We must have a comp image directory, the others can be made manually
					if (is_dir($cfg['sys']['base_incoming'] ."/". $directory . "/".COMPING_DIR."/")) {
					
						$number = $fn + 1;
					
						$directory_data[$number]['count']	= countDirectoryItems($cfg['sys']['base_incoming'] ."/". $directory . "/".COMPING_DIR."/");
						$directory_data[$number]['name']	= $directory;
						
						$fn++;
					
					}
				}
				
			}
			
		}
		
		@closedir($handle);
		
		if (is_array($directory_data)) {
		
			// Sort the items into alphabetical order (not all servers do it automatically)
			@natsort($directory_data);
			
			foreach ($directory_data as $key => $value) {
			
				$directory_name[]	= $value['name'];
				$directory_count[]	= $value['count'];
			
			}
		
		}
		
		## List directories in the incoming directory
		$handle=@opendir($cfg['sys']['base_incoming']);
		if($handle != FALSE) {
		while (false!==($file = readdir($handle))) { 
			if (is_dir($cfg['sys']['base_incoming'] . $file) && substr($file,0,1) != ".") { 
				  
				$show_directories 	= (bool)TRUE;
				
				$directories[]		= $file;

			}
		}
		}
		@closedir($handle);
		
		$smarty->assign("show_directories",$show_directories);
		$smarty->assign("directories",$directories);
		
		// Tell Smarty whether or not to show a table of items
		if (is_array($directory_name)) { $smarty->assign("display","TRUE"); }
		
		// Assign variables to Smarty object
		$smarty->assign("directory_name",$directory_name);
		$smarty->assign("directory_count",$directory_count);
		
		// Set the page title
		$smarty->assign("page_title","Browse complete directories");
		
		// Display the html header
		$smarty->pixDisplay('admin.library/library.add.directory.tpl');
	
	}

	/*
	*
	*	
	*
	*/
	function importUploaded ($files) {
	
		global $cfg, $smarty, $objEnvData;
		
		if (is_array($objEnvData->fetchGlobal('fn'))) {
			$files = $objEnvData->fetchGlobal('fn');
		}
		
		if (is_array($files)) {
		
			foreach ($files as $key => $filename) {
			
				$pathinfo = pathinfo($cfg['sys']['base_incoming']."/".$filename);
				if (strtolower($pathinfo['extension']) == "jpg") { // Only include JPEG files
				
					$imageinfo = @getimagesize($cfg['sys']['base_incoming']."/".$filename);
										
						$image_data[$fn]['filepath']	= base64_encode($cfg['sys']['base_incoming']."/".$filename);
						$image_data[$fn]['filename']	= $filename;
						$image_data[$fn]['filesize']	= round((filesize($cfg['sys']['base_incoming']."/".$filename) / 1024),2);
						$image_data[$fn]['width']		= $imageinfo[0];
						$image_data[$fn]['height']		= $imageinfo[1];
						$image_data[$fn]['filemtime']	= filemtime($cfg['sys']['base_incoming']."/".$filename);
						$image_data[$fn]['resizeable']	= imageIsResizeable($cfg['sys']['base_incoming']."/".$filename);
						$fn++;
					
				}
			
			}
			
		}
		
		$filepath	= array();
		$filename	= array();
		$filesize	= array();
		$width		= array();
		$height		= array();
		$filemtime	= array();
		$resizeable	= array();
		
		if (is_array($image_data)) {
		
			// Sort the items into alphabetical order (not all servers do it automatically)
			@natsort($image_data);
			
			foreach ($image_data as $key => $value) {
			
				$filepath[]		= $value['filepath'];
				$filename[]		= $value['filename'];
				$filesize[]		= $value['filesize'];
				$width[]		= $value['width'];
				$height[]		= $value['height'];
				$filemtime[]	= $value['filemtime'];
				$resizeable[]	= $value['resizeable'];
			
			}
		
		}
		
		/*
		*	List directories in the incoming directory
		*/
		$handle=@opendir($cfg['sys']['base_incoming']);
		if($handle != FALSE) {
		while (false!==($file = readdir($handle))) { 
			if (is_dir($cfg['sys']['base_incoming'] . $file) && substr($file,0,1) != ".") { 
				$show_directories 	= (bool)TRUE;
				$incoming[]			= $file;
			}
		}
		}
		@closedir($handle);
		
		
		/*
		*	List directories in the library directory
		*/
		$handle=@opendir($cfg['sys']['base_library']);
		if($handle != FALSE) {
		while (false!==($file = readdir($handle))) { 
			if (is_dir($cfg['sys']['base_library'] . $file) && substr($file,0,1) != ".") { 
				$show_directories 	= (bool)TRUE;
				$library[]			= $file;
			}
		}
		}
		@closedir($handle);
		
		/*
		*	Input directory information into Smarty
		*/
		$smarty->assign("show_directories",$show_directories);
		$smarty->assign("incoming",$incoming);
		$smarty->assign("library",$library);
		
		// Tell Smarty whether or not to show a table of items
		if (is_array($filepath)) { $smarty->assign("display","TRUE"); }
		
		// Assign variables to Smarty object
		$smarty->assign("filepath",$filepath);
		$smarty->assign("filename",$filename);
		$smarty->assign("filesize",$filesize);
		$smarty->assign("width",$width);
		$smarty->assign("height",$height);
		$smarty->assign("filemtime",$filemtime);
		$smarty->assign("resizeable",$resizeable);
		
		// Set the page title
		$smarty->assign("page_title","Import uploaded images");
		
		// Display the html header
		$smarty->pixDisplay('admin.library/library.files.uploaded.tpl');
	
	}
	
	/*
	*
	*	
	*
	*/
	function checkSafeMode () {
		
		global $cfg, $smarty;
		
		// Catch safe mode problems
		if ($cfg['sys']['safe_mode']) {
		
			$smarty->assign("page_title","Safe mode warning");
			$smarty->pixDisplay('admin.snippets/admin.html.error.safemode.tpl');
			exit;
		
		} else {
		
			return true;
		
		}
	
	}
	
	/*
	*
	*	Take an image ($src_path), resize it to fixed width/height ($new_dims) and save it to a new location ($des_path)
	*
	*/
	function resizeImageFile($src_path,$des_path,$new_dims) {
		
		global $cfg;
		
		// Load the source graphic
		$source = @imagecreatefromjpeg($src_path);
		
		if ($source) {
		
			$imageX = @imagesx($source);
			$imageY = @imagesy($source);
			
			if ($imageX >= $imageY) {
			
				$thumbX = $new_dims;
				$thumbY = (int)(($thumbX*$imageY) / $imageX );
				
			} else {
			
				$thumbY = $new_dims;
				$thumbX = (int)(($thumbY*$imageX) / $imageY );
				
			}
		
			$dest_thum  = @imagecreatetruecolor($thumbX, $thumbY);
			@imagecopyresampled ($dest_thum, $source, 0, 0, 0, 0, $thumbX, $thumbY, $imageX, $imageY);
			@imageinterlace($dest_thum);
			@imagejpeg($dest_thum,$des_path,$cfg['set']['gd_quality']);
			
			return TRUE;
		
		} else {
		
			return FALSE;
		
		}
		
	}

	/*
	*
	*	Check if a directory exists and if it doesn't make it
	*
	*/
	function createDirectory($path) {
		
		global $cfg;
		
		if (!file_exists($path)) {
			
			$umask_old = umask(0);
			mkdir($path, 0777);
			$directory_created = TRUE;
			umask($old);
			
		} elseif (substr(sprintf('%o', fileperms($path)), -4) != 0777) {
			
			$path = umask(0);
			@chmod($path,0777);
			umask($old);
			
			$directory_created = FALSE;
			
		}
		
		return $directory_created;
	
	}
	
	/*
	*
	*	Move an image file from the incoming to library folder
	*
	*/
	function moveImageFileToLibrary($image_filename, $image_path) {
	
		global $cfg;
				
		$m_dir_i = $cfg['sys']['base_incoming'] . $image_path;
		$o_dir_i = $cfg['sys']['base_incoming'] . $image_path . "/original/";
		$c_dir_i = $cfg['sys']['base_incoming'] . $image_path . "/".COMPING_DIR."/";
		$l_dir_i = $cfg['sys']['base_incoming'] . $image_path . "/160x160/";
		$s_dir_i = $cfg['sys']['base_incoming'] . $image_path . "/80x80/";
		$i_dir_i = $cfg['sys']['base_incoming'] . $image_path . "/32x32/";
		
		$m_dir_l = $cfg['sys']['base_library'] . $image_path;
		$o_dir_l = $cfg['sys']['base_library'] . $image_path . "/original/";
		$c_dir_l = $cfg['sys']['base_library'] . $image_path . "/".COMPING_DIR."/";
		$l_dir_l = $cfg['sys']['base_library'] . $image_path . "/160x160/";
		$s_dir_l = $cfg['sys']['base_library'] . $image_path . "/80x80/";
		$i_dir_l = $cfg['sys']['base_library'] . $image_path . "/32x32/";
		
		if (!is_dir($m_dir_l)) { // Image directory doesn't exist so we need to make it
			$this->createDirectory($m_dir_l);
		}
		
		if (!is_dir($o_dir_l)) {
			$this->createDirectory($o_dir_l);
		}
	
		if (!is_dir($c_dir_l)) {
			$this->createDirectory($c_dir_l);
		}
	
		if (!is_dir($l_dir_l)) {
			$this->createDirectory($l_dir_l);
		}
	
		if (!is_dir($s_dir_l)) {
			$this->createDirectory($s_dir_l);
		}
	
		if (!is_dir($i_dir_l)) {
			$this->createDirectory($i_dir_l);
		}
		
		// Get an array of all types of this file in the original directory
		$original_files = glob($cfg['sys']['base_incoming'] . $image_path . "/original/" . substr($image_filename,0,-4) . "*");
		
		if (is_array($original_files)) {
		
			// Loop through files in the original (full size directory) to move any files matching the name of this one
			foreach ($original_files as $filename) {
				
				$path_parts = pathinfo($filename);
				$current_file = $path_parts['basename'];
				if (file_exists($o_dir_i.$current_file) && !file_exists($o_dir_l.$current_file)) {
					// Move the original image to the library	
					@rename("$o_dir_i$current_file" , "$o_dir_l$current_file");
				}	
			
			}
			
		} else {
		
			if (file_exists($o_dir_i.$image_filename) && !file_exists($o_dir_l.$image_filename)) {
				// Move the original image to the library	
				@rename("$o_dir_i$image_filename" , "$o_dir_l$image_filename");
			}	
			
		}
		
		if (file_exists($c_dir_i.$image_filename) && !file_exists($c_dir_l.$image_filename)) {
			// Move the original image to the library	
			@rename("$c_dir_i$image_filename" , "$c_dir_l$image_filename");
		} else {
			return false;
		}
		
		if (file_exists($l_dir_i.$image_filename) && !file_exists($l_dir_l.$image_filename)) {
			// Move the original image to the library	
			@rename("$l_dir_i$image_filename" , "$l_dir_l$image_filename");
		} else {
			return false;
		}
		
		if (file_exists($s_dir_i.$image_filename) && !file_exists($s_dir_l.$image_filename)) {
			// Move the original image to the library	
			@rename("$s_dir_i$image_filename" , "$s_dir_l$image_filename");
		} else {
			return false;
		}
		
		if (file_exists($i_dir_i.$image_filename) && !file_exists($i_dir_l.$image_filename)) {
			// Move the original image to the library	
			@rename("$i_dir_i$image_filename" , "$i_dir_l$image_filename");
		} else {
			return false;
		}
		
		return true;
		
	}
	
	/*
	*	Recursive function to delete a directory
	*/
	function deleteDirectory ($dir) {
	
	   if (substr($dir, strlen($dir)-1, 1) != '/')
		   $dir .= '/';
	
	   //echo $dir;
	
	   if ($handle = opendir($dir))
	   {
		   while ($obj = readdir($handle))
		   {
			   if ($obj != '.' && $obj != '..')
			   {
				   if (is_dir($dir.$obj))
				   {
					   if (!pix_delete_directory($dir.$obj))
						   return false;
				   }
				   elseif (is_file($dir.$obj))
				   {
					   if (!unlink($dir.$obj))
						   return false;
				   }
			   }
		   }
	
		   closedir($handle);
	
		   if (!@rmdir($dir))
			   return false;
		   return true;
	   }
	   return false;
	}
	
}

?>