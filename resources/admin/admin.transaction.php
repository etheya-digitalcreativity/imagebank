<?php

/*
*
*	Pixaria Gallery 1.0
*	Copyright 2002 - 2006 Jamie Longstaff
*
*	Script author:		Jamie Longstaff
*	Script version:		1.0	
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "store";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Check that the user is a SuperUser
pix_authorise_user("administrator");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

$cmd = $objEnvData->fetchGlobal('cmd');

switch ($cmd) {
	
	case 30: case 'viewOpenCart': // View a user's cart
	
		// Load required variables
		$cart_id = $objEnvData->fetchGlobal('cart_id');
		
		// Load the cart object view class
		require_once ('class.PixariaCartObjects.php');
		
		// Initialise the cart object viewer class
		$objCartObjects = new PixariaCartObjects($cart_id);
		
		if ($objCartObjects->cart_contents) {
						
			$smarty->assign("cart_id",$objCartObjects->cart_id);
			$smarty->assign("cart_status",$objCartObjects->cart_status);
			$smarty->assign("calculator_href",$objCartObjects->cart_item_calculator_href);
			$smarty->assign("cart_item_id",$objCartObjects->cart_item_id);
			$smarty->assign("cart_item_type",$objCartObjects->getCartItemTypeText());
			$smarty->assign("item_usage_text",$objCartObjects->cart_item_usage_text);
			$smarty->assign("item_price",$objCartObjects->cart_item_price);
			$smarty->assign("usage_text",$objCartObjects->cart_usage_text);			
			$smarty->assign("quote_complete",$objCartObjects->cart_quote_complete);
			$smarty->assign("price_complete",$objCartObjects->cart_price_complete);	
			$smarty->assign("image_id",$objCartObjects->cart_item_image_id);
			$smarty->assign("image_title",$objCartObjects->cart_item_image_title);
			$smarty->assign("image_filename",$objCartObjects->cart_item_image_filename);
			$smarty->assign("image_icon_path",$objCartObjects->cart_item_image_icon_path);
			$smarty->assign("image_icon_dims",$objCartObjects->cart_item_image_icon_dims);
			$smarty->assign("image_small_path",$objCartObjects->cart_item_image_small_path);
			$smarty->assign("image_small_dims",$objCartObjects->cart_item_image_small_dims);
			$smarty->assign("image_large_path",$objCartObjects->cart_item_image_large_path);
			$smarty->assign("image_large_dims",$objCartObjects->cart_item_image_large_dims);
			$smarty->assign("image_comp_path",$objCartObjects->cart_item_image_comp_path);
			$smarty->assign("image_comp_size",$objCartObjects->cart_item_image_comp_size);
			$smarty->assign("shipping",$objCartObjects->getShipping());
			$smarty->assign("subtotal",$objCartObjects->cart_subtotal);
			$smarty->assign("sales_tax",$objCartObjects->cart_tax);
			$smarty->assign("tax_total",$objCartObjects->cart_tax_total);
			$smarty->assign("download_attempts",$objCartObjects->cart_item_download_counter);
			$smarty->assign("quote_price",$objCartObjects->cart_item_price);	
			$smarty->assign("total",$objCartObjects->cart_total);
			$smarty->assign("message_id",$objCartObjects->cart_message_id);
			$smarty->assign("message_username",$objCartObjects->cart_message_username);
			$smarty->assign("message_user_email",$objCartObjects->cart_message_user_email);
			$smarty->assign("message_text",$objCartObjects->cart_message_text);
			$smarty->assign("message_time",$objCartObjects->cart_message_time);
			$smarty->assign("message_on",$objCartObjects->cart_message_on);
			$smarty->assign("user_name",$objCartObjects->getUserName());
			$smarty->assign("user_email",$objCartObjects->getUserEmail());
			
			// Give Smarty the URI of the current page
			$smarty->assign("referer",$_SERVER['REQUEST_URI']);
				
			// Set the HTML page title and masthead text
			$smarty->assign("page_title","View transaction");
			
			// Display the cart page
			$smarty->pixDisplay('admin.sales/admin.sales.view.tpl');
			
		} else { // There is nothing in the cart or there is no cart to view
			
			// Set the HTML page title and masthead text
			$smarty->assign("page_title","The cart is empty");
		
			// Display the empty cart page
			$smarty->pixDisplay('admin.sales/cart.empty.tpl');

		}
		
	break;
	
	case 20: // Reset the download counter for a cart item
	
		$id			= $objEnvData->fetchGlobal('id');
		$cart_id	= $objEnvData->fetchGlobal('cart_id');
		
		// Load the shopping cart class
		require_once ('class.PixariaCartObject.php');
	
		// Instantiate cart object
		$objCartObject = new PixariaCartObject($id);
		
		// Set the download count to zero
		$objCartObject->setCartItemDownloadCounter(0);
	
		// Update the cart object
		$objCartObject->updateCartObject();
	
		// Set redirect url to the referring page
		$meta_url	= $_SERVER['HTTP_REFERER'];
		
		// Print out the waiting screen
		$smarty->pixWaiting($meta_url,"1");
		
		// Stop running script
		exit;
	
	break;
	
	case 13: // Delete an item from a cart
	
		$cart_id		= $objEnvData->fetchGlobal(cart_id);
		$cart_item_id	= $objEnvData->fetchGlobal(cart_item_id);
		
		if (isset($cart_item_id) && is_numeric($cart_item_id)) {
			
			// Load the shopping cart class
			require_once ('class.PixariaCartObject.php');
		
			// Instantiate cart object
			$objCartObject = new PixariaCartObject($cart_item_id);
			
			// Delete the item from the cart
			$objCartObject->deleteCartObject($cart_item_id);
			
		}

		// Set redirect url
		$meta_url	= $cfg['sys']['base_url'].$cfg['fil']['admin_transaction']."?cmd=10&amp;cart_id=$cart_id";
		
		// Print out the waiting screen
		$smarty->pixWaiting($meta_url,"1");
		
		// Stop running script
		exit;
	
	break;
	
	case 12: // Edit details of a quotation
		
		$sales_tax		= trim($objEnvData->fetchGlobal('sales_tax'), " %");
		$shipping		= $objEnvData->fetchGlobal('shipping');
		$quote_price	= $objEnvData->fetchGlobal('quote_price');
		$image_id		= $objEnvData->fetchGlobal('image_id');
		$cart_id		= $objEnvData->fetchGlobal('cart_id');
		$cart_item_id	= $objEnvData->fetchGlobal('cart_item_id');
		$status			= $objEnvData->fetchGlobal('status');
		
		if (is_numeric($cart_id) && is_array($cart_item_id) && is_array($quote_price)) {
			
			foreach ($cart_item_id as $key => $value) {
				
				$quote_price_val 	= trim($quote_price[$key], " %");
				$item_id			= $cart_item_id[$key];
				
				if (is_numeric($quote_price_val)) {
					
					// Load the shopping cart item class
					require_once ('class.PixariaCartObject.php');
					
					// Instantiate cart object
					$objCartObject = new PixariaCartObject($item_id);
					
					// Set the new quote price
					$objCartObject->setPrice($quote_price_val);

					if (is_numeric($sales_tax) && is_numeric($shipping)) {
				
						$objCartObject->setTax($sales_tax);
						$objCartObject->setShipping($shipping);
					
					} else {
					
						if ($status == 4) {
							
							$objCartObject->setDateCompleted("NOW");
						
						}
						
					}
					
					// Set the new quote price
					$objCartObject->setStatus($status);

					// Update the cart object
					$objCartObject->updateCartObject();
				
				}
				
			}

			// Get the user's name and e-mail address
			$details = getUserContactDetails($objCartObject->getUserId());
		
		} else {
	
			// Load the shopping cart class
			require_once ('class.PixariaCart.php');
			
			// Instantiate cart object
			$objCart = new PixariaCart($cart_id);
			
			if ($status == 4) {
				
				$objCart->setDateCompleted("NOW");
			
			}
				
			// Set the new quote price
			$objCart->setStatus($status);

			// Update the cart
			$objCart->updateCart();
			
			// Get the user's name and e-mail address
			$details = getUserContactDetails($objCart->getUserId());
		
		}
		
		// Build required values for e-mails
		$to_name	= $details[0];
		$to_email	= $details[1];
		
		// Load variables required to send e-mail
		$smarty->assign("to_name",$to_name);
		$smarty->assign("to_email",$to_email);
		$smarty->assign("cart_id",$cart_id);
		
		// Load the message text from the template
		$message = $smarty->pixFetch('email.templates/cart.notify.tpl');
			
		// Send an e-mail to the client to tell them their quote is ready
		pix_send_email($to_email, $to_name, $message, $cfg['set']['contact_email'], $cfg['set']['site_name'], $cfg['set']['site_name']." Shopping Cart Updated");
		
		// Set redirect url
		$meta_url	= $cfg['sys']['base_url'].$cfg['fil']['admin_store']."?cmd=salesView&amp;status=$status";
		
		// Print out the waiting screen
		$smarty->pixWaiting($meta_url,"1");
		
	break;
	
	case 11: // Add a message to a pending quote
		
		// Load required variables
		$cart_id	= $objEnvData->fetchGlobal('cart_id');
		$message	= addslashes($objEnvData->fetchGlobal('message'));
		$userid		= $ses['psg_userid'];
		
		// Load the shopping cart class
		require_once ('class.PixariaCart.php');
		
		// Instantiate cart object
		$objCart = new PixariaCart($cart_id);
				
		// Add the message to the cart
		$objCart->createCartMessage($message);
		
		// Get the user's name and e-mail address
		$details = getUserContactDetails($objCart->getUserId());
		
		// Build required values for e-mails
		$to_name	= $details[0];
		$to_email	= $details[1];
		
		// Load variables required to send e-mail
		$smarty->assign("to_name",$to_name);
		$smarty->assign("to_email",$to_email);
		$smarty->assign("cart_id",$cart_id);
		
		// Load the e-mail class
		require_once ('class.PixariaEmail.php');
		
		// Initialise the e-mail object
		$objEmail = new PixariaEmail();
		
		// Set the object properties
		$objEmail->setSubject($cfg['set']['site_name']." Quote Request");
		$objEmail->setMessage($smarty->pixFetch('email.templates/transaction.message.user.tpl'));
		$objEmail->setRecipientAddress($to_email);
		$objEmail->setRecipientName($to_name);
		$objEmail->setSenderAddress($cfg['set']['contact_email']);
		$objEmail->setSenderName($cfg['set']['contact_name']);
		
		// Send the e-mail
		$objEmail->sendEmail();

		// Set redirect url
		$meta_url	= $cfg['sys']['base_url'].$cfg['fil']['admin_transaction']."?cmd=10&amp;cart_id=$cart_id";
		
		// Print out the waiting screen
		$smarty->pixWaiting($meta_url,"1");
		
		// Stop running script
		exit;

	break;
	
	case 10: case 'viewSubmittedCart': // Show details of a pending quote
		
		// Load required variables
		$cart_id	= $objEnvData->fetchGlobal('cart_id');
		$userid		= $ses['psg_userid'];
		
		// Load the shopping cart class
		require_once ('class.PixariaCartObjects.php');
		
		// Instantiate cart object
		$objCart = new PixariaCartObjects($cart_id);
				
		if ($objCart->cart_contents) {
						
			$smarty->assign("cart_id",$objCart->cart_id);
			$smarty->assign("cart_status",$objCart->cart_status);
			$smarty->assign("payment_method",$objCart->getPaymentMethod());
			$smarty->assign("calculator_href",$objCart->cart_item_calculator_href);
			$smarty->assign("cart_item_quantity",$objCart->getCartItemQuantity());
			$smarty->assign("cart_item_type",$objCart->getCartItemType());
			$smarty->assign("cart_item_type_text",$objCart->getCartItemTypeText());
			$smarty->assign("cart_item_id",$objCart->cart_item_id);
			$smarty->assign("item_usage_text",$objCart->cart_item_usage_text);
			$smarty->assign("photographer_details",$objCart->getCartItemPhotographerUserDetails());
			$smarty->assign("photographer_userid",$objCart->getCartItemPhotographerUserId());
			$smarty->assign("item_price",$objCart->cart_item_price);
			$smarty->assign("cart_usage_text",$objCart->cart_usage_text);			
			$smarty->assign("quote_complete",$objCart->cart_quote_complete);
			$smarty->assign("price_complete",$objCart->cart_price_complete);	
			$smarty->assign("image_id",$objCart->cart_item_image_id);
			$smarty->assign("image_title",$objCart->cart_item_image_title);
			$smarty->assign("image_filename",$objCart->cart_item_image_filename);
			$smarty->assign("image_icon_path",$objCart->cart_item_image_icon_path);
			$smarty->assign("image_icon_dims",$objCart->cart_item_image_icon_dims);
			$smarty->assign("image_small_path",$objCart->cart_item_image_small_path);
			$smarty->assign("image_small_dims",$objCart->cart_item_image_small_dims);
			$smarty->assign("image_large_path",$objCart->cart_item_image_large_path);
			$smarty->assign("image_large_dims",$objCart->cart_item_image_large_dims);
			$smarty->assign("image_comp_path",$objCart->cart_item_image_comp_path);
			$smarty->assign("image_comp_size",$objCart->cart_item_image_comp_size);
			$smarty->assign("shipping",$objCart->getShipping());
			$smarty->assign("subtotal",$objCart->cart_subtotal);
			$smarty->assign("sales_tax",$objCart->cart_tax);
			$smarty->assign("tax_total",$objCart->cart_tax_total);
			$smarty->assign("download_attempts",$objCart->cart_item_download_counter);
			$smarty->assign("quote_price",$objCart->cart_item_price);	
			$smarty->assign("total",$objCart->cart_total);
			$smarty->assign("message_id",$objCart->cart_message_id);
			$smarty->assign("message_username",$objCart->cart_message_username);
			$smarty->assign("message_user_email",$objCart->cart_message_user_email);
			$smarty->assign("message_text",$objCart->cart_message_text);
			$smarty->assign("message_time",$objCart->cart_message_time);
			$smarty->assign("message_on",$objCart->cart_message_on);
			
			//Shane copied this code to add customer details onto cart page. Altered class variable to select cart user id.
			require_once ("class.PixariaUser.php");	
		
			$objPixariaUser = new PixariaUser($objCart->cart_userid);
			
			$smarty->assign("first_name",$objPixariaUser->getFirstName());
			$smarty->assign("family_name",$objPixariaUser->getFamilyName());
			$smarty->assign("email_address",$objPixariaUser->getEmail());
			$smarty->assign("address1",$objPixariaUser->getAddress1());
			$smarty->assign("address2",$objPixariaUser->getAddress2());
			$smarty->assign("address3",$objPixariaUser->getAddress3());
			$smarty->assign("city",$objPixariaUser->getCity());
			$smarty->assign("region",$objPixariaUser->getRegion());
			$smarty->assign("country",$objPixariaUser->getCountryName());
			$smarty->assign("postal_code",$objPixariaUser->getPostCode());
			
			
			// Give Smarty the URI of the current page
			$smarty->assign("referer",$_SERVER['REQUEST_URI']);
				
			// Set the HTML page title and masthead text
			$smarty->assign("page_title","View transaction");
			
			switch ($cfg['set']['store_type']) {
			
				case 10:
				
					// Display the cart page
					$smarty->pixDisplay('admin.sales/basic.view.tpl');
					
				break;
				
				case 11: case 12: // Basic or advanced mode
				
					// Display the cart page
					$smarty->pixDisplay('admin.sales/intermediate.view.tpl');
					
				break;
				
			}
			
		} else { // There is nothing in the cart or there is no cart to view
			
			// Set the HTML page title and masthead text
			$smarty->assign("page_title","The cart is empty");
		
			// Display the empty cart page
			$smarty->pixDisplay('admin.sales/cart.empty.tpl');

		}

	break;
	
	default:
	
		// Set redirect url
		$meta_url	= $cfg['sys']['base_url'].$cfg['fil']['index_account'];
		
		// Print out the waiting screen
		$smarty->pixWaiting($meta_url,"1");
		
		// Stop running script
		exit;
	
	break;

}

?>