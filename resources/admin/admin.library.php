<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "images";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Define the navigation section for the admin toolbar
$smarty->assign("toolbar","library");

// Check that the user is a SuperUser
pix_authorise_user("administrator");

// Load view option for listbox into Smarty
$smarty->assign("cmd",$objEnvData->fetchGlobal('cmd'));

// Assign the number of images in the library to Smarty
$smarty->assign("image_count",pix_image_count());

$objAdminLibrary = new AdminLibrary();

/*
*
*	
*
*/
class AdminLibrary {
	
	var $action_complete = false;
	
	function AdminLibrary() {
		
		global $objEnvData, $cfg;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		// Perform the correct command for this page
		switch ($objEnvData->fetchGlobal('cmd')) {
			
			case "formCreateGalleryWithImages":
				$this->formCreateGalleryWithImages();
			break;
			
			case "actionDeleteFromLibrary":
				$this->actionDeleteFromLibrary();
			break;
			
			case "formDeleteFromLibrary":
				$this->formDeleteFromLibrary();
			break;
			
			case "formAddImagesToGallery":
				$this->formAddImagesToGallery();
			break;
			
			case "showFormBatchEdit":
				$this->showFormBatchEdit();
			break;
			
			case "showOrphanImages":
				$this->showOrphanImages();
			break;
			
			case "showGalleryListAlphabetical":
				header('Location: ' . $cfg['sys']['base_url'] . $cfg['fil']['admin_gallery']);
			break;
			
			case "showGalleryListHierarchical": default:
				header('Location: ' . $cfg['sys']['base_url'] . $cfg['fil']['admin_gallery']);
			break;
		
		}
	
	}
		
	/*
	*
	*	This method shows the global batch edit form
	*
	*/
	function showFormBatchEdit () {
		
		// Display the global batch edit form
		showFormBatchEdit();
	
	}
	
	/*
	*
	*	Method allows user to add images to a gallery
	*
	*/
	function formAddImagesToGallery () {
	
		global $cfg, $objEnvData, $smarty;
		
		// Show the global form to add images to a gallery
		formAddImagesToGallery();
	
	}
	
}

?>