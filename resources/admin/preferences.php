<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "site";

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Check that the user is a administrator
pix_authorise_user("administrator");

new Preferences();

class Preferences {

	/*
	*
	*
	*
	*/
	function Preferences () {
	
		global 	$objEnvData,
				$ses,
				$cfg;

		// Load an initialise the database class
		require_once ('class.Database.php');
		require_once (SYS_BASE_PATH . 'resources/classes/class.InputData.php');

		$this->db		= new Database();
		$this->input	= new InputData();
		$this->view 	= new Smarty_Pixaria();
		$this->config	= $cfg;
		$this->session 	= $ses;
				
		// Load and initialise the JSON class
		require_once (SYS_BASE_PATH . 'resources/classes/class.JSON.php');
		$this->json = new Pixaria_JSON();
		
		switch ($this->input->name('cmd')) {
			
			case 'savePreferences':
				$this->savePreferences();
			break;
			
			default:
				$this->index();
			break;
			
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function index () {
		
		$sql = "SELECT 	 pref.name
						,pref.default_value
						,uprf.value
						
				FROM ".PIX_TABLE_PREF." pref
		
				JOIN ".PIX_TABLE_UPRF." uprf ON pref.preference_id = uprf.preference_id
				
				WHERE user_id = '".$this->db->escape($this->session['psg_userid'])."'";
		
		list (
			
			$user_preference_name,
			$user_preference_default_value,
			$user_preference_value
		
		) = $this->db->rowsAsColumns($sql);
		
		$this->view->assign('user_preference_name',$user_preference_name);
		$this->view->assign('user_preference_default_value',$user_preference_default_value);
		$this->view->assign('user_preference_value',$user_preference_value);
		
		list (
			
			$preference_id,
			$preference_name,
			$preference_default_value
		
		) = $this->db->rowsAsColumns("SELECT * FROM ".PIX_TABLE_PREF."");
		
		$this->view->assign('preference_id',$preference_id);
		$this->view->assign('preference_name',$preference_name);
		$this->view->assign('preference_default_value',$preference_default_value);
		
		list (
			
			$charset_id,
			$charset_name,
			$charset_code
		
		) = $this->db->rowsAsColumns("SELECT * FROM ".PIX_TABLE_CHAR."");
		
		$this->view->assign('charset_id',$charset_id);
		$this->view->assign('charset_name',$charset_name);
		$this->view->assign('charset_code',$charset_code);
		
		
		
		$this->view->assign("page_title","Edit Preferences");
		
		$this->view->display('preferences/index.html');
	
		// Stop running the script here
		exit;
		
	}
	
	/*
	*
	*
	*
	*/
	function savePreferences () {
		
		list (
			
			$preference_id,
			$preference_name
		
		) = $this->db->rowsAsColumns("SELECT * FROM ".PIX_TABLE_PREF);
		
		$preferences = $this->input->name('preferences');
		
		foreach ($preferences as $key => $value) {
			
			$preference_key = array_search($key, $preference_name);
			
			$id = $preference_id[$preference_key];
			
			switch ($value) {
				
				case 'checkbox':
					$data = $this->input->checkbox($key);
				break;
				
				case 'html':
					$data = $this->input->html($key);
				break;
				
				default:
					$data = $this->input->raw($key);
				break;
			
			}
			
			if ($this->settingsProcessor($id, $data)) {
				
				$changed[] = $key;
				
			}
			
		}
		
		if (is_array($changed)) {
			
			$changed = $this->json->encodeArrayAsJson ($changed);
			
			$this->json->sendJson($changed);
			
		} else {
		
			return false;
			
		}
		
	}
	
	/*
	*
	*	Apply updated settings
	*
	*/
	function settingsProcessor ($preference_id, $value) {
		
		if (is_numeric($preference_id)) {
			
			// Delete any previous records for this preference
			$sql = "DELETE FROM ".PIX_TABLE_UPRF."
					
					WHERE preference_id = '".$preference_id."'
					
					AND user_id = '".$this->db->escape($this->session['psg_userid'])."'";
					
			$this->db->sqlQuery($sql);
			
			// Create a new record for this preference
			$sql = "INSERT INTO ".PIX_TABLE_UPRF."
					
					VALUES (
						
						'0',
						'".$preference_id."',
						'".$this->db->escape($this->session['psg_userid'])."',
						'".$this->db->escape($value)."'
					)";
					
			$this->db->sqlQuery($sql);
			
			// Update settings in the configuration data array
			$this->session['preferences'][$name] = $value;
			
			// Update settings in the template object
			$this->view->assign("pref_" . $name, $value);
		
		}
		
		return true;

	}
	
}

?>