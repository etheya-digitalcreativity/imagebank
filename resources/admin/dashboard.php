<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Check that the user is a SuperUser
pix_authorise_user("administrator");

// Initialise the class for this page
$dashboard = new Dashboard();

class Dashboard {
	
	function Dashboard (){
				
		// Load the database class
		require_once ('class.Database.php');
		require_once (SYS_BASE_PATH . 'resources/classes/class.InputData.php');
		
		// Create required objects
		$this->db		= new Database();
		$this->input	= new InputData();
		$this->view		= new Smarty_Pixaria;
	
		switch ($this->input->name('cmd')) {
		
			case "status":
				$this->view->assign("admin_page_section","tools");
				$this->status();
			break;
			
			case 'flushCache':
				$this->flushCache();
			break;
			
			case "updateImageKeywords":
				$this->view->assign("admin_page_section","site");
				$this->updateImageKeywords();
			break;
		
			case "updateImageDimensions":
				$this->view->assign("admin_page_section","site");
				$this->updateImageDimensions();
			break;
		
			case "showPixariaNews":
				$this->view->assign("admin_page_section","tools");
				$this->showPixariaNews();
			break;
		
			case "optimiseDatabase":
				$this->view->assign("admin_page_section","tools");
				$this->optimiseDatabase();
			break;
		
			default:
				$this->view->assign("admin_page_section","site");
				$this->index();
			break;
			
		}
	
	}

	/*
	*	Shows the Pixaria dashboard page
	*	
	*	@return void
	*/
	function index () {
		
		// Get the Smarty object
		global $cfg, $ses;
		
		// Get information about this installation
		$this->getData();
		
		$sql = "SELECT account_status, email_address FROM ".PIX_TABLE_USER." WHERE userid = ".$this->db->escape($ses['psg_userid']);
		
		list (
			
			$user_account_status,
			$user_email_address
			
		) = $this->db->row($sql);
		
		$this->view->assign('user_email_address',$user_email_address);
		$this->view->assign('user_account_status',$user_account_status);
		$this->view->assign('user_id',$ses['psg_userid']);
		
		$this->view->assign("image_count",$this->image_count);
		$this->view->assign("temporary_stat",$this->temporary_stat);
		$this->view->assign("temporary_perms",$this->temporary_perms);
		$this->view->assign("base_incoming_stat",$this->base_incoming_stat);
		$this->view->assign("incoming_perms",$this->incoming_perms);
		$this->view->assign("base_library_stat",$this->base_library_stat);
		$this->view->assign("library_perms",$this->library_perms);
		$this->view->assign("base_archive_stat",$this->base_archive_stat);
		$this->view->assign("archive_perms",$this->archive_perms);
		$this->view->assign("php_version",$this->php_version);
		$this->view->assign("server_software",$this->server_software);
		$this->view->assign("gd_version",$this->gd_version);
		$this->view->assign("gd_upload_stat",$this->gd_upload_stat);
		$this->view->assign("gd_popcard_stat",$this->gd_popcard_stat);
		$this->view->assign("mysql_host_info",$this->mysql_host_info);
		$this->view->assign("mysql_server_info",$this->mysql_server_info);
		$this->view->assign("mysql_client_info",$this->mysql_client_info);
		$this->view->assign("memory_limit",$this->memory_limit);
		$this->view->assign("upload_max_filesize",$this->upload_max_filesize);
		$this->view->assign("pixaria_table_count",$this->pixaria_table_count);
		$this->view->assign("ext_gd",$this->ext_gd);
		$this->view->assign("ext_exif",$this->ext_exif);
		$this->view->assign("ext_zlib",$this->ext_zlib);
		$this->view->assign("ext_xml",$this->ext_xml);
		$this->view->assign("register_globals",$this->register_globals);
		$this->view->assign("active_users",$this->active_users);
		$this->view->assign("inactive_users",$this->inactive_users);
		$this->view->assign("all_users",$this->all_users);
		
		// Tell Smarty if we're going to show the list after a completed action
		$this->view->assign("action_complete",$this->action_complete);
		
		// Define the navigation section for the admin toolbar
		$this->view->assign("toolbar","status");
		
		// Set the page title
		$this->view->assign("page_title","Pixaria Dashboard");
		
		// Display the admin status page
		$this->view->display('dashboard/index.html');
	
	}
	
	/*
	*	Shows the software diagnostics page
	*	
	*	@return void
	*/
	function status () {
		
		// Get information about this installation
		$this->getData();
		
		$this->view->assign("image_count",$this->image_count);
		$this->view->assign("temporary_stat",$this->temporary_stat);
		$this->view->assign("temporary_perms",$this->temporary_perms);
		$this->view->assign("base_incoming_stat",$this->base_incoming_stat);
		$this->view->assign("incoming_perms",$this->incoming_perms);
		$this->view->assign("base_library_stat",$this->base_library_stat);
		$this->view->assign("library_perms",$this->library_perms);
		$this->view->assign("base_archive_stat",$this->base_archive_stat);
		$this->view->assign("archive_perms",$this->archive_perms);
		$this->view->assign("php_version",$this->php_version);
		$this->view->assign("server_software",$this->server_software);
		$this->view->assign("gd_version",$this->gd_version);
		$this->view->assign("gd_upload_stat",$this->gd_upload_stat);
		$this->view->assign("gd_popcard_stat",$this->gd_popcard_stat);
		$this->view->assign("mysql_host_info",$this->mysql_host_info);
		$this->view->assign("mysql_server_info",$this->mysql_server_info);
		$this->view->assign("mysql_client_info",$this->mysql_client_info);
		$this->view->assign("memory_limit",$this->memory_limit);
		$this->view->assign("upload_max_filesize",$this->upload_max_filesize);
		$this->view->assign("pixaria_table_count",$this->pixaria_table_count);
		$this->view->assign("ext_gd",$this->ext_gd);
		$this->view->assign("ext_exif",$this->ext_exif);
		$this->view->assign("ext_zlib",$this->ext_zlib);
		$this->view->assign("ext_xml",$this->ext_xml);
		$this->view->assign("register_globals",$this->register_globals);
		$this->view->assign("active_users",$this->active_users);
		$this->view->assign("inactive_users",$this->inactive_users);
		$this->view->assign("all_users",$this->all_users);
		
		// Define the navigation section for the admin toolbar
		$this->view->assign("toolbar","status");
		
		// Set the page title
		$this->view->assign("page_title","Software Status");
		
		// Display the admin status page
		$this->view->display('dashboard/status.html');
	
	}
	
	/*
	*	
	*/
	function flushCache () {
		
		// Clear the template cache
		pix_clear_cache();
	
	}
	
	/*
	*	Get site news from Pixaria and show
	*/
	function showPixariaNews () {
	
		global $cfg;
		
		$mysql_server		= urlencode($this->mysql_server_info);
		$mysql_client		= urlencode($this->mysql_client_info);
		$php_version		= urlencode($this->php_version);
		$pixaria_version	= urlencode($cfg['set']['software_version']);
		$site_name			= urlencode($cfg['set']['site_name']);
		
		$env_data			= "mysql_server=$mysql_server&mysql_client=$mysql_client&php_version=$php_version&pixaria_version=$pixaria_version&site_name=$site_name&request_version=4";
		
		$data = unserialize( $this->getRemoteFile('www.pixaria.com','GET','/news/pixaria',$env_data));
		
		$this->view->assign("news_id",$data['news_id']);
		$this->view->assign("news_date",$data['news_date']);
		$this->view->assign("news_timeago",$data['news_timeago']);
		$this->view->assign("news_title",$data['news_title']);
		$this->view->assign("news_stitle",$data['news_stitle']);
		$this->view->assign("news_content",$data['news_content']);
		$this->view->assign("news_href",$data['news_href']);
		$this->view->assign("news_cat_pixaria",$data['news_cat_pixaria']);
		$this->view->assign("news_cat_divestock",$data['news_cat_divestock']);
		$this->view->assign("news_cat_trivia",$data['news_cat_trivia']);
		$this->view->assign("news_cat_announcement",$data['news_cat_announcement']);
		
		// Display the admin status page
		$this->view->display('dashboard/news.html');
		
	}
	
	/*
	*
	*	 Defragments and clears empty space in all the database tables
	*
	*/
	function optimiseDatabase () {
		
		$sql = "OPTIMIZE TABLE 	`".PIX_TABLE_DOMN."` ,
								`".PIX_TABLE_BLOG."` ,
								`".PIX_TABLE_CAAR."` ,
								`".PIX_TABLE_CAOB."` ,
								`".PIX_TABLE_CART."` ,
								`".PIX_TABLE_CRME."` ,
								`".PIX_TABLE_CATG."` ,
								`".PIX_TABLE_CAME."` ,
								`".PIX_TABLE_CRME."` ,
								`".PIX_TABLE_DLOG."` , 
								`".PIX_TABLE_GALL."` ,
								`".PIX_TABLE_GORD."` ,
								`".PIX_TABLE_GVIE."` ,
								`".PIX_TABLE_GRPS."` ,
								`".PIX_TABLE_GRME."` ,
								`".PIX_TABLE_IMGS."` ,
								`".PIX_TABLE_IPRD."` ,
								`".PIX_TABLE_IMVI."` ,
								`".PIX_TABLE_LBOX."` ,
								`".PIX_TABLE_LBME."` ,
								`".PIX_TABLE_PSDL."` ,
								`".PIX_TABLE_PROD."` ,
								`".PIX_TABLE_SRCH."` ,
								`".PIX_TABLE_SLOG."` ,
								`".PIX_TABLE_SRCS."` ,
								`".PIX_TABLE_SETT."` ,
								`".PIX_TABLE_SGRP."` ,
								`".PIX_TABLE_SGME."` ,
								`".PIX_TABLE_TMSG."` ,
								`".PIX_TABLE_USER."` ,
								`".PIX_TABLE_USDA."` ,
								`".PIX_TABLE_ULOG."`";
		
		$this->db->query($sql);

		$this->action_complete = 1;

		$meta_url	= $_SERVER['HTTP_REFERER'];
		
		// Print out a holding page telling the user
		// that their request is being processed
		$this->view->pixWaiting($meta_url,"1");

	}
	
	/*
	*	Upgrade keywords to be compatible with version 2.0 database
	*	
	*	@return void
	*/
	function updateImageKeywords () {
		
		global $smarty, $cfg;
		
		$sql = "SELECT image_id,image_keywords FROM ".PIX_TABLE_IMGS;
		
		$result = $this->db->rows($sql);
		
		if (is_array($result)) {
		
			foreach ($result as $key => $value) {
				
				$new = array();
				$image_id = $value['image_id'];
				$keywords = $value['image_keywords'];
				
				if (!eregi(",",$keywords)) {
				
					$keywords = explode(" ",$keywords);
					
					if (is_array($keywords)) {
					
						foreach ($keywords as $item => $keyword) {
						
							$keyword = eregi_replace("QZQ", " ",$keyword);
							$keyword = eregi_replace("QQ", "",$keyword);
							$new[] = $keyword;
						}
					
					}
					
					$updated = implode(", ",$new);
					
					$sql = "UPDATE ".PIX_TABLE_IMGS." SET image_keywords = '".$this->db->escape($updated)."' WHERE image_id = '$image_id'";
					
					$this->db->query($sql);
				
				}
				
			}
		
		}
		
		$sql = "UPDATE ".PIX_TABLE_SETT." SET value = '1' WHERE name = 'keywords_updated'";
		
		$this->db->query($sql);
		
		// Tell Smarty if an action is complete
		$this->action_complete = true;
		
		$this->view->assign("set_keywords_updated",1);
		
		// Generate a redirection URL 
		$this->index();			

	}
	
	/*
	*	Upgrade image sizes for all items in the library
	*	
	*	@return void
	*/
	function updateImageDimensions () {
		
		global $smarty, $cfg;
		
		$sql = "SELECT image_id, image_path, image_filename
				
				FROM ".PIX_TABLE_IMGS."
				
				WHERE icon_w = '0' 
				
				OR smal_w = '0'
				
				OR larg_w = '0'
				
				OR comp_w = '0'";
		
		$result = $this->db->rows($sql, MYSQL_BOTH);
				
		if (is_array($result)) {
		
			foreach ($result as $key => $value) {
				
				$new = array();
				$image_id 			= $value['image_id'];
				$image_path 		= $value['image_path'];
				$image_filename 	= $value['image_filename'];
				
				list($icon_w, $icon_h) = @getimagesize($cfg['sys']['base_library'] . $image_path . "/32x32/" . $image_filename);
				list($smal_w, $smal_h) = @getimagesize($cfg['sys']['base_library'] . $image_path . "/80x80/" . $image_filename);
				list($larg_w, $larg_h) = @getimagesize($cfg['sys']['base_library'] . $image_path . "/160x160/" . $image_filename);
				list($comp_w, $comp_h) = @getimagesize($cfg['sys']['base_library'] . $image_path . "/".COMPING_DIR."/" . $image_filename);
				list($orig_w, $orig_h) = @getimagesize($cfg['sys']['base_library'] . $image_path . "/original/" . $image_filename);
				
				$sql = "UPDATE ".PIX_TABLE_IMGS." 
						
						SET
						
						icon_w = '$icon_w',
						icon_h = '$icon_h',
						smal_w = '$smal_w',
						smal_h = '$smal_h',
						larg_w = '$larg_w',
						larg_h = '$larg_h',
						comp_w = '$comp_w',
						comp_h = '$comp_h',
						orig_w = '$orig_w',
						orig_h = '$orig_h'
						
						WHERE image_id = '$image_id'";
				
				$this->db->query($sql);
				
			}
				
		}
		
		$this->db->query("UPDATE ".PIX_TABLE_SETT." SET value = '1' WHERE name = 'db_update_230'");
		
		// Tell Smarty if an action is complete
		$this->action_complete = true;
		
		$this->view->assign("set_db_update_230",1);
		
		// Generate a redirection URL 
		$this->index();			

	}
	
	/*
	*	Sets all the internal properties for this class
	*	
	*	@return int number of users in the system
	*/
	function getUserCount ($type="") {
		
		if ($type == "active") {
		
			$sql = "SELECT COUNT(userid) FROM ".PIX_TABLE_USER." WHERE userid != '' AND account_status = '1'";
		
		} elseif ($type == "inactive") {
		
			$sql = "SELECT COUNT(userid) FROM ".PIX_TABLE_USER." WHERE userid != '' AND account_status != '1'";
		
		} else {
		
			$sql = "SELECT COUNT(userid) FROM ".PIX_TABLE_USER." WHERE userid != ''";
		
		}
		
		$result = $this->db->row($sql);
		
		if ($result == "") { return "0"; } else { return $result; }
		
	}
	
	/*
	*	SETS ALL THE INTERNAL PROPERTIES FOR THIS CLASS
	*/
	function getData () {
		
		global $cfg, $ses;
		
		// Find the total num ber of images
		$this->image_count = pix_image_count();
		
		// Find the numbers of users in the system
		$this->active_users 	= $this->getUserCount('active');
		$this->inactive_users 	= $this->getUserCount('inactive');
		$this->all_users 		= $this->getUserCount();
		
		// Check that the temporary items folder is properly set
		if (file_exists($cfg['set']['temporary'])) {
			$stat_result = substr(sprintf('%o', @fileperms($cfg['set']['temporary'])), -4);
			if ($stat_result == "1777" || $stat_result == "0777") {
				$this->temporary_stat = "Y";
			} else {
				$this->temporary_stat = "N";
			}
		} else {
			$this->temporary_stat = "M";
		}
		
		// Assign the Unix permissions for the temp directory
		$this->temporary_perms = $this->getFilePermissions($cfg['set']['temporary']);
		
		// Check that the main library folder is properly set
		if (file_exists($cfg['sys']['base_incoming'])) {
			if (is_writable($cfg['sys']['base_incoming'])) {
				$this->base_incoming_stat = "Y";
			} else {
				$this->base_incoming_stat = "N";
			}
		} else {
			$this->base_incoming_stat = "M";
		}
		
		
		// Assign the Unix permissions for the incoming directory
		$this->incoming_perms = $this->getFilePermissions($cfg['sys']['base_incoming']);
		
		// Check that the main library folder is properly set
		if (file_exists($cfg['sys']['base_library'])) {
			if (is_writable($cfg['sys']['base_library'])) {
				$this->base_library_stat = "Y";
			} else {
				$this->base_library_stat = "N";
			}
		} else {
			$this->base_library_stat = "M";
		}
		
		// Assign the Unix permissions for the library directory
		$this->library_perms = $this->getFilePermissions($cfg['sys']['base_library']);
		
		// Check that the main archive folder is properly set
		if (file_exists($cfg['sys']['base_library']."archive/")) {
			if (is_writable($cfg['sys']['base_library']."archive/")) {
				$this->base_archive_stat = "Y";
			} else {
				$this->base_archive_stat = "N";
			}
		} else {
			$this->base_archive_stat = "M";
		}
		
		// Check the PHP version
		if (function_exists("phpversion")) {
			$this->php_version = phpversion();
		} else {
			$this->php_version = "Function disabled";
		}
		
		// Server software information
		$this->server_software = $_SERVER['SERVER_SOFTWARE'];
		
		
		// GD graphics library version
		$this->gd_version = $cfg['set']['gd_version'][0];
		
		
		// Uploading enabled
		if ($cfg['set']['gd_upload']) {
			$this->gd_upload_stat = TRUE;
		} else {
			$this->gd_upload_stat = FALSE;
		}
		
		// PopCard enabled
		if ($cfg['set']['gd_popcard']) {
			$this->gd_popcard_stat = "TRUE";
		} else {
			$this->gd_popcard_stat = "FALSE";
		}
		
		// MySQL host information
		$this->mysql_host_info = mysql_get_host_info();
		
		// MySQL host information
		$this->mysql_server_info = mysql_get_server_info();
		
		// MySQL host information
		$this->mysql_client_info = mysql_get_client_info();
		
		// MySQL host information
		$this->memory_limit = get_cfg_var(memory_limit);
		
		// MySQL host information
		$this->upload_max_filesize = get_cfg_var(upload_max_filesize);
		
		// Is the Boutell GD Graphics Library library loaded
		$this->ext_gd = @extension_loaded('gd');
		
		// Is the EXIF library loaded
		$this->ext_exif = @extension_loaded('exif');
		
		// Is the Zlib library loaded
		$this->ext_zlib = @extension_loaded('zlib');
		
		// Is the XML library loaded
		$this->ext_xml = @extension_loaded('xml');
		
		$this->pixaria_table_count = @mysql_num_rows(@mysql_query("SHOW TABLES"));
		
	}
	
	/*
	*	Get permissions of a file or directory
	*	
	*	@return string $info File permissions in the form 'drwxrwxrwt'
	*/
	function getFilePermissions($path) {
		
		// Courtesy http://uk.php.net/
		
		$perms = @fileperms($path);
		
		if (($perms & 0xC000) == 0xC000) {
		   // Socket
		   $info = 's';
		} elseif (($perms & 0xA000) == 0xA000) {
		   // Symbolic Link
		   $info = 'l';
		} elseif (($perms & 0x8000) == 0x8000) {
		   // Regular
		   $info = '-';
		} elseif (($perms & 0x6000) == 0x6000) {
		   // Block special
		   $info = 'b';
		} elseif (($perms & 0x4000) == 0x4000) {
		   // Directory
		   $info = 'd';
		} elseif (($perms & 0x2000) == 0x2000) {
		   // Character special
		   $info = 'c';
		} elseif (($perms & 0x1000) == 0x1000) {
		   // FIFO pipe
		   $info = 'p';
		} else {
		   // Unknown
		   $info = 'u';
		}
		
		// Owner
		$info .= (($perms & 0x0100) ? 'r' : '-');
		$info .= (($perms & 0x0080) ? 'w' : '-');
		$info .= (($perms & 0x0040) ?
				   (($perms & 0x0800) ? 's' : 'x' ) :
				   (($perms & 0x0800) ? 'S' : '-'));
		
		// Group
		$info .= (($perms & 0x0020) ? 'r' : '-');
		$info .= (($perms & 0x0010) ? 'w' : '-');
		$info .= (($perms & 0x0008) ?
				   (($perms & 0x0400) ? 's' : 'x' ) :
				   (($perms & 0x0400) ? 'S' : '-'));
		
		// World
		$info .= (($perms & 0x0004) ? 'r' : '-');
		$info .= (($perms & 0x0002) ? 'w' : '-');
		$info .= (($perms & 0x0001) ?
				   (($perms & 0x0200) ? 't' : 'x' ) :
				   (($perms & 0x0200) ? 'T' : '-'));
		
		return $info;
	
	}
	
	/*
	*	Get a remote file
	*	
	*	@return string $file Contents of a remote URL
	*/
	function getRemoteFile ($host, $method, $path, $data) {
	
		$method = strtoupper($method);        
		
		if ($method == "GET") {
			$path.= '?'.$data;
		}    
		
		$filePointer = fsockopen($host, 80, $errorNumber, $errorString, 10);
		
		if (!$filePointer) {
			logEvent('debug', 'Failed opening http socket connection: '.$errorString.' ('.$errorNumber.')<br/>\n');
			return false;
		}
		
		$requestHeader = $method." ".$path."  HTTP/1.1\r\n";
		$requestHeader.= "Host: ".$host."\r\n";
		$requestHeader.= "User-Agent:      Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1) Gecko/20061010 Firefox/2.0\r\n";
		$requestHeader.= "Content-Type: application/x-www-form-urlencoded\r\n";
		
		if ($method == "POST")
		{
		$requestHeader.= "Content-Length: ".strlen($data)."\r\n";
		}
		
		$requestHeader.= "Connection: close\r\n\r\n";
		
		if ($method == "POST")
		{
		$requestHeader.= $data;
		}            
		
		fwrite($filePointer, $requestHeader);
		
		$responseHeader = '';
		$responseContent = '';
		
		do 
		{
		$responseHeader.= fread($filePointer, 1); 
		}
		while (!preg_match('/\\r\\n\\r\\n$/', $responseHeader));
		
		
		if (!strstr($responseHeader, "Transfer-Encoding: chunked"))
		{
		while (!feof($filePointer))
		{
		   $responseContent.= fgets($filePointer, 128);
		}
		}
		else 
		{
		
		while ($chunk_length = hexdec(fgets($filePointer))) 
		{
		   $responseContentChunk = '';
		
		  
		   $read_length = 0;
		   
		   while ($read_length < $chunk_length) 
		   {
			   $responseContentChunk .= fread($filePointer, $chunk_length - $read_length);
			   $read_length = strlen($responseContentChunk);
		   }
		
		   $responseContent.= $responseContentChunk;
		   
		   fgets($filePointer);
		   
		}
		
		}
		
		return chop($responseContent);
		
	}
	
}

?>