<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "images";

// Check that the user is a SuperUser
pix_authorise_user("administrator");

// Send HTTP header and don't cache
pix_http_headers("html","0");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

//print_r($_POST);exit;

$objAdminSearch = new AdminSearch();

class AdminSearch {
	
	// Search terms
	var $keywords_any;
	var $keywords_all;
	var $keywords_not;
	var $gallery_id;
	var $srch_date_st;
	var $srch_date_en;
	var $date_srch;
	var $srch_date_is_dd;
	var $srch_date_is_mm;
	var $srch_date_is_yy;
	var $srch_date_st_dd;
	var $srch_date_st_mm;
	var $srch_date_st_yy;
	var $srch_date_en_dd;
	var $srch_date_en_mm;
	var $srch_date_en_yy;
	var $model_release;
	var $property_release;
	var $rights_managed;
	
	// Search data output
	var $output_total;
	var $output_keywords_all;
	var $output_keywords_any;
	var $output_keywords_not;
	var $output_gallery_id;
	var $output_srch_date_st;
	var $output_srch_date_en;
	var $output_date_srch;
	var $output_date_is_time;
	var $output_date_st_time;
	var $output_date_en_time;
	var $output_model_release;
	var $output_property_release;
	var $output_rights_managed;
	var $output_gallery_list;
	var $output_ipages;
	var $output_image_data;
	
	function AdminSearch () {
	
		global $cfg, $ses, $objEnvData, $smarty;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		$this->thumbnail_page	= $objEnvData->fetchGlobal('ipg'); // The image thumbnail icon page we are currently on
		$this->search_image		= $objEnvData->fetchGlobal('img'); // The number of a specific image we want to view in the search results list

		require_once ('class.PixariaSearch.php');
		
		if ($objEnvData->fetchGlobal('mode') == "popup") { $type = "admin_popup"; } else { $type = "admin"; }
		
		$this->objSearch = new PixariaSearch($this->thumbnail_page, null, $type, $objEnvData->fetchGlobal('sid'), $objEnvData->fetchGlobal('search_within'));
		
		// Get information about this lightbox
		$this->search_image_list		= $this->objSearch->getSearchMultiImageData();
		$this->ipages					= $this->objSearch->getMultiImagePageNavigation();
		$this->search_image_data		= $this->objSearch->getSearchImageData();
		$this->search_results_count		= $this->objSearch->getSearchResultsCount();
		
		$this->keywords_simple_boolean	= $this->objSearch->keywords_simple_boolean;
		$this->keywords_all				= $this->objSearch->keywords_all;
		$this->keywords_any				= $this->objSearch->keywords_any;
		$this->keywords_not				= $this->objSearch->keywords_not;
		
		switch ($objEnvData->fetchGlobal('cmd')) {
		
			case "doSearch":
				$this->doSearch();
			break;
			
			case "addImagesToGallery":
				$this->addImagesToGallery();
			break;
			
			case "batchEditImages":
				$this->batchEditImages();
			break;
			
			case "deleteImages":
				$this->deleteImages();
			break;
			
			case "confirmDeleteImages":
				$this->confirmDeleteImages();
			break;
			
			case "formPopupSearch":
				$this->formPopupSearch();
			break;
			
			case "actionPopupSearch":
				$this->actionPopupSearch();
			break;
			
			default:
				$this->showSearchForm();
			break;
		
		}
		
	}
	
	/*
	*	This method takes an image ID or array of image IDs and passes the user on to the
	*	page where they can then choose which gallery to add the images to
	*/
	function addImagesToGallery () {
		
		// Show the global form to add images to a gallery
		formAddImagesToGallery();
	
	}
	
	/*
	*	This method takes an image ID or array of image IDs and passes the user on to the
	*	page where they can batch edit multiple images or otherwise edit a single image
	*/
	function batchEditImages () {
	
		global $cfg, $objEnvData, $smarty;
		
		$images = $objEnvData->fetchGlobal('images');
		
		if (is_array($images) && count($images) == 1) { // We are editing a single image
			
			// Get the image_id
			$image_id = $images[0];
			
			// Generate a redirection URL 
			$meta_url	= $cfg['sys']['base_url'] . $cfg['fil']['admin_image']."?cmd=edit&amp;image_id=$image_id";
						
			// Show redirect page
			$smarty->pixWaiting($meta_url,"1");
			
		} elseif (is_array($images) && count($images) > 1) { // We are editing multiple images
			
			// Display the global batch edit form
			showFormBatchEdit();
			exit;
			
		} else { // No images were selected
				
			// Set the page title
			$smarty->assign("page_title","No images selected");
			
			// Set the title to show
			$smarty->assign("error_title","You didn't select any images to edit");
			
			// Set the error message
			$smarty->assign("error_message","You didn't select any images to edit, please <a href=\"javascript:history.go(-1);\">go back</a> to the previous page and make a selection.");
			
			// Display the html header
			$smarty->pixDisplay('admin.snippets/admin.html.error.tpl');
		
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function deleteImages () {
		
		global $cfg, $objEnvData;
		
		// Get an array of the images to be deleted
		$images = $objEnvData->fetchGlobal('images');
		
		// Set the URL of the next page
		$action = $cfg['sys']['base_url'].$cfg['fil']['admin_library_search'];
		
		// Method of deletion to use (trash image files or move to incoming)
		$delete_option = $objEnvData->fetchGlobal('delete_option');
	
		// Show confirmation page for deleting images
		resultDeleteImageFromLibrary($images,$action,$delete_option,$hidden_name,$hidden_value);
	
	}
	
	/*
	*
	*	
	*
	*/
	function confirmDeleteImages () {
	
		global $cfg, $objEnvData;
		
		// Get an array of the images to be deleted
		$images = $objEnvData->fetchGlobal('images');
		
		// Set the URL of the next page
		$action = $cfg['sys']['base_url'].$cfg['fil']['admin_library_search'];
		
		// Hidden form field names and values for the HTML output as two arrays
		$hidden_name	= array("cmd");
		$hidden_value	= array("deleteImages");
		
		// Show confirmation page for deleting images
		formDeleteImageFromLibrary($images,$action,$hidden_name,$hidden_value);

	}
	
	/*
	*
	*	
	*
	*/
	function getImageDirectories () {
	
		$sql = "SELECT image_path FROM ".PIX_TABLE_IMGS." GROUP BY image_path";
		
		$result = $this->_dbl->sqlSelectRows($sql);
		
		if (is_array($result)) {
			
			foreach ($result as $key => $value) {
			
				$image_path[]	= $value['image_path'];
				
			}
			
			return $image_path;
			
		} else {
			
			return false;
		
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function showSearchForm () {
		
		global $cfg, $ses, $objEnvData, $smarty;
		
		// Load an array of nested galleries
		$gallery_list = galleryListingArray();
		
		// Load the category list data into Smarty
		$smarty->assign("menu_gallery_title",$gallery_list[0]);
		$smarty->assign("menu_gallery_id",$gallery_list[1]);
		
		// Get image paths from database
		$smarty->assign("image_path",$this->getImageDirectories());
		
		if ($this->output_total == 0): $smarty->assign("results_count","NULL"); endif;
		
		$this->output_image_colr_r 			= $this->objSearch->image_colr_r;
		$this->output_image_colr_g 			= $this->objSearch->image_colr_g;
		$this->output_image_colr_b 			= $this->objSearch->image_colr_b;
		$this->output_image_colr_mod 		= $this->objSearch->getImageModRGB();
		$this->output_image_colr_dec 		= $this->objSearch->getImageDecRGB();
		$this->output_image_colr_hex 		= $this->objSearch->getImageHexRGB();

		$smarty->assign("image_colr_mod",$this->output_image_colr_mod);
		$smarty->assign("image_colr_hex",$this->output_image_colr_hex);
		$smarty->assign("image_colr_dec",$this->output_image_colr_dec);
		$smarty->assign("image_colr_mod",$this->output_image_colr_mod);
		$smarty->assign("image_colr_r",$this->objSearch->getImageModRGB());
		$smarty->assign("image_colr_g",$this->objSearch->getImageModRGB());
		$smarty->assign("image_colr_b",$this->objSearch->getImageModRGB());

		// Get user list for search by userid
		$users = $this->getUsers();
		$smarty->assign("userid",$users[0]);
		$smarty->assign("user_name",$users[1]);
		
		// Set the page title
		$smarty->assign("page_title","Search the library");
		
		// Display the category list
		$smarty->pixDisplay('admin.search/search.form.tpl');	
	
	}
		
	/*
	*
	*	
	*
	*/
	function doSearch () {
	
		global $cfg, $ses, $objEnvData, $smarty;
				
		if ($this->search_results_count > 0) { // If there are results, show them
			
			// Trim whitespace off the end of the search keywords
			$keywords_simple_boolean = rtrim($this->keywords_simple_boolean);
			
			// Create an array of punctuation to remove from the keyword fields
			$punctuation = array(",","'","\"","\\","/",";","\$","%","&","(",")","{","}","[","]","+","*","<",">");
		
			// Pass original variables back to Smarty for refine search
			$this->output_keywords_simple_boolean = $keywords_simple_boolean;
			
			$this->output_gallery_id 		= $this->objSearch->gallery_id;
			$this->output_srch_date_st 		= $this->objSearch->srch_date_st;
			$this->output_srch_date_en 		= $this->objSearch->srch_date_en;
			$this->output_date_srch 		= $this->objSearch->date_srch;
			
			$this->output_date_is_time		= $this->objSearch->srch_date_is_yy."-".$this->objSearch->srch_date_is_mm."-".$this->objSearch->srch_date_is_dd;
			$this->output_date_st_time 		= $this->objSearch->srch_date_st_yy."-".$this->objSearch->srch_date_st_mm."-".$this->objSearch->srch_date_st_dd;
			$this->output_date_en_time 		= $this->objSearch->srch_date_en_yy."-".$this->objSearch->srch_date_en_mm."-".$this->objSearch->srch_date_en_dd;
			
			$this->output_model_release 	= $this->objSearch->model_release;
			$this->output_property_release 	= $this->objSearch->property_release;
			$this->output_rights_managed 	= $this->objSearch->rights_managed;
			$this->output_results_order 	= $this->objSearch->results_order;
			$this->output_image_orientation = $this->objSearch->image_orientation;

			$this->output_image_colr_enable = $this->objSearch->image_colr_enable;
			$this->output_image_colr_r 		= $this->objSearch->image_colr_r;
			$this->output_image_colr_g 		= $this->objSearch->image_colr_g;
			$this->output_image_colr_b 		= $this->objSearch->image_colr_b;
			$this->output_image_colr_mod 	= $this->objSearch->getImageModRGB();
			$this->output_image_colr_dec 	= $this->objSearch->getImageDecRGB();
			$this->output_image_colr_hex 	= $this->objSearch->getImageHexRGB();
			$this->search_directory		 	= $this->objSearch->getSearchDirectory();
			
			// Get the search ID and pass it into Smarty
			$smarty->assign("sid",$this->objSearch->getSearchId());
			
			// Load an array of nested galleres
			$this->output_gallery_list 		= galleryListingArray();
			
			// View the search results list
			$this->viewSearchResults();
			
		} else { // Else there are no results so we need to show the search form again
			
			if ($objEnvData->fetchGlobal('mode') == "popup") {
			
				// Show the search form
				$this->formPopupSearch();
			
			} else {
			
				// Show the search form
				$this->showSearchForm();
			
			}
			
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function viewSearchResults () {
		
		global $cfg, $ses, $smarty, $objEnvData;
		
		$smarty->assign("results_total",$this->output_total);
		$smarty->assign("keywords_simple_boolean",$this->keywords_simple_boolean);
		$smarty->assign("srch_date_st",$this->output_srch_date_st);
		$smarty->assign("srch_date_en",$this->output_srch_date_en);
		$smarty->assign("date_srch",$this->output_date_srch);
		$smarty->assign("date_is_time",$this->output_date_is_time);
		$smarty->assign("date_st_time",$this->output_date_st_time);
		$smarty->assign("date_en_time",$this->output_date_en_time);
		$smarty->assign("model_release",$this->output_model_release);
		$smarty->assign("property_release",$this->output_property_release);
		$smarty->assign("rights_managed",$this->output_rights_managed);
		$smarty->assign("results_order",$this->output_results_order);
		$smarty->assign("menu_gallery_title",$this->output_gallery_list[0]);
		$smarty->assign("gallery_id",$this->output_gallery_id);
		$smarty->assign("image_orientation",$this->output_image_orientation);
		$smarty->assign("image_colr_mod",$this->output_image_colr_mod);
		$smarty->assign("image_colr_hex",$this->output_image_colr_hex);
		$smarty->assign("image_colr_dec",$this->output_image_colr_dec);
		$smarty->assign("image_colr_mod",$this->output_image_colr_mod);
		$smarty->assign("image_colr_enable",$this->output_image_colr_enable);
		$smarty->assign("image_colr_r",$this->output_image_colr_r);
		$smarty->assign("image_colr_g",$this->output_image_colr_g);
		$smarty->assign("image_colr_b",$this->output_image_colr_b);
		$smarty->assign("search_directory",$this->search_directory);
		$smarty->assign("name",$objEnvData->fetchGlobal('name'));
		
		// Get image paths from database
		$smarty->assign("image_path",$this->getImageDirectories());
		
		/*
		*	Load an array of nested galleries
		*/
		$gallery_list = galleryListingArray();
		
		// Load the category list data into Smarty
		$smarty->assign("menu_gallery_title",$gallery_list[0]);
		$smarty->assign("menu_gallery_id",$gallery_list[1]);
	
		// Load variables into Smarty
		$smarty->assign("thumbnails",$this->search_image_list);
		$smarty->assign("ipage_current",$this->ipages[0]);
		$smarty->assign("ipage_numbers",$this->ipages[1]);
		$smarty->assign("ipage_links",$this->ipages[2]);
		$smarty->assign("view_mode","search");
		
		if ($objEnvData->fetchGlobal('mode') == "popup") {
		
			// Set the page title
			$smarty->assign("page_title","Search results");
			
			// Display the lightbox page
			$smarty->pixDisplay('admin.search/image.find.02.tpl');
		
		} else {
		
			// Set the page title
			$smarty->assign("page_title","Search results");
			
			// Display the lightbox page
			$smarty->pixDisplay('admin.search/search.view.tpl');
		
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function formPopupSearch () {
	
		global $cfg, $ses, $smarty, $objEnvData;
		
		// Load form field name into Smarty
		$smarty->assign("name",aglobal(name));
		
		$sql = "SELECT image_id, image_path FROM ".$cfg['sys']['table_imgs']." GROUP BY image_path";
		
		$result = sql_select_rows($sql);
		
		if (is_array($result)) {
			
			foreach ($result as $key => $value) {
			
				$image_path[]	= $value['image_path'];
				$image_id[]		= $value['image_id'];
				
			}
			
		}
		
		$smarty->assign("image_path",$image_path);
		$smarty->assign("image_id",$image_id);
		
		// Load an array of nested categories
		$gallery_list = galleryListingArray();
		
		// Load the category list data into Smarty
		$smarty->assign("menu_gallery_title",$gallery_list[0]);
		$smarty->assign("menu_gallery_id",$gallery_list[1]);

		// Get array of date menu information
		$date_menus = pix_date_menu();
		
		// Load date information into Smarty for the menus
		$smarty->assign("years",$date_menus[0]);
		$smarty->assign("months",$date_menus[1]);
		$smarty->assign("days",$date_menus[2]);
		$smarty->assign("this_year",$date_menus[3]);
		$smarty->assign("this_month",$date_menus[4]);
		$smarty->assign("this_day",$date_menus[5]);
		
		// Get user list for search by userid
		$users = $this->getUsers();
		$smarty->assign("userid",$users[0]);
		$smarty->assign("user_name",$users[1]);
		
		// Display the html header
		$smarty->pixDisplay('admin.search/image.find.01.tpl');
	
	}
	
	/*
	*
	*	
	*
	*/
	function actionPopupSearch () {
		
		global $cfg, $ses, $smarty, $objEnvData;
		
		$smarty->assign("name",$objEnvData->fetchGlobal('name'));
		
		if ($this->search_results_count < 1) {
			$smarty->assign("results_title","Your search did not match any results");
			$smarty->assign("results_fail",TRUE);
		} elseif ($this->search_results_count == 1) {
			$smarty->assign("results_title","Your search found one image");
		} else {
			$smarty->assign("results_title","Your search found ".$this->search_results_count." images");
		}		
		
		// Load variables into Smarty
		$smarty->assign("thumbnails",$this->search_image_list);
		$smarty->assign("ipage_current",$this->ipages[0]);
		$smarty->assign("ipage_numbers",$this->ipages[1]);
		$smarty->assign("ipage_links",$this->ipages[2]);
		
		// Get the search ID and pass it into Smarty
		$smarty->assign("sid",$this->objSearch->getSearchId());
			
		// Set the page title
		$smarty->assign("page_title","Search results");
		
		// Display the lightbox page
		$smarty->pixDisplay('admin.search/image.find.02.tpl');
	
	}
	
	/*
	*
	*	
	*
	*/
	function getUsers () {
	
		global $cfg, $ses, $smarty, $objEnvData;
		
		$sql = "SELECT ".PIX_TABLE_USER.".userid,
											
				CONCAT(".PIX_TABLE_USER.".family_name,', ',".PIX_TABLE_USER.".first_name) AS user_name
											
				FROM ".PIX_TABLE_USER." 
											
				LEFT JOIN ".PIX_TABLE_GRME." ON ".PIX_TABLE_USER.".userid = ".PIX_TABLE_GRME.".userid
				
				LEFT JOIN ".PIX_TABLE_SGME." ON ".PIX_TABLE_USER.".userid = ".PIX_TABLE_SGME.".sys_userid
				
				WHERE ".PIX_TABLE_SGME.".sys_group_id = '3'
						
				GROUP BY ".PIX_TABLE_USER.".userid
				
				ORDER BY ".PIX_TABLE_USER.".family_name ASC";
		
		$result = $this->_dbl->sqlSelectRows($sql);
		
		if (is_array($result)) {
		
			foreach ($result as $key => $value) {
			
				$userid[] 		= $value['userid'];
				$user_name[] 	= $value['user_name'];
			
			}
			
			return (array($userid,$user_name));
			
		} else {
		
			return (bool)false;
		
		}
		
	}
	
}

?>