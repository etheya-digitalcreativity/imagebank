<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "images";

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Tell Smarty this is a Pixaria 3.0 admin page
$smarty->assign('pixaria_version','3');

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Check that the user is a SuperUser
pix_authorise_user("administrator");

$gallery = new gallery();

class gallery {
	
	var $db;
	var $input;
	var $view;
	var $GalleryCore;
	
	/*
	*
	*	
	*
	*/
	function gallery () {
	
		global $objEnvData, $smarty, $ses, $cfg;

		// Load the database class
		require_once ('class.Database.php');
		require_once (SYS_BASE_PATH . 'resources/classes/class.InputData.php');
		
		// Create the database object
		$this->db		= new Database();
		$this->input	= new InputData();
		$this->view		=& $smarty;
		
		// Assign an array of form actions to the view object
		$this->view->assign('form_actions', array (
		
			'gallery_archive' 		=> "{url:'".SYS_BASE_URL.FILE_ADM_GALLERY."',cmd:'actionGalleryArchive'}",
			'gallery_unarchive' 	=> "{url:'".SYS_BASE_URL.FILE_ADM_GALLERY."',cmd:'actionGalleryUnarchive'}",
			'gallery_show' 			=> "{url:'".SYS_BASE_URL.FILE_ADM_GALLERY."',cmd:'actionGalleryActive'}",
			'gallery_hide' 			=> "{url:'".SYS_BASE_URL.FILE_ADM_GALLERY."',cmd:'actionGalleryInactive'}",
			'gallery_images_create' => "{url:'".SYS_BASE_URL.FILE_ADM_GALLERY."',cmd:'formCreateNewGallery'}",
			'gallery_images_add' 	=> "{url:'".SYS_BASE_URL.FILE_ADM_GALLERY."',cmd:'formAddImagesToGallery'}",
			'gallery_images_move' 	=> "{url:'".SYS_BASE_URL.FILE_ADM_GALLERY."',cmd:'formMoveImagesToGallery'}",
			'gallery_remove_images' => "{url:'".SYS_BASE_URL.FILE_ADM_GALLERY."',cmd:'formRemoveImagesFromGallery'}",
			'images_delete' 		=> "{url:'".SYS_BASE_URL.FILE_ADM_IMAGE."',cmd:'formDeleteFromLibrary'}",
			'images_batch_edit' 	=> "{url:'".SYS_BASE_URL.FILE_ADM_IMAGE."',cmd:'editMultiple'}"
		
		));
		
		// Load Gallery Core class
		require_once (SYS_BASE_PATH . 'resources/classes/class.Core.Gallery.php');
		
		// Instantiate the Gallery Core object
		$this->GalleryCore = new GalleryCore();
		
		switch ($this->input->name('cmd')) {
						
			case "actionGalleryActive":
			case "actionGalleryInactive":
			case "actionGalleryActiveToggle":
			case "actionGalleryArchive":
			case "actionGalleryUnarchive":
			case "actionGalleryArchiveToggle":
				$this->changeGalleryStatus();
			break;
			
			/*
			*	CREATING AND EDITING GALLERIES
			*/
			case 'formCreateNewGallery':
				$this->formCreateNewGallery();
			break;
			
			case 'formCheckNewGallery':
				$this->formCheckNewGallery();
			break;
			
			case 'saveNewGallery':
				$this->saveNewGallery();
			break;
			
			case 'formEditGallery':
				$this->formEditGallery();
			break;
		
			case 'saveEditedGallery':
				$this->saveEditedGallery();
			break;
		
			/*
			*	MOVING IMAGES INTO GALLERIES
			*/
			case 'formAddImagesToGallery':
				$this->formAddImagesToGallery();
			break;
			
			case 'addImagesToGallery':
				$this->addImagesToGallery();
			break;
			
			/*
			*	MOVING IMAGES BETWEEN GALLERIES
			*/
			case 'formMoveImagesToGallery':
				$this->formMoveImagesToGallery();
			break;
			
			case 'actionMoveImagesToGallery':
				$this->actionMoveImagesToGallery();
			break;
			
			/*
			*	REMOVE IMAGES FROM GALLERIES
			*/
			case 'formRemoveImagesFromGallery':
				$this->formRemoveImagesFromGallery();
			break;
			
			case 'removeImagesFromGallery':
				$this->removeImagesFromGallery();
			break;
			
			/*
			*	REMOVE IMAGES FROM GALLERIES
			*/
			case 'changeGalleryImageOrder':
				$this->changeGalleryImageOrder();
			break;
			
			case 'changeGalleryImageOrderManual':
				$this->changeGalleryImageOrderManual();
			break;
			
			case 'saveChangedImageOrder':
				$this->saveChangedImageOrder();
			break;
			
			case 'saveAutoImageOrder':
				$this->saveAutoImageOrder();
			break;
			
			/*
			*	REMOVE GALLERY
			*/
			case 'confirmDeleteGallery':
				$this->confirmDeleteGallery();
			break;
			
			case 'actionDeleteGallery':
				$this->actionDeleteGallery();
			break;
			
			/*
			*	LISTING GALLERIES AND CONTENTS
			*/
			case 'galleryList':
				$this->galleryList();
			break;
		
			case 'listImages':
				$this->listImages();
			break;
			
			default:
				$this->index();
			break;
		
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function index () {
		
		global $cfg;
		
		// Load Gallery Core class
		require_once (SYS_BASE_PATH . 'resources/classes/class.Core.Gallery.php');
		
		$filter_sql = $this->galleryFilterSQL();
		
		// Load an array of nested categories
		list ($gallery_titles, $gallery_ids) = $this->GalleryCore->galleryListingArray($filter_sql);
		
		// Load the category list data into Smarty
		$this->view->assign("gallery_title",$gallery_titles);
		$this->view->assign("gallery_id",$gallery_ids);
		
		if (is_array($gallery_ids)) {
		
			foreach ($gallery_ids as $key => $value) {
			
				$sql = "SELECT 		 ".PIX_TABLE_GALL.".*
									,".PIX_TABLE_IMGS.".image_path
									,".PIX_TABLE_IMGS.".image_filename
									,COUNT(".PIX_TABLE_GORD.".image_id) AS gallery_images
									
						FROM 		".PIX_TABLE_GALL."
				
						LEFT JOIN 	".PIX_TABLE_GORD." ON ".PIX_TABLE_GALL.".gallery_id = ".PIX_TABLE_GORD.".gallery_id
						
						LEFT JOIN 	".PIX_TABLE_IMGS." ON ".PIX_TABLE_GALL.".gallery_key = ".PIX_TABLE_IMGS.".image_id
						
						WHERE 		".PIX_TABLE_GALL.".gallery_id = '$value'
						
						AND 		$filter_sql
						
						GROUP BY ".PIX_TABLE_GALL.".gallery_id";
				
				$data = $this->db->row($sql);
				
				$gallery_counter[]		= $data['gallery_counter'];
				$gallery_short_title[]	= $data['gallery_short_title'];
				$gallery_active[]		= $data['gallery_active'];
				$gallery_archived[]		= $data['gallery_archived'];
				$gallery_date[]			= $data['gallery_date'];
				$gallery_images[]		= $data['gallery_images'];
				
				if (file_exists($cfg['sys']['base_library'] . $data['image_path'] . "/32x32/" . $data['image_filename'])) {
					$icon_path 			= $cfg['sys']['base_library'] . $data['image_path'] . "/32x32/" . $data['image_filename'];
				} else {
					$icon_path 			= SYS_BASE_PATH . "resources/themes/" . $cfg['set']['theme'] . "/images/32x32_missing.jpg";
				}
				
				$gallery_icon_path[]	= base64_encode($icon_path);
				$gallery_icon_dims[]	= @getImageSize($icon_path);
				
				if (file_exists($cfg['sys']['base_library'] . $data['image_path'] . "/80x80/" . $data['image_filename'])) {
					$small_path 		= $cfg['sys']['base_library'] . $data['image_path'] . "/80x80/" . $data['image_filename'];
				} else {
					$small_path 		= SYS_BASE_PATH . "resources/themes/" . $cfg['set']['theme'] . "/images/80x80_missing.jpg";
				}
				
				$gallery_small_path[]	= base64_encode($small_path);
				$gallery_small_dims[]	= @getImageSize($small_path);
				
				if ($cfg['set']['func_mod_rewrite'] == 0) { // We need to generate standard URLs
				
					$gallery_url[]		= $cfg['sys']['base_url'] . $cfg['fil']['index_gallery'] . "?gid=" . $data['gallery_id'];
				
				} else { // We need to generate Mod_Rewrite comaptible URLs
				
					$gallery_url[]		= $cfg['sys']['base_url'] . "gallery" . "/" . $data['gallery_id'];
				
				}
				
				if ($value != "") { $show_galleries = (bool)TRUE; } else { $show_galleries = (bool)FALSE; }
				
			}
		
		}
		
		$this->view->assign("show_galleries",$show_galleries);
		$this->view->assign("gallery_counter",$gallery_counter);
		$this->view->assign("gallery_short_title",$gallery_short_title);
		$this->view->assign("gallery_images",$gallery_images);
		$this->view->assign("gallery_url",$gallery_url);
		$this->view->assign("gallery_date",$gallery_date);
		$this->view->assign("gallery_active",$gallery_active);
		$this->view->assign("gallery_archived",$gallery_archived);
		$this->view->assign("gallery_small_path",$gallery_small_path);
		$this->view->assign("gallery_small_dims",$gallery_small_dims);
		$this->view->assign("gallery_icon_path",$gallery_icon_path);
		$this->view->assign("gallery_icon_dims",$gallery_icon_dims);
		$this->view->assign("function","showGalleryListHierarchical");
		
		// Set the page title
		$this->view->assign("page_title","List all galleries");
		
		// Output the gallery list
		$this->view->display('gallery/gallery.list.html');
		
		exit;
		
	}
	
	/*
	*
	*	
	*
	*/
	function galleryList () {
	
		global $cfg;
		
		$filter_sql = $this->galleryFilterSQL();
		
		// SQL to load the list of galleries
		$sql = "SELECT 		 ".PIX_TABLE_GALL.".*
							,".PIX_TABLE_IMGS.".image_path
							,".PIX_TABLE_IMGS.".image_filename
							,COUNT(".PIX_TABLE_GORD.".image_id) AS gallery_images
							
				FROM 		".PIX_TABLE_GALL."
		
				LEFT JOIN 	".PIX_TABLE_GORD." ON ".PIX_TABLE_GALL.".gallery_id = ".PIX_TABLE_GORD.".gallery_id
				
				LEFT JOIN 	".PIX_TABLE_IMGS." ON ".PIX_TABLE_GALL.".gallery_key = ".PIX_TABLE_IMGS.".image_id
				
				WHERE 		$filter_sql
				
				GROUP BY 	".PIX_TABLE_GALL.".gallery_id";
		
		$result = $this->db->rows($sql);
		
		if (is_array($result)) {
		
			foreach ($result as $key => $value) {
				
				$gallery_counter[]		= $value['gallery_counter'];
				$gallery_title[]		= $value['gallery_title'];
				$gallery_active[]		= $value['gallery_active'];
				$gallery_archived[]		= $value['gallery_archived'];
				$gallery_short_title[]	= $value['gallery_short_title'];
				$gallery_description[]	= $value['gallery_description'];
				$gallery_id[]			= $value['gallery_id'];
				$gallery_date[]			= $value['gallery_date'];
				$gallery_images[]		= $value['gallery_images'];

				if (file_exists($cfg['sys']['base_library'] . $value['image_path'] . "/32x32/" . $value['image_filename'])) {
					$icon_path 			= $cfg['sys']['base_library'] . $value['image_path'] . "/32x32/" . $value['image_filename'];
				} else {
					$icon_path 			= SYS_BASE_PATH . "resources/themes/" . $cfg['set']['theme'] . "/images/32x32_missing.jpg";
				}
				
				$gallery_icon_path[]	= base64_encode($icon_path);
				$gallery_icon_dims[]	= @getImageSize($icon_path);
				
				if (file_exists($cfg['sys']['base_library'] . $value['image_path'] . "/80x80/" . $value['image_filename'])) {
					$small_path 		= $cfg['sys']['base_library'] . $value['image_path'] . "/80x80/" . $value['image_filename'];
				} else {
					$small_path 		= SYS_BASE_PATH . "resources/themes/" . $cfg['set']['theme'] . "/images/80x80_missing.jpg";
				}
				
				$gallery_small_path[]	= base64_encode($small_path);
				$gallery_small_dims[]	= @getImageSize($small_path);

				if ($cfg['set']['func_mod_rewrite'] == 0) { // We need to generate standard URLs
				
					$gallery_url[]		= $cfg['sys']['base_url'] . $cfg['fil']['index_gallery'] . "?gid=" . $value['gallery_id'];
				
				} else { // We need to generate Mod_Rewrite comaptible URLs
				
					$gallery_url[]		= $cfg['sys']['base_url'] . "gallery" . "/" . $value['gallery_id'];
				
				}
				
				if ($value != "") { $show_galleries = (bool)TRUE; } else { $show_galleries = (bool)FALSE; }
				
			}
		
		}
		
		$this->view->assign("show_galleries",$show_galleries);

		$this->view->assign("gallery_counter",$gallery_counter);
		$this->view->assign("gallery_title",$gallery_title);
		$this->view->assign("gallery_active",$gallery_active);
		$this->view->assign("gallery_archived",$gallery_archived);
		$this->view->assign("gallery_short_title",$gallery_short_title);
		$this->view->assign("gallery_description",$gallery_description);
		$this->view->assign("gallery_images",$gallery_images);
		$this->view->assign("gallery_url",$gallery_url);
		$this->view->assign("gallery_id",$gallery_id);
		$this->view->assign("gallery_date",$gallery_date);
		$this->view->assign("gallery_small_path",$gallery_small_path);
		$this->view->assign("gallery_small_dims",$gallery_small_dims);
		$this->view->assign("gallery_icon_path",$gallery_icon_path);
		$this->view->assign("gallery_icon_dims",$gallery_icon_dims);
		
		// Define the method to come back to after performing an action
		$this->view->assign("function","galleryList");
		
		// Set the page title
		$this->view->assign("page_title","Gallery Listing");
		
		// Output the gallery list
		$this->view->display('gallery/gallery.list.html');
		
	}
	
	/********************************************************************************
	*
	*	METHODS CONCERNING LISTING IMAGES INSIDE A GALLERY
	*
	********************************************************************************/
	
	/*
	*
	*	
	*
	*/
	function listImages () {
	
		global $objEnvData, $smarty, $cfg;
		
		$gallery_id = $this->input->name('gallery_id');
		
		$sql = "SELECT 		".PIX_TABLE_IMGS.".*
		
				FROM 		`".PIX_TABLE_IMGS."`
				
				LEFT JOIN 	`".PIX_TABLE_GORD."` ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_GORD.".image_id
				
				WHERE 		".PIX_TABLE_GORD.".gallery_id = '$gallery_id'
				
				GROUP BY	".PIX_TABLE_IMGS.".image_id
				
				ORDER BY	".PIX_TABLE_GORD.".order_id";
		
		// Load an array of image data for this gallery
		$images = $this->db->rows($sql);
		
		$image_id		= array();
		$image_name		= array();
		$image_filename	= array();
		$asset_iconpath	= array();
		
		if (count($images) > 0 && is_array($images)) {
		
			foreach ($images as $row) {
			
				$image_id[]			= $row['image_id'];
				$image_active[]		= $row['image_active'];
				$image_filename[] 	= $row['image_filename'];
				$image_path[]		= $row['image_path'];
				$image_date[]		= $row['image_date'];
				
				$icon_path[]		= base64_encode($cfg['sys']['base_library'].$row[image_path]."/32x32/".$row[image_filename]);
				$icon_size			= @getimagesize($cfg['sys']['base_library'].$row[image_path]."/32x32/".$row[image_filename]);
				$icon_width[]		= $icon_size[0];
				$icon_height[]		= $icon_size[1];
				
				$comp_path[]		= base64_encode($cfg['sys']['base_library'].$row[image_path]."/".COMPING_DIR."/".$row[image_filename]);
				$comp_size			= @getimagesize($cfg['sys']['base_library'].$row[image_path]."/".COMPING_DIR."/".$row[image_filename]);
				$comp_width[]		= $comp_size[0];
				$comp_height[]		= $comp_size[1];
				
				$image_width[]		= $row[image_width];
				$image_height[]		= $row[image_height];
				
				$image_title[]		= stripslashes($row[image_title]);

			}
			
			// Assign image information to Smarty
			$this->view->assign("image_id",$image_id);
			$this->view->assign("image_active",$image_active);
			$this->view->assign("image_filename",$image_filename);
			$this->view->assign("image_path",$image_path);
			$this->view->assign("image_date",$image_date);
			$this->view->assign("icon_path",$icon_path);
			$this->view->assign("icon_width",$icon_width);
			$this->view->assign("icon_height",$icon_height);
			$this->view->assign("comp_path",$comp_path);
			$this->view->assign("comp_width",$comp_width);
			$this->view->assign("comp_height",$comp_height);
			$this->view->assign("image_width",$image_width);
			$this->view->assign("image_height",$image_height);
			$this->view->assign("image_title",$image_title);
		
		}
		
		// Count number of images in the library
		$this->view->assign("image_id_count",count($image_id));
		
		// Set the page title
		$this->view->assign("page_title","Edit or remove images in this gallery");
		
		// Assign the image path variable to allow the template to show
		// the correct parts of the results confirmation page
		$this->view->assign("gallery_id",$gallery_id);
		
		// Display the main HTML page
		$this->view->display('gallery/images.list.html');
		
	}
	
	/********************************************************************************
	*
	*	METHODS CONCERNING EDITING OF GALLERIES
	*
	********************************************************************************/
	
	/*
	*
	*	
	*
	*/
	function formEditGallery () {
		
		// Get the gallery ID
		$gallery_id = $this->input->name('gallery_id');
		
		// Load Gallery Core class
		require_once (SYS_BASE_PATH . 'resources/classes/class.Core.Gallery.php');
		
		// Create a gallery data access object instance
		$gallery = $this->GalleryCore->createGalleryObject($gallery_id);
		
		$this->view->assign("gallery_id",			$gallery->getGalleryId());
		$this->view->assign("gallery_title",		$gallery->getGalleryTitle());
		$this->view->assign("gallery_active",		$gallery->getGalleryActive());
		$this->view->assign("gallery_short_title",	$gallery->getGalleryShortTitle());
		$this->view->assign("gallery_description",	$gallery->getGalleryDescription());
		$this->view->assign("gallery_parent",		$gallery->getGalleryParentId());
		$this->view->assign("gallery_parent_title",	$gallery->getGalleryParentTitle());
		$this->view->assign("gallery_date",			$gallery->getGalleryDate());
		$this->view->assign("gallery_key",			$gallery->getGalleryKeyImageId());
		$this->view->assign("gallery_permissions",	$gallery->getGalleryPermissions());
		$this->view->assign("gallery_password",		$gallery->getGalleryPassword());
		$this->view->assign("gallery_rss",			$gallery->getGalleryRss());
		$this->view->assign("gallery_view_selected",$gallery->getGalleryViewers());
		
		// Load informtion about the gallery avatar image
		$gallery_avatar_data = $this->GalleryCore->getAvatarData($gallery->getGalleryKeyImageId());
		
		$this->view->assign("icon_size",			$gallery_avatar_data['icon']['size']);
		$this->view->assign("icon_path",			$gallery_avatar_data['icon']['path']);
		$this->view->assign("small_size",			$gallery_avatar_data['small']['size']);
		$this->view->assign("small_path",			$gallery_avatar_data['small']['path']);
		$this->view->assign("large_size",			$gallery_avatar_data['large']['size']);
		$this->view->assign("large_path",			$gallery_avatar_data['large']['path']);

		// Load an array of nested galleries (excluding this one)
		$gallery_list = $this->GalleryCore->galleryListingArray(PIX_TABLE_GALL.".gallery_id != '".$gallery->getGalleryId()."'");
		
		// Load the category list data into Smarty
		$this->view->assign("menu_gallery_title",	$gallery_list[0]);
		$this->view->assign("menu_gallery_id",		$gallery_list[1]);
		
		// Get a list of the groups and whether or not they are selected
		$group_list = $this->GalleryCore->groupListArray($gallery->getGalleryViewers());
		
		// Load the category list data into Smarty
		$this->view->assign("group_name",			$group_list[0]);
		$this->view->assign("group_id",				$group_list[1]);
		
		
		// Set the page title
		$this->view->assign("page_title","Edit gallery");
		
		// Display the admin status page
		$this->view->pixDisplay('gallery/form.edit.html');
	
	}
	
	/*
	*
	*	
	*
	*/
	function saveEditedGallery () {

		// Get form data
		$gallery_id				= $this->input->post('gallery_id');
		$image_path				= $this->input->post('image_path');
		$image_string			= $this->input->post('image_string');
		$gallery_title 			= $this->input->post('gallery_title');
		$gallery_active 		= $this->input->post('gallery_active');
		$gallery_short_title 	= $this->input->post('gallery_short_title');
		$gallery_date			= $this->input->post('gallery_date');
		$gallery_parent			= $this->input->post('gallery_parent');
		$gallery_description	= $this->input->html('gallery_description');
		$gallery_key			= $this->input->post('gallery_key');
		$gallery_perms_copy		= $this->input->post('gallery_perms_copy');
		$gallery_permissions	= $this->input->post('gallery_permissions');
		$gallery_viewers		= $this->input->post('gallery_viewers');
		$gallery_password		= $this->input->post('gallery_password');
		$gallery_rss			= $this->input->checkbox('gallery_rss');
		
		// Generate gallery date
		$gallery_date			= 	$this->input->post('date_Year') ."-". 
									str_pad($this->input->post('date_Month'),2,"00",STR_PAD_LEFT) ."-". 
									str_pad($this->input->post('date_Day'),2,"00",STR_PAD_LEFT) . " 00:00:00";
		
		// Get gallery permissions
		if ($gallery_perms_copy) {
		
			$gallery_parent_permissions = $this->GalleryCore->getGalleryParentPermissions($gallery_parent);
			
			$gallery_permissions 	= $gallery_parent_permissions['gallery_permissions'];
			$gallery_password 		= $gallery_parent_permissions['gallery_password'];
			$gallery_viewers 		= $gallery_parent_permissions['gallery_viewers'];
		
		}
		
		// Create the gallery data access object
		$gallery = $this->GalleryCore->createGalleryObject($gallery_id);
		
		$gallery->setGalleryTitle($gallery_title);
		$gallery->setGalleryActive($gallery_active);
		$gallery->setGalleryShortTitle($gallery_short_title);
		$gallery->setGalleryDate($gallery_date);
		$gallery->setGalleryParent($gallery_parent);
		$gallery->setGalleryDescription($gallery_description);
		$gallery->setGalleryKeyImageId($gallery_key);
		$gallery->setGalleryPermissions($gallery_permissions);
		$gallery->setGalleryViewers($gallery_viewers);
		$gallery->setGalleryPassword($gallery_password);
		$gallery->setGalleryRss($gallery_rss);
		
		// Check that the gallery information is correct
		$gallery->validateGalleryInformation("edit");
	
		// Create the new gallery
		$gallery->editGallery();

		header("Content-type: application/json");
		print "{'message':'The changes have been saved!'}";
	
	}
	
	/********************************************************************************
	*
	*	METHODS RELATING TO CREATING NEW GALLERIES
	*
	********************************************************************************/
	
	/*
	*
	*	CREATE A NEW GALLERY
	*
	*/
	function formCreateNewGallery () {
		
		/*
		*
		*	If some of the gallery settings are going to be preloaded
		*	then load them in and do any processing.
		*
		*	This is used when creating galleries from the import screen.
		*
		*/
		if ($this->input->post('preloaded')) {
		
			$this->view->assign("gallery_active",$this->input->checkbox('gallery_active'));
			$this->view->assign("gallery_rss",$this->input->checkbox('gallery_rss'));
			$this->view->assign("gallery_key",$this->input->post('gallery_key'));
			$this->view->assign("gallery_title",$this->input->post('gallery_title'));

			$this->view->assign("gallery_parent",$this->input->post('gallery_parent'));
			$this->view->assign("gallery_description",$this->input->html('gallery_description'));
			
			if (trim($this->input->post('gallery_title')) != '' && trim($this->input->post('gallery_short_title')) == '') {
				
				$this->view->assign("gallery_short_title",strtolower(preg_replace('/[^a-z-A-Z-0-9-_]+/i', '_', $this->input->post('gallery_title'))));
			
			} else {
			
				$this->view->assign("gallery_short_title",$this->input->post('gallery_short_title'));
			
			}
			
			// Get the gallery key
			$gallery_key = $this->input->post('gallery_key');
		
			// Get gallery avatar
			$gallery_avatar_data	= $this->getAvatarData($gallery_key);
		
			$this->view->assign("icon_size",$gallery_avatar_data['icon']['size']);
			$this->view->assign("icon_path",$gallery_avatar_data['icon']['path']);
			$this->view->assign("small_size",$gallery_avatar_data['small']['size']);
			$this->view->assign("small_path",$gallery_avatar_data['small']['path']);
			$this->view->assign("large_size",$gallery_avatar_data['large']['size']);
			$this->view->assign("large_path",$gallery_avatar_data['large']['path']);
			
		}
		
		$image_string			= $this->input->name('image_string');
		$image_path				= $this->input->name('image_path');
		
		// If we are getting a string of comma separated image_ids, break into an array
		if ($image_string != "" && is_string($image_string)) {
	
			// Load these images into an array
			$images = explode(",",$image_string);
	
		} elseif (is_array($this->input->name('images'))) {
		
			$images = $this->input->name('images');
		
		}
		
		// If the user is starting with a pre-made directory, load it into Smarty
		$this->view->assign("image_path",$image_path);
		
		// Load the images into Smarty
		$this->view->assign("images",$images);
	
		// Load an array of nested categories
		$gallery_list = $this->GalleryCore->galleryListingArray();
		
		// Load the category list data into Smarty
		$this->view->assign("menu_gallery_title",$gallery_list[0]);
		$this->view->assign("menu_gallery_id",$gallery_list[1]);
							
		$group_list = $this->GalleryCore->groupListArray();
		
		// Load the category list data into Smarty
		$this->view->assign("group_name",$group_list[0]);
		$this->view->assign("group_id",$group_list[1]);
	
		// Set the page title
		$this->view->assign("page_title","Create a new gallery");
		
		// Display the admin status page
		$this->view->display('gallery/form.create.html');
	
	}
	
	/*
	*
	*	PREVIEW A NEW GALLERY'S SETTINGS
	*
	*/
	function formCheckNewGallery () {
		
		global $objEnvData, $smarty, $cfg;
		
		// Get form data
		$image_path				= $this->input->post('image_path');
		$images					= $this->input->post('images');
		$gallery_active 		= $this->input->post('gallery_active');
		$gallery_rss 			= $this->input->checkbox('gallery_rss');
		$gallery_title 			= $this->input->post('gallery_title');
		$gallery_short_title 	= $this->input->post('gallery_short_title');
		$gallery_date			= $this->input->post('date_Year') ."-". 
									str_pad($this->input->post('date_Month'),2,"00",STR_PAD_LEFT) ."-". 
									str_pad($this->input->post('date_Day'),2,"00",STR_PAD_LEFT) . " 00:00:00";
		$parent_gallery_id		= $this->input->post('gallery_parent');
		$parent_gallery_title	= $this->getParentGalleryTitle($parent_gallery_id);
		$gallery_description	= $this->input->html('gallery_description');
		$gallery_key			= $this->input->post('gallery_key');
		$gallery_perms_copy		= $this->input->post('gallery_perms_copy');
		$gallery_permissions	= $this->input->post('gallery_permissions');
		$gallery_viewers		= $this->input->post('gallery_viewers');
		$gallery_password		= $this->input->post('gallery_password');
		
		// Get gallery avatar
		$gallery_avatar_data	= $this->getAvatarData($gallery_key);
		
		// Create the gallery data access object
		$objPixariaGallery = $this->GalleryCore->createGalleryObject();
		
		$objPixariaGallery->setGalleryActive($gallery_active);
		$objPixariaGallery->setGalleryRss($gallery_rss);
		$objPixariaGallery->setGalleryTitle($gallery_title);
		$objPixariaGallery->setGalleryShortTitle($gallery_short_title);
		$objPixariaGallery->setGalleryDate($gallery_date);
		$objPixariaGallery->setGalleryParent($parent_gallery_id);
		$objPixariaGallery->setGalleryDescription($gallery_description);
		$objPixariaGallery->setGalleryKeyImageId($gallery_key);
		$objPixariaGallery->setGalleryPermissions($gallery_permissions);
		$objPixariaGallery->setGalleryViewers($gallery_viewers);
		$objPixariaGallery->setGalleryPassword($gallery_password);
		
		// Check that the gallery information is correct
		$objPixariaGallery->validateGalleryInformation("create");
		
		$this->view->assign("images",$images);
		$this->view->assign("image_path",$image_path);
		$this->view->assign("image_string",$image_string);
		$this->view->assign("gallery_rss",$objPixariaGallery->getGalleryRss());
		$this->view->assign("gallery_active",$objPixariaGallery->getGalleryActive());
		$this->view->assign("gallery_title",$objPixariaGallery->getGalleryTitle());
		$this->view->assign("gallery_short_title",$objPixariaGallery->getGalleryShortTitle());
		$this->view->assign("gallery_description",$objPixariaGallery->getGalleryDescription());
		$this->view->assign("gallery_parent",$objPixariaGallery->getGalleryParentId());
		$this->view->assign("gallery_parent_title",$objPixariaGallery->getGalleryParentTitle());
		$this->view->assign("gallery_date",$objPixariaGallery->getGalleryDate());
		$this->view->assign("gallery_key",$objPixariaGallery->getGalleryKeyImageId());
		$this->view->assign("gallery_perms_copy",$gallery_perms_copy);
		$this->view->assign("gallery_permissions",$objPixariaGallery->getGalleryPermissions());
		$this->view->assign("gallery_password",$objPixariaGallery->getGalleryPassword());
		$this->view->assign("gallery_view_selected",$objPixariaGallery->getGalleryViewers());
	
		$this->view->assign("icon_size",$gallery_avatar_data['icon']['size']);
		$this->view->assign("icon_path",$gallery_avatar_data['icon']['path']);
		$this->view->assign("small_size",$gallery_avatar_data['small']['size']);
		$this->view->assign("small_path",$gallery_avatar_data['small']['path']);
		$this->view->assign("large_size",$gallery_avatar_data['large']['size']);
		$this->view->assign("large_path",$gallery_avatar_data['large']['path']);
		
		if (!$objPixariaGallery->getGalleryValid()) { // There were errors in the submitted information
		
			$this->view->assign("problem",(bool)true);
			$this->view->assign("gallery_errors",$objPixariaGallery->getGalleryErrors());
			
			// Load an array of nested categories
			$gallery_list = $this->GalleryCore->galleryListingArray();
			
			// Load the category list data into Smarty
			$this->view->assign("menu_gallery_title",$gallery_list[0]);
			$this->view->assign("menu_gallery_id",$gallery_list[1]);
			
			// Get a list of the groups and whether or not they are selected
			$group_list = $this->GalleryCore->groupListArray($objPixariaGallery->getGalleryViewers());
			
			// Load the category list data into Smarty
			$this->view->assign("group_name",$group_list[0]);
			$this->view->assign("group_id",$group_list[1]);
			$this->view->assign("group_selected",$group_list[2]);
			
			// Set the page title
			$this->view->assign("page_title","Create a new gallery");
			
			// Display the admin status page
			$this->view->display('gallery/form.create.html');
				
		} else { // The gallery information submitted was OK so show a confirmation page
			
			if ($gallery_perms_copy) {
			
				$gallery_parent_permissions = $this->getGalleryParentPermissions($objPixariaGallery->getGalleryParentId());
				
				$this->view->assign("gallery_permissions",$gallery_parent_permissions['gallery_permissions']);
				$this->view->assign("gallery_password",$gallery_parent_permissions['gallery_password']);
				$this->view->assign("gallery_viewers",$gallery_parent_permissions['gallery_viewers']);
				$this->view->assign("gallery_viewers_name",$gallery_parent_permissions['gallery_viewers_name']);
			
			} else {
			
				// Get a list of the groups and whether or not they are selected
				$group_list = groupListArray($objPixariaGallery->getGalleryViewers());
			
				$this->view->assign("gallery_viewers",$objPixariaGallery->getGalleryViewers());
				$this->view->assign("gallery_viewers_name",$objPixariaGallery->getGalleryViewerGroupNames());
			
			}
			
			// Set the page title
			$this->view->assign("page_title","Confirm new gallery settings");
			
			// Display the admin status page
			$this->view->display('gallery/form.preview.html');
		
		}
		
		exit;
		
	}
	
	/*
	*
	*	
	*
	*/
	function saveNewGallery () {
	
		global $objEnvData, $smarty, $cfg, $ses;
		
		// Get form data
		$image_path				= $this->input->post('image_path');
		$images					= $this->input->post('images');
		$gallery_title 			= $this->input->post('gallery_title');
		$gallery_active 		= $this->input->post('gallery_active');
		$gallery_short_title 	= $this->input->post('gallery_short_title');
		$gallery_date			= $this->input->post('gallery_date');
		$parent_gallery_id		= $this->input->post('gallery_parent');
		$parent_gallery_title	= $this->getParentGalleryTitle($parent_gallery_id);
		$gallery_description	= urldecode($this->input->html('gallery_description'));
		$gallery_key			= $this->input->post('gallery_key');
		$gallery_perms_copy		= $this->input->post('gallery_perms_copy');
		$gallery_permissions	= $this->input->post('gallery_permissions');
		$gallery_viewers		= $this->input->post('gallery_viewers');
		$gallery_password		= $this->input->post('gallery_password');
		$gallery_avatar_data	= $this->getAvatarData($gallery_key);
		
		require_once ("class.DAO.PixariaGallery.php");
		
		// Create the gallery data access object
		$objPixariaGallery = new DAO_PixariaGallery();
		
		$objPixariaGallery->setGalleryTitle($gallery_title);
		$objPixariaGallery->setGalleryActive($gallery_active);
		$objPixariaGallery->setGalleryShortTitle($gallery_short_title);
		$objPixariaGallery->setGalleryDate($gallery_date);
		$objPixariaGallery->setGalleryParent($parent_gallery_id);
		$objPixariaGallery->setGalleryDescription($gallery_description);
		$objPixariaGallery->setGalleryKeyImageId($gallery_key);
		$objPixariaGallery->setGalleryPermissions($gallery_permissions);
		$objPixariaGallery->setGalleryViewers($gallery_viewers);
		$objPixariaGallery->setGalleryPassword($gallery_password);
		
		// Check that the gallery information is correct
		$objPixariaGallery->validateGalleryInformation("create");
	
		if (!$objPixariaGallery->getGalleryValid()) { // There were errors in the submitted information
		
			$this->view->assign("image_path",$image_path);
			$this->view->assign("images",$images);
			$this->view->assign("gallery_title",$objPixariaGallery->getGalleryTitle());
			$this->view->assign("gallery_active",$objPixariaGallery->getGalleryActive());
			$this->view->assign("gallery_short_title",$objPixariaGallery->getGalleryShortTitle());
			$this->view->assign("gallery_description",$objPixariaGallery->getGalleryDescription());
			$this->view->assign("gallery_parent",$objPixariaGallery->getGalleryParentId());
			$this->view->assign("gallery_parent_title",$objPixariaGallery->getGalleryParentTitle());
			$this->view->assign("gallery_date",$objPixariaGallery->getGalleryDate());
			$this->view->assign("gallery_key",$objPixariaGallery->getGalleryKeyImageId());
			$this->view->assign("gallery_perms_copy",$gallery_perms_copy);
			$this->view->assign("gallery_permissions",$objPixariaGallery->getGalleryPermissions());
			$this->view->assign("gallery_password",$objPixariaGallery->getGalleryPassword());
		
			$this->view->assign("icon_size",$gallery_avatar_data['icon']['size']);
			$this->view->assign("icon_path",$gallery_avatar_data['icon']['path']);
			$this->view->assign("small_size",$gallery_avatar_data['small']['size']);
			$this->view->assign("small_path",$gallery_avatar_data['small']['path']);
			$this->view->assign("large_size",$gallery_avatar_data['large']['size']);
			$this->view->assign("large_path",$gallery_avatar_data['large']['path']);
			
			$this->view->assign("problem",(bool)true);
			$this->view->assign("gallery_errors",$objPixariaGallery->getGalleryErrors());
			
			// Load an array of nested categories
			$gallery_list = galleryListingArray();
			
			// Load the category list data into Smarty
			$this->view->assign("menu_gallery_title",$gallery_list[0]);
			$this->view->assign("menu_gallery_id",$gallery_list[1]);
			
			// Get a list of the groups and whether or not they are selected
			$group_list = groupListArray($objPixariaGallery->getGalleryViewers());
			
			// Load the category list data into Smarty
			$this->view->assign("group_name",$group_list[0]);
			$this->view->assign("group_id",$group_list[1]);
			$this->view->assign("group_selected",$group_list[2]);
			
			// Set the page title
			$this->view->assign("page_title","Create a new gallery");
			
			// Display the admin status page
			$this->view->display('gallery/form.create.html');
				
		} else {
			
			// Create the new gallery
			$objPixariaGallery->createGallery();
			
			// Add images to this gallery by path or string of image_ids (if present)
			if (count($images) < 1 && $image_path != "") {
			
				$objPixariaGallery->addImagesByPath($image_path);
			
			} else {
			
				$objPixariaGallery->addImagesById($images);
			
			}
			
			// Create a new blog entry for this gallery
			if ($this->input->name('blog_create') == "on") {
			
				// Laod information from form
				$blog_title		= $this->input->post('blog_title');
				$blog_content	= $this->input->post('blog_content');
				$blog_userid	= $ses['psg_userid'];
				
				$blog_sql = "INSERT INTO ".PIX_TABLE_BLOG." VALUES(
																	'0',
																	'".$this->db->escape($blog_title)."',
																	'".$this->db->escape($blog_content)."',
																	NOW(),
																	'".$this->db->escape($blog_userid)."',
																	'".$objPixariaGallery->getGalleryId()."'
																	);";
				$this->db->query($blog_sql);
			
			}
			
			// Assign the image path variable to allow the template to show
			// the correct parts of the results confirmation page
			$this->view->assign("gallery_id",$objPixariaGallery->getGalleryId());
			
			// Assign the title of the gallery we've imported images into
			$this->view->assign("gallery_title",$objPixariaGallery->getGalleryTitle());
			
			// Set the page title
			$this->view->assign("page_title","The gallery has been created");
			
			// Display the main HTML page
			$this->view->display('gallery/form.complete.html');
		
		}

	}
	
	/********************************************************************************
	*
	*	METHODS CONCERNING ADDING IMAGES TO GALLERIES
	*
	********************************************************************************/
		
	/*
	*
	*	
	*
	*/
	function formAddImagesToGallery () {
		
		// Show the global form to add images to a gallery
		formAddImagesToGallery();
			
	}
	
	/*
	*
	*	
	*
	*/
	function addImagesToGallery () {
		
		$gallery_list = $this->input->name('gallery_id');
		
		$image_list = $this->input->name('images');
		
		if (is_array($gallery_list)) {
			
			foreach ($gallery_list as $key => $gallery_id) {
				
				// Instantiate object
				$objPixariaGallery = $this->GalleryCore->createGalleryObject($gallery_id);
				
				// Add images to the gallery
				$objPixariaGallery->addImagesById($this->input->name('images'),$this->input->name('insert_location'));
				
				unset($objPixariaGallery);
				
			}
			
		} else {
			
			// Instantiate object
			$objPixariaGallery = $this->GalleryCore->createGalleryObject($gallery_list);
			
			// Add images to the gallery
			$objPixariaGallery->addImagesById($this->input->name('images'),$this->input->name('insert_location'));
			
			// Assign the title of the gallery we've imported images into
			$this->view->assign("gallery_title",$objPixariaGallery->getGalleryTitle());
			
		}
		
		// Set the page title
		$this->view->assign("page_title","Images added successfully");
		
		// Display the admin status page
		$this->view->display('gallery/form.images.add.complete.html');
			
	}
	
	/********************************************************************************
	*
	*	METHODS CONCERNING MOVING IMAGES BETWEEN GALLERIES
	*
	********************************************************************************/
		
	/*
	*
	*	FORM TO MOVE IMAGES FROM ONE GALLERY TO ANOTHER
	*
	*/
	function formMoveImagesToGallery () {
	
		$images 		= $this->input->name('images');
		$old_gallery 	= $this->input->name('gallery_id');
		
		// This case displays a page to import images into an existing gallery
		
		// Load data into Smarty
		$this->view->assign("old_gallery",$old_gallery);
		$this->view->assign("images",$images);
		
		// Load an array of nested galleries
		$gallery_list = $this->GalleryCore->galleryListingArray();
		
		// Load the gallery list data into Smarty
		$this->view->assign("menu_gallery_title",$gallery_list[0]);
		$this->view->assign("menu_gallery_id",$gallery_list[1]);
	
		// Set the page title
		$this->view->assign("page_title","Move images to gallery");
		
		// Display the admin status page
		$this->view->display('gallery/images.move.01.html');
		
	}
	
	/*
	*
	*	
	*
	*/
	function actionMoveImagesToGallery () {
	
		// Get form data
		$old_gallery 		= $this->input->name('old_gallery');
		$new_gallery 		= $this->input->name('new_gallery');
		$images				= $this->input->name('images');
		$insert_location	= $this->input->name('insert_location');
		
		// Instantiate object
		$objOldGallery = $this->GalleryCore->createGalleryObject($old_gallery);
		
		// Remove the images from the gallery
		$objOldGallery->removeImagesById($images);
		
		// Instantiate object
		$objNewGallery = $this->GalleryCore->createGalleryObject($new_gallery);
		
		// Add images to the gallery
		$objNewGallery->addImagesById($images,$insert_location);
		
		// Assign the title of the gallery we've imported images into
		$this->view->assign("new_gallery_title",$objNewGallery->getGalleryTitle());
		$this->view->assign("old_gallery_title",$objOldGallery->getGalleryTitle());
		
		// Set the page title
		$this->view->assign("page_title","Images moved");
		
		// Display the main HTML page
		$this->view->pixDisplay('gallery/images.move.02.html');
	
	}
	
	/********************************************************************************
	*
	*	METHODS CONCERNING REMOVING IMAGES FROM GALLERIES
	*
	********************************************************************************/
		
	/*
	*
	*	
	*
	*/
	function formRemoveImagesFromGallery () {
	
		global $objEnvData, $smarty, $cfg;
		
		$gallery_id = $this->input->name('gallery_id');
		
		// Load an array of the images chosen for deletion
		$images = $this->input->name('images');
		
		if (count($images) > 0 && is_array($images)) {
		
			foreach ($images as $key => $value) {
				
				$row = $this->db->row("SELECT * FROM ".PIX_TABLE_IMGS." WHERE image_id = '".$this->db->escape($value)."'");
			
				$image_id[]			= $row['image_id'];
				
				$image_filename[] 	= $row['image_filename'];
				$image_path[]		= $row['image_path'];
				$image_date[]		= $row['image_date'];
				
				$icon_path[]		= base64_encode($cfg['sys']['base_library'].$row[image_path]."/32x32/".$row[image_filename]);
				$icon_size			= getimagesize($cfg['sys']['base_library'].$row[image_path]."/32x32/".$row[image_filename]);
				$icon_width[]		= $icon_size[0];
				$icon_height[]		= $icon_size[1];
				
				$comp_path[]		= base64_encode($cfg['sys']['base_library'].$row[image_path]."/".COMPING_DIR."/".$row[image_filename]);
				$comp_size			= getimagesize($cfg['sys']['base_library'].$row[image_path]."/".COMPING_DIR."/".$row[image_filename]);
				$comp_width[]		= $comp_size[0];
				$comp_height[]		= $comp_size[1];
				
				$image_width[]		= $row[image_width];
				$image_height[]		= $row[image_height];
				
				$image_title[]		= $row[image_title];
			}
			
			// Assign image information to Smarty
			$this->view->assign("gallery_id",$gallery_id);
			$this->view->assign("image_id",$image_id);
			$this->view->assign("image_filename",$image_filename);
			$this->view->assign("image_path",$image_path);
			$this->view->assign("image_date",$image_date);
			$this->view->assign("icon_path",$icon_path);
			$this->view->assign("icon_width",$icon_width);
			$this->view->assign("icon_height",$icon_height);
			$this->view->assign("comp_path",$comp_path);
			$this->view->assign("comp_width",$comp_width);
			$this->view->assign("comp_height",$comp_height);
			$this->view->assign("image_width",$image_width);
			$this->view->assign("image_height",$image_height);
			$this->view->assign("image_title",$image_title);
		
		}
		
		// Set the page title
		$this->view->assign("page_title","Edit or remove images in this gallery");
		
		// Assign the image path variable to allow the template to show
		// the correct parts of the results confirmation page
		$this->view->assign("gallery_id",$gallery_id);
		
		// Display the main HTML page
		$this->view->display('gallery/form.images.remove.html');
	
	}
		
	/*
	*
	*	
	*
	*/
	function removeImagesFromGallery () {
	
		// Load gallery DAO library
		require_once ("class.DAO.PixariaGallery.php");
		
		// Instantiate object
		$objPixariaGallery = new DAO_PixariaGallery($this->input->name('gallery_id'));
		
		// Remove the images from the gallery
		$objPixariaGallery->removeImagesById($this->input->name('images'));
		
		// Set the page title
		$this->view->assign("page_title","Images removed");
		
		// Display the main HTML page
		$this->view->display('gallery/form.images.remove.complete.html');

	}
	
	/********************************************************************************
	*
	*	METHODS FOR CHANGING GALLERY IMAGE ORDERING
	*
	********************************************************************************/
		
	/*
	*
	*	
	*
	*/
	function changeGalleryImageOrder () {
	
		$gallery_id = $this->input->name('gallery_id');
		
		// Count number of images in the library
		$this->view->assign("image_id_count",count($image_id));
		
		// Set the page title
		$this->view->assign("page_title","Rearrange images in this gallery");
		
		// Assign the image path variable to allow the template to show
		// the correct parts of the results confirmation page
		$this->view->assign("image_path",$image_path);
		
		// Assign the image path variable to allow the template to show
		// the correct parts of the results confirmation page
		$this->view->assign("gallery_id",$gallery_id);
		
		// Assign the title of the gallery we've imported images into
		$this->view->assign("gallery_title",$result['gallery_title']);
		
		// Display the main HTML page
		$this->view->display('gallery/gallery.order.auto.html');

	}
	
	/*
	*
	*	
	*
	*/
	function changeGalleryImageOrderManual () {
	
		global $objEnvData, $smarty, $cfg;
		
		$gallery_id = $this->input->name('gallery_id');
		
		// Load an array of image data for this gallery
		$sql = "SELECT ".PIX_TABLE_IMGS.".* 
				
				FROM `".PIX_TABLE_IMGS."`
				
				LEFT JOIN `".PIX_TABLE_GORD."` ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_GORD.".image_id
				
				WHERE ".PIX_TABLE_GORD.".gallery_id = '".$this->db->escape($gallery_id)."'
				
				GROUP by ".PIX_TABLE_IMGS.".image_id
				
				ORDER by ".PIX_TABLE_GORD.".order_id";
		
		$images = $this->db->rows($sql);
		
		$image_id		= array();
		$image_name		= array();
		$image_filename	= array();
		$asset_iconpath	= array();

		if (is_array($images)) {
		
			foreach ($images as $key => $value) {
			
				$image_id[]			= $value['image_id'];
				$image_title[]		= $value['image_title'];
				$image_filename[]	= $value['image_filename'];
				$image_date[]		= $value['image_date'];
				$small_path[]		= base64_encode(SYS_BASE_LIBRARY.$value['image_path']."/80x80/".$value['image_filename']);
				$small_dims[]		= getimagesize(SYS_BASE_LIBRARY.$value['image_path']."/80x80/".$value['image_filename']);
				$comp_path[]		= base64_encode(SYS_BASE_LIBRARY.$value['image_path']."/".COMPING_DIR."/".$value['image_filename']);
				$comp_size			= getimagesize(SYS_BASE_LIBRARY.$value['image_path']."/".COMPING_DIR."/".$value['image_filename']);
				$comp_width[]		= $comp_size[0];
				$comp_height[]		= $comp_size[1];

			}
			
			$this->view->assign("image_id",$image_id);
			$this->view->assign("image_name",$image_name);
			$this->view->assign("image_filename",$image_filename);		
			$this->view->assign("image_title",$image_title);		
			$this->view->assign("image_date",$image_date);		
			$this->view->assign("small_path",$small_path);		
			$this->view->assign("small_dims",$small_dims);		
			$this->view->assign("comp_path",$comp_path);
			$this->view->assign("comp_width",$comp_width);
			$this->view->assign("comp_height",$comp_height);
		
		}
		
		// Count number of images in the library
		$this->view->assign("image_id_count",count($image_id));
		
		// Set the page title
		$this->view->assign("page_title","Rearrange images in this gallery");
		
		// Assign the image path variable to allow the template to show
		// the correct parts of the results confirmation page
		$this->view->assign("image_path",$image_path);
		
		// Assign the image path variable to allow the template to show
		// the correct parts of the results confirmation page
		$this->view->assign("gallery_id",$gallery_id);
		
		// Assign the title of the gallery we've imported images into
		$this->view->assign("gallery_title",$result['gallery_title']);
		
		// Display the main HTML page
		$this->view->display('gallery/gallery.order.manual.html');

	}

	/*
	*
	*	
	*
	*/
	function saveChangedImageOrder () {
		
		global $objEnvData, $smarty, $cfg;
		
		// Set the gallery id
		$gallery_id = $this->input->name('gallery_id');
				
		// Set the gallery order from the input data
		$gallery_order = explode("|",$this->input->name('gallery_order'));
		
		if (is_array($gallery_order) && count($gallery_order) > 0 && is_numeric($gallery_id)) {
			
			// Erase the existing data before we update it with new information
			$this->db->query("DELETE FROM ".PIX_TABLE_GORD." WHERE gallery_id = '".$this->db->escape($gallery_id)."'");
			
			// Write the new order of images into the gallery_order table
			foreach ($gallery_order as $key => $value) {
				
				// The script will pass an array of div id's some of which are not needed
				// We will only need ones that begin with the prefix 'image_'.
				if (eregi("image_",$value)) {
				
					$image_id = str_replace("image_","",$value);
				
					// Write each new item back to the database
					$this->db->query("INSERT INTO ".PIX_TABLE_GORD." VALUES ('$key','".$this->db->escape($gallery_id)."','".$this->db->escape($image_id)."');");
				
				}
				
			}
	
		}
		
		// Set the page title
		$this->view->assign("page_title","Image display order changed");
		
		// Display the admin status page
		$this->view->display('gallery/gallery.order.complete.html');

	}
	
	/*
	*
	*	
	*
	*/
	function saveAutoImageOrder () {
		
		global $objEnvData, $smarty, $cfg;
		
		// Set the gallery id
		$gallery_id = $this->input->name('gallery_id');
		
		switch ($this->input->name('sort')) {
		
			case 10: // Natural language A-Z by file name
				
				$sort_sql = "ORDER BY image_filename ASC";
			
			break;
			
			case 11: // Natural language Z-A by file name
			
				$sort_sql = "ORDER BY image_filename DESC";
			
			break;
		
			case 12: // Natural language A-Z by title
			
				$sort_sql = "ORDER BY image_title ASC";
			
			break;
		
			case 13: // Natural language Z-A by title
			
				$sort_sql = "ORDER BY image_title DESC";
			
			break;
		
			case 14: // Newest (by ID) first
			
				$sort_sql = "ORDER BY image_id ASC";
			
			break;
		
			case 15: // Newest (by ID) last
			
				$sort_sql = "ORDER BY image_id DESC";
			
			break;
		
			case 16: // Newest (by date) first
			
				$sort_sql = "ORDER BY image_date ASC";
			
			break;
		
			case 17: // Newest (by date) last
			
				$sort_sql = "ORDER BY image_date DESC";
			
			break;
		
		}
		
		$sql = "SELECT ".PIX_TABLE_IMGS.".image_filename, ".PIX_TABLE_IMGS.".image_id FROM ".PIX_TABLE_IMGS."
				
				LEFT JOIN ".PIX_TABLE_GORD." ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_GORD.".image_id
				
				WHERE ".PIX_TABLE_GORD.".gallery_id = '".$this->db->escape($gallery_id)."'

				$sort_sql";
		
		$result = $this->db->rows($sql);
		
		foreach ($result as $value) {
			
			$list[$value['image_id']] = $value['image_filename'];
		
		}
		
		if (is_array($list)) {
			
			// Erase the existing data before we update it with new information
			$this->db->query("DELETE FROM ".PIX_TABLE_GORD." WHERE gallery_id = '".$this->db->escape($gallery_id)."'");
			
			// Set the starting point of the gallery order to 1
			$i = 1;
			
			// Write the new order of images into the gallery_order table
			foreach ($list as $key => $value) {
				
				// Add this item to the database
				$this->db->query("INSERT INTO ".PIX_TABLE_GORD." VALUES ('$i','".$this->db->escape($gallery_id)."','$key');");
				
				// Increment the gallery order
				$i++;
				
			}
		
		}
		
		// Set the page title
		$this->view->assign("page_title","Image display order changed");
		
		// Display the admin status page
		$this->view->display('gallery/gallery.order.complete.html');

	}
	
	
	/********************************************************************************
	*
	*	METHODS FOR DELETING GALLERIES
	*
	********************************************************************************/
		
	/*
	*
	*	
	*
	*/
	function confirmDeleteGallery () {
	
		$gallery_id = $this->input->name('gallery_id');
		
		// Get the gallery information
		$gallery_info = $this->db->row("SELECT * FROM ".PIX_TABLE_GALL." WHERE gallery_id = '$gallery_id'");
		
		$sql = "SELECT ".PIX_TABLE_IMGS.".*
				
				FROM `".PIX_TABLE_IMGS."`
				
				LEFT JOIN `".PIX_TABLE_GORD."` ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_GORD.".image_id
				
				WHERE ".PIX_TABLE_GORD.".gallery_id = '".$this->db->escape($gallery_id)."'
				
				GROUP by ".PIX_TABLE_IMGS.".image_id
				
				ORDER by ".PIX_TABLE_GORD.".order_id";
		
		// Load an array of image data for this gallery
		$images = $this->db->rows($sql);
		
		$image_id		= array();
		$image_name		= array();
		$image_filename	= array();
		$asset_iconpath	= array();
		
		if (count($images) > 0 && is_array($images)) {
		
			foreach ($images as $row) {
			
				$image_id[]			= $row['image_id'];
				
				$image_filename[] 	= $row['image_filename'];
				$image_path[]		= $row['image_path'];
				$image_date[]		= $row['image_date'];
				
				$icon_path[]		= base64_encode(SYS_BASE_LIBRARY.$row[image_path]."/32x32/".$row[image_filename]);
				$icon_size			= getimagesize(SYS_BASE_LIBRARY.$row[image_path]."/32x32/".$row[image_filename]);
				$icon_width[]		= $icon_size[0];
				$icon_height[]		= $icon_size[1];
				
				$comp_path[]		= base64_encode(SYS_BASE_LIBRARY.$row[image_path]."/".COMPING_DIR."/".$row[image_filename]);
				$comp_size			= getimagesize(SYS_BASE_LIBRARY.$row[image_path]."/".COMPING_DIR."/".$row[image_filename]);
				$comp_width[]		= $comp_size[0];
				$comp_height[]		= $comp_size[1];
				
				$image_width[]		= $row[image_width];
				$image_height[]		= $row[image_height];
				
				$image_title[]		= $row[image_title];
			}
			
			// Assign image information to Smarty
			$this->view->assign("image_id",$image_id);
			$this->view->assign("image_filename",$image_filename);
			$this->view->assign("image_path",$image_path);
			$this->view->assign("image_date",$image_date);
			$this->view->assign("icon_path",$icon_path);
			$this->view->assign("icon_width",$icon_width);
			$this->view->assign("icon_height",$icon_height);
			$this->view->assign("comp_path",$comp_path);
			$this->view->assign("comp_width",$comp_width);
			$this->view->assign("comp_height",$comp_height);
			$this->view->assign("image_width",$image_width);
			$this->view->assign("image_height",$image_height);
			$this->view->assign("image_title",$image_title);
		
		}
		
		// Count number of images in the library
		$this->view->assign("image_id_count",count($image_id));
		
		// Load an array of nested categories
		$gallery_list = $this->GalleryCore->galleryListingArray();
		
		// Load the category list data into Smarty
		$this->view->assign("menu_gallery_title",$gallery_list[0]);
		$this->view->assign("menu_gallery_id",$gallery_list[1]);
		
		// Assign some key information to the Smarty object
		$this->view->assign("gallery_id",$this->input->name('gallery_id'));
		$this->view->assign("gallery_title",$gallery_info['gallery_title']);
		
		// Set the page title
		$this->view->assign("page_title","Delete the gallery '".$gallery_info['gallery_title']."'");
		
		// Display the main HTML page
		$this->view->display('gallery/gallery.delete.01.html');
	
	}
	
	/*
	*
	*	
	*
	*/
	function actionDeleteGallery () {
	
		global $objEnvData, $smarty, $cfg, $lang, $errors;
		
		// Create an array to hold the errors in
		$errors = array();
		
		// Get form data
		$gallery_id		= $this->input->name('gallery_id');
		$gallery_parent	= $this->input->name('gallery_parent');
		$delete_option	= $this->input->name('delete_option');
		
		// Instantiate object
		$objPixariaGallery = $this->GalleryCore->createGalleryObject($gallery_id);
		
		$gallery_title = $objPixariaGallery->getGalleryTitle();
		
		// Check if gallery is present to be deleted
		if ($objPixariaGallery->getGalleryId() == "") {
			
			$this->index();
			
			exit;
			
		}
		
		// Get an array of all the image_ids for images in this gallery
		$gallery_contents = $objPixariaGallery->getGalleryImageOrder();
		
		// Delete the gallery
		$objPixariaGallery->deleteGallery($gallery_parent);
		
		// This loop deletes the real files for images in this gallery if the user has chosen to do this
		if (count($gallery_contents) > 0 && is_array($gallery_contents) && $delete_option != "off") {
			
			foreach ($gallery_contents as $key => $value) {

				// Delete the image from the database
				deleteImageFromLibrary($value,$delete_option);			
							
			}
			
		}
		
		// Count number of images that cound not be deleted
		$this->view->assign("error_count",count($errors));
		
		// Set the page title
		$this->view->assign("page_title","Gallery deleted");
		
		// Assign some key information to the Smarty object
		$this->view->assign("gallery_title",$gallery_title);
		$this->view->assign("errors",$errors);
		
		// Display the main HTML page
		$this->view->display('gallery/gallery.delete.02.html');
			
	}
	
	/*
	*
	*	Method for changing gallery archive status
	*
	*/
	function changeGalleryStatus () {
		
		global $objEnvData;
		
		$galleries 	= $this->input->name('galleries');
		$function 	= $this->input->name('function');
		$cmd 		= $this->input->name('cmd');
		
		if (is_array($galleries)) {
			
			foreach ($galleries as $key => $value) {
			
				if ($cmd == "actionGalleryActive") {
					
					// Set the gallery as archived
					$this->db->query("UPDATE ".PIX_TABLE_GALL." SET gallery_active = '1' WHERE gallery_id = '$value'");
					
				} elseif ($cmd == "actionGalleryInactive") {
					
					// Set the gallery as unarchived
					$this->db->query("UPDATE ".PIX_TABLE_GALL." SET gallery_active = '0' WHERE gallery_id = '$value'");
					
				} elseif ($cmd == "actionGalleryActiveToggle") {
					
					// Use a bitwise operation to toggle the state of this gallery
					$this->db->query("UPDATE ".PIX_TABLE_GALL." SET gallery_active = 1 ^ gallery_active WHERE gallery_id = '$value'");
				
				} elseif ($cmd == "actionGalleryArchive") {
					
					// Set the gallery as archived
					$this->db->query("UPDATE ".PIX_TABLE_GALL." SET gallery_archived = '1' WHERE gallery_id = '$value' OR gallery_parent = '$value'");
					
				} elseif ($cmd == "actionGalleryUnarchive") {
					
					// Set the gallery as unarchived
					$this->db->query("UPDATE ".PIX_TABLE_GALL." SET gallery_archived = '0' WHERE gallery_id = '$value' OR gallery_parent = '$value'");
					
				} elseif ($cmd == "actionGalleryArchiveToggle") {
					
					// Use a bitwise operation to toggle the state of this gallery
					$this->db->query("UPDATE ".PIX_TABLE_GALL." SET gallery_archived = 1 ^ gallery_archived WHERE gallery_id = '$value'");
				
				}
			
			}
		
			// Tell Smarty this is a completed action
			$this->action_complete = 1;

		}
		
		if ($function == "galleryList") {
		
			$this->galleryList();
			
		} else {
		
			$this->index();
			
		}
		
	}

	/*
	*
	*	Generate filter SQL
	*
	*/
	function galleryFilterSQL () {
		
		global $objEnvData, $smarty, $cfg;
		
		$filter_on			= $this->input->name('filter_on');
		$filter_name		= $this->input->name('filter_name');
		$filter_active		= $this->input->name('filter_active');
		$filter_archived	= $this->input->name('filter_archived');
		
		// Assign the filter name back to Smarty
		$this->view->assign("filter_name",$filter_name);
		
		if ($filter_on) {

			// Update the filter settings
			$this->db->query("UPDATE ".PIX_TABLE_SETT." SET value = '$filter_active' WHERE name = 'filter_gallery_active';");
			$this->db->query("UPDATE ".PIX_TABLE_SETT." SET value = '$filter_archived' WHERE name = 'filter_gallery_archived';");
			
			$this->view->assign("set_filter_gallery_active",$filter_active);
			$this->view->assign("set_filter_gallery_archived",$filter_archived);

		}
		
		$sql_filter = array();
		
		$sql_filter[] = " ".PIX_TABLE_GALL.".gallery_id != '' ";
		
		if ($filter_active == "1" || ($cfg['set']['filter_gallery_active'] == "1" && !$filter_on)) {
		
			$sql_filter[] = " ".PIX_TABLE_GALL.".gallery_active = '1' ";
			
		} elseif ($filter_active == "2" || ($cfg['set']['filter_gallery_active'] == "2" && !$filter_on)) {
		
			$sql_filter[] = " ".PIX_TABLE_GALL.".gallery_active = '0' ";
			
		}
		
		if ($filter_archived == "1" || ($cfg['set']['filter_gallery_archived'] == "1" && !$filter_on)) {
		
			$sql_filter[] = " ".PIX_TABLE_GALL.".gallery_archived = '0' ";
			
		} elseif ($filter_archived == "2" || ($cfg['set']['filter_gallery_archived'] == "2" && !$filter_on)) {
		
			$sql_filter[] = " ".PIX_TABLE_GALL.".gallery_archived = '1' ";
			
		}
		
		if ($filter_name != "") {
			$sql_filter[] = " ".PIX_TABLE_GALL.".gallery_title LIKE '%$filter_name%' AND ".PIX_TABLE_GALL.".gallery_short_title LIKE '%$filter_name%' ";
		}
		
		$sql = implode(" AND ", $sql_filter) . " ";
		
		return $sql;
		
	}
	
	/*
	*
	*	
	*
	*/
	function getParentGalleryTitle ($gallery_parent) {
		
		if (($gallery_parent) != "0") {
		
			// Load the gallery title for the chosen parent gallery
			list ($result) = $this->db->row("SELECT gallery_title FROM ".PIX_TABLE_GALL." WHERE gallery_id = '".$this->db->escape($gallery_parent)."'");
			
			return $result;
			
		} else {
		
			return (bool)FALSE;
			
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function getAvatarData ($image_id) {
		
		global $cfg;
		
		// Get the ID of the key image from the database (if it exists)
		$result = $this->db->row("SELECT image_filename,image_path FROM ".PIX_TABLE_IMGS." WHERE image_id = '".$this->db->escape($image_id)."'");
		
		if (is_array($result)) {
			
			if (file_exists($cfg['sys']['base_library'].$result['image_path']."/80x80/".$result['image_filename'])) {
			
				$small_size	= @getimagesize($cfg['sys']['base_library'].$result['image_path']."/80x80/".$result['image_filename']);
				$large_size	= @getimagesize($cfg['sys']['base_library'].$result['image_path']."/160x160/".$result['image_filename']);
				$comp_size	= @getimagesize($cfg['sys']['base_library'].$result['image_path']."/".COMPING_DIR."/".$result['image_filename']);
				
				$small_path	= base64_encode($cfg['sys']['base_library'].$result['image_path']."/80x80/".$result['image_filename']);
				$large_path	= base64_encode($cfg['sys']['base_library'].$result['image_path']."/160x160/".$result['image_filename']);
				$comp_path	= base64_encode($cfg['sys']['base_library'].$result['image_path']."/".COMPING_DIR."/".$result['image_filename']);
			
			} else {
				
				$small_size	= @getimagesize(SYS_BASE_PATH."/resources/images/missing/80x80_missing.jpg");
				$large_size	= @getimagesize(SYS_BASE_PATH."/resources/images/missing/160x160_missing.jpg");
				
				$small_path	= base64_encode(SYS_BASE_PATH."/resources/images/missing/80x80_missing.jpg");
				$large_path	= base64_encode(SYS_BASE_PATH."/resources/images/missing/160x160_missing.jpg");
				$comp_path	= (bool)false;
			
			}
			
		} else {
			
			$small_size	= getimagesize(SYS_BASE_PATH."/resources/images/missing/80x80_missing.jpg");
			$large_size	= getimagesize(SYS_BASE_PATH."/resources/images/missing/160x160_missing.jpg");
			
			$small_path	= base64_encode(SYS_BASE_PATH."/resources/images/missing/80x80_missingjpg");
			$large_path	= base64_encode(SYS_BASE_PATH."/resources/images/missing/160x160_missing.jpg");
			$comp_path	= (bool)false;
		
		}
		
		// Load data into Smarty
		$data['small']['size'] 	= $small_size;
		$data['small']['path'] 	= $small_path;
		$data['large']['size'] 	= $large_size;
		$data['large']['path'] 	= $large_path;
		$data['comp']['size'] 	= $comp_size;
		$data['comp']['path'] 	= $comp_path;	
		
		return $data;
		
	}
	
	/*
	*
	*	
	*
	*/
	function getGalleryParentPermissions ($gallery_parent_id) {
		
		if ($gallery_parent_id == "0") {
			
			$data['gallery_password']		= "";
			$data['gallery_permissions']	= "10";
			$data['gallery_viewers']		= array();			
			$data['gallery_viewers_name']	= array();			
			
			return $data;
		
		}
		
		// Load permissions from parent gallery
		$result = $this->db->row("SELECT * FROM ".PIX_TABLE_GALL." WHERE gallery_id = '".$this->db->escape($gallery_parent_id)."'");
		
		$data['gallery_password']		= $result['gallery_password'];
		$data['gallery_permissions']	= $result['gallery_permissions'];
								
		// Load parent gallery viewing users
		$result = $this->db->rows("SELECT * FROM ".PIX_TABLE_GVIE." WHERE gallery_id = '".$this->db->escape($gallery_parent_id)."'");
		
		if (is_array($result)) {
		
			foreach($result as $key => $value) {
			
				$gallery_viewers[]		= $result[$key]['userid'];
				$gallery_viewers_name[]	= getGroupName($result[$key]['userid']);
			
			}
		
		}
		
		$data['gallery_viewers']		= $gallery_viewers;			
		$data['gallery_viewers_name']	= $gallery_viewers_name;			
		
		return $data;
		
	}
	
}

?>