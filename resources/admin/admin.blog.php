<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set the include path for files used in this script
ini_set("include_path","../includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Set the site section
$admin_page_section = "blog";

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Send HTML content HTTP header and don't cache
pix_http_headers("html","");

// Check that the user is a SuperUser
pix_authorise_user("administrator");

$Admin_Blog = new Admin_Blog();

class Admin_Blog {

	var $action_complete = false;
	var $_dbl;

	function Admin_Blog () {
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		global $objEnvData, $ses, $cfg;
		
		switch ($objEnvData->fetchGlobal('cmd')) {
			
			case "showFormEntryCreate":
				$this->showFormEntryCreate();
			break;
			
			case "saveNewEntry":
				$this->saveNewEntry();
			break;
			
			case "showFormEditEntry":
				$this->showFormEditEntry();
			break;
			
			case "saveEditedEntry":
				$this->saveEditedEntry();
			break;
			
			case "deleteEntry":
				$this->deleteEntry();
			break;
			
			default:
				$this->showFormNewEntryAndList();
			break;
		
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function showFormNewEntryAndList () {
		
		global $smarty, $objEnvData, $cfg;
		
		// Get an array of blog entries from the database
		
		$sql = "SELECT *, CONCAT(".PIX_TABLE_USER.".first_name,' ',".PIX_TABLE_USER.".family_name) AS blog_name
											
				FROM ".PIX_TABLE_BLOG." 
											
				LEFT JOIN ".PIX_TABLE_USER." ON ".PIX_TABLE_BLOG.".blog_author = ".PIX_TABLE_USER.".userid
				
				ORDER BY blog_date DESC";
		
		$blog_info = $this->_dbl->sqlSelectRows($sql);
		
		if (is_array($blog_info) && count($blog_info) > 0) {
		
			foreach($blog_info as $key => $value) {
				
				$blog_item_id[]	= $blog_info[$key]['blog_item_id'];
				$blog_title[]	= stripslashes($blog_info[$key]['blog_title']);
				$blog_date[]	= $blog_info[$key]['blog_date'];
				$blog_author[]	= stripslashes($blog_info[$key]['blog_name']);
				
			}
		
		}
		
		// Assign the number of items in the blog to Smarty
		$smarty->assign("blog_count",count($blog_info));
		
		// Assign the current time to Smarty
		$smarty->assign("blog_date",time());
		
		// Assign blog items to Smarty
		$smarty->assign("blog_item_id",$blog_item_id);
		$smarty->assign("blog_title",$blog_title);
		$smarty->assign("blog_date",$blog_date);
		$smarty->assign("blog_author",$blog_author);
		
		// Set the page title
		$smarty->assign("page_title","Blog control panel");
		
		// Tell Smarty if an action is complete
		$smarty->assign("action_complete",$this->action_complete);
		
		// Output the library page
		$smarty->pixDisplay('admin.blog/blog.tpl');
		
	}
	
	/*
	*
	*	
	*
	*/
	function showFormEntryCreate () {
	
		global $smarty, $objEnvData, $cfg;
		
		// Load an array of nested categories
		$gallery_list = galleryListingArray();
		
		// Load the category list data into Smarty
		$smarty->assign("menu_gallery_title",$gallery_list[0]);
		$smarty->assign("menu_gallery_id",$gallery_list[1]);
							
		// Set the page title
		$smarty->assign("page_title","Create a new blog entry");
		
		// Output the library page
		$smarty->pixDisplay('admin.blog/blog.form.create.tpl');
		
	}
	
	/*
	*
	*	
	*
	*/
	function saveNewEntry () {
	
		global $smarty, $objEnvData, $cfg, $ses;
		
		// Function replicated in admin.gallery.php for new blog when creating gallery
		
		// Laod information from form
		$blog_title			= addslashes($objEnvData->fetchGlobal('blog_title'));
		$blog_content		= addslashes($objEnvData->html('blog_content'));
		$blog_gallery_id	= addslashes($objEnvData->fetchGlobal('blog_gallery_id'));
		$blog_userid		= $ses['psg_userid'];
		
		$toda_Month			= str_pad($objEnvData->fetchGlobal('toda_Month'),2,"00",STR_PAD_LEFT);
		$toda_Day			= str_pad($objEnvData->fetchGlobal('toda_Day'),2,"00",STR_PAD_LEFT);
		$toda_Year			= $objEnvData->fetchGlobal('toda_Year');
		$toda_Hour			= str_pad($objEnvData->fetchGlobal('toda_Hour'),2,"00",STR_PAD_LEFT);
		$toda_Minute		= str_pad($objEnvData->fetchGlobal('toda_Minute'),2,"00",STR_PAD_LEFT);
		$toda_Second		= str_pad($objEnvData->fetchGlobal('toda_Second'),2,"00",STR_PAD_LEFT);
		
		// Turn the input date and time values into a MySQL compatible string
		$blog_date			= $toda_Year."-".$toda_Month."-".$toda_Day." ".$toda_Hour.":".$toda_Minute.":".$toda_Second;
		
		$this->_dbl->sqlQuery("INSERT INTO ".PIX_TABLE_BLOG." VALUES('0','$blog_title','$blog_content','$blog_date','$blog_userid','$blog_gallery_id');");
		
		// Confirm that this action is complete
		$this->action_complete = true;
		
		// Generate a redirection URL 
		$this->showFormNewEntryAndList();		

	}
	
	/*
	*
	*	
	*
	*/
	function showFormEditEntry () {
	
		global $smarty, $objEnvData, $cfg;
		
		// Laod information from form
		$blog_item_id		= $objEnvData->fetchGlobal('blog_item_id');

		$sql = "SELECT *
											
				FROM ".PIX_TABLE_BLOG." 
											
				WHERE blog_item_id = '$blog_item_id'";
		
		$blog_info = $this->_dbl->sqlSelectRow($sql);
		
		// Assign blog items to Smarty
		$smarty->assign("blog_item_id",$blog_info['blog_item_id']);
		$smarty->assign("blog_title",stripslashes($blog_info['blog_title']));
		$smarty->assign("blog_content",stripslashes($blog_info['blog_content']));
		$smarty->assign("blog_gallery_id",$blog_info['blog_gallery_id']);
		$smarty->assign("blog_date",$blog_info['blog_date']);
		
		// Set the page title
		$smarty->assign("page_title","Blog control panel");
		
		// Load an array of nested categories
		$gallery_list = galleryListingArray();
		
		// Load the category list data into Smarty
		$smarty->assign("menu_gallery_title",$gallery_list[0]);
		$smarty->assign("menu_gallery_id",$gallery_list[1]);
							
		// Output the library page
		$smarty->pixDisplay('admin.blog/blog.form.edit.tpl');
		
	}
	
	/*
	*
	*	
	*
	*/
	function saveEditedEntry () {
		
		global $smarty, $objEnvData, $cfg;
		
		// Laod information from form
		$blog_title			= addslashes($objEnvData->fetchGlobal('blog_title'));
		$blog_content		= addslashes($objEnvData->html('blog_content'));
		$blog_item_id		= $objEnvData->fetchGlobal('blog_item_id');
		$blog_gallery_id	= $objEnvData->fetchGlobal('blog_gallery_id');
		
		$toda_Month			= str_pad($objEnvData->fetchGlobal('toda_Month'),2,"00",STR_PAD_LEFT);
		$toda_Day			= str_pad($objEnvData->fetchGlobal('toda_Day'),2,"00",STR_PAD_LEFT);
		$toda_Year			= $objEnvData->fetchGlobal('toda_Year');
		$toda_Hour			= str_pad($objEnvData->fetchGlobal('toda_Hour'),2,"00",STR_PAD_LEFT);
		$toda_Minute		= str_pad($objEnvData->fetchGlobal('toda_Minute'),2,"00",STR_PAD_LEFT);
		$toda_Second		= str_pad($objEnvData->fetchGlobal('toda_Second'),2,"00",STR_PAD_LEFT);
		
		// Turn the input date and time values into a MySQL compatible string
		$blog_date			= $toda_Year."-".$toda_Month."-".$toda_Day." ".$toda_Hour.":".$toda_Minute.":".$toda_Second;
		
		$sql = "UPDATE ".PIX_TABLE_BLOG." 	SET			blog_title		= '$blog_title',
														blog_date		= '$blog_date',
														blog_content	= '$blog_content',
														blog_gallery_id	= '$blog_gallery_id'
														
														WHERE blog_item_id = '$blog_item_id';";
		
		$this->_dbl->sqlQuery($sql);
		
		// Confirm that this action is complete
		$this->action_complete = true;
		
		// Generate a redirection URL 
		$this->showFormNewEntryAndList();		

	}
	
	/*
	*
	*	
	*
	*/
	function deleteEntry () {
	
		global $smarty, $objEnvData, $cfg;
		
		$blog_item_id = $objEnvData->fetchGlobal('blog_item_id');
	
		$this->_dbl->sqlQuery("DELETE FROM ".PIX_TABLE_BLOG." WHERE blog_item_id = '$blog_item_id'");
		
		// Confirm that this action is complete
		$this->action_complete = true;
		
		// Generate a redirection URL 
		$this->showFormNewEntryAndList();		

	}
	
}

?>