<?php if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	SMARTY EXTENDER - ONLY FOR USE WITH PIXARIA 3.0 FRAMEWORKS
*	
*	This class creates an instance of Smarty with variables and settings
*	specific to Pixaria Gallery including all the global system and language
*	settings and style attributes
*
*/

// Load Pixaria specific Smarty extensions
require_once (APP_PATH . "smarty.php");

// Load the main Smarty class
require_once (RES_PATH . "smarty/libs/Smarty.class.php");


class SmartyPixaria extends Smarty {
	
	var $pixaria_config;
	
	/*
	*
	*	PIXARIA SMARTY TEMPLATE ENGINE CLASS
	*
	*	Extends the Smarty class with Pixaria specific additions
	*
	*/
	function SmartyPixaria($pixaria_config) {
				
		/*
		*
		*	DEFINE TEMPLATE DIRECTORY PATH
		*
		*/
		if (eregi("resources/admin",$_SERVER['REQUEST_URI'])) {
			$this->template_dir		= RES_PATH . 'templates_admin/';
		} else {
			$this->template_dir		= RES_PATH . 'templates_default/';
		}
		
		/*
		*
		*	SET UP SMARTY ENVIRONMENT
		*
		*/
		$this->compile_dir		= BASE_PATH.'resources/smarty/templates_c/';
		$this->config_dir		= BASE_PATH.'resources/smarty/configs/';
		$this->cache_dir		= BASE_PATH.'resources/smarty/cache/';
		$this->compile_id		= LOCAL_LANGUAGE;
		
		/*
		*
		*	ASSIGN SMARTY CONFIGURATION VARIABLES
		*
		*/
		$this->compile_check 	= false;
		$this->debugging 		= false;
		$this->caching 			= false;
		
		/*
		*
		*	IF PIXARIA TEMPLATE CACHING IS OFF, CLEAR COMPILED TEMPLATES
		*
		*/
		if ($cfg['set']['smarty_cache'] == 1) { $this->clear_compiled_tpl(); }
				
		/*
		*
		*	ASSIGN SITE PROPERTIES TO SMARTY
		*
		*/
		$this->assign("site_name",$cfg['set']['site_name']);
		$this->assign("base_url",$cfg['sys']['base_url']);
		$this->assign("base_path",BASE_PATH);
		$this->assign("base_incoming",$cfg['sys']['base_incoming']);
		$this->assign("base_uri",htmlentities($_SERVER['REQUEST_URI']));
		$this->assign("query_string",htmlentities($_SERVER['QUERY_STRING']));
		$this->assign("base_complete",$cfg['sys']['base_url'] . ltrim($_SERVER['REQUEST_URI'],"/"));
		
		/*
		*
		*	REGISTER CUSTOM FUNCTIONS AND POST-FILTERS
		*
		*/
		$this->register_function("include_template","smarty_include_template");
		$this->register_function("load_pixie","smarty_load_pixie");
		$this->register_function("html_fancy_select_multiple","smarty_fancy_select_multiple");
		$this->register_function("html_fancy_select_single","smarty_fancy_select_single");
		$this->register_postfilter("smarty_localisation_processor");
		
		if (is_array($lang)) { foreach ($lang as $key => $value) {
		
			$this->assign("lan_".$key,$value);
		
		} }
		
		if (is_array($cfg['sys'])) { foreach ($cfg['sys'] as $key => $value) {
		
			$this->assign("sys_".$key,$value);
		
		} }

		if (is_array($cfg['set'])) { foreach ($cfg['set'] as $key => $value) {
		
			$this->assign("set_".$key,$value);
		
		} }

		if (is_array($cfg['fil'])) { foreach ($cfg['fil'] as $key => $value) {
		
			$this->assign("fil_".$key,$value);
		
		} }

		if (count($ses) > 0) {
		
			foreach ($ses as $key => $value) {
			
				$this->assign("ses_".$key,$value);
			
			}
			
		}

	}
	
	/*
	*
	*	EXTENDS THE PARENT DISPLAY METHOD
	*
	*	Checks to see if template file is in the theme directory and
	*	if it isn't loads and displays the default template file instead.
	*
	*/
	function display ($template) {
		
		global $cfg, $admin_page;
		
		// Security
		if (eregi("http://",$template)) { return; }
		if (eregi("ftp://",$template)) { return; }
		
		if ($admin_page) {
		
			Smarty::display($template);
			
		} else {
		
			if (file_exists(RES_PATH."themes/".$cfg['set']['theme']."/templates/".$template)) {
				
				Smarty::display(RES_PATH."themes/".$cfg['set']['theme']."/templates/".$template);
			
			} else {
				
				Smarty::display($template);
			
			}
		
		}
		
	}
	
	/*
	*
	*	EXTENDS THE PARENT FETCH METHOD
	*
	*	Checks to see if template file is in the theme directory and
	*	if it isn't loads and outputs the default template file as a
	*	string instead.
	*
	*/
	function parse ($template) {
		
		global $cfg, $admin_page;
		
		// Security
		if (eregi("http://",$template)) { return; }
		if (eregi("ftp://",$template)) { return; }
		
		if ($admin_page) {
		
			$result = Smarty::fetch($template);
			
		} else {
		
			if (file_exists(RES_PATH."themes/".$cfg['set']['theme']."/templates/".$template)) {
				
				$result = Smarty::fetch(RES_PATH."themes/".$cfg['set']['theme']."/templates/".$template);
			
			} else {
				
				$result = Smarty::fetch($template);
			
			}
		
		}
		
		return $result;
		
	}

}

?>