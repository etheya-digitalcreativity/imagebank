<?php if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

class FileCore {
	
	function FileCore () {
	
		
	
	}
	
	/*
	*
	*	
	*
	*/
	function chmod ($file_name, $octal=0777) {
		
		$old_umask = umask(0);
		@chmod($file_name,$octal);
		umask($old_umask);
	
	}
	
	/*
	*
	*	
	*
	*/
	function getDateTimeModified ($path, $format='') {
		
		if (file_exists($path) && !is_dir($path)) {
			
			$timestamp = filemtime($path);
			
			if ($format != '') {
				
				return @strftime($format, $timestamp);
			
			} else {
				
				return @strftime('%Y-%m-%d %T', $timestamp);
				
			}
		
		} else {
		
			return false;
		
		}
	
	}
	
	/*
	*
	*	CHECK IF A DIRECTORY EXISTS AND IF IT DOESN'T MAKE IT
	*
	*/
	function createDirectory ($path) {
		
		if (!file_exists($path)) {
			
			$umask_old = umask(0);
			mkdir($path, 0777);
			$directory_created = true;
			umask($old);
			
		} elseif (substr(sprintf('%o', fileperms($path)), -4) != 0777) {
			
			$path = umask(0);
			@chmod($path,0777);
			umask($old);
			
			$directory_created = false;
			
		}
		
		return $directory_created;
	
	}
	
	/*
	*
	*	DELETE A DIRECTORY AND EVERYTHING IN IT
	*
	*/
	function removeDirectory ($dir) {
		
		if (substr($dir, strlen($dir)-1, 1) != '/') {
			
			$dir .= '/';
		
		}
				
		if ($handle = @opendir($dir)) {
		
			while ($obj = @readdir($handle)) {
			
				if ($obj != '.' && $obj != '..') {
				
					if (@is_dir($dir.$obj)) {
					
						if (!$this->removeDirectory($dir.$obj)) {
						
							return false;
							
						}
						
					} elseif (@is_file($dir.$obj)) {
						
						if (!@unlink($dir.$obj)) {
						
							return false;
							
						}
					
					}
				
				}
			
			}
			
			@closedir($handle);
			
			if (!@rmdir($dir)) {
			
				return false;
				
			}
			
			return true;
		
		}
	
	}
	
	/*
	*
	*	CHECK A DIRECTORY IS EMPTY
	*
	*	Returns true if dir is empty, false if it isn't
	*
	*/
	function isEmptyDirectory ($dir) {
	
		if (is_dir($dir)) {
	
			$dl = opendir($dir);
			
			if ($dl) {
			
				while($name = readdir($dl)) {
					
					if (!is_dir("$dir/$name") && !substr($name,0,1) != '.') {
					
						return false;
						
						break;
					
					}
				
				}
	
				closedir($dl);
			
			}
			
			return true;
		
		} else {
		
			return true;
		
		}
	
	}

}