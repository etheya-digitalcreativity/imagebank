<?php if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

class MySQL {

	var $dbl;
	var $query_time;
	
	function MySQL () {
		
		$this->dbl = mysql_connect(DB_HOST, DB_USER, DB_PASS) or die(mysql_error());
			
		$db = mysql_select_db(DB_NAME, $this->dbl) or die(mysql_error());
		
		// Tell MySQL to work with UTF-8 text
		$this->query("SET NAMES 'utf8'");
		
		// Tell MySQL to operate in 'traditional' SQL mode (http://bugs.mysql.com/bug.php?id=18551)
		$this->query("SET sql_mode = ''");

		// Turn on big SQL select join capability to deal with large databases
		$this->query("SET SQL_BIG_SELECTS=1");
		
		return $db;
		
	}
	
	/*
	*	
	*	sqlQuery -- Select one row from table
	*
	*	mixed sqlQuery(string sql)
	*
	*	Sends SQL to the mysql_query function
	*
	*/
	function query ($sql) {
		
		$start_time = microtime(true);
		
		$result = @mysql_query($sql);
		
		$end_time = microtime(true);
		
		$this->query_time = $end_time - $start_time;
		
		$this->errorHandler(__METHOD__, 'mysql_query', $sql);
		
		return $result;
		
	}
	
	/*
	*	
	*	RETURN ARRAY OF A SINGLE ROW OF DATA
	*
	*	Returns a single row of data as an array with each value
	*	representing a single column from the select query.
	*
	*/
	function row ($sql, $result_type=MYSQL_BOTH) {
		
		if ($result_type == "") { $result_type = MYSQL_BOTH; }
		
		$start_time = microtime(true);
		
		$result = @mysql_query($sql);
		
		$end_time = microtime(true);
		
		$this->query_time = $end_time - $start_time;
		
		// Catch errors
		$this->errorHandler(__METHOD__, 'mysql_query', $sql);
		
		if (($numrow = @mysql_num_rows($result)) < 1) { return(FALSE); }
	
		$array = @mysql_fetch_array($result, $result_type);
		
		// Catch errors
		$this->errorHandler(__METHOD__, 'mysql_fetch_array', $sql);
		
		return($array);
	
	}
	
	/*
	*	
	*	RETURN AN ARRAY OF ROWS OF DATAS
	*
	*	Returns multiple rows of data as an array with each value
	*	representing a row of data as another array of columns.
	*
	*/
	function rows ($sql, $result_type=MYSQL_BOTH, $debug="") {
		
		if ($debug): print $sql; exit; endif;
		
		$start_time = microtime(true);
		
		$result = @mysql_query($sql);
	
		$end_time = microtime(true);
		
		$this->query_time = $end_time - $start_time;
		
		// Catch errors
		$this->errorHandler(__METHOD__, 'mysql_query', $sql);
		
		if(($numrow = @mysql_num_rows($result)) < 1){return(FALSE);}
	
		$i=0;while($i < $numrow){
	
			$array[$i] = @mysql_fetch_array($result, $result_type);
	
			$i++;
		}
	
		@mysql_free_result($result);
		
		return $array;
	
	}
	
	/*
	*	
	*	RETURN MULTIPLE ROWS OF DATA AS ARRAY OF COLUMNS
	*
	*	This method selects a set of rows of data and then transforms
	*	the array of rows into an array of columns.  It returns an array
	*	containing a list of the returned values for each column included
	*	in the SQL select statement.
	*
	*/
	function columns ($sql) {
		
		$result = $this->rows($sql, MYSQL_NUM);
		
		if (is_array($result)) {
			
			$result = $result;
			
			foreach ($result as $key => $value) {
			
				$items = count($value);
				
				for ($i = 0; $i < $items; $i++) {
					
					$data_{$i}[] = $value[$i];
				
				}
				
			}
			
			for ($i = 0; $i < $items; $i++) {
					
				$array[] = $data_{$i};
				
			}
			
			return $array;
	
		} else {
		
			return false;
		
		}
		
	}

	/*
	*	
	*	RETURN THE NUMBER OF ROWS IN THE RESULT
	*
	*/
	function count($sql) {
		
		$start_time = microtime(true);
		
		$result = @mysql_query($sql);
		
		$end_time = microtime(true);
		
		$this->query_time = $end_time - $start_time;
		
		// Catch errors
		$this->errorHandler(__METHOD__, 'mysql_query', $sql);
		
		$numrow = @mysql_num_rows($result);
		
		// Catch errors
		$this->errorHandler(__METHOD__, 'mysql_num_rows', $sql);
		
		if ($numrow < 1 || $numrow == "") { return('0'); }
	
		$array = @mysql_fetch_array($result);
		
		// Catch errors
		$this->errorHandler(__METHOD__, 'mysql_fetch_array', $sql);
		
		return($array[0]);
	
	}
	
	/*
	*	
	*	CLOSE SQL CONNECTION
	*
	*/
	function close () {
		return @mysql_close($this->dbl);
	}
	
	/*
	*	
	*	RETURN VALUE OF LAST AUTO_INCREMENT
	*
	*/
	function insertId () {
		$start_time = microtime(true);
		
		$result = @mysql_insert_id();
		
		$end_time = microtime(true);
		
		$this->query_time = $end_time - $start_time;
		
		$this->errorHandler(__METHOD__, 'mysql_insert_id', $sql);
		
		return $result;
		
	}
	
	/*
	*	
	*	RETURN LAST ERROR (IF ANY)
	*
	*	Returns the error message if there is one and false
	*	if there is no error to return.
	*
	*/
	function errorHandler ($method, $function, $sql) {
		
		$sql = preg_replace('/\s+/',' ',$sql);
		
		$error = @mysql_error();

		if ($error == '' && MYSQL_DEBUG == 0) {
		
			return false;
		} elseif ($error == '' && MYSQL_DEBUG == 1) {
			
			$error_report .= "<span class=\"pre\">$sql</span>";
			$error_report .= "MySQL method - '<tt>$method</tt>':Run time - $this->query_time";
			
			trigger_error($error_report, E_USER_WARNING);
			
		} else {
			
			$error_report  = "MySQL error in method '<tt>$method</tt>':<br /><span class=\"pre\">$error</span>";
			$error_report .= "Using function '$function' with SQL:<span class=\"pre\">$sql</span>";
			
			// Trigger an error
			trigger_error($error_report, E_USER_WARNING);
			
		}
				
	}
	
	/*
	*	
	*	SET A VARIABLE WITHIN MYSQL
	*
	*	Takes the variable name '$name' and the value of the variable '$value'
	*
	*/
	function set ($name, $value) {
		
		$sql = "SET @".$name." = '".$value."'";
		
		$start_time = microtime(true);
		
		$result = @mysql_query($sql);
		
		$this->errorHandler(__METHOD__, 'mysql_query', $sql);
		
		return $result;
		
	}
	
	/*
	*
	*	RETURN ESCAPED STRING
	*
	*/
	function escape ($string) {
	
	    $string = str_replace ("\0",'',$string);
	    
	    if (get_magic_quotes_gpc()) $string = stripslashes($string);
	    
	    return mysql_real_escape_string($string);
	    
	}
		
}

?>