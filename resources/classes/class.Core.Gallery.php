<?php if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

class GalleryCore {
	
	function GalleryCore () {
	
		require_once ("class.DAO.PixariaGallery.php");
		
		// Create the database object
		$this->db = new Database();
	
	}
	
	/*
	*	
	*	INSTANTIATE AND RETURN A GALLERY DATA OBJECT
	*	
	*/
	function createGalleryObject ($gallery_id='') {
	
		return new DAO_PixariaGallery($gallery_id);
	
	}
	
	/*
	*
	*	
	*
	*/
	function getAvatarData ($image_id) {
		
		global $cfg;
		
		$sql = "SELECT	 image_filename
						,image_path
				
				FROM 	".PIX_TABLE_IMGS."
				
				WHERE 	image_id = '".$this->db->escape($image_id)."'";
		
		// Get the ID of the key image from the database (if it exists)
		$result = $this->db->row($sql);
		
		if (is_array($result)) {
			
			if (file_exists($cfg['sys']['base_library'].$result['image_path']."/80x80/".$result['image_filename'])) {
			
				$small_size	= @getimagesize($cfg['sys']['base_library'].$result['image_path']."/80x80/".$result['image_filename']);
				$large_size	= @getimagesize($cfg['sys']['base_library'].$result['image_path']."/160x160/".$result['image_filename']);
				$comp_size	= @getimagesize($cfg['sys']['base_library'].$result['image_path']."/".COMPING_DIR."/".$result['image_filename']);
				
				$small_path	= base64_encode($cfg['sys']['base_library'].$result['image_path']."/80x80/".$result['image_filename']);
				$large_path	= base64_encode($cfg['sys']['base_library'].$result['image_path']."/160x160/".$result['image_filename']);
				$comp_path	= base64_encode($cfg['sys']['base_library'].$result['image_path']."/".COMPING_DIR."/".$result['image_filename']);
			
			} else {
				
				$small_size	= @getimagesize(SYS_BASE_PATH."/resources/images/missing/80x80_missing.jpg");
				$large_size	= @getimagesize(SYS_BASE_PATH."/resources/images/missing/160x160_missing.jpg");
				
				$small_path	= base64_encode(SYS_BASE_PATH."/resources/images/missing/80x80_missing.jpg");
				$large_path	= base64_encode(SYS_BASE_PATH."/resources/images/missing/160x160_missing.jpg");
				$comp_path	= (bool)false;
			
			}
			
		} else {
			
			$small_size	= getimagesize(SYS_BASE_PATH."/resources/images/missing/80x80_missing.jpg");
			$large_size	= getimagesize(SYS_BASE_PATH."/resources/images/missing/160x160_missing.jpg");
			
			$small_path	= base64_encode(SYS_BASE_PATH."/resources/images/missing/80x80_missingjpg");
			$large_path	= base64_encode(SYS_BASE_PATH."/resources/images/missing/160x160_missing.jpg");
			$comp_path	= (bool)false;
		
		}
		
		// Load data into Smarty
		$data['small']['size'] 	= $small_size;
		$data['small']['path'] 	= $small_path;
		$data['large']['size'] 	= $large_size;
		$data['large']['path'] 	= $large_path;
		$data['comp']['size'] 	= $comp_size;
		$data['comp']['path'] 	= $comp_path;	
		
		return $data;
		
	}
	
	/*
	*
	*	This function returns a two dimensional array of nested gallery names
	*	and the id number of each gallery.  Output is sorted alphabetically.
	*
	*/
	function galleryListingArray ($filter="") {
	
		global $cfg, $gallery_array, $gallery_id, $gallery_parents, $gallery_name;
		
		$gallery_name		= array();
		$gallery_parents	= array();
		$gallery_array		= array();
		$gallery_id			= array();
		
		if ($filter != "") {
			$filter = " AND " . $filter . " ";
		}
		
		$sql = "SELECT 		 ".PIX_TABLE_GALL.".gallery_id
							,".PIX_TABLE_GALL.".gallery_title
							,".PIX_TABLE_GALL.".gallery_parent
								
				FROM 		".PIX_TABLE_GALL."
				
				LEFT JOIN 	".PIX_TABLE_GVIE." ON ".PIX_TABLE_GALL.".gallery_id = ".PIX_TABLE_GVIE.".gallery_id
				
				WHERE 		".PIX_TABLE_GALL.".gallery_id != '0'
				
				".$cfg['set']['gallery_access_sql']."
				
				".$cfg['set']['gallery_visibility_sql']."
				
				$filter
				
				GROUP BY 	".PIX_TABLE_GALL.".gallery_id";
		
		$result = $this->db->rows($sql);
		
		if (is_array($result) && count($result) > 0) {
			
			foreach ($result as $key => $value) {
			
				$gallery_parents[$value["gallery_parent"]][] 	= $value["gallery_id"];
				$gallery_name[$value["gallery_id"]]				= $value["gallery_title"];
			
			}
			
			ksort($gallery_parents);
			
			$sql = "SELECT 		".PIX_TABLE_GALL.".gallery_id
			
					FROM 		".PIX_TABLE_GALL."
			
					LEFT JOIN 	".PIX_TABLE_GVIE." ON ".PIX_TABLE_GALL .".gallery_id = ".PIX_TABLE_GVIE.".gallery_id
			
					WHERE 	".PIX_TABLE_GALL.".gallery_parent = '0'
					
					".$cfg['set']['gallery_access_sql']."
					
					".$cfg['set']['gallery_visibility_sql']."
					
					$filter
					
					GROUP BY 	".PIX_TABLE_GALL.".gallery_id
					
					ORDER by 	".PIX_TABLE_GALL.".gallery_title ASC";
			
			$parents = $this->db->rows($sql);
			
			if (is_array($parents)) {
			
				foreach ($parents as $key => $value) {
				
					$this->galleryNestedArray($value['gallery_id']);
				
				}
			
			}
			
		}
		
		$gallery_info[] = $gallery_array;
		$gallery_info[] = $gallery_id;
		
		return $gallery_info;
	
	}

	/*
	*
	*	THIS METHOD CREATES AN ARRAY OF NESTED GALLERIES
	*
	*/
	function galleryNestedArray ($id) {
	
		global $gallery_parents, $gallery_name, $cfg, $gallery_id, $gallery_array, $indent;
	
		$gallery_array[]	= $indent.$gallery_name[$id];
		
		$gallery_id[]		= $id;
		
		if(isset($gallery_parents[$id]) && is_array($gallery_parents[$id])) {
		
		
			foreach($gallery_parents[$id] as $kind) {
				$indent .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				$this->galleryNestedArray($kind);
			}
			
		}
		
		$indent = substr($indent,0,-36);
	
	}

	/*
	*
	*	
	*
	*/
	function getGalleryParentPermissions ($gallery_parent_id) {
		
		if ($gallery_parent_id == "0") {
			
			$data['gallery_password']		= "";
			$data['gallery_permissions']	= "10";
			$data['gallery_viewers']		= array();			
			$data['gallery_viewers_name']	= array();			
			
			return $data;
		
		}
		
		// Load permissions from parent gallery
		$result = $this->db->row("SELECT * FROM ".PIX_TABLE_GALL." WHERE gallery_id = '$gallery_parent_id'");
		
		$data['gallery_password']		= $result['gallery_password'];
		$data['gallery_permissions']	= $result['gallery_permissions'];
								
		// Load parent gallery viewing users
		$result = $this->db->rows("SELECT * FROM ".PIX_TABLE_GVIE." WHERE gallery_id = '$gallery_parent_id'");
		
		if (is_array($result)) {
		
			foreach($result as $key => $value) {
			
				$gallery_viewers[]		= $result[$key]['userid'];
				$gallery_viewers_name[]	= getGroupName($result[$key]['userid']);
			
			}
		
		}
		
		$data['gallery_viewers']		= $gallery_viewers;			
		$data['gallery_viewers_name']	= $gallery_viewers_name;			
		
		return $data;
		
	}
	
	/*
	*
	*	This method returns a two dimensional array of user group names
	*	and the id number of each group.  Output is sorted alphabetically.
	*
	*/
	function groupListArray () {
		
		// Administrators can do everything so we don't include them in this list
		$sql = "SELECT 		*
				
				FROM 		" . PIX_TABLE_GRPS . "
				
				WHERE 		group_id != '1'
				
				AND 		group_id != 2
				
				ORDER BY 	group_name ASC";
		
		$result = $this->db->rows($sql);
		
		if (is_array($result)) {
			
			$group_id = array();
			$group_name	= array();
			
			foreach ($result as $key => $value) {
			
				$group_id[] 		= $value['group_id'];
				$group_name[]		= $value['group_name'];
				
			}
		
		}
	
		$group_info[] = $group_name;
		$group_info[] = $group_id;
		
		return $group_info;
			
	}

}