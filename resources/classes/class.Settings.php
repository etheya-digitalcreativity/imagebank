<?php if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

class Settings {
	
	// Private variables
	var $db;
	var $input;
	var $settings;
	var $config = array();
	
	/*
	*	
	*	CONSTRUCTOR FOR THE SETTINGS OBJECT
	*	
	*	Manage settings and configuration
	*
	*/
	function Settings($db, $input) {
		
		$this->db		=& $db;
		$this->input	=& $input;
		
		// Get the stored settings from the database
		$this->settings = $this->db->rows("SELECT * FROM " .  PIX_TABLE_SETT, MYSQL_ASSOC);
		
		// Loads all the stored settings and pre-defined settings into an array
		$this->loadConfiguration();
		
		$this->settingsToConstants();
		
		return $this->config;
		
	}
	
	/*
	*	
	*	LOAD DATABASE SETTINGS AND PRE-DEFINED VARIABLES INTO AN ARRAY
	*
	*/
	function loadConfiguration() {
		
		//	Feed data into settings array
		while (list($key, $value) = @each($this->settings)) {
			
			$this->config[$value['name']] = stripslashes($value['value']);
		
		}

		// Preserve the default values for the number of images/galleries per page
		$this->config['thumb_per_page_def']		= $this->config['thumb_per_page'];
		$this->config['gallery_per_page_def'] 	= $this->config['gallery_per_page'];
		
		// The number of images that can be imported in one go in the admin area
		$this->config['import_limit'] 		= "100";
		
		// Load variables from user preferences cookie
		if ($psg_prefs = $this->input->cookie('psg_prefs')) {
		
			$vars = explode("|",$psg_prefs);
			
			// If the user has set a number of thumbnail items to show, set that as the default
			if (is_numeric($vars[1])) { $this->config['thumb_per_page'] = $vars[1]; }
			
			// If the user has set a number of gallery items to show, set that as the default
			if (is_numeric($vars[2])) { $this->config['gallery_per_page'] = $vars[2]; }
			
			// If the user has set a preferred thumbnail view use this
			if ($vars[3] == "large" || $vars[3] == "small") { $this->config['view_thumb_view'] = $vars[3]; } else { $this->config['view_thumb_view'] = "large"; }
			
		}
		
		// Shopping cart popup window sizes
		$this->config['popup_cart_width']		= "700";
		$this->config['popup_cart_height']	= "600";
		
		// If the photo storage directoy is an absolute path (Unix or Windows)
		if (substr(0,1,$this->config['path_library']) == '/' || substr(1,1,$this->config['path_library']) == ':') {
				
			//  Photo storage directory
			$this->config['base_library']			= $this->config['path_library'];
			
		} else {
				
			//  Photo storage directory
			$this->config['base_library']			= BASE_PATH . $this->config['path_library'];
			
		}
		
		define("SYS_BASE_LIBRARY",$this->config['base_library']);
		
		// If the incoming directoy is an absolute path (Unix or Windows)
		if (substr(0,1,$this->config['path_incoming']) == '/' || substr(1,1,$this->config['path_incoming']) == ':') {
				
			//  FTP upload directory
			$this->config['base_incoming']		= $this->config['path_incoming'];
			
		} else {
				
			//  FTP upload directory
			$this->config['base_incoming']		= BASE_PATH . $this->config['path_incoming'];
			
		}
		
		define("SYS_BASE_INCOMING",$this->config['base_library']);

		switch ($this->config['store_currency']) {
		
			case "USD": // Currency is dollars
			case "CAD":
			case "AUD":
			case "NZD":
			case "SGD":
				$this->config['store_symbol'] 	= "\$";
			break;
			
			case "GBP": // Currency is Pounds Sterling
				$this->config['store_symbol'] 	= "&pound;";
			break;
			
			case "EUR": // Currency is Euros
				$this->config['store_symbol'] 	= "&euro;";
			break;
			
			case "JPY": // store_symbol is Japanese Yen
				$this->config['store_symbol'] 	= "&yen;";
			break;
			
			case "CHF": // Currency is Swiss Francs
				$this->config['store_symbol'] 	= "Fr ";
			break;
			
			case "HUF": // Currency is Hungarian Forint
				$this->config['store_symbol'] 	= "Ft ";
			break;
			
			case "PLN": // Currency is Polish Zloty
				$this->config['store_symbol'] 	= "z&#322; ";
			break;
			
			case "CZK": // Currency is Czech Koruna
				$this->config['store_symbol'] 	= "K&#269; ";
			break;
			
			case "NOK": // Currency is Kroner
			case "DKK":
			case "SEK":
				$this->config['store_symbol'] 	= "kr ";
			break;
			
			default:
				$this->config['store_symbol'] 	= $this->config['store_currency'];
			break;
		
		}
				
		/*
		*
		*	CHECK GD SUPPORT FOR CARDS AND UPLOADING
		*
		*	These features require gd 2.0.1 Or higher
		*
		*/
		$this->config['gd_version']			= $this->getGDVersion();

		if (version_compare($this->config['gd_version'],'2.0.1')) {
		
			$this->config['gd_popcard']		= (bool)TRUE;
			$this->config['gd_upload']		= (bool)TRUE;
		
		} else {
		
			$this->config['gd_popcard']		= (bool)FALSE;
			$this->config['gd_upload']		= (bool)FALSE;
		
		}
		
		/*
		*
		* 	GET MYSQL SERVER INFORMATION
		*
		*/
		$this->config['mysql_version'] = mysql_get_server_info();
		
		if (version_compare($this->config['mysql_version'],'4.0.0') >= 0) {
			
			$this->config['search_boolean'] = (bool)TRUE;
			
		} else {
		
			$this->config['search_boolean'] = (bool)FALSE;
		
		}
		
				
		/*
		*
		*	DETECT WHERE THE TEMP ITEMS DIRECTORY IS
		*
		*/
		if (substr($this->config['temporary'],0,1) == "/" || substr($this->config['temporary'],1,1) == ":") {
			
			$this->config['temporary']	= $this->config['temporary'];
			
		} else {
		
			$this->config['temporary_original']	= $this->config['temporary'];
			$this->config['temporary']			= BASE_PATH . $this->config['temporary'];
		
		}

		/*
		*
		*	GENERATE A UNIQUE ID
		*
		*/
		$this->config['unique_id'] = strtoupper(substr(md5($_SERVER['SERVER_NAME']),0,6));
		
		/*
		*
		*	PATH TO THE DEFAULT FRONT END TEMPLATE DIRECTORY
		*
		*/
		$this->config['default_template_dir'] = SYS_BASE_PATH."resources/templates_default/";
		
		/*
		*
		*	GET THE PHP MEMORY LIMIT
		*
		*/
		$this->config['memory_limit'] = rtrim(get_cfg_var('memory_limit'),"M");
		
		/*
		*
		*	CHECK THE PHP VERSION
		*
		*/
		$this->config['php_vers'] = PHP_VERSION;
		
		if (version_compare(PHP_VERSION,'5.0.0') >= 0) { define("PHP_GT_5",true); } else { define("PHP_GT_5",false); }
		if (version_compare(PHP_VERSION,'6.0.0') >= 0) { define("PHP_GT_6",true); } else { define("PHP_GT_6",false); }
		
		/*
		*
		*	Tell Pixaria whether we need to obey image visibility settings
		*
		*/
		if (WORKSPACE == "FE") {
			$this->config['image_visibility_sql'] 	= " AND ". PIX_TABLE_IMGS .".image_active = '1' ";
			$this->config['gallery_visibility_sql']	= " AND ". PIX_TABLE_GALL .".gallery_active = '1' ";
		}
		
	}
	
	/*
	*
	*	CONVERT SETTINGS ARRAY INTO CONSTANTS
	*
	*/
	function settingsToConstants () {
	
		while (list($key, $value) = @each($this->config)) {
			
			if (!defined(strtoupper($key)) && !is_array($value)) {
			
				define(strtoupper($key),$value);
			
			} else {
				
				//die ('Error defining constant \'' . strtoupper($key) . '\' on line ' . __LINE__ . "<br />\n" . __FILE__ );
			
			}
		
		}
	
	}
	
	/*
	*	Return the version number of the installed GD Library
	*	
	*	Thanks to: Justin Greer
	*/
	function getGDVersion() {
	
		static $gd_version_number = null;
		
		if ($gd_version_number === null) {
		
			/*	
			*	Use output buffering to get results from phpinfo() 
			*	without disturbing the page we're in.  Output 
			*	buffering is "stackable" so we don't even have to 
			*	worry about previous or encompassing buffering.
			*/
			
			ob_start();
			
			phpinfo(8);
			
			$module_info = ob_get_contents();
			ob_end_clean();
			if (preg_match("/\bgd\s+version\b[^\d\n\r]+?([\d\.]+)/i",
					$module_info,$matches)) {
				$gd_version_number = $matches[1];
			} else {
				$gd_version_number = 0;
			}
		}
		
		return $gd_version_number;
		
	}
	
}

?>