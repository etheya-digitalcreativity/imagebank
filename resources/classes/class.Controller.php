<?php if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	LOAD CLASS FILES ON DEMAND (PHP 5 ONLY)
*
*/
function __autoload ($classname) {

	if (file_exists(RES_PATH . 'classes/class.' . $classname . EXT)) {
	
		require_once (RES_PATH . 'classes/class.' . $classname . EXT);
		
	} elseif (file_exists(RES_PATH . 'models/class.' . $classname . EXT)) {
	
		require_once (RES_PATH . 'models/class.' . $classname . EXT);
		
	}
	
}

/*
*	MAIN APPLICATION CONTROLLER
*	
*	This class provides controller objects with global
*	settings and acess to shared properties and methods.
*
*/
class Controller  {
	
	var	$view;
	var	$form;
	var	$database;
	var $user;
	var $menu;
	var $cfg;
	var $session;
	var $auth;
	var $controller = array();
	
	function Controller () {
		
		// Get settings from database and environment
		$this->setupSettings();
		
		// Setup the localisation system
		$this->setupLocalisation();
		
		// Instantiate the input data object
		$this->input	= new InputData();

		// Instantiate the database object
		$this->db		= new MySQL(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		
		// Create the settings object
		$this->cfg		= new Settings($this->db, $this->input);
		
		// Create the authentication object
		$this->auth		= new Authentication($this->db, $this->input);
		
		// Instantiate the Smarty object for legacy templates
		$this->view		= new SmartyPixaria($this->cfg);
		
		// Set up properties for this controller object
		$this->controllerProperties();
		
		// Assign information about the controller and method to the view object
		$this->view->assign("_controller",'/'.$this->controller['name']);
		$this->view->assign("_method",'/'.$this->controller['method']);
		
		// Start the model object for this controller (if there is one)
		$this->startModel();
		
		// Call the first method to be executed in this controller
		$this->callMethod();
		
	}
	
	/*
	*
	*	GET CONTROLLER NAME, METHOD AND PARAMETERS FROM URL
	*	
	*	Creates an array property containing the name of the currently
	*	initialised controller, the method being called and the parameters
	*	passed to it from the URL.
	*	
	*/
	function controllerProperties () {
		
		if (preg_match("/\?/",$_SERVER['REQUEST_URI'])) {
			list ($path, $query_string) = explode('?',$_SERVER['REQUEST_URI']);
		} else {
			$path = $_SERVER['REQUEST_URI'];
		}
		
		$path = explode ('/',trim($path,'/'));
		
		if (is_array($path)) {
		
			foreach ($path as $key => $value) {
				
				if ($key == 0 && $value != '') {
					$this->controller['name'] = $value;
				} elseif ($key == 0) {
					$this->controller['name'] = DEFAULT_CONTROLLER;
				}
				
				if ($key == 1) {
					$this->controller['method'] = $value;
				}
				
				if ($key > 1) {
					$this->controller['params'][] = $value;
				}
				
			}
			
		}
			
	}
	
	/*
	*
	*	IF THERE IS A MODEL FOR THIS CONTROLLER THEN INITIALISE IT
	*
	*	It's useful to intialise the model for the controller when
	*	the controller object is created.  This method checks that
	*	the model file exists and then loads the model from it. 
	*
	*/
	function startModel () {
		
		$model_path = MODEL_PATH . 'model.' . $this->controller['name'] . EXT;
		
		if (file_exists($model_path)) {
			
			require_once($model_path);
			
			$model_name = 'model_'.$this->controller['name'];
			
			// Pass the user profile object to model
			$this->model = new $model_name();
		
		}
	
	}
	
	/*
	*
	*	PICK THE DEFAULT METHOD TO RUN AS THE CONTROLLER INITIALISES
	*	
	*	When the controller starts up, choose the default method to run.
	*	The order of precedence is:
	*	1) Method name from URL
	*	2) Method called 'index' (for defaulting to a view)
	*	3) Method called 'init' (for defaulting to code handling without a view)
	*
	*/
	function callMethod () {
		
		// Execute method with name derived from URL
		if (method_exists($this, $this->controller['method']) && $this->controller['method'] != $this->controller['name']) {
		
			call_user_func_array(array($this, $this->controller['method']), $this->controller['params']);
		
		// If there is a method name given in the URL but the method doesn't exist, redirect to defualt controller
		} elseif ($this->controller['method'] != '') {
		
			header ('HTTP/1.1 301 Moved Permanently');
			header ('Location: /' . $this->controller['name']);
		
		// Execute default view method of index
		} elseif (method_exists($this, 'index') && $this->controller['name'] != 'index') {
		
			$this->index();
		
		// Execute default non-view method of init
		} elseif (method_exists($this, 'init') && $this->controller['name'] != 'init') {
		
			$this->init();
			
		}
	
	}
	
	/*
	*
	*	SETUP THE INTERNAL SETTINGS
	*
	*/
	function setupSettings () {
		
		// Set up constant for front end activities
		define("PIXGALL","ON");

		// Load constants
		require_once(APP_PATH . 'constants.php');
		
		// Load required classes
		require_once(RES_PATH.'classes/class.Smarty.php');
		require_once(RES_PATH.'classes/class.TemplateEngine.php');
		require_once(RES_PATH.'classes/class.InputData.php');
		require_once(RES_PATH.'classes/class.MySQL.php');
		require_once(RES_PATH.'classes/class.Settings.php');
		require_once(RES_PATH.'classes/class.Authentication.php');
		
		// Work around REQUEST_URI bug under W2K/IIS/CGI/PHP 
		if (!isset($_SERVER['REQUEST_URI']) and isset($_SERVER['SCRIPT_NAME'])) {
		
			$_SERVER['REQUEST_URI'] = $_SERVER['SCRIPT_NAME'];
			
			if (isset($_SERVER['QUERY_STRING']) && !empty($_SERVER['QUERY_STRING'])) {
				$_SERVER['REQUEST_URI'] .= '?' . $_SERVER['QUERY_STRING'];
			}
			
		}
		
		unset($GLOBALS['cfg']);
		
	}
	
	/*
	*
	*	SETUP THE LOCALISATION SYSTEM
	*
	*/
	function setupLocalisation () {
	
		// Include the Pixaria Gallery main language file
		if (file_exists(RES_PATH . "local/".LOCAL_LANGUAGE.".php")) {
			require_once (RES_PATH . "local/".LOCAL_LANGUAGE.".php");
		} else {
			require_once (RES_PATH . "local/en_GB.php");
		}
		
		// Include the e-mail strings file
		if (file_exists(RES_PATH . "local/".LOCAL_LANGUAGE."_email.php")) {
			require_once (RES_PATH . "local/".LOCAL_LANGUAGE."_email.php");
		} else {
			require_once (RES_PATH . "local/en_GB_email.php");
		}
		
		// Move the localised strings into the global scope
		$GLOBALS['_STR_'] = $strings;
		
		// Destroy the $lan variable to free up memory
		unset($strings);
		
	}
	
}

?>