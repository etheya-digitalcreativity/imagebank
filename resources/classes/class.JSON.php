<?php if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

class Pixaria_JSON {

	/*
	*	Convert a PHP array into JSON
	*
	*	http://www.bin-co.com/php/scripts/array2json/
	*
	*/
	function encodeArrayAsJson ($arr) { 
	
		if(function_exists('json_encode')) return json_encode($arr); //Lastest versions of PHP already has this functionality. 
		$parts = array(); 
		$is_list = false; 
	
		//Find out if the given array is a numerical array 
		$keys = array_keys($arr); 
		$max_length = count($arr)-1; 
		if(($keys[0] == 0) and ($keys[$max_length] == $max_length)) {//See if the first key is 0 and last key is length - 1 
			$is_list = true; 
			for($i=0; $i<count($keys); $i++) { //See if each key correspondes to its position 
				if($i != $keys[$i]) { //A key fails at position check. 
					$is_list = false; //It is an associative array. 
					break; 
				} 
			} 
		} 
	
		foreach($arr as $key=>$value) { 
			if(is_array($value)) { //Custom handling for arrays 
				if($is_list) $parts[] = array2json($value); /* :RECURSION: */ 
				else $parts[] = '"' . $key . '":' . array2json($value); /* :RECURSION: */ 
			} else { 
				$str = ''; 
				if(!$is_list) $str = '"' . $key . '":'; 
	
				//Custom handling for multiple data types 
				if(is_numeric($value)) $str .= $value; //Numbers 
				elseif($value === false) $str .= 'false'; //The booleans 
				elseif($value === true) $str .= 'true'; 
				else $str .= '"' . addslashes($value) . '"'; //All other things 
				// :TODO: Is there any more datatype we should be in the lookout for? (Object?) 
	
				$parts[] = $str; 
			} 
		} 
		$json = implode(',',$parts); 
		 
		if($is_list) return '[' . $json . ']';//Return numerical JSON 
		return '{' . $json . '}';//Return associative JSON 
	}
	
	/*
	*	Convert JSON into an array
	*/
	function json_decode ($json) {  
	
	    // Author: walidator.info 2009 
	    $comment = false; 
	    $out = '$x='; 
	    
	    for ($i=0; $i<strlen($json); $i++) { 
	        if (!$comment) 
	        { 
	            if ($json[$i] == '{')        $out .= ' array('; 
	            else if ($json[$i] == '}')    $out .= ')'; 
	            else if ($json[$i] == ':')    $out .= '=>'; 
	            else                         $out .= $json[$i];            
	        } 
	        else $out .= $json[$i]; 
	        if ($json[$i] == '"')    $comment = !$comment; 
	    }
	    
	    eval($out . ';'); 
	    
	    return $x; 
	
	}
	
	/*
	*	Output JSON data to the browser
	*/
	function sendJson ($data) {
		
		header ("Content-type: application/json");
		
		print $data;
		
	}
	
}










?>