<?php if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

class EXIFCore {

	var $meta		= array();
	var $hasmeta	= false;
	var $file		= false;
	
	/*
	*
	*	
	*
	*/
	function EXIFCore ($filename) {
		
		if (function_exists(exif_read_data) && extension_loaded('exif')) {
			
			if (!@exif_read_data($filename, 'IFD0')) {
				
				return false;
			
			} else {
			
				$this->meta		= @exif_read_data($filename, 0, true);
				
				$this->hasmeta	= true;
				
				$this->file		= $filename;
				
			}
				
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function getDateTime ($format='') {
		
		if (is_array($this->meta)) {
			
			$exif_date_dat = explode(" ",$this->meta['EXIF']['DateTimeOriginal']);
				
			$exif_date_ymd	= explode(":",$exif_date_dat[0]);
			$exif_date_hms	= explode(":",$exif_date_dat[1]);
			
			$exif_date_day	= $exif_date_ymd[2];
			$exif_date_mon	= $exif_date_ymd[1];
			$exif_date_yrs	= $exif_date_ymd[0];
			
			if ($exif_date_day != '' && $exif_date_mon != '' && $exif_date_yrs != '') {
			
				$exif_date_sec	= $exif_date_hms[2];
				$exif_date_min	= $exif_date_hms[1];
				$exif_date_hrs	= $exif_date_hms[0];
				
				// Create a Unix style timestamp for the image capture date
				$timestamp = @mktime($exif_date_hrs,$exif_date_min,$exif_date_sec,$exif_date_mon,$exif_date_day,$exif_date_yrs);
				
				if ($format != '') {
					
					return @strftime($format, $timestamp);
				
				} else {
					
					return @strftime('%Y-%m-%d %T', $timestamp);
					
				}
			
			} else {
				
				return false;
			
			}
			
		} else {
			
			return false;
		
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function getGeoTagFractions () {
		
		if (is_array($this->meta)) {
			
			$geotag = $this->meta['GPS'];
			
			$data['gps_latitude_ref'] 	= $geotag['GPSLatitudeRef'];
			$data['gps_latitude_deg'] 	= $geotag['GPSLatitude'][0];
			$data['gps_latitude_min'] 	= $geotag['GPSLatitude'][1];
			$data['gps_latitude_sec'] 	= $geotag['GPSLatitude'][2];
			$data['gps_longitude_ref'] 	= $geotag['GPSLongitudeRef'];
			$data['gps_longitude_deg'] 	= $geotag['GPSLongitude'][0];
			$data['gps_longitude_min'] 	= $geotag['GPSLongitude'][1];
			$data['gps_longitude_sec'] 	= $geotag['GPSLongitude'][2];
			
			return $data;
			
		} else {
			
			return false;
		
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function dump() {
	
		print_r($this->meta);
		
	}
	
	/*
	*
	*	
	*
	*/
	function exifToNumber ($value, $format) {
	
		$spos = strpos($value, '/');
		
		if ($spos === false) {
		
			return sprintf($format, $value);
			
		} else {
		
			list($base,$divider) = split("/", $value, 2);
			
			if ($divider == 0) 
				return sprintf($format, 0);
			else
				return sprintf($format, ($base / $divider));
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function exifToCoordinate ($reference, $coordinate) {
	
		if ($reference == 'S' || $reference == 'W')
			$prefix = '-';
		else
			$prefix = '';
			
		return $prefix . sprintf('%.6F', $this->exifToNumber($coordinate[0], '%.6F') +
			((($this->exifToNumber($coordinate[1], '%.6F') * 60) +	
			($this->exifToNumber($coordinate[2], '%.6F'))) / 3600));
	}
	
	/*
	*
	*	
	*
	*/
	function getGeoTagCoordinates () {
	
		if (is_array($this->meta)) {
		
			$geotag = $this->meta['GPS'];
			
			if (isset($geotag['GPSLatitudeRef']) && isset($geotag['GPSLatitude']) && 
				isset($geotag['GPSLongitudeRef']) && isset($geotag['GPSLongitude'])) {
				return array (
					$this->exifToCoordinate($geotag['GPSLatitudeRef'], $geotag['GPSLatitude']), 
					$this->exifToCoordinate($geotag['GPSLongitudeRef'], $geotag['GPSLongitude'])
				);
				
			} else {
				
				return array();
			
			}
			
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function coordinate2DMS ($coordinate, $pos, $neg) {
	
		$sign = $coordinate >= 0 ? $pos : $neg;
		
		$coordinate = abs($coordinate);
		
		$degree = intval($coordinate);
		
		$coordinate = ($coordinate - $degree) * 60;
		
		$minute = intval($coordinate);
		
		$second = ($coordinate - $minute) * 60;
		
		return sprintf("%s %d&#xB0; %02d&#x2032; %05.2f&#x2033;", $sign, $degree, $minute, $second);
		
	}

}

?>