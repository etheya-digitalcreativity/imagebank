<?php if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*	MAIN APPLICATION MODEL
*	
*	This class provides model objects with global
*	settings and acess to shared properties and methods.
*
*/
class Model  {
	
	var $db;
	
	/*
	*
	* 
	*
	*/
	function Model () {
	
		// Instantiate the database object as the internal property 'database'
		$this->db		= new MySQL(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		
		// Instantiate the input data objext as the internal property 'input'
		$this->input	= new InputData();
		
	}
	
}

?>