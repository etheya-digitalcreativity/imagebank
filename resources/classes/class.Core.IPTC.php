<?php if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

define('IPTC_OBJECT_NAME', '005');
define('IPTC_EDIT_STATUS', '007');
define('IPTC_PRIORITY', '010');
define('IPTC_CATEGORY', '015');
define('IPTC_SUPPLEMENTAL_CATEGORY', '020');
define('IPTC_FIXTURE_IDENTIFIER', '022');
define('IPTC_KEYWORDS', '025');
define('IPTC_RELEASE_DATE', '030');
define('IPTC_RELEASE_TIME', '035');
define('IPTC_SPECIAL_INSTRUCTIONS', '040');
define('IPTC_REFERENCE_SERVICE', '045');
define('IPTC_REFERENCE_DATE', '047');
define('IPTC_REFERENCE_NUMBER', '050');
define('IPTC_CREATED_DATE', '055');
define('IPTC_CREATED_TIME', '060');
define('IPTC_ORIGINATING_PROGRAM', '065');
define('IPTC_PROGRAM_VERSION', '070');
define('IPTC_OBJECT_CYCLE', '075');
define('IPTC_BYLINE', '080');
define('IPTC_BYLINE_TITLE', '085');
define('IPTC_CITY', '090');
define('IPTC_PROVINCE_STATE', '095');
define('IPTC_COUNTRY_CODE', '100');
define('IPTC_COUNTRY', '101');
define('IPTC_ORIGINAL_TRANSMISSION_REFERENCE', 	'103');
define('IPTC_HEADLINE', '105');
define('IPTC_CREDIT', '110');
define('IPTC_SOURCE', '115');
define('IPTC_COPYRIGHT_STRING', '116');
define('IPTC_CAPTION', '120');
define('IPTC_LOCAL_CAPTION', '121');

class IPTCCore {

	var $meta		= array();
	var $hasmeta	= false;
	var $file		= false;
	
	/*
	*
	*	
	*
	*/
	function IPTCCore ($filename) {
		
		if (file_exists($filename)) {
			
			require_once('class.Core.JPEG.php');
			
			$objJPEG = new JPEG();
			
			$headers = $objJPEG->readHeaders($filename);
			
			while ( list($key, $data) = @each($headers) ) {
				
				// Check for APP13 headers
				if ($data['SegType'] == '237' && !$this->hasmeta) {
				
					$this->hasmeta = true;
					$this->meta = iptcparse($data['SegData']);
					
				}
				
				// Check for APP1 headers and locate XMP Metadata
				if ($data['SegType'] == '225' && strncmp ($data['SegData'], "http://ns.adobe.com/xap/1.0/\x00", 29) == 0) {
					
					$xmp = substr($data['SegData'], 29);
					
				}
			
			}
			
			$this->file = $filename;

		} else {
			
			return false;
		
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function set($tag, $data) {
	
		$this->meta ["2#$tag"]= Array( $data );
		$this->hasmeta=true;
		
	}
	
	/*
	*
	*	
	*
	*/
	function getAll () {
		
		while ( (is_array($this->meta)) && (list($code, $value) = each($this->meta)) ) {
		
			$code = str_replace("2#", "", $code);
			
			if( ($code != "000") && ($code != "140") )  {
			
				while(list($index, ) = each($value)) {
				
					$iptc[$code] .= $value[$index].$linefeed;
					
					$linefeed = "\n";
						
				}
					
			}
	
		}
		
		if ($iptc['025'] != "") {
			
			$keywords = explode("\n",trim($iptc['025']));
			
			$iptc['025'] = implode(", ",$keywords);
		
		}
		
		if (is_array($iptc)) {
		
			return $iptc; 
		
		} else {
		
			return false;
		
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function setAll ($data) {
		
		if (is_array($data)) {
		
			foreach ($data as $key => $value) {
				
				$this->meta ['2#'.$key] = array( trim($value) );
			
			}
			
			$this->hasmeta=true;
		
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function get($tag) {
		
		if (isset($this->meta["2#$tag"])) {
		
			$data = $this->meta["2#$tag"];
			
			if (count($data) > 1) {
			
				return $data;
				
			} elseif (count($data) == 1) {
			
				return $data[0];
				
			} else {
			
				return false;
				
			}
			
		} else {
		
			return false;
			
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function getDateTime ($format='') {
		
		$date = $this->get('055');
		$time = $this->get('060');
		
		if ($date != '') {
		
			$iptc_date_yy	= substr($date,0,4);
			$iptc_date_mm	= substr($date,4,2);
			$iptc_date_dd	= substr($date,6,2);
			
			$iptc_time_hrs	= 0;
			$iptc_time_min	= 0;
			$iptc_time_sec	= 0;
			
			// Create a Unix style timestamp for the image capture date
			$timestamp = @mktime($iptc_time_hrs,$iptc_time_min,$iptc_time_sec,$iptc_date_mm,$iptc_date_dd,$iptc_date_yy);
			
			if ($format != '') {
				
				return @strftime($format, $timestamp);
			
			} else {
				
				return @strftime('%Y-%m-%d %T', $timestamp);
				
			}
		
		} else {
			
			return false;
		
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function dump() {
	
		print_r($this->meta);
		
	}
	
	/*
	*
	*	
	*
	*/
	function binary() {
	
		if (is_array($this->meta)) {
			
			$iptc_new = '';
			
			foreach (array_keys($this->meta) as $s) {
			
				$tag = str_replace("2#", "", $s);
				$iptc_new .= $this->iptc_maketag(2, $tag, $this->meta[$s][0]);
				
			} 		
			
			return $iptc_new;
			
		} else {
			
			return false;
		
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function iptc_maketag($rec,$dat,$val) {
	
		$len = strlen($val);
		if ($len < 0x8000) {
		
			return chr(0x1c).chr($rec).chr($dat).
			chr($len >> 8).
			chr($len & 0xff).
				$val;
				
		} else {
	
			return chr(0x1c).chr($rec).chr($dat).
			chr(0x80).chr(0x04).
			chr(($len >> 24) & 0xff).
			chr(($len >> 16) & 0xff).
			chr(($len >> 8 ) & 0xff).
			chr(($len ) & 0xff).
			$val;
			
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function write() {
		
		if (is_writable($this->file) && file_exists($this->file)) {
		
			if(!function_exists('iptcembed')) return false;
			$mode = 0;
			$content = iptcembed($this->binary(), $this->file, $mode);
			$filename = $this->file;
				
			@unlink($filename); #delete if exists
			
			$fp = fopen($filename, "w");
			fwrite($fp, $content);
			fclose($fp);
			
		} else {
			
			return false;
					
		}
		
	}
	
	/*
	*
	*	REMOVE IPTC METADATA
	*
	*	Requires GD
	*
	*/
	function removeAllTags() {
	
		$this->hasmeta=false;
		$this->meta=Array();
		$img = imagecreatefromstring(implode(file($this->file)));
		@unlink($this->file); #delete if exists
		imagejpeg($img,$this->file,100);
		
	}
	
	function readXMP( $data )
	{
			//Cycle through the header segments
			//for( $i = 0; $i < count( $jpeg_header_data ); $i++ )
			//{
					// If we find an APP1 header,
					//if ( strcmp ( $jpeg_header_data[$i]['SegName'], "APP1" ) == 0 )
					//{
							// And if it has the Adobe XMP/RDF label (http://ns.adobe.com/xap/1.0/\x00) ,
							if( strncmp ( $data, "http://ns.adobe.com/xap/1.0/\x00", 29) == 0 )
							{
									// Found a XMP/RDF block
									// Return the XMP text
									$xmp_data = substr ( $data, 29 );
	
									return $xmp_data;
							}
					//}
			//}
			return FALSE;
	}
	
}


?>