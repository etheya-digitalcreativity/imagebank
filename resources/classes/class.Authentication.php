<?php if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

class Authentication {

	// Private variables
	var $db;
	var $input;
	var $settings = array();
	
	/*
	*	
	*	CONSTRUCTOR FOR THE AUTHENTICATION CLASS
	*	
	*	Authentication methods
	*
	*/
	function Authentication ($db, $input) {
		
		global $cfg;
		
		$this->db		=& $db;
		$this->input	=& $input;
		
		$this->getSession($this->input->cookie('psg_userid'));
		
	}
	
	/*
	*
	*	
	*
	*/
	function get ($name) {
		
		return $this->settings[$name];
	
	}
	
	/*
	*
	*	
	*
	*/
	function createSession ($userid, $duration = "") {
	
		global $cfg;	
		
		$now = time();
		
		if ($this->input->name('cookie_persist') == "on" || $this->input->cookie('psg_persist') == "1") {
		
			$duration = $now + 63072000;
			
		} elseif ($duration != "") {
		
			$duration = $now + $duration;
			
		} else {
		
			$duration = null;
		
		}
			
		$cookie_string = md5($userid.$cfg['sys']['encryption_key']) . "|" . $userid;
		
		if ($cookie_string) {
			
			// Bypass microsoft's lousy security management settings in ie 6 and above
			header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');
	
			if ($this->input->name('cookie_persist') == "on" || $this->input->cookie('psg_persist') == "1") {
				
				// Load the user's id into a cookie expiring in 2 years
				setcookie("psg_userid",$cookie_string,$duration,"/");
				
				// Load the user's id into a cookie expiring in 2 years
				setcookie("psg_persist","1",$duration,"/");
				
			} else {
			
				// Load the user's id into a cookie expiring in 20 mins
				setcookie("psg_userid",$cookie_string,$duration,"/");
	
			}
			
		}
		
	}
	
	/*
	*
	* 	This function checks for session cookies and if the user
	*
	*/
	function getSession ($psg_userid) {
	
		if ($psg_userid) {
			
			global $cfg;
			
			// Create array to hold session info
			$ses = array();
			
			$data = explode("|",$psg_userid);
			
			if ($data[0] === md5($data[1].$cfg['sys']['encryption_key'])) {
				
				// Add user id to session
				$this->settings['psg_userid'] = $data[1];
				
				$sql = 	"SELECT account_status,
								group_id,
								active_image_box,
								email_address,
								concat(first_name,' ',family_name) AS name
						
						FROM ".PIX_TABLE_USER."
				
						LEFT JOIN ".PIX_TABLE_GRME."
											
						ON ".PIX_TABLE_GRME.".userid = ".PIX_TABLE_USER.".userid
						
						WHERE ".PIX_TABLE_USER.".userid = '".$this->settings['psg_userid']."'";
						
				
				// Load key user data from user table
				$groups_result	= $this->db->rows($sql);
				
				$sql = 	"SELECT account_status,
								sys_group_id,
								active_image_box
						
						FROM ".PIX_TABLE_USER."
				
						LEFT JOIN ".PIX_TABLE_SGME."
						
						ON ".PIX_TABLE_SGME.".sys_userid = ".PIX_TABLE_USER.".userid
											
						WHERE ".PIX_TABLE_USER.".userid = '".$this->settings['psg_userid']."'";
						
				
				// Load key user data from user table
				$sys_groups_result	= $this->db->rows($sql);
				
				// Load the user's name into the session array
				$this->settings['psg_name'] = trim($groups_result[0]['name']);
				
				// Set the account status for this user
				$this->settings['psg_account_status'] = $groups_result[0]['account_status'];
				
				// Set the active lightbox_id for this user
				$this->settings['psg_lightbox_id'] = $groups_result[0]['active_image_box'];
				
				// If the user has a complete profile with username and password
				if  ($groups_result[0]['email_address'] != "") {
					
					$this->settings['psg_full_user'] = true;
				
				} else {
					
					$this->settings['psg_full_user'] = false;
				
				}
				
				// If the userid is set and the account is active, load an array of the groups this user is in
				if ($psg_userid != "" && $groups_result[0]['account_status'] == 1) {
				
					// Get the user's privileges
					$this->settings['psg_groups_member_of'] = $this->getPermissions($groups_result,"groups");
					
				}
				
				// If the userid is set and the account is active, load an array of the system levelgroups this user is in
				if ($psg_userid != "" && $sys_groups_result[0]['account_status'] == 1) {
					
					// Get the user's privileges
					$this->settings['psg_sys_groups_member_of'] = $this->getPermissions($sys_groups_result,"sys_groups");
					
				}			
				
				// If the system group membership array exists
				if (is_array($this->settings['psg_sys_groups_member_of'])) {
				
					// Check if the user is an administrator
					if ( in_array('1',$this->settings['psg_sys_groups_member_of'],TRUE) ) {
						
						$this->settings['psg_administrator'] = (bool)TRUE;
						
					}
					
					// Check if the user is a global image editor
					if ( in_array('2',$this->settings['psg_sys_groups_member_of'],TRUE) ) {
						
						$this->settings['psg_image_editor'] = (bool)TRUE;
						
					}
					
					// Check if the user is a global image editor
					if ( in_array('3',$this->settings['psg_sys_groups_member_of'],TRUE) ) {
						
						$this->settings['psg_photographer'] = (bool)TRUE;
						
					}
					
					// Check if the user is a global image editor
					if ( in_array('4',$this->settings['psg_sys_groups_member_of'],TRUE) ) {
						
						$this->settings['psg_delete'] = (bool)TRUE;
						
					}
					
					// Check if the user is a global image editor
					if ( in_array('5',$this->settings['psg_sys_groups_member_of'],TRUE) ) {
						
						$this->settings['psg_download'] = (bool)TRUE;
						
					}
					
				}
				
				// Refresh the session timeout
				$this->createSession($this->settings['psg_userid']);
				
				// Output the session information back into the global domain
				return $this->settings;
			
			}
			
		}
	
	}
	
	/*
	*
	*	This function creates a temporary user id if the user is not logged in
	*
	*/
	function createTemporaryUser () {
		
		global $ses, $cfg;
		
		if ($this->settings['psg_userid'] == "" && !$this->settings['psg_full_user']) {
		
			require_once ('class.PixariaUser.php');
			
			$objUser = new PixariaUser();
			
			$objUser->createNewUserTemp();
			
			// Set a new session cookie expiring in 28 days
			$this->createSession($objUser->getUserId(), "2419200");
			
			// Output the session information back into the global domain
			$ses = $this->getSession(md5($objUser->getUserId().$cfg['sys']['encryption_key'])."|".$objUser->getUserId());
			
		}
		
	}
	
	/*
	*
	* 	This function gets the user's permissions and puts them into an array
	*
	*/
	function getPermissions ($group_data, $group_type) {
	
		global $cfg;
		
		if (is_array($group_data)) {
			
			// Initialise the groups array
			$groups = array();
			
			// Loop through each group that this user is a member of
			foreach ($group_data as $key => $value) {
				
				if ($group_type == "groups") { // We're build an array of user groups
				
					// Add the group_id to an array
					$groups[] = $value['group_id'];
			
				} elseif ($group_type == "sys_groups") { // We're building an array of system groups
				
					// Add the sys_group_id to an array
					$groups[] = $value['sys_group_id'];
			
				}
				
			}
			
			// Put the array of groups into the existing $ses array
			return $groups;
			
		}
	
	}
	
	
	/*
	*	
	*	Check that the user has the correct access privileges
	*	If the user doesn't have access, bounce them to the login page
	*
	*/
	function checkPermission ($level) {
		
		global $cfg, $ses;
		
		$redirect_url = $cfg['sys']['base_url'].$cfg['fil']['index_login'] . "?referer=" . urlencode($cfg['sys']['base_url'].ltrim($_SERVER['REQUEST_URI'],"/"));
		
		if ($_SERVER['QUERY_STRING'] != "" && !preg_match("/\?/",$_SERVER['REQUEST_URI'])) { $redirect_url .= urlencode("?" . $_SERVER['QUERY_STRING']); }
		
		switch ($level) {
			
			case "temp":
				
				if (!isset($this->settings['psg_userid'])) {
				
					header ("Location: $redirect_url");
					exit;
					
				}
				
			break;
			
			case "basic": // User is registered but their account is not active to view restricted pages
				
				if (!isset($this->settings['psg_userid']) || $this->settings['psg_name'] == "") {
				
					header ("Location: $redirect_url");
					exit;
					
				}
				
			break;
			
			
			case "registered": // User is registered with an active account
			
				if ($this->settings['psg_account_status'] === "0" && !isset($this->settings['psg_userid'])) {
				
					header ("Location: $redirect_url");
					exit;
					
				}
				
			break;
			
			
			case "image_editor": // User is registered and active with an administrator's account
		
				if (!$this->settings['psg_administrator'] && !$this->settings['psg_image_editor']) {
					
					header ("Location: " . $cfg['sys']['base_url'] . $cfg['fil']['index_account']);
					exit;
					
				}
				
			break;
							
			case "administrator": // User is registered and active with an administrator's account
		
				if (!$this->settings['psg_administrator']) {
					
					header ("Location: $redirect_url");
					exit;
					
				}
				
			break;
							
			case "photographer": // User is not registered and active with an photographer's account
		
				if (!$this->settings['psg_photographer']) {
					
					header ("Location: " . $cfg['sys']['base_url'] . $cfg['fil']['index_account']);
					exit;
					
				}
				
			break;
							
			case "deletor": // User is not registered and active with an photographer's account
		
				if (!$this->settings['psg_deletor']) {
					
					header ("Location: " . $cfg['sys']['base_url'] . $cfg['fil']['index_account']);
					exit;
					
				}
				
			break;
							
		}
	
		return true;
		
	}
	
	/*
	*	
	*	This function generates a short SQL fragment to include in part of the WHERE section
	*	of a SQL statement.  The SQL will prevent MySQL returning images the user isn't authorised to view.
	*
	*/
	function getImagePermissionSql () {
	
		global $ses, $cfg;
		
		if ($this->settings['psg_administrator']) { // User is an admin, there is no need to limit what they can see
		
			$image_access_sql = "";
		
		} elseif (!$this->settings['psg_administrator'] && is_array($this->settings['psg_groups_member_of'])) { // User is registered but is not an admin (place limits on which images they can view)
		
			$image_access_sql = "AND (";
				
			foreach ($this->settings['psg_groups_member_of'] as $key => $value) {
			
				$image_access_sql .= "(".$cfg['sys']['table_imvi'].".group_id = '$value') OR ";
			
			}
			
			$image_access_sql .= "(".$cfg['sys']['table_imgs'].".image_permissions != '12'))";
			
		} else { // User isn't logged in at all, we can only show them images where there are no restrictions
		
			$image_access_sql = "AND ".$cfg['sys']['table_imgs'].".image_permissions = '10'";
		
		}
		
		$cfg['image_access_sql'] = $image_access_sql;
		
	}
	
	/*
	*	
	*	This function generates a short SQL fragment to include in part of the WHERE section
	*	of a SQL statement.  The SQL will prevent MySQL returning images the user isn't authorised to view.
	*
	*/
	function getGalleryPermissionSql () {
	
		global $ses, $cfg;
		
		if ($this->settings['psg_administrator']) { // User is an admin, there is no need to limit what they can see
		
			$gallery_access_sql = "";
		
		} elseif (!$this->settings['psg_administrator'] && is_array($this->settings['psg_groups_member_of'])) { // User is registered but is not an admin (place limits on which galleries they can view)
		
			$gallery_access_sql = "AND (";
				
			foreach ($this->settings['psg_groups_member_of'] as $key => $value) {
			
				$gallery_access_sql .= "(".PIX_TABLE_GVIE.".userid = '$value') OR ";
			
			}
			
			$gallery_access_sql .= "(".PIX_TABLE_GALL.".gallery_permissions != '12'))";
			
		} else { // User isn't logged in at all, we can only show them images where there are no restrictions
			
			$gallery_access_sql = "AND ".PIX_TABLE_GALL.".gallery_permissions = '10'";
		
		}
		
		$cfg['gallery_access_sql'] = $gallery_access_sql;
		
	}
	
	
	/*
	*	
	*	This function stores the time and IP address of a user login
	*
	*/
	function registerLogin ($log_userid,$log_success="",$email="") {
	
		global $cfg;
		
		$email		= addslashes($email);
		
		$log_ip		= $_SERVER['REMOTE_ADDR'];
		$log_host	= addslashes(gethostbyaddr($log_ip));
		
		@mysql_query("INSERT INTO ".PIX_TABLE_ULOG." VALUES ('$log_userid','$log_success','$email','$log_ip',NOW(),'$log_host');");
		
	}
	
}

?>