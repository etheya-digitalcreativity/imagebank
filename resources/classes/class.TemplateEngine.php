<?php if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

/*
*	This class facilitates use of PHP based templates for outputting HTML
*	
*	The method names and actions loosely emulate the more comprehensive
*	templating sytem Smarty.  See http://www.smarty.net/
*	
*/
class TemplateEngine {
	
	var $template_path_default;
	var $template_path_theme;
	var $theme_name;
	var $self;
	var $data 			= array();
	var $cycles 		= array();
	var $tabindex 		= array();
	var $local_strings 	= array();
	var $errors	 		= false;
	var $error_list		= array();
	var $_str;
	
	/*
	*
	*	Constructor
	*
	*/
	function TemplateEngine () {
		
		/*
		*	Load global strings into an internal array
		*/
		$strings = RES_PATH . "languages/".RJ_LANG_CODE.".php";
		if (file_exists($strings)) {
			require_once ($strings);
		}
		
		/*
		*	Load theme strings on top of the internal array
		*/
		$strings = RES_PATH . "themes/".RJ_THEME_DIR."/languages/".RJ_LANG_CODE.".php";
		if (file_exists($strings)) {
			require_once ($strings);
		}
		
		/*
		*	
		*/
		$this->_str = $str;
		
		// Set the internal path to the template directory
		$this->template_path_default 	= RES_PATH . "views/";
		
		// Set the internal path to the template directory
		$this->template_path_theme 		= RES_PATH . "themes/".RJ_THEME_DIR."/views/";
		
		// Define the theme name for use in templates
		$this->theme_name = RJ_THEME_DIR;
		
		// Set up some defaults
		$this->data['page_title'] = APP_NAME;
		
	}
	
	/*
	*
	*	Assign variables to internal data array
	*
	*/
	function assign ($name, $value) {
		
		// The variable names errors and error list are reserved
		if ($name == "errors" || $name == "error_list") {
			return false;
		}
		
		$this->data[$name] = $value;
		
	}
	
	/*
	*	Output a PHP template to the browser
	*	
	*	Variables from the internal data array are loaded
	*	into the local scope for this method at the time
	*	it is called.
	*/
	function display ($template) {
		
		// Load the template file
		echo $this->fetch($template);
		
	}
	
	/*
	*	Returns the parsed contents of a PHP template as a string
	*	
	*	Variables from the internal data array are loaded
	*	into the local scope for this method at the time
	*	it is called.
	*/
	function fetch ($template) {
				
		// If there is data in the assignment array
		if (is_array($this->data)) {
			
			// Loop through the assignment array
			foreach ($this->data as $name => $value) {
				
				// Create a variable in this scope
				${$name} = $value;
				
			}
			
		}
		                                     	
		// Get any errors passed to this object
		$errors 		= $this->errors;
		$error_list 	= $this->error_list;

		// Start output buffering
		ob_start();
		                              	
		if (file_exists($this->template_path_theme . $template)) {
		
			// Load the template file from the current theme
			include ($this->template_path_theme . $template);
		
		} elseif (file_exists($this->template_path_default . $template)) {
		
			// Load the template file from the default template location
			include ($this->template_path_default . $template);
		
		} else {
			
			// If the file is missing, print an error message
			trigger_error("Failed to load template file '$template' because it was not found", E_USER_WARNING);
		
		}
				
		// Get output and load into a string
		$output = ob_get_contents();
		
		// End output buffering and clear the buffer
		ob_end_clean();
		
		$output = $this->multiLanguage($output);
		
		// Return the output
		return $output;
		
	}
	
	function escape ($string, $esc_type = 'html', $char_set = 'ISO-8859-1') {
	
		switch ($esc_type) {
		
			case 'html':
				return htmlspecialchars($string, ENT_QUOTES, $char_set);
	
			case 'htmlall':
				return htmlentities($string, ENT_QUOTES, $char_set);
	
			case 'url':
				return rawurlencode($string);
	
			case 'urlpathinfo':
				return str_replace('%2F','/',rawurlencode($string));
				
			case 'quotes':
				// escape unescaped single quotes
				return preg_replace("%(?<!\\\\)'%", "\\'", $string);
	
			case 'hex':
				// escape every character into hex
				$return = '';
				for ($x=0; $x < strlen($string); $x++) {
					$return .= '%' . bin2hex($string[$x]);
				}
				return $return;
				
			case 'hexentity':
				$return = '';
				for ($x=0; $x < strlen($string); $x++) {
					$return .= '&#x' . bin2hex($string[$x]) . ';';
				}
				return $return;
	
			case 'decentity':
				$return = '';
				for ($x=0; $x < strlen($string); $x++) {
					$return .= '&#' . ord($string[$x]) . ';';
				}
				return $return;
	
			case 'javascript':
				// escape quotes and backslashes, newlines, etc.
				return strtr($string, array('\\'=>'\\\\',"'"=>"\\'",'"'=>'\\"',"\r"=>'\\r',"\n"=>'\\n','</'=>'<\/'));
				
			case 'mail':
				// safe way to display e-mail address on a web page
				return str_replace(array('@', '.'),array(' [AT] ', ' [DOT] '), $string);
				
			case 'nonstd':
			   // escape non-standard chars, such as ms document quotes
			   $_res = '';
			   for($_i = 0, $_len = strlen($string); $_i < $_len; $_i++) {
				   $_ord = ord(substr($string, $_i, 1));
				   // non-standard char, escape it
				   if($_ord >= 126){
					   $_res .= '&#' . $_ord . ';';
				   }
				   else {
					   $_res .= substr($string, $_i, 1);
				   }
			   }
			   return $_res;
	
			default:
				return $string;
		}

	}
	
	/*
	*
	*
	*
	*
	*
	*/
	function error ($errors, $name, $output) {
		
		if ($errors && is_array($errors)) {
			if ($output != '' && in_array($name, $errors)) {
				return $output;
			} else {
				return true;
			}
		} else {
			return false;
		}
		
	}
	
	/*
	*
	* SHOW PHP ERRORS IN A NICE DIALOG BOX
	*
	*/
	function showPhpErrors () {
		
		global $__app_error;
		
		if (count($__app_error) > 0) {
		
			$this->assign("__app_error",$__app_error);
		
			$this->display('plugins/error.html');
		
		}
		
	}
	
	/*
	*
	*	Returns a formatted time based on the input time string
	*	
	*	e.g. dateformat("%b %d %Y %X","2008-04-29 15:55:30")
	*	
	*	Returns the formatted string: April 29 2008 15:55:30
	*
	*/
	function dateformat ($params,$string="") {
		
		// If the date is null, then return false
		if (eregi("^00",$string)) { return false; }
		
		if (trim($string) != "") {
		
			return strftime($params,strtotime($string));
		
		} else {
			
			return strftime($params,time());
		
		}
		
	}
	
	/*
	*
	*	Assigns an error value to the 
	*
	*/
	function add_error ($string) {
		
		if ($string != "") {
			$this->errors		= true;
			$this->error_list[] = $string;
		} else {
			return false;
		}
		
	}
	
	/*
	*
	*	Truncates a string $str to length $len and appends an ellipsis
	*	
	*/
	function truncate ($str, $len) {
		
		if (strlen($str) > $len) {
			// Trim to lenght $len and append ellipsis
			return substr($str,0,$len) . "&#0133;";
		} else {
			return $str;
		}
		
	}
	
	/*
	*
	*	This method returns $item1 and $item2 in turn each time it is called
	*	Multiple 'cycles' can be used provided that $item1 is different in each
	*	instance of use.
	*
	*/
	function cycle ($item1,$item2,$advance=true) {
		
		$cycle_name = md5($item1.$item2);
		
		// There is no array in memory for this cycle
		if ($this->cycles[$cycle_name] < 1) {
			
			$this->cycles[$cycle_name] = 1;

		} elseif ($advance) {
		
			$this->cycles[$cycle_name] = $this->cycles[$cycle_name] + 1;
			
		}
		
		return ( $this->cycles[$cycle_name] % 2 == 0 ) ? $item1 : $item2;
		
	}
	
	/*
	*
	*	
	*
	*/
	function fancySelectSingle ($name, $rows, $options, $values, $default_option, $default_value, $selected, $width, $height) {
		
		$row_height = 30;
		
		if ($default_option != "") {
			
			if ($selected == "") { $selected = $default_value; }
			
			$default_option = array($default_option);
			$default_value	= array($default_value);
			
			$options 		= array_merge($default_option,$options);
			$values 		= array_merge($default_value,$values);
			
		}
		
		if ($selected != "") {
			
			$offset = array_search($selected,$values) * $row_height;
		
		}
		
		if (count($options) < $rows) {
		
			for ($i=0; $i < $rows; $i++) {
				$row[] = $i;
			}
		
		} else {
		
			for ($i=0; $i < count($options); $i++) {
				$row[] = $i;
			}
		
		}
		
		if (is_numeric($rows)) {
			
			$height = $rows * $row_height;
		
		}
		
		$this->assign("fss_name",$name);
		$this->assign("fss_rows",$row);
		$this->assign("fss_width",$width);
		$this->assign("fss_height",$height);
		$this->assign("fss_selected",$selected);
		$this->assign("fss_options",$options);
		$this->assign("fss_values",$values);
		$this->assign("fss_offset",$offset);
		
		$template = "plugins/fancy.select.single.html";
		
		$this->display($template);
		
	}

	/*
	*
	*	
	*
	*/
	function fancySelectMultiple ($name, $rows, $options, $values, $default_option, $default_value, $selected, $width, $height) {
		
		$row_height = 30;
		
		if ($default_option != "") {
			
			if ($selected == "") { $selected = $default_value; }
			
			$default_option = array($default_option);
			$default_value	= array($default_value);
			
			$options 		= array_merge($default_option,$options);
			$values 		= array_merge($default_value,$values);
			
		}
		
		if ($selected != "") {
			
			$offset = array_search($selected,$values) * $row_height;
		
		}
		
		if (count($options) < $rows) {
		
			for ($i=0; $i < $rows; $i++) {
				$row[] = $i;
			}
		
		} else {
		
			for ($i=0; $i < count($options); $i++) {
				$row[] = $i;
			}
		
		}
		
		if (is_numeric($rows)) {
			
			$height = $rows * $row_height;
		
		}
		
		$this->assign("fsm_name",$name);
		$this->assign("fsm_rows",$row);
		$this->assign("fsm_width",$width);
		$this->assign("fsm_height",$height);
		$this->assign("fsm_selected",$selected);
		$this->assign("fsm_options",$options);
		$this->assign("fsm_values",$values);
		$this->assign("fsm_offset",$offset);
		
		$template = "plugins/fancy.select.multiple.html";
		
		$this->display($template);
		
	}
	
	/*
	*
	* ICON PICKER 
	*
	* Takes arguments:
	*
	* 1) Element ID
	* 2) Element Name
	* 3) Default icon (as name of file e.g. 'lock.png')
	*
	*/
	function iconPicker ($id, $name, $default='') {
		
		$template = "plugins/icon.html";
		
		if ($default != '') {
		
			$this->assign('iconpicker_selected',$default);
			$this->assign('iconpicker_text',$default);
			
		} else {
			
			$this->assign('iconpicker_selected','add.png');
			$this->assign('iconpicker_text','Choose an icon');
		
		}
		
		$this->assign("iconpicker_unique",mt_rand());
		$this->assign("iconpicker_id",$id);
		$this->assign("iconpicker_name",$name);

		$this->display($template);
		
		unset($this->data['iconpicker_unique']);
		unset($this->data['iconpicker_id']);
		unset($this->data['iconpicker_name']);
		unset($this->data['iconpicker_selected']);
		
	}
	
	/*
	*
	* YAHOO UI DATE PICKER 
	*
	* Takes arguments:
	*
	* 1) Element ID
	* 2) Element Name
	* 3) Default date (in in any format compatible with strtotime)
	*
	*/
	function datePicker ($id, $name, $default='') {
		
		$template = "plugins/date.html";
		
		if ($default != '') {
		
			if (($timestamp = strtotime($default)) !== false) {
			
				$this->assign("datepicker_date",date("d/m/Y",$timestamp));
				$this->assign("datepicker_selected",date("m/d/Y",$timestamp));
				$this->assign("datepicker_pagedate",date("m/Y",$timestamp));
				
			}
			
		}
		
		$this->assign("datepicker_unique",mt_rand());
		$this->assign("datepicker_id",$id);
		$this->assign("datepicker_name",$name);

		$this->display($template);
		
		unset($this->data['datepicker_unique']);
		unset($this->data['datepicker_id']);
		unset($this->data['datepicker_name']);
		unset($this->data['datepicker_date']);
		unset($this->data['datepicker_selected']);
		unset($this->data['datepicker_pagedate']);
		
	}
	
	/*
	*
	* YAHOO UI DATE RANGE PICKER 
	*
	* Takes arguments:
	*
	* 1) Date 'from' element ID
	* 2) Date 'to' element ID
	* 3) Date 'from' element Name
	* 4) Date 'to' element Name
	* 5) Date 'from' default date (in in any format compatible with strtotime)
	* 6) Date 'to' default date (in in any format compatible with strtotime)
	*
	*/
	function dateRangePicker ($id_from, $id_to, $name_from, $name_to, $default_from='', $default_to='') {
		
		$template = "plugins/date.range.html";
		
		if ($default_from != '') {
		
			if (($timestamp = strtotime($default_from)) !== false) {
			
				$this->assign("datepicker_from_date",date("d/m/Y",$timestamp));
				$this->assign("datepicker_from_selected",date("m/d/Y",$timestamp));
				$this->assign("datepicker_from_pagedate",date("m/Y",$timestamp));
				
			}
			
		}
		
		if ($default_to != '') {
		
			if (($timestamp = strtotime($default_to)) !== false) {
			
				$this->assign("datepicker_to_date",date("d/m/Y",$timestamp));
				$this->assign("datepicker_to_selected",date("m/d/Y",$timestamp));
				$this->assign("datepicker_to_pagedate",date("m/Y",$timestamp));
				
			}
			
		}
		
		$this->assign("datepicker_unique",mt_rand());
		
		$this->assign("datepicker_from_id",$id_from);
		$this->assign("datepicker_to_id",$id_to);
		
		$this->assign("datepicker_from_name",$name_from);
		$this->assign("datepicker_to_name",$name_to);

		$this->display($template);
		
		unset($this->data['datepicker_unique']);
		
		unset($this->data['datepicker_from_id']);
		unset($this->data['datepicker_to_id']);
		
		unset($this->data['datepicker_from_name']);
		unset($this->data['datepicker_to_name']);
		
		unset($this->data['datepicker_from_date']);
		unset($this->data['datepicker_from_selected']);
		unset($this->data['datepicker_from_pagedate']);
		
		unset($this->data['datepicker_to_date']);
		unset($this->data['datepicker_to_selected']);
		unset($this->data['datepicker_to_pagedate']);
		
	}
	
	/*
	*
	* OUTPUT A LIST FROM AN ARRAY
	*
	* Converts values in an array into a formatted HTML list
	* 
	*/
	function listFromArray ($array, $li_class='', $ul_class='') {
		
		if (is_array($array)) {
			
			if ($li_class != '') {
				$li_class = ' class="'.$li_class.'"';
			}
			
			if ($ul_class != '') {
				$ul_class = ' class="'.$ul_class.'"';
			}
			
			foreach ($array as $key => $value) {
				
				$output .= "<li$li_class>$value</li>";
			
			}
			
			$this->assign("list_from_array_data",$output);
			$this->assign("list_from_array_ul_class",$ul_class);
			
			$this->display('plugins/list.html');
		
			unset($this->data['list_from_array_data']);
			unset($this->data['list_from_array_ul_class']);
			
		}
	
	}
	
	/*
	* 
	* 
	* 
	* 
	* 
	*/
	function dataTable ($dt_columns, $dt_data_source, $dt_initial_request) {
		
		$this->assign('dt_columns',$dt_columns);
		$this->assign('dt_data_source',$dt_data_source);
		$this->assign('dt_initial_request',$dt_initial_request);
	
		$this->display('plugins/datatable.html');
		
	}
	
	/*
	*
	*
	*
	*/
	function multiLanguage($tpl_source) {
		
		if (!is_array($this->_str)) {
		  trigger_error ("Error loading Multilanguage Support", E_USER_NOTICE);
		}
		
		// Now replace the matched language strings with the entry in the file
		return preg_replace_callback('/##(.+?)##/', array($this,'compileLanguage'), $tpl_source);
		
	}
	
	/*
	*
	*
	*
	*/
	function compileLanguage($key) {
	
		$data = $key[1];
		
		return ($this->_str[$data]);
		
	}
	
}

?>