<?php if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

class LibraryCore {
	
	function LibraryCore () {
	
		// Create the database object
		$this->db = new Database();
	
	}
	
	/*
	*
	*
	*
	*/
	function checkImageExists ($identifier='', $string='') {
		
		if ($identifier != '' && $string != '') {
		
			$sql = "SELECT image_id FROM ".PIX_TABLE_IMGS."
					
					WHERE ".$this->db->escape($identifier)." = '".$this->db->escape($string)."'";
			
			list ( $image_id ) = $this->db->row($sql);
			
			if ($image_id != '') {
			
				return $image_id;
				
			} else {
			
				return false;
				
			}
			
		} else {
			
			trigger_error("To check if an image exists, you must specify an column and value to check against.", E_USER_WARNING);
			
		}
		
	}
	
}