<?php

class ImageCore_ImageMagick {
	
	var $convert_path;
	var $idenfity_path;
	
	var $temp_file;
	var $count			= 0;
	var $file_history 	= array();
	
	/*
	*
	* CONSTRUCTOR
	*
	* Create a temporary working file and file modification history
	* 
	*/
	function ImageCore_ImageMagick ($image_path) {
		
		global $cfg;
		
		$this->temp_dir = $cfg['set']['temporary'];
		
		@exec('which convert', $output, $return_var);

		if (is_array($output) && count($output) > 0) {

			$this->convert_path = $output[0];
		
		}
		
		$path_info = pathinfo($image_path);
		
		$this->temp_file = time().ereg_replace("[^a-zA-Z0-9_.]", '_', $path_info['filename']);
		
		@copy($image_path, $this->temp_dir.'/tmp'.$this->count.'_'.$this->temp_file);
		
		$this->file_history[] = $this->temp_dir.'/tmp'.$this->count.'_'.$this->temp_file;
		
	}
	
	/*
	*
	* RESIZE AN IMAGE
	*
	* Takes output width and height and optional method which, if set
	* to 'fit' will preserve the aspect ratio and fit to a box with the
	* specified width and height.
	*
	*/
	function resize ($x_size, $y_size, $how='keep_aspect') {
		
		if ($this->verbose == TRUE) {
		
			echo "Resize:\n";
			
		}

		$method = $how=='keep_aspect'?'>':($how=='fit'?'!':'');

		if($this->verbose == TRUE) {
		
			echo "  Resize method: {$how}\n";
			
		}

		$command = "$this->convert_path -geometry '{$x_size}x{$y_size}{$method}' '{$this->temp_dir}/tmp{$this->count}_{$this->temp_file}' '{$this->temp_dir}/tmp".++$this->count."_{$this->temp_file}'";

		if($this->verbose == TRUE) {
		
			echo "  Command: {$command}\n";
			
		}

		exec($command, $returnarray, $returnvalue);

		if($returnvalue) {
		
			$this->error .= "ImageMagick: Resize failed\n";
			
			if($this->verbose == TRUE) {
			
				echo "Resize failed\n";
				
			}
			
		} else {
		
			$this->file_history[] = $this->temp_dir.'/tmp'.$this->count.'_'.$this->temp_file;
			
		}
	
	}
	
	/*
	*
	* SAVE AN IMAGE FILE
	*
	*/
	function save ($targetdir) {
		
		if ($this->verbose == TRUE) {
		
			echo "Save:\n";
			
		}

		if (!@copy($this->temp_dir.'/tmp'.$this->count.'_'.$this->temp_file, $targetdir)) {
		
			$this->error .= "ImageMagick: Couldn't save to {$this->targetdir}/'{$prefix}{$this->temp_file}\n";
			
			if($this->verbose == TRUE) {
			
				echo "Save failed to {$this->targetdir}/{$prefix}{$this->temp_file}\n";
				
			}
			
		} else {
		
			if ($this->verbose == TRUE) {
			
				echo "  Saved to {$this->targetdir}/{$prefix}{$this->temp_file}\n";
				
			}
		}
		return $prefix.$this->temp_file;
	
	}
	
	/*
	*
	* CLEAN UP AND CLEAR MEMORY
	*
	*/
	function clean () {

		if($this->verbose == TRUE) {
		
			echo "Cleanup:\n";
			
		}

		$num = count($this->file_history);

		for($i=0;$i<$num;$i++) {
		
			if(!@unlink($this->file_history[$i])) {
			
				$this->error .= "ImageMagick: Removal of temporary file '{$this->file_history[$i]}' failed\n";
				
				if($this->verbose == TRUE) {
				
					echo "  Removal of temporary file '{$this->file_history[$i]}' failed\n";
					
				}
				
			} else {
			
				if($this->verbose == TRUE) {
				
					echo "  Deleted temp file: {$this->file_history[$i]}\n";
					
				}
				
			}
			
		}

		if ($this->verbose == TRUE) {
		
			echo '</pre>';
			
		}
		
	}
	
}

class ImageCore_GD {
	
	var $im;
	var $src_path;
	
	function ImageCore_GD ($source_path) {
		
		$this->src_path = $source_path;
		
		$this->im = imagecreatefromjpeg($this->src_path);
	
	}
	
	/*
	*
	* RESIZE AN IMAGE
	*
	* Takes output width and height and optional method which, if set
	* to 'fit' will preserve the aspect ratio and fit to a box with the
	* specified width and height.
	*
	*/
	function resize ($width, $height, $method) {
		
		if ($this->im) {
			
			$imageX = @imagesx($this->im);
			$imageY = @imagesy($this->im);
			
			if ($method == 'keep_aspect') {
				
				if ($imageX >= $imageY) {
				
					$thumbX = $width;
					$thumbY = (int)(($thumbX*$imageY) / $imageX );
					
				} else {
				
					$thumbY = $height;
					$thumbX = (int)(($thumbY*$imageX) / $imageY );
					
				}
							
			} elseif ($method == 'fit') {
				
				$thumbX = $width;
				$thumbY = $height;
			
			}
		
			$dest_thum  = @imagecreatetruecolor($thumbX, $thumbY);
			
			@imagecopyresampled ($dest_thum, $this->im, 0, 0, 0, 0, $thumbX, $thumbY, $imageX, $imageY);
						
			@imageinterlace($dest_thum);
			
			@imagedestroy($this->im);
			
			$this->im = $dest_thum;
			
			return TRUE;
		
		} else {
		
			return FALSE;
		
		}
	
	}
	
	/*
	*
	* SAVE AN IMAGE FILE
	*
	*/
	function save ($des_path) {
		
		@imagejpeg($this->im, $des_path, 90);
	
	}
	
	/*
	*
	* CLEAN UP AND CLEAR MEMORY
	*
	*/
	function clean () {
		
		@imagedestroy($this->im);
		
	}
	
}

class ImageCore {

	var $src_path;
	
	/*
	*
	* CONSTRUCTOR
	*
	* Takes image source path and image destination path
	*
	*/
	function ImageCore ($src_path) {
		
		if (!file_exists($src_path)) {
			
			die ("Source file not found");
			
		} else {
			
			$this->src_path = $src_path;
			
		}
		
		switch ($this->detectGraphicsLibrary()) {
			
			case 'GD':
				$this->engine = new ImageCore_GD ($this->src_path);
			break;
			
			case 'IM':
				$this->engine = new ImageCore_ImageMagick ($this->src_path);
			break;
			
			default:
				print 'No image processing engine selected.';
			break;
			
		}
		
	}
	
	/*
	*
	*	RETURN WHETHER A GRAPHICS ENGINE IS PRESENT
	*
	*/
	function isInit () {
	
		if (!$this->engine) {
			return false;
		} else {
			return true;
		}
		
	}

	/*
	*
	* DETECT THE INSTALLED IMAGE LIBRARY
	*
	* ImageMagick is preferred because it performs transforms outside of PHP
	* and is therefore not affected by PHP's memory limit restrictions.
	*
	*/
	function detectGraphicsLibrary () {
		
		if (function_exists('exec') && strtolower(substr(php_uname(), 0, 7)) != 'windows') {
			
			@exec('which convert', $output, $return_var);
			
			if (is_array($output) && count($output) > 0) {

				if ($output[0] != '' && @file_exists($output[0])) {
					
					return 'IM';
					
				}
			
			}
			
		}
		
		$gd_info = $this->getGdVersion();
		
		if (function_exists('imagecreatetruecolor') && (version_compare($gd_info, '2.0') >= 0) ) {
			
			return 'GD';
		
		} else {
			
			return false;
			
		}
		
	}
	
	/*
	*
	* GET THE VERSION NUMBER OF THE GD IMAGE LIBRARY
	*	
	*/
	function getGdVersion() {
	
		static $gd_version_number = null;
		
		if ($gd_version_number === null) {
		
			/*	
			*	Use output buffering to get results from phpinfo() 
			*	without disturbing the page we're in.  Output 
			*	buffering is "stackable" so we don't even have to 
			*	worry about previous or encompassing buffering.
			*/
			
			ob_start();
			
			phpinfo(8);
			
			$module_info = ob_get_contents();
			
			ob_end_clean();
			
			if (preg_match("/\bgd\s+version\b[^\d\n\r]+?([\d\.]+)/i", $module_info, $matches)) {
			
				$gd_version_number = $matches[1];
				
			} else {
			
				$gd_version_number = 0;
				
			}
			
		}
		
		return $gd_version_number;
		
	}
	
	function resize ($x_size, $y_size, $method) {
		
		$this->engine->resize($x_size, $y_size, $method);
	
	}
	
	function save ($des_path) {
	
		$this->engine->save($des_path);
	
	}
	
	function clean () {
	
		$this->engine->clean();
	
	}
	
}

?>