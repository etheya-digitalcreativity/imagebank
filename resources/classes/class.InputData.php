<?php if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*	
*	CLASS TO WORK WTIH FORM DATA AND OTHER HTTP DATA SUCH AS COOKIES
*	
*	Exposes the following methods:
*	
*	public		post()					Return the value of a posted form item named $name
*	public		get()					Return the value of a query string (GET request) form item named $name
*	public		both()					Return the value of any (Get/Post) form item named $name - post data takes precedent
*	public		cookie()				Return the value of a cookie variable named $name
*	public		server()				Return the value of a server variable named $name
*	private		unregisterGlobals()		Clear the super global $GLOBALS array and re-instate safe values
*	
*/

class InputData {
	
	var $allowed_html_tags;
	
	/*
	*	
	*	This is the class constructor for the Form class
	*	
	*	EnvData -- Unregisters global variables and intialises local properties
	*	
	*	class EnvData(void)
	*
	*/
	function __construct () {
		
		// Set the allowed HTML tags which can be received in formdata
		$this->allowed_html_tags = "";
						
	}
	
	/*
	*	Return the value of a posted form item named $name
	*	
	*	string  fetchPost(string key)
	*	
	*	If key matches key in $_POST array, Returns string which is value of key from $_POST array 
	*	
	*	if key does NOT match key in $_POST array, Returns EMPTY STRING.
	*
	*/
	function post ($name, $replace_quotes=true) {
		
		if ($_POST[$name] !== '') { $output = $_POST[$name]; }
		
		if (!isset($output)) {
		
			return false;
		
		} elseif (is_string($output)) {
			
			// If $replace_quotes is true (default) then replace quotes with HTML entity
			if ($replace_quotes) { $output = preg_replace("/\"/","&quot;",$output); }
			
			// Remove poison null bytes
			$output = str_replace(chr(0),"",$output);
			
			// Strip HTML tags
			$output = strip_tags(substr(trim($output),0,10000),$this->allowed_html_tags);
		
			// If magic_quotes is on then strip the slashes
			if (ini_get('magic_quotes_gpc')) { return stripslashes($output); } else { return $output; }
			
		} else {
		
			return $output;
		
		}
		
	}
	
	/*
	*	Return the value of a query string (GET request) form item named $name
	*	
	*	string  fetchGet(string key)
	*	
	*	If key matches key in $_GET array, Returns string which is value of key from $_GET array 
	*	
	*	if key does NOT match key in $_POST array, Returns EMPTY STRING.
	*
	*/
	function get ($name, $replace_quotes=true) {
	
		if ($_GET[$name] !== '') { $output = $_GET[$name]; }
	
		if (!isset($output)) {
		
			return false;
		
		} elseif (is_string($output)) {
		
			// If $replace_quotes is true (default) then replace quotes with HTML entity
			if ($replace_quotes) { $output = preg_replace("/\"/","&quot;",$output); }
			
			// Remove poison null bytes
			$output = str_replace(chr(0),"",$output);
			
			// Strip HTML tags
			$output = strip_tags(substr(trim($output),0,5000),$this->allowed_html_tags);
		
			// If magic_quotes is on then strip the slashes
			if (ini_get('magic_quotes_gpc')) { return stripslashes($output); } else { return $output; }
			
		} else {
		
			return $output;
		
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function raw ($name, $replace_quotes=true) {
		
		if ($_GET[$name] != "") { $output = $_GET[$name]; }
		
		if ($_POST[$name] != "") { $output = $_POST[$name]; }		
		
		if (!isset($output)) {
		
			return false;
		
		} elseif (is_string($output)) {
			
			// Remove poison null bytes
			$output = str_replace(chr(0),"",$output);
			
			// If magic_quotes is on then strip the slashes
			if (ini_get('magic_quotes_gpc')) { return stripslashes($output); } else { return $output; }
		
		} else {
		
			return $output;
		
		}
		
	}
	
	/*
	*	Return the value of any (Get/Post) form item named $name
	*
	*	string  fetchGlobal(string key)
	*	
	*	If key matches key in $_POST or $_GET array, Returns string which is value of key from $_POST or $_GET array.
	*	
	*	if key does NOT match key in $_POST or $_GET array, Returns EMPTY STRING.
	*	
	*	by order of execution, precedence is given to $_POST.
	*
	*/
	function name ($name, $replace_quotes=true) {
	
		if ($_GET[$name] != "") { $output = $_GET[$name]; }
		
		if ($_POST[$name] != "") { $output = $_POST[$name]; }		
		
		if (!isset($output)) {
		
			return false;
		
		} elseif (is_string($output)) {
			
			// If $replace_quotes is true (default) then replace quotes with HTML entity
			if ($replace_quotes) { $output = preg_replace("/\"/","&quot;",$output); }
			
			// Remove poison null bytes
			$output = str_replace(chr(0),"",$output);
			
			// Strip HTML tags
			$output = strip_tags(substr(trim($output),0,10000),$this->allowed_html_tags);
		
			// If magic_quotes is on then strip the slashes
			if (ini_get('magic_quotes_gpc')) { return stripslashes($output); } else { return $output; }
			
		} else {
		
			return $output;
		
		}
		
	}

	/*
	*
	*	Return values based on whether a checkbox is checked or not
	*	
	*	The $if_on and $if_off parameters allow custom return based on
	*	whether the checkbox is ticked or not.  The defaults are 1 and
	*	0 respectively returned as string values NOT boolean.
	*	
	*	This assumes the checkbox has no HTML value attribute set
	*
	*/
	function checkbox ($name, $if_on="", $if_off="") {
	
		$data = $this->name($name);
		
		if ($data == "on") 	{
		
			if ($if_on != "") 	{ return $if_on; } else { return "1"; }
		
		} else {
		
			if ($if_off != "") 	{ return $if_off; } else { return "0"; }
		
		}
		
	}
	
	/*
	*	Return the value of a cookie variable named $name
	*	
	*	string  fetchCookie(string key)
	*	
	*	If key matches key in $_COOKIE array, Returns string which is value of key from $_COOKIE array 
	*	
	*	if key does NOT match key in $_COOKIE array, Returns EMPTY STRING.
	*
	*/
	function cookie ($name, $replace_quotes=true) {
	
		foreach($_COOKIE as $key=>$value){
	
			if ($name == $key) {
			
				if ($value !== '') { $output = $value; }
				
			}
			
		}
	
		if (!isset($output)) {
		
			$output = '';
		
		} elseif (is_string($output)) {
			
			// If $replace_quotes is true (default) then replace quotes with HTML entity
			if ($replace_quotes) { $output = preg_replace("/\"/","&quot;",$output); }
			
			// Remove poison null bytes
			$output = str_replace(chr(0),"",$output);
			
			// Strip HTML tags
			$output = strip_tags(substr(trim($output),0,5000));
		
			// If magic_quotes is on then strip the slashes
			if (ini_get('magic_quotes_gpc')) { return stripslashes($output); } else { return $output; }
		
		} else {
		
			return $output;
		
		}
		
	}
	
	/*
	*
	*	GET HTML FORM DATA AND SANITISE IT BY REMOVING JAVASCRIPT
	*
	*/
	function html ($name) {
		
		// Get default allowed HTML tags
		$original_tags = $this->allowed_html_tags;
		
		// Override the allowed HTML tags which can be received in formdata
		$this->allowed_html_tags = "<div><b><i><a><img><h1><h2><h3><h4><h5><h6><p><ul><ol><li><em><strong><emphasis><blockquote><code><pre>";
		
		// Get form data leaving double quotes intact
		$data = $this->name($name, false);
		
		// Restore original allowed HTML tags
		$this->allowed_html_tags = $original_tags;
		
		// Return cleaned up data
		$output = preg_replace_callback('/^<([a-z])\s.*?>/i',array($this,'removeChars'),$data);
	
		// If magic_quotes is on then strip the slashes
		if (ini_get('magic_quotes_gpc')) { return stripslashes($output); } else { return $output; }
		
	}
	
	/*
	*
	*	REMOVE JAVASCRIPT/SQL DANGEROUS CHARACTERS
	*
	*/
	function removeChars ($string) {
		
		// Strip out the following characters: ' ; ( ) ` { } +
		return preg_replace("/'|;|\(|\)|`|\{|\}|\+/",'',$string[0]);
	
	}
	
	/*
	*	Clear out the entire global scope and reinstate safe values
	*	
	*	null unregisterGlobals(void)
	*	
	*/
	function unregisterGlobals() {
	
	   // Save the existing superglobals first 
	   $REQUEST		= $_REQUEST;
	   $GET			= $_GET;
	   $POST		= $_POST;
	   $COOKIE		= $_COOKIE;
	   $FILES		= $_FILES;
	   $ENV			= $_ENV;
	   $SERVER		= $_SERVER;
	
	   if (isset($_SESSION)) { 
	      $SESSION = $_SESSION;
	   }
	
	   // Unset the $GLOBALS array (clear all)
	   foreach($GLOBALS as $key => $value) {
	
	      if ($key != 'GLOBALS') {
	
	         unset($GLOBALS[$key]);
	
	      }
	
	   }
	
	   // Re-assign the saved superglobals again
	   $_REQUEST	= $REQUEST;
	   $_GET		= $GET;
	   $_POST		= $POST;
	   $_COOKIE		= $COOKIE;
	   $_FILES		= $FILES;
	   $_ENV		= $ENV;
	   $_SERVER		= $SERVER;
	
	   if (isset($SESSION)) {
	      $_SESSION = $SESSION;
	   }
	
	}
	
}

?>