<?php

// Minimal search
$strings['PIXIE_EA1_01']		= "Busca";

// Advanced search
$strings['PIXIE_EA1_02']		= "Busca por imagens";
$strings['PIXIE_EA1_03']		= "Entre palavra-chave para busca";
$strings['PIXIE_EA1_04']		= "Busca por imagens nas galerias";
$strings['PIXIE_EA1_05']		= "Toda a biblioteca de imagens";
$strings['PIXIE_EA1_06']		= "Com autorização de modelo";
$strings['PIXIE_EA1_07']		= "Com autorização de propriedade";
$strings['PIXIE_EA1_08']		= "Tipo de controle de direitos";
$strings['PIXIE_EA1_09']		= "Somente Direitos Controlados (DC)";
$strings['PIXIE_EA1_10']		= "Somente Royalty-free (RF)";
$strings['PIXIE_EA1_11']		= "Todas as imagens";
$strings['PIXIE_EA1_12']		= "Orientação da imagem";
$strings['PIXIE_EA1_13']		= "Qualquer Orientação";
$strings['PIXIE_EA1_14']		= "Paisagem (landscape)";
$strings['PIXIE_EA1_15']		= "Retrato (portrait)";
$strings['PIXIE_EA1_16']		= "Quadrada";
$strings['PIXIE_EA1_17']		= "Busca por imagens"; // Submit button
$strings['PIXIE_EA1_18']		= "Nova busca";
$strings['PIXIE_EA1_19']		= "Buscar entre os resultados";
$strings['PIXIE_EA1_20']		= "Mostrar os resultados em ordem";
$strings['PIXIE_EA1_21']		= "Imagens mais recentes primeiro";
$strings['PIXIE_EA1_22']		= "Nome de arquivo A - Z";
$strings['PIXIE_EA1_23']		= "Nome de arquivo Z - A";
$strings['PIXIE_EA1_24']		= "Título da imagem A - Z";
$strings['PIXIE_EA1_25']		= "Título da imagem Z - A";
$strings['PIXIE_EA1_26']		= "Busca imagens por data";
$strings['PIXIE_EA1_27']		= "Especifique uma data exata ou um período para refinar sua busca.";
$strings['PIXIE_EA1_28']		= "Não busca por data";
$strings['PIXIE_EA1_29']		= "Busca por uma data específica";
$strings['PIXIE_EA1_30']		= "Busca por período";
$strings['PIXIE_EA1_31']		= "Encontrar imagens por cor";
$strings['PIXIE_EA1_32']		= "Habilitar a ferramenta de busca por cores";
$strings['PIXIE_EA1_33']		= "Seleção da paleta de cores";
$strings['PIXIE_EA1_34']		= "Hex";
$strings['PIXIE_EA1_35']		= "RGB";
$strings['PIXIE_EA1_36']		= "Mostrar paleta de cor HUD";
$strings['PIXIE_EA1_37']		= "Show the colour palette and colour selection heads up display.";
$strings['PIXIE_EA1_38']		= "";
$strings['PIXIE_EA1_39']		= "";
$strings['PIXIE_EA1_40']		= "";


?>