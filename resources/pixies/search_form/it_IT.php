<?php

// Minimal search
$strings['PIXIE_EA1_01']		= "Cerca";

// Advanced search
$strings['PIXIE_EA1_02']		= "Cerca immagini";
$strings['PIXIE_EA1_03']		= "Inserisci una parola chiave di ricerca";
$strings['PIXIE_EA1_04']		= "Cerca immagini nelle gallerie";
$strings['PIXIE_EA1_05']		= "Tutta la libreria";
$strings['PIXIE_EA1_06']		= "Con liberatoria model release";
$strings['PIXIE_EA1_07']		= "Con liberatoria property release";
$strings['PIXIE_EA1_08']		= "Tipo di gestione diritti";
$strings['PIXIE_EA1_09']		= "Solo con diritti gestiti";
$strings['PIXIE_EA1_10']		= "Solo royalty free";
$strings['PIXIE_EA1_11']		= "Tutte le immagini";
$strings['PIXIE_EA1_12']		= "Formato immagine";
$strings['PIXIE_EA1_13']		= "Tutti i formati";
$strings['PIXIE_EA1_14']		= "Orizzontale";
$strings['PIXIE_EA1_15']		= "Verticale";
$strings['PIXIE_EA1_16']		= "Quadrato";
$strings['PIXIE_EA1_17']		= "Cerca immagini"; // Tasto invio
$strings['PIXIE_EA1_18']		= "Nuova ricerca";
$strings['PIXIE_EA1_19']		= "Cerca all'interno";
$strings['PIXIE_EA1_20']		= "Mostra risultati in ordine";
$strings['PIXIE_EA1_21']		= "Prima le immagini più recenti";
$strings['PIXIE_EA1_22']		= "Nome file A - Z";
$strings['PIXIE_EA1_23']		= "Nome file Z - A";
$strings['PIXIE_EA1_24']		= "Titolo immagine A - Z";
$strings['PIXIE_EA1_25']		= "Titolo immagine Z - A";
$strings['PIXIE_EA1_26']		= "Cerca immagini secondo la data";
$strings['PIXIE_EA1_27']		= "Specifica una data esatta o un arco di tempo per restringere la ricerca.";
$strings['PIXIE_EA1_28']		= "Non cercare secondo la data";
$strings['PIXIE_EA1_29']		= "Cerca secondo la data esatta";
$strings['PIXIE_EA1_30']		= "Cerca secondo un arco di tempo";
$strings['PIXIE_EA1_31']		= "Trova immagini secondo il colore";
$strings['PIXIE_EA1_32']		= "Abilita lo strumento di ricerca colore";
$strings['PIXIE_EA1_33']		= "Selezione tavolozza colori";
$strings['PIXIE_EA1_34']		= "Hex";
$strings['PIXIE_EA1_35']		= "RGB";
$strings['PIXIE_EA1_36']		= "Mostra tavolozza colori HUD";
$strings['PIXIE_EA1_37']		= "Mostra tavolozza colori and colour selection heads up display.";
$strings['PIXIE_EA1_38']		= "";
$strings['PIXIE_EA1_39']		= "";
$strings['PIXIE_EA1_40']		= "";


?> 