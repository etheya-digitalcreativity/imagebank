{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

{if $EA1_form_type == "minimal"}
	
	<form action="{$base_url}{$fil_index_search}" method="post">
	
	<input type="hidden" name="cmd" value="doSearch" />
	
	<input type="hidden" name="new" value="1" />
	
		<table border="0" cellpadding="3" cellspacing="0" width="280" style="height:30px;">
		<tr>
		<td align="right"><input type="text" value="{$keywords_simple_boolean}" name="keywords_simple_boolean" class="forminput" style="width:150px; font-size:9pt; font-weight:bold;" /></td>
		<td><input type="submit" name="search" value="##PIXIE_EA1_01##" style="text-align:center; width:60px; border:2px outset #888; color:#FFF; background:#888; font-family:Helvetica; font-size:10pt; font-weight:bold;" /></td>
		</tr>
		</table>
	
	</form>
	
{elseif $EA1_form_type == "sidepanel"}
	
	<form action="{$base_url}{$fil_index_search}" method="post" name="search_advanced">
	
	<input type="hidden" name="cmd" value="doSearch" />
	
	<input type="hidden" name="new" value="1" />
	
	<table class="table-search">
	
	<tr>
	<th>##PIXIE_EA1_02##</th>
	</tr>
	
	<tr>
	<td style="padding:4px;">
	
		{if $set_search_opt_keywords}
			
			<span class="bold">##PIXIE_EA1_03##:</span><br />
			<input type="text" value="{$keywords_simple_boolean}" name="keywords_simple_boolean" class="forminput" style="width:245px;" />
			
			<br /><br />
			
		{/if}
	
		{if $set_search_opt_galleries}
		
				<span class="bold">##PIXIE_EA1_04##:</span><br />
				
				<select name="gallery_id" class="forminput" style="width:245px;" size="10">
				<option value="0" {if $EA1_menu_gallery_id[gallery] == ""} selected="selected"{/if}>##PIXIE_EA1_05##</option>
				<option value="0">-------------------------</option>
				{section name=gallery loop=$EA1_menu_gallery_title}
					
					<option value="{$EA1_menu_gallery_id[gallery]}" {if $EA1_menu_gallery_id[gallery]==$gallery_id} selected="selected"{/if}>{$EA1_menu_gallery_title[gallery]}</option>
				
				{/section}
				
				</select>
				
				<br /><br />
		
		{/if}
		
		{if $set_search_opt_model_release}
	
			<input type="checkbox" name="model_release" class="forminput" {if $model_release == "on"}checked="checked"{/if} /> <span class="bold">##PIXIE_EA1_06##</span><br />
			
		{/if}
	
		{if $set_search_opt_property_release}
		
			<input type="checkbox" name="property_release" class="forminput" {if $property_release == "on"}checked="checked"{/if} /> <span class="bold">##PIXIE_EA1_07##</span><br />
			
		{/if}		
		
		{if $set_search_opt_rights_managed}
			
			<br />
			<span class="bold">##PIXIE_EA1_08##:</span><br />
			<input type="radio" name="rights_managed" value="2" class="forminput" {if $rights_managed == "2"}checked="checked"{/if} /> ##PIXIE_EA1_09##<br />
			<input type="radio" name="rights_managed" value="3" class="forminput" {if $rights_managed == "3"}checked="checked"{/if} /> ##PIXIE_EA1_10##<br />
			<input type="radio" name="rights_managed" value="1" class="forminput" {if $rights_managed == "1" || !$rights_managed}checked="checked"{/if} /> ##PIXIE_EA1_11##	
			
		{/if}		
		
		{if $set_search_opt_property_release || $set_search_opt_model_release || $set_search_opt_rights_managed}
		
			<br /><br />
			
		{/if}
		
		{if $set_search_orientation}
		
			<span class="bold">##PIXIE_EA1_12##:</span><br />
			<input type="radio" name="image_orientation" class="forminput" value="ne" {if $image_orientation == "ne" || !$image_orientation}checked="checked"{/if} /> ##PIXIE_EA1_13##<br />
			<input type="radio" name="image_orientation" class="forminput" value="ls" {if $image_orientation == "ls"}checked="checked"{/if} /> ##PIXIE_EA1_14##<br />
			<input type="radio" name="image_orientation" class="forminput" value="po" {if $image_orientation == "po"}checked="checked"{/if} /> ##PIXIE_EA1_15##<br />
			<input type="radio" name="image_orientation" class="forminput" value="sq" {if $image_orientation == "sq"}checked="checked"{/if} /> ##PIXIE_EA1_16##<br />
			
			<br />
		
		{/if}

		<input type="submit" class="formbutton" style="width:245px;" value="##PIXIE_EA1_17##" />
		
	
	</td>
	</tr>
	
	</table>
	
	</form>


{elseif $EA1_form_type == "advanced"}
	
	
	<form action="{$base_url}{$fil_index_search}" method="post" name="search_advanced">
	
	<input type="hidden" name="cmd" value="doSearch" />
	
	<input type="hidden" name="new" value="1" />
	
	<table class="table-search">
	
	<tr>
	<th>##PIXIE_EA1_02##</th>
	</tr>
	
	<tr>
	<td>
	
	<div class="search-box" style="border-right:1px solid #BBB; min-height:255px;">
	
	<input type="radio" name="search_within" value="false" checked="checked" class="forminput" /> <b>##PIXIE_EA1_18##</b>
	
	{if $sid != ""}&nbsp; &nbsp; <input type="radio" name="search_within" value="{$sid}" class="forminput" /> <b>##PIXIE_EA1_19##</b>{/if}
	
	<br /><br />
	
		{if $set_search_opt_keywords}
			
			<span class="bold">##PIXIE_EA1_03##:</span><br />
			<input type="text" value="{$keywords_simple_boolean}" name="keywords_simple_boolean" class="forminput" style="width:100%;" />
			
			<br /><br />
			
		{/if}
	
		{if $set_search_opt_model_release}
	
			<input type="checkbox" name="model_release" class="forminput" {if $model_release == "on"}checked="checked"{/if} /> <span class="bold">##PIXIE_EA1_06##</span><br />
			
		{/if}
	
		{if $set_search_opt_property_release}
		
			<input type="checkbox" name="property_release" class="forminput" {if $property_release == "on"}checked="checked"{/if} /> <span class="bold">##PIXIE_EA1_07##</span><br />
			
		{/if}		
		
		{if $set_search_opt_rights_managed}
		
			<input type="radio" name="rights_managed" value="2" class="forminput" {if $rights_managed == "2"}checked="checked"{/if} /> <span class="bold">##PIXIE_EA1_09##</span><br />
			<input type="radio" name="rights_managed" value="3" class="forminput" {if $rights_managed == "3"}checked="checked"{/if} /> <span class="bold">##PIXIE_EA1_10##</span><br />
			<input type="radio" name="rights_managed" value="1" class="forminput" {if $rights_managed == "1" || !$rights_managed}checked="checked"{/if} /> <span class="bold">##PIXIE_EA1_11##</span>		
			
		{/if}		
		
		{if $set_search_opt_property_release || $set_search_opt_model_release || $set_search_opt_rights_managed}
		
			<br /><br />
			
		{/if}
		
		
		<input type="submit" class="formbutton" style="width:100%;" value="##PIXIE_EA1_17##" />
		
	</div>

	{if $set_search_opt_galleries}
	
		<div class="search-box" style="border-right:1px solid #BBB; min-height:255px;">
		
			<span class="bold">##PIXIE_EA1_04##:</span><br />
			
			<select name="gallery_id" class="forminput" style="width:100%;" size="11">
			<option value="0" {if $EA1_menu_gallery_id[gallery] == ""} selected="selected"{/if}>##PIXIE_EA1_05##</option>
			<option value="0">-------------------------</option>
			{section name=gallery loop=$EA1_menu_gallery_title}
				
				<option value="{$EA1_menu_gallery_id[gallery]}" {if $EA1_menu_gallery_id[gallery]==$gallery_id} selected="selected"{/if}>{$EA1_menu_gallery_title[gallery]}</option>
			
			{/section}
			
			</select>
			
			<br /><br />
		
		<span class="bold">##PIXIE_EA1_20##:</span><br />
		
		<select name="results_order" class="forminput" style="width:100%;">
		<option value="1"{if $results_order == 1} selected="selected"{elseif !$results_order && $set_search_default_order == 1} selected="selected"{/if}>##PIXIE_EA1_21##</option>
		<option value="3"{if $results_order == 3} selected="selected"{elseif !$results_order && $set_search_default_order == 3} selected="selected"{/if}>##PIXIE_EA1_22##</option>
		<option value="4"{if $results_order == 4} selected="selected"{elseif !$results_order && $set_search_default_order == 4} selected="selected"{/if}>##PIXIE_EA1_23##</option>
		<option value="5"{if $results_order == 5} selected="selected"{elseif !$results_order && $set_search_default_order == 5} selected="selected"{/if}>##PIXIE_EA1_24##</option>
		<option value="6"{if $results_order == 6} selected="selected"{elseif !$results_order && $set_search_default_order == 6} selected="selected"{/if}>##PIXIE_EA1_25##</option>
		</select>


		<br /><br />
		
		</div>
	
	{/if}
	
	{if $set_search_opt_date}
		
		<div class="search-box" style="border-right:1px solid #BBB; width:220px; min-height:255px;">
			
			<span class="bold">##PIXIE_EA1_26##</span>
			
			<br /><br />
			
			##PIXIE_EA1_27##
			
			<br /><br />
			
			<input type="radio" name="date_srch" value="10"{if $date_srch==10 || !$date_srch} checked="checked"{/if} />
			
			<span class="bold">##PIXIE_EA1_28##</span>
				
			<br /><br />
			
			<input type="radio" name="date_srch" value="11"{if $date_srch==11} checked="checked"{/if} /> <span class="bold">##PIXIE_EA1_29##:</span><br />
			
			{if $set_date_format == "dmy"}
			
				{html_select_date time=$time field_order="DMY" prefix="date_is_" start_year="1970" reverse_years="TRUE" time=$date_is_time}
				
			{else}
			
				{html_select_date time=$time field_order="MDY" prefix="date_is_" start_year="1970" reverse_years="TRUE" time=$date_is_time}
				
			{/if}
			
			<br /><br />
			
			<input type="radio" name="date_srch" value="12"{if $date_srch==12} checked="checked"{/if} /> <span class="bold">##PIXIE_EA1_30##:</span><br />
			
			
			{if $set_date_format == "dmy"}
			
				{html_select_date time=$time field_order="DMY" prefix="date_st_" start_year="1970" reverse_years="TRUE" time=$date_st_time}
		
			{else}
			
				{html_select_date time=$time field_order="MDY" prefix="date_st_" start_year="1970" reverse_years="TRUE" time=$date_st_time}
		
			{/if}
			
			<br />
		
			{if $set_date_format == "dmy"}
			
				{html_select_date time=$time field_order="DMY" prefix="date_en_" start_year="1970" reverse_years="TRUE" time=$date_en_time}
		
			{else}
			
				{html_select_date time=$time field_order="MDY" prefix="date_en_" start_year="1970" reverse_years="TRUE" time=$date_en_time}
		
			{/if}
		
			<br /><br />
			
		</div>
		
	{/if}

	<div class="search-box" style="min-height:255px;">
	
		{if $set_search_orientation}
		
			<span class="bold">##PIXIE_EA1_12##:</span><br />
			<input type="radio" name="image_orientation" class="forminput" value="ne" {if $image_orientation == "ne" || !$image_orientation}checked="checked"{/if} /> ##PIXIE_EA1_13##<br />
			<input type="radio" name="image_orientation" class="forminput" value="ls" {if $image_orientation == "ls"}checked="checked"{/if} /> ##PIXIE_EA1_14##<br />
			<input type="radio" name="image_orientation" class="forminput" value="po" {if $image_orientation == "po"}checked="checked"{/if} /> ##PIXIE_EA1_15##<br />
			<input type="radio" name="image_orientation" class="forminput" value="sq" {if $image_orientation == "sq"}checked="checked"{/if} /> ##PIXIE_EA1_16##<br />
			
			<br />
		
		{/if}


	{if $set_search_image_color}
	
		<span class="bold">##PIXIE_EA1_31##:</span><br />
		
		<input type="checkbox" name="image_colr_enable" class="forminput" {if $image_colr_enable} checked="checked"{/if} /> ##PIXIE_EA1_32##<br />
			
			<div id="color_picker" style="display:inline;">
			
			{literal}<script language="javascript" type="text/javascript">function openHUD() { Effect.Appear('hud-color',{duration:0.5}); }</script>{/literal}
			
			<br />
			
			<b>##PIXIE_EA1_33##:</b>
			
			<div style="width:190px;">
			
			<div style="float:left; width:30px; height:30px; background:#{$image_colr_hex.3}; margin:5px; cursor:pointer;" id="selected-color-form" onclick="javascript:openHUD();"></div>
			<div style="float:left; width:130px; margin:5px;">
			
				<div id="color-form-hex">##PIXIE_EA1_34##: {$image_colr_hex.3}</div>
				<div id="color-form-rgb">##PIXIE_EA1_35##: {$image_colr_dec.3}</div>
			
			</div>
			
			</div>
			
			<input type="hidden" name="image_colr_r" id="image_colr_r" value="{$image_colr_mod.0}" />
			<input type="hidden" name="image_colr_g" id="image_colr_g" value="{$image_colr_mod.1}" />
			<input type="hidden" name="image_colr_b" id="image_colr_b" value="{$image_colr_mod.2}" />
				
			<br clear="all" />
			
			<a href="javascript:openHUD();" title="##PIXIE_EA1_37##">##PIXIE_EA1_36##</a>
			
			</div>
		
	{/if}
	
	</div>
	
	
	</td>
	</tr>
	
	</table>
	
	</form>
	
	{if $set_search_image_color}{include_template file="index.search/image.hud.color.palette.tpl"}{/if}

{/if}

