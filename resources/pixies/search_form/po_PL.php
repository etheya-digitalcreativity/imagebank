<?php

// Minimal search
$strings['PIXIE_EA1_01']		= "Szukaj";

// Advanced search
$strings['PIXIE_EA1_02']		= "Szukaj zdjęć";
$strings['PIXIE_EA1_03']		= "Szukaj słów kluczowych";
$strings['PIXIE_EA1_04']		= "Szukaj zdjęć w galeriach";
$strings['PIXIE_EA1_05']		= "Cała biblioteka";
$strings['PIXIE_EA1_06']		= "Z upoważnieniem modela/ki";
$strings['PIXIE_EA1_07']		= "Z upoważnieniem nieruchomości";
$strings['PIXIE_EA1_08']		= "Typ praw własności";
$strings['PIXIE_EA1_09']		= "Tylko z prawami własności";
$strings['PIXIE_EA1_10']		= "Z wolną licencją";
$strings['PIXIE_EA1_11']		= "Wszystkie zdjęcia";
$strings['PIXIE_EA1_12']		= "Orientacja obrazu";
$strings['PIXIE_EA1_13']		= "Dowolna orientacja";
$strings['PIXIE_EA1_14']		= "Poziomo";
$strings['PIXIE_EA1_15']		= "Pionowo";
$strings['PIXIE_EA1_16']		= "Kwadrat";
$strings['PIXIE_EA1_17']		= "Szukaj zdjęć"; // Submit button
$strings['PIXIE_EA1_18']		= "Nowe szukanie";
$strings['PIXIE_EA1_19']		= "Szukaj wewnątrz";
$strings['PIXIE_EA1_20']		= "Uporządkuj wyniki wg";
$strings['PIXIE_EA1_21']		= "Najczęściej oglądane pierwsze";
$strings['PIXIE_EA1_22']		= "Nazwa pliku A - Z";
$strings['PIXIE_EA1_23']		= "Nazwa pliku Z - A";
$strings['PIXIE_EA1_24']		= "Tytuł zdjęcia A - Z";
$strings['PIXIE_EA1_25']		= "Tytuł zdjęcia Z - A";
$strings['PIXIE_EA1_26']		= "Szukaj zdjęć wg daty";
$strings['PIXIE_EA1_27']		= "Określ zakres daty lub konretny dzień, aby przeszukać wyniki poszukiwania.";
$strings['PIXIE_EA1_28']		= "Nie szukaj wg daty";
$strings['PIXIE_EA1_29']		= "Szukaj na konkretny dzień";
$strings['PIXIE_EA1_30']		= "Szukaj z danego okresu";
$strings['PIXIE_EA1_31']		= "Szukaj zdjęć wg przeważającego koloru";
$strings['PIXIE_EA1_32']		= "Włącz narzędzie poszukiwania wg koloru";
$strings['PIXIE_EA1_33']		= "Paleta wyboru koloru";
$strings['PIXIE_EA1_34']		= "Hex";
$strings['PIXIE_EA1_35']		= "RGB";
$strings['PIXIE_EA1_36']		= "Pokaż interfejs palety koloru";
$strings['PIXIE_EA1_37']		= "Pokaż nagłowki palety koloru i wyboru koloru.";
$strings['PIXIE_EA1_38']		= "";
$strings['PIXIE_EA1_39']		= "";
$strings['PIXIE_EA1_40']		= "";


?>