<?php

// Minimal search
$strings['PIXIE_EA1_01']		= "Search";

// Advanced search
$strings['PIXIE_EA1_02']		= "Search for images";
$strings['PIXIE_EA1_03']		= "Enter a keyword search";
$strings['PIXIE_EA1_04']		= "Search for images in galleries";
$strings['PIXIE_EA1_05']		= "Entire library";
$strings['PIXIE_EA1_06']		= "With model release";
$strings['PIXIE_EA1_07']		= "With property release";
$strings['PIXIE_EA1_08']		= "Rights management type";
$strings['PIXIE_EA1_09']		= "Rights managed only";
$strings['PIXIE_EA1_10']		= "Royalty free only";
$strings['PIXIE_EA1_11']		= "All images";
$strings['PIXIE_EA1_12']		= "Image orientation";
$strings['PIXIE_EA1_13']		= "Any orientation";
$strings['PIXIE_EA1_14']		= "Landscape";
$strings['PIXIE_EA1_15']		= "Portrait";
$strings['PIXIE_EA1_16']		= "Square";
$strings['PIXIE_EA1_17']		= "Search for images"; // Submit button
$strings['PIXIE_EA1_18']		= "New Search";
$strings['PIXIE_EA1_19']		= "Search within";
$strings['PIXIE_EA1_20']		= "Display results in order";
$strings['PIXIE_EA1_21']		= "Most recent images first";
$strings['PIXIE_EA1_22']		= "File name A - Z";
$strings['PIXIE_EA1_23']		= "File name Z - A";
$strings['PIXIE_EA1_24']		= "Image Title A - Z";
$strings['PIXIE_EA1_25']		= "Image Title Z - A";
$strings['PIXIE_EA1_26']		= "Search for images by date";
$strings['PIXIE_EA1_27']		= "Specify an exact date or range of dates to refine your image search.";
$strings['PIXIE_EA1_28']		= "Don't search by date";
$strings['PIXIE_EA1_29']		= "Search by exact date";
$strings['PIXIE_EA1_30']		= "Search by date range";
$strings['PIXIE_EA1_31']		= "Find images by colour";
$strings['PIXIE_EA1_32']		= "Enable color search tool";
$strings['PIXIE_EA1_33']		= "Colour palette selection";
$strings['PIXIE_EA1_34']		= "Hex";
$strings['PIXIE_EA1_35']		= "RGB";
$strings['PIXIE_EA1_36']		= "Show colour palette HUD";
$strings['PIXIE_EA1_37']		= "Show the colour palette and colour selection heads up display.";
$strings['PIXIE_EA1_38']		= "";
$strings['PIXIE_EA1_39']		= "";
$strings['PIXIE_EA1_40']		= "";


?>