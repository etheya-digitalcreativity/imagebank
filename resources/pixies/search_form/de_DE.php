<?php

// Minimal search
$strings['PIXIE_EA1_01']		= "Suche";

// Advanced search
$strings['PIXIE_EA1_02']		= "Suche Bilder";
$strings['PIXIE_EA1_03']		= "Geben Sie ein Stichwort ein";
$strings['PIXIE_EA1_04']		= "Suche Bilder in Galerien";
$strings['PIXIE_EA1_05']		= "Gesamte Bibliothek";
$strings['PIXIE_EA1_06']		= "Mit Model Release";
$strings['PIXIE_EA1_07']		= "Mit Property Release";
$strings['PIXIE_EA1_08']		= "Rights management Typ";
$strings['PIXIE_EA1_09']		= "Nur Rights managed";
$strings['PIXIE_EA1_10']		= "Nur Royalty free";
$strings['PIXIE_EA1_11']		= "Alle Bilder";
$strings['PIXIE_EA1_12']		= "Bildausrichtung";
// MZ
//$strings['PIXIE_EA1_13']		= "alle";
//$strings['PIXIE_EA1_14']		= "querformatige";
//$strings['PIXIE_EA1_15']		= "hochformatige";
//$strings['PIXIE_EA1_16']		= "quadratische";

$strings['PIXIE_EA1_13']		= "Alle";
$strings['PIXIE_EA1_14']		= "Querformat";
$strings['PIXIE_EA1_15']		= "Hochformat";
$strings['PIXIE_EA1_16']		= "Quadratisch";
$strings['PIXIE_EA1_17']		= "Suche Bilder"; // Submit button
$strings['PIXIE_EA1_18']		= "Neue Suche";
$strings['PIXIE_EA1_19']		= "Suche innerhalb";
$strings['PIXIE_EA1_20']		= "Zeige Ergebnisreihenfolge";
$strings['PIXIE_EA1_21']		= "Neueste Bilder zuerst";
$strings['PIXIE_EA1_22']		= "Dateiname A - Z";
$strings['PIXIE_EA1_23']		= "Dateiname Z - A";
$strings['PIXIE_EA1_24']		= "Bildtitel A - Z";
$strings['PIXIE_EA1_25']		= "Bildtitel Z - A";
$strings['PIXIE_EA1_26']		= "Suche Bilder mit Datum";
$strings['PIXIE_EA1_27']		= "Geben Sie ein bestimmtes Datum oder einen Zeitraum für die Suche an.";
$strings['PIXIE_EA1_28']		= "Keine Datumseingrenzung";
$strings['PIXIE_EA1_29']		= "Bilder mit Datum";
$strings['PIXIE_EA1_30']		= "Bilder aus Zeitraum";
$strings['PIXIE_EA1_31']		= "Finde Bilder mit Farben";
$strings['PIXIE_EA1_32']		= "Aktiviere Farbsuche";
$strings['PIXIE_EA1_33']		= "Farbpalettenauswahl";
$strings['PIXIE_EA1_34']		= "Hex";
$strings['PIXIE_EA1_35']		= "RGB";
//$strings['PIXIE_EA1_36']		= "Zeige Farbpalette HUD";
$strings['PIXIE_EA1_36']		= "Farbe aus Farbpalette wählen";
$strings['PIXIE_EA1_37']		= "Zeige Farbpalette und Farbpalettenauswahl.";
$strings['PIXIE_EA1_38']		= "";
$strings['PIXIE_EA1_39']		= "";
$strings['PIXIE_EA1_40']		= "";


?>