<?php

// Minimal search
$strings['PIXIE_EA1_01']		= "Zoeken";

// Advanced search
$strings['PIXIE_EA1_02']		= "Zoeken naar beelden";
$strings['PIXIE_EA1_03']		= "Voer een sleutelwoord in";
$strings['PIXIE_EA1_04']		= "Zoeken naar beelden in albums";
$strings['PIXIE_EA1_05']		= "Gehele bibliotheek";
$strings['PIXIE_EA1_06']		= "Met model release";
$strings['PIXIE_EA1_07']		= "Met eigendom release";
$strings['PIXIE_EA1_08']		= "Rights management type";
$strings['PIXIE_EA1_09']		= "Rights managed only";
$strings['PIXIE_EA1_10']		= "Royalty free only";
$strings['PIXIE_EA1_11']		= "Alle beelden";
$strings['PIXIE_EA1_12']		= "Beeld orientatie";
$strings['PIXIE_EA1_13']		= "Elke orientatie";
$strings['PIXIE_EA1_14']		= "Liggend beeld";
$strings['PIXIE_EA1_15']		= "Staand beeld";
$strings['PIXIE_EA1_16']		= "Vierkant beeld";
$strings['PIXIE_EA1_17']		= "Zoeken naar beelden"; // Submit button
$strings['PIXIE_EA1_18']		= "Nieuwe zoekopdracht";
$strings['PIXIE_EA1_19']		= "Zoeken tussen";
$strings['PIXIE_EA1_20']		= "Toon resultaat in volgorde";
$strings['PIXIE_EA1_21']		= "Meest recente beelden eerst";
$strings['PIXIE_EA1_22']		= "Filenaam A - Z";
$strings['PIXIE_EA1_23']		= "Filenaam Z - A";
$strings['PIXIE_EA1_24']		= "Beeld titel A - Z";
$strings['PIXIE_EA1_25']		= "Beeld titel Z - A";
$strings['PIXIE_EA1_26']		= "Zoeken naar beelden op datum";
$strings['PIXIE_EA1_27']		= "Specificeer een exacte datum of een periode waarin u wilt zoeken.";
$strings['PIXIE_EA1_28']		= "Niet op datum zoeken";
$strings['PIXIE_EA1_29']		= "Zoeken op exacte datum";
$strings['PIXIE_EA1_30']		= "Zoeken binnen periode";
$strings['PIXIE_EA1_31']		= "Vind beelden op kleur";
$strings['PIXIE_EA1_32']		= "Op kleur zoeken toestaan";
$strings['PIXIE_EA1_33']		= "Kleuren palet selectie";
$strings['PIXIE_EA1_34']		= "Hex";
$strings['PIXIE_EA1_35']		= "RGB";
$strings['PIXIE_EA1_36']		= "Toon kleuren palet HUD";
$strings['PIXIE_EA1_37']		= "Toon kleuren palet en geselecteerde kleuren.";
$strings['PIXIE_EA1_38']		= "";
$strings['PIXIE_EA1_39']		= "";
$strings['PIXIE_EA1_40']		= "";


?>