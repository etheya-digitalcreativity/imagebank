<?php

// Minimal search
$strings['PIXIE_EA1_01']		= "Search";

// Advanced search
$strings['PIXIE_EA1_02']		= "Buscar im&aacute;genes";
$strings['PIXIE_EA1_03']		= "Buscar por palabras";
$strings['PIXIE_EA1_04']		= "Buscar imagenes en galerias";
$strings['PIXIE_EA1_05']		= "En toda la libreria";
$strings['PIXIE_EA1_06']		= "Por modelo";
$strings['PIXIE_EA1_07']		= "Por propiedad";
$strings['PIXIE_EA1_08']		= "Por tipo derechos";
$strings['PIXIE_EA1_09']		= "Por derecho de uso";
$strings['PIXIE_EA1_10']		= "Por libre uso";
$strings['PIXIE_EA1_11']		= "Todas las imagenes";
$strings['PIXIE_EA1_12']		= "Orientación de la imagen";
$strings['PIXIE_EA1_13']		= "Cualquier orientacion";
$strings['PIXIE_EA1_14']		= "Paisaje";
$strings['PIXIE_EA1_15']		= "Retrato";
$strings['PIXIE_EA1_16']		= "Cuadrada";
$strings['PIXIE_EA1_17']		= "Buscar imágenes"; // Submit button
$strings['PIXIE_EA1_18']		= "Nueva búsqueda";
$strings['PIXIE_EA1_19']		= "Search within";
$strings['PIXIE_EA1_20']		= "Ordenar resultados";
$strings['PIXIE_EA1_21']		= "Ordenar por imagenes recientes";
$strings['PIXIE_EA1_22']		= "Nombre de archivo A - Z";
$strings['PIXIE_EA1_23']		= "Nombre de archivo Z - A";
$strings['PIXIE_EA1_24']		= "Titulo imagen A - Z";
$strings['PIXIE_EA1_25']		= "Titulo imagen Z - A";
$strings['PIXIE_EA1_26']		= "Buscar por fecha";
$strings['PIXIE_EA1_27']		= "Especifica una fecha o rango de fechas para delimitar mejor la búsqueda.";
$strings['PIXIE_EA1_28']		= "No buscar por fecha";
$strings['PIXIE_EA1_29']		= "Buscar fecha exacta";
$strings['PIXIE_EA1_30']		= "Buscar rango de fechas";
$strings['PIXIE_EA1_31']		= "Buscar por color";
$strings['PIXIE_EA1_32']		= "Activar herramienta de color";
$strings['PIXIE_EA1_33']		= "Selección por paleta de colores";
$strings['PIXIE_EA1_34']		= "Hex";
$strings['PIXIE_EA1_35']		= "RGB";
$strings['PIXIE_EA1_36']		= "Mostrar paleta de colores HUD";
$strings['PIXIE_EA1_37']		= "Show the colour palette and colour selection heads up display.";
$strings['PIXIE_EA1_38']		= "";
$strings['PIXIE_EA1_39']		= "";
$strings['PIXIE_EA1_40']		= "";


?>