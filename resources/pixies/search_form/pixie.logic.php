<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	This Pixie loads and displays the number of images in the library
*
*	It takes the parameters:
*
*	$pixie_params[0]	(BOOL)		If set to true then this Pixie will respect image access rights and permissions
*
*/

// Load language file for this Pixie
if (file_exists($pixie_dir.LOCAL_LANGUAGE.".php")) {
	require_once ($pixie_dir.LOCAL_LANGUAGE.".php");
} else {
	require_once ($pixie_dir."en_GB.php");
}

// Load language data into global array
if (is_array($strings)) {
	foreach ($strings as $key => $value) {
		$GLOBALS['_STR_'][$key] = $value;
	}
}

if ($cfg['set']['func_search']) {

	if (!$pixie_params[0] || $pixie_params[0] == "minimal") {
	
		// Tell Smarty to show a minimal form
		$smarty->assign("EA1_form_type","minimal");
	
	} elseif ($pixie_params[0] == "sidepanel") {
	
		// Load an array of nested galleries
		$gallery_list = galleryListingArray();
		
		// Load the category list data into Smarty
		$smarty->assign("EA1_menu_gallery_title",$gallery_list[0]);
		$smarty->assign("EA1_menu_gallery_id",$gallery_list[1]);
	
		// Tell Smarty to show an advanced search form
		$smarty->assign("EA1_form_type","sidepanel");
	
	} else {
	
		// Load an array of nested galleries
		$gallery_list = galleryListingArray();
		
		// Load the category list data into Smarty
		$smarty->assign("EA1_menu_gallery_title",$gallery_list[0]);
		$smarty->assign("EA1_menu_gallery_id",$gallery_list[1]);
	
		// Tell Smarty to show an advanced search form
		$smarty->assign("EA1_form_type","advanced");
	
	}
	
	// Pass data back into Smarty
	$pixie_return = $smarty->fetch($pixie_dir . "search_form.tpl");

}

?>