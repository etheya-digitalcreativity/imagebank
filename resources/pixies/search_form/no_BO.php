<?php

// Minimal search
$strings['PIXIE_EA1_01']		= "Søk";

// Advanced search
$strings['PIXIE_EA1_02']		= "Søk etter bilder";
$strings['PIXIE_EA1_03']		= "Skriv et stikkordsøk";
$strings['PIXIE_EA1_04']		= "Søk etter bilder i galleriene";
$strings['PIXIE_EA1_05']		= "Hele arkivet";
$strings['PIXIE_EA1_06']		= "Med modellkontrakt ";
$strings['PIXIE_EA1_07']		= "Med eierkontrakt";
$strings['PIXIE_EA1_08']		= "Rights management type";
$strings['PIXIE_EA1_09']		= "Rights managed only";
$strings['PIXIE_EA1_10']		= "Bare Royalty free bilder";
$strings['PIXIE_EA1_11']		= "Alle bilder";
$strings['PIXIE_EA1_12']		= "Bilderetning";
$strings['PIXIE_EA1_13']		= "Alle bilderetninger";
$strings['PIXIE_EA1_14']		= "Landskap";
$strings['PIXIE_EA1_15']		= "Portrett";
$strings['PIXIE_EA1_16']		= "Firkantet";
$strings['PIXIE_EA1_17']		= "Søk etter bilder"; // Submit button
$strings['PIXIE_EA1_18']		= "Nytt søk";
$strings['PIXIE_EA1_19']		= "Søk innenfor";
$strings['PIXIE_EA1_20']		= "Vis resultatet i rekkefølgen";
$strings['PIXIE_EA1_21']		= "Nyeste bilder først";
$strings['PIXIE_EA1_22']		= "Filnavn A - Å";
$strings['PIXIE_EA1_23']		= "Filnavn Å - Å";
$strings['PIXIE_EA1_24']		= "Bildetittel A - Å";
$strings['PIXIE_EA1_25']		= "Bildetittel Å - A";
$strings['PIXIE_EA1_26']		= "Søk etter bilder på dato";
$strings['PIXIE_EA1_27']		= "Spesifiser et spesielt tidspunkt eller et tidspunkt mellom to datoer.";
$strings['PIXIE_EA1_28']		= "Ikke søk på dato";
$strings['PIXIE_EA1_29']		= "Søk på spesifisert dato";
$strings['PIXIE_EA1_30']		= "Søk på et tidspunkt mellom to datoer";
$strings['PIXIE_EA1_31']		= "Finn bilder ved å benytte fargesøk";
$strings['PIXIE_EA1_32']		= "Slå på fargesøk";
$strings['PIXIE_EA1_33']		= "Fargepalettvalg";
$strings['PIXIE_EA1_34']		= "Hex";
$strings['PIXIE_EA1_35']		= "RGB";
$strings['PIXIE_EA1_36']		= "Vis fargepalett HUD";
$strings['PIXIE_EA1_37']		= "Vis fargepalett og fargevalg-verktøy i et eget sprettoppvindu.";
$strings['PIXIE_EA1_38']		= "";
$strings['PIXIE_EA1_39']		= "";
$strings['PIXIE_EA1_40']		= "";


?>