<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

// Load language file for this Pixie
if (file_exists($pixie_dir.LOCAL_LANGUAGE.".php")) {
	require_once ($pixie_dir.LOCAL_LANGUAGE.".php");
} else {
	require_once ($pixie_dir."en_GB.php");
}

// Load language data into global array
if (is_array($strings)) {
	foreach ($strings as $key => $value) {
		$GLOBALS['_STR_'][$key] = $value;
	}
}

// Pass data back into Smarty
$pixie_return = $smarty->pixFetch($pixie_dir . "facebook.tpl");

?>