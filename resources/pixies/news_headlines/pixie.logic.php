<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	This Pixie loads and displays a random image from the library
*
*	It takes the parameters:
*
*	$pixie_params[0]	(INT)		Number of news articles to get from the database
*	$pixie_params[1]	(INT)		Number of items to show in detail before switching to side-bar list
*
*/

// Load language file for this Pixie
if (file_exists($pixie_dir.LOCAL_LANGUAGE.".php")) {
	require_once ($pixie_dir.LOCAL_LANGUAGE.".php");
} else {
	require_once ($pixie_dir."en_GB.php");
}

// Load language data into global array
if (is_array($strings)) {
	foreach ($strings as $key => $value) {
		$GLOBALS['_STR_'][$key] = $value;
	}
}

// Get the parameters for this plugin
if (!is_numeric($pixie_params[0])) { $pixie_params[0] = 40; }
if (!is_numeric($pixie_params[1])) { $pixie_params[1] = 5; }

// Load Markdown library
require_once("functions.Markdown.php");
		
//	Get list of news items from the blog table
if ($cfg['set']['func_news'] == 1) {
	
	$sql = "SELECT ".$cfg['sys']['table_blog'].".*,
			
			".$cfg['sys']['table_imgs'].".image_path,
			
			".$cfg['sys']['table_imgs'].".image_filename
			
			FROM ".$cfg['sys']['table_blog']."
	
			LEFT JOIN ".$cfg['sys']['table_gall']." ON ".$cfg['sys']['table_blog'].".blog_gallery_id = ".$cfg['sys']['table_gall'].".gallery_id
			
			LEFT JOIN ".$cfg['sys']['table_imgs']." ON ".$cfg['sys']['table_gall'].".gallery_key = ".$cfg['sys']['table_imgs'].".image_id
						
			ORDER BY blog_date DESC LIMIT 0," . $pixie_params[0];
	
	$result = sql_select_rows($sql);
	
	if (is_array($result)) {
	
		foreach($result as $key => $value) {
		
			$blog_item_id[]		= $value[blog_item_id];
			$blog_date[]		= $value[blog_date];
			$blog_title[]		= stripslashes($value[blog_title]);
			$blog_content[]		= strip_tags(markdown(stripslashes($value[blog_content])));
			$blog_gallery_id[]	= $value[blog_gallery_id];
		
			if ($cfg['set']['func_mod_rewrite']) {
				$blog_url[] = $cfg['sys']['base_url'] . "news/" . $value['blog_item_id'];
			} else {
				$blog_url[] = $cfg['sys']['base_url'] . $cfg['fil']['index_news'] . "?id=" . $value['blog_item_id'];
			}
			
			$image_small_uri	= $cfg['sys']['base_library'].$value['image_path']."/80x80/".$value['image_filename'];
			$image_large_uri	= $cfg['sys']['base_library'].$value['image_path']."/160x160/".$value['image_filename'];
			
			if (file_exists($image_small_uri)) {
			
				$image_small_size[]	= getimagesize($image_small_uri);
				$image_small_path[]	= base64_encode($image_small_uri);
		
				$image_large_size[]	= getimagesize($image_large_uri);
				$image_large_path[]	= base64_encode($image_large_uri);
		
			} else {
			
				$image_small_size[] 	= 0;
				$image_small_path[] 	= 0;
		
				$image_large_size[] 	= 0;
				$image_large_path[] 	= 0;
		
			}
		
		}
	
	}
	
	$smarty->assign("blog_item_id",$blog_item_id);
	$smarty->assign("blog_date",$blog_date);
	$smarty->assign("blog_title",$blog_title);
	$smarty->assign("blog_content",$blog_content);
	$smarty->assign("blog_gallery_id",$blog_gallery_id);
	$smarty->assign("blog_image_small_dims",$image_small_size);
	$smarty->assign("blog_image_small_path",$image_small_path);
	$smarty->assign("blog_image_large_dims",$image_large_size);
	$smarty->assign("blog_image_large_path",$image_large_path);
	$smarty->assign("blog_main_items",$pixie_params[1]);
	$smarty->assign("blog_url",$blog_url);

	// Pass data back into Smarty
	$pixie_return = $smarty->pixFetch($pixie_dir . "news_headlines.tpl");
	
} else {

	// Pass data back into Smarty
	$pixie_return = (bool)false;
	
}

?>