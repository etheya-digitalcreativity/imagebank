{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}


<table class="table-news">

<thead>

<tr>

<th colspan="2">{$set_site_name} ##PIXIE_AD4_01##</th>

</tr>

</thead>

<tbody>

<tr>

<th style="vertical-align:top;">

	{section name="blog" loop=$blog_item_id}
		
		{if $smarty.section.blog.index < $blog_main_items}
		
		<h1><a href="{$blog_url[blog]}" class="plain">{$blog_title[blog]}</a></h1>
		
		<h3>##PIXIE_AD4_02## {$blog_date[blog]|date_format:"%B %e, %Y"}</h3>
					
		{if $blog_image_small_path[blog] != "0"}<div style="width:100px; text-align:center; float:left;"><a href="{$blog_url[blog]}"><img src="{$base_url}{$fil_psg_image_thumbnail}?file={$blog_image_small_path[blog]}" {$blog_image_small_dims[blog].3} class="image" style="margin:0px 5px 5px 0px;" alt="{$blog_title[blog]}" border="0" /></a></div>{/if}
		
		{$blog_content[blog]|truncate:400:"..."}
		
		<br clear="all" />
		
		<a href="{$blog_url[blog]}" class="promote">##PIXIE_AD4_03##</a>
		
		<br clear="all" />
		
		<hr />
		
		{/if}
		
	{/section}
	
</th>

<td style="width:350px; vertical-align:top;">
		
		<h1>##PIXIE_AD4_04##</h1>
		
		<br />
		
		{section name="blog" loop=$blog_item_id}
			
			{if $smarty.section.blog.index >= $blog_main_items}
			
			<h3><a href="{$blog_url[blog]}" class="plain">{$blog_title[blog]}</a></h3>{$blog_date[blog]|date_format:"%B %e, %Y"}<br /><br />
			
			{/if}
			
		{/section}
		
		{if $set_func_rss == "1"}
		
		<a href="{$base_url}{$fil_index_rss}" class="link-rss">##PIXIE_AD4_05##</a>
		
		{/if}


</td>

</tr>

</tbody>

</table>

