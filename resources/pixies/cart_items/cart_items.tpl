{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/
*}

{if $cart_items==0}
<div class="pix-cart-items">##PIXIE_F8C_01##: <a href="{$base_url}{$fil_index_cart}" title="##PIXIE_F8C_03##" style="color:#FFF;">##PIXIE_F8C_02##</a></div>
{else}
<div class="pix-cart-items">##PIXIE_F8C_01##: <a href="{$base_url}{$fil_index_cart}" title="##PIXIE_F8C_03##" style="color:#FFF;">{$cart_items}</a></div>
{/if}