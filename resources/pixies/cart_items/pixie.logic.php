<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	This Pixie loads and displays the number of items in the cart
*
*	It takes the parameters:
*
*	$pixie_params[0]	(BOOL)		If true then the cart message will be shown if the cart is empty
*
*/

// Load language file for this Pixie
if (file_exists($pixie_dir.LOCAL_LANGUAGE.".php")) {
	require_once ($pixie_dir.LOCAL_LANGUAGE.".php");
} else {
	require_once ($pixie_dir."en_GB.php");
}

// Load language data into global array
if (is_array($strings)) {
	foreach ($strings as $key => $value) {
		$GLOBALS['_STR_'][$key] = $value;
	}
}

if ($cfg['set']['func_store']) {

	// Load the database class
	require_once ('class.Database.php');
	
	// Create the database object
	$_dbl = new Database();
	
	$sql = "SELECT COUNT(".PIX_TABLE_CRME.".id) FROM ".PIX_TABLE_CRME."
			
			LEFT JOIN ".PIX_TABLE_CART." ON ".PIX_TABLE_CRME.".cart_id = ".PIX_TABLE_CART.".cart_id
			
			LEFT JOIN ".PIX_TABLE_USER." ON ".PIX_TABLE_CRME.".userid = ".PIX_TABLE_USER.".userid
			
			WHERE ".PIX_TABLE_CART.".status = '1'
			
			AND ".PIX_TABLE_CRME.".userid = '" . $ses['psg_userid'] . "' AND ".PIX_TABLE_USER.".email_address != ''";
	
	$cart_items = $_dbl->sqlCountRows($sql);
	
	$smarty->assign("cart_items",$cart_items);
	
	if ($cart_items==0 && $pixie_params[0]==true) {
		
		// Pass data back into Smarty
		$pixie_return = $smarty->pixFetch($pixie_dir . "cart_items.tpl");
	
	} elseif ($cart_items > 0) {
	
		// Pass data back into Smarty
		$pixie_return = $smarty->pixFetch($pixie_dir . "cart_items.tpl");
	
	}
	
}

?>