<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	This Pixie loads and displays the number of images in the library
*
*	It takes the parameters:
*
*	$pixie_params[0]	(BOOL)		If set to true then this Pixie will respect image access rights and permissions
*
*/
// Initialise this page
$objIndexGallery = new IndexGallery();

// Pass the data back to the Pixie
$pixie_return = $objIndexGallery->viewGallery($pixie_dir);


/*
*	Class constructor for the group
*/
class IndexGallery {
	
	var $gallery_id;
	var $gallery_page;
	var $thumbnail_page;
	var $gallery_image;
	var $gallery_title;
	var $gallery_description;
	var $gallery_key_image;
	var $gallery_child_data;
	var $gallery_image_list;
	var $gallery_navigation;
	var $ipages;
	var $gpages;
	var $gallery_url;
	var $gallery_password;
	var $gallery_items;
	var $gallery_thumb_pages;
	
	/*
	*	Class constructor for viewing a gallery
	*/
	function IndexGallery () {
		
		// Load globals
		global $objEnvData, $cfg, $ses, $smarty;
		
		// Load the gallery class
		require_once ('class.PixariaGallery.php');
		
		// Load the global gallery and image group properties for page navigation
		$this->gallery_id 		= $objEnvData->fetchGlobal('gid'); // The gallery ID number
		$this->gallery_page		= $objEnvData->fetchGlobal('gpg'); // The gallery thumbnail icon page we are currently on
		$this->thumbnail_page	= $objEnvData->fetchGlobal('ipg'); // The image thumbnail icon page we are currently on
		$this->gallery_image	= $objEnvData->fetchGlobal('img'); // The number of a specific image we want to view in the gallery
		
		// Initialise the gallery object
		$objGallery = new PixariaGallery(	$this->gallery_id,
											$this->gallery_page,
											$this->thumbnail_page,
											$this->gallery_image);
		
		// Get information about this gallery
		$this->gallery_title		= $objGallery->getGalleryTitle();
		$this->gallery_description	= $objGallery->getGalleryDescription();
		$this->gallery_key_image	= $objGallery->getGalleryKeyImageData();
		$this->gallery_child_data	= $objGallery->getGalleryChildData();
		$this->gallery_image_list	= $objGallery->getGalleryMultiImageData();
		$this->gallery_navigation	= $objGallery->getMultiImageNavigation();
		$this->ipages				= $objGallery->getMultiImagePageNavigation();
		$this->gpages				= $objGallery->getGalleryPageNavigation();
		$this->gallery_url			= $objGallery->getGalleryURL();
		$this->gallery_password		= $objGallery->getGalleryPassword();
		$this->gallery_image_data	= $objGallery->getGalleryImageData();
			
	}
	
	/*
	*	View a gallery
	*/
	function viewGallery ($pixie_dir) {
		
		// Load globals
		global $objEnvData, $cfg, $smarty;

		// Load variables into Smarty
		$smarty->assign("nav_title",$this->gallery_navigation[0]);
		$smarty->assign("nav_href",$this->gallery_navigation[1]);
		$smarty->assign("galleries",$this->gallery_child_data);
		$smarty->assign("gallery_avatar",$this->gallery_key_image);
		$smarty->assign("gallery_title",$this->gallery_title);
		$smarty->assign("gallery_description",$this->gallery_description);
		$smarty->assign("gallery_id",$this->gallery_id);
		$smarty->assign("gallery_url",$this->gallery_url);
		$smarty->assign("gallery_thumb_pages",$this->gallery_thumb_pages);
		$smarty->assign("ipage_current",$this->ipages[0]);
		$smarty->assign("ipage_numbers",$this->ipages[1]);
		$smarty->assign("ipage_links",$this->ipages[2]);
		$smarty->assign("gpage_current",$this->gpages[0]);
		$smarty->assign("gpage_numbers",$this->gpages[1]);
		$smarty->assign("gpage_links",$this->gpages[2]);
		$smarty->assign("view_mode","gallery");
		
		// Display the gallery page
		return $smarty->pixFetch($pixie_dir . "gallery_listing.tpl");
		
	}

}

?>