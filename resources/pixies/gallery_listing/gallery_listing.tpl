{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

{*

/*
*
*	For consistency, this plug-in uses the default gallery thumbnail view as set out
*	in the theme you have selected in the appearance control panel.
*
*/

*}


<!-- INCLUDE TEMPLATE FOR DISPLAYING IMAGE THUMBNAILS -->
{include_template file="index.gallery/gallery.thumbnails.tpl"}

