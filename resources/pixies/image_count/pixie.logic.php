<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	This Pixie loads and displays the number of images in the library
*
*	It takes the parameters:
*
*	$pixie_params[0]	(BOOL)		If set to true then this Pixie will respect image access rights and permissions
*
*/

if (!$pixie_params[0]) {

	// Get SQL to load number of images
	$sql = 	"SELECT COUNT('image_id') FROM ".$cfg['sys']['table_imgs']."";

} else {

	// Get SQL to load number of images
	$sql = 	"SELECT COUNT('image_id') FROM ".$cfg['sys']['table_imgs']." ".$cfg['set']['image_access_sql'];

}

// Get gallery information from the database
$count = sql_count($sql);

if (is_numeric($count)) {
	
	// Tell Smarty how the gallery links should work
	$smarty->assign("C3A_image_count",$count);
	
	// Pass data back into Smarty
	$pixie_return = smartyPixFetch($pixie_dir . "image_count.tpl");
	
} else {

	// Tell the template there was nothing to display
	$pixie_return = (bool)FALSE;

}


?>