<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Prevent this script being executed outside of the Pixaria scope
if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	This Pixie loads and displays a random image from the library
*
*	It takes the parameters:
*
*	$pixie_params[0]	(BOOL)		If set to true then this Pixie will respect image access rights and permissions
*	$pixie_params[1]	(STRING)	The aspect ratio of the required image: portrait, landscape, square
*	$pixie_params[2]	(STRING)	The size of the image to output: icon, small, large or comp
*	$pixie_params[3]	(INT)		The unique ID (gallery_id) of a gallery to show an image from
*
*/

// Get the right kind of image depending on the selected aspect ratio
switch ($pixie_params[1]) {

	case portrait: // Portrait image
	
		$aspect_sql = "AND image_height / image_width > 1";

	break;
	
	case landscape: // Landscape image
	
		$aspect_sql = "AND image_height / image_width < 1";

	break;
	
	case square: // Square image
	
		$aspect_sql = "AND image_height / image_width  = 1";

	break;
	
	default: // None specified
	
		$aspect_sql = "";

	break;
	
}

// Tell Smarty what size of image to show
switch ($pixie_params[2]) {

	case icon:
	
		$smarty->assign("pixie_random_image_size","icon");
	
	break;

	case small:
	
		$smarty->assign("pixie_random_image_size","small");
	
	break;

	case large:
	
		$smarty->assign("pixie_random_image_size","large");
	
	break;

	default:
	
		$smarty->assign("pixie_random_image_size","comp");
	
	break;

}

if ($pixie_params[3] != "") {

	$gallery_sql = "AND gallery_id = '".$pixie_params[3]."'";

}

if (!$pixie_params[0]) {

	// Get SQL to load an image
	$sql = 	"SELECT ".PIX_TABLE_IMGS.".image_id FROM ".PIX_TABLE_IMGS."
			
			LEFT JOIN ".PIX_TABLE_GORD." ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_GORD.".image_id
			
			WHERE ".PIX_TABLE_IMGS.".image_id != ''
			
			$aspect_sql
			
			$gallery_sql
			
			ORDER BY RAND()
			
			LIMIT 0,1";
	
} else {
	
	// Get SQL to load an image
	$sql = 	"SELECT ".PIX_TABLE_IMGS.".image_id FROM ".PIX_TABLE_IMGS."
			
			LEFT JOIN ".PIX_TABLE_GORD." ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_GORD.".image_id
			
			LEFT JOIN `".PIX_TABLE_IMVI."` ON ".PIX_TABLE_IMGS.".image_id = ".PIX_TABLE_IMVI.".image_id
			
			WHERE ".PIX_TABLE_IMGS.".image_id != ''
			
			$aspect_sql
			
			$gallery_sql
			
			".$cfg['set']['image_access_sql']."
			
			ORDER BY RAND()
			
			LIMIT 0,1";
	
}

// Get information about the image
$result = sql_select_row($sql);

if (is_array($result)) {
	
	require_once('class.PixariaImage.php');
	
	$objPixariaImage = new PixariaImage($result['image_id']);
	
	// Load information about this image into Smarty
	$smarty->assign("random_image", $objPixariaImage->getImageDataComplete());
	
	// Pass data back into Smarty
	$pixie_return = $smarty->pixFetch($pixie_dir . "random_image.tpl");
	
} else {

	// Tell the template there was nothing to display
	$pixie_return = (bool)FALSE;

}


?>