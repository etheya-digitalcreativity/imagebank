{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}


{if $pixie_random_image_size == "icon"}
	
	<img src="{$random_image.files.icon_url}" border="0" {$AD4_img_icon_dimensions.2} title="{$AD4_img_title}" alt="{$AD4_img_title}" class="image" />

{elseif $pixie_random_image_size == "small"}

	<img src="{$random_image.files.small_url}" border="0" {$AD4_img_small_dimensions.2} title="{$AD4_img_title}" alt="{$AD4_img_title}" class="image" />

{elseif $pixie_random_image_size == "large"}

	<img src="{$random_image.files.large_url}" border="0" {$AD4_img_large_dimensions.2} title="{$AD4_img_title}" alt="{$AD4_img_title}" class="image" />

{else $pixie_random_image_size == "comp"}
	
	{if $set_image_output_format=="11" }
		
		<table class="image" align="center" cellpadding="0" cellspacing="0"><tr><td style="padding:0px; margin:0px;"><object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" {$random_image.files.comp_size.2} id="image">
		<param name="allowScriptAccess" value="sameDomain" />
		<param name="movie" value="{$random_image.files.comp_url}" />
		<param name="quality" value="high" />
		<param name="bgcolor" value="#FFFFFF" />
		<embed src="{$image.files.comp_url}" quality="high" bgcolor="#FFFFFF" {$random_image.files.comp_size.2} name="image" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
		</object></td></tr></table>
		
	{elseif $set_image_output_format=="12" }
		

		<table class="image" align="center" cellpadding="0" cellspacing="0"><tr><td style="padding:0px; margin:0px;"><embed src="{$random_image.files.comp_url}"
		type="image/svg+xml" pluginspage="http://www.adobe.com/svg/viewer/install/" {$random_image.files.comp_size.2}></td></tr></table>
		
	{elseif $set_image_output_format=="10" }
		
		<img src="{$random_image.files.comp_url}" border="0" {$image.files.comp_size.2} title="{$random_image.basic.title}" alt="{$random_image.basic.title}" class="image" />
		
	{/if}
	
{/if}
