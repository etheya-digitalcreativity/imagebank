{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}



<div id="admin-outerdiv">

<div id="admin-innerdiv">







<table class="table-text">

<tr>
<th>You are now viewing details of the images downloaded by {$username} during {$month}.</th>
<td>

<h1>Downloaded images</h1>

{if $transaction == "pay"}

	<p>This page lists all of the purchased images downloaded by the user {$username} during {$month}.</p>
	
{else}

	<p>This page lists all of the images downloaded using the free download service by the user {$username} during {$month}.  Free image download settings can be modified from within the '<a href="{$base_url}{$fil_admin_settings}" style="text-decoration:underline;">General Settings</a>' control panel.</p>
	
{/if}

<br />

<ul class="actionlist">
<li><a href="{$base_url}{$fil_admin_user_edit}?userid={$userid}">View user profile</a></li>
<li><a href="mailto:{$email}">Send e-mail</a></li>
</ul>

<br />

</td>
<td>&nbsp;</td>
</tr>

</table>





<table class="table-listbox">

<tr>
	<th colspan="2">File name</th>	
	<th>Image title</th>	
	<th>Number of downloads</th>	
	<th>Type of file</th>	
	<th>Default price</th>	
	<th>Actions</th>
</tr>


{section name="images" loop=$icon_path}

	<tr class="{cycle values='list-one,list-two'}">
	
	<td style="width:42px; height:42px; text-align:center;"><a href="javascript:imagePreviewWindow('{$base_url}{$fil_admin_image_output}?html={$comp_path[images]}&amp;','{$comp_width[images]}','{$comp_height[images]}');"><img src="{$base_url}{$fil_admin_image_output}?file={$icon_path[images]}&amp;" border="0" width="{$icon_width[images]}" height="{$icon_height[images]}" title="{$image_filename[images]}" alt="{$image_filename[images]}" class="icon" /></a>
	</td>
	
	<td><a class="cl" href="javascript:imagePreviewWindow('{$base_url}{$fil_admin_image_output}?html={$comp_path[images]}&amp;','{$comp_width[images]}','{$comp_height[images]}');">{$image_filename[images]}</a></td>
	
	<td>{$image_title[images]}</td>
	
	<td>{$download_count[images]}</td>
	
	<td>{$format_type[images]}</td>
	
	<td>{$set_store_symbol}{$image_price[images]}</td>
	
	<td>
	
	<ul class="actionlist">
	<li><a href="{$base_url}{$fil_admin_image}?cmd=edit&amp;image_id={$image_id[images]}">Image editor page</a></li>
	</ul>
	
	</td>
	
	</tr>

{sectionelse}

	<tr class="{cycle values='list-one,list-two'}">
			
	<td colspan="7" style="height:42px;">There are no images to show in this view.</td>
									
	</tr>

{/section}	

</table>




	
</div>

</div>




<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.footer.tpl"}

