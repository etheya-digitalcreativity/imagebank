{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}




<!-- SHOW THE CART CONTENTS FOR THIS USER -->
<div id="admin-outerdiv">

<div id="admin-innerdiv">






	<table class="table-text">
	
	<tr>
	<th>This shopping cart doesn't contain any items to view.</th>
	<td>
	
	<p class="title">The cart is empty</p>
	
	<p>There are no items to show in this user's cart.  This may be because the user has not yet added any items or because they have removed items that were previously in it.</p>
	
	<form method="post" action="{$base_uri}">
	
	<input type="button" onclick="javascript:history.go(-1);" value="Go back to the previous page" class="forminput" style="width:100%;" />
	
	</form>
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	</table>
		
</div>

</div>


<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.footer.tpl"}


