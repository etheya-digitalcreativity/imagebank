{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}



<div id="admin-outerdiv">

<div id="admin-innerdiv">








	<table class="table-text">
	
	<tr>
	<th>From here you can view all the e-commerce transactions in progress on your website.</th>
	<td>
	
	<p class="title">Sales manager homepage</p>
	
	<p>Use this page to find and manage all orders for images placed on your website.</p>
		
	{include_template file="admin.snippets/admin.html.notification.note.tpl" form_title_text="Click the column headers to sort the transaction list and click again to reverse the sort order."}
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	</table>
	
	
<table class="list">
	
	<tr>
		<th>Status</th>
		<th><a href="{$base_url}{$fil_admin_store_sales}&amp;sort=family_name&amp;method={$method}&amp;status={$status}" class="plain">Buyer name</a></th>	
		<th><a href="{$base_url}{$fil_admin_store_sales}&amp;sort=cart_items&amp;method={$method}&amp;status={$status}" class="plain">Items</a></th>	
		<th><a href="{$base_url}{$fil_admin_store_sales}&amp;sort=date_processed&amp;method={$method}&amp;status={$status}" class="plain">Date submitted</a></th>	
		<th><a href="{$base_url}{$fil_admin_store_sales}&amp;sort=date_completed&amp;method={$method}&amp;status={$status}" class="plain">Date completed</a></th>	
		<th>Actions</th>	
	</tr>
	
	{if $carts_present == TRUE}
	
		{section name="carts" loop="$cart_id"}
		
				<tr class="{cycle values='row1,row2'}">
				
				<td style="height:30px;"><div style="background:url('{$base_url}resources/images/icons.browser/cart.{$cart_status_icon[carts]}.gif') no-repeat; padding-left:21px; font-weight:bold; line-height:18px;">{$status_description[carts]}</td>
									
				<td><a href="mailto:{$cart_user_email[carts]}" class="plain" title="Click here to e-mail this user">{$cart_username[carts]}</a></td>
				
				<td style="text-align:center;">{$cart_items[carts]}</td>
									
				<td>{if $date_processed[carts] != "0000-00-00 00:00:00"}{$date_processed[carts]|date_format:"%b %e, %Y %H:%M"}{else}<b>Not submitted</b>{/if}</td>
									
				<td>{if $date_completed[carts] != "0000-00-00 00:00:00"}{$date_completed[carts]|date_format:"%b %e, %Y %H:%M"}{else}<b>Not complete</b>{/if}</td>
									
				<td>
				
				<ul class="actionlist">
				<li><a href="{$cart_view_url[carts]}" title="View details and make changes to this transaction">View details</a></li>
				<li><a href="{$base_url}{$fil_admin_user_edit}?userid={$cart_user_id[carts]}" title="View user's profle">View user profile</a></li>
				<li><a href="javascript:confirmLink('{$base_url}{$fil_admin_store}?cmd=cartDelete&amp;cart_id={$cart_id[carts]}','Permanently delete this transaction and the items associated with it?');" title="Permanently delete this transaction and the items associated with it">Delete cart</a></li>
				{if $cart_status[carts] < 4}<li><a href="javascript:confirmLink('{$base_url}{$fil_admin_store}?cmd=cartClear&amp;cart_id={$cart_id[carts]}','Are you sure that you want to remove the contents of this cart?');" title="Remove all the items in this cart">Empty this cart</a></li>{/if}
				</ul>
				
				 </td>
				
				</tr>
							
		{/section}
	
	{else}
	
		<tr>
		
			<td colspan="6" style="height:30px;">There are no items to view.</td>
		
		</td>
	
	{/if}
			
	</table>
		
	<br />
	
</div>

</div>




<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.footer.tpl"}

