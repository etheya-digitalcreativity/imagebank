{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}



<div id="admin-outerdiv">

<div id="admin-innerdiv">







<table class="table-text">

<tr>
<th>This page allows you to view details of images sold by photographer users on your website.</th>
<td>

<h1>Photographer sales viewer</h1>

<p>To view details of any photographer's completed sales during a given month, select the month you with to view using the dropdown menus and choose the user from the list box.</p>

</td>
<td>&nbsp;</td>
</tr>

</table>




<form action="{$base_url}{$fil_admin_store}" method="post">

<input type="hidden" name="cmd" value="listPhotographerSales" />

<table class="table-search">

<thead>

<tr>
<th>View details of images sold by your photographers:</th>
</tr>

</thead>

<tbody>

<tr>
<td>

<div class="search-box" style="border-right:1px solid #BBB; width:300px; min-height:240px;">

<b>How to view sales by photographers</b>

<p>To view all sales during a given month, select a month using the controls below and a photographer to view from the panel on the right.</p>

<b>Choose a month:</b><br />
{html_select_date display_days=false prefix="date_" start_year="-3" time=$time}<br /><br />

<input type="checkbox" name="show_all" class="forminput"{if $show_all == "on"}checked="checked"{/if} /> <b>Show all sales for selected user</b>

</div>

<div class="search-box" style="border-right:1px solid #BBB; width:530px; min-height:240px;">

	<div style="width:530px; height:190px; background:#FFF; overflow:auto; border:1px solid #AAA;" onselectstart="javascript:return false;">
		
		<table width="100%" style="border-collapse:collapse; border:0px;">
		
		{section name="photographers" loop=$photographer_userid}
		
		<tr style="height:38px; background:#{cycle values='F0F5FA,FFF'};"><td style="width:18px;"><input type="radio" name="userid" value="{$photographer_userid[photographers]}" class="forminput"{if $photographer_userid[photographers] == $userid || ($smarty.section.photographers.index == 0 && $userid == "")} checked="checked"{/if} /><br /><br /></td><td style="padding-left:3px;"><b>{$photographer_name[photographers]}</b><br /><a href="mailto:{$email_address[photographers]}" style="font-size:8pt; color:#888;">{$email_address[photographers]}</a></td><td style="text-align:right; padding-right:10px;"><ul class="actionlist"><li><a href="{$base_url}{$fil_admin_user_edit}?userid={$photographer_userid[photographers]}" style="font-size:9pt;">Edit user profile</a></li></ul></td></tr>
		
		{sectionelse}
		
		<tr><td><b>There are no photographer users to display.</b><br /><br />You can grant any registered user photographer privileges from the user's profile page or groups control panel.</td></tr>
		
		{/section}
				
		</table>
		
	</div>

<br />

<input type="submit" name="submit" value="Show results" class="formbutton" style="width:200px; float:right;" />

</div>

</td>
</tr>

</tbody>

</table>

</form>

{if $sales}

	<table class="table-listbox">
		
	<tr>
		<th>Sales of digital images</th>	
		<th>Sales from physical products</th>
		<th>Total sales</th>	
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
		<td style="height:30px;">{$set_store_symbol}{$total_digital}</td>
		<td>{$set_store_symbol}{$total_physical}</td>
		<td><b>{$set_store_symbol}{$total}</b></td>
	</tr>
	
	</table>
	
{/if}

		
{if $userid != ""}

	<table class="table-listbox">
		
	<tr>
		<th colspan="2">File name</th>	
		<th style="width:100px;">Date</th>
		<th>Price</th>
		<th style="text-align:center;">Quantity</th>
		<th>Item total</th>
		<th>Image title</th>	
		<th>Description</th>
		<th>Actions</th>
	</tr>
		
		{section name="assets" loop=$icon_path}
		
			<tr class="{cycle values='list-one,list-two'}">
			
			<td style="width:44px; height:44px; text-align:center;"><a href="javascript:imagePreviewWindow('{$base_url}{$fil_admin_image_output}?html={$comp_path[assets]}&amp;','{$comp_width[assets]}','{$comp_height[assets]}');"><img src="{$base_url}{$fil_admin_image_output}?file={$icon_path[assets]}&amp;" border="0" width="{$icon_width[assets]}" height="{$icon_height[assets]}" title="{$image_filename[assets]}" alt="{$image_filename[assets]}" class="icon" /></a>
			</td>
			
			<td><a class="cl" href="javascript:imagePreviewWindow('{$base_url}{$fil_admin_image_output}?html={$comp_path[assets]}&amp;','{$comp_width[assets]}','{$comp_height[assets]}');">{$image_filename[assets]}</a></td>
			
			<td>{$date_completed[assets]|date_format}</td>
									
			<td>{$set_store_symbol}{$price[assets]}</td>
									
			<td style="text-align:center;">{$quantity[assets]}</td>
									
			<td>{$set_store_symbol}{$item_total[assets]}</td>
									
			<td>{$image_title[assets]}</td>
									
			<td title="{$quote_text[assets]}">{$quote_text[assets]|truncate:70}</td>
			
			<td>
			
			<ul class="actionlist">
			<li><a href="{$base_url}{$fil_admin_transaction}?cmd=10&amp;cart_id={$cart_id[assets]}">View this entire transaction</a></li>
			</ul>
			
			</td>
											
			</tr>
		
		{sectionelse}	
			
			<tr><td colspan="9" style="height:30px;">This user didn't sell any images during this month</td></tr>
			
		{/section}
		
	</table>

{/if}
		
<br />

</div>

</div>




<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.footer.tpl"}

