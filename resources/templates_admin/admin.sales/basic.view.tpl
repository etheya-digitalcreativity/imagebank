{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}




<div id="admin-outerdiv">

<div id="admin-innerdiv">








	<table class="table-text">
	
	<tr>
	<th>Use this form to create or edit a quote for the items in this user's shopping cart.</th>
	<td>
	
	<p class="title">Quote editor</p>
	
	<p>If you have questions about the order or need more information from the buyer, use the messaging form to contact the buyer directly.</p>
	
		{* /*Start Shane's modified code to add customer details*/ *}
		<div class="notification-note">
		
		<h1>Contact information</h1>
		<p>
		<b>Name & Address:</b><br /><br />
			{$first_name} {$family_name}<br />
			{if $address1 != ""}{$address1}<br />{/if}
			{if $address2 != ""}{$address2}<br />{/if}
			{if $address3 != ""}{$address3}<br />{/if}
			{if $city != ""}{$city}<br />{/if}
			{if $region != ""}{$region}<br />{/if}
			{if $country != ""}{$country}<br />{/if}
			{if $postal_code != ""}{$postal_code}<br />{/if}
		
		</p>
		
		</div>
		{*	/*End modified code*/ *}

	</td>
	<td></td>
	</tr>

	</table>
	
	
	
	<form action="{$base_url}{$fil_admin_transaction}" method="post">
	
	<input type="hidden" name="cart_id" value="{$cart_id}" />
	
	<input type="hidden" name="cmd" value="11" />
	
	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">Cart messages and notes</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<!-- SHOW MESSAGES CONNECTED WITH THIS CART -->
	{if $message_on == TRUE}
	
			{section name="messages" loop="$message_id"}
			
				<tr class="{cycle values='list-one,list-two'}">
				
				<td>&nbsp;</td>
				
				<td valign="top">
				
				<table class="messages">
				<tr>
				<td style="width:100px; font-weight:bold;">Date posted:</td>
				<td>{$message_time[messages]|date_format:"%B %e, %Y %H:%M"}</td>
				</tr>
				<tr>
				<td style="width:100px; font-weight:bold;">Posted by:</td>
				<td><a href="mailto:{$message_user_email[messages]}" class="plain">{$message_username[messages]|nl2br}</a></td>
				</tr>
				<tr>
				<td style="width:100px; font-weight:bold;">Message:</td>
				<td>{$message_text[messages]|nl2br}</td>
				</tr>
				</table>
								
				</td>
				
				<td>&nbsp;</td>
					
				</tr>
								
			{/section}
	
	{/if}
	<!-- END OF MESSAGES CONNECTED WITH THIS CART -->	
	
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>Send a message to the buyer:</th>
	<td><textarea name="message" rows="4" style="width:100%" class="forminput" cols="15"></textarea></td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td><input type="submit" name="submit" value="Send a message to the buyer" style="width:100%;" class="formbutton" /></td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
	</table>
	
	</form>
		
	
	<!-- SHOW ORDER INFORMATION FOR THIS CART -->
	
	<form action="{$base_url}{$fil_admin_transaction}" method="post">
	
	<input type="hidden" name="cart_id" value="{$cart_id}" />
	
	<input type="hidden" name="cmd" value="12" />
	
	<table class="table-form" border="0" cellpadding="3" cellspacing="0">
	
	<thead>
	
	<tr>
	<th colspan="3">Quote details</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Details of your quote request:</th>
	<td>{$cart_usage_text|nl2br}</td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
	</table>
	
	<!-- END OF ORDER INFORMATION FOR THIS CART -->
	
	
	
	
	<table class="table-cart">
	
	<thead>
	
	<tr>
	<th colspan="3">Cart contents</th>
	</tr>
	
	</thead>
	
	<tbody>
	
		{section name="image" loop="$image_id"}
		
				<tr class="{cycle values='list-one,list-two'}">
				
				<th>
				{if $image_comp_path[image] == TRUE}
					<a href="javascript:openPop('{$base_url}{$fil_admin_image_output}?html={$image_comp_path[image]}&amp;','{$image_comp_size[image].0-2}','{$image_comp_size[image].1-2}','no','no','no','no','no','no','no');"><img src="{$base_url}{$fil_psg_image_thumbnail}?file={$image_small_path[image]}" {$image_small_dims[image]} class="thumbnail" alt="{$image_title[image]}" /></a>
				{else}
					<img src="{$base_url}{$fil_psg_image_thumbnail}?file={$image_icon_path[image]}" {$image_icon_dims[image]} class="thumbnail" alt="{$image_title[image]}" />
				{/if}
				
				<br />
				<a href="{$base_url}{$fil_admin_image}?cmd=edit&amp;image_id={$image_id[image]}" style="text-decoration:underline;">Go to edit screen</a>
				</th>
				
				<td>
				
					<table class="cart-item">
					
					<tr>
					<th>Type of product:</th>
					<td class="">{$cart_item_type_text[image]}</td>
					</tr>
					
					{if $image_filename[image]!=""}<tr>
					<th>Filename:</th>
					<td>{$image_filename[image]}
					<input type="hidden" name="cart_item_id[]" value="{$cart_item_id[image]}" />
					<input type="hidden" name="image_id[]" value="{$image_id[image]}" />
					<input type="hidden" name="image_filename[]" value="{$image_filename[image]}" />
					</td>
					</tr>{/if}
					
					<tr>
					<th>Photographer user:</th>
					<td>{if $photographer_details[image].0}<ul class="actionlist"><li><a href="{$base_url}{$fil_admin_store}?cmd=listPhotographerSales&amp;userid={$photographer_userid[image]}">{$photographer_details[image].0} : view sales by this user</a></li></ul>{else}Unable to determine{/if}</td>
					</tr>
				
					<tr>
					<th>Title:</th>
					<td>{if $image_title[image]!=""}{$image_title[image]}{else}N/A{/if}<input type="hidden" name="image_title[]" value="{$image_title[image]}" /></td>
					</tr>
					
					<tr>
					<th>Your requirements:</th>
					<td>{$item_usage_text[image]}</td>
					</tr>
					
					{if $image_comp_path[image] == TRUE}
					
						<tr>
						<th>Enter quote ({$set_store_currency}):</th>
						<td>{if $cart_status == 2}<input type="text" name="quote_price[]" value="{$quote_price[image]}" style="width:80px;" class="forminput" />{else}{$quote_price[image]}{/if}</td>
						</tr>
					
					{else}
					
						<tr>
						<th>Warning:</th>
						<td>This image has been removed from the library and is no longer available.<br />You are advised to remove it from the cart.</td>
						</tr>
					
					{/if}
				
					{if $cart_item_type[image] == "physical"}
	
						<tr>
						<th>Quantity:</th>
						<td>{$cart_item_quantity[image]}</td>
						</tr>
	
					{/if}
					
					{if $cart_status != 4}
					
						<tr>
						<th>Actions:</th>
						<td><a href="{$base_url}{$fil_admin_transaction}?cmd=13&amp;cart_item_id={$cart_item_id[image]}&amp;cart_id={$cart_id}" title="Remove from this cart">Remove from cart</a>
						</td>
						</tr>
					
					{else}
					
						<tr>
						<th>Download attempts:</th>
						<td>{$download_attempts[image]} (<a href="{$base_url}{$fil_admin_transaction}?cmd=20&amp;id={$cart_item_id[image]}&amp;cart_id={$cart_id}" title="Reset download counter" class="plain">Reset download counter</a>)</td>
						</tr>
					
					{/if}
					
					</table>				

				</td>
					
				<td>&nbsp;</td>
				
				</tr>
							
		{/section}
		
		</tbody>
		
	</table>
	
	
	
	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">Invoice details</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Sales tax rate in percent (e.g. 17.5%):</th>
	<td>{if $cart_status == 2}<input type="text" name="sales_tax" value="{$sales_tax}" style="width:100%;" />{else}{$sales_tax}{/if}</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Cost of shipping ({$set_store_currency}):</th>
	<td>{if $cart_status == 2}<input type="text" name="shipping" value="{$shipping}" style="width:100%;" />{else}{$set_store_symbol}{$shipping}{/if}</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Subtotal:</th>
	<td>{$set_store_symbol}{$subtotal}</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Tax:</th>
	<td>{$set_store_symbol}{$tax_total}<br />{$set_store_tax_number|nl2br}</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Total:</th>
	<td>{$set_store_symbol}{$total}</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Payment method:</th>
	<td>
	{if !$payment_method}Not avalable for this transaction{/if}
	{if $payment_method == "10"}PayPal{/if}
	{if $payment_method == "11"}Cheque{/if}
	{if $payment_method == "12"}Postal Order{/if}
	{if $payment_method == "13"}Money Order{/if}
	{if $payment_method == "14"}Bank Transfer{/if}
	{if $payment_method == "15"}2Checkout{/if}
	</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>Transaction status:</th>
	<td>
	
	<input type="radio" name="status" value="2" class="forminput"{if $cart_status == 2} checked="checked"{/if} />In progress<br />
	<input type="radio" name="status" value="3" class="forminput"{if $cart_status == 3} checked="checked"{/if} />Awaiting payment<br />
	<input type="radio" name="status" value="4" class="forminput"{if $cart_status == 4} checked="checked"{/if} />Paid
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td><input type="submit" name="submit" value="Update this transaction" style="width:100%;" class="formbutton" /></td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
	</table>

	</form>
	
</div>

</div>


<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.footer.tpl"}


