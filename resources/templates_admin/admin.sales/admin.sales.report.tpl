{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}




<div id="admin-outerdiv">

<div id="admin-innerdiv">




	
	
	<form action="{$base_url}{$fil_admin_store}" method="post">
	
	<input type="hidden" name="cmd" value="salesReportOuput" />
	
	<table class="table-search">
		
		<thead>
		
		<tr>
		<th>Generate sales reports for your website</th>
		</tr>
		
		</thead>
		
		<tbody>
		
		<tr>
		
		<td>
		
		<div class="search-box" style="border-right:1px solid #BBB; width:350px; min-height:200px;">
		
		<b>How to generate reports</b><br />
		
		<p>To create a sales report in a Microsoft Excel compatible CSV (comma separated values) format, use the date selection controls below to choose a timeframe from which to view sales.</p>
		
		<p>You can choose which columns of data you wish to include in the report using the checkboxes on the right.</p>
		
		<b>Choose a date range:</b><br />
		
		{html_select_date prefix="from_" start_year="-2" time="2006-01-01" display_days=true all_extra='class="forminput"'} (from)
		
		<br />
				
		{html_select_date prefix="to_" start_year="-2" display_days=true all_extra='class="forminput"'} (to)
		
		
		</div>
		
		<div class="search-box" style="border-right:1px solid #BBB; width:400px; min-height:200px;">
		
		<b>Include these columns:</b><br /><br/>
		
		<table>
		
		<tr>
		<td style="padding:3px 0px 3px 0px; width:180px; vertical-align:top;" valign="top">
		
		<input type="checkbox" checked="checked" name="inc_userid" class="forminput" /> User ID<br />
		<input type="checkbox" checked="checked" name="inc_name" class="forminput" /> Full Name<br />
		<input type="checkbox" checked="checked" name="inc_first_name" class="forminput" /> First name<br />
		<input type="checkbox" checked="checked" name="inc_family_name" class="forminput" /> Family name<br />
		<input type="checkbox" checked="checked" name="inc_address" class="forminput" /> Address<br />
		<input type="checkbox" checked="checked" name="inc_country" class="forminput" /> Country<br />

		</td>
		
		<td style="padding:3px 0px 3px 0px; width:180px; vertical-align:top;" valign="top">
		
		<input type="checkbox" checked="checked" name="inc_post_code" class="forminput" /> Post code<br />
		<input type="checkbox" checked="checked" name="inc_telephone" class="forminput" /> Telephone<br />
		<input type="checkbox" checked="checked" name="inc_fax" class="forminput" /> Fax<br />
		<input type="checkbox" checked="checked" name="inc_email" class="forminput" /> E-mail<br />
		<input type="checkbox" checked="checked" name="inc_sub_total" class="forminput" /> Sub-total<br />
		<input type="checkbox" checked="checked" name="inc_tax_total" class="forminput" /> Tax<br />
		
		</td>

		<td style="padding:3px 0px 3px 0px; width:180px; vertical-align:top;" valign="top">
		
		<input type="checkbox" checked="checked" name="inc_shippng" class="forminput" /> Shipping<br />
		<input type="checkbox" checked="checked" name="inc_total" class="forminput" /> Total<br />
		<input type="checkbox" checked="checked" name="inc_txn_id" class="forminput" /> Transaction ID<br />
		<input type="checkbox" checked="checked" name="inc_date_completed" class="forminput" /> Date completed<br />
		</td>
		
		</tr>
		
		</table>
		
		<br />
		
		<input type="submit" value="Download report" style="width:200px; float:right;" class="formbutton" />
		
		</div>
		
		</td>
		
		</tr>
		
	</table>
	
	</form>
	
</div>
	
</div>




<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.footer.tpl"}

