{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}



<div id="admin-outerdiv">

<div id="admin-innerdiv">







<table class="table-text">

<tr>
<th>This page allows you to view details of who has downloaded paid for and free high-resolution images from your website.</th>
<td>

<h1>Download log viewer</h1>

<p>The image download log viewer is designed to allow you to monitor how user of your website are using the free and paid for image download services.</p>

<p>If you are offering downloads to registered users without prior payment, this tool makes it possible to track image downloads on a user by user basis and bill them for images they have downloaded.</p>
	
</td>
<td>&nbsp;</td>
</tr>

</table>

<form action="{$base_url}{$fil_admin_store}" method="post">

<input type="hidden" name="cmd" value="listImageDownloads" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">Select view options for image downloads:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>Choose a month:</th>
<td>{html_select_date display_days=false prefix="date_" start_year="-3" time=$time}</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>Choose download type:</th>
<td><input type="radio" name="transaction" value="pay"{if $transaction == "pay" || $transaction == ""} checked="checked"{/if} /> Paid downloads from completed store purchases</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>&nbsp;</th>
<td><input type="radio" name="transaction" value="free"{if $transaction == "free"} checked="checked"{/if} /> Free downloads</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="submit" name="submit" value="List downloads" class="formbutton" style="width:100%;" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>


	
<table class="table-listbox">
	
	<tr>
		<th align="left">User name</th>	
		<th align="left">Number of images downloaded in {$month}</th>	
		<th align="left">Action</th>	
	</tr>
	
	{section name="downloads" loop="$userid"}
	
			<tr class="{cycle values='list-one,list-two'}">
			
			<td style="height:30px;">{$username[downloads]}</td>
			
			<td>{$download_count[downloads]}</td>
			
			<td>
			
			<ul class="actionlist">
			<li><a href="{$detail_url[downloads]}" title="View complete details of all images downloaded by this user during {$month}">View details</a></li>
			</ul>
			
			 </td>
			
			</tr>
	
	{sectionelse}
		
		{if $transaction == "pay" || $transaction == ""}
		<tr><td colspan="3" style="height:30px;">There are no paid for downloads to show for this month</td></tr>
		{else}
		<tr><td colspan="3" style="height:30px;">There are no free downloads to show for this month</td></tr>
		{/if}
		
	{/section}
	
	</table>
		
	<br />
	
</div>

</div>




<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.footer.tpl"}

