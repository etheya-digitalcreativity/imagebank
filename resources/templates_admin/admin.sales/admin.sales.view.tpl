{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}




<!-- SHOW THE CART CONTENTS FOR THIS USER -->
<div id="admin-outerdiv">

<div id="admin-innerdiv">








	<table class="table-text">
	
	<tr>
	<th colspan="3">This page lists all the items in the shopping cart for the user {$user_name}.</th>
	<td>
	
	<p class="title">Cart viewer</p>
	
	<p>This page lists the contents of the Pixaria shopping cart for {mailto address=$user_email text=$user_name encode="hex"}.</p>
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	</table>
	
	<table class="table-cart">
	
	<thead>
	
	<tr>
	<th colspan="3">Cart contents</th>
	</tr>
	
	</thead>
	
	<tbody>

		{section name="image" loop="$image_id"}
		
			<tr class="{cycle values='list-one,list-two'}">
			
				<th>
				
				{if $image_comp_path[image] == TRUE}
				<a href="javascript:openPop('{$base_url}{$fil_admin_image_output}?html={$image_comp_path[image]}&amp;', '{$image_comp_size[image].0-2}', '{$image_comp_size[image].1-2}', 'no', 'no', 'no', 'no', 'no', 'no', 'no');"><img src="{$base_url}{$fil_psg_image_thumbnail}?file={$image_small_path[image]}" {$image_small_dims[image]} class="thumbnail" alt="{$image_title[image]}" /></a>
				{else}
				<img src="{$base_url}{$fil_psg_image_thumbnail}?file={$image_icon_path[image]}" {$image_icon_dims[image]} class="thumbnail" alt="{$image_title[image]}" />
				{/if}
				
				</th>
				
				<td>
				
				<table class="cart-item">
				
				<tr>
				<th>Type of product:</th>
				<td>{$cart_item_type[image]}</td>
				</tr>
				
				{if $image_filename[image]!=""}<tr>
				<th>Filename:</th>
				<td>{$image_filename[image]}</td>
				</tr>
				{/if}
				
				<tr>
				<th>Title:</th>
				<td>{if $image_title[image]!=""}{$image_title[image]}{else}N/A{/if}</td>
				</tr>
				
				<tr>
				<th>Your requirements:</th>
				<td>{$item_usage_text[image]}</td>
				</tr>
				
					{if $image_comp_path[image] == TRUE}
					
						<tr>
						<th>Price ({$set_store_currency}):</th>
						<td>{$quote_price[image]}</td>
						</tr>
					
					{else}
					
						<tr>
						<th>Warning:</th>
						<td>This image has been removed from the library and is no longer available.<br />You are advised to remove it from your cart.</td>
						</tr>
					
					{/if}
					
				</table>
				
			</td>
			
			<td>&nbsp;</td>
			
			</tr>
							
		{/section}
		
	</tbody>
	
	</table>
	
	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">Invoice details</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Sales tax rate in percent (e.g. 17.5%):</th>
	<td>{if $cart_status == 2}<input type="text" name="sales_tax" value="{$sales_tax}" style="width:100%;" />{else}{$sales_tax}{/if}</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Cost of shipping ({$set_store_currency}):</th>
	<td>{if $cart_status == 2}<input type="text" name="shipping" value="{$shipping}" style="width:100%;" />{else}{$shipping}{/if}</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Subtotal:</th>
	<td>{$set_store_symbol}{$subtotal}</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Tax:</th>
	<td>{$set_store_symbol}{$tax_total}<br />{$set_store_tax_number|nl2br}</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Total:</th>
	<td>{$set_store_symbol}{$total}</td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
	</table>
	
</div>

</div>


<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.footer.tpl"}


