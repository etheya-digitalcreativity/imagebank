{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">

<table class="table-text">

	<tr>
	
		<th>Pixaria does not have permission to work with some of the selected files.</th>
			
		<td>
			
			<p class="title">Please note:</p>
			
			<p>Some of the images you're trying to import into Pixaria Gallery are not writable by PHP and therefore cannot be imported at this time.</p>
			
			<p>To fix this so that you can import these images, please use the FTP program you uploaded the images with (or a terminal application) to set the permissions to '666' or read/write for all users.</p>
			
			<p class="title">Tips:</p>
			
			<ul>
			<li>If you don't know how to do this, consult the user documentation for your FTP client.</li>
			<li>If you still get this error after confirming the permissions of these images have been set to '666', try setting the file permissions to '777' instead.</li>
			</ul>
			
			<form method="get" action="" onsubmit="javascript:history.go(-1);">
				
			<input type="button" onclick="javascript:history.go(-1);" name="js_back" value="Go back to the previous page" style="width:100%;" class="forminput" />
			
			</form>
		
		</td>
		
		<td>&nbsp;</td>
			
	</tr>
	
</table>
	
<table class="table-listbox">
			
	<tr>
	<th>Image filename</th>
	<th>Writable by PHP</th>
	</tr>
	
	{section name="images" loop=$images}
	
		<tr class="{cycle values='list-one,list-two'}">
		<td style="height:30px;">resources/incoming/{$images[images]}</td>
		<td>{if $images_writable[images]=="TRUE"}Writable{else}<span class="error">Not writable</span>{/if}</td>
		</tr>
	
	{sectionelse}
	
		<tr class="{cycle values='list-one,list-two'}">
		<td style="height:30px;" colspan="2">None of the images in this directory are writable by Pixaria.</td>
		</tr>
	
	{/section}
			
</table>

</div>

</div>


{include_template file="admin.snippets/admin.html.footer.tpl"}
