{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">



<form enctype="multipart/form-data" action="{$base_url}{$fil_admin_library_upload}" method="post">

<input type="hidden" name="MAX_FILE_SIZE" value="{$popp_upload_limit}" />

<input type="hidden" name="cmd" value="1" />

<table class="table-text">

<tr>
<th>Use this form to upload JPEG images into your Pixaria website.  Images you upload here will be available for you to import into your image library.</th>

<td>

<h1>Instructions for library images.</h1>

<p>The maximum allowed file upload size for JPEG images in Pixaria Gallery is {$popp_upload_limit/1024000} megabytes.  To increase this limit, you will need to raise PHP's script <i>memory usage</i> and <i>upload file size</i> limit in the php.ini configuration file for your web server.</p>

{include_template file="admin.snippets/admin.html.notification.note.tpl" form_title_text="To prevent PHP running out of memory when manipulating decompressed JPEG files, the maximum file upload size in php.ini should not be more than 30% of the maximum script memory usage."}
	
</td>
<td>&nbsp;</td>
</tr>

</table>

<table class="table-form">

<thead>

<tr>
<th colspan="3">Select files</th>
</tr>

</thead>

<tbody>

{if $set_gd_upload}

	<tr class="list-one">
	<th>Select a JPEG image file to upload:</th>
	<td><input type="file" name="img_high" id="get_file[]" /></td>
	<td>&nbsp;</td>
	</tr>
	
{/if}

<tr class="list-two">
<th>Your selected files:</th>
<td><div id="files_message">Selected files will be listed here</div><table><tbody id="files_list"></tbody></table></td>
<td>&nbsp;</td>
</tr>

<tr class="list-two">
<th>&nbsp;</th>
<td><input type="submit" name="submit" value="Proceed to next step" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>

<script language="JavaScript" type="text/javascript">

	<!-- Create an instance of the multiSelector class, pass it the output target and the max number of files -->
	var multi_selector = new MultiSelector( document.getElementById( 'files_list' ), 5 );
	<!-- Pass in the file element -->
	multi_selector.addElement( document.getElementById( 'get_file[]' ) );
	
</script>
	
</div>

</div>



<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.footer.tpl"}


