{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">





<form method="post" action="{$base_url}{$fil_admin_library_import}" name="selector">

<input type="hidden" name="cmd" value="6" />
	
<input type="hidden" name="image_path" value="{$directory}" />
	
<table class="table-search">

<thead>

<tr>
<th>1. Choose how you want to upload the images</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<td>

<div class="search-box" style="border-right:1px solid #BBB; min-height:130px;">

<b>a) Upload zip file or other archive</b>

<p>Use this option to upload a zipped (zip, tar, gz, tgz) archive of images.   The archive must not contain any directories (folders).</p>

<input type="button" value="Upload an archive of images" class="formbutton" onclick="new Ajax.Updater('uploader', '{$base_url}{$fil_admin_library_import}?cmd=showArchiveUploader&action=2', {literal}{asynchronous:true, parameters:Form.serialize(this)}{/literal}); return false;" style="width:100%;" />

</div>

<div class="search-box" style="border-right:1px solid #BBB; min-height:130px;">

<b>b) Web based image upload</b>

<p>Use this option to upload individual image files using your web browser.  This method only allows up to five images at a time.</p>

<input type="button" value="Web based image upload" class="formbutton" onclick="new Ajax.Updater('uploader', '{$base_url}{$fil_admin_library_import}?cmd=showJpegUploader', {literal}{asynchronous:true, parameters:Form.serialize(this),onComplete: createMultiSelector}{/literal}); return false;" style="width:100%;" />

</div>

</td>
</tr>
	
</tbody>

</table>

</form>

<div id="uploader">


</div>




<div id="forms">

</div>

<script language="javascript" type="text/javascript">
// <![CDATA[
	{literal}
	
	function createMultiSelector() {
		var multi_selector = new MultiSelector( document.getElementById( 'files_list' ), 5 );
		multi_selector.addElement( document.getElementById( 'get_file[]' ) );
	}

	function updateFileBrowser(url) {
		Effect.toggle('processing','appear',{duration:0.3});
		var ajax = new Ajax.Updater('forms',url,{method:'get',asynchronous:true,onComplete: showResponse});
	}
	
	function showResponse () {
		$('filebrowser').innerHTML = req.responseText;
		Effect.toggle('processing','appear',{duration:0.3});
	}
	{/literal}
// ]]>
</script>

</div>

</div>

{include_template file="admin.snippets/admin.html.footer.tpl"}
