{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">



<form method="post" action="{$base_url}{$fil_admin_library_import}" name="import">

<input type="hidden" name="cmd" value="addImageMetaData" />
	
<input type="hidden" name="directory" value="{$directory}" />
	
<table class="table-text">

<tr>
<th>Please review the contents of the directory that you are going to import into your library.</th>

<td>

	{if $all_present=="TRUE"}
	
		<p class="title">All required images are present</p>
	
		<p>All the necessary files needed to import these images into your library are present.  Please review the list of images and continue to the import screen.</p>
		
		{include_template file="admin.snippets/admin.html.notification.note.tpl" form_title_text="For performance reasons, you can only import $set_import_limit images at once into your library.<br /><br />You will be given the opportunity to import the remainder of the selected images once the first batch of $set_import_limit files has been added to the library."}
		
	{else}
	
		<p class="title">Some required images missing</p>
		
		<p>Pixaria Gallery stores up to five different sized versions of each image you add to your site.  These images include two sizes of thumbnails, an small icon sized image and the standard size 'comping' image which is what visitors see when they browse your site.</p>
		
		<p>Some of the files needed by Pixaria Gallery are missing from the directory you selected or could not be automatically generated.  Please check that PHP has read/write access to this directory and try refreshing this page.</p>
	
	{/if}

</td>
<td>&nbsp;</td>
</tr>

{if $all_present=="TRUE"}
		
	<tr>
	<th>&nbsp;</th>
	<td><input type="submit" name="submit" value="Proceed to next step" style="width:100%;" class="formbutton" /></td>
	<td>&nbsp;</td>
	</tr>

{/if}
			
</table>

</form>



<table class="table-listbox">

	<tr>
	<th colspan="2">Image filename</th>
	<th style="text-align:center;">High Resolution</th>
	<th style="text-align:center;">Comping</th>
	<th style="text-align:center;">Large Thumbnail</th>
	<th style="text-align:center;">Small Thumbnail</th>
	<th style="text-align:center;">Icon</th>
	</tr>
	
	{section name="assets" loop=$icon_path}
	
		<tr class="{cycle values='list-one,list-two'}">
		
		<td style="width:42px; height:42px; text-align:center;">{if $upload_arr_i[assets]}
			<a href="javascript:imagePreviewWindow('{$base_url}{$fil_admin_image_output}?html={$comp_path[assets]}&amp;','{$comp_width[assets]}','{$comp_height[assets]}');"><img src="{$base_url}{$fil_admin_image_output}?file={$icon_path[assets]}&amp;" border="0" width="{$icon_width[assets]}" height="{$icon_height[assets]}" title="{$file_name[assets]}" alt="{$file_name[assets]}" class="icon" /></a>
		{else}
			<img src="{$base_url}resources/images/furniture/cross.gif" width="32" height="32" border="0" alt="File not present" title="File not present">
		{/if}</td>
		
		<td><a class="plain" href="javascript:openPop('{$base_url}{$fil_admin_image_output}?html={$comp_path[assets]}&amp;','{$comp_width[assets]-2}','{$comp_height[assets]-2}','no','no','no','no','no','no','no');">{$file_name[assets]}</a></td>
		
		<td style="text-align:center;">{if $upload_arr_o[assets]}
			<img src="{$base_url}resources/images/furniture/tick.gif" width="32" height="32" border="0" alt="File present" title="File present">
		{else}
			<img src="{$base_url}resources/images/furniture/cross.gif" width="32" height="32" border="0" alt="File not present" title="File not present">
		{/if}</td>
		
		<td style="text-align:center;">{if $upload_arr_c[assets]}
			<img src="{$base_url}resources/images/furniture/tick.gif" width="32" height="32" border="0" alt="File present" title="File present">
		{else}
			<img src="{$base_url}resources/images/furniture/cross.gif" width="32" height="32" border="0" alt="File not present" title="File not present">
		{/if}</td>
		
		<td style="text-align:center;">{if $upload_arr_l[assets]}
			<img src="{$base_url}resources/images/furniture/tick.gif" width="32" height="32" border="0" alt="File present" title="File present">
		{else}
			<img src="{$base_url}resources/images/furniture/cross.gif" width="32" height="32" border="0" alt="File not present" title="File not present">
		{/if}</td>
		
		<td style="text-align:center;">{if $upload_arr_s[assets]}
			<img src="{$base_url}resources/images/furniture/tick.gif" width="32" height="32" border="0" alt="File present" title="File present">
		{else}
			<img src="{$base_url}resources/images/furniture/cross.gif" width="32" height="32" border="0" alt="File not present" title="File not present">
		{/if}</td>
		
		<td style="text-align:center;">{if $upload_arr_i[assets]}
			<img src="{$base_url}resources/images/furniture/tick.gif" width="32" height="32" border="0" alt="File present" title="File present">
		{else}
			<img src="{$base_url}resources/images/furniture/cross.gif" width="32" height="32" border="0" alt="File not present" title="File not present">
		{/if}</td>
							
		</tr>
	
	{/section}

</table>

</div>

</div>

{include_template file="admin.snippets/admin.html.footer.tpl"}