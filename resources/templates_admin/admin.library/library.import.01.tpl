{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">

<form method="post" action="{$base_url}{$fil_admin_library_import}">

<input type="hidden" name="directory" value="{$temp_name}" />

<input type="hidden" name="cmd" value="addImageMetaData" />

<table class="table-text">

<tr>
<th>The images you selected have been made into thumbnails and moved into a directory ready to be imported into Pixaria Gallery.</td>

<td>

<h1>Image thumbnails created</h1>

<p>The images you selected have been resized into thumbnail and icon sized images and have placed into a directory called '{$temp_name}' ready to be imported into the library.</p>

<p>To import the images now, please continue using the form below or alternatively, go back to the <a href="{$base_url}{$fil_admin_library_import}">image import screen</a> where you will find these images listed under the menu item 'Show complete galleries'.</p>

{if $jpeg_error}

{include_template file="admin.snippets/admin.html.notification.warning.tpl" form_title_text="One or more of the images you're attempting to import could not be loaded by Pixaria.  This may be because the file(s) are corrupt or missing, possibly because they have been uploaded incorrectly or an error occurred during the upload process.<br /><br />If you used an FTP client to upload the image(s) ensure you use 'binary mode' if your FTP client has a setting for it."}

{/if}

</td>
<td>&nbsp;</td>
</tr>

<tr>
<th>&nbsp;</th>
<td><input type="submit" name="submit" value="Import these images into the library" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</table>

</form>

<table class="table-listbox">

	<tr>
	<th colspan="2">Image filename</th>
	<th style="text-align:center;">High Resolution</th>
	<th style="text-align:center;">Comping</th>
	<th style="text-align:center;">Large Thumbnail</th>
	<th style="text-align:center;">Small Thumbnail</th>
	<th style="text-align:center;">Icon</th>
	</tr>
	
	{section name="assets" loop=$icon_path}
	
		<tr class="{cycle values='list-one,list-two'}">
		
		<td style="width:42px; height:42px; text-align:center;">{if $upload_arr_i[assets]}
			<a href="javascript:openPop('{$base_url}{$fil_admin_image_output}?html={$comp_path[assets]}&amp;','{$comp_width[assets]-2}','{$comp_height[assets]-2}','no','no','no','no','no','no','no');"><img src="{$base_url}{$fil_admin_image_output}?file={$icon_path[assets]}&amp;" border="0" width="{$icon_width[assets]}" height="{$icon_height[assets]}" title="{$file_name[assets]}" alt="{$file_name[assets]}" class="icon" /></a>
		{else}
			<img src="{$base_url}resources/images/furniture/cross.gif" width="32" height="32" border="0" alt="File not present" title="File not present">
		{/if}</td>
		
		<td><a class="plain" href="javascript:openPop('{$base_url}{$fil_admin_image_output}?html={$comp_path[assets]}&amp;','{$comp_width[assets]-2}','{$comp_height[assets]-2}','no','no','no','no','no','no','no');">{$file_name[assets]}</a></td>
		
		<td style="text-align:center;">{if $upload_arr_o[assets]}
			<img src="{$base_url}resources/images/furniture/tick.gif" width="32" height="32" border="0" alt="File present" title="File present">
		{else}
			<img src="{$base_url}resources/images/furniture/cross.gif" width="32" height="32" border="0" alt="File not present" title="File not present">
		{/if}</td>
		
		<td style="text-align:center;">{if $upload_arr_c[assets]}
			<img src="{$base_url}resources/images/furniture/tick.gif" width="32" height="32" border="0" alt="File present" title="File present">
		{else}
			<img src="{$base_url}resources/images/furniture/cross_warning.gif" width="32" height="32" border="0" alt="File not present" title="File not present">
		{/if}</td>
		
		<td style="text-align:center;">{if $upload_arr_l[assets]}
			<img src="{$base_url}resources/images/furniture/tick.gif" width="32" height="32" border="0" alt="File present" title="File present">
		{else}
			<img src="{$base_url}resources/images/furniture/cross_warning.gif" width="32" height="32" border="0" alt="File not present" title="File not present">
		{/if}</td>
		
		<td style="text-align:center;">{if $upload_arr_s[assets]}
			<img src="{$base_url}resources/images/furniture/tick.gif" width="32" height="32" border="0" alt="File present" title="File present">
		{else}
			<img src="{$base_url}resources/images/furniture/cross_warning.gif" width="32" height="32" border="0" alt="File not present" title="File not present">
		{/if}</td>
		
		<td style="text-align:center;">{if $upload_arr_i[assets]}
			<img src="{$base_url}resources/images/furniture/tick.gif" width="32" height="32" border="0" alt="File present" title="File present">
		{else}
			<img src="{$base_url}resources/images/furniture/cross_warning.gif" width="32" height="32" border="0" alt="File not present" title="File not present">
		{/if}</td>
							
		</tr>
	
	{/section}

</table>

</div>

</div>

{include_template file="admin.snippets/admin.html.footer.tpl"}