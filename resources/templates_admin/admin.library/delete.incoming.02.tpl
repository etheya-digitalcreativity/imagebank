{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">

<form method="post" action="{$base_url}{$fil_admin_library_import}">

<table class="table-text">

<tr>
<th>Some of the selected images could not be deleted.</th>

<td>

<h1>Problem deleting images</h1>

<p>Some of the selected images could not be deleted.  This is probably because the permissions on the files are not set in a way that allows PHP to remove them automatically.</p>

<p>The images that could not be removed are listed below and will need to be deleted using an FTP program.</p>

</td>
<td>&nbsp;</td>
</tr>

</table>
		
<table class="table-listbox">

<tr>
	<th colspan="2">File name</th>	
	<th>File size (kilobytes)</th>	
	<th>Image dimensions (pixels)</th>
</tr>

{section name=files loop=$filename}
	
	<tr class="{cycle values='list-one,list-two'}">
	<td style="width:20px;"><a href="javascript:openPop('{$base_url}{$fil_admin_image_output}?html={$filepath[files]}&amp;','{$width[files]-2}','{$height[files]-2}','no','no','no','no','no','no','no');"><img src="{$base_url}resources/images/icons.inline/magnify.gif" title="Magnify Image" alt="Magnify Image" width="16" height="16" border="0" /></a></td>
	<td align="left"><a href="javascript:openPop('{$base_url}{$fil_admin_image_output}?html={$filepath[files]}&amp;','{$width[files]-2}','{$height[files]-2}','no','no','no','no','no','no','no');">{$filename[files]}</a></td>
	<td>{$filesize[files]}kb</td>
	<td>{$width[files]} pixels x {$height[files]} pixels</td>
	</tr>

{/section}

	
</table>

<table class="table-text">

<tr>
<th>&nbsp;</th>
<td><input type="submit" value="Go back to the image import screen" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

</table>

</form>

</div>

</div>

<!-- INCLUDE THE TOP HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}

