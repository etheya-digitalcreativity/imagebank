{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}



<div id="admin-outerdiv">

<div id="admin-innerdiv">



<form method="post" action="{$base_url}{$fil_admin_library_import}" onsubmit="javascript:history.go(-1);">

<table class="table-text" border="0" cellpadding="3" cellspacing="0">

<tr>
<th>There was a problem with the information you submitted on the previous page.</th>

<td>

<h1>Error message</h1>

<p>{$error_message}</p>

</td>
<td>&nbsp;</td>
</tr>

<tr>
<td>&nbsp;</td>
<td><input type="button" value="Go back to the import screen" style="width:100%;" class="forminput" onclick="javascript:history.go(-1);" /></td>
<td>&nbsp;</td>
</tr>

</table>

</form>

</div>

</div>

{include_template file="admin.snippets/admin.html.footer.tpl"}
