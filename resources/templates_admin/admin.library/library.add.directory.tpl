{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

<table class="table-search">

<thead>

<tr>
<th>2. Select a directory to import into the library</th>
</tr>

</thead>

<tbody>

<tr>

<td>

<div class="search-box" style="min-height:100px; width:770px;">

<p class="title">Import complete directories of images</p>
	
<p>This page lists complete directories which are ready to import into Pixaria.</p>

<p>The minimum requirement for Pixaria to be able to import a directory of images is that it contains a sub-directory called '{$sys_comping_dir}' which must contain images with dimensions not larger than {$sys_comping_size} pixels square.  High-resolution images in a directory called 'original' and directories of smaller images for thumbnails are optional.</p>

</div>

</td>

</tr>

</tbody>

</table>


{if ($display=="TRUE") }

	<form method="post" name="img_grid" action="{$base_url}{$fil_admin_library_import}" target="_top"{if ($cmd=="gall") } onsubmit="return confirmSubmit('Please note that large groups of files, especially high-resolution images, can take some time to process.  Are you sure you want to continue?');"{/if}>
	
	<input type="hidden" name="action" value="{$cmd}" />

	<table class="table-listbox">

	<tr>
		<th>Directory name</th>	
		<th>Number of images</th>	
		<th>Action</th>	
	</tr>

	{section name=directories loop=$directory_name}
		
		<tr class="{cycle values='list-one,list-two'}">
		<td style="height:30px;"><img src="{$base_url}resources/images/icons/22x22/folder.gif" alt="" title="" width="22" height="22" style="vertical-align:middle;" />&nbsp;&nbsp;resources/incoming/{$directory_name[directories]}</td>
		<td>{$directory_count[directories]}</td>
		<td>
		
		<ul class="actionlist">
		<li><a href="{$base_url}{$fil_admin_library_import}?cmd=checkAllImagesPresent&amp;directory={$directory_name[directories]}">Import directory</a></li>
		<li><a href="{$base_url}{$fil_admin_library_import}?cmd=deleteCompleteDirectory&amp;directory={$directory_name[directories]}">Delete directory</a></li>
		</ul>
		
		</td>
		</tr>
	
	{/section}
	
	</table>
		
	</form>

{else}

	<table class="table-form">
	
	<thead>
	
	<tr>
		<th colspan="3">No images in selected source.</th>	
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two'}">
	
		<th>&nbsp;</th>
		
		<td>
		There are no images to view in this selection.
		
		<ol>
		
		<li>'Comping images' view lists images {$sys_comping_size} x {$sys_comping_size} pixels or smaller in the /resources/incoming/ directory.</li>
		<li>'High-res images' view lists images larger than {$sys_comping_size} x {$sys_comping_size} pixels in the /resources/incoming/ directory.</li>
		<li>'Complete directories' view lists images uploaded in the standard Pixaria directory format (see <a href="http://www.pixaria.com/wiki/">documentation</a>).</li>
		
		</td>
		
		<td>&nbsp;</td>
		
	</tr>
	
	</tbody>
	
	</table>
	
{/if}
