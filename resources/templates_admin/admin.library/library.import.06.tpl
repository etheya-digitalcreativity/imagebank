{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">

{if $complete_failure}

	<table class="table-text">
	
	<tr>
	<th>The images you are attempting to import already exist in the library.</th>
	
	<td>
	
		<h1>No images were imported</h1>
		
		<p>None of the images you selected were imported because image files with the same name already exist in the target directory in your image library.</p>
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	</table>
	
{else}
	
	{if $partial_failure}
	
		<table class="table-text">
		
		<tr>
		<th>Some images with the same file names already exist in the target directory and were not imported.</th>
		
		<td>
		
			<h1>Some images were not imported</h1>
			
			<p>Some images with the same names as ones you tried to import are already in you image library and were therefore not imported.  These images are listed below:</p>
		
		</td>
		<td>&nbsp;</td>
		</tr>
		
		</table>
		
		<table class="table-listbox">
		
		<tr>
		<th>Names of files which were not imported</th>
		</tr>
		
		{section name="failed" loop="$failed_images"}
		
		<tr>
		<td style="height:30px;">{$failed_images[failed]}</td>
		</tr>
		
		{/section}
		
		</table>
		
	{else}
	
		<table class="table-text">
		
		<tr>
		<th>The selected images have been successfully imported into your Pixaria library.</th>
		
		<td>
		
			<h1>Images successfully imported</h1>
			
			<p>The images have been successfully imported into the Pixaria library.  You can now choose from the following actions:</p>
		
		</td>
		<td>&nbsp;</td>
		</tr>
		
		</table>
	
	{/if}
	
	{if $more_to_do}
	
		<form method="post" action="{$base_url}{$fil_admin_library_import}">
		
		<input type="hidden" name="directory" value="{$directory}" />

		<input type="hidden" name="image_active" value="{$image_active}" />
		<input type="hidden" name="image_title" value="{$image_title}" />
		<input type="hidden" name="image_caption" value="{$image_caption}" />
		<input type="hidden" name="image_keywords" value="{$image_keywords}" />
		<input type="hidden" name="image_keywords_append" value="{$image_keywords_append}" />
		<input type="hidden" name="date_option" value="{$date_option}" />
		<input type="hidden" name="mysql_format_time" value="{$mysql_format_time}" />
		<input type="hidden" name="toda_Month" value="{$toda_Month}" />
		<input type="hidden" name="toda_Day" value="{$toda_Day}" />
		<input type="hidden" name="toda_Year" value="{$toda_Year}" />
		<input type="hidden" name="image_copyright" value="{$image_copyright}" />
		<input type="hidden" name="image_price" value="{$image_price}" />
		<input type="hidden" name="image_sale" value="{$image_sale}" />
		<input type="hidden" name="image_product_link" value="{$image_product_link}" />
		<input type="hidden" name="image_permissions" value="{$image_permissions}" />

		{section name="group" loop=$image_groups}
			<input type="hidden" name="image_groups[]" value="{$image_groups[group]}" />
		{sectionelse}
			<input type="hidden" name="image_groups[]" value="" />
		{/section}
		
		{section name="product" loop=$image_products}
			<input type="hidden" name="image_products[]" value="{$image_products[product]}" />
		{sectionelse}
			<input type="hidden" name="image_products[]" value="" />
		{/section}
	
		<input type="hidden" name="image_rights_type" value="{$image_rights_type}" />
		<input type="hidden" name="image_rights_text" value="{$image_rights_text}" />
		<input type="hidden" name="image_model_release" value="{$image_model_release}" />
		<input type="hidden" name="image_property_release" value="{$image_property_release}" />
		<input type="hidden" name="image_extra_01" value="{$image_extra_01}" />
		<input type="hidden" name="image_extra_02" value="{$image_extra_02}" />
		<input type="hidden" name="image_extra_03" value="{$image_extra_03}" />
		<input type="hidden" name="image_extra_04" value="{$image_extra_04}" />
		<input type="hidden" name="image_extra_05" value="{$image_extra_05}" />

		<table class="table-form">
		
		<thead>
		
		<tr>
		<th colspan="3">Import remaining images?</th>
		</tr>
		
		</thead>
		
		<tbody>
		
		<tr class="{cycle values='list-one,list-two'}">
		<th>What's next...?</th>
		<td>
		
		<b>Continue importing from this directory?</b><br /><br />
		
		<p>You have successfully imported some image files from the selected directory but there are more to do.  Would you like to continue importing the remaining images in this directory?</p>
		
		<p>You can choose which step of the import process to continue from and all your previous global metadata is saved if you wish to reuse it.</p>
		
		</td>
		<td>&nbsp;</td>
		</tr>
		
		<tr class="{cycle values='list-one,list-two' advance='0'}">
		<th></th>
		<td><input type="radio" name="cmd" value="approveImageMetaData" id="cmd_1" class="forminput" checked="checked" /> <label for="cmd_1">Continue from the image metadata list screen</label></td>
		<td>&nbsp;</td>
		</tr>
		
		<tr class="{cycle values='list-one,list-two' advance='0'}">
		<th></th>
		<td><input type="radio" name="cmd" value="addImageMetaData" id="cmd_2" class="forminput" /> <label for="cmd_2">Amend global metadata for this import</label></td>
		<td>&nbsp;</td>
		</tr>
		
		<tr class="{cycle values='list-one,list-two'}">
		<th>&nbsp;</th>
		<td><input type="submit" name="submit" value="Continue importing remaining images" style="width:100%;" class="formbutton" /></td>
		<td>&nbsp;</td>
		</tr>
		
		</table>
		
		</form>

	{/if}





	
	<table class="table-search">
	
	<tr>
	<th>What would you like to do next?</th>
	</tr>
	
	<tr>
	
	<td>
	
	<div class="search-box" style="border-right:1px solid #BBB; width:520px; min-height:320px;">
	
	<form method="post" action="{$base_url}{$fil_admin_gallery}">
	
	<input type="hidden" name="cmd" value="addImagesToGallery" />
	
	{section name="image" loop="$images"}
		<input type="hidden" name="images[]" value="{$images[image]}" />
	{/section}

	<h1>Add images to a gallery:</h1>

	<p>Select an existing gallery you would like to add these images to and then choose whether to place the new images at the start or end of the gallery.</p>

	{html_fancy_select_single options=$gallery_title values=$gallery_id name='gallery_id' width='520' rows='5'}
	
	<br />
	<b>Where do you want to place the images?</b><br /><br />
	
	&nbsp;&nbsp;&nbsp;&nbsp;<input name="insert_location" class="forminput" type="radio" value="start" checked="checked" /> Insert at the start of the gallery<br />
	&nbsp;&nbsp;&nbsp;&nbsp;<input name="insert_location" class="forminput" type="radio" value="end" /> Insert at the end of the gallery<br /><br />

	
	<input type="submit" name="submit" value="Add images to the gallery" style="width:295px;s" class="formbutton" />
	
	</form>
	
	</div>
	
	<div class="search-box" style="border-right:1px solid #BBB; width:350px; min-height:320px;">
	
	<h1>Other options</h1>

	<p>If you don't want to add these images to an existing gallery, you can create a new one, go back to the main gallery list or browse all the images not yet added to any gallery (orphans).</p>
	
	<form method="post" action="{$base_url}{$fil_admin_gallery}">
	
	<input type="hidden" name="image_path" value="{$directory}" />
	
	{section name="image" loop="$images"}
		<input type="hidden" name="images[]" value="{$images[image]}" />
	{/section}
	
	<input type="radio" name="cmd" id="cmd1" value="formCreateGalleryWithImages" class="forminput" /> <label for="cmd1"><b>Create a new gallery with these images</b></label><br /><br />
	
	<br />
	<input type="submit" name="submit" value="Continue" style="width:200px; float:right;" class="formbutton" />
	
	</form>
	
	</div>
	
	</td>
	
	</tr>
	
	</table>
	
	

	
{/if}

</div>

</div>

{include_template file="admin.snippets/admin.html.footer.tpl"}
