{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">


<table class="table-text">

<tr>
<th>Please review the contents of the directory that you are going to import into your library.</th>

<td>

	{if $all_present=="TRUE"}
	
		<h1>All required images are present</h1>
	
		<p>All the necessary files needed by Pixaria are present.  To begin importing these images into your database, choose how you would like to import the images.</p>
		
		{include_template file="admin.snippets/admin.html.notification.note.tpl" form_title_text="If you're importing more than 100 files, you will be given the opportunity to import the remainder of the selected images once the first batch of $set_import_limit files has been added to the library."}
		
	{else}
	
		<h1 class="title">Some required images missing</h1>
		
		<p>Pixaria Gallery stores up to five different sized versions of each image you add to your site.  These images include two sizes of thumbnails, an small icon sized image and the standard size 'comping' image which is what visitors see when they browse your site.</p>
		
		<p>Some of the files needed by Pixaria are missing from the directory you selected or could not be automatically generated.  Please check that PHP has read/write access to this directory and try refreshing this page.</p>
	
	{/if}

</td>
<td>&nbsp;</td>
</tr>

</table>



<form method="post" action="{$base_url}{$fil_admin_library_import}">
	
{if $all_present=="TRUE"}
	
	<input type="hidden" name="cmd" value="importImages" />
		
	<input type="hidden" name="directory" value="{$directory}" />
			
	<input type="hidden" name="total_images" value="{$total_images}" />
			
	<input type="hidden" name="global_image_active" value="{$image_active}" />
	<input type="hidden" name="global_image_title" value="{$image_title}" />
	<input type="hidden" name="global_image_caption" value="{$image_caption}" />
	<input type="hidden" name="global_image_keywords" value="{$image_keywords}" />
	<input type="hidden" name="global_image_keywords_append" value="{$image_keywords_append}" />
	<input type="hidden" name="global_date_option" value="{$date_option}" />
	<input type="hidden" name="global_mysql_format_time" value="{$mysql_format_time}" />
	<input type="hidden" name="global_toda_Month" value="{$toda_Month}" />
	<input type="hidden" name="global_toda_Day" value="{$toda_Day}" />
	<input type="hidden" name="global_toda_Year" value="{$toda_Year}" />
	<input type="hidden" name="global_image_copyright" value="{$image_copyright}" />
	<input type="hidden" name="global_image_price" value="{$image_price}" />
	<input type="hidden" name="global_image_sale" value="{$image_sale}" />
	<input type="hidden" name="global_image_product_link" value="{$image_product_link}" />
	<input type="hidden" name="global_image_permissions" value="{$image_permissions}" />
	
	{section name="group" loop=$image_groups}
		<input type="hidden" name="global_image_groups[]" value="{$image_groups[group]}" />
	{sectionelse}
		<input type="hidden" name="global_image_groups[]" value="" />
	{/section}
	
	{section name="product" loop=$image_products}
		<input type="hidden" name="global_image_products[]" value="{$image_products[product]}" />
	{sectionelse}
		<input type="hidden" name="global_image_products[]" value="" />
	{/section}
	
	<input type="hidden" name="global_image_rights_type" value="{$image_rights_type}" />
	<input type="hidden" name="global_image_rights_text" value="{$image_rights_text}" />
	<input type="hidden" name="global_image_model_release" value="{$image_model_release}" />
	<input type="hidden" name="global_image_property_release" value="{$image_property_release}" />
	<input type="hidden" name="global_image_extra_01" value="{$image_extra_01}" />
	<input type="hidden" name="global_image_extra_02" value="{$image_extra_02}" />
	<input type="hidden" name="global_image_extra_03" value="{$image_extra_03}" />
	<input type="hidden" name="global_image_extra_04" value="{$image_extra_04}" />
	<input type="hidden" name="global_image_extra_05" value="{$image_extra_05}" />

	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">Global image settings:</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>Products available for this image:</th>
	<td>{if $image_product_link}Selected image products:{else}All image products{/if}<input type="hidden" name="image_product_link" value="{$image_product_link}" /></td>
	<td>&nbsp;</td>
	</tr>
	
	{if $image_product_link}

		<tr class="{cycle values='list-one,list-two'}">
		<th>&nbsp;</th>
		<td>
		<div style="padding-left:20px;">
		{section name="product" loop=$product_id}
		{section name="enabled" loop=$image_products}{if $image_products[enabled] == $product_id[product]} <li><input type="hidden" name="image_products[]" value="{$product_id[product]}" />{$product_name[product]}</li>{/if}{/section}
		{sectionelse}
		<b>There are no image products configured for this website.</b>
		{/section}
		</div>
		</td>
		<td>&nbsp;</td>
		</tr>

	{/if}
		
	<tr class="{cycle values='list-one,list-two'}">
	<th>Make images visible to site visitors:</th>
	<td>{if $image_permissions=="10"}All users{elseif $image_permissions=="11"}Registered users only{else}Selected user groups{/if}<input type="hidden" name="image_permissions" value="{$image_permissions}" /></td>
	<td>&nbsp;</td>
	</tr>
	
	{if $image_permissions=="12"}
	
		<tr class="{cycle values='list-one,list-two'}">
		<th>Groups who can view these images:</th>
		<td>
		
		{if $image_group_name!=""}
			
			{section name="groups" loop=$image_group_name}
			
				{$smarty.section.groups.index+1})&nbsp;&nbsp;{$image_group_name[groups]}<br />
			
				<input type="hidden" name="image_groups[]" value="{$image_group_id[groups]}" />
		
			{/section}
		
		{else}None{/if}
		
		</td>
		<td>&nbsp;</td>
		</tr>
	
	{/if}
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td><input type="submit" name="submit" value="Add these images to your library" style="width:100%;" class="formbutton" /></td>
	<td></td>
	</tr>
	
	</tbody>
	
	</table>
	
{/if}

<table class="table-import">

	<tr>
	<th>Individual image settings</th>
	</tr>
	
	{section name="assets" loop=$icon_path}
	
		{if $smarty.section.assets.index <= $set_import_limit-1}
		
		<tr class="{cycle values='list-one,list-two'}">
		
		<td>
		
		<div class="import-box" style="border-right:1px solid #CCC;">

			<span class="bold">File name:</span> {$file_name[assets]|truncate:30}<br />
			<input type="hidden" name="image_filename[]" value="{$image_filename[assets]}" /><br />
			
			<span class="bold">Image size:</span> {$image_size[assets].0} x {$image_size[assets].1} pixels<br /><br />
			<input type="hidden" name="image_width[]" value="{$image_size[assets].0}" />
			<input type="hidden" name="image_height[]" value="{$image_size[assets].1}" />
			
			<input type="checkbox" name="image_import[{$smarty.section.assets.index}]" class="forminput" checked="checked" /> <span class="bold">Import this image</span><br />
			
			<input type="checkbox" name="image_active[{$smarty.section.assets.index}]" class="forminput" {if $image_active == "on"}checked="checked" {/if}/> <span class="bold">Image is active</span><br />
			
			<br />
			
			<table style="width:100%;"><tr><td style="text-align:center; border:0px;"><a href="javascript:openPop('{$base_url}{$fil_admin_image_output}?html={$comp_path[assets]}&amp;','{$comp_width[assets]-2}','{$comp_height[assets]-2}','no','no','no','no','no','no','no');"><img src="{$base_url}{$fil_admin_image_output}?file={$large_path[assets]}&amp;" border="0" width="{$large_width[assets]}" height="{$large_height[assets]}" title="{$file_name[assets]}" alt="{$file_name[assets]}" class="thumbnail" /></a>
			
			<br /><br />
			
			<ul class="actionlist">
			<li><a href="javascript:openPop('{$base_url}{$fil_admin_library_import}?cmd=viewImageMetaData&amp;filepath={$image_mpath[assets]}&amp;','560','450','no','yes','no','no','yes','no','no');" style="padding:5px 8px 5px 24px; font-weight:bold;" class="infobutton">View image metadata</a>
			</li>
			</ul>
			
			</td></tr></table>

			
			
		</div>
		
		<div class="import-box" style="border-right:1px solid #CCC;">
			
			<input type="hidden" name="iptc_creator[{$smarty.section.assets.index}]" value="{$iptc_creator[assets]}" />
			<input type="hidden" name="iptc_jobtitle[{$smarty.section.assets.index}]" value="{$iptc_jobtitle[assets]}" />
			<input type="hidden" name="iptc_city[{$smarty.section.assets.index}]" value="{$iptc_city[assets]}" />
			<input type="hidden" name="iptc_country[{$smarty.section.assets.index}]" value="{$iptc_country[assets]}" />
			<input type="hidden" name="iptc_credit[{$smarty.section.assets.index}]" value="{$iptc_credit[assets]}" />
			<input type="hidden" name="iptc_source[{$smarty.section.assets.index}]" value="{$iptc_source[assets]}" />
			<input type="hidden" name="iptc_object[{$smarty.section.assets.index}]" value="{$iptc_object[assets]}" />
			<input type="hidden" name="iptc_byline[{$smarty.section.assets.index}]" value="{$iptc_byline[assets]}" />
			
			<span class="bold">Image title (headline):</span><br />
			<input type="text" value="{if $iptc_headline[assets]!=""}{$iptc_headline[assets]}{elseif $iptc_objname[assets]!=""}{$iptc_objname[assets]}{else}{$image_title}{/if}" placeholder="Image title" name="image_title[{$smarty.section.assets.index}]" class="forminput" style="width:100%;" />
			
			<br /><br /><span class="bold">Image caption:</span><br />
			<textarea name="image_caption[{$smarty.section.assets.index}]" class="forminput" style="width:100%;" rows="3">{if $iptc_caption[assets]!=""}{$iptc_caption[assets]}{else}{$image_caption}{/if}</textarea>
			
			<br /><br /><span class="bold">Image keywords (comma separated):</span><br />
			<textarea name="image_keywords[{$smarty.section.assets.index}]" class="forminput" style="width:100%;" rows="3">{if $iptc_keywords[assets]!=""}{$iptc_keywords[assets]}{else}{$image_keywords}{/if}</textarea>
			
			<br /><br /><span class="bold">Image copyright:</span><br />
			<input type="text" value="{if $iptc_copyright[assets]!=""}{$iptc_copyright[assets]}{else}{$image_copyright}{/if}" placeholder="Image copyright owner" name="image_copyright[{$smarty.section.assets.index}]" class="forminput" style="width:100%;" />
			
			<br /><br /><span class="bold">Image date:</span><br />
			
			{html_select_date field_order="DMY" field_array="date_custom" prefix=$smarty.section.assets.index start_year="1970" reverse_years="TRUE" time=$image_datetime[assets]}<br /><br />
			<input type="hidden" name="date_custom[{$smarty.section.assets.index}Hour]" value="{$image_datetime[assets]|date_format:"%H"}" />
			<input type="hidden" name="date_custom[{$smarty.section.assets.index}Minute]" value="{$image_datetime[assets]|date_format:"%M"}" />
			<input type="hidden" name="date_custom[{$smarty.section.assets.index}Second]" value="{$image_datetime[assets]|date_format:"%S"}" />
		</div>
		
		<div class="import-box" style="border-right:1px solid #CCC;">
			
			<input type="checkbox" name="model_release[{$smarty.section.assets.index}]" class="forminput" {if $image_model_release == "on"}checked="checked" {/if} /> <span class="bold">Has model release</span><br />
		
			<input type="checkbox" name="property_release[{$smarty.section.assets.index}]" class="forminput" {if $image_property_release == "on"}checked="checked" {/if} /> <span class="bold">Has property release</span><br />
		
			<br /><input type="checkbox" name="image_sale[{$smarty.section.assets.index}]" class="forminput"{if $image_sale=="on"}checked="checked" {/if} /> <span class="bold">Image is for sale</span>
		
			<br /><br /><span class="bold">Image price ({$set_store_currency}):</span><br />
			{$set_store_symbol}<input type="text" value="{$image_price}" placeholder="10.00" name="image_price[{$smarty.section.assets.index}]" class="forminput" size="10" />
			
			<br /><br /><span class="bold">Right management type:</span><br />
			<input type="radio" name="image_rights_type[{$smarty.section.assets.index}]" class="forminput" value="10" {if $image_rights_type == "10"}checked="checked" {/if} /> Royalty free<br />
			<input type="radio" name="image_rights_type[{$smarty.section.assets.index}]" class="forminput" value="11" {if $image_rights_type == "11"}checked="checked" {/if} /> Rights managed
			
			<br /><br /><span class="bold">Rights management information:</span><br />
			<input type="text" value="{$image_rights_text}" placeholder="Rights management info" name="image_rights_text[{$smarty.section.assets.index}]" class="forminput" style="width:100%;" />
			
		</div>
		
		<div class="import-box" style="border-right:1px solid #CCC;">
			
			<span class="bold">{$set_extra_field_01}:</span><br />
			<input type="text" value="{$image_extra_01}" name="image_extra_01[{$smarty.section.assets.index}]" class="forminput" style="width:100%;" />
			
			<br /><br /><span class="bold">{$set_extra_field_02}:</span><br />
			<input type="text" value="{$image_extra_02}" name="image_extra_02[{$smarty.section.assets.index}]" class="forminput" style="width:100%;" />
			
			<br /><br /><span class="bold">{$set_extra_field_03}:</span><br />
			<input type="text" value="{$image_extra_03}" name="image_extra_03[{$smarty.section.assets.index}]" class="forminput" style="width:100%;" />
			
			<br /><br /><span class="bold">{$set_extra_field_04}:</span><br />
			<input type="text" value="{$image_extra_04}" name="image_extra_04[{$smarty.section.assets.index}]" class="forminput" style="width:100%;" />
			
			<br /><br /><span class="bold">{$set_extra_field_05}:</span><br />
			<input type="text" value="{$image_extra_05}" name="image_extra_05[{$smarty.section.assets.index}]" class="forminput" style="width:100%;" />
			
		</div>
			
		</td>
		
		</tr>
		
		{/if}
		
	{/section}

</table>

</form>

</div>

</div>

{include_template file="admin.snippets/admin.html.footer.tpl"}
