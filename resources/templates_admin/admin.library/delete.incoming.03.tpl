{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">

<form method="post" action="{$base_url}{$fil_admin_library_import}">

<table class="table-text">

<tr>
<th>The selected image were deleted.</th>

<td>

<h1>Images deleted</h1>

<p>All of the selected images have been permanently deleted from the incoming directory of your server.</p>

</td>
<td>&nbsp;</td>
</tr>

<tr>
<td>&nbsp;</td>
<td><input type="submit" value="Go back to the image import screen" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

</table>

</form>

</div>

</div>

<!-- INCLUDE THE TOP HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}

