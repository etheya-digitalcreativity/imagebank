{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">


<form action="{$base_url}{$fil_admin_library_import}" method="post">

<input type="hidden" name="cmd" value="approveImageMetaData" />

<input type="hidden" name="directory" value="{$directory}" />



<table class="table-text">

<tr>
<th>Use this form to enter any information that you would like applied to the images you are about to import.  You will have the option to amend details for individual images on the next screen.</th>

<td>

<h1>Global image metadata</h1>

<p>Use this form to enter metadata and image specific information that you would like to associate with these file when they are imported into your image library.</p>

{if $img_note=="1" && $uploadtype=="12"}
	
<ul>
<li>{$img_note_status}</li>
</ul>
	
{/if}

</td>
<td>&nbsp;</td>
</tr>

<tr>
<th>&nbsp;</th>
<td><input type="submit" name="submit" value="Skip adding metadata" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>



</table>



<table class="table-form">

<thead>

<tr>
<th colspan="3">Image properties:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>Instructions:</th>
<td>Enter additional information to associate with these images.  If some images already have this information stored in EXIF or IPTC metadata, you will be given the option to choose what you want to use on the next screen.</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Images active:</th>
<td><input type="checkbox" name="image_active" class="forminput" checked="checked" /> (Check to make these images visible)</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Image display title:</th>
<td><input type="text" name="image_title" value="{$image_title}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Image caption text:</th>
<td>
<textarea name="image_caption" style="width:100%;" rows="3" class="forminput" cols="16">{$image_caption}</textarea>
</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>Keywords:</td>
<td><textarea cols="30" rows="3" name="image_keywords" style="width:100%;" class="forminput" cols="16">{$image_keywords}</textarea></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="checkbox" name="image_keywords_append" class="forminput"{if $image_keywords_append || $image_keywords_append == "on"} checked="checked"{/if} /> Append these keywords to existing ones</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Image date:</td>
<td><input type="radio" name="date_option" value="toda" class="forminput" checked="checked" /> {html_select_date prefix="toda_" time=$mysql_format_time start_year="-100" display_days=true all_extra='class="forminput"'}</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Copyright owner (artist or photographer):</th>
<td><input type="text" name="image_copyright" value="{$image_copyright}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

<table class="table-form" border="0" cellpadding="3" cellspacing="0">

<thead>

<tr>
<th colspan="3">Purchase and download options:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>Image default purchase price ({$set_store_currency}):</th>
<td>{$set_store_symbol}<input type="text" name="image_price" length="6" class="forminput" value="{$image_price}" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Is this image for sale (see store control panel):</th>
<td><input type="checkbox" name="image_sale" class="forminput"{if $image_sale || $image_sale == "on"} checked="checked"{/if} /> (Check to activate)</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>Products available for this image:</th>
<td>
<input type="radio" name="image_product_link" value="0" class="forminput" checked="checked" /> All image products<br />
<input type="radio" name="image_product_link" value="1" class="forminput"{if $image_product_link == "1"} checked="checked"{/if} /> Only selected image products:
</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td>
<div style="padding-left:20px;">
{section name="product" loop=$product_id}
<input type="checkbox" name="image_products[]" value="{$product_id[product]}" {section name="enabled" loop=$image_products}{if $image_products[enabled] == $product_id[product]} checked="checked"{/if}{/section} />{$product_name[product]}<br />
{sectionelse}
<b>There are no image products configured for this website.</b>
{/section}
</div>
</td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

<table class="table-form">

<thead>

<tr>
<th colspan="3">Digital rights management and display options:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>Make images visible to site visitors:</th>
<td>
<input type="radio" name="image_permissions" value="10" checked="checked" />All users<br />
<input type="radio" name="image_permissions" value="11" {if $image_permissions == "11"} checked="checked"{/if} />Registered users only<br />
<input type="radio" name="image_permissions"value="12" {if $image_permissions == "12"} checked="checked"{/if} />Selected user groups<br />
</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Select groups who can view these images:</th>
<td>
<select name="image_groups[]" class="forminput" style="width:100%;" size="6" multiple="multiple">
{section name="groups" loop=$group_id}
	<option value="{$group_id[groups]}" {section name="enabled" loop=$image_groups}{if $image_groups[enabled] == $group_id[groups]} selected="selected"{/if}{/section}>{$group_name[groups]}</option>
{/section}
</select>
</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Image usage rights:</th>
<td>
<input type="radio" name="image_rights_type" class="forminput" value="10" checked="checked" /> Royalty free<br />
<input type="radio" name="image_rights_type" class="forminput" value="11" {if $image_rights_type == "11"} checked="checked"{/if} /> Rights managed<br />
</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Rights management information:</th>
<td><textarea cols="30" rows="3" name="image_rights_text" style="width:100%;" class="forminput" cols="16">{$image_keywords}</textarea></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Images have model release:</th>
<td><input type="checkbox" name="image_model_release" class="forminput" {if $image_model_release || $image_model_release == "on"} checked="checked"{/if} /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Images have property release:</th>
<td><input type="checkbox" name="image_property_release" class="forminput" {if $image_property_release || $image_property_release == "on"} checked="checked"{/if} /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

<table class="table-form">

<thead>

<tr>
<th colspan="3">Custom metadata:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>{$set_extra_field_01}:</th>
<td><input type="text" name="image_extra_01" value="{$image_extra_01}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>{$set_extra_field_02}:</th>
<td><input type="text" name="image_extra_02" value="{$image_extra_02}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>{$set_extra_field_03}:</th>
<td><input type="text" name="image_extra_03" value="{$image_extra_03}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>{$set_extra_field_04}:</th>
<td><input type="text" name="image_extra_04" value="{$image_extra_04}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>{$set_extra_field_05}:</th>
<td><input type="text" name="image_extra_05" value="{$image_extra_05}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="submit" name="submit" value="Proceed to next step" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>


</div>

</div>

{include_template file="admin.snippets/admin.html.footer.tpl"}