{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

<table class="table-search">

<thead>

<tr>
<th>2. Select images to import into the library</th>
</tr>

</thead>

<tbody>

<tr>

<td>

<div class="search-box" style="border-right:1px solid #BBB; min-height:180px; width:386px;">

	<p class="title">Import images into your library</p>
	
	<p>This form lists all the images in your incoming directory waiting to be imported into your Pixaria image library so you can display them online.</p>
	
	<p>Select image files to import using the checkboxes on the left of the file list and enter the name of a folder where you would like to store the images and their thumbnails once they are moved into the library directory on your server.</p>

</div>

<div class="search-box" style="border-right:1px solid #BBB; min-height:180px; width:385px;">

	{include_template file="admin.snippets/admin.html.notification.note.tpl" form_title_text="Creating thumbnails from large images can use more than the $sys_memory_limit MB of memory PHP is allowed to use on this server.<br /><br />You may need to manually create thumbnails for images marked with a warning in the list below or else <a href=\"http://www.pixaria.com/wiki/ResizingHighResFailure\">increase the memory allocation</a> for PHP on your web server.  If in doubt, try large images individually rather than as part of a group."}

</div>

</td>

</tr>

</tbody>

</table>

{if ($display=="TRUE") }

	<form method="post" id="image_list" action="{$base_url}{$fil_admin_library_import}" target="_top" onsubmit="return confirmSubmit('Please note that large groups of files, especially high-resolution images, can take some time to process.\n\nAre you sure you want to continue?');">
	
	<table class="table-listbox">

	<tr>
		<th style="padding-left:0px; width:20px;"><input title="Toggle all checkboxes" type="checkbox" name="toggle" onclick="javascript:this.checked ? selectAll('image_list','images[]') : deselectAll('image_list','images[]');" /></th>
		<th>File name</th>	
		<th>File size</th>	
		<th>Last modified</th>	
		<th>Image dimensions</th>	
		<th colspan="2">Memory requirements</th>	
	</tr>
	
	{section name=files loop=$filename}
		
		<tr class="{cycle values='list-one,list-two'}">
		<td style="padding-left:0px; width:20px; height:40px;"><input type="checkbox" border="0" name="images[]" value="{$filename[files]}" class="forminput" /></td>
		<td><img src="{$base_url}resources/images/icons/22x22/image-x-generic.gif" alt="" title="" width="22" height="22" style="vertical-align:middle;" />&nbsp;&nbsp;{$filename[files]|truncate:50}</td>
		<td>{$filesize[files]}kb</td>
		<td>{$filemtime[files]|date_format:"%B %e, %Y"}</td>
		<td>{$width[files]} x {$height[files]}</td>
		<td>{$resizeable[files].1} MB</td>
		<td>{if $resizeable[files].0 == "0"}<ul class="actionlist"><li><a href="javascript:void(0);" onclick="javascript:alert('This image may be too large for Pixaria to resize automatically as it has been estimated that it will probably require {$resizeable[files].1} MB of memory to import and the limit set by your server is {$sys_memory_limit} MB.\n\nPlease proceed with caution or create the thumbnails for this image offline and upload them by FTP.');" class="warningbutton">Important Notice</a></li></ul>{/if}</td>
		</tr>
	
	{/section}
	
	</table>

	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">Options for these image files:</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Specify a directory to store the images in</th>
	<td>
	
	If you wish, you can specify the name of a directory in which to store these images when the thumbnails are generated and they are moved into your library.<br /><br />Please only use letters, numbers, hyphen '-' and underscore '_' only.  If you do not supply a name, Pixaria will create one for you.<br /><br />
	
	{if $show_directories}
	
	<select name="directory" onchange="javascript:this.form.directory_name.value=this.value;" style="width:100%;" class="forminput">
	<option value="">Choose existing folder in the 'incoming' directory</option>
	<option value="">------------------------------</option>
	{section name="inc" loop=$incoming}
	<option value="{$incoming[inc]}">{$incoming[inc]}</option>
	{/section}
	<option value=""></option>
	<option value="">Choose existing folder in the 'library' directory</option>
	<option value="">------------------------------</option>
	{section name="lib" loop=$library}
	<option value="{$library[lib]}">{$library[lib]}</option>
	{/section}
	</select><br />
	
	{/if}
	
	<input type="text" name="directory_name" value="" style="width:100%;" class="forminput" /></td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>Choose required action:</th>
	<td>
	<input type="radio" name="cmd" value="directoryFromIncoming" checked="checked" /> Import selected files into the library<br />
	<input type="radio" name="cmd" value="formDeleteImageFiles" /> Delete selected files from the server
	</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td><input type="submit" value="Proceed to the next step" style="width:100%;" class="formbutton" /></td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
	</table>
			
	</form>

{else}

	<table class="table-form">
	
	<thead>
	
	<tr>
		<th colspan="3">No images in selected source.</th>	
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two'}">
	
		<th>&nbsp;</th>
		
		<td>
		There are no images to view in this selection.
		
		<ol>
		
		<li>'Comping images' view lists images {$sys_comping_size} x {$sys_comping_size} pixels or smaller in the /resources/incoming/ directory.</li>
		<li>'High-res images' view lists images larger than {$sys_comping_size} x {$sys_comping_size} pixels in the /resources/incoming/ directory.</li>
		<li>'Complete directories' view lists images uploaded in the standard Pixaria directory format (see <a href="http://www.pixaria.com/wiki/">documentation</a>).</li>
		
		</ol>
		
		</td>
		
		<td>&nbsp;</td>
		
	</tr>
	
	</tbody>
	
	</table>
	
{/if}
