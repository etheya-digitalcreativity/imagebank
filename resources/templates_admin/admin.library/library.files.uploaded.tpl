{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">

<table class="table-text">

<tr>
<th>Use this form to begin the image import process.  Once you've completed this, visitors to your website will be able to see the images you've uploaded in your library.</th>

<td>

<p class="title">The images you've uploaded</p>

<p>To import images, select the files you wish to import using the checkboxes in the left of the file list, choose import preferences and proceed to the next step.</p>

{include_template file="admin.snippets/admin.html.notification.note.tpl" form_title_text="Any images you don't import at this stage will remain in the 'incoming' directory of your website and you will be able to import them at a later date."}

	{include_template file="admin.snippets/admin.html.notification.warning.tpl" form_title_text="Creating thumbnails from large images can use more than the $sys_memory_limit MB of memory PHP is allowed to use on this server.<br /><br />You may need to manually create thumbnails for images marked with a warning in the list below.  If in doubt, try large images individually rather than as part of a group."}

</td>
<td>&nbsp;</td>
</tr>

</table>

	<form method="post" id="image_list" action="{$base_url}{$fil_admin_library_import}" target="_top"{if ($cmd=="gall") } onsubmit="return confirmSubmit('Please note that large groups of files, especially high-resolution images, can take some time to process.  Are you sure you want to continue?');"{/if}>
	
	<table class="table-listbox">

	<tr>
		<th style="padding-left:0px; width:20px;"><input title="Toggle all checkboxes" type="checkbox" name="toggle" onclick="javascript:this.checked ? selectAll('image_list','images[]') : deselectAll('image_list','images[]')" checked="checked" /></th>
		<th>File name</th>	
		<th>File size</th>	
		<th>Last modified</th>	
		<th>Image Dimensions</th>
		<th colspan="2">Memory requirements</th>	
	</tr>
	
	{section name=files loop=$filename}
		
		<tr class="{cycle values='list-one,list-two'}">
		<td style="padding-left:0px; width:20px; height:30px"><input type="checkbox" name="images[]" value="{$filename[files]}" class="forminput" checked="checked" /></td>
		<td><a href="javascript:imagePreviewWindow('{$base_url}{$fil_admin_image_output}?html={$filepath[files]}&amp;','{$width[files]}','{$height[files]}');" id="trigger_{$smarty.section.files.index+1}" onmouseover="var my_tooltip_{$smarty.section.files.index+1} = new Tooltip('trigger_{$smarty.section.files.index+1}', 'tooltip_{$smarty.section.files.index+1}')"><img src="{$base_url}resources/images/icons/22x22/image-x-generic.gif" alt="" title="" width="22" height="22" style="vertical-align:middle;" border="0" />&nbsp;&nbsp;{$filename[files]}</a></td>
		<td>{$filesize[files]}kb</td>
		<td>{$filemtime[files]|date_format:"%B %e, %Y"}</td>
		<td>{$width[files]} pixels x {$height[files]} pixels
		
		<div id="tooltip_{$smarty.section.files.index+1}" style="display:none; padding: 5px; background-color: #FFF;">
		<img src="{$base_url}{$fil_admin_image_output}?file={$filepath[files]}&amp;" width="{$width[files]/3}" height="{$height[files]/3}" />
		</div>

		</td>
		
		<td>{$resizeable[files].1} MB</td>
		<td>{if $resizeable[files].0 == "0"}<ul class="actionlist"><li><a href="javascript:void(0);" onclick="javascript:alert('This image may be too large for Pixaria to resize as it will require {$resizeable[files].1} MB of memory to import and the limit set by your server is {$sys_memory_limit} MB.\n\nPlease proceed with caution or create the thumbnails for this image offline and upload them by FTP.');" style="background:#AB1313; color:#FFF; font-weight:bold;">WARNING</a></li></ul>{/if}</td>
		</tr>
	
	{/section}
	
		
	</table>
	



	
	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">Options for these image files:</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Specify a directory to store the images in</th>
	<td>
	
	If you wish, you can specify the name of a directory in which to store these images when the thumbnails are generated and they are moved into your library.<br /><br />Please only use letters, numbers, hyphen '-' and underscore '_' only.  If you do not supply a name, Pixaria will create one for you.<br /><br />
	
	{if $show_directories}
	
	<select name="directory" onchange="javascript:this.form.directory_name.value=this.value;" style="width:100%;" class="forminput">
	<option value="">Choose existing folder in the 'incoming' directory</option>
	<option value="">------------------------------</option>
	{section name="inc" loop=$incoming}
	<option value="{$incoming[inc]}">{$incoming[inc]}</option>
	{/section}
	<option value=""></option>
	<option value="">Choose existing folder in the 'library' directory</option>
	<option value="">------------------------------</option>
	{section name="lib" loop=$library}
	<option value="{$library[lib]}">{$library[lib]}</option>
	{/section}
	</select><br />
	
	{/if}
	
	<input type="text" name="directory_name" value="" style="width:100%;" class="forminput" /></td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>Choose required action:</th>
	<td>
	<input type="radio" name="cmd" value="directoryFromIncoming" checked="checked" /> Import selected files into the library<br />
	<input type="radio" name="cmd" value="formDeleteImageFiles" /> Delete selected files from the server
	</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td><input type="submit" value="Proceed to the next step" style="width:100%;" class="formbutton" /></td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	</tbody>
	
	</table>
		
	</form>

</div>

</div>

{include_template file="admin.snippets/admin.html.footer.tpl"}