{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">




<form method="post" action="{$base_url}{$fil_admin_library_import}" onsubmit="new Ajax.Updater('forms', '{$base_url}{$fil_admin_library_import}', {literal}{asynchronous:true, parameters:Form.serialize(this)}{/literal}); return false;" name="selector" id="selector">

<input type="hidden" name="cmd" value="6" />
	
<input type="hidden" name="image_path" value="{$directory}" />
	
<table class="table-search" id="step1">

<thead>

<tr>
<th>1. Choose the action you wish to perform</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<td>


<div class="search-box" style="border-right:1px solid #BBB; min-height:130px;">

<b>a) Import images from incoming directory</b>

<p>Use this option to import images in the 'incoming directory' of your website to the Pixaria library database.</p>

<input type="button" name="button" value="Import images" class="formbutton" onclick="new Ajax.Updater('forms', '{$base_url}{$fil_admin_library_import}?cmd=importFromIncoming&action=2', {literal}{asynchronous:true, parameters:Form.serialize(this)}{/literal}); return true;" style="width:100%;" />

</div>

<div class="search-box" style="border-right:1px solid #BBB; min-height:130px;">

<b>b) Import complete directories of images</b>

<p>Use this option to import complete directories of images and thumbnails in your 'incoming images' directory.</p>

<input type="button" name="button" value="Import directories of images" class="formbutton" onclick="new Ajax.Updater('forms', '{$base_url}{$fil_admin_library_import}?cmd=importDirectories&action=2', {literal}{asynchronous:true, parameters:Form.serialize(this)}{/literal}); return true;" style="width:100%;" />

</div>

</td>
</tr>

</tbody>

</table>

</form>



<div id="forms">

</div>

<script language="javascript" type="text/javascript">
// <![CDATA[
	{literal}
	
	function createMultiSelector() {
		var multi_selector = new MultiSelector( document.getElementById( 'files_list' ), 5 );
		multi_selector.addElement( document.getElementById( 'get_file[]' ) );
	}

	function updateFileBrowser(url) {
		Effect.toggle('processing','appear',{duration:0.3});
		var ajax = new Ajax.Updater('forms',url,{method:'get',asynchronous:true,onComplete: showResponse});
	}
	
	function showResponse () {
		$('filebrowser').innerHTML = req.responseText;
		Effect.toggle('processing','appear',{duration:0.3});
	}
	{/literal}
// ]]>
</script>

</div>

</div>

{include_template file="admin.snippets/admin.html.footer.tpl"}
