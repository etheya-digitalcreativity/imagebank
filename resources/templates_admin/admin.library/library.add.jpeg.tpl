
<table class="table-search">

<thead>

<tr>
<th>2. Upload images</th>
</tr>

</thead>

<tbody>

<tr>

<td>

<div class="search-box" style="border-right:1px solid #BBB; min-height:120px; width:386px;">

<p class="title">Upload individual JPEG images</p>
	
<p>The maximum allowed file upload size for JPEG images in Pixaria Gallery is {$sys_memory_limit} megabytes.  To increase this limit, you will need to raise PHP's script <i>memory usage</i> and <i>upload file size</i> limit in the php.ini configuration file for your web server.</p>

</div>

<div class="search-box" style="border-right:1px solid #BBB; min-height:120px; width:385px;">

	{include_template file="admin.snippets/admin.html.notification.note.tpl" form_title_text="If you get any errors when importing or uploading high resolution images, please consult the troubleshooting pages of the documentation as this may represent a problem with the way PHP is configured on your web server."}

</div>

</td>

</tr>

</tbody>

</table>




<form enctype="multipart/form-data" action="{$base_url}{$fil_admin_library_import}" method="post">

<input type="hidden" name="cmd" value="processJpeg" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">Select individual JPEG image files to upload</th>
</tr>

</thead>

<tbody>

{if $set_gd_upload}

	<tr class="list-one">
	<th>Select a JPEG image file to upload:</th>
	<td><input type="file" name="img_high" id="get_file[]" /></td>
	<td>&nbsp;</td>
	</tr>
	
{/if}

<tr class="list-two">
<th>Your selected files:</th>
<td><div id="files_message">Selected files will be listed here</div><table><tbody id="files_list"></tbody></table></td>
<td>&nbsp;</td>
</tr>

<tr class="list-two">
<th>&nbsp;</th>
<td><input type="submit" name="submit" value="Upload and process files" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>
