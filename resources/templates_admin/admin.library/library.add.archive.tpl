<table class="table-search">

<thead>

<tr>
<th>2. Upload an archive of images</th>
</tr>

</thead>

<tbody>

<tr>

<td>

<div class="search-box" style="border-right:1px solid #BBB; min-height:120px; width:386px;">

<p class="title">Upload an archive of images</p>
	
<p>The maximum allowed file upload size for an archive in Pixaria Gallery is {$sys_memory_limit} megabytes.  To increase this limit, you will need to raise PHP's script <i>memory usage</i> and <i>upload file size</i> limit in the php.ini configuration file for your web server.</p>


</div>

<div class="search-box" style="border-right:1px solid #BBB; min-height:120px; width:385px;">

{include_template file="admin.snippets/admin.html.notification.note.tpl" form_title_text="If you get any errors when importing or uploading high resolution images, please consult the troubleshooting pages of the documentation as this may represent a problem with the way PHP is configured on your web server."}
	
</div>

</td>

</tr>

</tbody>

</table>



<form enctype="multipart/form-data" action="{$base_url}{$fil_admin_library_import}" method="post">

<input type="hidden" name="cmd" value="processArchive" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">Upload a zip, tar or gz archive of images</th>
</tr>

</thead>

<tbody>

{if $set_gd_upload}

	<tr class="list-one">
	<th>Select an archive to upload:</th>
	<td>Please note that the archive you upload must only contain images (no folders or directories).<br /><br /><input type="file" name="file_archive" /></td>
	<td>&nbsp;</td>
	</tr>
	
{/if}

<tr class="list-one">
<th>&nbsp;</th>
<td><input type="submit" name="submit" value="Upload and process archive" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>

