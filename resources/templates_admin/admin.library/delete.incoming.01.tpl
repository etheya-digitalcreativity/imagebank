{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">

<form method="post" action="{$base_url}{$fil_admin_library_import}">

<input type="hidden" name="cmd" value="confirmDeleteFromIncoming" />

<table class="table-text">

<tr>
<th>Are you sure you want to delete these images from your incoming FTP directory?</th>

<td>

<h1>Warning!</h1>

<p>Deleting these images cannot be undone.  Only proceed if you are sure that you want to permanently remove these image files from your server.</p>

</td>
<td>&nbsp;</td>
</tr>

</table>
		
{if ($display=="TRUE") }

	<table class="table-listbox">
	
	<tr>
		<th colspan="2">File name</th>	
		<th>File size (kilobytes)</th>	
		<th>Image dimensions (pixels)</th>
	</tr>
	
	{section name=files loop=$filename}
		
		<tr class="{cycle values='list-one,list-two'}">
		<td style="width:20px;"><a href="javascript:openPop('{$base_url}{$fil_admin_image_output}?html={$filepath[files]}&amp;','{$width[files]-2}','{$height[files]-2}','no','no','no','no','no','no','no');"><img src="{$base_url}resources/images/icons.inline/magnify.gif" title="Magnify Image" alt="Magnify Image" width="16" height="16" border="0" /></a></td>
		<td align="left"><a href="javascript:openPop('{$base_url}{$fil_admin_image_output}?html={$filepath[files]}&amp;','{$width[files]-2}','{$height[files]-2}','no','no','no','no','no','no','no');">{$filename[files]}</a></td>
		<td>{$filesize[files]}kb</td>
		<td >{$width[files]} pixels x {$height[files]} pixels <input type="hidden" name="images[]" value="{$filename[files]}" /></td>
		</tr>
	
	{/section}
	
		
	</table>
	
	<table class="table-text" border="0" cellpadding="3" cellspacing="0">
	
	<tr>
	<th>&nbsp;</th>
	<td><input type="submit" value="Delete these images" style="width:100%;" class="forminput" /></td>
	<td>&nbsp;</td>
	</tr>
	
	</table>

</form>

{else}

	<table class="table-text">
	
	<tr>
	<th>You did not select any image files to delete</th>
	<td>
	
	<h1>No images selected</h1>
	
	<p>You chose the option to delete some images from your incoming directory but didn't select any image files.  Please go back and try again.</p>
	
	<input type="button" value="Go back to previous page" onclick="javascript:history.go(-1);" style="width:100%;" class="forminput" />
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	</table>

{/if}

</div>

</div>

<!-- INCLUDE THE TOP HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}

