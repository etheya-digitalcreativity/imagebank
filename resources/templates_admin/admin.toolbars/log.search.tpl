<div id="toolbar-tools">

	<div id="toolbar-title">Search keyword logging tools</div>
	<div id="toolbar-body">
	
	<table width="250">
	
		<tr>
		
		<td><a href="{$base_url}{$fil_admin_log_keywords}?show=inc"><img src="{$base_url}resources/images/toolbar/log-search.png" title="Show keywords that users wanted to include in their searches" alt="Show keywords that users wanted to include in their searches" width="60" height="60" border="0" /></a></td>
		
		<td><a href="{$base_url}{$fil_admin_log_keywords}?cmd=clearSearchLog" onclick="return confirmSubmit('You are about to empty the search keyword log for this website.  Are you sure you want to continue?');"><img src="{$base_url}resources/images/toolbar/log-search-trash.png" title="Delete all the entries in the keyword search log" alt="Delete all the entries in the keyword search log" width="60" height="60" border="0" /></a></td>
		
		</tr>
		
		<tr>
			
		<td style="width:50%;"><a href="{$base_url}{$fil_admin_log_keywords}?show=inc" title="Show keywords that users wanted to include in their searches" class="plain">Show keyword<br />search log</a></td>
		
		<td style="width:50%;"><a href="{$base_url}{$fil_admin_log_keywords}?cmd=clearSearchLog" title="Delete all the entries in the keyword search log" onclick="return confirmSubmit('You are about to empty the search keyword log for this website.  Are you sure you want to continue?');" class="plain">Empty the<br />search log</a></td>
		
		</tr>
		
	</table>
	
	</div>
	
</div>