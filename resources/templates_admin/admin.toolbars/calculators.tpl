<div id="toolbar-tools">

	<div id="toolbar-title">Related pricing management control panels</div>
	<div id="toolbar-body">
	
	<table width="300">
	
		<tr>
		
		<td style="text-align:center; width:100px;"><a href="{$base_url}{$fil_admin_calculators}"><img src="{$base_url}resources/images/toolbar/calculator-edit.png" title="Edit rules and rule options" alt="Edit rules and rule options" width="60" height="60" border="0" /></a></td>
		
		<td style="text-align:center; width:100px;"><a href="{$base_url}{$fil_admin_calculators}?view=sort"><img src="{$base_url}resources/images/toolbar/calculator-reorder.png" title="Change the order rules are processed" alt="Change the order rules are processed" width="60" height="60" border="0" /></a></td>
		
		<td style="text-align:center; width:100px;"><a href="{$base_url}{$fil_admin_calculators}?cmd=showFormPriceRuleTest"><img src="{$base_url}resources/images/toolbar/calculator-test.png" title="Test the price calculation rules" alt="Test the price calculation rules" width="60" height="60" border="0" /></a></td>
		
		</tr>
		
		<tr>
		
		<td style="text-align:center; width:100px;"><a href="{$base_url}{$fil_admin_calculators}" class="plain" title="Edit rules and rule options">List rules<br /> and options</a></td>
		
		<td style="text-align:center; width:100px;"><a href="{$base_url}{$fil_admin_calculators}?view=sort" class="plain" title="Change the order rules are processed">Change rule<br />order</a></td>
		
		<td style="text-align:center; width:100px;"><a href="{$base_url}{$fil_admin_calculators}?cmd=showFormPriceRuleTest" class="plain" title="Test the price calculation rules">Test price<br />rules</a></td>
		
		</tr>
		
	</table>
	
	</div>
	
</div>