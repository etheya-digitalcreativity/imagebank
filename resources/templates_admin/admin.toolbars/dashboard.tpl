{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

<div id="toolbar-tools">

	<div id="toolbar-title">Quick Start Links</div>
	
	<div id="toolbar-body">

		<table width="830">
	
		<tr>
		
		<td><a href="{$base_url}{$fil_admin_upload}"><img src="{$base_url}resources/images/toolbar/image-upload.png" title="Import new images into Pixaria" alt="Import new images into Pixaria" width="60" height="60" border="0" /></a></td>
	
		<td><a href="{$base_url}{$fil_admin_gallery}"><img src="{$base_url}resources/images/toolbar/galleries-hierarchical.png" title="List galleries hierarchically" alt="List galleries hierarchically" width="60" height="60" border="0" /></a></td>
	
		<td><a href="{$base_url}{$fil_admin_image}"><img src="{$base_url}resources/images/toolbar/orphan-images.png" title="List all images not in a gallery" alt="List all images not in a gallery" width="60" height="60" border="0" /></a></td>
		
		<td><a href="{$base_url}{$fil_admin_preferences}"><img src="{$base_url}resources/images/toolbar/preferences.png" title="Edit personal preferences and settings" alt="Edit personal preferences and settings" width="60" height="60" border="0" /></a></td>
		
		
		<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
		
		
		<td><a href="{$base_url}{$fil_admin_store}"><img src="{$base_url}resources/images/toolbar/settings-store.png" title="E-Commerce settings" alt="" width="60" height="60" border="0" /></a></td>
	
		<td><a href="{$base_url}{$fil_admin_store_sales}" title="View all transactions in the database"><img src="{$base_url}resources/images/toolbar/sales-list.png" alt="" width="60" height="60" border="0" /></a></td>
	
		<td><a href="{$base_url}{$fil_admin_store}?cmd=listImageDownloads" title="View high-resolution image download logs"><img src="{$base_url}resources/images/toolbar/sales-downloads.png" alt="" width="60" height="60" border="0" /></a></td>
		
		<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
		
		<td><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}"><img src="{$base_url}resources/images/toolbar/users-list.png" title="List all registered users of this site" alt="List all registered users of this site" width="60" height="60" border="0" /></a></td>
		
		</tr>
		
		<tr>
		
		<td style="width:12%;"><a href="{$base_url}{$fil_admin_upload}" class="plain">Upload new<br />images</a></td>
	
		<td style="width:12%;"><a href="{$base_url}{$fil_admin_gallery}" class="plain">List all<br />Galleries</a></td>
		
		<td style="width:12%;"><a href="{$base_url}{$fil_admin_image}" class="plain">Show orphan<br />images</a></td>
		
		<td style="width:12%;"><a href="{$base_url}{$fil_admin_preferences}" class="plain">My personal<br />preferences</a></td>
		
		<td style="width:12%;"><a href="{$base_url}{$fil_admin_store}" class="plain">Store<br />settings</a></td>
		
		<td style="width:12%;"><a href="{$base_url}{$fil_admin_store_sales}" title="View all transactions in the database" class="plain">Show all<br />purchases</a></td>
		
		<td style="width:12%;"><a href="{$base_url}{$fil_admin_store}?cmd=listImageDownloads" title="View high-resolution image download logs" class="plain">Image download<br />reports</a></td>
		
		<td style="width:12%;"><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}" class="plain">Show registered<br />users</a></td>
	
		</tr>
	
		</table>
	
	</div>

</div>