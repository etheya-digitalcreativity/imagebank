{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

<div id="toolbar-tools">

	<div id="toolbar-title">Settings Control Panels</div>
	
	<div id="toolbar-body">
	
		<table width="860">
		
			<tr>
			
			<td><a href="{$base_url}{$fil_admin_preferences}"><img src="{$base_url}resources/images/toolbar/preferences.png" title="Edit personal preferences and settings" alt="Edit personal preferences and settings" width="60" height="60" border="0" /></a></td>
		
			<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
			
			<td><a href="{$base_url}{$fil_admin_settings}"><img src="{$base_url}resources/images/toolbar/settings-general.png" title="General settings" alt="" width="60" height="60" border="0" /></a></td>
			
			<td><a href="{$base_url}{$fil_admin_settings_search}"><img src="{$base_url}resources/images/toolbar/settings-search.png" title="Search settings" alt="" width="60" height="60" border="0" /></a></td>
			
			<td><a href="{$base_url}{$fil_admin_appearance}"><img src="{$base_url}resources/images/toolbar/settings-appearance.png" title="Appearance and behaviour settings" alt="" width="60" height="60" border="0" /></a></td>
			
			<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
			
			<td><a href="{$base_url}{$fil_admin_settings}?cmd=login"><img src="{$base_url}resources/images/toolbar/settings-security.png" title="User login and registration settings" alt="" width="60" height="60" border="0" /></a></td>
			
			<td><a href="{$base_url}{$fil_admin_settings}?cmd=regdata"><img src="{$base_url}resources/images/toolbar/settings-registration.png" title="Registration and data capture settings" alt="" width="60" height="60" border="0" /></a></td>
			
			<td><a href="{$base_url}{$fil_admin_settings}?cmd=regban"><img src="{$base_url}resources/images/toolbar/settings-regban.png" title="User registration banning" alt="" width="60" height="60" border="0" /></a></td>
			
			<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
			
			<td><a href="{$base_url}{$fil_admin_store}"><img src="{$base_url}resources/images/toolbar/settings-store.png" title="E-Commerce settings" alt="" width="60" height="60" border="0" /></a></td>
			
			</tr>
		
			<tr>
			
			<td style="width:12%;"><a href="{$base_url}{$fil_admin_preferences}" class="plain">My personal<br />preferences</a></td>
			
			<td style="width:12%;"><a href="{$base_url}{$fil_admin_settings}" class="plain">General<br />settings</a></td>
			
			<td style="width:12%;"><a href="{$base_url}{$fil_admin_settings_search}" class="plain">Search<br />settings</a></td>
			
			<td style="width:12%;"><a href="{$base_url}{$fil_admin_appearance}" class="plain">Apperarance and<br />behaviour</a></td>
			
			<td style="width:12%;"><a href="{$base_url}{$fil_admin_settings}?cmd=login" class="plain">Login and<br />registration</a></td>
			
			<td style="width:12%;"><a href="{$base_url}{$fil_admin_settings}?cmd=regdata" class="plain">Registration data<br />capture</a></td>
			
			<td style="width:12%;"><a href="{$base_url}{$fil_admin_settings}?cmd=regban" class="plain">Registration<br />blocking</a></td>
			
			<td style="width:12%;"><a href="{$base_url}{$fil_admin_store}" class="plain">Store<br />settings</a></td>
			
			</tr>
			
		</table>
	
	</div>

</div>