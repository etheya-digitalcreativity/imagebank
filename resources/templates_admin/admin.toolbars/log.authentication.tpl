<div id="toolbar-tools">

	<div id="toolbar-title">Related authentication log tools</div>
	
	<div id="toolbar-body">
	
	<table width="410">
	
		<tr>
		
		<td><a href="{$base_url}{$fil_admin_log_login}"><img src="{$base_url}resources/images/toolbar/log-authentication.png" title="Show last 50 logins" alt="Show last 50 logins" width="60" height="60" border="0" /></a></td>
		
		<td><a href="{$base_url}{$fil_admin_log_login}?show=fail"><img src="{$base_url}resources/images/toolbar/log-authentication-failed.png" title="Show failed logins" alt="Show failed logins" width="60" height="60" border="0" /></a></td>
		
		<td><a href="{$base_url}{$fil_admin_log_login}?show=ano"><img src="{$base_url}resources/images/toolbar/log-authentication-anomalous.png" title="Show logins from multiple IP addresses" alt="Show logins from multiple IP addresses" width="60" height="60" border="0" /></a></td>
		
		<td><a href="{$base_url}{$fil_admin_log_login}?cmd=1" onclick="return confirmSubmit('You are about to empty the user login log for this website.  Are you sure you want to continue?');"><img src="{$base_url}resources/images/toolbar/log-authentication-trash.png" title="Delete all the entries in the user login log" alt="Delete all the entries in the keyword search log" width="60" height="60" border="0" /></a></td>
		
		</tr>
		
		<tr>
		
		<td style="width:25%;"><a href="{$base_url}{$fil_admin_log_login}" title="Show last 50 logins" class="plain">Most recent<br />logins</a></td>
		
		<td style="width:25%;"><a href="{$base_url}{$fil_admin_log_login}?show=fail" title="Show failed logins" class="plain">Show failed<br />logins</a></td>
		
		<td style="width:25%;"><a href="{$base_url}{$fil_admin_log_login}?show=ano" title="Show logins from multiple IP addresses" class="plain">Show anomalous<br />logins</a></td>
		
		<td style="width:25%;"><a href="{$base_url}{$fil_admin_log_login}?cmd=1" title="Delete all the entries in the user login log" onclick="return confirmSubmit('You are about to empty the user login log for this website.  Are you sure you want to continue?');" class="plain">Clear the login log</a></td>
		
		</tr>
		
	</table>
	
	</div>
	
</div>