<div id="toolbar-tools">

	<div id="toolbar-title">Maintenance tools</div>
	<div id="toolbar-body">
	
	<table width="450">
	
		<tr>
		
		<td><a href="{$base_url}{$fil_admin_diagnostics}"><img src="{$base_url}resources/images/toolbar/tools-diagnostics.png" title="View details of your Pixaria installtion" alt="" width="60" height="60" border="0" /></a></td>
	
		<td><a href="{$base_url}{$fil_admin_software}"><img src="{$base_url}resources/images/toolbar/tools-status.png" title="View details of your server configuration" alt="" width="60" height="60" border="0" /></a></td>
	
		<td><a href="{$base_url}{$fil_admin_pixie}"><img src="{$base_url}resources/images/toolbar/tools-plugins.png" title="View details of installed plug-ins" alt="" width="60" height="60" border="0" /></a></td>
	
		<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
	
		</tr>
		
		<tr>
	
		<td style="width:25%;"><a href="{$base_url}{$fil_admin_diagnostics}" class="plain">Pixaria<br />Diagnostics</a></td>
	
		<td style="width:25%;"><a href="{$base_url}{$fil_admin_software}" class="plain">Pixaria<br />Server Status</a></td>
	
		<td style="width:25%;"><a href="{$base_url}{$fil_admin_pixie}" class="plain">Pixaria<br />Plug-in Manager</a></td>
	
		</tr>
		
	</table>
	
	</div>

</div>