{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

<div id="toolbar-tools">

	<div id="toolbar-title">Related tools to help you view and manage your library</div>
	
	<div id="toolbar-body">
	
	<table cellspacing="0" cellpadding="2" border="0" width="900">
	
		<tr>
	
		<td><a href="{$base_url}{$fil_admin_upload}"><img src="{$base_url}resources/images/toolbar/image-upload.png" title="Upload new images to your server" alt="Upload new images to your server" width="60" height="60" border="0" /></a></td>
		
		<td><a href="{$base_url}{$fil_admin_import}"><img src="{$base_url}resources/images/toolbar/image-import.png" title="Import new images into Pixaria" alt="Import new images into Pixaria" width="60" height="60" border="0" /></a></td>
		
		<td><a href="{$base_url}{$fil_admin_jclient}"><img src="{$base_url}resources/images/toolbar/image-upload-java.png" title="Upload images by FTP using the JFileUpload applet" alt="Upload images by FTP using the JFileUpload applet" width="60" height="60" border="0" /></a></td>
	
		<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
		
		<td><a href="{$base_url}{$fil_admin_image}"><img src="{$base_url}resources/images/toolbar/orphan-images.png" title="List all images not in a gallery" alt="List all images not in a gallery" width="60" height="60" border="0" /></a></td>
	
		<td><a href="{$base_url}{$fil_admin_gallery}?cmd=formCreateNewGallery"><img src="{$base_url}resources/images/toolbar/galleries-create.png" title="Create a new gallery" alt="Create a new gallery" width="60" height="60" border="0" /></a></td>
	
		<td><a href="{$base_url}{$fil_admin_gallery}"><img src="{$base_url}resources/images/toolbar/galleries-hierarchical.png" title="List galleries hierarchically" alt="List galleries hierarchically" width="60" height="60" border="0" /></a></td>
	
		<td><a href="{$base_url}{$fil_admin_gallery}?cmd=galleryList"><img src="{$base_url}resources/images/toolbar/galleries-alphabetical.png" title="List galleries alphabetically" alt="List galleries alphabetically" width="60" height="60" border="0" /></a></td>
	
		<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
		
		<td><a href="{$base_url}{$fil_admin_library_search}"><img src="{$base_url}resources/images/toolbar/library-search.png" title="Search for images in your library" alt="Search for images in your library" width="60" height="60" border="0" /></a></td>
	
		</tr>
		
		<tr>
	
		<td><a href="{$base_url}{$fil_admin_upload}" class="plain">Upload new<br />images</a></td>
			
		<td><a href="{$base_url}{$fil_admin_import}" class="plain">Import new<br />images</a></td>
			
		<td><a href="{$base_url}{$fil_admin_jclient}" class="plain">Java Upload<br />with resize</a></td>
			
		<td><a href="{$base_url}{$fil_admin_image}" class="plain">Images not in<br />galleries</a></td>
	
		<td><a href="{$base_url}{$fil_admin_gallery}?cmd=formCreateNewGallery" class="plain">Create new<br />gallery</a></td>
		
		<td><a href="{$base_url}{$fil_admin_gallery}" class="plain">Hierarchical<br />Gallery List</a></td>
	
		<td><a href="{$base_url}{$fil_admin_gallery}?cmd=galleryList" class="plain">Alphabetical<br />Gallery List</a></td>
	
		<td><a href="{$base_url}{$fil_admin_library_search}" class="plain">Search your<br />Library</a></td>
	
		</tr>
		
	</table>
	
	</div>

</div>