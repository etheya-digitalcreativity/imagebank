<div id="toolbar-tools">

	<div id="toolbar-title">Related publishing tools</div>

	<div id="toolbar-body">

	<table cellspacing="0" cellpadding="2" border="0" width="210">
	
		<tr>
		
		<td style="text-align:center;"><a href="{$base_url}{$fil_admin_blog}"><img src="{$base_url}resources/images/toolbar/publishing-list.png" title="Show all articles" alt="Show all articles" width="60" height="60" border="0" /></a></td>
		
		<td style="text-align:center;"><a href="{$base_url}{$fil_admin_blog}?cmd=showFormEntryCreate"><img src="{$base_url}resources/images/toolbar/publishing-new.png" title="Add a new article" alt="Add a new article" width="60" height="60" border="0" /></a></td>
		
		</tr>
		
		<tr>
		
		<td><a href="{$base_url}{$fil_admin_blog}" class="plain" title="Show all articles">List all<br />articles</a></td>
		
		<td><a href="{$base_url}{$fil_admin_blog}?cmd=showFormEntryCreate" class="plain" title="Add a new article">Create new<br />article</a></td>
		
		</tr>
		
	</table>
	
</div>