<div id="toolbar-tools">

	<div id="toolbar-title">Related control panel views</div>
	<div id="toolbar-body">
	
	<table width="830">
	
		<tr>
		
		<td><a href="{$base_url}{$fil_admin_store_sales}" title="View all transactions in the database"><img src="{$base_url}resources/images/toolbar/sales-list.png" alt="" width="60" height="60" border="0" /></a></td>
	
		<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
	
		<td><a href="{$base_url}{$fil_admin_store_sales}&amp;status=1" title="View open shopping carts for all users"><img src="{$base_url}resources/images/toolbar/sales-list-active.png" alt="" width="60" height="60" border="0" /></a></td>
		
		<td><a href="{$base_url}{$fil_admin_store_sales}&amp;status=2" title="View pending transactions"><img src="{$base_url}resources/images/toolbar/sales-list-quotation.png" alt="" width="60" height="60" border="0" /></a></td>
	
		<td><a href="{$base_url}{$fil_admin_store_sales}&amp;status=3" title="Transactions awaiting payment"><img src="{$base_url}resources/images/toolbar/sales-list-payment.png" alt="" width="60" height="60" border="0" /></a></td>
		
		<td><a href="{$base_url}{$fil_admin_store_sales}&amp;status=4" title="View completed transactions"><img src="{$base_url}resources/images/toolbar/sales-list-complete.png" alt="" width="60" height="60" border="0" /></a></td>
		
		<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
	
		<td><a href="{$base_url}{$fil_admin_store}?cmd=listImageDownloads" title="View high-resolution image download logs"><img src="{$base_url}resources/images/toolbar/sales-downloads.png" alt="" width="60" height="60" border="0" /></a></td>
		
		<td><a href="{$base_url}{$fil_admin_store_report}" title="Export transaction reports"><img src="{$base_url}resources/images/toolbar/sales-report.png" alt="" width="60" height="60" border="0" /></a></td>
		
		<td><a href="{$base_url}{$fil_admin_store}?cmd=listPhotographerSales" title="List sales by photographer users"><img src="{$base_url}resources/images/toolbar/sales-report-users.png" alt="" width="60" height="60" border="0" /></a></td>
		
		</tr>
		
		<tr>
		
		<td><a href="{$base_url}{$fil_admin_store_sales}" title="View all transactions in the database" class="plain">Show all<br />purchases</a></td>
		
		<td><a href="{$base_url}{$fil_admin_store_sales}&amp;status=1" title="View open shopping carts for all users" class="plain">Show active<br />carts</a></td>
	
		<td><a href="{$base_url}{$fil_admin_store_sales}&amp;status=2" title="View pending transactions" class="plain">Sales awaiting<br />quotation</a></td>
		
		<td><a href="{$base_url}{$fil_admin_store_sales}&amp;status=3" title="View completed transactions" class="plain">Sales awaiting<br />Payment</a></td>
		
		<td><a href="{$base_url}{$fil_admin_store_sales}&amp;status=4" title="View completed transactions" class="plain">All completed<br />sales</a></td>
	
		<td><a href="{$base_url}{$fil_admin_store}?cmd=listImageDownloads" title="View high-resolution image download logs" class="plain">Image download<br />reports</a></td>
	
		<td><a href="{$base_url}{$fil_admin_store_report}" title="Export transaction reports" class="plain">Transaction<br />reporting</a></td>
	
		<td><a href="{$base_url}{$fil_admin_store}?cmd=listPhotographerSales" title="List sales by photographer users" class="plain">Sales by<br />photographers</a></td>
	
		</tr>
		
	</table>
	
	</div>

</div>