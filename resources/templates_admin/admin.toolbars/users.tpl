{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

<div id="toolbar-tools">

	<div id="toolbar-title">Related user management control panels</div>
	
	<div id="toolbar-body">
	
		<table width="850">
		
			<tr>
			
			<td><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}"><img src="{$base_url}resources/images/toolbar/users-list-standard.png" title="List all registered users of this site" alt="List all registered users of this site" width="60" height="60" border="0" /></a></td>
			
			<td><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}&amp;administrator=1"><img src="{$base_url}resources/images/toolbar/users-list-administrator.png" title="List all users with administrator privileges" alt="List all users with administrator privileges" width="60" height="60" border="0" /></a></td>
			
			<td><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}&amp;photographer=1"><img src="{$base_url}resources/images/toolbar/users-list-photographer.png" title="List all users with photographer privileges" alt="List all users with photographer privileges" width="60" height="60" border="0" /></a></td>
			
			<td><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}&amp;inactive=1"><img src="{$base_url}resources/images/toolbar/users-list-inactive.png" title="List all inactive user accounts" alt="List all inactive user accounts" width="60" height="60" border="0" /></a></td>
			
			<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
		
			<td><a href="{$base_url}{$fil_admin_users}?cmd=createNewUserForm"><img src="{$base_url}resources/images/toolbar/users-add.png" title="Add a new user" alt="Add a new user" width="60" height="60" border="0" /></a></td>
			
			<td><a href="{$base_url}{$fil_admin_groups}"><img src="{$base_url}resources/images/toolbar/users-groups.png" title="Manage user groups and permissions" alt="Manage user groups and permissions" width="60" height="60" border="0" /></a></td>
			
			<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
		
			<td><a href="{$base_url}{$fil_admin_lightbox}"><img src="{$base_url}resources/images/toolbar/users-lightboxes.png" title="List all active lightboxes on your website" alt="List all active lightboxes on your website" width="60" height="60" border="0" /></a></td>
			
			<td><a href="{$base_url}{$fil_admin_store}?cmd=listImageDownloads" title="View high-resolution image download logs"><img src="{$base_url}resources/images/toolbar/sales-downloads.png" alt="" width="60" height="60" border="0" /></a></td>
			
			</tr>
			
			<tr>
			
			<td style="width:14%;"><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}" class="plain">All registered<br />users</a></td>
						
			<td style="width:14%;"><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}&amp;administrator=1" class="plain">List all<br />administrators</a></td>
			
			<td style="width:14%;"><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}&amp;photographer=1" class="plain">List all<br />photographers</a></td>
			
			<td style="width:14%;"><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}&amp;inactive=1" class="plain">List inactive<br />users</a></td>
			
			<td style="width:14%;"><a href="{$base_url}{$fil_admin_users}?cmd=createNewUserForm" class="plain">Create a<br />new user</a></td>
			
			<td style="width:14%;"><a href="{$base_url}{$fil_admin_groups}" class="plain">Manage user<br />groups</a></td>
			
			<td style="width:14%;"><a href="{$base_url}{$fil_admin_lightbox}" class="plain">List active<br />lightboxes</a></td>
			
			<td style="width:14%;"><a href="{$base_url}{$fil_admin_store}?cmd=listImageDownloads" title="View high-resolution image download logs" class="plain">Image<br />downloads</a></td>
			
			</tr>
			
		</table>
	
	</div>

</div>