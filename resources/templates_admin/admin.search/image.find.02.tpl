{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	
	<title>Find an image</title>
	
	<link rel="SHORTCUT ICON" href="{$base_url}favicon.ico" />
	
	<link rel="stylesheet" href="{$base_url}resources/css/pixaria.2.0.css" />
	<link rel="stylesheet" href="{$base_url}resources/css/pixaria.3.0.css" />
		 
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.global.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.main.js"></script>

	<script type="text/javascript" language="javascript">
	{literal}
	
	function setKeyImage (image_path, width, height) {
		
		var preview = window.opener.document.getElementById('image_preview');
		
		preview.innerHTML= '{/literal}<p style="text-align:center;"><img src="{$base_url}{$fil_admin_image_output}?file=' + image_path + '&amp;" width="' + width + '" height="' + height + '" border="0" class="thumbnail" alt="This image will be the thumbanil key image for this gallery" title="This image will be the thumbanil key image for this gallery" /></p>{literal}';
		
	}
	
	{/literal}
	</script>
	
	
</head>

<body>

<div id="popup-content">

	<div style="width:100%;">

<form action="{$base_url}{$fil_admin_image_find}?name={$name}&amp;cmd=2" method="post" name="images">

<table class="table-popup-form">

<thead>

<tr>
<th colspan="2">Click a thumbnail to select an image</th>
</tr>

</thead>

<tbody>

<tr>
<td colspan="2" class="options">

		<div class="option-title">Page number: </div>
			
		<div class="pagination">
		
			{section name="ipages" loop=$ipage_numbers}
			
				{if $ipage_numbers[ipages] == $ipage_current}
				
					<a href="{$ipage_links[ipages]}" class="current">{$ipage_numbers[ipages]}</a>
				
				{else}
				
					<a href="{$ipage_links[ipages]}">{$ipage_numbers[ipages]}</a>
				
				{/if}
			
			{/section}
			
		</div>


</td>
</tr>

<tr>
<td colspan="2" class="options">

	<div class="option-title">Thumbnail options: </div>
				
	<div class="pagination">
		
		<form action="{$base_url}{$fil_admin_library_search}?cmd=actionPopupSearch&amp;sid={$sid}" method="post" onsubmit="return false;">
		<input type="text" size="3" maxlength="3" id="thumb_per_page" name="thumb_per_page" value="{$set_thumb_per_page}" class="page_button" />
		<input type="button" value="Thumbnails per page" onclick="javascript:updateCookie(1,document.getElementById('thumb_per_page').value,'1|{$set_thumb_per_page}|{$set_gallery_per_page}|{$set_view_thumb_view}');location.href='{$base_url}{$fil_admin_library_search}?cmd=actionPopupSearch&amp;sid={$sid}&amp;name={$name}';" class="formbutton" />
		</form>
		
	</div>
		
</td>
</tr>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<td colspan="2">

	{section name="tn" loop=$thumbnails}
		
		<table class="popup-thumbnail"><tr><td style="vertical-align:middle; text-align:center;">
		
		<a href="javascript:void(0);" onclick="javascript:window.opener.document.getElementById('{$name}').value={$thumbnails[tn].basic.id};setKeyImage('{$thumbnails[tn].files.large_path}','{$thumbnails[tn].files.large_size.0}','{$thumbnails[tn].files.large_size.1}');window.close();"><img src="{$thumbnails[tn].files.small_url}" border="0" {$thumbnails[tn].files.small_size.2} title="{$thumbnails[tn].basic.title}" alt="{$thumbnails[tn].basic.title}" class="thumbnail" /></a>
		
		</td></tr></table>
		
	{/section}

</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="button" onclick="javascript:location.href='{$base_url}{$fil_admin_library_search}?cmd=formPopupSearch&amp;name={$name}';" style="width:100%;" class="formbutton" value="Search again" /></td>
</tr>

</table>

<input type="hidden" name="image_id" value="{$thumb_id[0]}" />

</form>

</div>

</div>

</body>
</html>
