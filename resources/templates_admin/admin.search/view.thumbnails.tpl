{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

{if $thumbnails != false}

	<table class="table-thumbnail">
	
	<thead>
		
	<tr>
	<th>Browse search results:</th>
	</tr>

	
	</thead>
	
	<tbody>
	
	<tr>
	<td class="options">
	
	<div class="option-title">Actions: </div>
	
	<div class="pagination">
		
	<a href="javascript:selectAllImages('images[]');">Select all</a>
	
	<a href="javascript:deselectAllImages('images[]');">Deselect all</a>
	
	<div style="margin:0 10px 0 10px; float:left;">&nbsp;</div>
	
	<a href="javascript:void(0);" onclick="javascript:submitImageGridForm('addImagesToGallery');">Add images to a gallery</a>
	
	<a href="javascript:void(0);" onclick="javascript:submitImageGridForm('editMultiple');">Edit details of the images</a>
	
	<a href="javascript:void(0);" onclick="javascript:submitImageGridForm('confirmDeleteImages');">Delete selected images</a>
	
	</div>
	
	
	</td>

	</tr>


	<tr>
	<td class="options">

		<div class="option-title">Page number: </div>
			
		<div class="pagination">
		
			{section name="ipages" loop=$ipage_numbers}
			
				{if $ipage_numbers[ipages] == $ipage_current}
				
					<a href="{$ipage_links[ipages]}" class="current">{$ipage_numbers[ipages]}</a>
				
				{else}
				
					<a href="{$ipage_links[ipages]}">{$ipage_numbers[ipages]}</a>
				
				{/if}
			
			{/section}
			
		</div>
	
	</td>
	</tr>


	
	<tr>
	<td class="options">
	
		<div class="option-title">Thumbnail options: </div>
			
		<div class="pagination">
			
			<a href="javascript:void(0);" onclick="javascript:toggleLarge();updateCookie(3,'large','1|{$set_thumb_per_page}|{$set_gallery_per_page}|{$set_view_thumb_view}');" rel="nofollow">Large</a>
			
			<a href="javascript:void(0);" onclick="javascript:toggleSmall();updateCookie(3,'small','1|{$set_thumb_per_page}|{$set_gallery_per_page}|{$set_view_thumb_view}');" rel="nofollow">Small</a>
			
		</div>

		<div style="margin:0 10px 0 10px; float:left;">&nbsp;</div>
			
		<div class="pagination">
			
			<form action="{$base_url}{$fil_admin_library_search}?cmd=doSearch&amp;sid={$sid}" method="post" onsubmit="return false;">
			<input type="text" size="3" maxlength="3" id="thumb_per_page" name="thumb_per_page" value="{$set_thumb_per_page}" class="page-button" />
			<input type="button" value="Thumbnails per page" onclick="javascript:updateCookie(1,document.getElementById('thumb_per_page').value,'1|{$set_thumb_per_page}|{$set_gallery_per_page}|{$set_view_thumb_view}');location.href='{$base_url}{$fil_admin_library_search}?cmd=doSearch&amp;sid={$sid}';" class="formbutton" />
			</form>
			
		</div>
			
	</td>
	</tr>
	
	
	
	
	
	<tr>
	<td>
	
	<!-- START OF LARGE THUMBNAIL SECTION -->
	<div id="image-large"{if $set_view_thumb_view == "small"} style="display:none;"{/if}>

	<form method="post" id="thumb_grid_large" name="thumb_grid_large" action="{$base_url}{$fil_admin_library_search}" style="margin-top:0px; margin-bottom:0px;">

	<input type="hidden" name="cmd" value="" />
	
	{section name="tn" loop=$thumbnails}
	
		<div class="thumb-lrg" style="width:200px; height:290px;">
		
		<table>
		
		<thead>
		<tr>
		<th colspan="2"><a href="{$thumbnails[tn].files.admin_preview_url}"><img src="{$thumbnails[tn].files.large_url}" class="thumbnail" {$thumbnails[tn].files.large_size.2} alt="" /></a></th>
		</tr>
		</thead>
		
		<tbody>
		<tr>
		<th>Title:</th>
		<td><a href="{$thumbnails[tn].files.admin_preview_url}" title="{$thumbnails[tn].basic.title}" class="plain">{$thumbnails[tn].basic.title|truncate:20:"...":true}</a></td>
		</tr>
		
		<tr>
		<th>Date:</th>
		<td>{$thumbnails[tn].basic.date|date_format:"%B %e, %Y"}</td>
		</tr>
		
		<tr>
		<th>Filename:</th>
		<td>{$thumbnails[tn].basic.file_name_full|truncate:20:"...":true}</td>
		</tr>
		
		<tr>
		<th>Select:</th>
		<td><input type="checkbox" name="images[]" value="{$thumbnails[tn].basic.id}" style="font-size:0.8em;" class="forminput" /></td>
		</tr>
	
		<tr>
		<th>Actions:</th>
		<td>
		
		<a href="{$thumbnails[tn].files.admin_edit_url}" title="Edit details of this image">Edit image</a><br />
		<a href="{$thumbnails[tn].files.download_comp}">Download comp</a>
		</td>
		</tr>

		</tbody>

		</table>
		
		</div>
		
	{/section}
			
	<br clear="all"/>
	
	</form>
	
	</div>
	<!-- END OF LARGE THUMBNAIL SECTION -->
	
	<!-- START OF SMALL THUMBNAIL SECTION -->
	<div id="image-small"{if $set_view_thumb_view == "large"} style="display:none;"{/if}>
		
	<form method="post" id="thumb_grid_small" name="thumb_grid_small" action="{$base_url}{$fil_admin_library_search}" style="margin-top:0px; margin-bottom:0px;">

	<input type="hidden" name="cmd" value="" />
	
	{section name="tn" loop=$thumbnails}
	
		<div class="thumb-sml" style="width:190px; height:200px;">
		
		<table>
		
		<thead>
		<tr>
		<th colspan="2"><a href="{$thumbnails[tn].files.admin_preview_url}"><img src="{$thumbnails[tn].files.small_url}" class="thumbnail" {$thumbnails[tn].files.small_size.2} alt="" /></a></th>
		</tr>
		</thead>
		
		<tbody>
		<tr>
		<th>Title:</th>
		<td><a href="{$thumbnails[tn].files.admin_preview_url}" title="{$thumbnails[tn].basic.title}" class="plain">{$thumbnails[tn].basic.title|truncate:20:"...":true}</a></td>
		</tr>
		
		<tr>
		<th>Date:</th>
		<td>{$thumbnails[tn].basic.date|date_format:"%B %e, %Y"}</td>
		</tr>
		
		<tr>
		<th>Filename:</th>
		<td>{$thumbnails[tn].basic.file_name_full|truncate:20:"...":true}</td>
		</tr>
		
		<tr>
		<th>Select:</th>
		<td><input type="checkbox" name="images[]" value="{$thumbnails[tn].basic.id}" style="font-size:0.8em;" class="forminput" /></td>
		</tr>
		
		<tr>
		<th>Actions:</th>
		<td>
		<a href="{$thumbnails[tn].files.admin_edit_url}" title="Edit details of this image">Edit image</a><br />
		<a href="{$thumbnails[tn].files.download_comp}">Download comp</a>
		</td>
		</tr>

		</tbody>
		
		</table>
		
		</div>
		
	{/section}
			
	<br clear="all"/>
	
	</form>
	
	</div>
	<!-- END OF SMALL THUMBNAIL SECTION -->
		
	
	</td>
	</tr>
	
	</tbody>
	
	</table>
	
	<script type="text/javascript" language="JavaScript">
	{literal}
	
	function submitImageGridForm (command) {
		
		var counter = 0;
		
		$$('input[name="images[]"]').each(function(e) { if (e.checked) { counter++ } } );
		
		if (counter == 0) {
			alert("No images were selected...");
			return false;
		}
		
		if ($('image-small').style.display == 'none') {
			var frm = $('thumb_grid_large');
		} else {
			var frm = $('thumb_grid_large');
		}
		
		if (command == 'editMultiple') {
			
			if (counter == 1) {
				frm.cmd.value = 'edit';
			} else {
				frm.cmd.value = 'editMultiple';
			}
			
			frm.action = '{/literal}{$base_url}{$fil_admin_image}{literal}';
			
		} else {
			
			frm.cmd.value = command;
			
		}
		
		frm.submit();
	
	}
	
	{/literal}
	</script>
	
{/if}
