{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">



<!-- SHOW INFORMATION ABOUT THE SEARCH RESULTS -->
{if $results_total == 1}

	<table class="table-text">
	
	<tr>
	<th>Your search returned one image</th>
	<td>
	
	<h1>Refine your search</h1>
	
	<p>The results of your search are listed below alongside the search terms you entered on the previous page.  You can refine your search by amending any of the parameters in the form and resubmitting it.</p>
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	</table>

{/if}




{include_template file="admin.search/search.form.html"}




<!-- INCLUDE TEMPLATE FOR DISPLAYING IMAGE THUMBNAILS -->
{include_template file="admin.search/view.thumbnails.tpl"}




</div>

</div>



<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.footer.tpl"}


