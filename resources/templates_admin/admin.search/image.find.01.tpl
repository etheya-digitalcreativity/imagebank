{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Find an image</title>
	<meta name="generator" content="BBEdit 8.0" />

	<link rel="stylesheet" href="{$base_url}resources/css/pixaria.2.0.css" />
	<link rel="stylesheet" href="{$base_url}resources/css/pixaria.3.0.css" />
		 
</head>

<body>

<div id="popup-content">

	<div style="width:100%;">
	
	<table class="table-popup-text">
	
	<tr>
	<th>Find gallery key image</th>
	<td><p>Use this form to find an image in your library to associate with this gallery.</p></td>
	</tr>
	
	</table>
	
	
	<form action="{$base_url}{$fil_admin_library_search}" method="post">
	
	<input type="hidden" name="cmd" value="doSearch" />
	
	<input type="hidden" name="name" value="{$name}" />
	
	<input type="hidden" name="mode" value="popup" />
	
	<table class="table-popup-form">
	
	<thead>
	
	<tr>
	<th colspan="2">Find images by directory</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>Images from directory:</th>
	<td>
	<select name="search_directory" class="forminput" style="width:100%;">
	{section name="directories" loop=$image_path}
	<option value="{$image_path[directories]}">{$image_path[directories]}</option>
	{/section}
	</select>
	</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td align="left"><input type="submit" class="formbutton" style="width:100%;" value="Find images" /></td>
	</tr>
	
	</tbody>
	
	</table>
	
	</form>
	
	
	
	
	
	<form action="{$base_url}{$fil_admin_library_search}" method="post">
	
	<input type="hidden" name="cmd" value="doSearch" />
	
	<input type="hidden" name="name" value="{$name}" />
	
	<input type="hidden" name="mode" value="popup" />
	
	<table class="table-popup-form">
	
	<thead>
	
	<tr>
	<th colspan="2">Advanced search:</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Any of these keywords:</th>
	<td><input type="search" placeholder="Enter keywords" autosave="{$sys_unique_id}_admin_AnyKeywords" results="10" name="keywords_simple_boolean" value="" class="forminput" style="width:100%;" /></td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Search galleries:</td>
	<td>
	<select name="gallery_id" class="forminput" style="width:100%;" size="7">
	<option value="0">Entire library</option>
	<option value="0"></option>
	{section name=gallery loop=$menu_gallery_title}
		
		<option value="{$menu_gallery_id[gallery]}">{$menu_gallery_title[gallery]}</option>
	
	{/section}
	
	</select>
	</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Don't search by date:</th>
	<td>
		<table border="0" cellpadding="1" cellspacing="0">
		<tr>
		<td style="width:20px;"><input type="radio" name="date_srch" value="10" checked="checked" /></td>
		<td style="width:170px;">Ignore dates for this search</td>
		</tr>
		</table>
	</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Date created is:</th>
	<td>
		<table>
		<tr>
		<td style="width:20px;"><input type="radio" name="date_srch" value="11" /></td>
		<td style="width:170px;">
			
			{if $set_date_format == "dmy"}
			
				<select name="srch_date_is_dd" class="forminput">
				{section name="day" loop=$days}
					<option value="{$days[day]}" {if $days[day]==$this_day} selected="selected"{/if}>{$days[day]}</option>
				{/section}
				</select>
				
				<select name="srch_date_is_mm" class="forminput">
				{section name="month" loop=$months}
					<option value="{$months[month]}" {if $months[month]==$this_month} selected="selected"{/if}>{$months[month]}</option>
				{/section}
				</select>
				
			{else}
			
				<select name="srch_date_is_mm" class="forminput">
				{section name="month" loop=$months}
					<option value="{$months[month]}" {if $months[month]==$this_month} selected="selected"{/if}>{$months[month]}</option>
				{/section}
				</select>
				
				<select name="srch_date_is_dd" class="forminput">
				{section name="day" loop=$days}
					<option value="{$days[day]}" {if $days[day]==$this_day} selected="selected"{/if}>{$days[day]}</option>
				{/section}
				</select>
				
			{/if}
			
			<select name="srch_date_is_yy" class="forminput">
			{section name="year" loop=$years}
				<option value="{$years[year]}" {if $years[year]==$this_year} selected="selected"{/if}>{$years[year]}</option>
			{/section}
			</select>
			
		</td>
		</tr>
		</table>
	</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Date created is in the range:</th>
	<td>
	
		<table>
		<tr>
		<td style="width:20px;"><input type="radio" name="date_srch" value="12" /></td>
		<td style="width:170px;">
			
			{if $set_date_format == "dmy"}
			
				<select name="srch_date_st_dd" class="forminput">
				{section name="day" loop=$days}
					<option value="{$days[day]}" {if $days[day]==$this_day} selected="selected"{/if}>{$days[day]}</option>
				{/section}
				</select>
				
				<select name="srch_date_st_mm" class="forminput">
				{section name="month" loop=$months}
					<option value="{$months[month]}" {if $months[month]==$this_month} selected="selected"{/if}>{$months[month]}</option>
				{/section}
				</select>
			
			{else}
			
				<select name="srch_date_st_mm" class="forminput">
				{section name="month" loop=$months}
					<option value="{$months[month]}" {if $months[month]==$this_month} selected="selected"{/if}>{$months[month]}</option>
				{/section}
				</select>
				
				<select name="srch_date_st_dd" class="forminput">
				{section name="day" loop=$days}
					<option value="{$days[day]}" {if $days[day]==$this_day} selected="selected"{/if}>{$days[day]}</option>
				{/section}
				</select>
				
			{/if}
			
			<select name="srch_date_st_yy" class="forminput">
			{section name="year" loop=$years}
				<option value="{$years[year]}" {if $years[year]==$this_year} selected="selected"{/if}>{$years[year]}</option>
			{/section}
			</select>
			
		</td>
		</tr>
		<tr>
		<td style="width:20px;">&nbsp;</td>
		<td style="width:170px;">
			
			{if $set_date_format == "dmy"}
			
				<select name="srch_date_en_dd" class="forminput">
				{section name="day" loop=$days}
					<option value="{$days[day]}" {if $days[day]==$this_day} selected="selected"{/if}>{$days[day]}</option>
				{/section}
				</select>
		
				<select name="srch_date_en_mm" class="forminput">
				{section name="month" loop=$months}
					<option value="{$months[month]}" {if $months[month]==$this_month} selected="selected"{/if}>{$months[month]}</option>
				{/section}
				</select>
		
			{else}
			
				<select name="srch_date_en_mm" class="forminput">
				{section name="month" loop=$months}
					<option value="{$months[month]}" {if $months[month]==$this_month} selected="selected"{/if}>{$months[month]}</option>
				{/section}
				</select>
		
				<select name="srch_date_en_dd" class="forminput">
				{section name="day" loop=$days}
					<option value="{$days[day]}" {if $days[day]==$this_day} selected="selected"{/if}>{$days[day]}</option>
				{/section}
				</select>
		
			{/if}
			
			<select name="srch_date_en_yy" class="forminput">
			{section name="year" loop=$years}
				<option value="{$years[year]}" {if $years[year]==$this_year} selected="selected"{/if}>{$years[year]}</option>
			{/section}
			</select>
			
		</td>
		</tr>
		</table>
	
	</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Model release:</th>
	<td>
	<input type="checkbox" name="model_release" class="forminput" {if $model_release == "on"}checked="checked"{/if} /> Limit to images with model release
	</td>
	</tr>

	<tr class="{cycle values='list-one,list-two'}">
	<th>Property release:</th>
	<td>
	<input type="checkbox" name="property_release" class="forminput" {if $property_release == "on"}checked="checked"{/if} /> Limit to images with property release
	</td>
	</tr>

	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>Images uploaded by user:</th>
	<td>
	<select name="search_userid" class="forminput" style="width:100%;">
	<option value="">Any user</option>
	{section name="userids" loop=$userid}
	<option value="{$userid[userids]}">{$user_name[userids]}</option>
	{/section}
	</select>
	</td>
	</tr>
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>Display results in order:</th>
	<td>
	<select name="results_order" class="forminput" style="width:100%;">
	<option value="1"{if $results_order == 1} selected="selected"{elseif !$results_order && $set_search_default_order == 1} selected="selected"{/if}>Most recent images first</option>
	<option value="2"{if $results_order == 2} selected="selected"{elseif !$results_order && $set_search_default_order == 2} selected="selected"{/if}>Closest matches first</option>
	<option value="3"{if $results_order == 3} selected="selected"{elseif !$results_order && $set_search_default_order == 3} selected="selected"{/if}>A - Z</option>
	<option value="4"{if $results_order == 4} selected="selected"{elseif !$results_order && $set_search_default_order == 4} selected="selected"{/if}>Z - A</option>
	</select>
	</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td><input type="submit" class="formbutton" style="width:100%;" value="Find images" /></td>
	</tr>
	
	</table>
	
	</form>

	</div>

</div>


</body>
</html>