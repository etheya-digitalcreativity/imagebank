Dear {$to_name},

This is an automated message from the administrator at {$site_name} to let you know that you have been invited to become a registered user.

If you would like to accept the invitation, you can complete your registration here:

{$registration_link}

If this is unexpected or if you have any questions, please reply to this message and we'll get back to you as soon as we can!

Best regards,

{$set_contact_name}

{$set_site_name} Administrator

{$base_url}
