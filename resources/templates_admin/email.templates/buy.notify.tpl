Dear {$name},

Thank you for purchasing from {$site_name}!  To view details of your transaction and to see if it's been shipped or made available for download yet, login to your account and go to the 'My Account' page:

{$base_url}{$fil_index_account}

Best wishes,

{$set_contact_name}

{$site_name} Administrator

{$base_url}
