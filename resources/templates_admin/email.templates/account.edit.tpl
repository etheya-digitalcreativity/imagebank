{*

/*
*
*	Pixaria Gallery 1.0
*	Copyright 2002 - 2006 Jamie Longstaff
*
*	Script author:		Jamie Longstaff
*	Script version:		1.0	
*
*/

*}

Message from {$site_name}

Dear {$to_name},

This is an automated message from the administrator at {$site_name} to let you know that your account information on {$site_name} has been updated by the site administrator.  You can manage your account at any time by going to your account homepage:

{$base_url}{$fil_index_account}

If this is unexpected or if you have any questions, please reply to this message and we'll try to get back to you as soon as we can!

Best regards,

{$set_contact_name}

{$site_name} Administrator

{$base_url}
