{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

Message from {$site_name}

Dear {$to_name},

This is an automated message from the administrator at {$site_name}.

The website administrator has posted a message in relation to a transaction you made at this site. You can view the order and its associated messages at any time by visting the URL below:

{$base_url}{$fil_index_store}?cmd=viewTransaction&cart_id={$cart_id}

If you have any questions, please reply to this message and we'll try to get back to you as soon as we can!

Best regards,

{$set_contact_name}

{$site_name} Administrator

{$base_url}
