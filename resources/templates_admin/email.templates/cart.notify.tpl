{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

Message from {$site_name}

Dear {$to_name},

This is an automated message from the administrator at {$site_name} to let you know that your shopping cart at {$site_name} has been updated.  To view the details of this cart, please visit {$site_name}:

{$base_url}{$fil_index_store}?cmd=viewTransaction&cart_id={$cart_id}

If this e-mail was unexpected or if you have any questions, please reply to this message and we'll try to get back to you as soon as we can!

Best regards,

{$set_contact_name}

{$site_name} Administrator

{$base_url}
