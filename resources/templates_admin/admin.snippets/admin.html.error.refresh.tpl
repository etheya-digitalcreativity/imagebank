{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

<br />

<table class="table-text">

	<tr>
	
		<th>{$error_title}</th>
			
		<td>
			
			<h1>Please note:</h1>
			
			<p>{$error_message}</p>
	
			<form method="post">
				
			<input type="button" onclick="javascript:location.href='{$error_href}';" value="{$error_button_caption}" style="width:100%;" class="formbutton" />
			
			</form>
		
		</td>
		
		<td>&nbsp;</td>
		
	</tr>

</table>
