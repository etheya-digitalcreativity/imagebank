{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">

<table class="table-text">

	<tr>
	
		<th>Pixaria has detected that the installation of PHP on your server is operating in safe mode.</th>
			
		<td>
			
			{include_template file="admin.snippets/admin.html.notification.warning.tpl" form_title_text="Pixaria cannot operate correctly in PHP safe mode because this configuration prevents Pixria from creating, moving or editing files and directories on the web server.<br /><br />To resolve this issue, you can either contact your hosting provider to see if they can turn this setting off or alternatively switch to a hosting supplier that allows you to use PHP with safe mode turned off."}
			
			<br />
			
			<h1>Recommended hosting suppliers</h1>
			
			<p>The following hosting services all provide systems that support Pixaria Gallery.</p>
			
			<ul>
				<li><a href="http://www.saratogahosting.com/">Saratoga Hosting</a> (USA)</li>
				<li><a href="http://www.jasonfriend.co.uk/hosting/">Jason Friend Hosting</a> (UK)</li>
				<li><a href="http://www.dsvr.co.uk/">Designer Servers</a> (UK)</li>
			</ul>
			
		</td>
		
		<td>&nbsp;</td>
	</tr>

</table>

</div>

</div>

{include_template file="admin.snippets/admin.html.footer.tpl"}
