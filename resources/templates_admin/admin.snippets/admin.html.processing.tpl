{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/2000/REC-xhtml1-20000126/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	
<title>{$set_site_name}</title>

{$meta_header}

<style type="text/css">
{literal}
body
	{
	background-color: #DDD;
	margin:0px 0px; padding:0px;
	text-align:center;
	font-family:'Lucida Grande', Arial, Verdana, Helvetica;
	font-size:9pt;
	color:#555;
	}
		
#content
	{
	background-color: transparent;
	text-align: center;
	margin-top: -50px;
	margin-left: -300px;
	position: absolute;
	top: 50%;
	left: 50%;
	width: 300px;
	height: 50px;
	visibility: visible;
	}
		
.bodytext
	{
	width:50px;
	}
			
a:link,a:visited,a:active
	{
	text-decoration:none;
	color: #444;
	}

a:hover
	{
	text-decoration:underline;
	color: #444;
	}
{/literal}
</style>
		
</head>

<body>
		
<div id="content">
	
	<div class="bodytext">
	
		<table border="0" cellspacing="3" cellpadding="0" width="600">
		
		<tr>
		
		<td align="center" valign="top">
			
		<p><img src="{$base_url}resources/images/furniture/progress.gif" width="27" height="35" alt="Please wait" /></p>
		
		<p align="center" style="font-size:14pt;">{$wait_mess}</p>

		<p align="center">{$meta_link}</p>
	
		</td>
				
		</tr>
		
		</table>
			
	</div>
			
</div>
			
</body>

</html>