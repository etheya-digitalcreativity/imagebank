{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}


<ul id="navheadings">
	
	<li class="tab"><a href="#tab-site" {if $admin_page_section == "site"}class="active"{/if}>Main</a></li>
	<li class="tab"><a href="#tab-images" {if $admin_page_section == "images"}class="active"{/if}>Library</a></li>
	<li class="tab"><a href="#tab-user" {if $admin_page_section == "user"}class="active"{/if}>Users</a></li>
	<li class="tab"><a href="#tab-products" {if $admin_page_section == "products"}class="active"{/if}>Products</a></li>
	<li class="tab"><a href="#tab-sales" {if $admin_page_section == "sales"}class="active"{/if}>Sales</a></li>
	<li class="tab"><a href="#tab-blog" {if $admin_page_section == "blog"}class="active"{/if}>Blog</a></li>
	<li class="tab"><a href="#tab-settings" {if $admin_page_section == "settings"}class="active"{/if}>Settings</a></li>
	<li class="tab"><a href="#tab-logs" {if $admin_page_section == "logs"}class="active"{/if}>Logs</a></li>

</ul>

<div class="clear"></div>
	
	<div id="subnav">
		
		<div id="tab-site" {if $admin_page_section != "site"}style="display:none;"{/if}>
			
			<table width="850" class="nav-icons" {if $ses_pref_admin_navigation_style=='text'}style="display:none;"{/if}>
			
				<tr>
				
				<td><a href="{$base_url}{$fil_admin_dashboard}"><img src="{$base_url}resources/images/toolbar/tools-dashboard.png" title="Go to the admin dashboard" alt="" width="60" height="60" border="0" /></a></td>
			
				<td><a href="{$base_url}"><img src="{$base_url}resources/images/toolbar/tools-home.png" title="Go to your website home page" alt="" width="60" height="60" border="0" /></a></td>
			
				<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
				
				<td><a href="{$base_url}{$fil_admin_status}"><img src="{$base_url}resources/images/toolbar/tools-status.png" title="View details of your server configuration" alt="" width="60" height="60" border="0" /></a></td>
			
				<td><a href="{$base_url}{$fil_admin_pixie}"><img src="{$base_url}resources/images/toolbar/tools-plugins.png" title="View details of installed plug-ins" alt="" width="60" height="60" border="0" /></a></td>
			
				<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
			
				<td><a href="javascript:void(0);" onclick="javascript:flushCache();"><img src="{$base_url}resources/images/toolbar/tools-flushcache.png" title="Flush the Smarty template cache" alt="" width="60" height="60" border="0" /></a></td>
			
				<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
			
				<td><a href="{$base_url}{$fil_admin_export}"><img src="{$base_url}resources/images/toolbar/tools-export.png" title="Export your database to back it up or install onto another server" alt="Export your database to back it up or install onto another server" width="60" height="60" border="0" /></a></td>
			
				<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
			
				<td><a href="{$base_url}{$fil_admin_preferences}"><img src="{$base_url}resources/images/toolbar/preferences.png" title="Edit personal preferences and settings" alt="Edit personal preferences and settings" width="60" height="60" border="0" /></a></td>
			
				</tr>
				
				<tr>
			
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_dashboard}" class="plain">Dashboard</a></td>
			
				<td style="width:14%;"><a href="{$base_url}" class="plain">Site Homepage</a></td>
				
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_status}" class="plain">Software Status</a></td>
			
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_pixie}" class="plain">Plug-in Manager</a></td>
				
				<td style="width:14%;"><a href="javascript:void(0);" onclick="javascript:flushCache();" class="plain">Clear Template Cache</a></td>
			
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_export}" class="plain">Database<br />Backup</a></td>
				
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_preferences}" class="plain">Personal<br />preferences</a></td>
				
				</tr>
				
			</table>
			
			<div class="nav-text" {if $ses_pref_admin_navigation_style!='text'}style="display:none;"{/if}>
				
				<ul>
					<li><a href="{$base_url}{$fil_admin_dashboard}" class="plain">Dashboard</a></li>
					<li><a href="{$base_url}" class="plain">Site Homepage</a></li>
					<li><a href="{$base_url}{$fil_admin_status}" class="plain">Software Status</a></li>
					<li><a href="{$base_url}{$fil_admin_pixie}" class="plain">Plug-in Manager</a></li>
					<li><a href="javascript:void(0);" onclick="javascript:flushCache();" class="plain">Clear Template Cache</a></li>
					<li><a href="{$base_url}{$fil_admin_export}" class="plain">Database Backup</a></li>
					<li><a href="{$base_url}{$fil_admin_preferences}" class="plain">Personal Preferences</a></li>
				</ul>
			
			</div>
			
		</div>

		<div id="tab-user" {if $admin_page_section != "user"}style="display:none;"{/if}>			
			
			<table width="900" class="nav-icons" {if $ses_pref_admin_navigation_style=='text'}style="display:none;"{/if}>
			
				<tr>
				
				<td><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}"><img src="{$base_url}resources/images/toolbar/users-list-standard.png" title="List all registered users of this site" alt="List all registered users of this site" width="60" height="60" border="0" /></a></td>
				
				<td><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}&amp;administrator=1"><img src="{$base_url}resources/images/toolbar/users-list-administrator.png" title="List all users with administrator privileges" alt="List all users with administrator privileges" width="60" height="60" border="0" /></a></td>
				
				<td><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}&amp;photographer=1"><img src="{$base_url}resources/images/toolbar/users-list-photographer.png" title="List all users with photographer privileges" alt="List all users with photographer privileges" width="60" height="60" border="0" /></a></td>
				
				<td><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}&amp;inactive=1"><img src="{$base_url}resources/images/toolbar/users-list-inactive.png" title="List all inactive user accounts" alt="List all inactive user accounts" width="60" height="60" border="0" /></a></td>
				
				<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
			
				<td><a href="{$base_url}{$fil_admin_users}?cmd=createNewUserForm"><img src="{$base_url}resources/images/toolbar/users-add.png" title="Add a new user" alt="Add a new user" width="60" height="60" border="0" /></a></td>
				
				<td><a href="{$base_url}{$fil_admin_groups}"><img src="{$base_url}resources/images/toolbar/users-groups.png" title="Manage user groups and permissions" alt="Manage user groups and permissions" width="60" height="60" border="0" /></a></td>
				
				<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
			
				<td><a href="{$base_url}{$fil_admin_lightbox}"><img src="{$base_url}resources/images/toolbar/users-lightboxes.png" title="List all active lightboxes on your website" alt="List all active lightboxes on your website" width="60" height="60" border="0" /></a></td>
				
				<td><a href="{$base_url}{$fil_admin_store}?cmd=listImageDownloads" title="View high-resolution image download logs"><img src="{$base_url}resources/images/toolbar/sales-downloads.png" alt="" width="60" height="60" border="0" /></a></td>
				
				</tr>
				
				<tr>
				
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}" class="plain">All registered<br />users</a></td>
							
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}&amp;administrator=1" class="plain">List all<br />administrators</a></td>
				
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}&amp;photographer=1" class="plain">List all<br />photographers</a></td>
				
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}&amp;inactive=1" class="plain">List inactive<br />users</a></td>
				
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_users}?cmd=createNewUserForm" class="plain">Create a<br />new user</a></td>
				
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_groups}" class="plain">Manage user<br />groups</a></td>
				
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_lightbox}" class="plain">List active<br />lightboxes</a></td>
				
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_store}?cmd=listImageDownloads" title="View high-resolution image download logs" class="plain">Image<br />downloads</a></td>
				
				</tr>
				
			</table>
			
			<div class="nav-text" {if $ses_pref_admin_navigation_style!='text'}style="display:none;"{/if}>
				
				<ul>
					<li><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}" class="plain">All Users</a></li>
					<li><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}&amp;administrator=1" class="plain">Administrators</a></li>
					<li><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}&amp;photographer=1" class="plain">Photographers</a></li>
					<li><a href="{$base_url}{$fil_admin_user}?sort={$sort}&amp;method={$method}&amp;inactive=1" class="plain">Inactive users</a></li>
					<li><a href="{$base_url}{$fil_admin_users}?cmd=createNewUserForm" class="plain">New User</a></li>
					<li><a href="{$base_url}{$fil_admin_groups}" class="plain">User Groups</a></li>
					<li><a href="{$base_url}{$fil_admin_lightbox}" class="plain">Lightboxes</a></li>
					<li><a href="{$base_url}{$fil_admin_store}?cmd=listImageDownloads" title="View high-resolution image download logs" class="plain">Image downloads</a></li>
				</ul>
			
			</div>
			
		</div>	
		
		<div id="tab-images" {if $admin_page_section != "images"}style="display:none;"{/if}>
			
			<table width="900" class="nav-icons" {if $ses_pref_admin_navigation_style=='text'}style="display:none;"{/if}>
			
				<tr>
				
					<td><a href="{$base_url}{$fil_admin_upload}"><img src="{$base_url}resources/images/toolbar/image-upload.png" title="Upload new images to your server" alt="Upload new images to your server" width="60" height="60" border="0" /></a></td>
					
					<td><a href="{$base_url}{$fil_admin_import}"><img src="{$base_url}resources/images/toolbar/image-import.png" title="Import new images into Pixaria" alt="Import new images into Pixaria" width="60" height="60" border="0" /></a></td>
					
					<td><a href="{$base_url}{$fil_admin_jclient}"><img src="{$base_url}resources/images/toolbar/image-upload-java.png" title="Upload images by FTP using the JFileUpload applet" alt="Upload images by FTP using the JFileUpload applet" width="60" height="60" border="0" /></a></td>
				
					<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
					
					<td><a href="{$base_url}{$fil_admin_image}"><img src="{$base_url}resources/images/toolbar/orphan-images.png" title="List all images not in a gallery" alt="List all images not in a gallery" width="60" height="60" border="0" /></a></td>
				
					<td><a href="{$base_url}{$fil_admin_gallery}?cmd=formCreateNewGallery"><img src="{$base_url}resources/images/toolbar/galleries-create.png" title="Create a new gallery" alt="Create a new gallery" width="60" height="60" border="0" /></a></td>
				
					<td><a href="{$base_url}{$fil_admin_gallery}"><img src="{$base_url}resources/images/toolbar/galleries-hierarchical.png" title="List galleries hierarchically" alt="List galleries hierarchically" width="60" height="60" border="0" /></a></td>
				
					<td><a href="{$base_url}{$fil_admin_gallery}?cmd=galleryList"><img src="{$base_url}resources/images/toolbar/galleries-alphabetical.png" title="List galleries alphabetically" alt="List galleries alphabetically" width="60" height="60" border="0" /></a></td>
				
					<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
					
					<td><a href="{$base_url}{$fil_admin_library_search}"><img src="{$base_url}resources/images/toolbar/library-search.png" title="Search for images in your library" alt="Search for images in your library" width="60" height="60" border="0" /></a></td>

				</tr>
				
				<tr>
				
					<td style="width:14%;"><a href="{$base_url}{$fil_admin_upload}" class="plain">Upload new<br />images</a></td>
						
					<td style="width:14%;"><a href="{$base_url}{$fil_admin_import}" class="plain">Import new<br />images</a></td>
						
					<td style="width:14%;"><a href="{$base_url}{$fil_admin_jclient}" class="plain">Java Upload<br />with resize</a></td>
						
					<td style="width:14%;"><a href="{$base_url}{$fil_admin_image}" class="plain">Images not in<br />galleries</a></td>
				
					<td style="width:14%;"><a href="{$base_url}{$fil_admin_gallery}?cmd=formCreateNewGallery" class="plain">Create new<br />gallery</a></td>
					
					<td style="width:14%;"><a href="{$base_url}{$fil_admin_gallery}" class="plain">Hierarchical<br />Gallery List</a></td>
				
					<td style="width:14%;"><a href="{$base_url}{$fil_admin_gallery}?cmd=galleryList" class="plain">Alphabetical<br />Gallery List</a></td>
				
					<td style="width:14%;"><a href="{$base_url}{$fil_admin_library_search}" class="plain">Search<br />Library</a></td>

				</tr>
				
			</table>
		
			<div class="nav-text" {if $ses_pref_admin_navigation_style!='text'}style="display:none;"{/if}>
				
				<ul>
					<li><a href="{$base_url}{$fil_admin_upload}" class="plain">Upload Images</a></li>
					<li><a href="{$base_url}{$fil_admin_import}" class="plain">Import Images</a></li>
					<li><a href="{$base_url}{$fil_admin_jclient}" class="plain">Java Upload</a></li>
					<li><a href="{$base_url}{$fil_admin_image}" class="plain">Images not in Galleries</a></li>
					<li><a href="{$base_url}{$fil_admin_gallery}?cmd=formCreateNewGallery" class="plain">New Gallery</a></li>
					<li><a href="{$base_url}{$fil_admin_gallery}" class="plain">List Galleries</a></li>
					<li><a href="{$base_url}{$fil_admin_library_search}" class="plain">Search Library</a></li>
				</ul>
			
			</div>
			
		</div>
		
		<div id="tab-products" {if $admin_page_section != "products"}style="display:none;"{/if}>
			
			<table width="400" class="nav-icons" {if $ses_pref_admin_navigation_style=='text'}style="display:none;"{/if}>
			
				<tr>
				
				<td><a href="{$base_url}{$fil_admin_products}"><img src="{$base_url}resources/images/toolbar/products-list.png" title="Manage physical products for sale on your websites" alt="Manage physical products for sale on your website" width="60" height="60" border="0" /></a></td>
				
				<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
			
				<td><a href="{$base_url}{$fil_admin_calculators}"><img src="{$base_url}resources/images/toolbar/calculator-edit.png" title="Edit rules and rule options" alt="Edit rules and rule options" width="60" height="60" border="0" /></a></td>
				
				<td><a href="{$base_url}{$fil_admin_calculators}?view=sort"><img src="{$base_url}resources/images/toolbar/calculator-reorder.png" title="Change the order rules are processed" alt="Change the order rules are processed" width="60" height="60" border="0" /></a></td>
				
				<td><a href="{$base_url}{$fil_admin_calculators}?cmd=showFormPriceRuleTest"><img src="{$base_url}resources/images/toolbar/calculator-test.png" title="Test the price calculation rules" alt="Test the price calculation rules" width="60" height="60" border="0" /></a></td>
				
				</tr>
				
				<tr>
				
				<td style="width:25%;"><a href="{$base_url}{$fil_admin_products}" class="plain" title="Manage physical products for sale on your website">Product<br />Manager</a></td>
				
				<td style="width:25%;"><a href="{$base_url}{$fil_admin_calculators}" class="plain" title="Edit rules and rule options">Price rules<br /> and options</a></td>
				
				<td style="width:25%;"><a href="{$base_url}{$fil_admin_calculators}?view=sort" class="plain" title="Change the order rules are processed">Change rule<br />order</a></td>
				
				<td style="width:25%;"><a href="{$base_url}{$fil_admin_calculators}?cmd=showFormPriceRuleTest" class="plain" title="Test the price calculation rules">Test price<br />rules</a></td>
				
				</tr>
				
			</table>
		
			<div class="nav-text" {if $ses_pref_admin_navigation_style!='text'}style="display:none;"{/if}>
				
				<ul>
					<li><a href="{$base_url}{$fil_admin_products}" class="plain" title="Manage physical products for sale on your website">Product Manager</a></li>
					<li><a href="{$base_url}{$fil_admin_calculators}" class="plain" title="Edit rules and rule options">Price Rules</a></li>
					<li><a href="{$base_url}{$fil_admin_calculators}?view=sort" class="plain" title="Change the order rules are processed">Change Price Rule Order</a></li>
					<li><a href="{$base_url}{$fil_admin_calculators}?cmd=showFormPriceRuleTest" class="plain" title="Test the price calculation rules">Test Rules</a></li>
				</ul>
			
			</div>
			
		</div>
					
		<div id="tab-sales" {if $admin_page_section != "store"}style="display:none;"{/if}>
			
			<table width="900" class="nav-icons" {if $ses_pref_admin_navigation_style=='text'}style="display:none;"{/if}>
			
				<tr>
				
				<td><a href="{$base_url}{$fil_admin_store_sales}" title="View all transactions in the database"><img src="{$base_url}resources/images/toolbar/sales-list.png" alt="" width="60" height="60" border="0" /></a></td>
			
				<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
			
				<td><a href="{$base_url}{$fil_admin_store_sales}&amp;status=1" title="View open shopping carts for all users"><img src="{$base_url}resources/images/toolbar/sales-list-active.png" alt="" width="60" height="60" border="0" /></a></td>
				
				<td><a href="{$base_url}{$fil_admin_store_sales}&amp;status=2" title="View pending transactions"><img src="{$base_url}resources/images/toolbar/sales-list-quotation.png" alt="" width="60" height="60" border="0" /></a></td>
			
				<td><a href="{$base_url}{$fil_admin_store_sales}&amp;status=3" title="Transactions awaiting payment"><img src="{$base_url}resources/images/toolbar/sales-list-payment.png" alt="" width="60" height="60" border="0" /></a></td>
				
				<td><a href="{$base_url}{$fil_admin_store_sales}&amp;status=4" title="View completed transactions"><img src="{$base_url}resources/images/toolbar/sales-list-complete.png" alt="" width="60" height="60" border="0" /></a></td>
				
				<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
			
				<td><a href="{$base_url}{$fil_admin_store}?cmd=listImageDownloads" title="View high-resolution image download logs"><img src="{$base_url}resources/images/toolbar/sales-downloads.png" alt="" width="60" height="60" border="0" /></a></td>
				
				<td><a href="{$base_url}{$fil_admin_store_report}" title="Export transaction reports"><img src="{$base_url}resources/images/toolbar/sales-report.png" alt="" width="60" height="60" border="0" /></a></td>
				
				<td><a href="{$base_url}{$fil_admin_store}?cmd=listPhotographerSales" title="List sales by photographer users"><img src="{$base_url}resources/images/toolbar/sales-report-users.png" alt="" width="60" height="60" border="0" /></a></td>
				
				</tr>
				
				<tr>
				
				<td><a href="{$base_url}{$fil_admin_store_sales}" title="View all transactions in the database" class="plain">Show all<br />purchases</a></td>
				
				<td><a href="{$base_url}{$fil_admin_store_sales}&amp;status=1" title="View open shopping carts for all users" class="plain">Show active<br />carts</a></td>
			
				<td><a href="{$base_url}{$fil_admin_store_sales}&amp;status=2" title="View pending transactions" class="plain">Sales awaiting<br />quotation</a></td>
				
				<td><a href="{$base_url}{$fil_admin_store_sales}&amp;status=3" title="View completed transactions" class="plain">Sales awaiting<br />Payment</a></td>
				
				<td><a href="{$base_url}{$fil_admin_store_sales}&amp;status=4" title="View completed transactions" class="plain">All completed<br />sales</a></td>
			
				<td><a href="{$base_url}{$fil_admin_store}?cmd=listImageDownloads" title="View high-resolution image download logs" class="plain">Image download<br />reports</a></td>
			
				<td><a href="{$base_url}{$fil_admin_store_report}" title="Export transaction reports" class="plain">Transaction<br />reporting</a></td>
			
				<td><a href="{$base_url}{$fil_admin_store}?cmd=listPhotographerSales" title="List sales by photographer users" class="plain">Sales by<br />photographers</a></td>
			
				</tr>
				
			</table>
			
			<div class="nav-text" {if $ses_pref_admin_navigation_style!='text'}style="display:none;"{/if}>
				
				<ul>
					<li><a href="{$base_url}{$fil_admin_store_sales}" title="View all transactions in the database" class="plain">All Purchases</a></li>
					<li><a href="{$base_url}{$fil_admin_store_sales}&amp;status=1" title="View open shopping carts for all users" class="plain">Active Carts</a></li>
					<li><a href="{$base_url}{$fil_admin_store_sales}&amp;status=2" title="View pending transactions" class="plain">Awaiting Quote</a></li>
					<li><a href="{$base_url}{$fil_admin_store_sales}&amp;status=3" title="View completed transactions" class="plain">Awaiting Payment</a></li>
					<li><a href="{$base_url}{$fil_admin_store_sales}&amp;status=4" title="View completed transactions" class="plain">Completed Sales</a></li>
					<li><a href="{$base_url}{$fil_admin_store}?cmd=listImageDownloads" title="View high-resolution image download logs" class="plain">Download Reports</a></li>
					<li><a href="{$base_url}{$fil_admin_store_report}" title="Export transaction reports" class="plain">Transactions</a></li>
					<li><a href="{$base_url}{$fil_admin_store}?cmd=listPhotographerSales" title="List sales by photographer users" class="plain">Photograher Sales</a></li>
				</ul>
			
			</div>
			
		</div>
		
		<div id="tab-blog" {if $admin_page_section != "blog"}style="display:none;"{/if}>
		
			<table width="210" class="nav-icons" {if $ses_pref_admin_navigation_style=='text'}style="display:none;"{/if}>
			
				<tr>
				
				<td style="text-align:center;"><a href="{$base_url}{$fil_admin_blog}"><img src="{$base_url}resources/images/toolbar/publishing-list.png" title="Show all articles" alt="Show all articles" width="60" height="60" border="0" /></a></td>
				
				<td style="text-align:center;"><a href="{$base_url}{$fil_admin_blog}?cmd=showFormEntryCreate"><img src="{$base_url}resources/images/toolbar/publishing-new.png" title="Add a new article" alt="Add a new article" width="60" height="60" border="0" /></a></td>
				
				</tr>
				
				<tr>
				
				<td><a href="{$base_url}{$fil_admin_blog}" class="plain" title="Show all articles">List all<br />articles</a></td>
				
				<td><a href="{$base_url}{$fil_admin_blog}?cmd=showFormEntryCreate" class="plain" title="Add a new article">Create new<br />article</a></td>
				
				</tr>
				
			</table>
		
			<div class="nav-text" {if $ses_pref_admin_navigation_style!='text'}style="display:none;"{/if}>
				
				<ul>
					<li><a href="{$base_url}{$fil_admin_blog}" class="plain" title="Show all articles">All Articles</a></li>
					<li><a href="{$base_url}{$fil_admin_blog}?cmd=showFormEntryCreate" class="plain" title="Add a new article">New Article</a></li>
				</ul>
			
			</div>
			
		</div>
		
		<div id="tab-settings" {if $admin_page_section != "settings"}style="display:none;"{/if}>
		
			<table width="800" class="nav-icons" {if $ses_pref_admin_navigation_style=='text'}style="display:none;"{/if}>
			
				<tr>
				
				<td><a href="{$base_url}{$fil_admin_settings}"><img src="{$base_url}resources/images/toolbar/settings-general.png" title="General settings" alt="" width="60" height="60" border="0" /></a></td>
				
				<td><a href="{$base_url}{$fil_admin_settings_search}"><img src="{$base_url}resources/images/toolbar/settings-search.png" title="Search settings" alt="" width="60" height="60" border="0" /></a></td>
				
				<td><a href="{$base_url}{$fil_admin_appearance}"><img src="{$base_url}resources/images/toolbar/settings-appearance.png" title="Appearance and behaviour settings" alt="" width="60" height="60" border="0" /></a></td>
				
				<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
				
				<td><a href="{$base_url}{$fil_admin_settings}?cmd=login"><img src="{$base_url}resources/images/toolbar/settings-security.png" title="User login and registration settings" alt="" width="60" height="60" border="0" /></a></td>
				
				<td><a href="{$base_url}{$fil_admin_settings}?cmd=regdata"><img src="{$base_url}resources/images/toolbar/settings-registration.png" title="Registration and data capture settings" alt="" width="60" height="60" border="0" /></a></td>
				
				<td><a href="{$base_url}{$fil_admin_settings}?cmd=regban"><img src="{$base_url}resources/images/toolbar/settings-regban.png" title="User registration banning" alt="" width="60" height="60" border="0" /></a></td>
				
				<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
				
				<td><a href="{$base_url}{$fil_admin_store}"><img src="{$base_url}resources/images/toolbar/settings-store.png" title="E-Commerce settings" alt="" width="60" height="60" border="0" /></a></td>
				
				</tr>
			
				<tr>
				
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_settings}" class="plain">General<br />settings</a></td>
				
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_settings_search}" class="plain">Search<br />settings</a></td>
				
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_appearance}" class="plain">Apperarance<br />settings</a></td>
				
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_settings}?cmd=login" class="plain">Login and<br />registration</a></td>
				
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_settings}?cmd=regdata" class="plain">Data<br />capture</a></td>
				
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_settings}?cmd=regban" class="plain">Registration<br />blocking</a></td>
				
				<td style="width:14%;"><a href="{$base_url}{$fil_admin_store}" class="plain">Store<br />settings</a></td>
				
				</tr>
				
			</table>
		
			<div class="nav-text" {if $ses_pref_admin_navigation_style!='text'}style="display:none;"{/if}>
				
				<ul>
					<li><a href="{$base_url}{$fil_admin_settings}" class="plain">General</a></li>
					<li><a href="{$base_url}{$fil_admin_settings_search}" class="plain">Search</a></li>
					<li><a href="{$base_url}{$fil_admin_appearance}" class="plain">Apperarance</a></li>
					<li><a href="{$base_url}{$fil_admin_settings}?cmd=login" class="plain">Login and Reegistration</a></li>
					<li><a href="{$base_url}{$fil_admin_settings}?cmd=regdata" class="plain">Data Capture</a></li>
					<li><a href="{$base_url}{$fil_admin_settings}?cmd=regban" class="plain">Registration Blocking</a></li>
					<li><a href="{$base_url}{$fil_admin_store}" class="plain">Store</a></li>
				</ul>
			
			</div>
			
		</div>
				
		<div id="tab-logs" {if $admin_page_section != "logs"}style="display:none;"{/if}>
		
			<table width="750" class="nav-icons" {if $ses_pref_admin_navigation_style=='text'}style="display:none;"{/if}>
			
			<tr>
			
			<td><a href="{$base_url}{$fil_admin_log_login}"><img src="{$base_url}resources/images/toolbar/log-authentication.png" title="Show last 50 logins" alt="Show last 50 logins" width="60" height="60" border="0" /></a></td>
			
			<td><a href="{$base_url}{$fil_admin_log_login}?show=fail"><img src="{$base_url}resources/images/toolbar/log-authentication-failed.png" title="Show failed logins" alt="Show failed logins" width="60" height="60" border="0" /></a></td>
			
			<td><a href="{$base_url}{$fil_admin_log_login}?show=ano"><img src="{$base_url}resources/images/toolbar/log-authentication-anomalous.png" title="Show logins from multiple IP addresses" alt="Show logins from multiple IP addresses" width="60" height="60" border="0" /></a></td>
			
			<td><a href="{$base_url}{$fil_admin_log_login}?cmd=1" onclick="return confirmSubmit('You are about to empty the user login log for this website.  Are you sure you want to continue?');"><img src="{$base_url}resources/images/toolbar/log-authentication-trash.png" title="Delete all the entries in the user login log" alt="Delete all the entries in the keyword search log" width="60" height="60" border="0" /></a></td>
			
			<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
			
			<td><a href="{$base_url}{$fil_admin_log_keywords}?show=inc"><img src="{$base_url}resources/images/toolbar/log-search.png" title="Show keywords that users wanted to include in their searches" alt="Show keywords that users wanted to include in their searches" width="60" height="60" border="0" /></a></td>
			
			<td><a href="{$base_url}{$fil_admin_log_keywords}?cmd=clearSearchLog" onclick="return confirmSubmit('You are about to empty the search keyword log for this website.  Are you sure you want to continue?');"><img src="{$base_url}resources/images/toolbar/log-search-trash.png" title="Delete all the entries in the keyword search log" alt="Delete all the entries in the keyword search log" width="60" height="60" border="0" /></a></td>
			
			<td rowspan="2"><img src="{$base_url}resources/images/furniture/toolbar.separator.gif" alt="" width="7" height="90" border="0" /></td>
		
			<td><a href="{$base_url}{$fil_admin_log_payscripts}"><img src="{$base_url}resources/images/toolbar/tools-payment-access.png" title="View payment script access log" alt="" width="60" height="60" border="0" /></a></td>
			
			</tr>
			
			<tr>
			
			<td style="width:14%;"><a href="{$base_url}{$fil_admin_log_login}" title="Show last 50 logins" class="plain">Recent<br />logins</a></td>
			
			<td style="width:14%;"><a href="{$base_url}{$fil_admin_log_login}?show=fail" title="Show failed logins" class="plain">Failed<br />logins</a></td>
			
			<td style="width:14%;"><a href="{$base_url}{$fil_admin_log_login}?show=ano" title="Show logins from multiple IP addresses" class="plain">Anomalous<br />logins</a></td>
			
			<td style="width:14%;"><a href="{$base_url}{$fil_admin_log_login}?cmd=1" title="Delete all the entries in the user login log" onclick="return confirmSubmit('You are about to empty the user login log for this website.  Are you sure you want to continue?');" class="plain">Clear login log</a></td>
				
			<td style="width:14%;"><a href="{$base_url}{$fil_admin_log_keywords}?show=inc" title="Show keywords that users wanted to include in their searches" class="plain">Keyword<br />search log</a></td>
			
			<td style="width:14%;"><a href="{$base_url}{$fil_admin_log_keywords}?cmd=clearSearchLog" title="Delete all the entries in the keyword search log" onclick="return confirmSubmit('You are about to empty the search keyword log for this website.  Are you sure you want to continue?');" class="plain">Clear<br />search log</a></td>
			
			<td style="width:14%;"><a href="{$base_url}{$fil_admin_log_payscripts}" class="plain">Payment Script<br />Log</a></td>
		
			</tr>
			
			</table>
		
			<div class="nav-text" {if $ses_pref_admin_navigation_style!='text'}style="display:none;"{/if}>
				
				<ul>
					<li><a href="{$base_url}{$fil_admin_log_login}" title="Show last 50 logins" class="plain">Recent Logins</a></li>
					<li><a href="{$base_url}{$fil_admin_log_login}?show=fail" title="Show failed logins" class="plain">Failed Logins</a></li>
					<li><a href="{$base_url}{$fil_admin_log_login}?show=ano" title="Show logins from multiple IP addresses" class="plain">Anomalous Logins</a></li>
					<li><a href="{$base_url}{$fil_admin_log_login}?cmd=1" title="Delete all the entries in the user login log" onclick="return confirmSubmit('You are about to empty the user login log for this website.  Are you sure you want to continue?');" class="plain">Clear Login Log</a></li>
					<li><a href="{$base_url}{$fil_admin_log_keywords}?show=inc" title="Show keywords that users wanted to include in their searches" class="plain">Search Log</a></li>
					<li><a href="{$base_url}{$fil_admin_log_keywords}?cmd=clearSearchLog" title="Delete all the entries in the keyword search log" onclick="return confirmSubmit('You are about to empty the search keyword log for this website.  Are you sure you want to continue?');" class="plain">Clear Search Log</a></li>
					<li><a href="{$base_url}{$fil_admin_log_payscripts}" class="plain">Payment Script Log</a></li>
				</ul>
			
			</div>
			
		</div>
		
	</div>
	
<script type="text/javascript" language="javascript">var tabs = new Control.Tabs('navheadings');tabs.setActiveTab('tab-{$admin_page_section}');</script>

<script>
YAHOO.namespace("pixaria.container");
{literal}
function init() {
	
	// Define various event handlers for Dialog
	var handleYes = function() {
		alert("You clicked yes!");
		this.hide();
	};
	var handleNo = function() {
		this.hide();
	};

	// Instantiate the Dialog
	YAHOO.pixaria.container.flushcache = new YAHOO.widget.SimpleDialog("dialog-flushcache", {
		width: "400px",
		fixedcenter: true,
		visible: false,
		draggable: false,
		close: true,
		modal:true,
		constraintoviewport: true
	} );
	
	// Render the Dialog
	YAHOO.pixaria.container.flushcache.render("container");
	
	
}

function flushCache () {

	$('dialog-flushcache').setStyle({display:'block'});
	YAHOO.pixaria.container.flushcache.show();
	
	new Ajax.Request('{/literal}{$base_url}{$fil_admin_dashboard}{literal}', {
		parameters:{ cmd:'flushCache' },
		onSuccess: function (e) {
			$('flushcache-done').toggle();
			$('flushcache-wait').toggle();
		}
	});
	
}

YAHOO.util.Event.addListener(window, "load", init);

{/literal}
</script>

<div id="dialog-flushcache" style="display:none;">
<div class="hd">Flush Template Cache</div>
<div class="bd">

	<div class="dialog-content" id="flushcache">
	
		<div id="flushcache-wait">
			
			Please wait while the Smarty cache is cleared...
		
		</div>
		
		<div id="flushcache-done" style="display:none;">
			
			The Smarty cache has been cleared and any changes made to your website's templates since the last cache cleanup will now be visible.
			
			<br />
			
			<br />
			
			<button class="formbutton" style="width:50px; float:right;" onclick="javascript:YAHOO.pixaria.container.flushcache.hide();$('flushcache-done').toggle();$('flushcache-wait').toggle();">OK</button>
			
			<br clear="all" />
			
		</div>
		
	</div>
	
</div>
</div>

<div class="clear"></div>

