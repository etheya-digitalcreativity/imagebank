{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">

<table class="table-text">

	<tr>
	
		<th>{$error_title}</th>
			
		<td>
			
			<h1>Please note:</h1>
			
			<p>{$error_message}</p>
	
			<form method="get">
				
			<input type="button" onclick="javascript:history.go(-1);" name="js_back" value="Go back to the previous page" style="width:100%;" class="forminput" />
			
			</form>
		
		</td>
		
		<td>&nbsp;</td>
	</tr>

</table>

</div>

</div>



{include_template file="admin.snippets/admin.html.footer.tpl"}