{*

/*
*
*	Pixaria Gallery 
*	Copyright Jamie Longstaff
*
*/

*}<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/2000/REC-xhtml1-20000126/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	
	<!-- Query String: {$query_string} -->
	<!-- Base URI: {$base_uri} -->

	<!-- Browser Name: {$browser_name} -->
	<!-- Browser Version: {$browser_version} -->
	
	<!-- OS Name: {$browser_OS_name} -->
	<!-- OS Version: {$browser_OS_version} -->
	
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	
	<title>{$set_site_name} &raquo; {$page_title}</title>
	
	<meta name="generator" content="Pixaria Gallery" />
	
	<meta name="keywords" content="Pixaria Gallery, popsoft, gallery, album, photography" />
	
	<meta name="description" content="Photo album powered by Pixaria Gallery." />
	
	{if $set_news_enable == "1"}<link href="{$base_url}{$fil_news_rss}" rel="alternate" type="application/rss+xml" title="{$set_site_name} News" />{/if}
	
	<link rel="stylesheet" href="{$base_url}resources/css/pixaria.2.0.css" />
	<link rel="stylesheet" href="{$base_url}resources/css/pixaria.3.0.css" />
	
	<link rel="stylesheet" type="text/css" href="{$base_url}resources/yui/build/container/assets/skins/sam/container.css">
	<link rel="stylesheet" type="text/css" href="{$base_url}resources/yui/build/container/assets/skins/sam/container-skin.css">
	<link rel="stylesheet" type="text/css" href="{$base_url}resources/yui/build/calendar/assets/skins/sam/calendar.css">
	<link rel="stylesheet" type="text/css" href="{$base_url}resources/yui/build/calendar/assets/skins/sam/calendar-skin.css">
	
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/scriptaculous/prototype.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/scriptaculous/scriptaculous.js"></script>
	
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/livepipe.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/tabs.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.scriptaculous.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.dropdown.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.global.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.main.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.tooltip.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.multifile.js"></script>	
	
	<script type="text/javascript" src="http://yui.yahooapis.com/2.7.0/build/yahoo-dom-event/yahoo-dom-event.js"></script>
	<script type="text/javascript" src="http://yui.yahooapis.com/2.7.0/build/element/element-min.js"></script>
	<script type="text/javascript" src="http://yui.yahooapis.com/2.7.0/build/uploader/uploader-min.js"></script>
	<script type="text/javascript" src="http://yui.yahooapis.com/2.7.0/build/dragdrop/dragdrop-min.js"></script>
	<script type="text/javascript" src="http://yui.yahooapis.com/2.7.0/build/container/container-min.js"></script> 
	<script type="text/javascript" src="http://yui.yahooapis.com/2.7.0/build/calendar/calendar-min.js"></script>
	
	{if $browser_name != "ie"}<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.checkboxes.js"></script>{/if}

	<!--[if lt IE 7]>
	<script defer type="text/javascript" src="{$base_url}resources/javascript/pixaria.pngfix.js"></script>
	<![endif]-->

</head>

<body onload="{if $page_pix_sortable}pixAdminSortable();{/if}{if $page_load_slider} loader();{/if}" class="yui-skin-sam">

<div id="content">
	
	<div id="masthead">
		
		<div id="logo">
		
			<div id="userinfo">
			
				<b>You are signed in as:</b> {$ses_psg_name}<br />
				<span class="session-end">To end your session, please <a href="{$base_url}{$fil_index_login}?cmd=signout">sign out</a>.</span>
			
			</div>
			
			<div id="pixaria-logo">Pixaria Gallery</div>
			
		</div>
	
		{include_template file="admin.snippets/admin.html.navigation.tpl"}
	
	</div>
	
	<div>
	
