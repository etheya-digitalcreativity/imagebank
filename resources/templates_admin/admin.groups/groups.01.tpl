{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">




<table class="table-search">

<thead>

<tr>
<th>Built in user groups</th>
</tr>

</thead>

<tbody>

<tr>
<td>

<div class="search-box" style="border-right:1px solid #BBB; min-height:210px;">

<b>What are built in user groups</b>

<p>Built in user groups are used by Pixaria to control access to various site functions such as the administration control panels and image upload functions.</p>

<p>You can edit the users who are members of these groups but you cannot create or delete these groups.</p>

</div>

<div class="search-box" style="border-right:1px solid #BBB; width:630px; min-height:210px;">

	<div style="width:630px; height:200px; background:#FFF; overflow:auto; overflow-x:hidden; border:1px solid #AAA;" onselectstart="javascript:return false;">
		
		<div>
		<table width="100%" style="border-collapse:collapse; border:0px;">
		
		{section name=sys_groups loop=$sys_group_id}
		
		<tr style="height:40px; background:#{cycle values='F0F5FA,FFF'};"><td style="padding:0 0 0 3px;"><b>{$sys_group_name[sys_groups]}</b><br /><span style="font-size:8pt; color:#888;">{$sys_group_description[sys_groups]}</span></td><td style="text-align:right; padding:0 20px 0 0;"><ul class="actionlist"><li><a href="{$base_url}{$fil_admin_groups}?cmd=30&amp;sys_group_id={$sys_group_id[sys_groups]}">Edit members</a></li></ul></td></tr>
		
		{/section}
				
		</table>
		</div>
		
	</div>

</div>

</td>
</tr>

</tbody>

</table>



<form action="{$base_url}{$fil_admin_groups}" method="post">

<input type="hidden" name="cmd" value="1" />



<table class="table-search">

<thead>

<tr>
<th>Customisable user access control groups</th>
</tr>

</thead>

<tbody>

<tr>
<td>

<div class="search-box" style="border-right:1px solid #BBB; min-height:360px;">

<b>Access control user groups</b>

<p>You can use user groups to control access to various parts of your website.  To create a new group, enter a name for the group in the form below and save the information.</p>

<p>When you import images into your library or create new image galleries, you can restrict access to those images and galleries by user group.  Adding users to groups gives you a fine level of control over what users of your site can see.</p>

<br />

</div>

<div class="search-box" style="border-right:1px solid #BBB; width:630px; min-height:360px;">

<b>Create a new user group:</b>

<p>To begin creating a new user group enter a title in the form field below:</p>

<input type="text" name="group_name" class="forminput" style="width:100%;" /><br /><br />

<input type="submit" value="Save new group" style="width:300px; float:right;" class="formbutton" />

<br clear="all" />

<br />

<b>View and edit details of existing groups:</b><br /><br />

	<div style="width:630px; height:160px; background:#FFF; overflow-y:auto; overflow-x:hidden; border:1px solid #AAA;" onselectstart="javascript:return false;">
		
		<table width="100%" style="border-collapse:collapse; border:0px;">
		
		{section name=groups loop=$group_id}
		
		<tr style="height:40px; background:#{cycle values='F0F5FA,FFF'};"><td style="padding:0 0 0 3px;"><b>{$group_name[groups]}</b><br /></td><td style="text-align:right; padding:0 20px 0 0;"><ul class="actionlist">{if $group_id[groups]!=1 && $group_id[groups]!=2}<li><a href="{$base_url}{$fil_admin_groups}?cmd=20&amp;group_id={$group_id[groups]}">Delete</a> </li>{/if}<li> <a href="{$base_url}{$fil_admin_groups}?cmd=10&amp;group_id={$group_id[groups]}">Edit members</a></li></ul></td></tr>
		
		{sectionelse}
		
		<tr><td>There are no user groups to display.  To create one, enter a title in the text field above and click the 'Save new group' button.</td></tr>
		
		{/section}
				
		</table>
		
	</div>

</div>

</td>
</tr>

</tbody>

</table>

</form>

</div>

</div>


<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}


