{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

<div id="admin-outerdiv">

<div id="admin-innerdiv">
	
<form action="{$base_url}{$fil_admin_groups}" method="post">

<input type="hidden" name="cmd" value="21" />

<input type="hidden" name="group_id" value="{$group_id}" />

<table class="table-text">

<tr>
<th>You are about to delete the user group '{$group_name}'.  Are you sure that you want to continue as this action cannot be undone!</th>
<td>

<h1>Are you sure you want to continue?</h1>

<p>You are about to delete the user group '{$group_name}', an action that can't be undone.  Are you sure that you want to continue and delete this user group?</p>

{include_template file="admin.snippets/admin.html.notification.note.tpl" form_title_text="Please note that any permissions available to users of this group will only be available to administrators if you proceed."}


</td>
<td>&nbsp;</td>
</tr>

<tr>
<th>&nbsp;</th>
<td><input type="submit" value="Delete this group" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</table>

</form>

</div>

</div>
