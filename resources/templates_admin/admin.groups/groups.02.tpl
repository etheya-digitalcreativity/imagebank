{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}




<div id="admin-outerdiv">

<div id="admin-innerdiv">






<form action="{$base_url}{$fil_admin_groups}" method="post">

<input type="hidden" name="cmd" value="11" />

<input type="hidden" name="group_id" value="{$group_id}" />

<table class="table-search">

<thead>

<tr>
<th>Edit this user group</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<td>

<div class="search-box" style="border-right:1px solid #BBB; min-height:390px;">

<b>About this form</b>

<p>Use this form to edit the users who have access to a group or to change the name of a user group.</p>

<br />

</div>

<div class="search-box" style="border-right:1px solid #BBB; width:630px; min-height:390px;">

<b>Title for this group:</b><br />

<input type="text" name="group_name" class="forminput" value="{$group_name}" style="width:100%;" />

<p>To select users you would like to be members of this group, check the boxes next to the user's names in the list below.</p>

	<div style="width:630px; height:252px; background:#FFF; overflow:scroll; overflow-x:hidden; border:1px solid #AAA;" onselectstart="javascript:return false;">
		
		<table width="100%" style="border-collapse:collapse; border:0px;">
		
		{section name=users loop=$userid}
		
		<tr style="height:42px; background:#{cycle values='F0F5FA,FFF'};"><td style="width:20px; vertical-align:top; padding:4px 0 0 0;"><input type="checkbox" name="users[]" value="{$userid[users]}"{if $user_member[users]==$userid[users]} checked="checked"{/if} /></td><td style="padding:0px;"><b>{$user_name[users]}</b><br /><a href="mailto:{$email_address[users]}" style="font-size:8pt; color:#888;">{$email_address[users]}</a></td><td style="text-align:right; padding:0 20px 0 0;"><ul class="actionlist"><li><a href="{$base_url}{$fil_admin_user_edit}?userid={$userid[users]}" style="font-size:9pt;">Edit user profile</a></li></ul></td></tr>
		
		{/section}
				
		</table>
		
	</div>
	
	<br />
	
	<input type="submit" value="Save changes" style="width:300px; float:right;" class="formbutton" />
	
</div>

</td>
</tr>

</tbody>

</table>

</form>

</div>

</div>


<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}


