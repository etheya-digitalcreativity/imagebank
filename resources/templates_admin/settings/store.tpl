{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">




<table class="table-text">

	<tr>
	<th>This page provides you with a summary of how your website is configured for accepting payment for images and products.</th>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	</tr>
		
</table>
	


<form action="{$base_url}{$fil_admin_store}" method="post" name="store">

<input type="hidden" name="cmd" value="settingsUpdate" />

<table class="table-search">

<tr>
<th>Store settings</th>
</tr>

<tr>
<td>

<div class="search-box" style="border-right:1px solid #BBB; min-height:100px;">

<input type="checkbox" name="func_store" class="forminput" {if $set_func_store == "1"} checked="checked"{/if} /> <b>Store features activated</b><br /><br />

</div>

<div class="search-box" style="border-right:1px solid #BBB; min-height:100px;">

<b>Show 'purchase options' for:</b><br />

&nbsp;&nbsp;&nbsp;<input type="radio" class="forminput" name="store_images_forsale" value="10"{if $set_store_images_forsale == "10"} checked="checked"{/if} />No images<br />
&nbsp;&nbsp;&nbsp;<input type="radio" class="forminput" name="store_images_forsale" value="11"{if $set_store_images_forsale == "11"} checked="checked"{/if} />All images with a price specified<br />
&nbsp;&nbsp;&nbsp;<input type="radio" class="forminput" name="store_images_forsale" value="12"{if $set_store_images_forsale == "12"} checked="checked"{/if} />All images marked as being for sale<br />
&nbsp;&nbsp;&nbsp;<input type="radio" class="forminput" name="store_images_forsale" value="13"{if $set_store_images_forsale == "13"} checked="checked"{/if} />All images
<br /><br />

</div>

<div class="search-box" style="border-right:1px solid #BBB; min-height:100px;">

<b>Default image pricing:</b><br />
Used if no individual prices are set.<br />
<input type="text" name="store_default_price" value="{$set_store_default_price}" class="forminput" style="width:50px;" /> {$set_store_currency}<br /><br />

</div>

</td>

</tr>

</table>


<table class="table-search">

<tr>
<th>Options for sales taxes</th>
</tr>

<tr>
<td>

<div class="search-box" style="border-right:1px solid #BBB; min-height:210px;">

<b>How taxes are applied</b>

<p>Sales taxes can be applied automatically or for transactions where you provide a quote, they can be set on a sale by sale basis.</p>

<b>Default sales tax rate:</b><br />
Used for fully automated transactions.<br />
<input type="text" name="store_default_tax" value="{$set_store_default_tax}" class="forminput" style="width:50px;" /> %<br /><br />

<p style="font-size:7pt;">Pixaria does not yet support sales tax calculations based on the buyers location.</p>

</div>

<div class="search-box-wide" style="border-right:1px solid #BBB; min-height:210px;">

<b>Sales tax message</b><br />

<p>If you add VAT or sales tax for your purchases, you may be legally required to supply your tax number or code.  Use this field to supply information about the tax type and your tax number or code.</p>

<textarea name="store_tax_number" class="forminput" style="width:100%;" rows="7" cols="15" onkeydown="textCounter(this.form.store_tax_number,this.form.store_tax_number_left,255);" onkeyup="textCounter(this.form.store_tax_number,this.form.store_tax_numberleft,255);">{$set_store_tax_number}</textarea><br />

<input readonly="readonly" type="text" name="store_tax_number_left" size="3" maxlength="3" value="{$store_tax_number_left}" tabindex="101" class="forminput" /> Characters left

</div>

</td>

</tr>

</table>


<table class="table-search">

<tr>
<th>Options for shipment of physical products</th>
</tr>

<tr>
<td>

<div class="search-box" style="border-right:1px solid #BBB; min-height:210px;">

<input type="checkbox" name="store_flatrate_ship" class="forminput"{if $set_store_flatrate_ship == "1"} checked="checked"{/if} /> <b>Use a flat rate for shipping fees</b><br />

<p style="margin-left:22px;">Applies this rate of shipping to all physical product purchases:</p>

<input type="text" name="store_flatrate_ship_val" value="{$set_store_flatrate_ship_val}" class="forminput" style="width:50px; margin-left:22px;" /> {$set_store_currency}<br />

</div>

<div class="search-box-wide" style="border-right:1px solid #BBB; min-height:210px;">

<b>Shipping messages to buyer:</b><br />

<p>When someone buys a physical product such as a t-shirt or print, you can show them a message about how the product will be shipped at the time they make the purchase.</p>

<textarea name="store_shipping_message" class="forminput" style="width:100%;" rows="7" cols="15" onkeydown="textCounter(this.form.store_shipping_message,this.form.store_shipping_messageleft,255);" onkeyup="textCounter(this.form.store_shipping_message,this.form.store_shipping_messageleft,255);">{$set_store_shipping_message|nl2br}</textarea><br />
<input readonly="readonly" type="text" name="store_shipping_messageleft" size="3" maxlength="3" value="{$store_shipping_messageleft}" tabindex="101" class="forminput" /> Characters left

</div>

</td>

</tr>

</table>


<table class="table-search">

<thead>

<tr>
<th>Choose your preferred workflow for selling images</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<td>

<div class="search-box" style="border-right:1px solid #BBB; min-height:210px;">

<input type="radio" name="store_type" value="10" class="forminput" {if $set_store_type == "10"} checked="checked"{/if} /> <b>Quote by contents of the cart</b><br />

<p style="padding-left:22px;">User will be asked to provide information about how they intend to use the images they want.  Administrator can they reply with a quote price for the images.</p>

<input type="radio" name="store_type" value="11" class="forminput" {if $set_store_type == "11"} checked="checked"{/if} /> <b>Quote by each image in the cart</b><br />

<p style="padding-left:22px;">User will be asked for details of how they intend to use each individual image in the cart.  Once this information has been supplied, you will be able to respond with a quote price for the images.</p>
	
</div>

<div class="search-box-wide" style="border-right:1px solid #BBB; min-height:210px;">

<input type="radio" name="store_type" value="12" class="forminput" {if $set_store_type == "12"} checked="checked"{/if} /> <b>Automatic price calculation for each image in the cart</b><br />

<p style="padding-left:22px;">User will be asked to complete a form providing information about how each image in the cart will be used.  Pixaria will then automatically generate a price for all the images in their cart.</p>

<p style="padding-left:22px;"><input type="checkbox" name="store_quote_option" class="forminput" {if $set_store_quote_option == "1"} checked="checked"{/if} /> <b>Enable quoting as an additional option for buyers</b></p>
		
<p style="padding-left:22px;"><input type="checkbox" name="store_calculate_login" class="forminput" {if $set_store_calculate_login == "1"} checked="checked"{/if} /> <b>Enable calculator for unregistered users</b></p>

<ul class="actionlist" style="padding-left:22px;"><li><a href="{$base_url}{$fil_admin_calculators}">Administer price calculation rules</a></li></ul></p>

</div>

</td>
</tr>

</tbody>


<table class="table-search">

<tr>
<th>Image download settings</th>
</tr>

<tr>
<td>

<div class="search-box" style="border-right:1px solid #BBB; min-height:170px;">

<b>Image download purchases</b><br />

<p>Do you wish to allow users to download high-resolution images once payment has been made for an image?</p>

<input type="checkbox" name="func_store_download" class="forminput" {if $set_func_store_download == "1"} checked="checked"{/if} /> <b>Enable instant download</b><br />
</div>

<div class="search-box" style="border-right:1px solid #BBB; min-height:170px;">

<b>Maximum allowed download attempts:</b>

<p>How many times do you want to allow a user to download a purchased high-resolution image?</p>

<select name="store_download_attempts" style="width:100%;" class="forminput">
<option value="1"{if $set_store_download_attempts == "1"} selected="selected"{/if}>1</option>
<option value="2"{if $set_store_download_attempts == "2"} selected="selected"{/if}>2</option>
<option value="3"{if $set_store_download_attempts == "3"} selected="selected"{/if}>3</option>
<option value="4"{if $set_store_download_attempts == "4"} selected="selected"{/if}>4</option>
<option value="5"{if $set_store_download_attempts == "5"} selected="selected"{/if}>5</option>
<option value="6"{if $set_store_download_attempts == "6"} selected="selected"{/if}>6</option>
<option value="7"{if $set_store_download_attempts == "7"} selected="selected"{/if}>7</option>
<option value="8"{if $set_store_download_attempts == "8"} selected="selected"{/if}>8</option>
<option value="9"{if $set_store_download_attempts == "9"} selected="selected"{/if}>9</option>
<option value="10"{if $set_store_download_attempts == "10"} selected="selected"{/if}>10</option>
</select>


</div>

<div class="search-box" style="border-right:1px solid #BBB; min-height:170px;">

<b>Free high-res image downloads</b><br /><br />

<input type="checkbox" name="func_downloads" class="forminput" onclick="javascript:document.store.func_downloads.checked ? Effect.toggle('downloads_settings','slide',{literal}{duration:0.3}{/literal}) : Effect.toggle('downloads_settings','slide',{literal}{duration:0.3}{/literal})" {if $set_func_downloads == "1"} checked="checked"{/if} /> Enable free downloads<br />

	<div id="downloads_settings" {if $set_func_downloads == '0'}style="display:none;"{/if}>
	<table cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-left:20px;">
	<input type="radio" name="func_downloads_user" class="forminput" value="10" {if $set_func_downloads_user == "10"} checked="checked"{/if} /> Enable for all users<br />
	<input type="radio" name="func_downloads_user" class="forminput" value="11" {if $set_func_downloads_user == "11"} checked="checked"{/if} /> Enable for registered only<br />
	<input type="radio" name="func_downloads_user" class="forminput" value="12" {if $set_func_downloads_user == "12"} checked="checked"{/if} /> Enable for selected individual users<br />
	</td></tr></table>
	</div>

<br />


<b>Comp download links</b><br />

<input type="checkbox" name="func_download_comp" class="forminput" {if $set_func_download_comp == "1"} checked="checked"{/if}> Enable comp download link<br />

<br />



</div>

</td>
</tr>

</table>
	
	
<table class="table-search">

<tr>
<th>PayPal Integration</th>
</tr>

<tr>
<td>

<div class="search-box-wide" style="border-right:1px solid #BBB; min-height:120px;">

<b>How does PayPal integration work?</b>

<p>Integrating Pixaria's store functions with PayPal is simple.  All you need to do is ensure you have a verified <a href="https://www.paypal.com/uk/mrb/pal=RDEF2K3QCDV76" style="text-decoration:underline;">PayPal merchant account</a> and that you have activated the option to enable support for 'Instant Payment Notifications'.</p>

<p>When your PayPal account has been configured, you need to enter your PayPal account e-mail address and check the box to turn on Pixaria's support for accepting payments through PayPal.</p>

{if !$set_store_func_paypal || $set_store_paypal_email == ""}<!-- Begin PayPal Logo -->
<p align="center"><a href="https://www.paypal.com/uk/mrb/pal=RDEF2K3QCDV76" target="_blank"><img src="http://images.paypal.com/en_GB/i/bnr/paypal_mrb_banner.gif" border="0" alt="Sign up for PayPal and start accepting credit card payments instantly." title="Sign up for PayPal and start accepting credit card payments instantly."></a></p>
<!-- End PayPal Logo -->{/if}

</div>

<div class="search-box" style="border-right:1px solid #BBB; min-height:120px;">

<input type="checkbox" name="store_func_paypal" class="forminput" {if $set_store_func_paypal == "1"} checked="checked"{/if} /> <b>Enable payments by PayPal</b><br /><br />

<b>PayPal account e-mail address:</b><br />
<input type="text" name="store_paypal_email" value="{$set_store_paypal_email}" class="forminput" style="width:100%;" />

</div>

</td>

</tr>

</table>

	
	


<table class="table-search">

<tr>
<th>2Checkout integration</th>
</tr>

<tr>

<td>

<div class="search-box" style="border-right:1px solid #BBB; min-height:150px;">

<b>How does 2Checkout integration work?</b>

<p>2Checkout integration enables customers to make payment using a credit card or PayPal account.</p>

<p>To use 2Checkout with Pixaria, you must provide your vendor account number and secret word.  Details can be found in the documentation.</p>

</div>

<div class="search-box" style="border-right:1px solid #BBB; min-height:150px;">

<input type="checkbox" name="store_func_2co" class="forminput" {if $set_store_func_2co == "1"} checked="checked"{/if} onclick="Effect.toggle('currency-warning','blind',{literal}{duration:0}{/literal});" /> <b>Enable support for 2Checkout</b><br /><br />

<input type="checkbox" name="store_2co_demo" class="forminput" {if $set_store_2co_demo == "1"} checked="checked"{/if} /> <b>Use 2Checkout Demo Mode</b><br />
<p style="padding-left:22px;">Allows you to test the 2Checkout sales workflow.  Card payments don't get processed but Pixaria will treat them as successfully completed transactions.</p>

</div>

<div class="search-box" style="border-right:1px solid #BBB; min-height:150px;">

<b>2Checkout vendor account number:</b><br />
<input type="text" name="store_2co_sid" value="{$set_store_2co_sid}" class="forminput" style="width:100%;" /><br /><br />

<b>2Checkout secret word:</b><br />
<input type="text" name="store_2co_secret" value="{$set_store_2co_secret}" class="forminput" style="width:100%;" /><br />

</div>

</td>

</tr>

</table>



<table class="table-search">

<tr>
<th>Store currency</th>
</tr>

<tr>
<td>

<div class="search-box" style="border-right:1px solid #BBB; min-height:165px;">

<b>Supported currencies</b>

<p>Pixaria supports a range of major currencies with the default list based on those supported by PayPal.</p>

<p>If you wish to use 2Checkout support in Pixaria, the list of available currencies is reduced as not all currencies supported by PayPal are able to be used with 2Checkout.</p>

</div>

<div class="search-box" id="currency-warning" style="border-right:1px solid #BBB; min-height:165px;{if $set_store_func_2co == "0"} display:none;{/if}">

	<div class="notification-warning">
	
	<h1>Warning!</h1>
			
	<p>2Checkout doesn't support these currencies: Czech Koruna, Polish Zloty, Singapore Dollars, Hungarian Forint</p>
	
	<p>You can't display prices or accept payment in these currencies if 2Checkout is enabled!</p>
	
	</div>

</div>

<div class="search-box" style="border-right:1px solid #BBB; min-height:165px;">

<b>Select a currency for payments:</b><br />

<select name="store_currency" style="width:100%;" class="forminput" onchange="checkCurrency(this.options[selectedIndex].value);" size="9">
<option value="AUD"{if $set_store_currency == "AUD"} selected="selected"{/if}>Australian Dollars</option>
<option value="CAD"{if $set_store_currency == "CAD"} selected="selected"{/if}>Canadian Dollars</option>
<option value="CZK"{if $set_store_currency == "CZK"} selected="selected"{/if}>Czech Koruna</option>
<option value="DKK"{if $set_store_currency == "DKK"} selected="selected"{/if}>Danish Kroner</option>
<option value="EUR"{if $set_store_currency == "EUR"} selected="selected"{/if}>Euro</option>
<option value="HUF"{if $set_store_currency == "HUF"} selected="selected"{/if}>Hungarian Forint</option>
<option value="HKD"{if $set_store_currency == "HKD"} selected="selected"{/if}>Hong Kong Dollars</option>
<option value="ILS"{if $set_store_currency == "ILS"} selected="selected"{/if}>Israeli New Shekel</option>
<option value="JPY"{if $set_store_currency == "JPY"} selected="selected"{/if}>Japanese Yen</option>
<option value="MXN"{if $set_store_currency == "MXN"} selected="selected"{/if}>Mexican Peso</option>
<option value="NZD"{if $set_store_currency == "NZD"} selected="selected"{/if}>New Zealand Dollars</option>
<option value="NOK"{if $set_store_currency == "NOK"} selected="selected"{/if}>Norwegian Kroner</option>
<option value="PLN"{if $set_store_currency == "PLN"} selected="selected"{/if}>Polish Zloty</option>
<option value="GBP"{if $set_store_currency == "GBP"} selected="selected"{/if}>Pound Sterling (UK)</option>
<option value="CHF"{if $set_store_currency == "CHF"} selected="selected"{/if}>Swiss Francs</option>
<option value="SGD"{if $set_store_currency == "SGD"} selected="selected"{/if}>Singapore Dollars</option>
<option value="SEK"{if $set_store_currency == "SEK"} selected="selected"{/if}>Swedish Kronor</option>
<option value="USD"{if $set_store_currency == "USD"} selected="selected"{/if}>United States Dollars</option>
</select>

</div>

</td>
</tr>

</table>


<table class="table-search">

<tr>
<th>Terms and conditions of sale</th>
</tr>

<tr>
<td>

<div class="search-box" style="border-right:1px solid #BBB; min-height:200px;">

<b>Terms and conditions message</b>

<p>If you wish, you can show a message explaining the terms and conditions of making a purchase through your online store.</p>

<p>To activate the message and require customers to agree to the terms before completing payment, check the box below and past in your terms and conditions in the space on the right.</p>

<input type="checkbox" name="store_func_terms_conditions" class="forminput" {if $set_store_func_terms_conditions == "1"} checked="checked"{/if} /> <b>Require agreement to terms</b><br /><br />

</div>

<div class="search-box-wide" style="border-right:1px solid #BBB; min-height:200px;">

<b>Terms and conditions message:</b><br />

<textarea name="store_terms_conditions" class="forminput" style="width:100%;" rows="12" cols="15">{$set_store_terms_conditions}</textarea><br />

</div>

</td>

</tr>

</table>

<table class="table-search">

<tr>
<th>Offline payment settings</th>
</tr>

<tr>
<td>

<div class="search-box" style="border-right:1px solid #BBB; min-height:230px;">

<b>Accepted offline payment methods</b>

<p>Pixaria allows your users to submit a cart and make payment using an offline method such as a bank checque.</p>

<input type="checkbox" name="store_func_cheque" class="forminput" {if $set_store_func_cheque == "1"} checked="checked"{/if} /> <b>Allow payments by cheque</b><br /><br />

<input type="checkbox" name="store_func_postalorder" class="forminput" {if $set_store_func_postalorder == "1"} checked="checked"{/if} /> <b>Allow payments by postal order</b><br /><br />

<input type="checkbox" name="store_func_moneyorder" class="forminput" {if $set_store_func_moneyorder == "1"} checked="checked"{/if} /> <b>Allow payments by money order</b><br /><br />

<input type="checkbox" name="store_func_banktransfer" class="forminput" {if $set_store_func_banktransfer == "1"} checked="checked"{/if} /> <b>Allow payments by bank transfer</b><br /><br />

</div>

<div class="search-box-wide" style="border-right:1px solid #BBB; min-height:230px;">

<b>Detailed payment instructions (your address etc.):</b><br />

<p>Enter details of how your customers can pay for images by cheque, bank transfer or postal order.  This information will be shown on the cart pages of your site.</p>

<textarea name="store_offline_payment" class="forminput" style="width:100%;" rows="6" cols="15" onkeydown="textCounter(this.form.store_offline_payment,this.form.store_offline_paymentleft,1000);" onkeyup="textCounter(this.form.store_offline_payment,this.form.store_offline_paymentleft,1000);">{$set_store_offline_payment}</textarea><br />

<input readonly="readonly" type="text" name="store_offline_paymentleft" size="4" maxlength="4" value="{$store_offline_paymentleft}" tabindex="101" class="forminput" /> Characters left

<br /><br />

<input type="submit" value="Save changes to your store settings" style="width:240px; float:right;" class="formbutton" />

</div>

</td>

</tr>

</table>



</form>


</div>

</div>



<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.footer.tpl"}


