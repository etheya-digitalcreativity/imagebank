{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">









<table class="table-text">

<tr>

<th>Pixies are small, dynamically loadable modules that allow you to enhance your Pixaria website without having to learn PHP or make your own templates.</th>

<td>

<p class="title">Browse Installed Pixies</p>

<p>Pixies are small modules that allow you to enhance your Pixaria site by adding features not included in the main package.  To begin using Pixies on your site, you should familiarise yourself with editing the Smarty template files that come with Pixaria.</p>

<p>To load a Pixie in a template, simply call the 'load_pixie' function at the place in your template where you want the Pixie to display.  You will need to make sure you include the appropriate options for the Pixie you're using by checking the documentation that comes with all Pixie modules.</p>

</td>
<td>&nbsp;</td>
</tr>

</table>




<table class="list">

<tr>
<th>Name</th>
<th>Version</th>
<th>Action</th>
</tr>

{section name=pixie_list loop=$installed_pixie_dir}
		
<tr class="{cycle values='row1,row2'}">
<td style="height:30px">{$installed_pixie_name[pixie_list]}</td>
<td>{$installed_pixie_version[pixie_list]}</td>
<td><ul class="actionlist"><li><a href="{$base_url}{$fil_admin_pixie}?cmd=showPluginInfo&amp;pixie_view={$installed_pixie_dir[pixie_list]}">Show details and documentation</a></li></ul></td>
</tr>
	
{/section}

</table>




</div>

</div>


<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}


