{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">









<table class="table-text">

<tr>

<th>{$pixie_name} {$pixie_version}</th>

<td>

<h1>Description</h1>

<p>{$pixie_description}</p>

{if $pixie_warning != ""}

{include_template file="admin.snippets/admin.html.notification.note.tpl" form_title_text=$pixie_warning}

{/if}

</td>
<td>&nbsp;</td>
</tr>

</table>




<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">Code samples to include in your templates</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	{section name="examples" loop=$pixie_examples.PixieExample}
			
		<tr class="{cycle values='list-one,list-two'}">
		
		<th>{$smarty.section.examples.index+1}) {$pixie_examples.PixieExample[examples].PixieExampleDescription}</th>
		
		<td>
	
			<p class="code">{$pixie_examples.PixieExample[examples].PixieExampleCode}</p>
			
		</td>
		
		<td>&nbsp;</td>
		
		</tr>
	
	{/section}

</tbody>

</table>

<table class="table-listbox">

	<tr>
	<th>Name</th>
	<th>Description</th>
	<th>Notes</th>
	<th>Type</th>
	<th>Allowed values</th>
	</tr>

	{section name=options loop=$pixie_options}
	
	<tr class="{cycle values='list-one,list-two'}">
	
	<td style="height:30px;">{$smarty.section.options.index+1}) {$pixie_options[options].OptionName}</td>
	
	<td>{$pixie_options[options].OptionDescription}</td>
		
	<td>{$pixie_options[options].OptionNotes}</td>
		
	<td>{$pixie_options[options].OptionType}</td>
		
	<td>
	{section name=OptionValues loop=$pixie_options[options].OptionValues.OptionAllowedValue}
				
		[ {$pixie_options[options].OptionValues.OptionAllowedValue[OptionValues]} ]
			
	{/section}
	</td>
	
	</tr>
	
	{sectionelse}
	
	<tr class="{cycle values='list-one,list-two'}">
	<td colspan="5" style="height:30px;">This plugin has no options.</td>
	</tr>
	
	{/section}

</table>




</div>

</div>


<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}


