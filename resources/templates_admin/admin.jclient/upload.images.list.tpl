{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">



{include_template file="admin.snippets/admin.notification.complete.html" form_title_text="Your request has been completed!"}



<table class="table-text">

<tr>
<th>This page lists all the images you've uploaded using the FTP upload applet.</th>
<td>

<h1>Listing images you've uploaded</h1>

<p>The files you uploaded are listed below.  Before you can import them into your image library, they need to be moved into a folder inside your incoming directory.</p>

<a href="{$base_url}{$fil_admin_jclient}" style="text-decoration:underline;">FTP upload applet</a>

</td>
<td>&nbsp;</td>
</tr>

</table>





{if ($display=="TRUE") }

	<form method="post" id="image_list" action="{$base_url}{$fil_admin_jclient}" target="_top">
	
	<table class="table-listbox">

	<tr>
		<th style="padding-left:0px; width:20px;"><input title="Toggle all checkboxes" type="checkbox" name="toggle" onclick="javascript:this.checked ? selectAll('image_list','images[]') : deselectAll('image_list','images[]');" checked="checked" /></th>
		<th>File name</th>	
		<th>Comp present</th>	
		<th>Original Present</th>	
		<th>File size</th>	
		<th>Image dimensions</th>	
	</tr>
	
	{section name=files loop=$filename}
		
		<tr class="{cycle values='list-one,list-two'}">
		
		<td style="padding-left:0px; width:20px; height:40px;"><input type="checkbox" border="0" name="images[]" value="{$filename[files]}" class="forminput" checked="checked" /></td>
		
		<td><img src="{$base_url}resources/images/icons/22x22/image-x-generic.gif" alt="" title="" width="22" height="22" style="vertical-align:middle;" />&nbsp;&nbsp;{$filename[files]|truncate:50}</td>
		
		<td>{if $comp_valid[files]}
			<img src="{$base_url}resources/images/furniture/tick.gif" width="32" height="32" border="0" alt="File present" title="File present">
		{else}
			<img src="{$base_url}resources/images/furniture/cross.gif" width="32" height="32" border="0" alt="File not present" title="File not present">
		{/if}</td>
		<td>{if $orig_valid[files]}
			<img src="{$base_url}resources/images/furniture/tick.gif" width="32" height="32" border="0" alt="File present" title="File present">
		{else}
			<img src="{$base_url}resources/images/furniture/cross.gif" width="32" height="32" border="0" alt="File not present" title="File not present">
		{/if}</td>
		
		<td>{$filesize[files]}kb</td>
		
		<td>{$width[files]} x {$height[files]}</td>
		
		</tr>
	
	{/section}
	
	</table>

	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">Options for these image files:</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Specify a directory to store the images in</th>
	<td>
	
	If you wish, you can specify the name of a directory in which to store these images when the thumbnails are generated and they are moved into your library.<br /><br />Please only use letters, numbers, hyphen '-' and underscore '_' only.  If you do not supply a name, Pixaria will create one for you.<br /><br />
	
	{if $show_directories}
	
	<select name="directory" onchange="javascript:this.form.directory_name.value=this.value;" style="width:100%;" class="forminput">
	<option value="">Choose existing folder in the 'incoming' directory</option>
	<option value="">------------------------------</option>
	{section name="inc" loop=$incoming}
	<option value="{$incoming[inc]}">{$incoming[inc]}</option>
	{/section}
	<option value=""></option>
	<option value="">Choose existing folder in the 'library' directory</option>
	<option value="">------------------------------</option>
	{section name="lib" loop=$library}
	<option value="{$library[lib]}">{$library[lib]}</option>
	{/section}
	</select><br />
	
	{/if}
	
	<input type="text" name="directory_name" value="" style="width:100%;" class="forminput" /></td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>Choose required action:</th>
	<td>
	<input type="radio" name="cmd" id="processUploadedFiles" value="processUploadedFiles" checked="checked" /> <label for="processUploadedFiles">Import selected files into the library</label><br />
	<input type="radio" name="cmd" id="formDeleteImageFiles" value="formDeleteImageFiles" /> <label for="formDeleteImageFiles">Delete selected files from the server</label>
	</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td><input type="submit" value="Proceed to the next step" style="width:100%;" class="formbutton" /></td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
	</table>
			
	</form>

{else}

	<table class="table-form">
	
	<thead>
	
	<tr>
		<th>No images in selected source.</th>	
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two'}">
	
		<td style="height:32px;">
		
		There are no images to view in this selection.  If you were expecting images you had uploaded to be visible, please check the FTP upload path is correct in the General Settings control panel.
		
		</td>
		
	</tr>
	
	</tbody>
	
	</table>
	
{/if}





</div>

</div>


<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}