
<table class="table-text">

<tr>
<th>Java FTP applet settings</th>
<td>

<h1>How to configure FTP uploads</h1>

<p>Pixaria includes a custom version of the JFileUpload FTP applet which enables support for drag and drop uploading of large images to your web site with an automatic resizing function.</p>

<p>To get started, you need to enter FTP login information for your server and set the path.</p>

<p>The upload applet is commercial software and to use it, you will first need to purchase a license from the <a href="http://www.jfileupload.com/" target="_blank" style="text-decoration:underline;">developer</a>.</p>



</td>
<td>&nbsp;</td>
</tr>

</table>



<form action="{$base_url}{$fil_admin_jclient}" method="post">

<input type="hidden" name="cmd" value="actionSaveSettings" />

<table class="table-search">

<tr>
<th>Java FTP applet configuration</th>
</tr>

<tr>
<td>

<div class="search-box" style="border-right:1px solid #BBB; min-height:320px;">

<b>What is Java based FTP uploading?</b>

<p>Pixaria includes built in support for the <a href="http://www.jfileupload.com/">JFileUpload FTP applet</a> which allows you to upload images to your site by FTP right from within your browser window.</p>

<p>For security reasons it is highly recommended that you create an FTP account which only has access to the incoming directory of your server.<p>

<p><b>JFileUpload FTP is <a href="http://www.jfileupload.com/" target="_blank" style="text-decoration:underline;">commercial software</a> provided by a third party, you should purchase a license to use it with Pixaria.</b></p>

<p><b>Please note that the FTP applet support in Pixaria is still in beta which means you should expect problems!!</b></p>

</div>

<div class="search-box" style="border-right:1px solid #BBB; width:500px; min-height:320px;">

<b>FTP address for your server</b><br />

Please provide the hostname or IP address of your server (e.g. ftp.mysite.com)

<input type="text" name="ftp_address" value="{$set_ftp_address}" style="width:100%" class="forminput" />

<br /><br />

<b>Path to your 'javaupload' directory</b><br />

This is the server file path from the place where your FTP account logs in to the location of the 'javaupload' directory on your site.<br /><br />It should end with a forward slash: e.g. httpdocs/pixaria/resources/javaupload/

<input type="text" name="ftp_path" value="{$set_ftp_path}" style="width:100%" class="forminput" />

<br /><br />

<b>FTP account username:</b><br />

<input type="text" name="ftp_username" value="{$set_ftp_username}" style="width:100%" class="forminput" />

<br /><br />

<b>FTP account password:</b><br />

<input type="text" name="ftp_password" value="{$set_ftp_password}" style="width:100%" class="forminput" />

<br /><br />
<input type="submit" value="Save the settings" style="width:100%;" class="formbutton" />

</div>

</td>

</tr>

</table>

</form>
