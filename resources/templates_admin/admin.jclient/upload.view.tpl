{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">



{include_template file="admin.snippets/admin.notification.complete.html" form_title_text="Your request has been completed!"}



{if $set_ftp_address != "" && $set_ftp_username != "" && $set_ftp_password != ""}

<table class="table-text">

<tr>
<th>From this page, you can upload images to your website using the JFileUpload FTP applet which includes automatic resizing of large images.</th>
<td>

<h1>FTP Upload with Resize</h1>

<p>Pixaria now includes a custom version of the JFileUpload FTP applet which enables support for drag and drop uploading of large images to your web site with an automatic resizing function.</p>

<p>To use the upload applet, you need to purchase a license from the developer.  Once the license is installed, you can upload files by dragging them into the upload applet box in the form below.</p>

<a href="{$base_url}{$fil_admin_jclient}?cmd=showFormSettings" style="text-decoration:underline;">FTP upload applet settings</a>

</td>
<td>&nbsp;</td>
</tr>

</table>



<table class="table-search">

<thead>
<th>Upload images using the Java FTP applet</th>
</thead>

<tbody>

<td>

<div class="search-box" style="border-right:1px solid #BBB; min-height:290px; width:750px;">

<object classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93" width="750" height="280" name="fileupload" codebase="http://java.sun.com/update/1.4.2/jinstall-1_4-windows-i586.cab#Version=1,4,0,0" style="border:1px solid #BBB;">

<applet code="jfileupload.upload.client.MApplet.class" java_codebase="../ext/jclient/" archive="jfileupload.jar,ftpimpl.jar,cnet.jar,clogging.jar,ifilter.jar" width="750" height="280" name="fileupload" style="border:1px solid #BBB;">

<param name="code" value="jfileupload.upload.client.MApplet.class">
<param name="codebase" value="../ext/jclient/">
<param name="archive" value="jfileupload.jar,ftpimpl.jar,cnet.jar,clogging.jar,ifilter.jar">
<param name="name" value="fileupload">
<param name="type" value="application/x-java-applet;version=1.4">
<param name="scriptable" value="true">
<param name="url" value="ftp://{$set_ftp_address}/">
<param name="param1" value="username">
<param name="value1" value="{$set_ftp_username}">
<param name="param2" value="password">
<param name="value2" value="{$set_ftp_password}">
<param name="param3" value="pasv">
<param name="value3" value="true">
<param name="param4" value="account">
<param name="value4" value="{$account_path}">

<param name="param5" value="accountcreation">
<param name="value5" value="true">
<param name="param6" value="accountcreationchmod">
<param name="value6" value="777">
<param name="param7" value="chmodafterupload">
<param name="value7" value="777">

<param name="additionalfilterid" value="webres,orig">

<param name="webresfilter" value="jfileupload.upload.client.filter.ImageFilter">
<param name="webresfilterparam1" value="maxwidth">
<param name="webresfiltervalue1" value="630">
<param name="webresfilterparam2" value="maxheight">
<param name="webresfiltervalue2" value="630">
<param name="webresfilterparam3" value="id">
<param name="webresfiltervalue3" value="comp_">
<param name="webresfilterparam4" value="renamepolicy">
<param name="webresfiltervalue4" value="custom">

<param name="origfilter" value="jfileupload.upload.client.filter.MetaFilter">
<param name="origfilterparam1" value="id">
<param name="origfiltervalue1" value="original_">
<param name="origfilterparam2" value="renamepolicy">
<param name="origfiltervalue2" value="custom">

<param name="resources" value="i18n">
<param name="mode" value="ftp">

<param name="whitelist" value="*.jpg">
<param name="hiddenfile" value="deny">
<param name="policy" value="ignore">
<param name="regfile" value="../ext/jclient/license.oxw">

</applet>

</object>

<br />

<br />

<button onclick="javascript:location.href='{$base_url}{$fil_admin_jclient}?cmd=showUploadedFiles';" class="formbutton" style="width:295px; float:right;">Show the uploaded files</button>

</div>

</td>

</tr>

</tbody>

</table>


<div id="filecontainer"></div>

{else}


{include_template file="admin.jclient/settings.form.tpl"}


{/if}

</div>

</div>


<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}




