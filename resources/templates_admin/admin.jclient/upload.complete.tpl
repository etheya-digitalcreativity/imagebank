{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">




{if $error}

	<table class="table-text">
	
	<tr>
	<th>Unfortunately, there was an error during the processing of your images.</th>
	<td>
	
	<h1>An error has occurred</h1>
	
	<p>There was a problem with the information you submitted on the previous page, please take a moment to review these messages:</p>

	<ol>
	{section name="error" loop=$error_message}
		
		<li class="html-error-warning">{$error_message[error]}</li>
	
	{/section}
	</ol>
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	</table>

{else}

	<table class="table-text">
	
	<tr>
	<th>The images you uploaded have been processed and can now be imported.</th>
	<td>
	
	<h1>Your images can now be imported</h1>
	
	
	<p>Your images have been moved into the folder '{$directory_name}' in the incoming directory and can now be imported into your library.</p>
	
	<button onclick="javascript:location.href='{$base_url}{$fil_admin_library_import}?cmd=checkAllImagesPresent&directory={$directory_name}';" class="formbutton" style="width:100%;">Import the images into your library</button>
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	</table>

{/if}





</div>

</div>


<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}