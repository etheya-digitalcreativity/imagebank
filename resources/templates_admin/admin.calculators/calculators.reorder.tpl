{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">



<script language="JavaScript" type="text/javascript">
<!--
{literal}

function checkForm() {

	var formName = document.forms[1];
	
	if (formName.calc_name.value == "") {
	
		alert('You must enter a name for this rule');
	
		return false;
	
	}
	
	return true;
	
}

{/literal}
// -->
</script>

<table class="table-text">

<tr>
<th>This form allows you to change the order that pricing rules are listed and processed when a user buys an image.</th>

<td>

<h1>Price calculation order</h1>

<p>When a user buys an image and automatic price calculation is turned on, they will be asked to specify answers to each of the pricing rule questions listed below.</p>

<p>The order that the resulting calculations are processed for each option will determine the end price so it's important that you arrange your rules in the correct order to give the pricing you require.</p>

{if $show_list}

	<h1>Drag and drop rules</h1>
	
	<p>Drag and drop the rule items into the order you wish to use and save changes to store your preferred order.</p>

{else}

	<h1>There are no rules to show</h1>
	
	<p>There are no price calculation rules to display.</p>

{/if}

</td>
<td>&nbsp;</td>
</tr>

</table>



{if $show_list}


	<div style="padding:5px;">
	
	<table style="background:#AAA; width:100%; border-collapse:collapse; border-spacing:0px;">
	
	<tr>
	
	<td style="font-weight:bold; height:25px; color:#FFF; font-size:11pt;">Starting Price</td>
	
	</tr>
	
	</table>
	
	</div>

	<div style="padding:5px;" id="rules">
	
	{section name="rules" loop=$calc_id}
	
		<div style="width:100%; float:left; cursor:move;" id="rule_{$calc_id[rules]}">
	
		<table class="table-listbox" style="-moz-border-radius:10px;">
		
		<tr style="background:#AAA;">
		<td colspan="3" style="font-weight:bold; text-align:left; height:25px; color:#FFF; font-size:11pt;">{$calc_name[rules]}</td>
		</tr>
		
		<tr>
			<th style="width:33%;">Title of this rule</th>	
			<th style="width:33%;">Options for this rule</th>
			<th style="width:33%;">Option function</th>
		</tr>
		
		{if $calc_ar_name[rules].1 != ""}
		
			{section name="options" loop=$calc_ar_id[rules]}
			
			<tr class="{cycle values='list-one,list-two'}">
			
			<td style="height:23px;">&nbsp; &nbsp; &nbsp; &nbsp; Option {$smarty.section.options.index+1}</td>
			
			<td>{$calc_ar_name[rules][options]}</td>
			
			<td>{$calc_ar_function[rules][options]}</td>
			
			</tr>
			
			{/section}
		
		{else}
		
			<tr class="{cycle values='list-one,list-two'}">
			
			<td colspan="3" style="height:23px;">&nbsp; &nbsp; &nbsp; &nbsp; There are no price calculation options to show for this rule.</td>
			
			</tr>
		
		{/if}
			
		</table>
		
		</div>
		
	{/section}	
	
	<br clear="all" />
	
	</div>
	
	
	<div style="padding:5px;">
	
	<table style="background:#AAA; width:100%; border-collapse:collapse; border-spacing:0px;">
	
	<tr>
	
	<td style="font-weight:bold; height:25px; color:#FFF; font-size:11pt;">Final Price</td>
	
	</tr>
	
	</table>
	
	</div>

	<script type="text/javascript" language="javascript">{literal}
	// <![CDATA[
	Sortable.create("rules", {tag:'div',overlap:'horizontal',constraint: 'vertical',
	
	onUpdate:function() {
	
	var list = document.getElementById('rules');
	var items = list.getElementsByTagName('div');
	var ids = '';
		
	for (var i = 0; i < items.length; i++)  {
		if (i > 0) ids += '|';
		var id = items[i].getAttribute('id');
		ids += id;
	}
	
	document.rules_edit.rules_order.value = ids;
	return true;
	
	}
	
	});
	// ]]>{/literal}
	</script>

	
	
	<form name="rules_edit" action="{$base_url}{$fil_admin_calculators}" method="post" onsubmit="return saveRules();">
	
	<input type="hidden" name="cmd" value="saveRuleOrder" />
	
	<input type="hidden" name="rules_order" value="" />
	
	<table class="table-text">
	
	<tr>
	<th>&nbsp;</th>
	
	<td>
	
	<h1>Saving the changes</h1>
	
	<p>Once you are happy with the order in which your pricing rules are arranged, you will need to save the order:</p>
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr>
	<td>&nbsp;</td>
	<td><input type="submit" value="Save changes to rule order" style="width:100%;" class="formbutton" /></td>
	<td>&nbsp;</td>
	</tr>
	
	</table>
	
	</form>
	
{/if}

</div>

</div>

<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}


