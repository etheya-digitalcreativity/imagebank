{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

<!-- INCLUDE THE TOP HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">
	
<form action="{$base_url}{$fil_admin_calculators}" name="create" method="post">

<input type="hidden" name="cmd" value="actionSaveOptionEdits" />

<input type="hidden" name="ar_id" value="{$ar_id}" />

<input type="hidden" name="calc_id" value="{$calc_id}" />


{if $problem}

	<table class="table-text">

	<tr>
	<th>There was an error in the information you submitted, please take a moment to review the problem and then go back to correct the problem.</th>
	<td>
	
	<h1>Warning</h1>
	
	<p>There was a problem with the information you entered for this option, please review the errors and go back to correct them.</p>
	
	<ol>
	
	{section name="problem" loop=$problem_message}
	<li class="html-error-warning">{$problem_message[problem]}</li>
	{/section}
	
	</ol>
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr>
	<td>&nbsp;</td>
	<td><input type="button" onclick="javascript:history.go(-1);" value="Go back to the previous page" style="width:100%;" class="forminput"></td>
	<td>&nbsp;</td>
	</tr>

	</table>

{else}

	<table class="table-text">

	<tr>
	<th>Please take a moment to confirm the settings for this option.</th>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	</tr>

	</table>
	
	
	
	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">Confirm changes to this option:</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Option name:</th>
	<td>{$ar_name}
	<input type="hidden" name="ar_name" value="{$ar_name}" class="forminput" style="width:100%;" />
	</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Effect of this option on the price:</th>
	<td>{$ar_function}
	<input type="hidden" name="ar_factor" value="{$ar_factor}" class="forminput" style="width:100%;" />
	<input type="hidden" name="ar_operand" value="{$ar_operand}" class="forminput" style="width:100%;" />
	</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>Description of this option:</th>
	<td>{$ar_description}
	<input type="hidden" name="ar_description" value="{$ar_description}" class="forminput" style="width:100%;" />
	</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td><input type="submit" value="Save changes to this option" style="width:100%;" class="formbutton"></td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
	</table>

{/if}

</form>

</div>

</div>

<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.footer.tpl"}


