{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

<!-- INCLUDE THE TOP HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">

	
<form action="{$base_url}{$fil_admin_calculators}" name="create" method="post">

<input type="hidden" name="cmd" value="actionSaveNewRule" />

{if $problem}
	
	<table class="table-text">

	<tr>
	<th>There was an error in the information you submitted, please take a moment to review the problem and then go back to correct the problem.</th>
	<td>
	
	<h1>Warning</h1>
	
	<p>There was a problem with the information you entered for this rule, please review the errors and go back to correct them.</p>
	
	<ol>
	
	{section name="problem" loop=$problem_message}
	<li class="html-error-warning">{$problem_message[problem]}</li>
	{/section}
	
	</ol>
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr>
	<td>&nbsp;</td>
	<td><input type="button" onclick="javascript:history.go(-1);" value="Go back to the previous page" style="width:100%;" class="formbutton"></td>
	<td>&nbsp;</td>
	</tr>

	</table>
	
{else}
	
	<table class="table-text">

	<tr>
	<th>Please take a moment to confirm the settings for this pricing rule before you save it.</th>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	</tr>

	</table>
	
	<table class="table-form">
	
	<thead>
	
	<tr class="{cycle values='list-one,list-two'}">
	<tr>
	<th colspan="3">Check the details of this new rule:</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Title of this rule as it will appear in the menu or list:</th>
	<td>{$calc_name}
	<input type="hidden" name="calc_name" value="{$calc_name}" />
	</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>Description of what this rule is about:</th>
	<td>{$calc_description}
	<input type="hidden" name="calc_description" value="{$calc_description}" />
	</td>
	<td>&nbsp;</td>
	</tr>
	
	{if $calc_download_size == "on"}
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>&nbsp;</th>
	<td><b>This rule will affect image download sizes</b>
	<input type="hidden" name="calc_download_size" value="{$calc_download_size}" />
	</td>
	<td>&nbsp;</td>
	</tr>
	{/if}
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td><input type="submit" value="Save this as a new rule" style="width:100%;" class="formbutton"></td>
	<td>&nbsp;</td>
	</tr>

	</tbody>
	
	</table>
	
{/if}

</form>

</div>

</div>

<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.footer.tpl"}


