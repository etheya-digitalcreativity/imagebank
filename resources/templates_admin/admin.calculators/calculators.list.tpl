{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">



{include_template file="admin.snippets/admin.notification.complete.html" form_title_text="Your changes have been saved!"}


<form action="{$base_url}{$fil_admin_calculators}" name="create" method="post">

<input type="hidden" name="cmd" value="showFormPreviewNewRule" />

<table class="table-search">

<tr>
<th>Price calculation rule manager</th>
</tr>

<tr>

<td>

<div class="search-box" style="min-height:240px; width:350px;">

<h1>Create a new price calculation rule</h1>

<p>If your store is set up to automatically offer price calculations based on pre-defined rules, you can use this control panel to create, edit and delete those rules.</p>

<p>The type of calculation rules setup you choose to create is entirely up to you but some frequently used rules for generating prices would include pricing by:</p>

<ul>

<li>Position of the image on a printed page</li>
<li>Publication distribution figures</li>
<li>Duration of campaign</li>
<!-- <li>The size of the final downloaded image</li> -->

</ul>

</div>

<div class="search-box" style="border-right:1px solid #BBB; min-height:240px; width:350px;">

<b>Title of this rule:</b><br />
<input type="text" name="calc_name" class="forminput" style="width:100%;" /><br /><br />

<b>Description of this rule:</b><br />
<textarea name="calc_description" class="forminput" style="width:100%;" rows="6" cols="16"></textarea><br /><br />

<!--
<input type="checkbox" name="calc_download_size" class="forminput" /> <b>Download image size rule</b><br />
<p style="padding:0 0 0 21px; margin:0;">Checking this box will create a rule that affects the size of the image provided to the user.  You can only have one rule of this type.</p><br />
-->

<input type="submit" value="Proceed to next step" style="width:100%;" class="formbutton" />

</div>

</td>

</tr>

</table>

</form>



<table class="table-listbox">

<tr>
	<th>Title of this rule</th>	
	<th>Options for this rule</th>
	<th>Option function</th>
	<th>Actions</th>
</tr>

{if $show_list}

	{section name="rules" loop=$calc_id}
	
		<tr style="background:#CCC;">
		
		<td colspan="3" style="text-align:left; height:30px; font-weight:bold;">{$calc_name[rules]}</td>
		
		<td align="left">
		
		<ul class="actionlist">
		<li><a href="{$base_url}{$fil_admin_calculators}?cmd=showFormEditPricingRule&amp;calc_id={$calc_id[rules]}">Edit</a></li>
		<li><a href="javascript:confirmLink('{$base_url}{$fil_admin_calculators}?cmd=deletePricingRule&amp;calc_id={$calc_id[rules]}','Delete this rule and all the options inside it?');">Delete</a></li>
		</ul>

		</td>
		
		</tr>
		
		{if $calc_options_show[rules]}
		
			{section name="options" loop=$calc_ar_id[rules]}
			
			<tr class="{cycle values='list-one,list-two'}">
			
			<td style="text-align:left; height:30px;">&nbsp; &nbsp; &nbsp; &nbsp; Option {$smarty.section.options.index+1}</td>
			
			<td style="text-align:left; height:30px;">{$calc_ar_name[rules][options]}</td>
			
			<td style="text-align:left; height:30px;">{$calc_ar_function[rules][options]}</td>
			
			<td style="text-align:left; height:30px;">

			<ul class="actionlist">
			<li><a href="{$base_url}{$fil_admin_calculators}?cmd=showFormEditOption&amp;ar_id={$calc_ar_id[rules][options]}&amp;calc_id={$calc_id[rules]}">Edit</a></li>
			<li><a href="javascript:confirmLink('{$base_url}{$fil_admin_calculators}?cmd=actionDeletePricingOption&amp;ar_id={$calc_ar_id[rules][options]}&amp;calc_id={$calc_id[rules]}','Delete this rule option?');">Delete</a></li>
			</ul>
			
			</td>
			
			</tr>
			
			{/section}
		
		{else}
		
			<tr class="{cycle values='list-one,list-two'}">
			
			<td colspan="4" style="text-align:left; height:30px;">&nbsp; &nbsp; &nbsp; &nbsp; There are no price calculation options to show for this rule.</td>
			
			</tr>
		
		{/if}
		
	{/section}	

{else}

	<tr class="{cycle values='list-one,list-two'}">
	
	<td colspan="4" style="text-align:left; height:30px;">There are no price calculation rules to show.</td>
	
	</tr>

{/if}
	
</table>

</div>

</div>


{include_template file="admin.snippets/admin.html.footer.tpl"}


