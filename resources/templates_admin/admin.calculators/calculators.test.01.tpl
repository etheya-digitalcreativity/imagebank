{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

<!-- INCLUDE THE TOP HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">




<table class="table-text">

<tr>
<th>Use this control panel to test your pricing rules as if you were a customer buying an image.</th>

<td>

<h1>Price calculation test</h1>

<p>Use this form to test how your pricing calculation rules will work in a production environment.  You can enter any starting price you wish in the space provided.</p>

</td>
<td>&nbsp;</td>
</tr>

</table>






<form action="{$base_url}{$fil_admin_calculators}" name="create" method="post">

<input type="hidden" name="cmd" value="actionPriceRuleTest" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">Starting image price:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<td>Starting image price ({$set_store_currency}):</td>
<td><input type="text" name="img_price" value="{$set_store_default_price}" class="forminput" style="width:100%;" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>
	
<table class="table-form" border="0" cellpadding="3" cellspacing="0">

<thead>

<tr>
<th colspan="3">Choose options for the test:</th>
</tr>

</thead>

<tbody>

{if $show_list}

	{section name="rules" loop=$calc_id}
	
		<tr class="{cycle values='list-one,list-two'}">
		<th>{$calc_name[rules]}</th>
		
		<td>
		
		{if $calc_ar_name[rules].1 != ""}
			
			<select name="options[]" class="forminput" style="width:100%;">
			
			{section name="options" loop=$calc_ar_id[rules]}
			
			<option value="{$calc_ar_id[rules][options]}">{$calc_ar_name[rules][options]}</option>
						
			{/section}
			
			</select>
			
		{/if}
		
		</td>
		<td>&nbsp;</td>
		</tr>
		
	{/section}	

{/if}


<tr class="{cycle values='list-two,list-one'}">
<th>&nbsp;</th>
<td><input type="submit" value="Calculate price" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>

</div>

</div>


<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.footer.tpl"}


