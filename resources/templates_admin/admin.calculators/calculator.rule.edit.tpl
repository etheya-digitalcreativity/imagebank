{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

<!-- INCLUDE THE TOP HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">



{include_template file="admin.snippets/admin.notification.complete.html" form_title_text="Your changes to this rule have been saved!"}



<table class="table-text">

<tr>
<th>This form allows you to edit the details of a pricing calculation rule.</th>

<td class="contentcell">

{if !$problem}
	
	<h1>Edit this rule</h1>
	
	<p>Use this form to edit the details of a rule or add, edit and remove options from this rule.</p>
	
{else}

	<h1>Warning!</h1>
	
	<p>There was a problem with some of the information you entered for this rule, please review the errors and correct them:</p>
	
	<ol>
	
	{section name="problem" loop=$problem_message}
	<li class="html-error-warning">{$problem_message[problem]}</li>
	{/section}
	
	</ol>
	
	<br />
	
{/if}

</td>
<td>&nbsp;</td>
</tr>

</table>



<form action="{$base_url}{$fil_admin_calculators}" name="create" method="post">

<input type="hidden" name="cmd" value="actionEditPricingRule" />

<input type="hidden" name="calc_id" value="{$calc_id}" />

<table class="table-form" border="0" cellpadding="3" cellspacing="0">

<thead>

<tr>
<th colspan="3">Rule title and description:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>Title of this rule as it will appear in the menu or list:</th>
<td><input type="text" name="calc_name" value="{$calc_name}" class="forminput" style="width:100%;" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>Description of what this rule is about:</th>
<td><input type="text" name="calc_description" value="{$calc_description}" class="forminput" style="width:100%;" /></td>
<td>&nbsp;</td>
</tr>

	{if $calc_type == 1}
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>&nbsp;</th>
	<td><b>This rule will affect image download sizes</b>
	</td>
	<td>&nbsp;</td>
	</tr>
	{/if}
	
<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="submit" value="Edit settings" style="width:100%;" class="formbutton"></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>









<form action="{$base_url}{$fil_admin_calculators}" name="create" method="post">

<input type="hidden" name="cmd" value="showFormAddNewRuleOption" />

<input type="hidden" name="calc_id" value="{$calc_id}" />
	
<table class="table-form">

<thead>
<tr>
<th colspan="3">Add a new option to this rule:</th>
</tr>
</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>Option name:</th>
<td><input type="text" name="ar_name" value="{$ar_name_error}" class="forminput" style="width:100%;" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Operation to apply to the price:</th>
<td>

<select name="ar_operand" class="forminput" style="width:100%;">
<option value="0" selected="selected">No change</option>
<option value="0">&nbsp;</option>
<option value="1">Adds currency ({$set_store_currency})</option>
<option value="2">Subtracts currency ({$set_store_currency})</option>
<option value="3">Multiplies previous by</option>
<option value="4">Divides previous by</option>
</select>

</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Amount by which the operator affects the price:</th>
<td><input type="text" name="ar_factor" class="forminput" style="width:100%;" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Description of this option:</th>
<td><input type="text" name="ar_description" value="{$ar_description_error}" class="forminput" style="width:100%;" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-two,list-one'}">
<th>&nbsp;</th>
<td><input type="submit" value="Add new option" style="width:100%;" class="formbutton"></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>

<table class="table-listbox">

<tr>
	<th>Option name</th>	
	<th>Effect on the price</th>	
	<th>Description</th>
	<th>Action</th>
</tr>

{if $show_options}

	{section name="options" loop=$ar_id}
	
		<tr class="{cycle values='list-one,list-two'}">
		
		<td style="height:32px;">{$ar_name[options]}</td>
								
		<td>{$ar_function[options]}</td>
								
		<td>{$ar_description[options]}</td>
								
		<td><ul class="actionlist"><li><a href="{$base_url}{$fil_admin_calculators}?cmd=showFormEditOption&amp;ar_id={$ar_id[options]}&amp;calc_id={$calc_id}">Edit</a></li>
		<li><a href="javascript:confirmLink('{$base_url}{$fil_admin_calculators}?cmd=actionDeletePricingOption&amp;ar_id={$ar_id[options]}&amp;calc_id={$calc_id}','Delete this rule option?');">Delete</a></li></ul></td>
								
		</tr>
	
	{/section}

{else}

	<tr class="{cycle values='list-one,list-two'}">
	
	<td colspan="4" style="height:32px;">There are no price calculation options to show for this rule.</td>
	
	</tr>

{/if}

</table>

</div>

</div>


<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.footer.tpl"}


