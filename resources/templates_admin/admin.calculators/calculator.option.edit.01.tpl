{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

<!-- INCLUDE THE TOP HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">





<table class="table-text">

<tr>
<th>This form allows you to edit the details of a pricing calculation rule.</th>

<td>
	
	<h1>Edit this rule</h1>
	
	<p>Use this form to edit the details of a rule or add, edit and remove options from this rule.</p>
	
</td>
<td>&nbsp;</td>
</tr>

</table>




<form action="{$base_url}{$fil_admin_calculators}" name="create" method="post">

<input type="hidden" name="cmd" value="formPreviewOptionEdits" />

<input type="hidden" name="ar_id" value="{$ar_id}" />

<input type="hidden" name="calc_id" value="{$calc_id}" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">Edit this option</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>Option name:</th>
<td><input type="text" name="ar_name" value="{$ar_name}" class="forminput" style="width:100%;" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Operation to apply to the price:</th>
<td>

<select name="ar_operand" class="forminput" style="width:100%;">
<option value="0" selected="selected">No change</option>
<option value="0">&nbsp;</option>
<option value="1"{if $ar_operand==1} selected="selected"{/if}>Adds currency ({$set_store_currency})</option>
<option value="2"{if $ar_operand==2} selected="selected"{/if}>Subtracts currency ({$set_store_currency})</option>
<option value="3"{if $ar_operand==3} selected="selected"{/if}>Multiplies previous by</option>
<option value="4"{if $ar_operand==4} selected="selected"{/if}>Divides previous by</option>
</select>

</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Amount by which the operator affects the price:</th>
<td><input type="text" name="ar_factor" value="{$ar_factor}" class="forminput" style="width:100%;" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>Description of this option:</th>
<td><input type="text" name="ar_description" value="{$ar_description}" class="forminput" style="width:100%;" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="submit" value="Save changes to this option" style="width:100%;" class="formbutton"></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>







</form>

</div>

</div>

<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.footer.tpl"}


