{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

<!-- INCLUDE THE TOP HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">
	
	


<table class="table-text" >

<tr>
<th>Use this control panel to test your pricing rules as if you were a customer buying an image.</th>

<td>

<h1>Price calculation test</h1>

<p>Use this form to test how your pricing calculation rules will work in a production environment.  You can enter any starting price you wish in the space provided.</p>

</td>
<td>&nbsp;</td>
</tr>

</table>
	
	
	
	
	
<table class="table-form" >

<thead>

<tr>
<th colspan="3">Starting image price:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>Your starting price was ({$set_store_currency}):</th>
<td>{$set_store_symbol}{$old_price}</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>How this price was calculated:</th>
<td>

<table cellpadding="0" cellspacing="0" border="0">

{section name="calculation" loop=$arithmetic}

<tr>
<td><b>{$ar_name[calculation]}</b></td>
<td>&nbsp;&nbsp;&nbsp;</td>
<td>{$arithmetic[calculation]}</td>
</tr>

{/section}

</table>

</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>The price calculated was ({$set_store_currency}):</th>
<td>{$set_store_symbol}{$new_price}</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-two,list-one'}">
<th>&nbsp;</th>
<td><input type="button" value="Try a different calculation" onclick="history.go(-1);" style="width:100%;" class="formbutton"></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</div>

</div>


<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}


