{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">








<table class="table-text">

	<tr>
	<th>This page shows you when the any of the payment processing scripts for this Pixaria installation has been accessed.</th>
	<td>
	
	<h1>Show payment script access</h1>
	
	<p>This page lists the occasions when the payment processing scripts on this site have been accessed.  Normally, the script 'index.paypal.php' or 'index.pay2co.php' will only be accessed when payments are made.</p>
	
	<p>Looking through the entries on this page can help you to diagnose problems with your site's payment notification configuration and also provide evidence of attempts to bypass normal payment systems.</p>
	
	</td>
	<td>&nbsp;</td>
	</tr>

</table>




<table class="table-listbox">

	<tr>
		<th><a href="{$base_url}{$fil_admin_log_payscripts}&amp;sort_column=event_time&amp;sort_order={$sort_order}" class="plain">Date</a></th>	
		<th><a href="{$base_url}{$fil_admin_log_payscripts}&amp;sort_column=ip_address&amp;sort_order={$sort_order}" class="plain">IP Address</a></th>	
		<th><a href="{$base_url}{$fil_admin_log_payscripts}&amp;sort_column=host_name&amp;sort_order={$sort_order}" class="plain">Host name</a></th>	
		<th style="text-align:center;">Post Data</th>	
		<th style="text-align:center;">Get Data</th>	
		<th style="text-align:center;">Cookies</th>	
		<th>Actions</th>	
	</tr>

	{section name=data loop=$ip_address}
		
		<tr class="{cycle values='list-one,list-two'}">
		
		<td style="height:30px;">{$event_time[data]|date_format:"%B %e, %Y, %H:%M:%S"}</td>
		<td>{$ip_address[data]}</td>
		<td>{$host_name[data]}</td>
		<td style="text-align:center;">{$post_count[data]}</td>
		<td style="text-align:center;">{$get_count[data]}</td>
		<td style="text-align:center;">{$cookie_count[data]}</td>
		<td>
		
			<ul class="actionlist">
			<li><a href="{$base_url}{$fil_admin_log}?cmd=showPaymentScriptLogItem&amp;id={$id[data]}">View details</a></li>
			</ul>
		
		</td>
		
		</tr>
	
	{sectionelse}
	
		<tr class="{cycle values='list-one,list-two'}">
		<td colspan="7">There are no entries to show</td>
		</tr>
	
	{/section}
	
</table>

</div>

</div>

{include_template file="admin.snippets/admin.html.footer.tpl"}


