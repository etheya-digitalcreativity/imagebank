{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}




<div id="admin-outerdiv">

<div id="admin-innerdiv">



<table class="table-text">

	<tr>
	<th>This page lists the 50 most popular keywords that users have entered to search for images on your website.</th>
	<td>
	
	<p class="title">View the search keyword log</p>
	
	<p>This page allows you to view keywords included and excluded from searches on your website.  You can sort the list by frequency and keyword by clicking on the titles of the column headers.</p>
	
	</td>
	<td>&nbsp;</td>
	</tr>

</table>

<table class="list">
			
<tr>
	<th style="width:20%;"><a href="{$base_url}{$fil_admin_log_keywords}?sort=frequency&amp;method={$method}" class="plain">Frequency</a></td>
	<th style="width:40%;"><a href="{$base_url}{$fil_admin_log_keywords}?sort=keyword&amp;method={$method}" class="plain">Keywords since {$set_search_log_date|date_format:"%A %B %e, %Y"}</a></td>	
	<th style="width:40%;">Actions</td>	
</tr>

	{section name=keywords loop=$keyword}
		
		<tr class="{cycle values='row1,row2'}">
		<td style="height:30px;">{$count[keywords]}</td>
		<td>{$keyword[keywords]}</td>
		<td>
		<ul class="actionlist">
		<li><a href="{$base_url}{$fil_admin_library_search}?cmd=doSearch&amp;keywords_simple_boolean={if $escape[keywords]}'{$keyword[keywords]}'{else}{$keyword[keywords]}{/if}" title="Run this keyword through the administrator's search engine.">Admin search</a></li>
		<li><a href="{$base_url}{$fil_index_search}?cmd=doSearch&amp;keywords_simple_boolean={if $escape[keywords]}'{$keyword[keywords]}'{else}{$keyword[keywords]}{/if}" title="Run this search term through the front end search engine.">Front end search</a></li>
		</ul>
		</td>
		</tr>
	
	{sectionelse}
	
		<tr class="{cycle values='row1,row2'}">
		<td colspan="3" style="height:30px;">There are no search keywords in the log</td>
		</tr>
	
	{/section}

</table>
	
</div>

</div>

<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}


