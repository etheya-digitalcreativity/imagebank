{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">




<form action="{$base_url}{$fil_admin_user_settings}" method="post">

<input type="hidden" name="cmd" value="1" />


<div class="form-horizontal">

	<div class="form-header">Authentication Log</div>

	<div class="form-body">
	
		<div class="form-box" style="min-height:190px; width:350px;">

			<h1>Show user logins</h1>
			
			<p>This page allows you to view details of user logins on your website.  You can sort the list by column by clicking on the titles of the column headers.</p>
			
			<input type="checkbox" name="login_notify" class="forminput" {if $set_login_notify == "1"} checked="checked"{/if} /> <b>Notify me by e-mail when someone logs in</b><br /><br />
			
			<input type="submit" value="Save changes" style="width:200px; float:right;" class="formbutton" />
		
		</div>
		
		<div class="form-box" style="width:350px; min-height:190px;">
		
			<h1>Viewing anomalous logins</h1>
			
			<p>Viewing anomalous logins lists all those users who have logged in from more than one unique IP address or host name.</p>
			
			<p>In many cases this is not unusual but it may indicate a user is sharing their login information if they have logged in from a large number of different host names.</p>
			
			<p>Users who have logged in from more than 5 distinct, unique IP addresses are highlighted in red.</p>
		
		</div>
		
	</div>
	
	<div class="clear"></div>
	
</div>



</form>

<br />

<h1>50 most recent logins since: {$set_login_log_date|date_format:"%A, %B %e, %Y, %H:%M"}</h1>

{if $show == "anomalous"}

	<table class="list">
		
		<tr>
			<th><a href="{$base_url}{$fil_admin_log_login}?sort=username&amp;method={$method}&amp;show=ano" class="plain">User name</a></th>
			<th><a href="{$base_url}{$fil_admin_log_login}?sort=emailaddr&amp;method={$method}&amp;show=ano" class="plain">Email Address</a></th>	
			<th><a href="{$base_url}{$fil_admin_log_login}?sort=ipaddr&amp;method={$method}&amp;show=ano" class="plain">IP Address</a></th>	
			<th><a href="{$base_url}{$fil_admin_log_login}?sort=hostname&amp;method={$method}&amp;show=ano" class="plain">Host name</a></th>	
			<th><a href="{$base_url}{$fil_admin_log_login}?sort=date&amp;method={$method}&amp;show=ano" class="plain">Date</a></th>	
			<th>Actions</th>	
		</tr>
		
		{if $login_result == TRUE}
		
		{section name=logins loop=$log_userid}
						
			<tr class="{cycle values='row1,row2'}">
			<td style="height:32px;">{$log_username[logins]}</td>
			<td>{mailto address=$log_email[logins] encode="hex"}</td>
			<td>{$log_ip[logins]}</td>
			<td>{$log_host[logins]}</td>
			<td>{$log_date[logins]|date_format:"%A, %B %e, %Y, %H:%M:%S"}</td>
			<td><ul class="actionlist"><li><a href="{$base_url}{$fil_admin_user_edit}?userid={$log_userid[logins]}" title="Click here to edit this user's account">Edit User</a></li></ul></td>
			</tr>
		
		{/section}
		
		{else}
		
			<tr class="{cycle values='row1,row2'}">
			<td colspan="6" style="height:30px;">There are no logins to show</td>
			</tr>
		
		{/if}
		
	</table>

{elseif $show == "failed"}

	<table class="list">
	
		<tr>
			<th><a href="{$base_url}{$fil_admin_log_login}?sort=username&amp;method={$method}&amp;show=ano" class="plain">User name</a></th>
			<th><a href="{$base_url}{$fil_admin_log_login}?sort=emailaddr&amp;method={$method}&amp;show=ano" class="plain">Email Address</a></th>	
			<th><a href="{$base_url}{$fil_admin_log_login}?sort=ipaddr&amp;method={$method}&amp;show=ano" class="plain">IP Address</a></th>	
			<th><a href="{$base_url}{$fil_admin_log_login}?sort=hostname&amp;method={$method}&amp;show=ano" class="plain">Host name</a></th>	
			<th><a href="{$base_url}{$fil_admin_log_login}?sort=date&amp;method={$method}&amp;show=ano" class="plain">Date</a></th>	
			<th>Actions</th>	
		</tr>

		{if $login_result == TRUE}
		
		{section name=logins loop=$log_userid}
						
			<tr class="{cycle values='row1,row2'}">
			<td style="height:32px;">{$log_username[logins]}</td>
			<td>{mailto address=$log_email[logins] encode="hex"}</td>
			<td>{$log_ip[logins]}</td>
			<td>{$log_host[logins]}</td>
			<td>{$log_date[logins]|date_format:"%A, %B %e, %Y, %H:%M:%S"}</td>
			<td><ul class="actionlist"><li><a href="{$base_url}{$fil_admin_user_edit}?userid={$log_userid[logins]}" title="Click here to edit this user's account">Edit User</a></li></ul></td>
			</tr>
		
		{/section}
		
		{else}
		
			<tr class="{cycle values='row1,row2'}">
			<td colspan="6">There are no logins to show</td>
			</tr>
		
		{/if}
		
	</table>

{else}

	<table class="list">
	
		<tr>
			<th><a href="{$base_url}{$fil_admin_log_login}?sort=username&amp;method={$method}&amp;show=ano" class="plain">User name</a></th>
			<th><a href="{$base_url}{$fil_admin_log_login}?sort=emailaddr&amp;method={$method}&amp;show=ano" class="plain">Email Address</a></th>	
			<th><a href="{$base_url}{$fil_admin_log_login}?sort=ipaddr&amp;method={$method}&amp;show=ano" class="plain">IP Address</a></th>	
			<th><a href="{$base_url}{$fil_admin_log_login}?sort=hostname&amp;method={$method}&amp;show=ano" class="plain">Host name</a></th>	
			<th><a href="{$base_url}{$fil_admin_log_login}?sort=date&amp;method={$method}&amp;show=ano" class="plain">Date</a></th>	
			<th>Actions</th>	
		</tr>

		{if $login_result == TRUE}
		
		{section name=logins loop=$log_userid}
			
			<tr class="{cycle values='row1,row2'}">
			<td style="height:32px;">{$log_username[logins]}</td>
			<td>{mailto address=$log_email[logins] encode="hex"}</td>
			<td>{$log_ip[logins]}</td>
			<td>{$log_host[logins]}</td>
			<td>{$log_date[logins]|date_format:"%A, %B %e, %Y, %H:%M:%S"}</td>
			<td><ul class="actionlist"><li><a href="{$base_url}{$fil_admin_user_edit}?userid={$log_userid[logins]}" title="Click here to edit this user's account">Edit User</a></li></ul></td>
			</tr>
		
		{/section}
		
		{else}
		
			<tr class="{cycle values='row1,row2'}">
			<td colspan="6">There are no logins to show</td>
			</tr>
		
		{/if}
		
	</table>

{/if}


</div>

</div>

{include_template file="admin.snippets/admin.html.footer.tpl"}


