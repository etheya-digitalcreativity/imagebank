{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">








<table class="table-text">

	<tr>
	<th>This page shows you detailed information about a single page impression on the PayPal instant payment notifications cript on this website.</th>
	<td>
	
	<h1>PayPal script access</h1>
	
	<p>This page details the information collected when a user or other computer accessed the PayPal instant payment notification script on your Pixaria website.  Normally, this script will only be accessed when a user makes a payment using PayPal and you have instant payment notifications turned on.</p>
	
	<p>Data shown on this page includes 'get' data which is appended to the URL of the script when it is accessed, 'post' data which is sent form certain types of forms and web services and 'cookie' data which is used by web browsers to maintain session information.</p>
	
	</td>
	<td>&nbsp;</td>
	</tr>

</table>


<table class="table-form">
	
	<thead>
	<tr>
	<th colspan="3">General information</th>
	</tr>
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>IP Address:</th>
	<td>{$ip_address}</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Host name:</th>
	<td>{$host_name}</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>Event time:</th>
	<td>{$event_time|date_format:"%A, %B %e, %Y, %H:%M:%S"}</td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
</table>



{if $post_count > 0}

<table class="table-form">
	
	<thead>
	<tr>
	<th colspan="3">Post Data</th>
	</tr>
	</thead>
	
	<tbody>
	
	{section name="post" loop=$post_vars}
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>{$post_vars[post].0}</th>
	<td>{$post_vars[post].1}</td>
	<td>&nbsp;</td>
	</tr>
	
	{/section}
	
	</tbody>
	
</table>

{/if}



{if $get_count > 0}

<table class="table-form">
	
	<thead>
	<tr>
	<th colspan="3">Get Data</th>
	</tr>
	</thead>
	
	<tbody>
	
	{section name="get" loop=$get_vars}
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>{$get_vars[get].0}</th>
	<td>{$get_vars[get].1}</td>
	<td>&nbsp;</td>
	</tr>
	
	{/section}
	
	</tbody>
	
</table>

{/if}



{if $cookie_count > 0}

<table class="table-form">
	
	<thead>
	<tr>
	<th colspan="3">Cookie Data</th>
	</tr>
	</thead>
	
	<tbody>
	
	{section name="cookie" loop=$cookie_vars}
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>{$cookie_vars[cookie].0}</th>
	<td>{$cookie_vars[cookie].1}</td>
	<td>&nbsp;</td>
	</tr>
	
	{/section}
	
	</tbody>
	
</table>

{/if}

</div>

</div>

{include_template file="admin.snippets/admin.html.footer.tpl"}


