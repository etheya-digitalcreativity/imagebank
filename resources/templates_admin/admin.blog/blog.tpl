{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

{include_template file="admin.blog/blog.tinymce.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">




{include_template file="admin.snippets/admin.notification.complete.html" form_title_text="Your request has been completed!"}


<table class="table-text">

<tr>
<th>Use this control panel to manage the news blog on your website.</th>

<td>

<p class="title">Blog manager</p>

<p>The blog functions in Pixaria allow you to publish news items on your website to announce new images, galleries and events.</p>

<p>If you have a modern web browser with Javascript enabled, Pixaria provides rich text editing capabilities by means of the TinyMCE rich text editor.</p>

<p></p>

</td>
<td>&nbsp;</td>
</tr>

</table>


<table class="list">

<tr>
	<th>Article title</th>	
	<th>Author</th>	
	<th>Date added</th>	
	<th>Action</th>	
</tr>

{section name=blogs loop=$blog_item_id}
	
	<tr class="{cycle values='row1,row2'}">
	<td style="height:30px;"><a href="{$view_category_url[galleries]}">{$blog_title[blogs]}</a></td>
	<td>{$blog_author[blogs]}</td>
	<td>{$blog_date[blogs]|date_format:"%B %e, %Y %H:%M"}</td>
	<td>
	
	<ul class="actionlist">
	<li><a href="{$base_url}{$fil_admin_blog}?cmd=showFormEditEntry&amp;blog_item_id={$blog_item_id[blogs]}" title="Edit the details of this entry">Edit blog item</a></li>
	<li><a href="{$base_url}{$fil_admin_blog}?cmd=deleteEntry&amp;blog_item_id={$blog_item_id[blogs]}" title="Permanently delete this entry" onclick="return confirmSubmit('You are about to permanently delete this entry, are you sure you wish to continue!');">Delete blog item</a></li>
	</ul>
	
	</td>
	</tr>

{sectionelse}

	<tr class="{cycle values='row1,row2'}">
	<td colspan="4" style="height:30px;">There are no entries in your blog yet</td>
	</tr>

{/section}
	
</table>


</div>

</div>

<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}


