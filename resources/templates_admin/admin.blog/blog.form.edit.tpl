{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">




{include_template file="admin.blog/blog.tinymce.tpl"}



<form action="{$base_url}{$fil_admin_blog}" method="post">

<input type="hidden" name="cmd" value="saveEditedEntry" />

<input type="hidden" name="blog_item_id" value="{$blog_item_id}" />



<table class="table-search">

<tr>
<th>Editing this blog entry</th>
</tr>

<tr>
<td>

<div class="search-box" style="border-right:1px solid #BBB; width:400px; min-height:170px;">

<b>Title for this news item:</b><br />
<input type="text" name="blog_title" class="forminput" value="{$blog_title}" style="width:395px;" /><br /><br />


<b>Date for this news item (date posted):</b><br />
{html_select_date prefix="toda_" time=$blog_date start_year="-5" display_days=true all_extra='class="forminput"'}<br /><br />


<b>Time for this news item (time posted):</b><br />
{html_select_time prefix="toda_" time=$blog_date all_extra='class="forminput"'}<br /><br />

</div>

<div class="search-box" style="border-right:1px solid #BBB; width:400px; min-height:170px;">

<b>Link this entry to a gallery:</b><br /><br />
{html_fancy_select_single options=$menu_gallery_title values=$menu_gallery_id selected=$blog_gallery_id name='blog_gallery_id' width='400' rows='5' default_option='None' default_value='0'}


</div>


</div>
</td>
</tr>

</table>


<table class="table-search">

<tr>
<th>Blog item content</th>
</tr>

<tr>
<td>


<div class="search-box" style="border-right:1px solid #BBB; width:821px; min-height:420px;">

<p>{if $set_func_tinymce}<a href="http://tinymce.moxiecode.com?id=powered_by_tinymce_v2"><img src="{$base_url}resources/images/furniture/powered_by_tinymce_v2.png" border="0" width="88" height="32" alt="Powered by TinyMCE" style="float:right; margin:0 0 5px 5px;" /></a>{/if}

If you have a modern web browser with Javascript enabled, you can use the rich text editing capabilities of the TinyMCE editor included with Pixaria.  Otherwise, you can use selected HTML tags and <a href="http://daringfireball.net/projects/markdown/" style="text-decoration:underline;" target="_new">Markdown</a> syntax to add formatting to your entry.</p>

<textarea name="blog_content" class="forminput" style="width:100%; font-family:Monaco,Courier; font-size:7pt;" rows="20" cols="15">{$blog_content}</textarea>

<br /><br />

<input type="submit" value="Save changes to this post" style="width:400px; float:right;" class="formbutton" />

</div>


</td>
</tr>

</table>




</form>

</div>

</div>

{include_template file="admin.snippets/admin.html.footer.tpl"}


