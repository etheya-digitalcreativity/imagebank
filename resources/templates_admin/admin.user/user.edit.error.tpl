{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">
	
<form action="{$base_url}{$fil_admin_user_edit}" method="post">

<input type="hidden" name="userid" value="{$userid}" />

<input type="hidden" name="cmd" value="2" />

<table class="table-text">

<tr>
<th>There were problems with the information you provided on the previous page.</th>
<td>

{if $problem=="True"}

	<h1>Please correct errors</h1>
	
	<p>There was a problem with the information you submitted on the previous page, please take a moment to review these messages:</p>

	<ul>
	{section name="problems" loop=$problem_output}
		
		<li class="error">{$problem_output[problems]}</li>
	
	{/section}
	</ul>
	
{/if}


</td>
<td>&nbsp;</td>
</tr>
	
<tr>
<th>&nbsp;</th>
<td><input type="button" name="button" onclick="javascript:history.go(-1);" value="Go back and correct the errors" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</table>

</form>

</div>

</div>

{include_template file="admin.snippets/admin.html.footer.tpl"}


