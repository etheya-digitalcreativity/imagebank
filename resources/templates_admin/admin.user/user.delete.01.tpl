{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">




<form method="post" action="{$base_url}{$fil_admin_user_edit}" onsubmit="return confirmSubmit('You are about to delete a user, are you sure you wish to continue!');">

<input type="hidden" name="cmd" value="actionDeleteUser" />

<input type="hidden" name="userid" value="{$userid}" />

<table class="table-text">

<tr>
<th>Are you sure that you want to permanently delete the user '{$user_name}' from the database?</th>
<td>

<h1>Delete user '{$user_name}'</h1>

<p>You are about to permanently delete the user '{$user_name}'.  Are you sure you wish to continue with this action?</p>

</td>
<td>&nbsp;</td>
</tr>

<tr>
<th>&nbsp;</th>
<td><input type="submit" value="Delete this user" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</table>

</form>

</div>

</div>

<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.footer.tpl"}


