{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">




{include_template file="admin.snippets/admin.notification.complete.html" form_title_text="Privileges for the selected users have been updated!"}


<form name="userfilter" action="{$base_url}{$fil_admin_user}" method="get">

<input type="hidden" name="char" value="{$char}" />
<input type="hidden" name="method" value="{$method}" />
<input type="hidden" name="sort" value="{$sort}" />
<input type="hidden" name="administrator" value="{$administrator}" />
<input type="hidden" name="inactive" value="{$inactive}" />
<input type="hidden" name="photograher" value="{$photograher}" />
<input type="hidden" name="download" value="{$download}" />

<div class="form-horizontal">

	<div class="form-header">
	
	Quick filter
	
	{if $administrator}
	
		- Administrator users
	
	{elseif $download}
	
		- Image download users
	
	{elseif $inactive}
	
		- Inactive users
	
	{elseif $photographer}
	
		- Photographer users
	
	{else}
	
		- All users
	
	{/if}
	
	</div>

	<div class="form-body">
	
		<div class="form-box" style="min-height:180px; width:225px; border:none;">
		
			<h3>Name contains:</h3>
			
			<input type="text" name="filter_name" value="{$filter_name}" style="width:175px;" class="text" /><br />
			
			<h3>E-mail address contains:</h3>
			
			<input type="text" name="filter_email" value="{$filter_email}" style="width:175px;" class="text" /><br />
			
		</div>
		
		
		<div class="form-box" style="min-height:180px; width:235px;">
		
			<h3>Account properties:</h3>
			
			<input type="checkbox" name="is_administrator" id="is_administrator" class="forminput" {if $is_administrator}checked="checked"{/if} /> <label for="is_administrator">Administrator user</label><br />
	
			<input type="checkbox" name="is_photographer" id="is_photographer" {if $is_photographer}checked="checked"{/if} class="forminput" /> <label for="is_photographer">Photographer user</label><br />
		
			<input type="checkbox" name="is_download" id="is_download" {if $is_download}checked="checked"{/if} class="forminput" /> <label for="is_download">Free downloads enabled</label><br />
	
			<input type="checkbox" name="is_disabled" id="is_disabled" {if $is_disabled}checked="checked"{/if} class="forminput" /> <label for="is_disabled">Account is disabled</label><br />
			
			<br />
			<br />
			
			<input type="submit" value="Apply filter" name="submit" style="width:195px;" class="formbutton" />
			
		</div>
		
		
		<div class="form-box" style="min-height:180px; width:240px;">
		
		<h3>Export user data</h3>
		
		<dl class="download-list">
			
			<dt><a href="{$base_url}{$fil_admin_user}?filter_email={$filter_email}&amp;filter_name={$filter_name}&amp;sort={$sort}&amp;method={$method}{if $char!=''}{/if}&amp;action=exportUsersAsCSV" rel="nofollow" title="Download an Excel compatible file containing detailed information for all the users currenty being displayed."><img src="{$base_url}resources/images/icons/32x32/file_csv.png" alt="" width="32" height="32" border="0" /></a></dt>
			<dd style="font-weight:bold; padding-top:3pt;"><a href="{$base_url}{$fil_admin_user}?filter_email={$filter_email}&amp;filter_name={$filter_name}&amp;sort={$sort}&amp;method={$method}{if $char!=''}{/if}&amp;action=exportUsersAsCSV" rel="nofollow" title="Download an Excel compatible file containing detailed information for all the users currenty being displayed.">Export current view</a></dd>
			<dd>CSV export of current list</dd>
			
			<dt><a href="{$base_url}{$fil_admin_user}?action=exportUsersAsCSV" rel="nofollow" title="Download an Excel compatible file containing detailed information for all the users registered on your website."><img src="{$base_url}resources/images/icons/32x32/file_csv.png" alt="" width="32" height="32" border="0" /></a></dt>
			<dd style="font-weight:bold; padding-top:3pt;"><a href="{$base_url}{$fil_admin_user}?action=exportUsersAsCSV" rel="nofollow" title="Download an Excel compatible file containing detailed information for all the users registered on your website.">Export of entire user list</a></dd>
			<dd>CSV export of all users</dd>
			
		</dl>
		
		</div>
	
	</div>
	
    <div class="clear"></div>
       	
</div>

</form>

<br />


<form action="{$base_url}{$fil_admin_user}" id="user_list" method="post">

<table class="list">

<tr>
<th colspan="7">

Now viewing - 

{if $administrator}

	Administrator users

{elseif $download}

	Image download users

{elseif $inactive}

	Inactive users

{elseif $photographer}

	Photographer users

{else}

	All users

{/if}

</th>
</tr>

<tr>
<td colspan="7" class="options">

		<div class="option-title">Page number: </div>
			
		<div class="pagination">
		
			{section name="ipages" loop=$ipage_numbers}
			
				{if $ipage_numbers[ipages] == $ipage_current}
				
					<a href="{$ipage_links[ipages]}" class="current">{$ipage_numbers[ipages]}</a>
				
				{else}
				
					<a href="{$ipage_links[ipages]}">{$ipage_numbers[ipages]}</a>
				
				{/if}
			
			{/section}
			
		</div>


</td>
</tr>

<tr>
<td colspan="7" class="options">

	<div class="option-title">Items per page: </div>
		
	<div class="pagination">
		
		<input type="text" size="3" maxlength="3" id="thumb_per_page" name="thumb_per_page" value="{$set_thumb_per_page}" class="page-button" />
		<input type="button" value="Users per page" onclick="javascript:updateCookie(1,document.getElementById('thumb_per_page').value,'1|{$set_thumb_per_page}|{$set_gallery_per_page}|{$set_view_thumb_view}');location.href='{$base_url}{$fil_admin_user}?{$query_string}';" class="formbutton" />
		
	</div>
	
</td>
</tr>
	
<tr>
	
	<th style="width:20px;"><input title="Toggle all checkboxes" type="checkbox" name="toggle" onclick="javascript:this.checked ? selectAll('user_list','users[]') : deselectAll('user_list','users[]');" /></th>
	
	<th class="{if $sort == 'name1' && $method == 'd'}desc{elseif $sort == 'name1' && $method == 'a'}asc{/if}"><a href="{$base_url}{$fil_admin_user}?sort=name1&amp;method={$new_method}&amp;{$type}" class="plain">Family Name</a></th>	
	
	<th class="{if $sort == 'name2' && $method == 'd'}desc{elseif $sort == 'name2' && $method == 'a'}asc{/if}"><a href="{$base_url}{$fil_admin_user}?sort=name2&amp;method={$new_method}&amp;{$type}" class="plain">First Name</a></th>	
	
	<th class="{if $sort == 'enabled' && $method == 'd'}desc{elseif $sort == 'enabled' && $method == 'a'}asc{/if}" style="width:80px;"><a href="{$base_url}{$fil_admin_user}?sort=enabled&amp;method={$new_method}&amp;{$type}" class="plain">Enabled</a></th>	
	
	<th class="{if $sort == 'email' && $method == 'd'}desc{elseif $sort == 'email' && $method == 'a'}asc{/if}"><a href="{$base_url}{$fil_admin_user}?sort=email&amp;method={$new_method}&amp;{$type}" class="plain">E-Mail</a></th>	
	
	<th class="{if $sort == 'date' && $method == 'd'}desc{elseif $sort == 'date' && $method == 'a'}asc{/if}"><a href="{$base_url}{$fil_admin_user}?sort=date&amp;method={$new_method}&amp;{$type}" class="plain">Date added</a></th>	
	
	<th>Action</th>	

</tr>

{section name=users loop=$userid}
	
	<tr class="{cycle values='row1,row2'}">
	<td style="width:20px; height:30px;">{if $userid[users] != $ses_psg_userid}<input type="checkbox" name="users[]" value="{$userid[users]}" />{/if}</td>
	<td class="{if $sort == 'name1'}active{/if}">{$family_name[users]}</td>
	<td class="{if $sort == 'name2'}active{/if}">{$first_name[users]}</td>
	<td class="{if $sort == 'enabled'}active{/if}" style="text-align:center;">
	{if $account_status[users] == "1"}
	<img src="{$base_url}resources/images/icons/27x27/tick.png" alt="Inactive" width="27" height="27" border="0" title="User is active" />
	{else}
	<img src="{$base_url}resources/images/icons/27x27/cross.png" alt="Inactive" width="27" height="27" border="0" title="User is inactive" /></span>
	{/if}
	</td>
	<td class="{if $sort == 'email'}active{/if}">{mailto address=$email_address[users] encode="javascript"}</td>
	<td class="{if $sort == 'date'}active{/if}">{$date_registered[users]|date_format:"%A, %B %e, %Y"}</td>
	<td>
	
	<ul class="actionlist">
	{if $photographer}
	<li><a href="{$base_url}{$fil_admin_user}?cmd=viewUserImages&amp;userid={$userid[users]}">Show Images</a></li>
	<li><a href="{$base_url}{$fil_admin_store}?cmd=listPhotographerSales&amp;userid={$userid[users]}">Sales this month</a></li>
	{/if}
	
	<li><a href="{$base_url}{$fil_admin_user_edit}?userid={$userid[users]}">Edit</a></li>
	{if $userid[users] != $ses_psg_userid}<li><a href="{$base_url}{$fil_admin_user}?cmd=formConfirmDeleteUsers&amp;users[]={$userid[users]}">Delete</a></li>{/if}
	</ul>
	
	</td>
	</tr>

{sectionelse}

	<tr class="{cycle values='row1,row2'}">
	<td colspan="7" style="text-align:left; height:30px;">There are no users to show in this view</td>
	</tr>

{/section}
	
	<tr>
		<th colspan="7">
		
		<select name="cmd" class="formselect">
			
			<option value="formConfirmDeleteUsers">Permanently delete the selected users</option>
			
			{if $is_inactive}
				<option value="promoteActive" selected="selected">Make the selected user accounts active</option>
			{else}
				<option value="demoteActive">Make the selected user accounts inactive</option>
			{/if}
			
			{if $is_administrator}
				<option value="demoteAdmin" selected="selected">Remove the selected users administrator privileges</option>
			{else}
				<option value="promoteAdmin">Grant the selected users administrator privileges</option>
			{/if}
					
			{if $is_photographer}
				<option value="demotePhotographer" selected="selected">Remove the selected users photographer privileges</option>
			{else}
				<option value="promotePhotographer">Grant the selected users photographer privileges</option>
			{/if}
			
			{if $is_download}
				<option value="demoteDownload" selected="selected">Remove the selected users free download privileges</option>
			{else}
				<option value="promoteDownload">Grant the selected users free download privileges</option>
			{/if}
			
		</select>
		
		<input type="submit" name="submit" value="Apply action to selected users" class="formbutton" />
		
		</th>
	</tr>
	
</table>

</form>

</div>

</div>

{include_template file="admin.snippets/admin.html.footer.tpl"}