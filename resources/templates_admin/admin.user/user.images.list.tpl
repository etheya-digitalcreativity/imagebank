{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}

<div id="admin-outerdiv">

<div id="admin-innerdiv">




<table class="table-text">

<tr>
<th>The following list shows all the images owned by the user {$user_name}.</th>

<td>

<h1>Images owned by {$user_name}</h1>

<p>To work with these images, select the items you want to manage and choose an action from the menu at the bottom of the page.</p>

</td>
<td>&nbsp;</td>
</tr>

</table>




<form method="post" id="image_grid" action="{$base_url}{$fil_admin_library}">

<table class="table-listbox">

<tr>
<th colspan="7">Images owned by {$user_name}</th>
</tr>

<tr>
<td colspan="7" class="options">

		<div class="option-title">Page number: </div>
			
		<div class="pagination">
		
			{section name="ipages" loop=$ipage_numbers}
			
				{if $ipage_numbers[ipages] == $ipage_current}
				
					<a href="{$ipage_links[ipages]}" class="current">{$ipage_numbers[ipages]}</a>
				
				{else}
				
					<a href="{$ipage_links[ipages]}">{$ipage_numbers[ipages]}</a>
				
				{/if}
			
			{/section}
			
		</div>


</td>
</tr>

<tr>
<td colspan="7" class="options">

	<div class="option-title">Items per page: </div>
		
	<div class="pagination">
		
		<input type="text" size="3" maxlength="3" id="thumb_per_page" name="thumb_per_page" value="{$set_thumb_per_page}" class="page_button" />
		<input type="button" value="Users per page" onclick="javascript:updateCookie(1,document.getElementById('thumb_per_page').value,'1|{$set_thumb_per_page}|{$set_gallery_per_page}|{$set_view_thumb_view}');location.href='{$base_url}{$fil_admin_user}?{$query_string}';" class="formbutton" />
		
	</div>
	
</td>
</tr>
	

<tr>
	<th colspan="2"><input title="Toggle all checkboxes" type="checkbox" name="toggle" onclick="javascript:this.checked ? selectAll('image_grid','images[]') : deselectAll('image_grid','images[]');" checked="checked" /></th>
	<th>File name</th>
	<th style="text-align:center;">Active</th>
	<th>Image title</th>	
	<th>Image dimensions</th>
	<th>Action</th>
</tr>

{if $images_present}

	{section name="assets" loop=$icon_path}
	
		<tr class="{cycle values='list-one,list-two'}">
		
		<td style="width:18px;"><input id="radio_row_{$smarty.section.assets.index}" type="checkbox" name="images[]" value="{$image_id[assets]}" checked="checked" /></td>
		
		<td style="width:42px; height:42px; text-align:center;"><a href="javascript:imagePreviewWindow('{$base_url}{$fil_admin_image_output}?html={$comp_path[assets]}&amp;','{$comp_width[assets]}','{$comp_height[assets]}');"><img src="{$base_url}{$fil_admin_image_output}?file={$icon_path[assets]}&amp;" border="0" width="{$icon_width[assets]}" height="{$icon_height[assets]}" title="{$image_filename[assets]}" alt="{$image_filename[assets]}" class="icon" /></a>
		</td>
		
		<td><a class="cl" href="javascript:openPop('{$base_url}{$fil_admin_image_output}?html={$comp_path[assets]}&amp;','{$comp_width[assets]-2}','{$comp_height[assets]-2}','no','no','no','no','no','no','no');">{$image_filename[assets]}</a></td>
		
		<td style="text-align:center;">
		{if $image_active[assets]}
		<img src="{$base_url}resources/images/icons/16x16/tick.png" alt="Inactive" width="16" height="16" border="0" title="This image is live for viewing" />
		{else}
		<img src="{$base_url}resources/images/icons/16x16/cross.png" alt="Inactive" width="16" height="16" border="0" title="This image is disabled for viewing" />
		{/if}
		</td>
								
		<td>{$image_title[assets]}</td>
								
		<td>{$image_width[assets]} x {$image_height[assets]} pixels</td>
		
		<td>
		
		<ul class="actionlist">
		<li><a href="{$base_url}{$fil_admin_image}?cmd=edit&amp;image_id={$image_id[assets]}">Edit image</a></li>
		<li><a href="{$base_url}{$fil_admin_library}?cmd=formAddImagesToGallery&amp;images[]={$image_id[assets]}">Add to gallery</a></li>
		<li><a href="{$base_url}{$fil_admin_library}?cmd=formDeleteFromLibrary&amp;images[]={$image_id[assets]}">Delete image</a></li>
		</ul>
		
		</td>
		
		</tr>
	
	{/section}	

{else}

	<tr class="{cycle values='list-one,list-two'}">
	
	<td colspan="7" style="height:30px;">This user does not own any images in the library.</td>
	
	</tr>

{/if}
	
</table>

{if $images_present}
	
	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">Options:</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="list-one">
		<th>Choose what you want to do with the selected images:</th>
		<td>
		
		<input type="radio" name="cmd" value="formAddImagesToGallery" checked="checked" class="forminput" /> Add selected images to a gallery<br />
		<input type="radio" name="cmd" value="formCreateGalleryWithImages" class="forminput" /> Create a gallery with the selected images<br />
		<input type="radio" name="cmd" value="formDeleteFromLibrary" class="forminput" /> Delete selected images from the library<br />
		<input type="radio" name="cmd" value="showFormBatchEdit" class="forminput" /> Batch edit selected images<br />
		
		</td>
		<td>&nbsp;</td>
	</tr>

	<tr class="list-one">
		<th>&nbsp;</th>
		<td><input type="submit" name="submit" value="Proceed to next step" style="width:100%;" class="formbutton" /></td>
		<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
	</table>

{/if}

</form>
	
</div>

</div>



<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}


