{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">



<table class="table-text">

<tr>
<th>The changes you made to this profile have been saved.</th>
<td>

<h1>The changes have been saved</h1>

<p>The changes you made to this user's profile have been saved to the database.</p>

<form method="post">
	
<input type="button" onclick="javascript:location.href='{$base_url}{$fil_admin_user}';" value="Go back to the user manager" style="width:100%;" class="formbutton" />

</form>

</td>
<td></td>
</tr>

</table>


</div>

</div>



<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.footer.tpl"}


