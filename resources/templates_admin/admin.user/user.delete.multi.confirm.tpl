{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">




<table class="table-text">

<tr>
<th>Are you sure that you want to permanently delete the previously selected group of users, this action cannot be undone?</th>

<td>

<h1>Delete multiple users</h1>

<p>You are about to permanently delete the group of users selected on the previous page.  Are you sure you wish to continue with this action?</p>

<p>Deleting a user also deletes any records of previous transactions, lightboxes and shopping cart entries for that user.</p>

</td>
<td>&nbsp;</td>
</tr>

</table>

<form method="post" action="{$base_url}{$fil_admin_user}" onsubmit="return confirmSubmit('You are about to delete multiple users, are you sure you want to continue!');">

<input type="hidden" name="cmd" value="actionDeleteUsers" />

<table class="list">

<tr>
	<th>First Name</th>	
	<th>E-Mail</th>	
</tr>

{if $user_count > 0 }
	
	{section name=users loop=$users}
		
		<tr class="{cycle values='row1,row2'}">
		<td style="height:30px;">{$user_name[users]}</td>
		<td>{mailto address=$email_address[users] encode="javascript"}<input type="hidden" name="users[]" value="{$users[users]}" /></td>
		</tr>
	
	{/section}
		
{else}

	<tr class="{cycle values='row1,row2'}">
	<td align="left" colspan="5">There are no users to show</td>
	</tr>

{/if}

</table>

<table class="table-text">

<tr>
<th>&nbsp;</th>
<td><input type="submit" name="submit" value="Delete selected users" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</table>

</form>

</div>

</div>

{include_template file="admin.snippets/admin.html.footer.tpl"}


