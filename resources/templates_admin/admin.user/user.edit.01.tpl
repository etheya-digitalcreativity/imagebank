{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">




<table class="table-text">

<tr>
<th>Use this form to edit the profile of the selected user.</th>
<td>

{if $problem}

	<h1>Please correct errors</h1>
	
	<p>There was a problem with the information you submitted on the previous page, please take a moment to review these messages:</p>

	<ol>
	{section name="problems" loop=$profile_errors}
		
		<li class="html-error-warning">{$profile_errors[problems]}</li>
	
	{/section}
	</ol>
	
{/if}

</td>
<td>&nbsp;</td>
</tr>

</table>



<form action="{$base_url}{$fil_admin_user_edit}" method="post" autocomplete="off">

<input type="hidden" name="userid" value="{$userid}" />

<input type="hidden" name="cmd" value="actionEditUser" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">Permissions:</th>
</tr>

</thead>

<tbody>

{if $ses_psg_userid==$userid}

<tr class="{cycle values='list-one,list-two'}">
<th>Account active:</th>
<td><input type="hidden" name="account_status" value="on" />{if $account_status==1}Yes{/if}</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>User is an administrator:</th>
<td><input type="hidden" name="is_admin" value="on" />{if $is_admin}Yes{/if}</td>
<td>&nbsp;</td>
</tr>

{else}

<tr class="{cycle values='list-one,list-two'}">
<th>Account active:</th>
<td><input type="checkbox" name="account_status" class="forminput"{if $account_status==1} checked="checked"{/if} /> Check to enable</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>User is an administrator:</th>
<td><input type="checkbox" name="is_admin" class="forminput"{if $is_admin} checked="checked"{/if} /> Check to enable</td>
<td>&nbsp;</td>
</tr>

{/if}

<tr class="{cycle values='list-one,list-two'}">
<th>User is a photographer (can upload images):</th>
<td><input type="checkbox" name="is_photo" class="forminput"{if $is_photo} checked="checked"{/if} /> Check to enable</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>User can edit their own uploaded images:</th>
<td><input type="checkbox" name="is_editor" class="forminput"{if $is_editor} checked="checked"{/if} /> Check to enable</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>User can delete their own uploaded images:</th>
<td><input type="checkbox" name="is_delete" class="forminput"{if $is_delete} checked="checked"{/if} /> Check to enable</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>User can download high-resolution images:</th>
<td><input type="checkbox" name="is_download" class="forminput"{if $is_download} checked="checked"{/if} /> Check to enable</td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>



<table class="table-form">

<thead>

<tr>
<th colspan="3">User groups:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>Groups this user belongs to:</th>
<td>

{section name="groups" loop=$group_id}
	<input type="checkbox" name="groups[]" class="forminput" value="{$group_id[groups]}"{if $group_member_id[groups]==$group_id[groups]} checked="checked"{/if}> {$group_name[groups]}<br />
{/section}

</td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>



<table class="table-form">

<thead>

<tr>
<th colspan="3">User information:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>Date registered:</th>
<td>{$date_registered|date_format:"%A, %B %e, %Y, %H:%M:%S"}</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Profile last edited:</th>
<td>{$date_edited|date_format:"%A, %B %e, %Y, %H:%M:%S"}</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Message to the site owner:</th>
<td>{$other_message|nl2br}</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Reason for requesting access:</th>
<td>{$other_image_interest|nl2br}</td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>



<table class="table-form">

<thead>

<tr>
<th colspan="3">Contact information:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>Title:</th>
<td><input type="text" name="formal_title" style="width:100%;" class="forminput" value="{$formal_title}" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>First name / Family name:</th>
<td>

<input type="text" maxlength="50" size="30" name="first_name" value="{$first_name}" style="width:48%;" class="forminput" />
<input type="text" maxlength="50" size="30" name="family_name" value="{$family_name}" style="width:48%;" class="forminput" />

</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>E-Mail address:</th>
<td>
<input type="text" maxlength="50" size="30" name="email_address" value="{$email_address}" style="width:100%;" class="forminput" />
<input type="hidden" name="email_address_old" value="{$email_address}" />
</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Telephone number:</th>
<td><input type="text" maxlength="50" size="30" name="telephone" value="{$telephone}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>
	
<tr class="{cycle values='list-one,list-two'}">
<th>Mobile telephone:</th>
<td><input type="text" maxlength="50" size="30" name="mobile" value="{$mobile}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>
	
<tr class="{cycle values='list-one,list-two'}">
<th>Fax number:</th>
<td><input type="text" maxlength="50" size="30" name="fax" value="{$fax}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>



<table class="table-form">

<thead>

<tr>
<th colspan="3">Address information:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>Address:</th>
<td><input type="text" maxlength="50" size="30" name="addr1" value="{$addr1}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>
	
<tr class="{cycle values='list-one,list-two'}">
<th></th>
<td><input type="text" maxlength="50" size="30" name="addr2" value="{$addr2}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>
	
<tr class="{cycle values='list-one,list-two'}">
<th></th>
<td><input type="text" maxlength="50" size="30" name="addr3" value="{$addr3}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>
	
<tr class="{cycle values='list-one,list-two'}">
<th>City / Town:</th>
<td><input type="text" maxlength="50" size="30" name="city" value="{$city}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>
	
<tr class="{cycle values='list-one,list-two'}">
<th>Region:</th>
<td><input type="text" maxlength="50" size="30" name="region" value="{$region}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>
	
<tr class="{cycle values='list-one,list-two'}">
<th>Country:</th>
<td>

	<select name="country" style="width:100%;" class="forminput">
	
	<option value="">Please choose</option>
	
	<option value=""></option>

	<option value="{$iso_codes.12}">{$printable_names.12}</option>
	<option value="{$iso_codes.20}">{$printable_names.20}</option>
	<option value="{$iso_codes.57}">{$printable_names.57}</option>
	<option value="{$iso_codes.72}">{$printable_names.72}</option>
	<option value="{$iso_codes.79}">{$printable_names.79}</option>
	<option value="{$iso_codes.102}">{$printable_names.102}</option>
	<option value="{$iso_codes.104}">{$printable_names.104}</option>
	<option value="{$iso_codes.149}">{$printable_names.149}</option>
	<option value="{$iso_codes.152}">{$printable_names.152}</option>
	<option value="{$iso_codes.159}">{$printable_names.159}</option>
	<option value="{$iso_codes.198}">{$printable_names.198}</option>
	<option value="{$iso_codes.204}">{$printable_names.204}</option>
	<option value="{$iso_codes.224}">{$printable_names.224}</option>
	<option value="{$iso_codes.225}">{$printable_names.225}</option>
	
	<option value=""></option>

	{section name="country" loop=$iso_codes}
	
	{if $country == $iso_codes[country]}
	
		<option value="{$iso_codes[country]}" selected="selected">{$printable_names[country]}</option>
	
	{elseif $country == $printable_names[country]}
	
		<option value="{$iso_codes[country]}" selected="selected">{$printable_names[country]}</option>
	
	{else}
	
		<option value="{$iso_codes[country]}">{$printable_names[country]}</option>
	
	{/if}
	
	{/section}
	
	</select>

</td>
<td>&nbsp;</td>
</tr>
	
<tr class="{cycle values='list-one,list-two'}">
<th>Postal code:</th>
<td><input type="text" maxlength="50" size="30" name="postal_code" value="{$postal_code}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>



<table class="table-form">

<thead>

<tr>
<th colspan="3">Other information:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>User's business or organisation name:</th>
<td><input type="text" maxlength="50" size="30" name="other_company_name" value="{$other_company_name}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>User's business area or interest:</th>
<td><input type="text" maxlength="50" size="30" name="other_business_type" value="{$other_business_type}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>User's position within their company:</th>
<td><input type="text" maxlength="50" size="30" name="other_business_position" value="{$other_business_position}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>How often does the user publish:</th>
<td><input type="text" maxlength="50" size="30" name="other_frequency" value="{$other_frequency}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>The user's publication circulation:</th>
<td><input type="text" maxlength="50" size="30" name="other_circulation" value="{$other_circulation}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Territories this user publishes in:</th>
<td><input type="text" maxlength="50" size="30" name="other_territories" value="{$other_territories}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>User's website address:</th>
<td><input type="text" maxlength="50" size="30" name="other_website" value="{$other_website}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

<table class="table-form">

<thead>

<tr>
<th colspan="3">Security information:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>Password:</th>
<td><input type="password" maxlength="50" size="30" name="password_1" value="" style="width:100%;" class="forminput" /><br />Minimum of 8 characters and composed only of letters and numbers</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Re-type password:</th>
<td><input type="password" maxlength="50" size="30" name="password_2" value="" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Password reminder question:</th>
<td><input type="text" maxlength="500" size="60" name="password_rem_que" value="{$password_rem_que}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>Answer to password reminder question:</th>
<td><input type="text" maxlength="500" size="60" name="password_rem_ans" value="{$password_rem_ans}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>
	
<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="submit" name="submit" value="Proceed to next step" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>

</div>

</div>

<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="admin.snippets/admin.html.footer.tpl"}


