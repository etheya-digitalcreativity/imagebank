{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">





<table class="table-text">

<tr>
<th>Use this form to invite a new user to register on your website.</th>
<td>

<h1>Create a new user</h1>

<p>You can use this form to invite a user to register on your website.  All you need to do is complete the basic information below and the user will be invited by e-mail to complete their registration information at a time convenient to them.</p>

</td>
<td>&nbsp;</td>
</tr>

</table>



<form action="{$base_url}{$fil_admin_users}" method="post" autocomplete="off">

<input type="hidden" name="cmd" value="createNewPartialUser" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">Permissions:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>User is an administrator:</th>
<td><input type="checkbox" name="is_admin" class="forminput"{if $is_admin} checked="checked"{/if}{if $ses_psg_userid==$userid} disabled="disabled"{/if} /> Check to enable</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>User can edit image information:</th>
<td><input type="checkbox" name="is_editor" class="forminput"{if $is_editor} checked="checked"{/if} /> Check to enable</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>User is a photographer:</th>
<td><input type="checkbox" name="is_photo" class="forminput"{if $is_photo} checked="checked"{/if} /> Check to enable</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Groups this user belongs to:</th>
<td>
<select name="groups[]" class="forminput" style="width:100%;" size="6" multiple="multiple">
{section name="groups" loop=$group_id}
	<option value="{$group_id[groups]}"{if $group_member_id[groups]==$group_id[groups]} selected="selected"{/if}>{$group_name[groups]}</option>
{/section}
</select>
</td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>



<table class="table-form">

<thead>

<tr>
<th colspan="3">Contact information:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>First name:</th>
<td><input type="text" name="first_name" style="width:100%;" class="forminput" value="{$first_name}" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Family name:</th>
<td><input type="text" maxlength="50" size="30" name="family_name" value="{$family_name}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>E-Mail address:</th>
<td>
<input type="text" maxlength="50" size="30" name="email_address" value="{$email_address}" style="width:100%;" class="forminput" />
</td>
<td>&nbsp;</td>
</tr>
	
<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="submit" name="submit" value="Create user and send invitation" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>







</div>

</div>

<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}
