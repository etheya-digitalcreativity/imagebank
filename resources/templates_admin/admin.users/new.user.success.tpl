{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">




<table class="table-text">

<tr>
<th>You have successfully completed a new user invitation.</th>
<td>

<h1>Invitation complete</h1>

<p>You have successfully invited a new user to register on your website.  The user will shortly receive an e-mail at the address you entered with instructions on how to complete their profile information and activate their account.</p>

<h1>What would you like to do next?</h1>

<p><a href="{$base_url}{$fil_admin_users}?cmd=createNewUserForm">Add another user</a></p>

<p><a href="{$base_url}{$fil_admin_user}">Go back to the list of users</a></p>

</td>
<td>&nbsp;</td>
</tr>

</table>







</div>

</div>

<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}
