{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">



<table class="table-text">

<tr>
<th>An error occurred while trying to create the new user invitation.</th>
<td>

<h1>Please correct errors</h1>

<p>There was a problem with the information you submitted on the previous page, please take a moment to review these messages:</p>

<ul>
{section name="problems" loop=$profile_errors}
	
	<li class="error">{$profile_errors[problems]}</li>

{/section}
</ul>

</td>
<td>&nbsp;</td>
</tr>

</table>







</div>

</div>

<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}
