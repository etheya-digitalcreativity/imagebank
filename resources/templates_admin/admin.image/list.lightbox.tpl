{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">



<table class="table-text">

<tr>

<th>You are now viewing a list of images in a lightbox created by {$first_name} {$family_name}.</th>

<td>

<h1>List of lightbox contents</h1>

<p>This page lists the contents of a lightbox create by the user {$first_name} {$family_name}.  From this page you are able to perform actions on the images in the lightbox including adding them to a gallery, creating a new gallery with them or removing them from the library completely.</p>

<div class="notification-note">

<h1>Contact information</h1>

<p><b>Name:</b> {$first_name} {$family_name}</p>

<p><b>E-mail:</b> <a href="mailto:{$email_address}">{$email_address}</a></p>

<p>
<b>Address:</b><br /><br />
{if $address1 != ""}{$address1}<br />{/if}
{if $address2 != ""}{$address2}<br />{/if}
{if $address3 != ""}{$address3}<br />{/if}
{if $city != ""}{$city}<br />{/if}
{if $region != ""}{$region}<br />{/if}
{if $country != ""}{$country}<br />{/if}
{if $postal_code != ""}{$postal_code}<br />{/if}

</p>

</div>

</td>
<td>&nbsp;</td>

</tr>

</table>




<form method="post" id="image_list" action="{$base_url}{$fil_admin_image_list}">

<input type="hidden" name="gallery_id" value="{$gallery_id}" />

<table class="table-listbox">

<tr>
	<th style="padding:0px;"><input title="Toggle all checkboxes" type="checkbox" name="toggle" onclick="javascript:this.checked ? selectAll('image_list','images[]') : deselectAll('image_list','images[]')" /></th>
	<th colspan="2">File name</th>	
	<th style="text-align:center;">Active</th>
	<th>Image title</th>	
	<th>Image dimensions</th>
	<th colspan="2">Actions</th>
</tr>

{section name="assets" loop=$icon_path}

	<tr class="{cycle values='list-one,list-two'}">
	
	<td style="width:18px;"><input type="checkbox" name="images[]" value="{$image_id[assets]}" /></td>
	
	<td style="width:42px; height:42px; text-align:center;"><a href="javascript:imagePreviewWindow('{$base_url}{$fil_admin_image_output}?html={$comp_path[assets]}&amp;','{$comp_width[assets]}','{$comp_height[assets]}');"><img src="{$base_url}{$fil_admin_image_output}?file={$icon_path[assets]}&amp;" border="0" width="{$icon_width[assets]}" height="{$icon_height[assets]}" title="{$image_filename[assets]}" alt="{$image_filename[assets]}" class="icon" /></a>
	</td>
	
	<td><a class="cl" href="javascript:imagePreviewWindow('{$base_url}{$fil_admin_image_output}?html={$comp_path[assets]}&amp;','{$comp_width[assets]}','{$comp_height[assets]}');">{$image_filename[assets]}</a></td>
	
	<td style="text-align:center;">
	{if $image_active[assets]}
	<img src="{$base_url}resources/images/icons/16x16/tick.png" alt="Inactive" width="16" height="16" border="0" title="This image is live for viewing" />
	{else}
	<img src="{$base_url}resources/images/icons/16x16/cross.png" alt="Inactive" width="16" height="16" border="0" title="This image is disabled for viewing" />
	{/if}
	</td>
							
	<td>{$image_title[assets]}</td>
							
	<td>{$image_width[assets]} x {$image_height[assets]} pixels</td>
	
	<td>
	
	<ul class="actionlist">
	<li><a href="{$base_url}{$fil_admin_image}?cmd=edit&amp;image_id={$image_id[assets]}">Edit image</a></li>
	</ul>
	
	</td>
	
	<td><img src="resources/themes/{$set_theme}/images/spacer.gif" alt="" width="1" height="32" border="0" /></td>
	
							
	</tr>

{sectionelse}

	<tr class="{cycle values='list-one,list-two'}">
		
	<td colspan="6">There are no images in this lightbox.</td>
								
	</tr>

{/section}	

</table>

<table class="table-form">

<thead>

<tr>
<th colspan="3">Image batch processing options:</th>
</tr>

</thead>

<tbody>

{if $image_id_count > 0 }
	
	<tr class="list-one">
		<th>Choose what you want to do with the selected images:</th>
		<td>
		
		<input type="radio" name="cmd" value="formBatchEdit" class="forminput" checked="checked" /> Batch edit the selected images<br />
		<input type="radio" name="cmd" value="formCreateGalleryWithImages" class="forminput" /> Create a gallery with the selected images<br />
		<input type="radio" name="cmd" value="formAddImagesToGallery" class="forminput" /> Copy these images into a gallery<br />
		<input type="radio" name="cmd" value="formDeleteFromLibrary" class="forminput" /> Permanently remove selection from your library<br />
		
		</td>
		<td>&nbsp;</td>
	</tr>

	<tr class="list-one">
		<th>&nbsp;</th>
		<td><input type="submit" name="submit" value="Proceed to next step" style="width:100%;" class="formbutton" /></td>
		<td>&nbsp;</td>
	</tr>

{else}
	
	<tr class="list-one">
	<th>&nbsp;</th>
	<td style="text-align:right;"><input type="button" onclick="javascript:history.go(-1);" value="Go back to the previous page" style="width:100%;" class="forminput" /></td>
		<td>&nbsp;</td>
	</tr>
	
{/if}

</tbody>

</table>

</form>

</div>

</div>


{include_template file="admin.snippets/admin.html.footer.tpl"}