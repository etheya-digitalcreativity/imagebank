{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Image Metadata</title>
	<meta name="generator" content="BBEdit 8.0" />

	<link rel="SHORTCUT ICON" href="{$base_url}favicon.ico" />
	
	<link rel="stylesheet" href="{$base_url}resources/css/pixaria.admin.css" />
		 
	<script src="{$base_url}resources/javascript/pixaria.main.js" type="text/javascript" language="javascript"></script>

</head>

<body>

<div id="popup-content">

	<div style="width:100%;">
	
	{if $image_metadata}		
		
		{if $iptc_raw}
		
			<table class="table-popup-form">
			
			<thead>
			
			<tr>
			<th colspan="2">IPTC Header Information:</th>
			</tr>
			
			</thead>
			
			<tbody>
			
			{if $image_iptc.caption != ""}
			<tr class="{cycle values='list-one,list-two'}">
			<th>{$image_iptc.descriptors.caption}:</th>
			<td>{$image_iptc.caption}</td>
			</tr>
			{/if}
			
			{if $image_iptc.headline != ""}
			<tr class="{cycle values='list-one,list-two'}">
			<th>{$image_iptc.descriptors.headline}:</th>
			<td>{$image_iptc.headline}</td>
			</tr>
			{/if}
			
			{if $image_iptc.byline != ""}
			<tr class="{cycle values='list-one,list-two'}">
			<th>{$image_iptc.descriptors.byline}:</th>
			<td>{$image_iptc.byline}</td>
			</tr>
			{/if}
			
			{if $image_iptc.bltitle != ""}
			<tr class="{cycle values='list-one,list-two'}">
			<th>{$image_iptc.descriptors.bltitle}:</th>
			<td>{$image_iptc.bltitle}</td>
			</tr>
			{/if}
			
			{if $image_iptc.credit != ""}
			<tr class="{cycle values='list-one,list-two'}">
			<th>{$image_iptc.descriptors.credit}:</th>
			<td>{$image_iptc.credit}</td>
			</tr>
			{/if}
			
			{if $image_iptc.source != ""}
			<tr class="{cycle values='list-one,list-two'}">
			<th>{$image_iptc.descriptors.source}:</th>
			<td>{$image_iptc.source}</td>
			</tr>
			{/if}
			
			{if $image_iptc.objname != ""}
			<tr class="{cycle values='list-one,list-two'}">
			<th>{$image_iptc.descriptors.objname}:</th>
			<td>{$image_iptc.objname}</td>
			</tr>
			{/if}
			
			{if $image_iptc.keywords != ""}
			<tr class="{cycle values='list-one,list-two'}">
			<th>{$image_iptc.descriptors.keywords}:</th>
			<td>{$image_iptc.keywords}</td>
			</tr>
			{/if}
			
			{if $image_iptc.city != ""}
			<tr class="{cycle values='list-one,list-two'}">
			<th>{$image_iptc.descriptors.city}:</th>
			<td>{$image_iptc.city}</td>
			</tr>
			{/if}
			
			{if $image_iptc.province != ""}
			<tr class="{cycle values='list-one,list-two'}">
			<th>{$image_iptc.descriptors.province}:</th>
			<td>{$image_iptc.province}</td>
			</tr>
			{/if}
			
			{if $image_iptc.country != ""}
			<tr class="{cycle values='list-one,list-two'}">
			<th>{$image_iptc.descriptors.country}:</th>
			<td>{$image_iptc.country}</td>
			</tr>
			{/if}
			
			{if $image_iptc.reference != ""}
			<tr class="{cycle values='list-one,list-two'}">
			<th>{$image_iptc.descriptors.reference}:</th>
			<td>{$image_iptc.reference}</td>
			</tr>
			{/if}
			
			{if $image_iptc.copyright != ""}
			<tr class="{cycle values='list-one,list-two'}">
			<th>{$image_iptc.descriptors.copyright}:</th>
			<td>{$image_iptc.copyright}</td>
			</tr>
			{/if}
			
			</tbody>
			
			</table>
		
		{/if}
		
		{if $exif_file}
		
			<table class="table-popup-form">
			
			<thead>
			
			<tr>
			<th colspan="2">EXIF Data Segment: File</th>
			</tr>
			
			</thead>
			
			<tbody>
			
			{section name="file" loop=$exif_file_keys}
			
			<tr class="{cycle values='list-one,list-two'}">
			<th>{$exif_file_keys[file]}:</th>
			<td>{$exif_file_values[file]}</td>
			</tr>
			
			{/section}
			
			</tbody>
			
			</table>
		
		{/if}
		
		{if $exif_computed}
		
			<table class="table-popup-form">
			
			<thead>
			
			<tr>
			<th colspan="2">EXIF Data Segment: Computed</th>
			</tr>
			
			</thead>
			
			<tbody>
			
			{section name="computed" loop=$exif_computed_keys}
			
			<tr class="{cycle values='list-one,list-two'}">
			<th>{$exif_computed_keys[computed]}:</th>
			<td>{$exif_computed_values[computed]}</td>
			</tr>
			
			{/section}
			
			</tbody>
			
			</table>
		
		{/if}
		
		{if $exif_ifd0}
		
			<table class="table-popup-form">
			
			<thead>
			
			<tr>
			<th colspan="2">EXIF Data Segment: IFD0</th>
			</tr>
			
			</thead>
			
			<tbody>
			
			{section name="ifd0" loop=$exif_ifd0_keys}
			
			<tr class="{cycle values='list-one,list-two'}">
			<th>{$exif_ifd0_keys[ifd0]}:</th>
			<td>{$exif_ifd0_values[ifd0]}</td>
			</tr>
			
			{/section}
			
			</tbody>
			
			</table>
		
		{/if}
		
		{if $exif_thumbnail}
		
			<table class="table-popup-form">
			
			<thead>
			
			<tr>
			<th colspan="2">EXIF Data Segment: Thumbnail</th>
			</tr>
			
			</thead>
			
			<tbody>
						
			<tr class="{cycle values='list-one,list-two'}">
			<th>EXIF Thumbnail:</th>
			<td>
			<img src="data:image/jpeg;base64,{$exif_thumbnail_data}" alt="EXIF Image Thumbnail" border="0" /></td>
			</tr>
			
			</tbody>
			
			</table>
		
		{/if}
		
		{if $exif_exif}
		
			<table class="table-popup-form">
			
			<thead>
			
			<tr>
			<th colspan="2">EXIF Data Segment: EXIF</th>
			</tr>
			
			</thead>
			
			<tbody>
			
			{section name="exif" loop=$exif_exif_keys}
			
			<tr class="{cycle values='list-one,list-two'}">
			<th>{$exif_exif_keys[exif]}:</th>
			<td>{$exif_exif_values[exif]}</td>
			</tr>
			
			{/section}
			
			</tbody>
			
			</table>
		
		{/if}
	
	{else}
		
		<form method="post" action="">
		
		<table class="table-popup-text">
		
		<tr>
		<th>No metadata present</th>
		<td>
		
		<h1>About metdata</h1>
		
		<p>There is no readable IPTC or EXIF metadata in this image file.</p>
		
		<input type="button" value="Close this window" name="close" onclick="javascript:self.close();" style="width:100%;" class="formbutton" />
		
		</td>
		</tr>
		
		</table>
		
		</form>
		
	{/if}
	
	
	</div>

</div>


</body>
</html>