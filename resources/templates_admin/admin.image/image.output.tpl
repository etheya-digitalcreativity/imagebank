{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>Image Preview</title>
	
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/scriptaculous/prototype.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/scriptaculous/scriptaculous.js"></script>

	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.scriptaculous.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.global.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.main.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.tooltip.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.navigation.js"></script>	
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.multifile.js"></script>	

	<!--[if lt IE 8.]><script type="text/javascript" src="{$base_url}resources/javascript/pixaria.pngfix.js"></script><![endif]-->
	
	<style type="text/css">
	
	{literal}
	  
	html, body
		{
		height:100%;
		margin: 0; 
		padding: 0; 
		overflow: none; 
		background-color: #7F7F7F;
		}
	
	#controls
		{ 
		color:#AAA; 
		font:9px Verdana; 
		background-color:#222; 
		xopacity:0.8; 
		padding:5px; 
		margin:10px;
		position:absolute;
		z-index:100;
		width:250px;
		}
	
	span.title
		{
		display:block;
		font-size:11px;
		color:#FFF;
		}
	
	#controls a
		{
		color: #AAA;
		}
	
	#image
		{
		width: {/literal}{$image_width}{literal}px;
		height: {/literal}{$image_height}{literal}px;
		text-align: center;
		position: absolute;
		left: 50%;
		top: 50%;
		margin-left: -{/literal}{$image_width_half}{literal}px; 
		margin-top: -{/literal}{$image_height_half}{literal}px; 
		}
	
	{/literal}
	
	</style>
	
</head>

<body id="image-preview">

<div id="controls" style="display:none; z-index:1">
	<!--
	<div onclick="lightBoxWhite()" style="cursor:pointer;float:right;background-color:#FFF;height:10px;width:10px;"></div>
	<div onclick="lightBoxGray()" style="cursor:pointer;float:right;background-color:#7F7F7F;height:10px;width:10px;"></div>
	<div onclick="lightBoxBlack()" style="cursor:pointer;float:right;background-color:#000;height:10px;width:10px;"></div>
	-->
	<span class="title">{$image_filename}</span>{$image_filetype}, {$image_filesize}

</div>

<div id="image"><img src="{$base_url}{$fil_admin_image_output}?file={$filepath}&amp;" width="{$image_width}" height="{$image_height}" alt="" /></div>

<script type="text/javascript" language="javascript">
// <![CDATA[
{literal}
$('image-preview')._lightbox = '#7F7F7F';

function lightBoxWhite() {
  if($('image-preview')._lightbox != '#FFFFFF') {
	new Effect.Highlight('image-preview',{
	  duration: 0.5,
	  restorecolor:'#FFFFFF',startcolor:$('image-preview')._lightbox,endcolor:'#FFFFFF' 
	});
	$('image-preview')._lightbox = '#FFFFFF';
  }
}

function lightBoxGray() {
  if($('image-preview')._lightbox != '#7F7F7F') {
	new Effect.Highlight('image-preview',{
	  duration: 0.5,
	  restorecolor:'#7F7F7F',startcolor:$('image-preview')._lightbox,endcolor:'#7F7F7F'
	});
	$('image-preview')._lightbox = '#7F7F7F';
  }
}

function lightBoxBlack() {
  if($('image-preview')._lightbox != '#000000') {
	new Effect.Highlight('image-preview',{
	  duration: 0.5,
	  restorecolor:'#000000',startcolor:$('image-preview')._lightbox,endcolor:'#000000'
	});
	$('image-preview')._lightbox = '#000000';
  }
}

var timer;

function displayInfo() {
  if(!Element.visible('controls')) Effect.Appear('controls',{to:0.8});
  if($('comments') && !Element.visible('comments')) Effect.Appear('comments',{to:0.8});
  if(timer) clearTimeout(timer);
  timer = setTimeout('fadeInfo()', 2500);
}

function fadeInfo() {
  if(Element.visible('controls')) Effect.Fade('controls');
  if($('comments') && Element.visible('comments')) Effect.Fade('comments');
}

Event.observe(document, 'mousemove', displayInfo);
displayInfo();

{/literal}
// ]]>
</script>

</body>
</html>