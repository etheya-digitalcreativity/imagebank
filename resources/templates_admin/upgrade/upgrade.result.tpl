{*

/*
*
*	Pixaria Gallery 
*	Copyright Jamie Longstaff
*
*/

*}<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/2000/REC-xhtml1-20000126/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	
	<!-- Query String: {$query_string} -->
	<!-- Base URI: {$base_uri} -->
	
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	
	<title>{$set_site_name} &raquo; {$page_title}</title>
	
	<meta name="generator" content="Pixaria Gallery" />
	
	<meta name="keywords" content="Pixaria Gallery, popsoft, gallery, album, photography" />
	
	<meta name="description" content="Photo album powered by Pixaria Gallery." />
	
	{ if $set_news_enable == "1" }<link href="{$base_url}{$fil_news_rss}" rel="alternate" type="application/rss+xml" title="{$set_site_name} News" />{/if}
	
	<link rel="stylesheet" href="{$base_url}resources/css/pixaria.2.0.css" />
	<link rel="stylesheet" href="{$base_url}resources/css/pixaria.3.0.css" />
	 
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/scriptaculous/prototype.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/scriptaculous/scriptaculous.js"></script>

	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.scriptaculous.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.global.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.main.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.navigation.js"></script>	

	<!--[if lt IE 7]>
	<script defer type="text/javascript" src="{$base_url}resources/javascript/pixaria.pngfix.js"></script>
	<![endif]-->

</head>

<body>

<div id="content">
	
	<div id="masthead">
	
		<table id="session">
		
			<tr>
				<td class="name">{if $ses_psg_userid}{$ses_psg_name}{else}You are not currently logged in{/if}</td>
				{if $ses_psg_userid}<td class="logout"><a href="{$base_url}{$fil_index_login}?cmd=signout" title="End your login session on this site"><img src="{$base_url}resources/images/bezel/logout.gif" width="67" height="14" alt="Sign out" title="Sign out" border="0" /></a></td>{/if}
			</tr>
			
			<tr>
				<td colspan="2" class="version">Pixaria Gallery Version {$set_software_version}</td>
			</tr>
		
		</table>
	
	</div>
		
	<div>
	

<div id="admin-outerdiv">

<div id="admin-innerdiv">

{if $database_modified}

	<table class="table-text">
	
		<tr>
		<th>Your database has been updated to version {$database_version}</th>
		<td>
		
		<h1>Database updated successfully!</h1>
		
		<p>Your database has been successfully upgraded and details of the changes made during this upgrade are shown in the list below.</p>
		
		{include_template file="admin.snippets/admin.html.notification.note.tpl" form_title_text="The current database version is now $database_version.<br /><br />The current software version is $software_version."}
		
		<br /><br />
		
		<form action="{$base_url}{$fil_admin_dashboard}" method="post">
		<input type="submit" name="submit" value="Go to the site Dashboard" style="width:100%;" class="formbutton" />
		</form>
		
		</td>
		<td>&nbsp;</td>
		</tr>
		
	</table>
	
	<table class="table-form">
		
		<thead>
		
		<tr>
		<th>Database commands completed:</th>
		</tr>
		
		</thead>
		
		<tbody>
		
		{section name="result" loop=$database_report}
		
			<tr class="{cycle values='list-one,list-two'}">
			<th>{$database_report[result]}</th>
			</tr>
		
		{sectionelse}
		
			<tr class="{cycle values='list-one,list-two'}">
			<th>There are no change reports to display for this update.</th>
			</tr>
		
		{/section}
		
		</tbody>
		
	</table>
		
{else}
	
	<table class="table-text">
	
		<tr>
		<th>Your database has not been modified.</th>
		<td>
		
		<h1>Database was not altered</h1>
		
		<p>Your database has not been altered by running this script.  If you were expecting the database to be upgraded, then you may be seeing this message because you refreshed the page accidentally.</p>
		
		<p>If you continue to experience probems, you are advised to contact info@pixaria.com.</p>
		
		{include_template file="admin.snippets/admin.html.notification.note.tpl" form_title_text="The current database version is $database_version.<br /><br />The current software version is $software_version."}
		
		<br /><br />
		
		<form action="{$base_url}{$fil_admin_dashboard}" method="post">
		<input type="submit" name="submit" value="Go to the site Dashboard" style="width:100%;" class="formbutton" />
		</form>
		
		</td>
		<td>&nbsp;</td>
		</tr>
		
	</table>
	
{/if}



</div>

</div>

{include_template file="admin.snippets/admin.html.footer.tpl"}

