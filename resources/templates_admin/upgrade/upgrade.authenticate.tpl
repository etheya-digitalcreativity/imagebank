{*

/*
*
*	Pixaria Gallery 
*	Copyright Jamie Longstaff
*
*/

*}<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/2000/REC-xhtml1-20000126/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	
	<!-- Query String: {$query_string} -->
	<!-- Base URI: {$base_uri} -->
	
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	
	<title>{$set_site_name} &raquo; {$page_title}</title>
	
	<meta name="generator" content="Pixaria Gallery" />
	
	<meta name="keywords" content="Pixaria Gallery, popsoft, gallery, album, photography" />
	
	<meta name="description" content="Photo album powered by Pixaria Gallery." />
	
	{ if $set_news_enable == "1" }<link href="{$base_url}{$fil_news_rss}" rel="alternate" type="application/rss+xml" title="{$set_site_name} News" />{/if}
	
	<link rel="stylesheet" href="{$base_url}resources/css/pixaria.2.0.css" />
	<link rel="stylesheet" href="{$base_url}resources/css/pixaria.3.0.css" />
	 
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/scriptaculous/prototype.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/scriptaculous/scriptaculous.js"></script>

	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.scriptaculous.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.global.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.main.js"></script>
	<script language="JavaScript" type="text/javascript" src="{$base_url}resources/javascript/pixaria.navigation.js"></script>	

	<!--[if lt IE 7]>
	<script defer type="text/javascript" src="{$base_url}resources/javascript/pixaria.pngfix.js"></script>
	<![endif]-->

</head>

<body>

<div id="content">
	
	<div id="masthead">
	
		<table id="session">
		
			<tr>
				<td class="name">{if $ses_psg_userid}{$ses_psg_name}{else}You are not currently logged in{/if}</td>
				{if $ses_psg_userid}<td class="logout"><a href="{$base_url}{$fil_index_login}?cmd=signout" title="End your login session on this site"><img src="{$base_url}resources/images/bezel/logout.gif" width="67" height="14" alt="Sign out" title="Sign out" border="0" /></a></td>{/if}
			</tr>
			
			<tr>
				<td colspan="2" class="version">Pixaria Gallery Version {$set_software_version}</td>
			</tr>
		
		</table>
	
	</div>
		
	<div>
	

<div id="admin-outerdiv">

<div id="admin-innerdiv">

<form action="{$sys_base_url}{$fil_admin_upgrade}" method="post">

<table class="table-form">

<thead>

<tr>
<th colspan="3">Authenticate as an administrator to continue</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>E-Mail Address:</th>
<td><input type="text" maxlength="50" size="20" name="email" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>Password:</th>
<td><input type="password" maxlength="50" size="20" name="password" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="submit" name="submit" value="Authenticate and run database upgrade" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>


</div>

</div>

{include_template file="admin.snippets/admin.html.footer.tpl"}

