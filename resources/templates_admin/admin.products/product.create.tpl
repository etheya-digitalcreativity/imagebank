{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">
	
<table class="table-text">

<tr>
<th>There were errors in the information you submitted on the previous page.  Please review the mistakes and re-submit the form.</td>

<td>

{if $error}

	<h1>Please correct these errors</h1>
	
	<ol>
	{section name="errors" loop=$error_log}
	<li class="html-error-warning">{$error_log[errors]}</li>
	{/section}
	</ol>

{/if}

</td>
<td>&nbsp;</td>
</tr>

</table>

<form action="{$base_url}{$fil_admin_products}" method="post">

<input type="hidden" name="cmd" value="1" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">Create a new product to sell on your website:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>Type of product:</th>
<td>

<input type="radio" name="prod_type" value="IMG" class="forminput" onclick="javascript:if ($('dnl').style.display != 'none') Effect.SlideUp('dnl',{literal}{duration:0.2}{/literal});" {if $prod_type == "IMG" || $prod_type == ""}checked="checked"{/if} /> Image based product e.g. framed print<br />

<input type="radio" name="prod_type" value="DNL" class="forminput" onclick="javascript:if ($('dnl').style.display == 'none') Effect.SlideDown('dnl',{literal}{duration:0.2}{/literal});" {if $prod_type == "DNL"}checked="checked"{/if}  /> Instant image download<br />

	<div id="dnl" {if $prod_type != "DNL"} style="display:none;"{/if}>
	<table cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-left:20px;">
	<select name="prod_dnl_size">
		<option value="comping">Choose size of image</option>
		<option value="comping" {if $prod_dnl_size == "comping"} selected="selected"{/if}>{$comping_dir} pixels</option>
		<option value="original" {if $prod_dnl_size == "original"} selected="selected"{/if}>Maximum available size</option>
	</select>
	</td></tr></table>
	</div>


<input type="radio" name="prod_type" value="IND" class="forminput" {if $prod_type == "IND"}checked="checked"{/if} disabled="disabled" /> Stand alone product e.g. compilation CD-ROM
</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Name of this product:</th>
<td><input type="text" name="prod_name" value="{$prod_name}" class="forminput" style="width:100%;" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Detailed description of this product:</th>
<td><textarea name="prod_description" class="forminput" style="width:100%;" rows="4">{$prod_description}</textarea></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Price of this product ({$set_store_currency}):</th>
<td>{$set_store_symbol}<input type="text" name="prod_price" value="{$prod_price}" class="forminput" style="width:70px;" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Shipping for the first item ({$set_store_currency}):</th>
<td>{$set_store_symbol}<input type="text" name="prod_shipping" value="{$prod_shipping}" class="forminput" style="width:70px;" /> (Will be ignored if 'Use a flat rate for shipping fees' is set in Store Settings.)<br /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Shipping for each subsequent item ({$set_store_currency}):</th>
<td>{$set_store_symbol}<input type="text" name="prod_shipping_multiple" value="{$prod_shipping_multiple}" class="forminput" style="width:70px;" /> (Will be ignored if 'Use a flat rate for shipping fees' is set in Store Settings.)<br /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>Product status:</th>
<td><input type="checkbox" name="prod_active" class="forminput" {if $prod_active}checked="checked"{/if} /> (Check to make product live)<br /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="submit" value="Save this new product" style="width:100%;" class="formbutton"></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>

</div>

</div>



<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}


