{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="admin.snippets/admin.html.header.tpl"}


<div id="admin-outerdiv">

<div id="admin-innerdiv">
	
<table class="table-text">

<tr>
<th>Use this control panel to manage products for sale on your website.</th>

<td>

{if $error}
<ol>
{section name="errors" loop=$error_log}

<li class="html-error-warning">{$error_log[errors]}</li>

{/section}
</ol>
{/if}

{include_template file="admin.snippets/admin.html.notification.note.tpl" form_title_text="Please note that this product manager is still in development.  At the present time, it isn't possible to add stand alone products such as CD-ROMs or books."}

</td>
<td>&nbsp;</td>
</tr>

</table>



<form action="{$base_url}{$fil_admin_products}" method="post">

<input type="hidden" name="cmd" value="1" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">Create a new product to sell on your website:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>Type of product:</th>
<td>

<input type="radio" name="prod_type" value="IMG" class="forminput" onclick="javascript:if ($('dnl').style.display != 'none') Effect.SlideUp('dnl',{literal}{duration:0.2}{/literal});" {if $prod_type == "IMG" || $prod_type == ""}checked="checked"{/if} /> Image based product e.g. framed print<br />

<input type="radio" name="prod_type" value="DNL" class="forminput" onclick="javascript:if ($('dnl').style.display == 'none') Effect.SlideDown('dnl',{literal}{duration:0.2}{/literal});" {if $prod_type == "DNL"}checked="checked"{/if}  /> Instant image download<br />

	<div id="dnl" {if $prod_type != "DNL"} style="display:none;"{/if}>
	<table cellpadding="0" cellspacing="0" border="0"><tr><td style="padding-left:20px;">
	<select name="prod_dnl_size">
		<option value="comping">Choose size of image</option>
		<option value="comping" {if $prod_dnl_size == "comping"} selected="selected"{/if}>{$comping_dir} pixels</option>
		<option value="original" {if $prod_dnl_size == "original"} selected="selected"{/if}>Maximum available size</option>
	</select>
	</td></tr></table>
	</div>

<input type="radio" name="prod_type" value="IND" class="forminput" disabled="disabled" /> Stand alone product e.g. compilation CD-ROM
</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Name of this product:</th>
<td>
<input type="text" name="prod_name" class="forminput" style="width:100%;" />
</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Detailed description of this product:</th>
<td><textarea name="prod_description" class="forminput" style="width:100%;" rows="4" cols="16"></textarea></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Price of this product ({$set_store_currency}):</th>
<td>{$set_store_symbol}<input type="text" name="prod_price" value="0.00" class="forminput" style="width:70px;" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Shipping for the first item ({$set_store_currency}):</th>
<td>{$set_store_symbol}<input type="text" name="prod_shipping" value="0.00" class="forminput" style="width:70px;" /> (Will be ignored if 'Use a flat rate for shipping fees' is set in Store Settings.)<br /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>Shipping for each subsequent item ({$set_store_currency}):</th>
<td>{$set_store_symbol}<input type="text" name="prod_shipping_multiple" value="0.00" class="forminput" style="width:70px;" /> (Will be ignored if 'Use a flat rate for shipping fees' is set in Store Settings.)<br /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>Product status:</th>
<td><input type="checkbox" name="prod_active" class="forminput" checked="checked" /> (Check to make product live)<br /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="submit" value="Save this new product" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>



<table class="table-listbox">

<tr>
	<th>Name of this product</th>	
	<th>Type</th>
	<th>Price</th>
	<th>Shipping</th>
	<th>Shipping (multiple)</th>
	<th>Actions</th>
</tr>

{if $show_list}

	{section name="products" loop=$prod_id}
	
		<tr class="{cycle values='list-one,list-two'}">
		
		<td style="height:30px;">{$prod_name[products]}</td>
		
		<td>{$prod_type_list[products]}</td>
		
		<td>{$prod_price[products]}</td>
		
		<td>{$prod_shipping[products]}</td>
		
		<td>{$prod_shipping_multiple[products]}</td>
		
		<td>
			
			<ul class="actionlist">
			<li><a href="{$base_url}{$fil_admin_products}?cmd=2&amp;prod_id={$prod_id[products]}">Edit</a></li>
			<li><a href="javascript:confirmLink('{$base_url}{$fil_admin_products}?cmd=5&amp;prod_id={$prod_id[products]}','Delete this product from the database?');">Delete</a></li>
			</ul>
			
		</td>
		
		</tr>
		
	{/section}	

{else}

	<tr class="{cycle values='list-one,list-two'}">
	
	<td colspan="6" style="height:30px;">There are no products to show.</td>
	
	</tr>

{/if}

</table>

</div>

</div>


<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="admin.snippets/admin.html.footer.tpl"}


