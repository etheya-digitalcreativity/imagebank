<?php 

/****************************************************************
**
**	Pixaria Localisation Data
**	Copyright Jamie Longstaff
**
**	Translation by:		Eduardo Hutter - 
**					contact {at} caipira {dot} ca
**					Por favor reporte qualquer erro encontrado
**					para que seja corrigido. Please report any
**					misspelling or error that you may find to
**					this email, thank you
**
**       Language:			Brazilian Portuguese - pt_BR
**
**	Script author:		Jamie Longstaff
**	Script version:		2.x
**
****************************************************************/

// Generic
$strings['EMAIL_001'] = "Mensagem de \$site_name";

// account.edit.tpl
$strings['EMAIL_002'] = "Caro(a) \$to_name,";
$strings['EMAIL_003'] = "Esta é uma mensagem do administrador do website \$site_name para notificá-lo que sua conta em  \$site_name foi atualizada com sucesso.";
$strings['EMAIL_004'] = "Caso tenha qualquer dúvida ou se esta mensagem é inesperada, por favor responda-a e entraremos em contato com você assim que for nos possível!";
$strings['EMAIL_005'] = "Atenciosamente,";

// buy.notify.administrator.tpl
$strings['EMAIL_006'] = "Caro(a) \$name,";
$strings['EMAIL_007'] = "Esta é uma mensagem automática do website \$site_name para notificá-lo de uma nova transação:";
$strings['EMAIL_008'] = "Método de pagamento:";
$strings['EMAIL_009'] = "ID da transação:";
$strings['EMAIL_010'] = "ID do Carrinho:";
$strings['EMAIL_011'] = "ID do usuário:";
$strings['EMAIL_012'] = "Usuário:";
$strings['EMAIL_013'] = "Ver a transação:";

// buy.notify.duplicate.tpl
$strings['EMAIL_014'] = "Caro(a) \$to_name,";
$strings['EMAIL_015'] = "Esta é uma mensagem automática do website \$site_name para notificá-lo de uma transação duplicada";
$strings['EMAIL_016'] = "Método de pagamento:";
$strings['EMAIL_017'] = "ID da transação:";
$strings['EMAIL_018'] = "ID do Carrinho:";
$strings['EMAIL_019'] = "ID do usuário:";
$strings['EMAIL_020'] = "Usuário:";
$strings['EMAIL_021'] = "Ver a transação:";

// buy.notify.tpl
$strings['EMAIL_022'] = "Caro(a) \$name,";
$strings['EMAIL_023'] = "Esta é uma mensagem automática do administrador do website \$site_name para confirmar que o pagamento do seu Carrinho foi submetido com sucesso. Caso tenha qualquer dúvida ou se esta mensagem é inesperada, por favor responda-a e entraremos em contato com você assim que for nos possível!";
$strings['EMAIL_024'] = "Atenciosamente,";

// login.notify.tpl
$strings['EMAIL_025'] = "O usuário \$user_name acabou de se logar em \$site_name com o endereço IP: \$ip_address";
$strings['EMAIL_026'] = "Múltiplos logins para uma mesma conta onde o endereço IP é completamente diferente a cada vez pode indicar que esta conta está sendo compartilhada por vários usuários.";
$strings['EMAIL_027'] = "Você pode desligar estas notificações aqui:";
$strings['EMAIL_028'] = "Veja informações detalhadas sobre o login de usuários:";

// password.reset.tpl
$strings['EMAIL_029'] = "Caro(a) \$to_name,";
$strings['EMAIL_030'] = "Esta é uma mensagem automática do administrador do website \$site_name para lhe dizer que a sua senha no site \$site_name foi alterada com sucesso.";
$strings['EMAIL_031'] = "Seu nome de usuário é: \$to_email";
$strings['EMAIL_032'] = "Sua senha é: \$password";
$strings['EMAIL_033'] = "Caso tenha qualquer dúvida ou se esta mensagem é inesperada, por favor responda-a e entraremos em contato com você assim que for nos possível!";
$strings['EMAIL_034'] = "Atenciosamente,";

// postcard.content.tpl
$strings['EMAIL_035'] = "Este cartão lhe foi enviado por \$name da galeria em <a href=\"\$base_url\" style=\"color:#777;\">\$set_site_name</a>.";
$strings['EMAIL_036'] = "E-cards powered by <a href=\"http://www.pixaria.com/\" style=\"color:#777;\">Pixaria Gallery</a>.";

// registration.confirmation.01.tpl
$strings['EMAIL_037'] = "Caro(a) \$to_name,";
$strings['EMAIL_038'] = "Esta é uma mensagem automática do administrador do website \$site_name Sua conta em \$site_name foi criada com sucesso e seu pedido será examinado por um administrador assim que for possível.";
$strings['EMAIL_039'] = "Enquanto sua conta não for aprovada você não verá álbuns que sejam protegidos por senha mas poderá se logar na sua conta e usar todos os recursos da mesa de luz do website.";
$strings['EMAIL_040'] = "Seu nome de usuário é: \$to_email";
$strings['EMAIL_041'] = "Sua senha é: \$password";
$strings['EMAIL_042'] = "Caso tenha qualquer dúvida ou se esta mensagem é inesperada, por favor responda-a e entraremos em contato com você assim que for nos possível!";
$strings['EMAIL_043'] = "Atenciosamente,";

// registration.confirmation.02.tpl
$strings['EMAIL_044'] = "Caro(a) \$set_contact_name,";
$strings['EMAIL_045'] = "Esta é uma mensagem automática do website \$site_name para lhe dizer que um novo usuário se registrou.";
$strings['EMAIL_046'] = "O e-mail do usuário é: \$to_email";
$strings['EMAIL_047'] = "Para administrar este usuário faça o seu login no painél de controle de administração aqui:";

// submit.cart.admin.tpl
$strings['EMAIL_048'] = "Esta é uma mensagem automática do administrador do website \$site_name.";
$strings['EMAIL_049'] = "O usuário \$to_name submeteu uma transação no site \$site_name, mais detalhes desta transação podem ser vistos aqui:";

// submit.cart.user.tpl
$strings['EMAIL_050'] = "Caro(a) \$to_name,";
$strings['EMAIL_051'] = "Esta é uma mensagem automática do administrador do website \$site_name.";
$strings['EMAIL_052'] = "Você submeteu com sucesso o seu carrinho de compras e agora pode revisar seu pedido a qualquer momento visitando a URL abaixo:";
$strings['EMAIL_053'] = "Caso tenha qualquer dúvida ou se esta mensagem é inesperada, por favor responda-a e entraremos em contato com você assim que for nos possível!";
$strings['EMAIL_054'] = "Atenciosamente,";

// transaction.message.admin.tpl
$strings['EMAIL_055'] = "Esta é uma mensagem automática do website \$site_name.";
$strings['EMAIL_056'] = "O usuário \$to_name colocou uma mensagem em relação a esta transação:";

?>