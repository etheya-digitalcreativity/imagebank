<?php 

/****************************************************************
**
**	Pixaria Localisation Data
**	Copyright Jamie Longstaff
**
**	Translation by:		Jamie Longstaff (Original File)
**
**	Script author:		Jamie Longstaff
**	Script version:		2.x
**
****************************************************************/

/****************************************************************
**
**	THIS FILE MUST BE SAVED AS "UTF-8 NO BOM" (BYTE ORDER MARK)
**
**	In order to work properly, this file MUST be saved as a UTF-8
**	encoded file with no byte order mark at the start of the file.
**
**  If you save this file with a BOM, you will get errors in your
**	HTML output when you try to view your site.
**
**	Most good text editors will allow you to save the file without
**	a BOM but Windows Notepad will not and should be avoided..!
**
****************************************************************/

/****************************************************************
**
**	To use assigned Smarty variables in localised strings, you
**	must ensure that the variable dollar sign is escaped with a
**	backslash character like this:
**	
**	\$myvariable_name
**
****************************************************************/

// Set the PHP language, location and time settings
setlocale(LC_TIME, 'pl_PL');

// Local language code (for RSS newsfeed)
$strings['LANG_CODE']					= "pl";

// The required character set for your language text.
$strings['LANG_CHARSET'] 				= "utf-8";


/****************************************************************
**
** Version update 	-	2.7.1
**
****************************************************************/

$strings['DEFAULT_LANG_01']			= "Wybierz twój preferowany język";
$strings['DEFAULT_LANG_02']			= "Wybierz preferowany język do przeglądania witryny.";
$strings['DEFAULT_LANG_03']			= "Zapisz wybór języka";
$strings['DEFAULT_LANG_04']			= "Język";

$strings['LIGHTBOX_47_1']			= "Zarządzaj lightbox'ami";

$strings['STORE_058_1']				= "W";

$strings['COLOR_HUD_00']			= "Wybierz kolor";


/****************************************************************
**
** Version update 	-	2.7.0
**
****************************************************************/

$strings['LIGHTBOX_79']				= "Zobacz moje zdjęcia w lightbox";

$strings['POSTCARD_23']				= "wysłał(a) Ci kartkę!"; // i.e. Bob has sent you a postcard!
$strings['POSTCARD_24']				= "Aby zobaczyć tę wiadomość, użyj programu pocztowego pokazującego wiadomości w HTML!";
$strings['POSTCARD_25']				= "Twoja kartka została wysłana";
$strings['POSTCARD_26']				= "Podejrzyj swoją kartkę";
$strings['POSTCARD_27']				= "Wyślij kartkę";

$strings['ACCOUNT_121']				= "Aktywne";
$strings['ACCOUNT_122']				= "Oczekuje oferty";
$strings['ACCOUNT_123']				= "Oczekuje płatności";
$strings['ACCOUNT_124']				= "Kompletne";


/****************************************************************
**
** Version update 	-	2.6.2
**
****************************************************************/

$strings['IM_THUMBS_33']			= "Następne";
$strings['IM_THUMBS_34']			= "Poprzednie";

$strings['STORE_192']				= "Twój koszyk";
$strings['STORE_193']				= "Twój koszyk jest pusty";
$strings['STORE_194']				= "Opcje zakupu";
$strings['STORE_195']				= "Przelicz cenę na podstawie użycia";
$strings['STORE_196']				= "Koszyk zaktualizowany";
$strings['STORE_197']				= "Oferta wygenerowana na podstawie przeliczenia użycia";
$strings['STORE_198']				= "Prośba o ofertę";
$strings['STORE_199']				= "Przetwarzanie oferty";
$strings['STORE_200']				= "Zamówienie";
$strings['STORE_201']				= "Zobacz twoje zapytania o oferty";
$strings['STORE_202']				= "Ten koszyk jest pusty";

/****************************************************************
**
** Section title 	-	Image Output Pages
**
** File name 		-	Multiple files
**
****************************************************************/

// image.output.tpl
$strings['IM_OUTPUT_01']		= "Podgląd zdjęcia";
$strings['IM_OUTPUT_02']		= "Ładowanie podglądu";

// image.popup.tpl, view.detail.thin.tpl, view.detail.wide.tpl
$strings['IM_DETAIL_01']		= "Start";
$strings['IM_DETAIL_02']		= "Galerie";
$strings['IM_DETAIL_03']		= "Wyniki poszukiwania";
$strings['IM_DETAIL_04']		= "Lightbox";
$strings['IM_DETAIL_05']		= "Nie widać zdjęć?  Pobierz darmowy <a href=\"http://www.adobe.com/go/getflashplayer\">Adobe Flash Viewer</a>";
$strings['IM_DETAIL_06']		= "No image displaying? Pobierz darwmoy <a href=\"http://www.adobe.com/svg/viewer/install/\">Adobe SVG Viewer</a>";
$strings['IM_DETAIL_07']		= "Podsumowanie";
$strings['IM_DETAIL_08']		= "Pobrania";
$strings['IM_DETAIL_09']		= "Data";
$strings['IM_DETAIL_10']		= "Plik";
$strings['IM_DETAIL_11']		= "Zarządzanie prawami";
$strings['IM_DETAIL_12']		= "Prawa ustawione";
$strings['IM_DETAIL_13']		= "Darmowe";
$strings['IM_DETAIL_14']		= "Zakres użycia";
$strings['IM_DETAIL_15']		= "Żadne";
$strings['IM_DETAIL_16']		= "Akcja";
$strings['IM_DETAIL_17']		= "Wyślij pocztówkę";
$strings['IM_DETAIL_18']		= "Zamów opcje";
$strings['IM_DETAIL_19']		= "Dodaj zdjęcie do Lightbox";
$strings['IM_DETAIL_20']		= "Słowa kluczowe";
$strings['IM_DETAIL_21']		= "Brak słów kluczowych do tego zdjęcia.";
$strings['IM_DETAIL_22']		= "Nie wpisałeś słów do poszukiwania";
$strings['IM_DETAIL_23']		= "Szukaj wybranych słów kluczowych";
$strings['IM_DETAIL_24']		= "Pobierz spakowane";
$strings['IM_DETAIL_25']		= "Możesz pobrać skompresowaną wersję zdjęcia";
$strings['IM_DETAIL_26']		= "Pobierz skompresowany JPEG";
$strings['IM_DETAIL_27']		= "Pobierz zdjęcia niskiej rozdzielczości.";
$strings['IM_DETAIL_28']		= "Darmowe pobieranie zdjęć wysokiej rozdzielczości";
$strings['IM_DETAIL_29']		= "Dostępne opcje pobierania wysokiej rozdzielczości";
$strings['IM_DETAIL_30']		= "Pobierz JPEG";
$strings['IM_DETAIL_31']		= "Pobierz JPEG wysokiej rozdzielczości";
$strings['IM_DETAIL_32']		= "Pobierz JPEG 2000";
$strings['IM_DETAIL_33']		= "Pobierz JPEG 2000 wysokiej rozdzielczości.";
$strings['IM_DETAIL_34']		= "Pobierz TIFF";
$strings['IM_DETAIL_35']		= "Pobierz TIFF wysokiej rodzielczości.";
$strings['IM_DETAIL_36']		= "Pobierz plik Photoshop PSD";
$strings['IM_DETAIL_37']		= "Pobierz plik Photoshop PSD wysokiej rozdzielczości.";
$strings['IM_DETAIL_38']		= "Pobierz EPS";
$strings['IM_DETAIL_39']		= "Pobierz hermetyczny plik PostScript&trade; ";
$strings['IM_DETAIL_40']		= "Brak darmowych plików wysokiej rozdzielczości";
$strings['IM_DETAIL_41']		= "Nie ma darmowych plików wysokiej rozdzielczości dla tego zdjęcia";
$strings['IM_DETAIL_42']		= "Wydania";
$strings['IM_DETAIL_43']		= "Zdjęcie posiada zgodę modela(ki)";					
$strings['IM_DETAIL_44']		= "Zdjęcie posiada zgodę właściciela posesji";
$strings['IM_DETAIL_45']		= "Brak informacji o modelu(ce) lub posesji";
$strings['IM_DETAIL_46']		= "Edytuj zdjęcie";
$strings['IM_DETAIL_47']		= "Usuń z lightbox";

// view.neighbours.tpl
$strings['IM_NEAR_01']			= "Szybkie linki do innych zdjęć w tej galerii:";
$strings['IM_NEAR_02']			= "Szybkie linki do pozostałych wyników wyszukiwania:";
$strings['IM_NEAR_03']			= "Szybkie linki do zdjęć w lightbox:";
$strings['IM_NEAR_04']			= "Zdjęcie";

// view.thumbnails.tpl
$strings['IM_THUMBS_01']		= "Przeglądaj zdjęcia w tej galerii:";
$strings['IM_THUMBS_02']		= "Przeglądaj swój lightbox:";
$strings['IM_THUMBS_03']		= "Wyszukano zdjęć: \$results_total ";
$strings['IM_THUMBS_04']		= "Przeglądaj zdjęcia:";
$strings['IM_THUMBS_05']		= "Akcje:";
$strings['IM_THUMBS_06']		= "Dodaj wybrane do lightbox";
$strings['IM_THUMBS_07']		= "Usuń wybrane z lightbox";
$strings['IM_THUMBS_08']		= "Zaznacz wszystko";
$strings['IM_THUMBS_09']		= "Odznacz wszystko";
$strings['IM_THUMBS_10']		= "Edytuj";
$strings['IM_THUMBS_11']		= "Kliknij tutaj aby edytować aktywny lightbox";
$strings['IM_THUMBS_12']		= "Zarządzaj Lightbox'ami";
$strings['IM_THUMBS_13']		= "Zobacz szczegóły i zarządzaj swoimi lightbox'ami";
$strings['IM_THUMBS_14']		= "Wyślij znajomemu";
$strings['IM_THUMBS_15']		= "Podsumowanie w PDF";
$strings['IM_THUMBS_16']		= "Pobierz podsumowanie w PDF z zawartości lightbox";
$strings['IM_THUMBS_17']		= "Opcje miniatur:";
$strings['IM_THUMBS_18']		= "Duże";
$strings['IM_THUMBS_19']		= "Małe";
$strings['IM_THUMBS_20']		= "Musisz wybrać wartość większą niż 0";
$strings['IM_THUMBS_21']		= "Miniatur na stronę";
$strings['IM_THUMBS_22']		= "Numer storny:";
$strings['IM_THUMBS_23']		= "Prosty pokaz slajdów";
$strings['IM_THUMBS_24']		= "Pobierz skompresowane zdjęcie";
$strings['IM_THUMBS_25']		= "Dodaj do lightbox";
$strings['IM_THUMBS_26']		= "Usuń lightbox";
$strings['IM_THUMBS_27']		= "Tytuł:";
$strings['IM_THUMBS_28']		= "Akcje:";
$strings['IM_THUMBS_29']		= "Data:";
$strings['IM_THUMBS_30']		= "Plik:";
$strings['IM_THUMBS_31']		= "Wybierz:";
$strings['IM_THUMBS_32']		= "Opcje zakupu";


/****************************************************************
**
** Section title 	-	Account Management and Registration
**
** File name 		-	index.account/*
**
****************************************************************/

// account.created.tpl, account.edit.tpl, invitation.accept.tpl
$strings['ACCOUNT_001']			= "Stwórz profil, aby uzyskać dostęp do specjalnych opcji i możliwości.";
$strings['ACCOUNT_002']			= "Popraw te błędy";
$strings['ACCOUNT_003']			= "Jest problem z podanymi informacjami, poświęć chwilę aby przejrzeć te komunikaty:";
$strings['ACCOUNT_004']			= "Nie wpisano prawidłowego kodu weryfikacji, wróć i popraw.";
$strings['ACCOUNT_005']			= "Stwórz swoje konto";
$strings['ACCOUNT_006']			= "Aby zacząć używać w pełni możliwości tej strony, wypełnij formularz i załóż sobie konto. Możesz nie otrzymać konta, jeśli nie wypełnisz poprawnie całego formularza.";
$strings['ACCOUNT_007']			= "Informacje osobiste:";
$strings['ACCOUNT_008']			= "Tytuł:";
$strings['ACCOUNT_009']			= "Wymagane";
$strings['ACCOUNT_010']			= "Imię (imiona):";
$strings['ACCOUNT_011']			= "Nazwisko:";
$strings['ACCOUNT_012']			= "Adres e-mail:";
$strings['ACCOUNT_013']			= "Numer telefonu stacjonarnego:";
$strings['ACCOUNT_014']			= "Numer telefonu komórkowego:";
$strings['ACCOUNT_015']			= "Numer faksu:";
$strings['ACCOUNT_016']			= "Twoja firma zajmuje się:";
$strings['ACCOUNT_017']			= "Nazwa firmy lub organizacji:";
$strings['ACCOUNT_018']			= "Twoje stanowisko:";
$strings['ACCOUNT_019']			= "Jaki masz powód rejestracji:";
$strings['ACCOUNT_020']			= "Pozostało znaków";
$strings['ACCOUNT_021']			= "Jak często publikujesz:";
$strings['ACCOUNT_022']			= "Jaki jest twój nakład:";
$strings['ACCOUNT_023']			= "W jakich krajach publikujesz:";
$strings['ACCOUNT_024']			= "Jaki jest adres Twojej strony www:";
$strings['ACCOUNT_025']			= "Adres:";
$strings['ACCOUNT_026']			= "Miasto:";
$strings['ACCOUNT_027']			= "Województwo:";
$strings['ACCOUNT_028']			= "Kraj:";
$strings['ACCOUNT_029']			= "Wybierz";
$strings['ACCOUNT_030']			= "Kod pocztowy:";
$strings['ACCOUNT_031']			= "Informacje o autoryzacji:";
$strings['ACCOUNT_032']			= "Wybierz hasło:";
$strings['ACCOUNT_033']			= "Conajmniej 8 znaków i same liczby oraz litery";
$strings['ACCOUNT_034']			= "Powtórz hasło:";
$strings['ACCOUNT_035']			= "Wybierz przypominacz hasła:";
$strings['ACCOUNT_036']			= "Wpisz odpowiedź na przypominacz hasła:";
$strings['ACCOUNT_037']			= "Potrzebyjemy potwierdzenia, że jesteś człowiekiem:";
$strings['ACCOUNT_038']			= "Wpisz litery w puste miejsca:";
$strings['ACCOUNT_039']			= "Obraz Captcha";
$strings['ACCOUNT_040']			= "Zakładam konto";
$strings['ACCOUNT_041']			= "Wiadomość do właściciela witryny:";
$strings['ACCOUNT_042']			= "Wpisz krótką wiadomość do właściciela witryny:";

// account.created.tpl
$strings['ACCOUNT_043']			= "Konto zostało założone, masz teraz dostęp do większości opcji i możliwości strony.";
$strings['ACCOUNT_044']			= "Konto założone";
$strings['ACCOUNT_045']			= "Konto zostało założone. Aby uzyskać dostęp do ustawień, zaloguj się i wejdź w stręfę 'Moje konto'.";

// account.edit.success.tpl
$strings['ACCOUNT_046']			= "Dziękujemy za aktualizacje Twojego profilu!";
$strings['ACCOUNT_047']			= "Profil został zapisany!";
$strings['ACCOUNT_048']			= "Dziękujemy za aktualizację Twojego profilu.";
$strings['ACCOUNT_049']			= "Wróć do Twojego konta";

// account.edit.tpl
$strings['ACCOUNT_050']			= "Użyj tego formularza, aby zaktualizować swój profil.";
$strings['ACCOUNT_051']			= "Popraw błędy";
$strings['ACCOUNT_052']			= "Edytuj profil konta";
$strings['ACCOUNT_053']			= "Możesz użyć tego formularza aby wypełnić swój profil.";
$strings['ACCOUNT_054']			= "Zmień swoje hasło (opcja):";
$strings['ACCOUNT_055']			= "Powtórz swoje hasło (opcja):";
$strings['ACCOUNT_056']			= "Zapisz zmiany";

// account.reset.01.tpl, account.reset.02.tpl, account.reset.03.tpl, account.reset.04.tpl
$strings['ACCOUNT_057']			= "Aby zresetować hasło do konta, wpisz swój email w puste pole.";
$strings['ACCOUNT_058']			= "Zresetuj hasło";
$strings['ACCOUNT_059']			= "Aby zresetować hasło, musisz podać swój email oraz odpowiedzieć na pytanie z przypominacza.";
$strings['ACCOUNT_060']			= "Wpisz swój adres email";
$strings['ACCOUNT_061']			= "Adres email podany podczas rejestracji:";
$strings['ACCOUNT_062']			= "Następny krok";
$strings['ACCOUNT_063']			= "Wpisz odpowiedź na pytanie z przypominacza.";
$strings['ACCOUNT_064']			= "Weryfikacja bezpieczeństwa";
$strings['ACCOUNT_065']			= "Wpisz odpowiedź na pytanie z przypominacza hasła, wpisz ją dokładnie tak jak podczas rejestracji.";
$strings['ACCOUNT_066']			= "Nieprawidłowa odpowiedź";
$strings['ACCOUNT_067']			= "Odpowiedz na swoje pytanie z przypominacza";
$strings['ACCOUNT_068']			= "Pytanie przypominacza:";
$strings['ACCOUNT_069']			= "Wpisz odpowiedź:";
$strings['ACCOUNT_070']			= "Wpisz nowe hasło, które zastąpi zapomniane.";
$strings['ACCOUNT_071']			= "Popraw błędy";
$strings['ACCOUNT_072']			= "Jest problem z informacjami podanymi na poprzedniej stronie:";
$strings['ACCOUNT_073']			= "Wpisz nowe hasło";
$strings['ACCOUNT_074']			= "Wpisz nowe hasło i upewnij się, że powtórzono je poprawnie.";
$strings['ACCOUNT_075']			= "Wpisz nowe hasło";
$strings['ACCOUNT_076']			= "Nowe hasło:";
$strings['ACCOUNT_077']			= "Powtórz nowe hasło:";
$strings['ACCOUNT_078']			= "Hasło zostało zresetowane.";
$strings['ACCOUNT_079']			= "Hasło zostało zmienione";
$strings['ACCOUNT_080']			= "Hasło zostało zmienione i możesz się zalogować. Jeśli masz nadal z tym problem, skontaktuj się z administratorem strony.";
$strings['ACCOUNT_081']			= "Idź do strony logowania";

// index.account.php
$strings['ACCOUNT_082']			= "Strona domowa konta";
$strings['ACCOUNT_083']			= "Utwórz konto";
$strings['ACCOUNT_084']			= "Konto zostało utworzone";
$strings['ACCOUNT_085']			= "Popraw błędy";
$strings['ACCOUNT_086']			= "Edytuj profil konta";
$strings['ACCOUNT_087']			= "Zaktualizowano informacje";
$strings['ACCOUNT_088']			= "Zakończ rejestrację";
$strings['ACCOUNT_089']			= "Reset hasła";
$strings['ACCOUNT_090']			= "Weryfikacja bezpieczeństwa";
$strings['ACCOUNT_091']			= "Wpisz nowe hasło";
$strings['ACCOUNT_092']			= "Hasła nie są identyczne";
$strings['ACCOUNT_093']			= "Musisz wpisać hasło dwukrotnie";
$strings['ACCOUNT_094']			= "Hasło musi mieć conajmniej 8 znaków i być złożone z liter i cyfr.";
$strings['ACCOUNT_095']			= "Hasło zostało zresetowane";

// index.account.tpl
$strings['ACCOUNT_096']			= "Tutaj możesz przejrzeć informacje o koszyku, poprzednich zamówieniach oraz edytować informacje o koncie.";
$strings['ACCOUNT_097']			= "Strona domowa konta";
$strings['ACCOUNT_098']			= "Możesz tutaj administrować lightbox'ami, edytować swój prywatny profil jak również, zobaczyć szczegóły zamówień i koszyka.";
$strings['ACCOUNT_099']			= "Konto nie zostało zatwierdzone, nie masz dostępu do zdjęć i galerii wymagających zatwierdzenia administratora.";
$strings['ACCOUNT_100']			= "Edytuj swój profil";
$strings['ACCOUNT_101']			= "Edytuj swoje lightbox'y";
$strings['ACCOUNT_102']			= "Zarządzaj swoimi zdjęciami";
$strings['ACCOUNT_103']			= "Wgraj nowe zdjęcia";
$strings['ACCOUNT_104']			= "Status";
$strings['ACCOUNT_105']			= "Elementy";
$strings['ACCOUNT_106']			= "Data utworzenia";
$strings['ACCOUNT_107']			= "Data zgłoszenia";
$strings['ACCOUNT_108']			= "Data ukończenia";
$strings['ACCOUNT_109']			= "Akcja";
$strings['ACCOUNT_110']			= "Nie zgłoszone";
$strings['ACCOUNT_111']			= "Niekompletne";
$strings['ACCOUNT_112']			= "Zobacz";
$strings['ACCOUNT_113']			= "Zobacz szczegóły";

// invitation.error.tpl
$strings['ACCOUNT_114']			= "Wystąpił błąd i nie możemy kontynuować.";
$strings['ACCOUNT_115']			= "Uwaga";
$strings['ACCOUNT_116']			= "Link dzięki któremu tu trafiłeś(aś) jest nieaktualny.  Jeśli chcesz dalszych inforamcji skontaktuj się z<a href=\"mailto:\$set_contact_email\">właścicielem witryny</a>.";

// invitation.accept.tpl
$strings['ACCOUNT_117']			= "Zarejestruj się już dziś!";
$strings['ACCOUNT_118']			= "Witaj!";
$strings['ACCOUNT_119']			= "Wysłano Tobie zaproszenie, do pełnej rejestracji. Dzięki wypełnieniu formularza będzie można aktywować twoje konto i dołączysz do ekskluzywnego kręgu użytkowników tej witryny.";
$strings['ACCOUNT_120']			= "Zakończ rejestrację";


/****************************************************************
**
** Section title 	-	Content Management Strings
**
** File name 		-	index.cms/*
**
****************************************************************/


$strings['CMS_001']				= "Jest problem z danymi podanymi na poprzedniej stronie.";
$strings['CMS_002']				= "Komunikat błędu";
$strings['CMS_003']				= "Wróć do formularza wgrywania";

// cms.form.import.01.tpl
$strings['CMS_004']				= "Lista plików które wgrano.";
$strings['CMS_005']				= "Wgraj zdjęcia";
$strings['CMS_006']				= "Lista plików które wgrano. Przed dodaniem zdjęć do galerii upewnij się, że każde zdjęcie posiada odpowiednie wpisy w polach tytuł, nazwa, opis, data itp.";
$strings['CMS_007']				= "Tworzenie miniatury dla dużych plików może zająć więcej niż \$sys_memory_limit MB dopuszczonej pamięci dla PHP na tym serwerze.<br /><br />Możesz nie być w stanie dołączyć plików oznaczonych jako ostrzeżenie. W razie wątpliwości skontaktuj się z administratorem";
$strings['CMS_008']				= "Pamiętaj że duża grupa zdjęć, zwłaszcza wysokiej rozdzielczości może być przetwarzana przez pewien czas.\n\nCzy na pewno chcesz kontynuować?";
$strings['CMS_009']				= "Nazwa pliku";
$strings['CMS_010']				= "Rozmiar pliku";
$strings['CMS_011']				= "Ostatnio modyfikowany";
$strings['CMS_012']				= "Wymiary";
$strings['CMS_013']				= "Wymagana pamięć";
$strings['CMS_014']				= "MB"; // Megabytes
$strings['CMS_015']				= "To zdjęcie jest zbyt duże do importu.\n\nPostępuj rozważnie lub przygotuj miniaturę offline i wgraj ją przez FTP.";
$strings['CMS_016']				= "Ostrzeżenie";
$strings['CMS_017']				= "Opcje dla tych plików:";
$strings['CMS_018']				= "Określ gdzie umieścić pliki";
$strings['CMS_019']				= "Możesz określić folder w którym będą przechowywane pliki w czasie tworzenia miniatur i dodania do Twojej biblioteki.";
$strings['CMS_020']				= "Używaj tylko liter, cyfr, myślnika '-' oraz podkreślenia '_' . Jeśli nie podasz nazwy, zostanie ona dodana automatycznie.";
$strings['CMS_021']				= "Wybierz jeden z istniejących folderów";
$strings['CMS_022']				= "Wybierz wymaganą akcje:";
$strings['CMS_023']				= "Importuj wybrane pliki do biblioteki";
$strings['CMS_024']				= "Usuń wybrane pliki z serwera";
$strings['CMS_025']				= "Kontynuuj";
$strings['CMS_026']				= "Brak zdjęć w źródłowym folderze.";
$strings['CMS_027']				= "Brak zdjęc do obejrzenia w obecnym wyborze.";

// cms.form.import.02.tpl
$strings['CMS_028']				= "Zaznaczone zdjęcia zostały zmniejszone i gotowe są do dodania do głównej biblioteki.";
$strings['CMS_029']				= "Miniatury zostały utworzone";
$strings['CMS_030']				= "Zaznaczone zdjęcia zostały zmniejszone i zapisany w folderze '\$temp_name' i są gotowe do dodania do głównej biblioteki.";
$strings['CMS_031']				= "Jedno lub więcej z wybranych zdjęć nie może być otwarte.  Być może plik jest uszkodzony, posiada nieprawidłowy typ lub został uszkodzony podczas wgrywania.";
$strings['CMS_032']				= "Jeśli problem się potwarza skontaktuj się z administratorem.";
$strings['CMS_033']				= "Dodaj zaznaczone zdjęcia do biblioteki";
$strings['CMS_034']				= "Sprawdź metadane zdjęć";
$strings['CMS_035']				= "Nazwa pliku:";
$strings['CMS_036']				= "Rozmiar zdjęcia:";
$strings['CMS_037']				= "pixeli";
$strings['CMS_038']				= "Posiada zgodę modela";
$strings['CMS_039']				= "Posiada zgodę właściciela posesji";
$strings['CMS_040']				= "Tytuł zdjęcia (nagłówek):";
$strings['CMS_041']				= "Podpis zdjęcia:";
$strings['CMS_042']				= "Słowa kluczowe:";
$strings['CMS_043']				= "Data zdjęcia:";
$strings['CMS_044']				= "Data z EXIF:";
$strings['CMS_045']				= "Brak daty w EXIF";
$strings['CMS_046']				= "Data z IPTC:";
$strings['CMS_047']				= "Brak daty w IPTC";
$strings['CMS_048']				= "Typ licencji:";
$strings['CMS_049']				= "Royalty free";
$strings['CMS_050']				= "Zarządzanie licencjami";
$strings['CMS_051']				= "Informacja o licencji:";
$strings['CMS_052']				= "Info o licencji";

// cms.form.import.03.tpl
$strings['CMS_053']				= "Zdjęcie już istnieje w bibliotece.";
$strings['CMS_054']				= "Żadne zdjęcie nie zostało przesłane";
$strings['CMS_055']				= "Żadne z zaznaczonych zdjęć nie zostało dodne, gdyż wszystkie istnieją już w bibliotece.";
$strings['CMS_056']				= "Niektóre zdjęcia mają taką samą nazwe jak pliki w docelowym katalogu i nie zostały dodane.";
$strings['CMS_057']				= "Niektóre zdjęcia nie zostały dodane";
$strings['CMS_058']				= "Niektóre zdjęcia nie zostały dodane, gdyz już takie nazwy istnieją w bibliotece, oto lista tych plików:";
$strings['CMS_059']				= "Nazwy plików, które nie zostały dodane:";
$strings['CMS_060']				= "Wybrane zdjęcia zostały dodane do biblioteki.";
$strings['CMS_061']				= "Zdjęcia zostały poprawnie zaimportowane";
$strings['CMS_062']				= "Zdjęcia zostału poprawnie dodane do biblioteki.";

// cms.form.upload.tpl
$strings['CMS_063']				= "Na tej stronie możesz wgrywać zdjęcia lub spakowane archiwa zdjęć.";
$strings['CMS_064']				= "Jak wgrać zdjęcia.";
$strings['CMS_065']				= "Maksymalnie możesz za jednym razem wgrać \$sys_memory_limit megabajtów, więc sprawdź, czy nie dodajesz na raz za dużo.";
$strings['CMS_066']				= "Wgraj spakowane archiwum zip, gz lub tar";
$strings['CMS_067']				= "Wybierz archiwum do wgrania:";
$strings['CMS_068']				= "Pamiętaj, że wgrywane archiwum nie może zawierać nic poza zdjęciami (inne typy lub katalogi są zabronione).  Maksymalny rozmiar archiwum jaki możesz wgrać to \$sys_memory_limit megabajtów.";
$strings['CMS_069']				= "Wgraj i przetwórz archiwum";
$strings['CMS_070']				= "Wybierz indywidualne zdjęcia JPEG do wgrania";
$strings['CMS_071']				= "Wybierz pliki JPEG do wgrania:";
$strings['CMS_072']				= "Maksymalna ilość danych do jednorazowego wgrania nie może przekroczyć \$sys_memory_limit megabajtów.";
$strings['CMS_073']				= "Wybrałeś pliki:";
$strings['CMS_074']				= "Wybrane pliki będą tutaj pokazane";
$strings['CMS_075']				= "Wgraj i przetwórz pliki";

// cms.image.edit.01.tpl
$strings['CMS_076']				= "Użyj tego formularza aby określić szczegóły zdjęcia";
$strings['CMS_077']				= "Edytuj zdjęcie";
$strings['CMS_078']				= "Zmień zdjęcie jak chcesz, poprzez poniższy formularz.";
$strings['CMS_079']				= "To zdjęcie nie jest do podglądu i nie zobaczy go nikt przeglądający witrynę.";
$strings['CMS_080']				= "Własności zdjęcia:";
$strings['CMS_081']				= "Podgląd zdjęcia:";
$strings['CMS_082']				= "Kliknij tutaj aby zobaczyć zdjęcie w oryginalnym rozmiarze";
$strings['CMS_083']				= "Włącz szukanie koloru dla tego zdjęcia:";
$strings['CMS_084']				= "Wypadkowy kolor zdjęcia:";
$strings['CMS_085']				= "Hex"; // Hexadecimal colour
$strings['CMS_086']				= "RGB"; // RGB colour
$strings['CMS_087']				= "Wybór palety koloru:";
$strings['CMS_088']				= "Data zdjęcia (w bazie):";
$strings['CMS_089']				= "Data zdjęcia (z metadanych EXIF):";
$strings['CMS_090']				= "Data zdjęcia (z metadanych IPTC):";
$strings['CMS_091']				= "Tytuł zdjęcia:";
$strings['CMS_092']				= "Podpis zdjęcia:";
$strings['CMS_093']				= "Słowa kluczowe (oddzielone przecinkiem):";
$strings['CMS_094']				= "Szerokość oryginału (pixeli):";
$strings['CMS_095']				= "Wysokość oryginału (pixeli):";
$strings['CMS_096']				= "Właściciel praw autorskich:";
$strings['CMS_097']				= "Dodatkowe pola:";
$strings['CMS_098']				= "Zapisz nowe informacje o zdjęciu";

// cms.image.edit.02.tpl
$strings['CMS_099']				= "Zmiany zostały zapisane.";
$strings['CMS_100']				= "Zmiany zastosowano";
$strings['CMS_101']				= "Zmiany zastosowane do tego zdjęcia zostały zapisane w bilbiotece.";
$strings['CMS_102']				= "Wróć do swojego poprzedniego zajęcia";
$strings['CMS_103']				= "Wróć do strony głównej";

// cms.image.list.tpl
$strings['CMS_104']				= "Lista zdjęć posiadanych przez użytkownika \$user_name.";
$strings['CMS_105']				= "Zdjęcia posiadane przez użytkownika \$user_name";
$strings['CMS_106']				= "Aby pracować z tymi zdjęciami, wybierz elementy, które chcesz zmieniać oraz wybierz akcję z menu na dole strony.";
$strings['CMS_107']				= "Znak wodny jest włączony dla zdjęć oglądanych na stronie.";
$strings['CMS_108']				= "Oglądanie twoich zdjęć";
$strings['CMS_109']				= "Opcje miniatur:";
$strings['CMS_110']				= "Miniatur na stronie";
$strings['CMS_111']				= "Wybierz wartość większą od 0";
$strings['CMS_112']				= "Numer strony:";
$strings['CMS_113']				= "Zaznacz wszystkie";
$strings['CMS_114']				= "Aktywne";
$strings['CMS_115']				= "Nazwa pliku";
$strings['CMS_116']				= "Tytuł zdjęcia";
$strings['CMS_117']				= "Wymiary";
$strings['CMS_118']				= "Akcja";
$strings['CMS_119']				= "Zdjęcie jest dostępne do oglądania";
$strings['CMS_120']				= "Zdjęcie jest niedostępne do oglądania";
$strings['CMS_121']				= "Edytuj zdjęcie";
$strings['CMS_122']				= "Nie masz zdjęć w bibliotece.";
$strings['CMS_123']				= "Opcje:";
$strings['CMS_124']				= "Wybierz co chcesz zrobić z zaznaczonymi zdjęciami:";
$strings['CMS_125']				= "Usuń zaznaczone zdjęcia z biblioteki";
$strings['CMS_126']				= "Kontynuuj";


/****************************************************************
**
** Section title 	-	Postcards Strings
**
** File name 		-	index.postcards/*
**
****************************************************************/

// index.postcard.01.tpl
$strings['POSTCARD_01']				= "Wyślij spersonalizowaną kartkę elektroniczną opartą na tym zdjęciu.";
$strings['POSTCARD_02']				= "Wyślij kartkę";
$strings['POSTCARD_03']				= "Aby stworzyć własną kartkę, wpisz wiadomość w pole obok. Następnie podaj swój własny email i adres osoby do której chcesz wysłać tę elektroniczną kartkę.";
$strings['POSTCARD_04']				= "Wyślij e-kartkę";
$strings['POSTCARD_05']				= "Wpisz swoją wiadomość:";
$strings['POSTCARD_06']				= "Pamiętaj aby wiadomość była krótka, bo inaczej wyjdzie poza kartkę!";
$strings['POSTCARD_07']				= "Nazywasz się:";
$strings['POSTCARD_08']				= "Twój email:";
$strings['POSTCARD_09']				= "Email odbiorcy:";
$strings['POSTCARD_10']				= "Wybierz font:";
$strings['POSTCARD_11']				= "Podgląd kartki";

// index.postcard.01.tpl
$strings['POSTCARD_12']				= "Musisz podać jak się nazywasz, swój email oraz email odbiorcy aby przejść dalej!";
$strings['POSTCARD_13']				= "Jeśli jesteś zadowolony z efektu i podałeś poprawne dane, możesz wysłać kartkę.";
$strings['POSTCARD_14']				= "Sprawdź kartkę";
$strings['POSTCARD_15']				= "Sprawdź wiadomość i kliknij 'Wyślij kartkę' aby  przekazać ją do odbiorcy.";
$strings['POSTCARD_16']				= "Wyślij kartkę";
$strings['POSTCARD_17']				= "Edytuj kartkę";
$strings['POSTCARD_18']				= "Twoje wybrane zdjęcie";
$strings['POSTCARD_19']				= "Kartka została wysłana do {$recipient} i wkrótce zostanie dostarczona!";
$strings['POSTCARD_20']				= "Kartka jest w drodze";
$strings['POSTCARD_21']				= "Dziękujemy za wysłanie kartki.  Email z kartką jest w drodze do \$recipient.";
$strings['POSTCARD_22']				= "Wróć do poprzednich zajęć";


/****************************************************************
**
** Section title 	-	HTML Snippets Strings
**
** File name 		-	index.snippets/*
**
****************************************************************/

// notification.note.tpl
$strings['HTML_SNIP_01']		= "Zapamiętaj!";

// notification.warning.tpl
$strings['HTML_SNIP_02']		= "Ostrzeżenie!";

// pixaria.processing.tpl
$strings['HTML_SNIP_03']		= "Proszę czekać";
$strings['HTML_SNIP_04']		= "Przetwarzam zadanie, proszę czekać!";
$strings['HTML_SNIP_05']		= "Jeśli strona nie odświeży się po 10 sekundach, <a href=\"\$meta_url\">kliknij tutaj</a>";


/****************************************************************
**
** Section title 	-	Lightbox Strings
**
** File name 		-	index.lightbox.tpl/*
**
****************************************************************/

// index.lightbox.popup.tpl
$strings['LIGHTBOX_01']			= "Zaktualizowano Twój lightbox";
$strings['LIGHTBOX_02']			= "Zaktualizowano Lightbox";
$strings['LIGHTBOX_03']			= "Wybrane zdjęcie dodano do lightboxu.";
$strings['LIGHTBOX_04']			= "Idź do lightboxu";
$strings['LIGHTBOX_05']			= "Zamknij to okno";

// lightbox.edit.tpl
$strings['LIGHTBOX_06']			= "Użyj lightboxów aby przechowywać ulubione kolekcje zdjęć.";
$strings['LIGHTBOX_07']			= "Edytuj lightbox";
$strings['LIGHTBOX_08']			= "Zmień nazwę lightboxa lub kolejność zdjęć w nim się znajdujących.";
$strings['LIGHTBOX_09']			= "Aby usunąć zdjęcie z lightboxa, musisz przejrzeć go, wybrać zdjęcia do usunięcia i kliknąć przycisk 'usuń z lightbox'.";
$strings['LIGHTBOX_10']			= "Edytuj lightbox:";
$strings['LIGHTBOX_11']			= "Nazwa Lightboxa :";
$strings['LIGHTBOX_12']			= "Ustaw ten lightbox jako aktywny:";
$strings['LIGHTBOX_13']			= "Zapisz zmiany w tym lightbox'ie";
$strings['LIGHTBOX_14']			= "Kliknij i przesuń miniatury:";
$strings['LIGHTBOX_15']			= "Dwuklik aby obejrzeć wybrane zdjęcie.";
$strings['LIGHTBOX_16']			= "Tytuł:";
$strings['LIGHTBOX_17']			= "Data:";
$strings['LIGHTBOX_18']			= "Nazwa pliku:";

// lightbox.error.nullbox.tpl
$strings['LIGHTBOX_19']			= "Nie mogę wyświetlić lightbox.";
$strings['LIGHTBOX_20']			= "Lightbox nie wybrany";
$strings['LIGHTBOX_21']			= "Nie wybrano lightboxu do oglądania, <a href=\"\$base_url\$fil_index_login\">zaloguj się</a> jeśli chcesz zobaczyć własny lightbox.";
$strings['LIGHTBOX_22']			= "";

// lightbox.error.tpl
$strings['LIGHTBOX_23']			= "Nie masz uprawnień do przeglądania tego lightboxu.";
$strings['LIGHTBOX_24']			= "Lightbox niedostępny";
$strings['LIGHTBOX_25']			= "Nie masz uprawnień do przeglądania tego lightbox. Jeśli chcesz przejrzeć lightbox, którego link ci ktoś przesłał, sprawdź, czy masz poprawny link.";
$strings['LIGHTBOX_26']			= "Jeśli chcesz wejść do swojego lightbox, że musisz się wcześniej <a href=\"\$base_url\$fil_index_login\">zalogować</a>.";

// lightbox.list.tpl
$strings['LIGHTBOX_27']			= "Jako zarejestrowany użytkownik możesz użyć lightbox do przechowywania swoich ulubionych zdjęć, aby łatwiej podjąć kolejne akcje z nimi związane.";
$strings['LIGHTBOX_28']			= "Zarządzanie Lightbox'em";
$strings['LIGHTBOX_29']			= "Lightbox służy do wirtualnego przechowywania wybranych zdjęć, które można dowolnie dodawać i usuwać. Możesz operować nimi na stronie bez żadnych kosztów ani konsekwencji.";
$strings['LIGHTBOX_30']			= "Możesz mieć jednocześnie do 10 lightbox'ów.";
$strings['LIGHTBOX_31']			= "Nie masz aktywnego lightbox'a, aktywuj jeden z dostępnych poniżej.";
$strings['LIGHTBOX_32']			= "Utwórz nowy lightbox:";
$strings['LIGHTBOX_33']			= "Nazwa lightbox:";
$strings['LIGHTBOX_34']			= "Ustaw go jako aktywny lightbox:";
$strings['LIGHTBOX_35']			= "Zaznacz to pole, aby uczynić ten lightbox domyślnym";
$strings['LIGHTBOX_36']			= "Zapisz lightbox";
$strings['LIGHTBOX_37']			= "Nazwa lightbox";
$strings['LIGHTBOX_38']			= "Zdjęcia w tym lightbox'ie";
$strings['LIGHTBOX_39']			= "Utworzony";
$strings['LIGHTBOX_40']			= "Akcja";
$strings['LIGHTBOX_41']			= "Zobacz";
$strings['LIGHTBOX_42']			= "Podsumowanie w PDF";
$strings['LIGHTBOX_43']			= "Edytuj";
$strings['LIGHTBOX_44']			= "Podziel się";
$strings['LIGHTBOX_45']			= "Domyślny";
$strings['LIGHTBOX_46']			= "Usuń";
$strings['LIGHTBOX_47']			= "Nie masz lightbox'ów do przeglądania.";

// lightbox.share.form.tpl
$strings['LIGHTBOX_48']			= "Jeśli chcesz komuś pokazać swój lightbox, podaj nam jego(jej) dane kontaktowe.";
$strings['LIGHTBOX_49']			= "Popraw błędy";
$strings['LIGHTBOX_50']			= "Jest problem z danymi jakie wcześniej podałeś, przejrzyj poniższe komunikaty:";
$strings['LIGHTBOX_51']			= "Podziel się lightbox'em";
$strings['LIGHTBOX_52']			= "Wypełnij poniższy formularz a email z zaproszeniem zostanie wysłany w Twoim imieniu na adres email podany w formularzu.";
$strings['LIGHTBOX_53']			= "Podziel się tym lightbox'em:";
$strings['LIGHTBOX_54']			= "Nazwa odbiorcy:";
$strings['LIGHTBOX_55']			= "Wymagane";
$strings['LIGHTBOX_56']			= "Adres email odbiorcy:";
$strings['LIGHTBOX_57']			= "Temat:";
$strings['LIGHTBOX_58']			= "Wiadomość:";
$strings['LIGHTBOX_59']			= "Cześć,\n\nZobacz jakie mam fajne zdjęcia w swojej kolekcji:";
$strings['LIGHTBOX_60']			= "Wyślij wiadomość";

// lightbox.share.sent.tpl
$strings['LIGHTBOX_61']			= "Wiadomość została wysłana.";
$strings['LIGHTBOX_62']			= "Wiadomość została wysłana!";
$strings['LIGHTBOX_63']			= "Link do Twojego lightboxa został przesłany do \$name.";
$strings['LIGHTBOX_64']			= "Wróć do swojego lightboxa";

// lightbox.view.image.tpl
$strings['LIGHTBOX_65']			= "Lightbox który przeglądasz jest pusty, lub zdjęcia nie zostały dopuszczone do przeglądania przez administratora witryny.";
$strings['LIGHTBOX_66']			= "Zdjęcia nie mogą być wyświetlone";
$strings['LIGHTBOX_67']			= "Niestety nie możesz nic zobaczyć, gdyż ten lightbox nie zawiera zdjęć lub zdjęcia nie zostały dopuszczone do publicznego przeglądania.";
$strings['LIGHTBOX_68']			= "Ten lightbox nie zawiera żadnych zdjęć.";
$strings['LIGHTBOX_69']			= "Ten lighbox jest pusty";
$strings['LIGHTBOX_70']			= "Aby dodąć zdjęcie do swojego lightbox'a kliknij 'dodaj do lightbox' podczas przeglądania zdjęcia.";
$strings['LIGHTBOX_71']			= "Nie można przeglądać obcych lightbox'ów bez odpowiedniego linku lub uprawnień.";
$strings['LIGHTBOX_72']			= "Zarządzaj swoimi Lightbox'ami";
$strings['LIGHTBOX_73']			= "Zobacz szczegóły i zarządzaj swoimi lightbox'ami";
$strings['LIGHTBOX_74']			= "Twóje lightbox'y zostały zaktualizowane.";
$strings['LIGHTBOX_75']			= "Lightbox zaktualizowany";
$strings['LIGHTBOX_76']			= "Możesz przeglądać lightbox lub wrócić na stronę z której wszedłeś do tutaj.";
$strings['LIGHTBOX_77']			= "Wróć na poprzednią stronę";
$strings['LIGHTBOX_78']			= "Wróć";


/****************************************************************
**
** Section title 	-	News Strings
**
** File name 		-	index.news.item.tpl
**
****************************************************************/

$strings['NEWS_01']				= "Przeglądaj galerię";
$strings['NEWS_02']				= "Wróć do newsów";


/****************************************************************
**
** Section title 	-	Store and Cart Strings
**
** File name 		-	index.store/*
**
****************************************************************/

// cart.empty.tpl
$strings['STORE_001']			= "Koszyk jest pusty.";
$strings['STORE_002']			= "Koszyk jest pusty";
$strings['STORE_003']			= "W chwili obecnej nic nie znajduje się w Twoim koszyku. Zdjęcia na sprzedaż są oznaczone ikoną, dzięki której możesz dodać zdjęcia do koszyka.";
$strings['STORE_004']			= "Wróć na stronę główną";

// cart.processed.advanced.tpl,  cart.processed.basic.tpl,  cart.processed.intermediate.tpl
$strings['STORE_005']			= "Koszyk został zrealizowany.";
$strings['STORE_006']			= "Potwierdzenie";
$strings['STORE_007']			= "Twoje zamówienie zostanie wkrótce zrealizowane. Nie możesz już edytować zamówienia, lecz możesz śledzić jego postęp w swoim koncie.";
$strings['STORE_008']			= "Wróć na stronę główną konta";

// cart.processing.duplicate.tpl, cart.processing.error.tpl
$strings['STORE_009']			= "To zamówienie nie zostało przetworzone.";
$strings['STORE_010']			= "Te zamówienia nie zostały przetworzone";
$strings['STORE_011']			= "Już zapłaciłeś za zawartość koszyka. Jest wiele powodów, dla których mogło się to stać:";
$strings['STORE_012']			= "Dwukrotnie kliknąłeś szybko w link do tej strony.";
$strings['STORE_013']			= "Odświeżyłeś stronę.";
$strings['STORE_014']			= "Jeśli uważasz że to jest błąd, skontaktuj się z administratorem witryny i przekaż mu te wiadomości:";
$strings['STORE_015']			= "Czas transakcji";
$strings['STORE_016']			= "Metoda płatności";
$strings['STORE_017']			= "Unikalny numer transakcji";
$strings['STORE_018']			= "Numer koszyka";
$strings['STORE_019']			= "Wróć na stronę konta";

// cart.processing.error.tpl
$strings['STORE_020']			= "Zamówienie nie zostało przetworzone.";
$strings['STORE_021']			= "Zamówienia nie zostały przetworzone";
$strings['STORE_022']			= "Jeśli uważasz że to jest błąd, skontaktuj się z administratorem witryny i przekaż mu te wiadomości:";

// cart.processing.partial.tpl
$strings['STORE_023']			= "Zamówienie przetworzone częściowo";
$strings['STORE_024']			= "Zamówienie przetworzone częściowo";
$strings['STORE_025']			= "Zamówienie zostało skompletowane, lecz nie mamy potwierdzenia dokonania płatności. Koszyk został oznaczony jako 'oczekiwanie na płatność' i może być przeglądany tutaj:";
$strings['STORE_026']			= "Zobacz szczegóły zamówienia";

// cart.processing.success.tpl
$strings['STORE_027']			= "Zamówienie zostało zrealizowane.";
$strings['STORE_028']			= "Twoje zamówienie zostało zrealizowane";
$strings['STORE_029']			= "Zamówienie zostało zrealizowane, tutaj są szczegóły:";
$strings['STORE_030']			= "Zobacz szczegóły zamówienia";

// cart.view.advanced.tpl, cart.view.basic.tpl, cart.view.intermediate.tpl
$strings['STORE_031']			= "Ukończ zamówienie dokonując płatności za elementy w koszyku.";
$strings['STORE_032']			= "Informacje kontaktowe";
$strings['STORE_033']			= "Wróć do poprzedniej strony";
$strings['STORE_034']			= "Zobacz swój koszyk.";
$strings['STORE_035']			= "Kontynuuj zakupy";
$strings['STORE_036']			= "Zawartość koszyka";
$strings['STORE_037']			= "Nazwa pliku:";
$strings['STORE_038']			= "Tytuł:";
$strings['STORE_039']			= "Cena:";
$strings['STORE_040']			= "Ilość:";
$strings['STORE_041']			= "Wysyłka:";
$strings['STORE_042']			= "Szczegóły faktury";
$strings['STORE_043']			= "nie dostępne"; // Not applicable
$strings['STORE_044']			= "Pierwszy element";
$strings['STORE_045']			= "Kolejne elementy";
$strings['STORE_046']			= "Twoje wymagania:";
$strings['STORE_047']			= "To zdjęcie zostało usunięte z biblioteki i nie jest w tej chwili dostępne.<br />Musisz usunąć je z koszuka, przed kontynuowaniem.";
$strings['STORE_048']			= "Akcje:";
$strings['STORE_049']			= "Przelicz cenę";
$strings['STORE_050']			= "Przelicz cenę dla tego zdjęcia";
$strings['STORE_051']			= "Usuń z koszuka";
$strings['STORE_052']			= "Usuń to z koszyka";
$strings['STORE_053']			= "Opcje";
$strings['STORE_054']			= "Zaktualizuj ilość";
$strings['STORE_055']			= "Opróżnij koszyk";
$strings['STORE_056']			= "Informacje o fakturze";
$strings['STORE_057']			= "Podsumowanie:";
$strings['STORE_058']			= "Podatek:";
$strings['STORE_059']			= "przy \$set_store_default_tax%";
$strings['STORE_060']			= "Koszt wysyłki:";
$strings['STORE_061']			= "Suma:";
$strings['STORE_062']			= "Po tym kroku nie ma możliwości edycji koszyka. Czy na pewno kontynuować?";
$strings['STORE_063']			= "Quote information";
$strings['STORE_064']			= "Prośba o ofertę";
$strings['STORE_065']			= "Jeśli wyceniłeś zdjęcia w koszyku, nie będziesz mógł nic zmienić jeśli poprosisz o ofertę na pozostałe zdjęcia.";
$strings['STORE_066']			= "Wyslij zapytanie o ofertę";
$strings['STORE_067']			= "Nie uzupełniłeś wymaganych danych w koszyku";
$strings['STORE_068']			= "Ukończ proces zamówienia";
$strings['STORE_069']			= "Przed kontynuacją musisz przeliczyć wartość zdjęć w koszyku.";
$strings['STORE_070']			= "Kontynuuj";
$strings['STORE_071']			= "Informacje kontaktowe";

// cart.view.basic.tpl, cart.view.intermediate.tpl
$strings['STORE_072']			= "Inforamcje o zamówieniu:";
$strings['STORE_073']			= "Wprowadź informacje o zamierzonym użyciu zdjęć:";
$strings['STORE_074']			= "Ostrzeżenie:";
$strings['STORE_075']			= "To zdjęcie zostało usunięte z biblioteki i nie jest w tej chwili dostępne.<br />Musisz usunąć je z koszuka, przed kontynuowaniem.";
$strings['STORE_076']			= "Szczegóły:";
$strings['STORE_077']			= "Brak ofert";
$strings['STORE_078']			= "Zapisz zmiany w koszyku:";
$strings['STORE_079']			= "Wybierz akcje:";
$strings['STORE_080']			= "Pobierz ofertę";
$strings['STORE_081']			= "Kontynuuj";

// image.added.calc.popup.tpl, image.added.quote.popup.tpl
$strings['STORE_082']			= "Koszyk został zaktualizowany nowymi cenami i wymaganiami zdjęć.";
$strings['STORE_083']			= "Koszyk został zaktualizowany!";
$strings['STORE_084']			= "Koszyk został zaktualizowany wymaganiami dotyczącymi zdjęć.";
$strings['STORE_085']			= "Idź do koszyka";
$strings['STORE_086']			= "Zamknij to okno";

// payment.methods.tpl
$strings['STORE_087']			= "Warunki i zasady";
$strings['STORE_088']			= "Zgadzam się z warunkami sprzedaży";
$strings['STORE_089']			= "Przejdź do płatności";
$strings['STORE_090']			= "Metody płatności offline";
$strings['STORE_091']			= "Zapłać offline";
$strings['STORE_092']			= "Nie będziesz mógł edytować koszyka, gdy wykonasz kolejny krok. Na pewno kontynuować?";
$strings['STORE_093']			= "Zapłacę czekiem";
$strings['STORE_094']			= "Zapłacę przekazem pocztowym";
$strings['STORE_095']			= "Zapłacę przekazem gotówkowym";
$strings['STORE_096']			= "Zapłacę przelewem";
$strings['STORE_097']			= "Metody płatności online";
$strings['STORE_098']			= "Przez Paypal";
$strings['STORE_099']			= "Kartą kredytową przez nasz sklep w 2Checkout";
$strings['STORE_100']			= "Kartą kredytową przez 2Checkout";

// price.calculate.inline.tpl, price.calculate.popup.tpl, price.recalculate.inline.tpl, price.recalculate.popup.tpl
$strings['STORE_101']			= "Twoja cena za to zdjęcie";
$strings['STORE_102']			= "Twoja cena";
$strings['STORE_103']			= "Zaoferowana przez ciebie cena za użycie tego zdjęcia to \$set_store_symbol\$new_price.  Aby dodać zdjęcie do koszyka, musisz być zarejestrowany i zalogowany.";
$strings['STORE_104']			= "Twoja cena za to zdjęcie";
$strings['STORE_105']			= "Wybrane przez ciebie opcje:";
$strings['STORE_106']			= "Wyliczona cena to (\$set_store_currency):";
$strings['STORE_107']			= "Wybierz opcje wyceny";
$strings['STORE_108']			= "Użyj mojej wyceny";
$strings['STORE_109']			= "Przekaż mi ofertę na podstawie moich wymagań";
$strings['STORE_110']			= "Określ wymagania odnośnie tego zdjęcia";
$strings['STORE_111']			= "Zapisz tę ofertę do koszyka";

// product.added.popup.tpl
$strings['STORE_112']			= "Koszyk został zaktualizowany";
$strings['STORE_113']			= "Koszyk zaktualizowany";
$strings['STORE_114']			= "Wybrane produkty zostały dodane do koszyka.";
$strings['STORE_115']			= "Idź do koszyka";
$strings['STORE_116']			= "Zamknij to okno";

// purchase.options.inline.tpl, purchase.options.popup.tpl, recalculate.options.inline.tpl, recalculate.options.popup.tpl
$strings['STORE_117']			= "Opcje zamówienia tego zdjęcia";
$strings['STORE_118']			= "Opcje zamówienia";
$strings['STORE_119']			= "Poniżej pokazano opcje zamówienia tego zdjęcia. Aby zamówić to zdjęcie musisz być zarejestrowany i zalogowany.";
$strings['STORE_120']			= "Opcje produktu dla tego zdjęcia";
$strings['STORE_121']			= "Ilość";
$strings['STORE_122']			= "Dodaj produkt do koszyka";
$strings['STORE_123']			= "Opcje cyfrowego zdjęcia";
$strings['STORE_124']			= "Instrukcje";
$strings['STORE_125']			= "Opcje wyceny dostępne tylko dla zalogowanych użytkowników.  Aby obejrzeć opcje wyceny cyfrowego zdjęcia zaloguj się.";
$strings['STORE_126']			= "Przelicz ceny dla cyfrowego zdjęcia";
$strings['STORE_127']			= "Przelicz ceny";
$strings['STORE_128']			= "Pobierz ofertę na użycie tego zdjęcia";
$strings['STORE_129']			= "Określ wymagania do tego zdjęcia";
$strings['STORE_130']			= "Zapisz zapytanie o ofertę";
$strings['STORE_131']			= "Dodaj to zdjęcie do koszyka";
$strings['STORE_132']			= "Instrukcje";
$strings['STORE_133']			= "Aby zakupić cyfrową wersję tego zdjęcia, musisz dodać je najpierw do koszyka. Gdy będzie już dodane, będziesz mógł określić jakie przewidujesz zastosowanie dla zdjęć w koszyku i wysłać zapytanie o ofertę.";
$strings['STORE_134']			= "Dodaj to zdjęcie do koszyka";

// transaction.downloads.exceeded.tpl, index.store.php
$strings['STORE_135']			= "To zdjęcie nie może być pobrane";
$strings['STORE_136']			= "Uwaga";
$strings['STORE_137']			= "To zdjęcie nie może być pobrane:";
$strings['STORE_138']			= "Wróć na poprzednią stronę";
$strings['STORE_139']			= "Zawartość koszyka nie została jeszcze opłacona.";
$strings['STORE_140']			= "WYbrane zdjęcie zostało już pobrane maksymalną ilość razy.";
$strings['STORE_141']			= "Ten element nie jest do pobrania cyfrowego.";

// 
$strings['STORE_142']			= "Zobacz swoje notowania cen";
$strings['STORE_143']			= "Twoje osobiste notowania";
$strings['STORE_144']			= "To jest twoja osobista wycena zdjęć w koszyku.";
$strings['STORE_145']			= "Wyślij wiadomość do \$site_name";
$strings['STORE_146']			= "Dodaj wiadomość do tej wyceny:";
$strings['STORE_147']			= "Dodaj wiadomość do tej wyceny";
$strings['STORE_148']			= "Wiadomości i notki w koszyku";
$strings['STORE_149']			= "Data:";
$strings['STORE_150']			= "Napisana przez:";
$strings['STORE_151']			= "Wiadomość:";
$strings['STORE_152']			= "Informacja o zamówieniu";
$strings['STORE_153']			= "Szczegóły twojego zamówienia wyceny";
$strings['STORE_154']			= "Zawartość koszyka";
$strings['STORE_155']			= "Nazwa pliku:";
$strings['STORE_156']			= "Tutuł:";
$strings['STORE_157']			= "Oferowana cena:";
$strings['STORE_158']			= "Wysyłka:";
$strings['STORE_159']			= "Uwaga:";
$strings['STORE_160']			= "nie dotyczy"; // Not applicable
$strings['STORE_161']			= "To zdjęcie zostało usunięte z biblioteki i nie jest już dostępne. Usuń je z koszyka aby dalej kontynuować";
$strings['STORE_162']			= "Ilość:";
$strings['STORE_163']			= "Pobierz plik zdjęcia:";
$strings['STORE_164']			= "Pobierze:";
$strings['STORE_165']			= "próba"; // e.g 1 attempt remaining
$strings['STORE_166']			= "prób(y)"; // e.g. 3 attempts remaining
$strings['STORE_167']			= "Szczegóły faktury";
$strings['STORE_168']			= "Podsumowanie:";
$strings['STORE_169']			= "Podatek:";
$strings['STORE_170']			= "Koszt wysyłki:";
$strings['STORE_171']			= "Suma:";
$strings['STORE_172']			= "Metoda płatności:";
$strings['STORE_173']			= "Nie dostępne dla tej transakcji";
$strings['STORE_174']			= "Czek";
$strings['STORE_175']			= "PayPal";
$strings['STORE_176']			= "Przekaz pocztowy";
$strings['STORE_177']			= "Przekaz pieniężny";
$strings['STORE_178']			= "Przelew";
$strings['STORE_179']			= "2Checkout";
$strings['STORE_180']			= "Instrukcje płatności:";
$strings['STORE_181']			= "Metody płatności offline";
$strings['STORE_182']			= "Zapłać ofline:";
$strings['STORE_183']			= "Nie będzie można edytować koszyka po wysłaniu zamówienia. Czy na pewno kontynuować";
$strings['STORE_184']			= "Zapłacę czekiem";
$strings['STORE_185']			= "Zapłacę przekazem pocztowym";
$strings['STORE_186']			= "Zapłacę przekazem pieniężnym";
$strings['STORE_187']			= "Zapłacę przelewem";
$strings['STORE_188']			= "Zmień preferowaną metodę płatności";
$strings['STORE_189']			= "Płatności online";
$strings['STORE_190']			= "Zapłać przez PayPal:";
$strings['STORE_191']			= "Zapłać przez PayPal";


/****************************************************************
**
** Section title 	-	RSS Strings
**
** File name 		-	rss.v2.0.tpl
**
****************************************************************/

$strings['RSS_01']				= "Copyright &copy;";


/****************************************************************
**
** Section title 	-	Gallery Strings
**
** File name 		-	index.gallery/*
**
****************************************************************/

// gallery.password.tpl
$strings['GALLERY_01']				= "Ta galeria jest chroniona hasłem.";
$strings['GALLERY_02']				= "Galeria prywatna";
$strings['GALLERY_03']				= "Galeria jest chroniona hasłem, wpisz hasło aby przejrzeć zawartość galerii.";
$strings['GALLERY_04']				= "Hasło będzie pamiętane, aż nie wejdziesz do galerii z innym hasłem.";
$strings['GALLERY_05']				= "Wpisano nieprawidłowe hasło.";
$strings['GALLERY_06']				= "Wpisz hasło do tej galerii:";
$strings['GALLERY_07']				= "Hasło:";
$strings['GALLERY_08']				= "Zobacz galerie";

// gallery.thumbnails.tpl
$strings['GALLERY_09']				= "Przeglądaj galerie:";
$strings['GALLERY_10']				= "Opcje miniatur";
$strings['GALLERY_11']				= "Duże";
$strings['GALLERY_12']				= "Małe";
$strings['GALLERY_13']				= "Wartość musi być większa od 0";
$strings['GALLERY_14']				= "Galerii na stronie";
$strings['GALLERY_15']				= "Numer strony";
$strings['GALLERY_16']				= "Tytuł:";
$strings['GALLERY_17']				= "Data:";
$strings['GALLERY_18']				= "Zawiera:";
$strings['GALLERY_19']				= "zdjęć";

// gallery.view.tpl
$strings['GALLERY_20']				= "Start";
$strings['GALLERY_21']				= "Galerie";


/****************************************************************
**
** Section title 	-	Global Layout Strings
**
** File name 		-	index.layout/*
**
****************************************************************/

// main.footer.tpl
$strings['LAYOUT_01']			= "Copyright &copy;";
$strings['LAYOUT_02']			= "Wszelkie prawa zastrzeżone.";

// main.header.tpl
$strings['NAVIGATION_01']		= "Rejestracja";
$strings['NAVIGATION_02']		= "Zapisz się";
$strings['NAVIGATION_03']		= "Start";
$strings['NAVIGATION_04']		= "News";
$strings['NAVIGATION_05']		= "Szukaj";
$strings['NAVIGATION_06']		= "Konto";
$strings['NAVIGATION_07']		= "Lightbox";
$strings['NAVIGATION_08']		= "Koszyk";
$strings['NAVIGATION_09']		= "Kontakt";
$strings['NAVIGATION_10']		= "Admin";
$strings['NAVIGATION_11']		= "Wypisz się";


/****************************************************************
**
** Section title 	-	Profile Validation Strings
**
** File name 		-	class.PixariaUser.php, pixaria.functions.php
**
****************************************************************/

$strings['PROFILE_VALID_01']			= "Trzeba wpisać imię.";
$strings['PROFILE_VALID_02']			= "Trzeba wpisać nazwisko.";
$strings['PROFILE_VALID_03']			= "Trzeba wpisać adres email.";
$strings['PROFILE_VALID_04']			= "Trzeba wpisać prawidłowy adres email.";
$strings['PROFILE_VALID_05']			= "Ten adres jest już używany w naszej bazie.  Jeśli już rejestrowałeś się z tym adresem (%1\$s), <a href=\"%2\$s\">zaloguj się</a> aby ukończyć poprzedni proces rejestracji.  Ewentualnie możesz użyć innego adresu email i dokonać ponownej rejestracji.";
$strings['PROFILE_VALID_06']			= "Trzeba wpisać hasło.";
$strings['PROFILE_VALID_07']			= "Powtórz uważnie hasło, abyś je dobrze zapamiętał.";
$strings['PROFILE_VALID_08']			= "Hasła nie są identyczne.";
$strings['PROFILE_VALID_09']			= "Hasło musi mieć conajmniej 8 znaków, złożone tylko z liczb i liter.";
$strings['PROFILE_VALID_10']			= "Trzeba wpisać pytanie przypominacza hasła.";
$strings['PROFILE_VALID_11']			= "Trzeba wpisać odpowiedź na pytanie przypominacza hasła.";

$strings['PROFILE_VALID_12']			= "Trzeba wpisać nr telefonu stacjonarnego.";
$strings['PROFILE_VALID_13']			= "Trzeba wpisać nr telefonu komórkowego.";
$strings['PROFILE_VALID_14']			= "Trzeba wpisać nr faksu.";
$strings['PROFILE_VALID_15']			= "Trzeba wpisać nazwę firmy.";
$strings['PROFILE_VALID_16']			= "Trzeba określić zakres działalności firmy.";
$strings['PROFILE_VALID_17']			= "Trzeba określić stanowisko w firmie.";
$strings['PROFILE_VALID_18']			= "Trzeba określić powód rejestracji.";
$strings['PROFILE_VALID_19']			= "Trzeba określić częstotliwość publikacji.";
$strings['PROFILE_VALID_20']			= "Trzeba określić nakład publikacji.";
$strings['PROFILE_VALID_21']			= "Trzeba określić terytorium publikacji.";
$strings['PROFILE_VALID_22']			= "Trzeba określić adres Twojej strony www.";
$strings['PROFILE_VALID_23']			= "Trzeba podać adres.";
$strings['PROFILE_VALID_24']			= "Trzeba podać nazwę miejscowości.";
$strings['PROFILE_VALID_25']			= "Trzeba podać kod pocztowy.";
$strings['PROFILE_VALID_26']			= "Trzeba wpisać wiadomość dla właściciela strony.";


/****************************************************************
**
** Section title 	-	Search Functions
**
** File name 		-	Multiple files
**
****************************************************************/

$strings['COLOR_HUD_01']		= "Zamknij paletę";
$strings['COLOR_HUD_02']		= "Aby odszukać zdjęcia poprzez dominujący kolor, należy wybrać kolor z palety";
$strings['COLOR_HUD_03']		= "Wybór koloru z palety";
$strings['COLOR_HUD_04']		= "Hex";
$strings['COLOR_HUD_05']		= "RGB";
$strings['COLOR_HUD_06']		= "Wybór koloru z palety";

$strings['SEARCH_PAGE_01']		= "Nic nie znaleziono.";
$strings['SEARCH_PAGE_02']		= "Spróbuj szukać czegoś innego";
$strings['SEARCH_PAGE_03']		= "Ten formularz umoźliwia wyszukiwanie poprzez podanie słów kluczowych, ograniczenie daty publikacji oraz galerii.";
$strings['SEARCH_PAGE_04']		= "Możesz wykonać złożone wyszukiwanie używająć 'AND', 'OR' oraz 'NOT' pomiędzy szukanymi frazami. Aby wyszukać konkretną frazę, obejmij ją apostrofami, np: 'Wieża Eifla'.";
$strings['SEARCH_PAGE_05']		= "Użyj tego formularza, aby szukać wg słów kluczowych, daty oraz galerii.";
$strings['SEARCH_PAGE_06']		= "Szukaj zdjęć";

$strings['SEARCH_RES_01']		= "Nie ma wyników wyszukiwania wśród poprzednich poszukiwań";
$strings['SEARCH_RES_02']		= "Nic nowego nie pasuje";
$strings['SEARCH_RES_03']		= "Wyniki poprzednich poszukiwań widoczne są poniżej. Możesz poszukać jeszcze raz, zawężając zakres poszukiwań.";
$strings['SEARCH_RES_04']		= "Znaleziono jedno zdjęcie";
$strings['SEARCH_RES_05']		= "Zmień poszukiwania";
$strings['SEARCH_RES_06']		= "Poniżej są wyniki poszukiwania wg warunków określonych wczesniej. Możesz zmienić zakres poszukiwań i spróbować podać inne parametry poszukiwania.";
$strings['SEARCH_RES_07']		= "Włącz zaawansowane poszukiwanie";
$strings['SEARCH_RES_08']		= "Znaleziono \$results_total zdjęć";
$strings['SEARCH_RES_09']		= "Znaleziono więcej niż \$set_search_max_limit wyników";
$strings['SEARCH_RES_10']		= "Tylko pierwsze \$set_search_max_limit wyników jest pokazanych. Możesz przeszukać wynik wyszukiwań aby znaleźć to co cię najbardziej interesuje.";
$strings['SEARCH_RES_11']		= "Zmień warunki poszukiwań";
$strings['SEARCH_RES_12']		= "Wyniki poszukiwań oraz ich warunki są pokazane poniżej. Możesz zmienic warunki aby jeszcze dokładniej znaleźć to czego szukasz.";


/****************************************************************
**
** Section title 	-	PayPal E-mail Notifications
**
** File name 		-	index.paypal.php
**
****************************************************************/

$strings['PAYPAL_01']		= "Firma";
$strings['PAYPAL_02']		= "Klient";
$strings['PAYPAL_03']		= "Email";
$strings['PAYPAL_04']		= "Transakcja dla";
$strings['PAYPAL_05']		= "ID użytkownika";
$strings['PAYPAL_06']		= "Status płatności";
$strings['PAYPAL_07']		= "Powód oczekiwania";
$strings['PAYPAL_08']		= "Data płatności";


/****************************************************************
**
** Section title 	-	Login form
**
** File name 		-	index.login.php
**
****************************************************************/

$strings['LOGIN_001']		= "Zaloguj się aby kontynuować";
$strings['LOGIN_002']		= "Zaloguj się, aby w pełni wykorzystać możliwości tej strony.";
$strings['LOGIN_003']		= "Zarejestruj się teraz";
$strings['LOGIN_004']		= "Zarejestruj się aby używać własnego konta, dodatkowych możliwości witryny. Rejestracja trwa nie dłużej niż minutę.";
$strings['LOGIN_005']		= "Zaloguj się do swojego konta";
$strings['LOGIN_006']		= "Podaj login i hasło aby uzyskać dostęp do zastrzeżonych rejonów witryny.";
$strings['LOGIN_007']		= "Jeśli zapomniano hasła, można je <a href=\"\$base_url\$fil_index_account?cmd=resetPasswordStart\">tutaj zresetować</a>.";
$strings['LOGIN_008']		= "Zaloguj się";
$strings['LOGIN_009']		= "Adres email";
$strings['LOGIN_010']		= "Hasło";
$strings['LOGIN_011']		= "Zapamiętaj mnie na tym komputerze";
$strings['LOGIN_012']		= "Hasło";
$strings['LOGIN_013']		= "Zaloguj się teraz";
$strings['LOGIN_014']		= "Zaloguj mnie";

$strings['LOGIN_ERROR_01'] 	= "Podane informacje nie sa prawidłowe.";
$strings['LOGIN_ERROR_02'] 	= "Logowanie nie powiodło się, upewnij się, ze używasz danych podanych podczas rejestracji.";
$strings['LOGIN_ERROR_03'] 	= "Konto nie jest jeszcze aktywne.";


/****************************************************************
**
** Section title 	-	Contact form
**
** File name 		-	index.contact.php
**
****************************************************************/

$strings['CTCT_FRM_TITLE_01']			= "Wyślij do nas wiadomość";
$strings['CTCT_FRM_TITLE_02']			= "Wiadomość wysłana";

$strings['CTCT_FRM_01'] 				= "Jeśli chcesz się skontaktować z administratorem witryny, wypełnij formularz.";
$strings['CTCT_FRM_02'] 				= "Informacje kontaktowe";
$strings['CTCT_FRM_03'] 				= "Uzyj tego formularza jeśli chcesz się skontaktować z administratorem tej witryny.";
$strings['CTCT_FRM_04'] 				= "Jak się nazywasz";
$strings['CTCT_FRM_05'] 				= "wymagane";
$strings['CTCT_FRM_06'] 				= "Adres email";
$strings['CTCT_FRM_07'] 				= "Kontaktowy numer telefonu";
$strings['CTCT_FRM_08'] 				= "Twoja wiadomość";
$strings['CTCT_FRM_09'] 				= "Wyślij wiadomość";
$strings['CTCT_FRM_10'] 				= "Popraw błędy";
$strings['CTCT_FRM_11'] 				= "Jest problem z podanymi informacjami, poświęć chwilę aby przejrzeć te komunikaty:";
$strings['CTCT_FRM_13'] 				= "Formularz kontaktowy";
$strings['CTCT_FRM_14'] 				= "Wiadomość przekazana do administratora witryny";
$strings['CTCT_FRM_15'] 				= "Wiadomość wysłana!";
$strings['CTCT_FRM_16'] 				= "Wiadomość przekazana do administratora.";

$strings['CTCT_FRM_ERROR_01']			= "Nieprawidłowy adres email.";
$strings['CTCT_FRM_ERROR_02']			= "Musisz podać jak się nazywasz.";
$strings['CTCT_FRM_ERROR_03']			= "Musisz wpisać wiadomość.";
$strings['CTCT_FRM_ERROR_04']			= "Nieprawidłowa odpowiedź.";

$strings['CTCT_FRM_RESPONSE_01']		= "Nazwa nadawcy";
$strings['CTCT_FRM_RESPONSE_02']		= "Email nadawcy";
$strings['CTCT_FRM_RESPONSE_03']		= "Telefon nadawcy";
$strings['CTCT_FRM_RESPONSE_04']		= "Wiadomość";
$strings['CTCT_FRM_RESPONSE_SUBJECT']	= "Wiadomość do przekazania";

$strings['CTCT_FRM_CHECK_01']			= "Musimy wiedzieć, czy jesteś prawdziwą osobą";
$strings['CTCT_FRM_CHECK_02']			= "Wpisz litery w puste pole";
$strings['CTCT_FRM_CHECK_03']			= "Wprowadzony błedny kod weryfikacji, popraw to.";

?>