<?php 

/****************************************************************
**
**	Pixaria Localisation Data
**	Copyright Jamie Longstaff
**
**	Translation by:		Jamie Longstaff (Original File)
**
**	Script author:		Jamie Longstaff
**	Script version:		2.x
**
****************************************************************/

/****************************************************************
**
**	THIS FILE MUST BE SAVED AS "UTF-8 NO BOM" (BYTE ORDER MARK)
**
**	In order to work properly, this file MUST be saved as a UTF-8
**	encoded file with no byte order mark at the start of the file.
**
**  If you save this file with a BOM, you will get errors in your
**	HTML output when you try to view your site.
**
**	Most good text editors will allow you to save the file without
**	a BOM but Windows Notepad will not and should be avoided..!
**
****************************************************************/

/****************************************************************
**
**	To use assigned Smarty variables in localised strings, you
**	must ensure that the variable dollar sign is escaped with a
**	backslash character like this:
**	
**	\$myvariable_name
**
****************************************************************/

// Generic
$strings['EMAIL_001'] = "Nachricht von \$site_name";

// account.edit.tpl
$strings['EMAIL_002'] = "Hallo \$to_name,";
$strings['EMAIL_003'] = "Mit dieser automatisch erzeugten Nachricht teilt Ihnen der Administrator von \$site_name mit, dass Ihr Konto bei \$site_name aktualisiert worden ist.";
$strings['EMAIL_004'] = "Wenn diese E-Mail unzutreffend ist oder Sie Fragen haben, antworten Sie bitte auf diesen E-Mail und wir kümmern uns um den Vorgang so schnell es geht!";
$strings['EMAIL_005'] = "Mit freundlichem Gruß,";

// buy.notify.administrator.tpl
$strings['EMAIL_006'] = "Hallo \$name,";
$strings['EMAIL_007'] = "Mit dieser automatisch erzeugten Nachricht von \$site_name werden Sie über einen neue Bestellung informiert:";
$strings['EMAIL_008'] = "Bezahlmethode:";
$strings['EMAIL_009'] = "Bestellnummer:";
$strings['EMAIL_010'] = "Warenkorbnummer:";
$strings['EMAIL_011'] = "Benutzernummer:";
$strings['EMAIL_012'] = "Benutzername:";
$strings['EMAIL_013'] = "Bestellung ansehen:";

// buy.notify.duplicate.tpl
$strings['EMAIL_014'] = "Hallo \$to_name,";
$strings['EMAIL_015'] = "Mit dieser automatisch erzeugten Nachricht von \$site_name werden Sie über einen doppelte Bestellung informiert:";
$strings['EMAIL_016'] = "Bezahlmethode:";
$strings['EMAIL_017'] = "Bestellnummer:";
$strings['EMAIL_018'] = "Warenkorbnummer:";
$strings['EMAIL_019'] = "Benutzernummer:";
$strings['EMAIL_020'] = "Benutzername:";
$strings['EMAIL_021'] = "Bestellung ansehen:";

// buy.notify.tpl
$strings['EMAIL_022'] = "Hallo \$name,";
$strings['EMAIL_023'] = "Mit dieser automatisch erzeugten Nachricht von \$site_name teilen wir Ihnen den Zahlungseingang für Ihre Bestellung mit. Wenn diese E-Mail unzutreffend ist oder Sie Fragen haben, antworten Sie bitte auf diesen E-Mail und wir kümmern uns um den Vorgang so schnell es geht!";
$strings['EMAIL_024'] = "Mit freundlichem Gruß,";

// login.notify.tpl
$strings['EMAIL_025'] = "Der Benutzer \$user_name hat sich soeben bei \$site_name mit der IP Adresse \$ip_address angemeldet:";
$strings['EMAIL_026'] = "Anmeldungen in einem Konto, bei denen jedesmal völlig unterschiedliche IP Adressen verwendet werden, lassen darauf schliessen, dass sich verschiedene Benutzer ein Konto teilen.";
$strings['EMAIL_027'] = "Sie können die Zustellung weiterer Hinweise hier abschalten:";
$strings['EMAIL_028'] = "Detaillierte Informationen über Benutzeranmeldungen hier ansehen:";

// password.reset.tpl
$strings['EMAIL_029'] = "Hallo \$to_name,";
$strings['EMAIL_030'] = "Mit dieser automatisch erzeugten Nachricht von \$site_name teilen wir Ihnen mit, dass Ihr Passwort für Ihr Konto bei \$site_name geändert wurde.";
$strings['EMAIL_031'] = "Ihr Benutzername lautet: \$to_email";
$strings['EMAIL_032'] = "Ihr Passwort lautet: \$password";
$strings['EMAIL_033'] = "Wenn diese E-Mail unzutreffend ist oder Sie Fragen haben, antworten Sie bitte auf diesen E-Mail und wir kümmern uns um den Vorgang so schnell es geht!";
$strings['EMAIL_034'] = "Mit freundlichem Gruß,";

// postcard.content.tpl
$strings['EMAIL_035'] = "Diese Postkarte schickt Ihnen \$name von der Fotogalerie <a href=\"\$base_url\" style=\"color:#777;\">\$set_site_name</a>.";
$strings['EMAIL_036'] = "E-cards powered by <a href=\"http://www.pixaria.com/\" style=\"color:#777;\">Pixaria Gallery</a>.";

// registration.confirmation.01.tpl
$strings['EMAIL_037'] = "Hallo \$to_name,";
$strings['EMAIL_038'] = "Mit dieser automatisch erzeugten Nachricht teilen wir Ihnen mit, Ihr Konto bei \$site_name wurde erfolgreich angelegt und wird vom Administrator so bald als möglich geprüft.";
$strings['EMAIL_039'] = "Solange Ihr Konto noch deaktiviert ist, können Sie geschützten Alben nicht einsehen, die Lightbox können Sie aber schon in vollem Umfang nutzen.";
$strings['EMAIL_040'] = "Ihr Benutzername lautet: \$to_email";
$strings['EMAIL_041'] = "Ihr Passwort lautet: \$password";
$strings['EMAIL_042'] = "Wenn diese E-Mail unzutreffend ist oder Sie Fragen haben, antworten Sie bitte auf diesen E-Mail und wir kümmern uns um den Vorgang so schnell es geht!";
$strings['EMAIL_043'] = "Mit freundlichem Gruß,";

// registration.confirmation.02.tpl
$strings['EMAIL_044'] = "Hallo \$set_contact_name,";
$strings['EMAIL_045'] = "Diese automatisch erzeugte Nachricht von \$site_name informiert über eine neue Benutzerregistrierung.";
$strings['EMAIL_046'] = "Die E-Mailadresse des Benutzers lautet: \$to_email";
$strings['EMAIL_047'] = "Um das Benutzerkonto einzusehen, loggen Sie sich in den Administratorbereich hier ein:";

// submit.cart.admin.tpl
$strings['EMAIL_048'] = "This is an automated message from the administrator at \$site_name.";
$strings['EMAIL_049'] = "The user \$to_name has submitted a shopping cart from \$site_name.  Further details of this transaction can be viewed here:";

// submit.cart.user.tpl
$strings['EMAIL_050'] = "Hallo \$to_name,";
$strings['EMAIL_051'] = "Dies ist eine automatisch erzeugte Nachricht von \$site_name.";
$strings['EMAIL_052'] = "Wir haben Ihre Bestellung erhalten. Vielen Dank. Die Bestellung jederzeit einsehen können Sie mit diesem Link:";
$strings['EMAIL_053'] = "Wenn diese E-Mail unzutreffend ist oder Sie Fragen haben, antworten Sie bitte auf diesen E-Mail und wir kümmern uns um den Vorgang so schnell es geht!";
$strings['EMAIL_054'] = "Mit freundlichem Gruß,";

// transaction.message.admin.tpl
$strings['EMAIL_055'] = "Eine automatisch erzeugte Nachricht von \$site_name.";
$strings['EMAIL_056'] = "Der Benutzer \$to_name hat eine Nachricht zu dieser Bestellung geschickt:";

?>