<?php 

/****************************************************************
**
**	Pixaria Localisation Data
**	Copyright Jamie Longstaff
**
**	Translation by:		Mika Koivunen / jxmedia.fi (Original File)
**
**	Script author:		Jamie Longstaff
**	Script version:		2.x
**
****************************************************************/

/****************************************************************
**
**	THIS FILE MUST BE SAVED AS "UTF-8 NO BOM" (BYTE ORDER MARK)
**
**	In order to work properly, this file MUST be saved as a UTF-8
**	encoded file with no byte order mark at the start of the file.
**
**  If you save this file with a BOM, you will get errors in your
**	HTML output when you try to view your site.
**
**	Most good text editors will allow you to save the file without
**	a BOM but Windows Notepad will not and should be avoided..!
**
****************************************************************/

/****************************************************************
**
**	To use assigned Smarty variables in localised strings, you
**	must ensure that the variable dollar sign is escaped with a
**	backslash character like this:
**	
**	\$myvariable_name
**
****************************************************************/

// Generic
$strings['EMAIL_001'] = "Viesti sivustolta \$site_name";

// account.edit.tpl
$strings['EMAIL_002'] = "Hyv&auml; \$to_name,";
$strings['EMAIL_003'] = "T&auml;m&auml; on automaattinen viesti sivuston \$site_name yll&auml;pit&auml;j&auml;lt&auml;. Sivuston \$site_name on p&auml;ivitetty onnistuneesti.";
$strings['EMAIL_004'] = "Jos t&auml;m&auml; viesti on odottamaton tai sinulla on jotakin kysytt&auml;v&auml;&auml;, ole hyv&auml; ja vastaa t&auml;h&auml;n viestiin. Pyrimme vastaamaan mahdollisimman pikaisesti!";
$strings['EMAIL_005'] = "Terveisin,";

// buy.notify.administrator.tpl
$strings['EMAIL_006'] = "Hyv&auml; \$name,";
$strings['EMAIL_007'] = "T&auml;m&auml; on automaattinen ilmoitus sivuston \$site_name tilauksesta:";
$strings['EMAIL_008'] = "Maksutapa:";
$strings['EMAIL_009'] = "Tapahtuma ID:";
$strings['EMAIL_010'] = "Kortin ID:";
$strings['EMAIL_011'] = "K&auml;ytt&auml;j&auml;n ID:";
$strings['EMAIL_012'] = "K&auml;ytt&auml;j&auml;nimi:";
$strings['EMAIL_013'] = "N&auml;yt&auml; tilaus:";

// buy.notify.duplicate.tpl
$strings['EMAIL_014'] = "Hyv&auml; \$to_name,";
$strings['EMAIL_015'] = "T&auml;m&auml; on automaattinen ilmoitus sivustolta \$site_name , sinulla tupla tilaus:";
$strings['EMAIL_016'] = "Maksutapa:";
$strings['EMAIL_017'] = "Tapahtuma ID:";
$strings['EMAIL_018'] = "Kortin ID:";
$strings['EMAIL_019'] = "K&auml;ytt&auml;j&auml;n ID:";
$strings['EMAIL_020'] = "K&auml;ytt&auml;j&auml;nimi:";
$strings['EMAIL_021'] = "N&auml;yt&auml; tilaus:";

// buy.notify.tpl
$strings['EMAIL_022'] = "Hyv&auml; \$name,";
$strings['EMAIL_023'] = "T&auml;m&auml; on automaattinen viesti sivustolta \$site_name . Olemme vastaanottaneet ostoskorisi maksutapahtuman onnistuneesti. Jos t&auml;m&auml; viesti on odottamaton tai sinulla on jotakin kysytt&auml;v&auml;&auml;, ole hyv&auml; ja vastaa t&auml;h&auml;n viestiin. Pyrimme vastaamaan mahdollisimman pikaisesti!";
$strings['EMAIL_024'] = "Terveisin,";

// login.notify.tpl
$strings['EMAIL_025'] = "K&auml;ytt&auml;j&auml; \$user_name on juuri kirjautunut sis&auml;&auml;n sivustolle \$site_name IP-osoitteesta: \$ip_address";
$strings['EMAIL_026'] = "Useat kirjautumiset t&auml;h&auml;n tiliin eri IP osoitteista eri kerroilla, voi olla merkkin&auml; ett&auml; tili&auml; k&auml;ytt&auml;&auml; useat k&auml;ytt&auml;j&auml;t.";
$strings['EMAIL_027'] = "Voit poistaa n&auml;m&auml; ilmoitukset k&auml;yt&ouml;st&auml; t&auml;&auml;ll&auml;:";
$strings['EMAIL_028'] = "Katso tarkat k&auml;ytt&auml;jien kirjautumistiedot t&auml;&auml;lt&auml;:";

// password.reset.tpl
$strings['EMAIL_029'] = "Hyv&auml; \$to_name,";
$strings['EMAIL_030'] = "T&auml;m&auml; on automaattinen viesti sivuston \$site_name yll&auml;pit&auml;j&auml;lt&auml;. Tilisi salasanasi sivustolle \$site_name on vaihdettu.";
$strings['EMAIL_031'] = "K&auml;ytt&auml;j&auml;tunnuksesi on: \$to_email";
$strings['EMAIL_032'] = "Salasanasi on: \$password";
$strings['EMAIL_033'] = "Jos t&auml;m&auml; viesti on odottamaton tai sinulla on jotakin kysytt&auml;v&auml;&auml;, ole hyv&auml; ja vastaa t&auml;h&auml;n viestiin. Pyrimme vastaamaan mahdollisimman pikaisesti!";
$strings['EMAIL_034'] = "Terveisin,";

// postcard.content.tpl
$strings['EMAIL_035'] = "T&auml;m&auml; postikortin sinulle l&auml;hetti \$name sivustolta <a href=\"\$base_url\" style=\"color:#777;\">\$set_site_name</a>.";
$strings['EMAIL_036'] = "Postikortin moottorina <a href=\"http://www.pixaria.com/\" style=\"color:#777;\">Pixaria Galleria</a>.";

// registration.confirmation.01.tpl
$strings['EMAIL_037'] = "Hyv&auml; \$to_name,";
$strings['EMAIL_038'] = "T&auml;m&auml; on automaattinen viesti sivuston \$site_name yll&auml;pidolta. Tilisi sivustolla \$site_name on luotu onnistuneesti. Aktivoimme tilisi mahdollisimman pian.";
$strings['EMAIL_039'] = "Ennenkuin tilisi on aktivoitu, et voi selata salasana suojattuja kategorioita, mutta voit kirjautua sis&auml;&auml;n tiliisi ja k&auml;ytt&auml;&auml; valop&ouml;yd&auml;n ominaisuuksia.";
$strings['EMAIL_040'] = "K&auml;ytt&auml;j&auml;tunnuksesi on: \$to_email";
$strings['EMAIL_041'] = "Salasanasi on: \$password";
$strings['EMAIL_042'] = "Jos t&auml;m&auml; viesti on odottamaton tai sinulla on jotakin kysytt&auml;v&auml;&auml;, ole hyv&auml; ja vastaa t&auml;h&auml;n viestiin. Pyrimme vastaamaan mahdollisimman pikaisesti!";
$strings['EMAIL_043'] = "Terveisin,";

// registration.confirmation.02.tpl
$strings['EMAIL_044'] = "Hyv&auml; \$set_contact_name,";
$strings['EMAIL_045'] = "T&auml;m&auml; on automaattinen viesti sivustolta \$site_name , uusi k&auml;ytt&auml;j&auml; on rekister&ouml;itynyt.";
$strings['EMAIL_046'] = "K&auml;ytt&auml;j&auml;n s&auml;hk&ouml;posti osoite on: \$to_email";
$strings['EMAIL_047'] = "Voit hyv&auml;ksy&auml; k&auml;ytt&auml;j&auml;n hallintapaneelissa, johon p&auml;&auml;set t&auml;st&auml;:";

// submit.cart.admin.tpl
$strings['EMAIL_048'] = "T&auml;m&auml; on automaattinen viesti sivuston \$site_name yll&auml;pidolta.";
$strings['EMAIL_049'] = "K&auml;ytt&auml;j&auml; \$to_name on l&auml;hett&auml;nyt tilauksen sivulta \$site_name.  Tarkemmat tiedot l&ouml;ytyy t&auml;&auml;lt&auml;:";

// submit.cart.user.tpl
$strings['EMAIL_050'] = "Hyv&auml; \$to_name,";
$strings['EMAIL_051'] = "T&auml;m&auml; on automaattinen viesti sivuston \$site_name yll&auml;pidolta.";
$strings['EMAIL_052'] = "Ostoskorisi sis&auml;lt&ouml; on onnistuneesti vastaanotettu. Voit tarkistaa tilauksesi osoitteessa:";
$strings['EMAIL_053'] = "Jos t&auml;m&auml; viesti on odottamaton tai sinulla on jotakin kysytt&auml;v&auml;&auml;, ole hyv&auml; ja vastaa t&auml;h&auml;n viestiin. Pyrimme vastaamaan mahdollisimman pikaisesti!";
$strings['EMAIL_054'] = "Terveisin,";

// transaction.message.admin.tpl
$strings['EMAIL_055'] = "T&auml;m&auml; on automaattinen viesti sivustolta \$site_name.";
$strings['EMAIL_056'] = "K&auml;ytt&auml;j&auml; \$to_name on postittanut viestin, joka liittyy tilaukseen:";

?>