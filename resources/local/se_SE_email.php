<?php 

/****************************************************************
**
**	Pixaria Localisation Data
**	Copyright Jamie Longstaff
**
**	Translation by:		Jamie Longstaff (Original File)
**
**	Script author:		Jamie Longstaff
**	Script version:		2.x
**
****************************************************************/

/****************************************************************
**
**	THIS FILE MUST BE SAVED AS "UTF-8 NO BOM" (BYTE ORDER MARK)
**
**	In order to work properly, this file MUST be saved as a UTF-8
**	encoded file with no byte order mark at the start of the file.
**
**  If you save this file with a BOM, you will get errors in your
**	HTML output when you try to view your site.
**
**	Most good text editors will allow you to save the file without
**	a BOM but Windows Notepad will not and should be avoided..!
**
****************************************************************/

/****************************************************************
**
**	To use assigned Smarty variables in localised strings, you
**	must ensure that the variable dollar sign is escaped with a
**	backslash character like this:
**	
**	\$myvariable_name
**
****************************************************************/

// Generic
$strings['EMAIL_001'] = "Meddelande ifrån \$site_name";

// account.edit.tpl
$strings['EMAIL_002'] = "Kära \$to_name,";
$strings['EMAIL_003'] = "Detta är ett automatiskt meddelande från administratören på \$site_name för att meddela dig om att ditt konto på \$site_name har uppdaterats.";
$strings['EMAIL_004'] = "Om detta är oväntade eller om du har frågor kan du svara på detta meddelande och vi försöker att komma tillbaka till dig så snart vi kan!";
$strings['EMAIL_005'] = "Med vänliga hälsningar,";

// buy.notify.administrator.tpl
$strings['EMAIL_006'] = "Kära \$name,";
$strings['EMAIL_007'] = "Detta är ett automatiskt meddelande från \$site_name för att notera dig för en ny transaktion:";
$strings['EMAIL_008'] = "Betalningsmetod:";
$strings['EMAIL_009'] = "Transaction ID:";
$strings['EMAIL_010'] = "Kundvagns ID:";
$strings['EMAIL_011'] = "Användar-ID:";
$strings['EMAIL_012'] = "Användarnamn:";
$strings['EMAIL_013'] = "Visa transaktion:";

// buy.notify.duplicate.tpl
$strings['EMAIL_014'] = "Kära, \$to_name,";
$strings['EMAIL_015'] = "Detta är ett automatiskt meddelande från \$site_name för att informera dig om en dubblett transaktion:";
$strings['EMAIL_016'] = "Betalningsmetod:";
$strings['EMAIL_017'] = "Transaction ID:";
$strings['EMAIL_018'] = "Kundvagns ID:";
$strings['EMAIL_019'] = "Användar-ID:";
$strings['EMAIL_020'] = "Användarnamn:";
$strings['EMAIL_021'] = "Visa transaktion:";

// buy.notify.tpl
$strings['EMAIL_022'] = "Kära, \$name,";
$strings['EMAIL_023'] = "Detta är ett automatiskt meddelande från administratören på \$site_name för att bekräfta att du har lämnat in betalning för din inköp.  Om detta är oväntade eller om du har frågor kan du svara på det här meddelandet så tar vi tillbaka till dig så snart vi kan!";
$strings['EMAIL_024'] = "Med vänliga hälsningar,";

// login.notify.tpl
$strings['EMAIL_025'] = "Användaren \$user_name har just loggat in på \$site_name med IP adress: \$ip_address";
$strings['EMAIL_026'] = "Flera inloggningar för ett konto där IP-adressen är helt olika varje gång, kan tyda på att detta konto håller på att delas mellan många användare.";
$strings['EMAIL_027'] = "Du kan stänga av dessa anmälningar här:";
$strings['EMAIL_028'] = "Visa detaljerad information om användarens inloggningar:";

// password.reset.tpl
$strings['EMAIL_029'] = "Kära, \$to_name,";
$strings['EMAIL_030'] = "Detta är ett automatiskt meddelande från administratören på \$site_name för att berätta att lösenordet för ditt konto på \$site_name har ändrats.";
$strings['EMAIL_031'] = "Ditt användarnamn är: \$to_email";
$strings['EMAIL_032'] = "Ditt lösenord är: \$password";
$strings['EMAIL_033'] = "Om detta är oväntade eller om du har frågor kan du svara på detta meddelande och vi försöker att komma tillbaka till dig så snart vi kan!";
$strings['EMAIL_034'] = "Med vänliga hälsningar,";

// postcard.content.tpl
$strings['EMAIL_035'] = "Kortet skickades till dig av \$name från arkivet på <a href=\"\$base_url\" style=\"color:#777;\">\$set_site_name</a>.";
$strings['EMAIL_036'] = "E-kort skapades av <a href=\"http://www.pixaria.com/\" style=\"color:#777;\">Pixaria Gallery</a>.";

// registration.confirmation.01.tpl
$strings['EMAIL_037'] = "Kära, \$to_name,";
$strings['EMAIL_038'] = "Detta är ett automatiskt meddelande från administratören på \$site_name Ditt konto på \$site_name har skapats och den ansökan kommer att granskas av en administratör så snart som möjligt.";
$strings['EMAIL_039'] = "Fram till ditt konto är aktiverat kommer du inte kunna bläddra bland lösenordsskyddade album, men under tiden kan du logga in på ditt konto och utnyttja ljusbords funktionen av webbplatsen.";
$strings['EMAIL_040'] = "Ditt användarnamn är: \$to_email";
$strings['EMAIL_041'] = "Ditt lösenord är: \$password";
$strings['EMAIL_042'] = "Om detta är oväntade eller om du har frågor kan du svara på detta meddelande och vi försöker att komma tillbaka till dig så snart vi kan!";
$strings['EMAIL_043'] = "Med vänliga hälsningar,";

// registration.confirmation.02.tpl
$strings['EMAIL_044'] = "Kära, \$set_contact_name,";
$strings['EMAIL_045'] = "Detta är ett automatiskt meddelande från \$site_name för att berätta att en ny användare har registrerat.";
$strings['EMAIL_046'] = "Användarens e-postadress: \$to_email";
$strings['EMAIL_047'] = "Att administrera denna användaren loggar in på administrationen kontrollpanelen här:";

// submit.cart.admin.tpl
$strings['EMAIL_048'] = "Detta är ett automatiskt meddelande från administratören på \$site_name.";
$strings['EMAIL_049'] = "Användaren \$to_name har lämnat in en beställning från \$site_name.  Ytterligare information om denna transaktion kan ses här:";

// submit.cart.user.tpl
$strings['EMAIL_050'] = "Kära, \$to_name,";
$strings['EMAIL_051'] = "Detta är ett automatiskt meddelande från administratören på \$site_name.";
$strings['EMAIL_052'] = "Du har skickat din beställning till oss och kan nu se över din beställning när som helst genom att besöka oss på adressen nedan:";
$strings['EMAIL_053'] = "Om detta är oväntade eller om du har frågor kan du svara på detta meddelande och vi försöker att komma tillbaka till dig så snart vi kan!";
$strings['EMAIL_054'] = "Med vänliga hälsningar,";

// transaction.message.admin.tpl
$strings['EMAIL_055'] = "Detta är ett automatiskt meddelande från din webbplats \$site_name.";
$strings['EMAIL_056'] = "Användaren \$to_name har skickat ett inlägg i samband med denna transaktion:";

?>