<?php 

/****************************************************************
**
**	Pixaria Localisation Data
**	Copyright Jamie Longstaff
**
**	Translation by:		Jamie Longstaff (Original File)
**
**	Script author:		Jamie Longstaff
**	Script version:		2.x
**
****************************************************************/

/****************************************************************
**
**	THIS FILE MUST BE SAVED AS "UTF-8 NO BOM" (BYTE ORDER MARK)
**
**	In order to work properly, this file MUST be saved as a UTF-8
**	encoded file with no byte order mark at the start of the file.
**
**  If you save this file with a BOM, you will get errors in your
**	HTML output when you try to view your site.
**
**	Most good text editors will allow you to save the file without
**	a BOM but Windows Notepad will not and should be avoided..!
**
****************************************************************/

/****************************************************************
**
**	To use assigned Smarty variables in localised strings, you
**	must ensure that the variable dollar sign is escaped with a
**	backslash character like this:
**	
**	\$myvariable_name
**
****************************************************************/

// Generic
$strings['EMAIL_001'] = "Message from \$site_name";

// account.edit.tpl
$strings['EMAIL_002'] = "Dear \$to_name,";
$strings['EMAIL_003'] = "This is an automated message from the administrator at \$site_name to let you know that your account on \$site_name has been updated successfully.";
$strings['EMAIL_004'] = "If this is unexpected or if you have any questions, please reply to this message and we'll try to get back to you as soon as we can!";
$strings['EMAIL_005'] = "Best regards,";

// buy.notify.administrator.tpl
$strings['EMAIL_006'] = "Dear \$name,";
$strings['EMAIL_007'] = "This is an automated message from \$site_name to notify you of a new transaction:";
$strings['EMAIL_008'] = "Payment method:";
$strings['EMAIL_009'] = "Transaction ID:";
$strings['EMAIL_010'] = "Cart ID:";
$strings['EMAIL_011'] = "User ID:";
$strings['EMAIL_012'] = "User name:";
$strings['EMAIL_013'] = "View transaction:";

// buy.notify.duplicate.tpl
$strings['EMAIL_014'] = "Dear \$to_name,";
$strings['EMAIL_015'] = "This is an automated message from \$site_name to notify you of a duplicate transaction:";
$strings['EMAIL_016'] = "Payment method:";
$strings['EMAIL_017'] = "Transaction ID:";
$strings['EMAIL_018'] = "Cart ID:";
$strings['EMAIL_019'] = "User ID:";
$strings['EMAIL_020'] = "User name:";
$strings['EMAIL_021'] = "View transaction:";

// buy.notify.tpl
$strings['EMAIL_022'] = "Dear \$name,";
$strings['EMAIL_023'] = "This is an automated message from the administrator at \$site_name to confirm that you have sucessfully submitted payment for your shopping cart.  If this is unexpected or if you have any questions, please reply to this message and we'll get back to you as soon as we can!";
$strings['EMAIL_024'] = "Best regards,";

// login.notify.tpl
$strings['EMAIL_025'] = "The user \$user_name has just logged into \$site_name with IP address: \$ip_address";
$strings['EMAIL_026'] = "Multiple logins for an account where the IP address is completely different every time, may indicate that this account is being shared between many users.";
$strings['EMAIL_027'] = "You can turn off these notifications here:";
$strings['EMAIL_028'] = "View detailed information about user logins:";

// password.reset.tpl
$strings['EMAIL_029'] = "Dear \$to_name,";
$strings['EMAIL_030'] = "This is an automated message from the administrator at \$site_name to tell you that your account password on \$site_name has been successfully changed.";
$strings['EMAIL_031'] = "Your login username is: \$to_email";
$strings['EMAIL_032'] = "Your login password is: \$password";
$strings['EMAIL_033'] = "If this is unexpected or if you have any questions, please reply to this message and we'll try to get back to you as soon as we can!";
$strings['EMAIL_034'] = "Best regards,";

// postcard.content.tpl
$strings['EMAIL_035'] = "This card was sent to you by \$name from the gallery at <a href=\"\$base_url\" style=\"color:#777;\">\$set_site_name</a>.";
$strings['EMAIL_036'] = "E-cards powered by <a href=\"http://www.pixaria.com/\" style=\"color:#777;\">Pixaria Gallery</a>.";

// registration.confirmation.01.tpl
$strings['EMAIL_037'] = "Dear \$to_name,";
$strings['EMAIL_038'] = "This is an automated message from the administrator at \$site_name Your account on \$site_name has been created successfully and the application will be reviewed by an administrator as soon as possible.";
$strings['EMAIL_039'] = "Until your account is activated, you will not be able to browse password protected albums but in the meantime, you can log-in to your account and make full use of the lightbox feature of the site.";
$strings['EMAIL_040'] = "Your login username is: \$to_email";
$strings['EMAIL_041'] = "Your login password is: \$password";
$strings['EMAIL_042'] = "If this is unexpected or if you have any questions, please reply to this message and we'll try to get back to you as soon as we can!";
$strings['EMAIL_043'] = "Best regards,";

// registration.confirmation.02.tpl
$strings['EMAIL_044'] = "Dear \$set_contact_name,";
$strings['EMAIL_045'] = "This is an automated message from the \$site_name to tell you that a new user has registered.";
$strings['EMAIL_046'] = "The user's e-mail address is: \$to_email";
$strings['EMAIL_047'] = "To administer this user, log into the administration control panel here:";

// submit.cart.admin.tpl
$strings['EMAIL_048'] = "This is an automated message from the administrator at \$site_name.";
$strings['EMAIL_049'] = "The user \$to_name has submitted a shopping cart from \$site_name.  Further details of this transaction can be viewed here:";

// submit.cart.user.tpl
$strings['EMAIL_050'] = "Dear \$to_name,";
$strings['EMAIL_051'] = "This is an automated message from the administrator at \$site_name.";
$strings['EMAIL_052'] = "You have sucessfully submitted your shopping cart to us and can now review your order at any time by visting us at the URL below:";
$strings['EMAIL_053'] = "If this is unexpected or if you have any questions, please reply to this message and we'll try to get back to you as soon as we can!";
$strings['EMAIL_054'] = "Best regards,";

// transaction.message.admin.tpl
$strings['EMAIL_055'] = "This is an automated message from your website \$site_name.";
$strings['EMAIL_056'] = "The user \$to_name has posted a message in relation to this transaction:";

?>