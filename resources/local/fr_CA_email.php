<?php 

/****************************************************************
**
**	Pixaria Localisation Data
**	Copyright Jamie Longstaff
**
**	Translation by:		Frédéric Sune (French File) v0.1
**
**	Script author:		Jamie Longstaff
**	Script version:		2.x
**
****************************************************************/

/****************************************************************
**
**	THIS FILE MUST BE SAVED AS "UTF-8 NO BOM" (BYTE ORDER MARK)
**
**	In order to work properly, this file MUST be saved as a UTF-8
**	encoded file with no byte order mark at the start of the file.
**
**  If you save this file with a BOM, you will get errors in your
**	HTML output when you try to view your site.
**
**	Most good text editors will allow you to save the file without
**	a BOM but Windows Notepad will not and should be avoided..!
**
****************************************************************/

/****************************************************************
**
**	To use assigned Smarty variables in localised strings, you
**	must ensure that the variable dollar sign is escaped with a
**	backslash character like this:
**	
**	\$myvariable_name
**
****************************************************************/

// Generic
$strings['EMAIL_001'] = "Message provenant de \$site_name";

// account.edit.tpl
$strings['EMAIL_002'] = "Chèr(e) \$to_name,";
$strings['EMAIL_003'] = "Ceux-ci est un message automatisé de l'administrateur du site \$site_name pour vous laissez savoir que votre compte sur \$site_name a été mis à jour avec succès.";
$strings['EMAIL_004'] = "Si vous cela n'était pas prévu ou si vous avez des questions à ce sujet, merci de répondre à ce message et nous vous recontacterons au plus vite !";
$strings['EMAIL_005'] = "Cordialement,";

// buy.notify.administrator.tpl
$strings['EMAIL_006'] = "Chèr(e) \$name,";
$strings['EMAIL_007'] = "Ceux-ci est un message automatisé du site \$site_name pour vous signaler une nouvelle transaction :";
$strings['EMAIL_008'] = "Méthode de paiement :";
$strings['EMAIL_009'] = "ID de la transaction :";
$strings['EMAIL_010'] = "ID du panier :";
$strings['EMAIL_011'] = "ID de l'utilisateur :";
$strings['EMAIL_012'] = "Nom d'utilisateur :";
$strings['EMAIL_013'] = "Voir la transaction :";

// buy.notify.duplicate.tpl
$strings['EMAIL_014'] = "Chèr(e) \$to_name,";
$strings['EMAIL_015'] = "Ceux-ci est un message automatisé du site \$site_name pour vous signaler une duplication de votre transaction :";
$strings['EMAIL_016'] = "Méthode de paiement :";
$strings['EMAIL_017'] = "ID de la transaction :";
$strings['EMAIL_018'] = "ID du panier :";
$strings['EMAIL_019'] = "ID de l'utilisateur :";
$strings['EMAIL_020'] = "Nom d'utilisateur :";
$strings['EMAIL_021'] = "Voir la transaction :";

// buy.notify.tpl
$strings['EMAIL_022'] = "Chèr(e) \$name,";
$strings['EMAIL_023'] = "Ceux-ci est un message automatisé de l'administrateur du site \$site_name pour vous confirmer que le paiement de votre panier a été soumis avec succès. Si vous cela n'était pas prévu ou si vous avez des questions à ce sujet, merci de répondre à ce message et nous vous recontacterons au plus vite !";
$strings['EMAIL_024'] = "Cordialement,";

// login.notify.tpl
$strings['EMAIL_025'] = "L'utilisateur \$user_name vient de se connecter sur \$site_name avec l'adresse IP : \$ip_address";
$strings['EMAIL_026'] = "Plusieurs connection pour un même compte avec une adresse IP différente à chaque fois peut indiquer que ce compte est partagé par plusieurs utilisateurs.";
$strings['EMAIL_027'] = "Vous pouvez cette notification ici :";
$strings['EMAIL_028'] = "Voir les détails au sujet des connections des utilisateurs :";

// password.reset.tpl
$strings['EMAIL_029'] = "Chèr(e) \$to_name,";
$strings['EMAIL_030'] = "Ceux-ci est un message automatisé de l'administrateur du site \$site_name pour vous signaler que votre mot de passe sur \$site_name a été changé avec succès.";
$strings['EMAIL_031'] = "Votre nom d'utilisateur est : \$to_email";
$strings['EMAIL_032'] = "Votre mot de passe : \$password";
$strings['EMAIL_033'] = "Si vous cela n'était pas prévu ou si vous avez des questions à ce sujet, merci de répondre à ce message et nous vous recontacterons au plus vite !";
$strings['EMAIL_034'] = "Cordialement,";

// postcard.content.tpl
$strings['EMAIL_035'] = "cette carte postale électronique vous a été envoyé par \$name depuis la librairie <a href=\"\$base_url\" style=\"color:#777;\">\$set_site_name</a>.";
$strings['EMAIL_036'] = "Carte électronique générée par la <a href=\"http://www.pixaria.com/\" style=\"color:#777;\">Librairie Pixaria</a>.";

// registration.confirmation.01.tpl
$strings['EMAIL_037'] = "Chèr(e) \$to_name,";
$strings['EMAIL_038'] = "Ceux-ci est un message automatisé de l'administrateur du site \$site_name, Votre compte sur \$site_name a été créé avec succès et que votre demande est maintenant vérifiée par l'administrateur de ce site. Une confirmation de validation vous sera envoyé très prochainement";
$strings['EMAIL_039'] = "Tant que votre compte n'est pas actif, vous ne serez pas capable de naviguer aux travers des galeries protégées par un mot de passe mais vous pouvez vous connecter sur votre compte et utiliser toutes les fonctions de la visionneuse de ce site.";
$strings['EMAIL_040'] = "Votre nom d'utilisateur est : \$to_email";
$strings['EMAIL_041'] = "Votre mot de passe : \$password";
$strings['EMAIL_042'] = "Si vous cela n'était pas prévu ou si vous avez des questions à ce sujet, merci de répondre à ce message et nous vous recontacterons au plus vite !";
$strings['EMAIL_043'] = "Cordialement,";

// registration.confirmation.02.tpl
$strings['EMAIL_044'] = "Chèr(e) \$set_contact_name,";
$strings['EMAIL_045'] = "Ceux-ci est un message automatisé du site internet \$site_name pour vous dire qu'un nouvel utilisateur c'est enregistré.";
$strings['EMAIL_046'] = "L'adresse courriel de l'utilisateur est : \$to_email";
$strings['EMAIL_047'] = "Pour administrer cet utilisateur, connectez-vous sur l'interface d'administration ci-dessous :";

// submit.cart.admin.tpl
$strings['EMAIL_048'] = "Ceux-ci est un message automatisé de l'administrateur du site \$site_name.";
$strings['EMAIL_049'] = "L'utilisateur \$to_name a soumis un nouveau panier depuis \$site_name. Plus de détails sur cette transaction peut-être vu ici :";

// submit.cart.user.tpl
$strings['EMAIL_050'] = "Chèr(e) \$to_name,";
$strings['EMAIL_051'] = "Ceux-ci est un message automatisé de l'administrateur du site \$site_name.";
$strings['EMAIL_052'] = "Vous nous avez soumis avec succès votre panier et maintenant vous pouvez revoir votre transaction à n'importe quelle moment en nous visitant à l'adresse ci-dessous :";
$strings['EMAIL_053'] = "Si vous cela n'était pas prévu ou si vous avez des questions à ce sujet, merci de répondre à ce message et nous vous recontacterons au plus vite !";
$strings['EMAIL_054'] = "Cordialement,";

// transaction.message.admin.tpl
$strings['EMAIL_055'] = "Ceux-ci est un message automatisé de votre site internet \$site_name.";
$strings['EMAIL_056'] = "L'utilisateur \$to_name a envoyé un message en relation avec cette transaction :";

?>