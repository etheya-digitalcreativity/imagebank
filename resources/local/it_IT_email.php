<?php 

/****************************************************************
**
**	Pixaria Localisation Data
**	Copyright Jamie Longstaff
**
**	Italian Translation by:		Lina.Bergonzoni@gmail.com
**
**	Script author:		Jamie Longstaff
**	Script version:		2.x
**
****************************************************************/

/****************************************************************
**
**	THIS FILE MUST BE SAVED AS "UTF-8 NO BOM" (BYTE ORDER MARK)
**
**	In order to work properly, this file MUST be saved as a UTF-8
**	encoded file with no byte order mark at the start of the file.
**
**  If you save this file with a BOM, you will get errors in your
**	HTML output when you try to view your site.
**
**	Most good text editors will allow you to save the file without
**	a BOM but Windows Notepad will not and should be avoided..!
**
****************************************************************/

/****************************************************************
**
**	To use assigned Smarty variables in localised strings, you
**	must ensure that the variable dollar sign is escaped with a
**	backslash character like this:
**	
**	\$myvariable_name
**
****************************************************************/

// Generic
$strings['EMAIL_001'] = "Messaggio da \$site_name";

// account.edit.tpl
$strings['EMAIL_002'] = "Caro \$to_name,";
$strings['EMAIL_003'] = "Questo è un messaggio automatico dell'amministratore a \$site_name per informarti che il tuo account su \$site_name è stato aggiornato con successo.";
$strings['EMAIL_004'] = "Se si è verificato un disguido o se desideri ricevere più informazioni a riguardo ti preghiamo di rispondere a questo messaggio e cercheremo di contattarti al più presto!";
$strings['EMAIL_005'] = "Cordiali saluti,";

// buy.notify.administrator.tpl
$strings['EMAIL_006'] = "Caro \$name,";
$strings['EMAIL_007'] = "Questo è un messaggio automatico da \$site_name per notificarti la nuova transazione:";
$strings['EMAIL_008'] = "Modo di pagamento:";
$strings['EMAIL_009'] = "ID transazione:";
$strings['EMAIL_010'] = "ID carrello:";
$strings['EMAIL_011'] = "ID utente:";
$strings['EMAIL_012'] = "Nome utente:";
$strings['EMAIL_013'] = "Visualizza transazione:";

// buy.notify.duplicate.tpl
$strings['EMAIL_014'] = "Caro \$to_name,";
$strings['EMAIL_015'] = "Questo è un messaggio automatico da \$site_name per notificarti of a duplicate transaction:";
$strings['EMAIL_016'] = "Modo di pagamento:";
$strings['EMAIL_017'] = "ID transazione:";
$strings['EMAIL_018'] = "ID carrello:";
$strings['EMAIL_019'] = "ID utente:";
$strings['EMAIL_020'] = "Nome utente:";
$strings['EMAIL_021'] = "Visualizza transazione:";

// buy.notify.tpl
$strings['EMAIL_022'] = "Caro \$name,";
$strings['EMAIL_023'] = "Questo è un messaggio automatico dell'amministratore a \$site_name per confermare che il pagamento del tuo carrello è stato inoltrato correttamente.  Se si è verificato un disguido o se desideri ricevere più informazioni a riguardo ti preghiamo di rispondere a questo messaggio e cercheremo di contattarti al più presto!";
$strings['EMAIL_024'] = "Cordiali saluti,";

// login.notify.tpl
$strings['EMAIL_025'] = "L'utente \$user_name ha appena effettuato il login al sito \$site_name con il seguente indirizzo IP: \$ip_address";
$strings['EMAIL_026'] = "Se si verificano logins multipli ad un account con indirizzi IP che ogni volta sono completamente diversi, questo può indicare che l'account è stato condiviso tra più utenti.";
$strings['EMAIL_027'] = "Puoi chiudere queste notifiche qui:";
$strings['EMAIL_028'] = "Visualizza informazioni dettagliate sui login degli utenti:";

// password.reset.tpl
$strings['EMAIL_029'] = "Caro \$to_name,";
$strings['EMAIL_030'] = "Questo è un messaggio automatico dell'amministratore a \$site_name per comunicarti che la tua password di account sul sito \$site_name è stata modificata con successo.";
$strings['EMAIL_031'] = "Il tuo nome utente di login è: \$to_email";
$strings['EMAIL_032'] = "La tua password di login è: \$password";
$strings['EMAIL_033'] = "Se si è verificato un disguido o se desideri ricevere più informazioni a riguardo ti preghiamo di rispondere a questo messaggio e cercheremo di contattarti al più presto!";
$strings['EMAIL_034'] = "Cordiali saluti,";

// postcard.content.tpl
$strings['EMAIL_035'] = "Questa cartolina ti è stata inviata da \$name dalla galleria <a href=\"\$base_url\" style=\"color:#777;\">\$set_site_name</a>.";
$strings['EMAIL_036'] = "E-cards powered by <a href=\"http://www.pixaria.com/\" style=\"color:#777;\">Pixaria Gallery</a>.";

// registration.confirmation.01.tpl
$strings['EMAIL_037'] = "Caro \$to_name,";
$strings['EMAIL_038'] = "Questo è un messaggio automatico dell'amministratore a \$site_name Il tuo account su \$site_name è stato creato con successo, l'applicazione sarà controllata al più presto dall'amministratore.";
$strings['EMAIL_039'] = "Fino a quando il tuo account non sarà attivato non potrai accedere agli album protetti da password, ma nel frattempo puoi effettuare il log-in al tuo account ed usufruire pienamente della funzione lightbox di questo sito.";
$strings['EMAIL_040'] = "Il tuo nome utente di login è: \$to_email";
$strings['EMAIL_041'] = "La tua password di login è: \$password";
$strings['EMAIL_042'] = "Se si è verificato un disguido o se desideri ricevere più informazioni a riguardo ti preghiamo di rispondere a questo messaggio e cercheremo di contattarti al più presto!";
$strings['EMAIL_043'] = "Cordiali saluti,";

// registration.confirmation.02.tpl
$strings['EMAIL_044'] = "Caro \$set_contact_name,";
$strings['EMAIL_045'] = "Questo è un messaggio automatico di \$site_name per notificarti che un nuovo utente si è registrato.";
$strings['EMAIL_046'] = "L'indirizzo e-mail dell'utente è: \$to_email";
$strings['EMAIL_047'] = "Per amministrare questo utente effettua il login nel pannello di controllo dell'amministrazione:";

// submit.cart.admin.tpl
$strings['EMAIL_048'] = "Questo è un messaggio automatico dell'amministratore a \$site_name.";
$strings['EMAIL_049'] = "L'utente \$to_name ha inoltrato un carrello dal sito \$site_name.  Ulteriori dettagli su questa transazione possono essere visualizzati qui:";

// submit.cart.user.tpl
$strings['EMAIL_050'] = "Caro \$to_name,";
$strings['EMAIL_051'] = "Questo è un messaggio automatico dell'amministratore a \$site_name.";
$strings['EMAIL_052'] = "Il tuo carrello spesa ci è stato inoltrato con successo, adesso puoi controllare il tuo ordine in qualsiasi momento visitandoci alla seguente URL:";
$strings['EMAIL_053'] = "Se si è verificato un disguido o se desideri ricevere più informazioni a riguardo ti preghiamo di rispondere a questo messaggio e cercheremo di contattarti al più presto!";
$strings['EMAIL_054'] = "Cordiali saluti,";

// transaction.message.admin.tpl
$strings['EMAIL_055'] = "Questo è un messaggio automatico dal tuo sito \$site_name.";
$strings['EMAIL_056'] = "L'utente \$to_name ha postato un messaggio in relazione a questa transazione:";

?>