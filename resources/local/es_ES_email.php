<?php 

/****************************************************************
**
**	Pixaria Localisation Data
**	Copyright Jamie Longstaff
**
**	Translation by:		Jamie Longstaff (Original File)
**
**	Script author:		Jamie Longstaff
**	Script version:		2.x
**
****************************************************************/

/****************************************************************
**
**	THIS FILE MUST BE SAVED AS "UTF-8 NO BOM" (BYTE ORDER MARK)
**
**	In order to work properly, this file MUST be saved as a UTF-8
**	encoded file with no byte order mark at the start of the file.
**
**  If you save this file with a BOM, you will get errors in your
**	HTML output when you try to view your site.
**
**	Most good text editors will allow you to save the file without
**	a BOM but Windows Notepad will not and should be avoided..!
**
****************************************************************/

/****************************************************************
**
**	To use assigned Smarty variables in localised strings, you
**	must ensure that the variable dollar sign is escaped with a
**	backslash character like this:
**	
**	\$myvariable_name
**
****************************************************************/

// Generic
$strings['EMAIL_001'] = "Mensaje de \$site_name";

// account.edit.tpl
$strings['EMAIL_002'] = "Estimado/a \$to_name,";
$strings['EMAIL_003'] = "Éste es un mensaje automático del administrador de \$site_name para hacerte saber que tu cuenta en  \$site_name ha sido actualizada correctamente.";
$strings['EMAIL_004'] = "Si no esparabas esto o tienes alguna pregunta, por favor responde a este mensaje e intentaremos contestarte tan pronto como nos sea posible";
$strings['EMAIL_005'] = "Atentamente,";

// buy.notify.administrator.tpl
$strings['EMAIL_006'] = "Estimado/a \$name,";
$strings['EMAIL_007'] = "Éste es un mensaje automático de \$site_name pàra notificarte una nueva transacción:";
$strings['EMAIL_008'] = "Método de pago:";
$strings['EMAIL_009'] = "ID de la transacción:";
$strings['EMAIL_010'] = "ID del carro:";
$strings['EMAIL_011'] = "ID del usuario:";
$strings['EMAIL_012'] = "Nombre de usuario:";
$strings['EMAIL_013'] = "Ver transacción:";

// buy.notify.duplicate.tpl
$strings['EMAIL_014'] = "Estimado/a \$to_name,";
$strings['EMAIL_015'] = "Éste es un mensaje automático de \$site_name para notificarte una transacción duplicada:";
$strings['EMAIL_016'] = "Método de pago:";
$strings['EMAIL_017'] = "ID de la transacción:";
$strings['EMAIL_018'] = "ID del carro:";
$strings['EMAIL_019'] = "ID del usuario:";
$strings['EMAIL_020'] = "Nombre de usuario:";
$strings['EMAIL_021'] = "Ver transacción:";

// buy.notify.tpl
$strings['EMAIL_022'] = "Estimado/a \$name,";
$strings['EMAIL_023'] = "Éste es un mensaje automático del administrador de \$site_name para confirmar que has pagado lo que tenías en el carro de la compra.  Si no esparabas esto o tienes alguna pregunta, por favor responde a este mensaje e intentaremos contestarte tan pronto como nos sea posible";
$strings['EMAIL_024'] = "Atentamente,";

// login.notify.tpl
$strings['EMAIL_025'] = "El usuario \$user_name ha accedido a \$site_name bajo la IP: \$ip_address";
$strings['EMAIL_026'] = "Accesos múltiples para una cuenta donde la dirección IP es completamente diferente cada vez, lo cual puede indicar que esta cuenta está siendo compartida por varias personas diferentes.";
$strings['EMAIL_027'] = "Puedes desactivar estas notificaciones aquí:";
$strings['EMAIL_028'] = "Ver información detallada de los accesos de los usuarios:";

// password.reset.tpl
$strings['EMAIL_029'] = "Estimado/a \$to_name,";
$strings['EMAIL_030'] = "Éste es un mensaje automático del administrador de \$site_name para decirte que la contraseña de tu cuenta en \$site_name ha sido cambiada correctamente.";
$strings['EMAIL_031'] = "Tu usuario de acceso es: \$to_email";
$strings['EMAIL_032'] = "Tu contraseña de acceso es: \$password";
$strings['EMAIL_033'] = "Si no esparabas esto o tienes alguna pregunta, por favor responde a este mensaje e intentaremos contestarte tan pronto como nos sea posible";
$strings['EMAIL_034'] = "Atentamente,";

// postcard.content.tpl
$strings['EMAIL_035'] = "Esta tarjeta fue enviada por \$name desde la galería de <a href=\"\$base_url\" style=\"color:#777;\">\$set_site_name</a>.";
$strings['EMAIL_036'] = "Tarjetas electrónicas potenciadas por <a href=\"http://www.pixaria.com/\" style=\"color:#777;\">Pixaria Gallery</a>.";

// registration.confirmation.01.tpl
$strings['EMAIL_037'] = "Estimado/a \$to_name,";
$strings['EMAIL_038'] = "Éste es un mensaje automático del administrador de \$site_name Tu cuenta en \$site_name ha sido correctamente creada y la aplicación será revisada por un administrador tan pronto como sea posible.";
$strings['EMAIL_039'] = "Hasta que tu cuenta sea activada no podrás explorar los álbumes protegidos por contraseña pero, mientras tanto, puedes acceder a tu cuenta y hacer un uso completo de la mesa de luz de esta web.";
$strings['EMAIL_040'] = "Tu usuario de acceso es: \$to_email";
$strings['EMAIL_041'] = "Tu contraseña de acceso es: \$password";
$strings['EMAIL_042'] = "Si no esparabas esto o tienes alguna pregunta, por favor responde a este mensaje e intentaremos contestarte tan pronto como nos sea posible";
$strings['EMAIL_043'] = "Atentamente,";

// registration.confirmation.02.tpl
$strings['EMAIL_044'] = "Estimado/a \$set_contact_name,";
$strings['EMAIL_045'] = "Éste es un mensaje automático de \$site_name para decirte que un nuevo usuario se ha registrado.";
$strings['EMAIL_046'] = "El e-mail del usuario es: \$to_email";
$strings['EMAIL_047'] = "Para administrar este usuario, accede al panel de control administrativo aquí:";

// submit.cart.admin.tpl
$strings['EMAIL_048'] = "Éste es un mensaje automático del administrador de \$site_name.";
$strings['EMAIL_049'] = "El usuario \$to_name ha introducido un carro de la compra desde \$site_name. Más detalles de esta transacción se pueden ver aquí:";

// submit.cart.user.tpl
$strings['EMAIL_050'] = "Estimado/a \$to_name,";
$strings['EMAIL_051'] = "Éste es un mensaje automático del administrador de \$site_name.";
$strings['EMAIL_052'] = "Has introducido correctamente tu carro de la compra y ahora puedes revisar tu compra visitándonos en esta dirección:";
$strings['EMAIL_053'] = "Si no esparabas esto o tienes alguna pregunta, por favor responde a este mensaje e intentaremos contestarte tan pronto como nos sea posible";
$strings['EMAIL_054'] = "Atentamente,";

// transaction.message.admin.tpl
$strings['EMAIL_055'] = "Éste es un mensaje automático de tu web \$site_name.";
$strings['EMAIL_056'] = "El usuario \$to_name ha dejado un mensaje relacionado con esta transacción:";

?>