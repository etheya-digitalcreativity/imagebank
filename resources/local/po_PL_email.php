<?php 

/****************************************************************
**
**	Pixaria Localisation Data
**	Copyright Jamie Longstaff
**
**	Translation by:		Jamie Longstaff (Original File)
**
**	Script author:		Jamie Longstaff
**	Script version:		2.x
**
****************************************************************/

/****************************************************************
**
**	THIS FILE MUST BE SAVED AS "UTF-8 NO BOM" (BYTE ORDER MARK)
**
**	In order to work properly, this file MUST be saved as a UTF-8
**	encoded file with no byte order mark at the start of the file.
**
**  If you save this file with a BOM, you will get errors in your
**	HTML output when you try to view your site.
**
**	Most good text editors will allow you to save the file without
**	a BOM but Windows Notepad will not and should be avoided..!
**
****************************************************************/

/****************************************************************
**
**	To use assigned Smarty variables in localised strings, you
**	must ensure that the variable dollar sign is escaped with a
**	backslash character like this:
**	
**	\$myvariable_name
**
****************************************************************/

// Generic
$strings['EMAIL_001'] = "Wiadomość od \$site_name";

// account.edit.tpl
$strings['EMAIL_002'] = "Drogi(a) \$to_name,";
$strings['EMAIL_003'] = "To jest automatyczna wiadomość od administratora \$site_name abyś wiedział że Twoje konto w \$site_name zostało zaktualizowane.";
$strings['EMAIL_004'] = "Jeśli nie oczekiwałeś(aś) tego, masz więcej pytań, odpisz na tę wiadomość a postaramy się możliwie najszybciej odpowiedzieć na wszystko!";
$strings['EMAIL_005'] = "Pozdrawiam,";

// buy.notify.administrator.tpl
$strings['EMAIL_006'] = "Drogi(a) \$name,";
$strings['EMAIL_007'] = "To jest automatyczne powiadomienie z \$site_name o nowej transakcji:";
$strings['EMAIL_008'] = "Metoda płatności:";
$strings['EMAIL_009'] = "ID transakcji:";
$strings['EMAIL_010'] = "ID koszyka:";
$strings['EMAIL_011'] = "ID użytkownika:";
$strings['EMAIL_012'] = "Nazwa użytkownika:";
$strings['EMAIL_013'] = "Zobacz transakcje:";

// buy.notify.duplicate.tpl
$strings['EMAIL_014'] = "Drogi(a) \$to_name,";
$strings['EMAIL_015'] = "To jest automatyczne powiadomienie z \$site_name o duplikacie transakcji:";
$strings['EMAIL_016'] = "Metoda płatności";
$strings['EMAIL_017'] = "ID transakcji:";
$strings['EMAIL_018'] = "ID koszyka:";
$strings['EMAIL_019'] = "ID użytkownika:";
$strings['EMAIL_020'] = "Nazwa użytkownika:";
$strings['EMAIL_021'] = "Zobacz transakcje:";

// buy.notify.tpl
$strings['EMAIL_022'] = "Drogi(a) \$name,";
$strings['EMAIL_023'] = "to jest automatyczne powiadomienie od administratora \$site_name o prawidłowej płatności za zakupy. Jeśli nie oczekiwałeś(aś) tego, masz więcej pytań, odpisz na tę wiadomość a postaramy się możliwie najszybciej odpowiedzieć na wszystko";
$strings['EMAIL_024'] = "Pozdrawiam,";

// login.notify.tpl
$strings['EMAIL_025'] = "Użytkownik \$user_name właśnie się zalogował do \$site_name z adresem IP: \$ip_address";
$strings['EMAIL_026'] = "Wieloktrone logowania na to samo konto z wielu adresów ip, mogą świadczyć o dzieleniu się loginem i hasłem do konta.";
$strings['EMAIL_027'] = "Możesz wyłączyć to powiadomienie:";
$strings['EMAIL_028'] = "Zobacz szczegóły o logowaniu użytkownika:";

// password.reset.tpl
$strings['EMAIL_029'] = "Drogi(a) \$to_name,";
$strings['EMAIL_030'] = "To jest automatyczne powiadomienie z \$site_name o poprawnej zmianie hasła logowania do \$site_name.";
$strings['EMAIL_031'] = "Twój login: \$to_email";
$strings['EMAIL_032'] = "Twoje hasło: \$password";
$strings['EMAIL_033'] = "Jeśli nie oczekiwałeś(aś) tego, masz więcej pytań, odpisz na tę wiadomość a postaramy się możliwie najszybciej odpowiedzieć na wszystko!";
$strings['EMAIL_034'] = "Pozdrawiam,";

// postcard.content.tpl
$strings['EMAIL_035'] = "Ta kartka została wysłana od \$name z galerii <a href=\"\$base_url\" style=\"color:#777;\">\$set_site_name</a>.";
$strings['EMAIL_036'] = "E-cards powered by <a href=\"http://www.pixaria.com/\" style=\"color:#777;\">Pixaria Gallery</a>.";

// registration.confirmation.01.tpl
$strings['EMAIL_037'] = "Drogi(a) \$to_name,";
$strings['EMAIL_038'] = "To jest automatyczne powiadomienie od administratora \$site_name. Twoje konto na \$site_name zostało założone a zgłoszenie rozpatrzone przez administratora możliwie najszybciej.";
$strings['EMAIL_039'] = "Dopóki konto nie zostanie w pełni aktywowane, nie możesz przeglądać galerii zabezpieczonych hasłem, ale możesz się zalogować i używać lightboxa i innych funkcji specjalnych.";
$strings['EMAIL_040'] = "Twój login: \$to_email";
$strings['EMAIL_041'] = "Twoje hasło: \$password";
$strings['EMAIL_042'] = "Jeśli nie oczekiwałeś(aś) tego, masz więcej pytań, odpisz na tę wiadomość a postaramy się możliwie najszybciej odpowiedzieć na wszystko!";
$strings['EMAIL_043'] = "Pozdrawiam,";

// registration.confirmation.02.tpl
$strings['EMAIL_044'] = "Drogi(a) \$set_contact_name,";
$strings['EMAIL_045'] = "To jest automatyczne powiadomienie z \$site_name o nowej rejestracji na stronie.";
$strings['EMAIL_046'] = "Adres email użytkownika: \$to_email";
$strings['EMAIL_047'] = "Aby zarządzać tym użytkownikiem zaloguj się tuatj:";

// submit.cart.admin.tpl
$strings['EMAIL_048'] = "To jest automatyczne powiadominie z \$site_name.";
$strings['EMAIL_049'] = "Użytkownik \$to_name zgłosił zakup z \$site_name.  Dalsze informacje dostępne tutaj:";

// submit.cart.user.tpl
$strings['EMAIL_050'] = "Drogi(a) \$to_name,";
$strings['EMAIL_051'] = "To jest automatyczne powiadomienie z \$site_name.";
$strings['EMAIL_052'] = "Potwierdziłeś zakupy, możesz przejrzeć szczegóły zamówienia tutaj:";
$strings['EMAIL_053'] = "Jeśli nie oczekiwałeś(aś) tego, masz więcej pytań, odpisz na tę wiadomość a postaramy się możliwie najszybciej odpowiedzieć na wszystko!";
$strings['EMAIL_054'] = "Pozdrawiam,";

// transaction.message.admin.tpl
$strings['EMAIL_055'] = "To jest automatyczne powiadomienie z \$site_name.";
$strings['EMAIL_056'] = "Użytkownik \$to_name przekazał wiadomość z tym zamówieniem:";

?>