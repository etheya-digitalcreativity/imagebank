<?php

/****************************************************************
**
**  Pixaria Localisation Data
**  Copyright Jamie Longstaff
**
**  Translation by:    Zeljko Rogic
**
**  Script author:    Jamie Longstaff
**  Script version:    2.x
**
****************************************************************/

/****************************************************************
**
**  THIS FILE MUST BE SAVED AS "UTF-8 NO BOM" (BYTE ORDER MARK)
**
**  In order to work properly, this file MUST be saved as a UTF-8
**  encoded file with no byte order mark at the start of the file.
**
**  If you save this file with a BOM, you will get errors in your
**  HTML output when you try to view your site.
**
**  Most good text editors will allow you to save the file without
**  a BOM but Windows Notepad will not and should be avoided..!
**
****************************************************************/

/****************************************************************
**
**  To use assigned Smarty variables in localised strings, you
**  must ensure that the variable dollar sign is escaped with a
**  backslash character like this:
**
**  \$myvariable_name
**
****************************************************************/
// Generic
$strings['EMAIL_001'] = "\$site_name'den mesajiniz var";

// account.edit.tpl
$strings['EMAIL_002'] = "Sayin \$to_name,";
$strings['EMAIL_003'] = "Bu \$site_name tarafindan gönderilen otomatik bilgilendirme mesajidir. Hesabiniz aktiflestirilmistir.";
$strings['EMAIL_004'] = "Herhangi bir sorunuz varsa, lütfen bu mesaji yanitlayiniz. En kisa zamanda sizinle iltisim kurulacaktir!";
$strings['EMAIL_005'] = "Saygilarimizla,";

// buy.notify.administrator.tpl
$strings['EMAIL_006'] = "Sayin \$name,";
$strings['EMAIL_007'] = "Bu \$site_name tarafindan gönderilen otomatik bilgilendirme mesajidir. Alisverisiniz isleme konmustur:";
$strings['EMAIL_008'] = "Ödeme sekli:";
$strings['EMAIL_009'] = "Ödeme kodu:";
$strings['EMAIL_010'] = "Alisveris sepeti kodu:";
$strings['EMAIL_011'] = "Kullanici kodu:";
$strings['EMAIL_012'] = "Kullanici adi:";
$strings['EMAIL_013'] = "Alisverisi görüntüle:";

// buy.notify.duplicate.tpl
$strings['EMAIL_014'] = "Sayin \$to_name,";
$strings['EMAIL_015'] = "Bu \$site_name tarafindan gönderilen otomatik bilgilendirme mesajidir. Alisverisiniz tekrarlanmistir:";
$strings['EMAIL_016'] = "Ödeme sekli:";
$strings['EMAIL_017'] = "Ödeme kodu:";
$strings['EMAIL_018'] = "Alisveris sepeti kodu:";
$strings['EMAIL_019'] = "Kullanici kodu:";
$strings['EMAIL_020'] = "Kullanici adi:";
$strings['EMAIL_021'] = "Alisverisi görüntüle:";

// buy.notify.tpl
$strings['EMAIL_022'] = "Sayin \$name,";
$strings['EMAIL_023'] = "Bu \$site_name tarafindan gönderilen otomatik bilgilendirme mesajidir. Ödemenizi basariyla tamamladiniz.  Herhangi bir sorunuz varsa, lütfen bu mesaji yanitlayiniz. En kisa zamanda sizinle iltisim kurulacaktir!";
$strings['EMAIL_024'] = "Saygilarimizla,";

// login.notify.tpl
$strings['EMAIL_025'] = "\$user_name adli kullanici, \$site_name'e \$ip_address IP adresiyle giris yapti.";
$strings['EMAIL_026'] = "Birden fazla IP adresiyle yapilan girislerde, ayni hesap birden fazla kullanici tarafindan kullaniliyor olabilir.";
$strings['EMAIL_027'] = "Bu uyarilari buradan kapatabilirsiniz:";
$strings['EMAIL_028'] = "Kullanici hakkindaki bilgileri görüntüle:";

// password.reset.tpl
$strings['EMAIL_029'] = "Sayin \$to_name,";
$strings['EMAIL_030'] = "Bu \$site_name tarafindan gönderilen otomatik bilgilendirme mesajidir. \$site_name'deki hesap sifreniz basariyla degistirildi.";
$strings['EMAIL_031'] = "Kullanici adiniz: \$to_email";
$strings['EMAIL_032'] = "Sifreniz: \$password";
$strings['EMAIL_033'] = "Herhangi bir sorunuz varsa, lütfen bu mesaji yanitlayiniz. En kisa zamanda sizinle iltisim kurulacaktir!";
$strings['EMAIL_034'] = "Saygilarimizla,";

// postcard.content.tpl
$strings['EMAIL_035'] = "Bu kart size \$name tarafindan <a href=\"\$base_url\" style=\"color:#777;\">\$set_site_name</a> galerisinden gönderilmistir.";
$strings['EMAIL_036'] = "E-cards powered by <a href=\"http://www.pixaria.com/\" style=\"color:#777;\">Pixaria Gallery</a>.";

// registration.confirmation.01.tpl
$strings['EMAIL_037'] = "Sayin \$to_name,";
$strings['EMAIL_038'] = "Bu \$site_name tarafindan gönderilen otomatik bilgilendirme mesajidir. \$site_name'deki hesabiniz basariyla olusturulmustur. Basvurunuz en kisa zamanda site yöneticisi tarafindan degerlendirmeye alinacaktir.";
$strings['EMAIL_039'] = "Hesabiniz aktif edilene kadar, sifreli galeriler giris yapamazsiniz, ancak hesabiniza giris yapabilir ve sitedeki lightboxlari kullanabilirsiniz.";
$strings['EMAIL_040'] = "Kullanici adiniz: \$to_email";
$strings['EMAIL_041'] = "Sifreniz: \$password";
$strings['EMAIL_042'] = "Herhangi bir sorunuz varsa, lütfen bu mesaji yanitlayiniz. En kisa zamanda sizinle iltisim kurulacaktir!";
$strings['EMAIL_043'] = "Saygilarimizla,";

// registration.confirmation.02.tpl
$strings['EMAIL_044'] = "Sayin \$set_contact_name,";
$strings['EMAIL_045'] = "Bu \$site_name tarafindan gönderilen otomatik bilgilendirme mesajidir. Kayit islemi tamamlanmistir.";
$strings['EMAIL_046'] = "Kullanicinin e-posta adresi: \$to_email";
$strings['EMAIL_047'] = "Bu kullaniciyi yönetmek için, yönetici kontrol paneline giris yapiniz:";

// submit.cart.admin.tpl
$strings['EMAIL_048'] = "Bu \$site_name tarafindan gönderilen otomatik bilgilendirme mesajidir.";
$strings['EMAIL_049'] = "\$to_name adli kullanici, \$site_name'deki alisveris sepetini onayladi.  Alisverisin detaylari burada görüntülenebilir:";

// submit.cart.user.tpl
$strings['EMAIL_050'] = "Sayin \$to_name,";
$strings['EMAIL_051'] = "Bu \$site_name tarafindan gönderilen otomatik bilgilendirme mesajidir.";
$strings['EMAIL_052'] = "Alisverisinizi basariyla tamamladiniz. Asagidaki URL'yi kullanarak alisverisinizi görüntüleyebilirsiniz:";
$strings['EMAIL_053'] = "Herhangi bir sorunuz varsa, lütfen bu mesaji yanitlayiniz. En kisa zamanda sizinle iltisim kurulacaktir!";
$strings['EMAIL_054'] = "Saygilarimizla,";

// transaction.message.admin.tpl
$strings['EMAIL_055'] = "Bu \$site_name tarafindan gönderilen otomatik bilgilendirme mesajidir.";
$strings['EMAIL_056'] = "\$to_name adli kullanici, alisverisiyle ilgili mesaj gönderdi:";

?>