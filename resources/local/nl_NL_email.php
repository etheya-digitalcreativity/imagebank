<?php 

/****************************************************************
**
**	Pixaria Localisation Data
**	Copyright Jamie Longstaff
**
**	Translation by:		Jamie Longstaff (Original File)
**
**	Script author:		Jamie Longstaff
**	Script version:		2.x
**
****************************************************************/

/****************************************************************
**
**	THIS FILE MUST BE SAVED AS "UTF-8 NO BOM" (BYTE ORDER MARK)
**
**	In order to work properly, this file MUST be saved as a UTF-8
**	encoded file with no byte order mark at the start of the file.
**
**  If you save this file with a BOM, you will get errors in your
**	HTML output when you try to view your site.
**
**	Most good text editors will allow you to save the file without
**	a BOM but Windows Notepad will not and should be avoided..!
**
****************************************************************/

/****************************************************************
**
**	To use assigned Smarty variables in localised strings, you
**	must ensure that the variable dollar sign is escaped with a
**	backslash character like this:
**	
**	\$myvariable_name
**
****************************************************************/

// Generic
$strings['EMAIL_001'] = "Bericht van \$site_name";

// account.edit.tpl
$strings['EMAIL_002'] = "Beste \$to_name,";
$strings['EMAIL_003'] = "Dit is een automatich bericht van de beheerder van \$site_name om u te laten weten dat uw account op \$site_name succesvol is geupdate.";
$strings['EMAIL_004'] = "Als dit onverwacht is of als u vragen heeft, stuur dan een bericht terug en wij zullen proberen er zo snel mogelijk op terug te komen!";
$strings['EMAIL_005'] = "Met vriendelijke groet,";

// buy.notify.beheerder.tpl
$strings['EMAIL_006'] = "Beste \$name,";
$strings['EMAIL_007'] = "Dit is een automatich bericht van \$site_name om u op de hoogte te brengen van een nieuwe transactie:";
$strings['EMAIL_008'] = "Betaal methode:";
$strings['EMAIL_009'] = "Identiteit van transactie:";
$strings['EMAIL_010'] = "Identiteit van kaart:";
$strings['EMAIL_011'] = "Identiteit gebruiker:";
$strings['EMAIL_012'] = "Gebruikersnaam:";
$strings['EMAIL_013'] = "Bekijk transactie:";

// buy.notify.duplicate.tpl
$strings['EMAIL_014'] = "Beste \$to_name,";
$strings['EMAIL_015'] = "Dit is een automatich bericht van \$site_name om u op de hoogte te brengen van een dubbele transactie:";
$strings['EMAIL_016'] = "Betaal methode:";
$strings['EMAIL_017'] = "Identiteit van transactie:";
$strings['EMAIL_018'] = "Identiteit van kaart:";
$strings['EMAIL_019'] = "Identiteit gebruiker:";
$strings['EMAIL_020'] = "Gebruikersnaam:";
$strings['EMAIL_021'] = "Bekijk transactie:";

// buy.notify.tpl
$strings['EMAIL_022'] = "Beste \$name,";
$strings['EMAIL_023'] = "Dit is een automatich bericht van de beheerder van \$site_name om u op de hoogte te brengen van de succesvolle betaling van uw winkelwagen.  Als dit onverwacht is of als u vragen heeft, stuur dan een bericht terug en wij zullen proberen er zo snel mogelijk op terug te komen!";
$strings['EMAIL_024'] = "Met vriendelijke groet,";

// login.notify.tpl
$strings['EMAIL_025'] = "De gebruiker \$user_name heeft zich juist ingelogt op \$site_name met IP adres: \$ip_address";
$strings['EMAIL_026'] = "Meerdere logins voor een account waarvan het IP adres steeds totaal verschillend is, kunnen er op wijzen dat deze account word bebruikt door meerdere personen.";
$strings['EMAIL_027'] = "U kunt hier deze berichten uitzetten:";
$strings['EMAIL_028'] = "Bekijk de gedetailleerde informatie over de gebruiker zijn logins:";

// password.reset.tpl
$strings['EMAIL_029'] = "Beste \$to_name,";
$strings['EMAIL_030'] = "Dit is een automatich bericht van de beheerder van \$site_name om u te vertellen dat uw account wachtwoord op \$site_name succesvol is veranderd.";
$strings['EMAIL_031'] = "Uw inlog gebruikersnaam is: \$to_email";
$strings['EMAIL_032'] = "Uw inlog wachtwoord is: \$password";
$strings['EMAIL_033'] = "Als dit onverwacht is of als u vragen heeft, stuur dan een bericht terug en wij zullen proberen er zo snel mogelijk op terug te komen!";
$strings['EMAIL_034'] = "Met vriendelijke groet,";

// postcard.content.tpl
$strings['EMAIL_035'] = "Deze kaart is naar u gestuurd door \$name vanaf de gallerij op <a href=\"\$base_url\" style=\"color:#777;\">\$set_site_name</a>.";
$strings['EMAIL_036'] = "E-cards powered by <a href=\"http://www.pixaria.com/\" style=\"color:#777;\">Pixaria Gallery</a>.";

// registration.confirmation.01.tpl
$strings['EMAIL_037'] = "Beste \$to_name,";
$strings['EMAIL_038'] = "Dit is een automatich bericht van de beheerder van \$site_name Uw account op \$site_name is succesvol aangemaakt en uw aanvraag zal door de beheerder zo snel mogelijk worden bekeken.";
$strings['EMAIL_039'] = "Tot uw account is geactiveerd kunt u niet in door wachtwoord beschermde albums bladeren, maar in tussentijd kunt u op uw account inloggen en vollop gebruik maken van de lightbox funtie van de site.";
$strings['EMAIL_040'] = "Uw inlog gebruikersnaam is: \$to_email";
$strings['EMAIL_041'] = "Uw inlog wachtwoord is: \$password";
$strings['EMAIL_042'] = "Als dit onverwacht is of als u vragen heeft, stuur dan een bericht terug en wij zullen proberen er zo snel mogelijk op terug te komen!";
$strings['EMAIL_043'] = "Met vriendelijke groet,";

// registration.confirmation.02.tpl
$strings['EMAIL_044'] = "Beste \$set_contact_name,";
$strings['EMAIL_045'] = "Dit is een automatich bericht van \$site_name om u te vertellen dat er een nieuwe gebruiker is geregistreerd.";
$strings['EMAIL_046'] = "Het e-mail adres van de gebruiker is: \$to_email";
$strings['EMAIL_047'] = "Om deze gebruiker te beheren, log hier in op de beheeromgeving:";

// submit.cart.admin.tpl
$strings['EMAIL_048'] = "Dit is een automatich bericht van de beheerder van \$site_name.";
$strings['EMAIL_049'] = "De gebruiker \$to_name heeft een winkelwagen toegevoegt op \$site_name.  Verdere details van deze transactie kunnen hier bekeken worden:";

// submit.cart.user.tpl
$strings['EMAIL_050'] = "Dear \$to_name,";
$strings['EMAIL_051'] = "Dit is een automatich bericht van de beheerder van \$site_name.";
$strings['EMAIL_052'] = "U heeft succesvol een winkelwagen toegevoegt bij ons en u kunt uw bestellingen nu bekijken wanneer u onze site bezoekt. Met de URL hieronder gaat u naar de website:";
$strings['EMAIL_053'] = "Als dit onverwacht is of als u vragen heeft, stuur dan een bericht terug en wij zullen proberen er zo snel mogelijk op terug te komen!";
$strings['EMAIL_054'] = "Met vriendelijke groet,";

// transaction.message.admin.tpl
$strings['EMAIL_055'] = "Dit is een automatich bericht van uw website \$site_name.";
$strings['EMAIL_056'] = "De gebruiker \$to_name heeft een bericht geplaats in verband met deze transactie:";
S
?>