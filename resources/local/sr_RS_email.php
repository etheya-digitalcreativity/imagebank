<?php

/****************************************************************
**
**  Pixaria Localisation Data
**  Copyright Jamie Longstaff
**
**  Translation by:    Zeljko Rogic
**
**  Script author:    Jamie Longstaff
**  Script version:    2.x
**
****************************************************************/

/****************************************************************
**
**  THIS FILE MUST BE SAVED AS "UTF-8 NO BOM" (BYTE ORDER MARK)
**
**  In order to work properly, this file MUST be saved as a UTF-8
**  encoded file with no byte order mark at the start of the file.
**
**  If you save this file with a BOM, you will get errors in your
**  HTML output when you try to view your site.
**
**  Most good text editors will allow you to save the file without
**  a BOM but Windows Notepad will not and should be avoided..!
**
****************************************************************/

/****************************************************************
**
**  To use assigned Smarty variables in localised strings, you
**  must ensure that the variable dollar sign is escaped with a
**  backslash character like this:
**
**  \$myvariable_name
**
****************************************************************/

// Generic
$strings['EMAIL_001'] = "Poruka od \$site_name";

// account.edit.tpl
$strings['EMAIL_002'] = "Zdravo \$to_name,";
$strings['EMAIL_003'] = "ovo je automatski generisana poruka od strane \$site_name kao informacija da je vaš nalog na \$site_name uspješno osvježen.";
$strings['EMAIL_004'] = "Ukoliko je ova poruka neočekivana ili ako smatrate da je došlo do greške molimo vas da kontaktirate administratora!";
$strings['EMAIL_005'] = "Srdačan pozdrav,";

// buy.notify.administrator.tpl
$strings['EMAIL_006'] = "Zdravo \$name,";
$strings['EMAIL_007'] = "Ovo je automatski generisana poruka od \$site_name kao obavještenje o novoj transkaciji:";
$strings['EMAIL_008'] = "Način plaćanja:";
$strings['EMAIL_009'] = "Transakcija broj:";
$strings['EMAIL_010'] = "Broj korpe:";
$strings['EMAIL_011'] = "ID korisnika:";
$strings['EMAIL_012'] = "Korisničko ime:";
$strings['EMAIL_013'] = "Vidi transsakciju:";

// buy.notify.duplicate.tpl
$strings['EMAIL_014'] = "Zdravo, \$to_name,";
$strings['EMAIL_015'] = "Ovo je automatski generisana poruka od strane \$site_name kao informacija o kopiji vaše transakcije:";
$strings['EMAIL_016'] = "Način plaćanja:";
$strings['EMAIL_017'] = "Transakcija broj:";
$strings['EMAIL_018'] = "Broj korpe:";
$strings['EMAIL_019'] = "ID korisnika:";
$strings['EMAIL_020'] = "Korisničko ime:";
$strings['EMAIL_021'] = "Vidi transakciju:";

// buy.notify.tpl
$strings['EMAIL_022'] = "Zdravo \$name,";
$strings['EMAIL_023'] = "Ovo je automatski generisana poruka od administratora \$site_name kao potvrda da su svi proizvodi uspješno plaćeni.  Ukoliko je ova poruka neočekivana ili ako smatrate da je došlo do greške molimo vas da kontaktirate administratora!";
$strings['EMAIL_024'] = "Srdačan pozdrav,";

// login.notify.tpl
$strings['EMAIL_025'] = "Korisnik \$user_name se upravo prijavio na \$site_name, IP adresa: \$ip_address";
$strings['EMAIL_026'] = "Prijavljivanje na isti nalog sa više IP adresa u isto vrijeme indicira da više osoba koristi jedan nalog.";
$strings['EMAIL_027'] = "Ova obavještenja možete isključiti ovdje:";
$strings['EMAIL_028'] = "Vidi detaljan prikaz o prijavljivanju korisnika na sajt:";

// password.reset.tpl
$strings['EMAIL_029'] = "Zdravo \$to_name,";
$strings['EMAIL_030'] = "ovo je automatski generisana poruka od adminisratora \$site_name kao informacija da je vaša lozinka na \$site_name poništena.";
$strings['EMAIL_031'] = "Vaše korisničko ime je: \$to_email";
$strings['EMAIL_032'] = "Vaša lozinka je: \$password";
$strings['EMAIL_033'] = "Ukoliko je ova poruka neočekivana ili ako smatrate da je došlo do greške molimo vas da kontaktirate administratora!";
$strings['EMAIL_034'] = "Srdačan pozdrav,";

// postcard.content.tpl
$strings['EMAIL_035'] = "Ovaj sadržaj je poslan od strane \$name iz galerije <a href=\"\$base_url\" style=\"color:#777;\">\$set_site_name</a>.";
$strings['EMAIL_036'] = "E-cards powered by <a href=\"http://www.pixaria.com/\" style=\"color:#777;\">Pixaria Gallery</a>.";

// registration.confirmation.01.tpl
$strings['EMAIL_037'] = "Zdravo \$to_name,";
$strings['EMAIL_038'] = "ovo je automatski generisana poruka od strane administratora sajta \$site_name kao obavještenje da je vaš nalog na \$site_name uspješno kreiran i da čeka odobrenje administratora.";
$strings['EMAIL_039'] = "Dok vaš nalog ne postane aktivan nećete biti u mogućnosti da gledate galerije zaštićene lozinkom ali u međuvremenu možete se prijaviti na sajt i dodatno podesiti svoj nalog i lightbox.";
$strings['EMAIL_040'] = "Vaše korisničko ime je: \$to_email";
$strings['EMAIL_041'] = "Vaša lozinka je: \$password";
$strings['EMAIL_042'] = "Ukoliko je ova poruka neočekivana ili ako smatrate da je došlo do greške molimo vas da kontaktirate administratora!";
$strings['EMAIL_043'] = "Srdačan pozdrav,";

// registration.confirmation.02.tpl
$strings['EMAIL_044'] = "Zdravo \$set_contact_name,";
$strings['EMAIL_045'] = "Ovo je automatski generisana poruka sa sajta \$site_name kao informacija da je registrovan novi korisnik.";
$strings['EMAIL_046'] = "Korisnikov e-mail je: \$to_email";
$strings['EMAIL_047'] = "Za upravljanje ovoim nalogom prijavite se ovdje:";

// submit.cart.admin.tpl
$strings['EMAIL_048'] = "Ovo je automatski generisana poruka sa sajta \$site_name.";
$strings['EMAIL_049'] = "Korisnik \$to_name dodao korpu na \$site_name.  Za više detalja o transakciji klikni na link ispod:";

// submit.cart.user.tpl
$strings['EMAIL_050'] = "Zdravo \$to_name,";
$strings['EMAIL_051'] = "Ovo je automatski generisana poruka sa sajta \$site_name.";
$strings['EMAIL_052'] = "Uspješno ste dodali sadržaje iz vaše korpe, za pregled transakcije u bilo kom trenutku klikni na link ispod:";
$strings['EMAIL_053'] = "Ukoliko je ova poruka neočekivana ili ako smatrate da je došlo do greške molimo vas da kontaktirate administratora!";
$strings['EMAIL_054'] = "Srdačan pozdrav,";

// transaction.message.admin.tpl
$strings['EMAIL_055'] = "Ovo je automatski generisana poruka sa sajta \$site_name.";
$strings['EMAIL_056'] = "Korisnik \$to_name je postavio pitanje u vezi sa transakcijom:";

?>