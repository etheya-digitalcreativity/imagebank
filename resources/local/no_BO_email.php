<?php 

/****************************************************************
**
**	Pixaria Localisation Data
**	Copyright Jamie Longstaff
**
**	Translation by:		Morten Halvorsen(Original File)
**
**	Script author:		Jamie Longstaff
**	Script version:		2.x
**
****************************************************************/

/****************************************************************
**
**	THIS FILE MUST BE SAVED AS "UTF-8 NO BOM" (BYTE ORDER MARK)
**
**	In order to work properly, this file MUST be saved as a UTF-8
**	encoded file with no byte order mark at the start of the file.
**
**  If you save this file with a BOM, you will get errors in your
**	HTML output when you try to view your site.
**
**	Most good text editors will allow you to save the file without
**	a BOM but Windows Notepad will not and should be avoided..!
**
****************************************************************/

/****************************************************************
**
**	To use assigned Smarty variables in localised strings, you
**	must ensure that the variable dollar sign is escaped with a
**	backslash character like this:
**	
**	\$myvariable_name
**
****************************************************************/

// Generic
$strings['EMAIL_001'] = "Melding fra  \$site_name";

// account.edit.tpl
$strings['EMAIL_002'] = "Hei \$to_name,";
$strings['EMAIL_003'] = "Detta er en automatiskt melding fra administratoren på \$site_name for å meddele at din konto på \$site_name er oppdatert.";
$strings['EMAIL_004'] = "Hvis dette var en uventet melding eller om du har andre spørsmål, kan du sende denne epost i retur så skal vi svare så snart vi kan!";
$strings['EMAIL_005'] = "Med vennlig hilsen,";

// buy.notify.administrator.tpl
$strings['EMAIL_006'] = "Hei \$name,";
$strings['EMAIL_007'] = "Dette er en automatiskt melding fra \$site_name for å meddele om en ny bestilling:";
$strings['EMAIL_008'] = "Betalingsmetode:";
$strings['EMAIL_009'] = "Transaksjons ID:";
$strings['EMAIL_010'] = "Handlevogn ID:";
$strings['EMAIL_011'] = "Bruker-ID:";
$strings['EMAIL_012'] = "Brukernavn:";
$strings['EMAIL_013'] = "Vis bestilling:";

// buy.notify.duplicate.tpl
$strings['EMAIL_014'] = "Hei, \$to_name,";
$strings['EMAIL_015'] = "Dette er en automatiskt melding fra \$site_name for å meddele om en dobbelbestilling:";
$strings['EMAIL_016'] = "Betalingsmetode:";
$strings['EMAIL_017'] = "Transaksjon ID:";
$strings['EMAIL_018'] = "Handlevogn ID:";
$strings['EMAIL_019'] = "bruker-ID:";
$strings['EMAIL_020'] = "Brukenavn:";
$strings['EMAIL_021'] = "Vis bestilling:";

// buy.notify.tpl
$strings['EMAIL_022'] = "Hei, \$name,";
$strings['EMAIL_023'] = "Dette er en automatiskt melding fra administratoren på \$site_name som bekrefter din innbetaling for dine kjøp.  Hvis dette var en uventet melding eller om du har andre spørsmål, kan du sende denne epost i retur så skal vi svare så snart vi kan!";
$strings['EMAIL_024'] = "Med vennlig hilsen,";

// login.notify.tpl
$strings['EMAIL_025'] = "Brukeren \$user_name har akkurat logget inn på \$site_name med IP adresse: \$ip_address";
$strings['EMAIL_026'] = "Flere inlogginger for en konto der IP-adressen er helt ulika hver gang, kan bety at denna kontoen bli delt av flere brukere.";
$strings['EMAIL_027'] = "Du kan slå av disse meldingene her:";
$strings['EMAIL_028'] = "Vis detaljert informasjon om brukerens inlogginger:";

// password.reset.tpl
$strings['EMAIL_029'] = "Hei, \$to_name,";
$strings['EMAIL_030'] = "Dette er en automatiskt melding fra administratoren på \$site_name , som er sndt for å gi deg melding om at ditt passord på \$site_name er endret.";
$strings['EMAIL_031'] = "Ditt brukernavn er: \$to_email";
$strings['EMAIL_032'] = "Ditt passord er: \$password";
$strings['EMAIL_033'] = "Hvis dette var en uventet melding eller om du har andre spørsmål, kan du sende denne epost i retur så skal vi svare så snart vi kan!";
$strings['EMAIL_034'] = "Med vennlig hilsen,";

// postcard.content.tpl
$strings['EMAIL_035'] = "Kortet ble sendt til deg av \$name fra arkivet på <a href=\"\$base_url\" style=\"color:#777;\">\$set_site_name</a>.";
$strings['EMAIL_036'] = "E-kortene blir laget med <a href=\"http://www.pixaria.com/\" style=\"color:#777;\">Pixaria Gallery</a>.";

// registration.confirmation.01.tpl
$strings['EMAIL_037'] = "Hei, \$to_name,";
$strings['EMAIL_038'] = "Dette er en automatiskt melding fra administratoren på \$site_name Din konto på \$site_name er opprettet og søknaden blir gjennongått av administratoren så snart som mulig.";
$strings['EMAIL_039'] = "Frem til din konto er aktivert kan du ikke bla gjennom passordbeskyttet album, men du kan logge deg inn å benytte favorittalbum-funksjonen.";
$strings['EMAIL_040'] = "Ditt brukernavn er: \$to_email";
$strings['EMAIL_041'] = "Ditt passord er: \$password";
$strings['EMAIL_042'] = "Hvis dette var en uventet melding eller om du har andre spørsmål, kan du sende denne epost i retur så skal vi svare så snart vi kan!";
$strings['EMAIL_043'] = "Med vennlig hilsen,";

// registration.confirmation.02.tpl
$strings['EMAIL_044'] = "Hei, \$set_contact_name,";
$strings['EMAIL_045'] = "Dette er en automatiskt melding fra \$site_name for a meddele at en ny bruker har registrert seg.";
$strings['EMAIL_046'] = "Brukerens e-postadresse: \$to_email";
$strings['EMAIL_047'] = "For å administrere denne brukeren, logger du inn på administrasjonspanelet her:";

// submit.cart.admin.tpl
$strings['EMAIL_048'] = "Dette er en automatiskt melding fra administratoren på \$site_name.";
$strings['EMAIL_049'] = "Brukeren \$to_name har lagt inn en bestilling på \$site_name.  Ytterligere informasjon om denne bestillingen kan ses her:";

// submit.cart.user.tpl
$strings['EMAIL_050'] = "Hei, \$to_name,";
$strings['EMAIL_051'] = "Dette er en automatiskt melding fra administratoren på på \$site_name.";
$strings['EMAIL_052'] = "Du har sendt din bestilling til oss og du kan se din bestilling når som helst ved å besøke oss på adressen under:";
$strings['EMAIL_053'] = "Hvis dette var en uventet melding eller om du har andre spørsmål, kan du sende denne epost i retur så skal vi svare så snart vi kan!";
$strings['EMAIL_054'] = "Med vennlig hilsen,";

// transaction.message.admin.tpl
$strings['EMAIL_055'] = "Dette er en automatiskt melding fra ditt nettsted \$site_name.";
$strings['EMAIL_056'] = "Brukeren \$to_name har sendt en melding i sammenheng med denne bestillingen:";

?>