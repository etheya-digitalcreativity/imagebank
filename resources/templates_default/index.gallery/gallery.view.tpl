{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}

{if $gallery_id}

	<table class="table-gallery-description">
	
		<thead>
		
			<tr>
			<th><a href="{$base_url}{$fil_index}" class="plain">##GALLERY_20##</a> &raquo; <a href="{$base_url}{$fil_index_gallery}" class="plain">##GALLERY_21##</a> &raquo; <a href="{$nav_href}" class="plain">{$nav_title}</a></th>
			</tr>
		
		</thead>
		
		<tbody>
		
			<tr>
			
			<td>
			
				<table>
				<tr>
				<th><img src="{$base_url}{$fil_psg_image_thumbnail}?file={$gallery_avatar.files.large_path}" {$gallery_avatar.files.large_size.2} class="thumbnail" alt="{$gallery_title}" title="{$gallery_title}" /></th>
				<td>
				
				<h1>{$gallery_title}</h1>
				
				<p>{$gallery_description|nl2br}</p>
				
				</td>
				</tr>
				</table>
			
			</td>
			
			</tr>
		
		</tbody>
	
	</table>

{/if}





<!-- INCLUDE TEMPLATE FOR DISPLAYING IMAGE THUMBNAILS -->
{include_template file="index.gallery/gallery.thumbnails.tpl"}



	
<!-- INCLUDE TEMPLATE FOR DISPLAYING IMAGE THUMBNAILS -->
{include_template file="index.image/view.thumbnails.tpl"}



	
<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="index.layout/main.footer.tpl"}


