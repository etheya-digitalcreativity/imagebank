{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}



<table class="table-text">

<tr>
<th>##GALLERY_01##</th>
<td>

<h1>##GALLERY_02##</h1>

<p>##GALLERY_03##</p>

<p>##GALLERY_04##</p>

{if $warning == TRUE}
<p class="html-error-warning">##GALLERY_05##</p>
{/if}

</td>
<td>&nbsp;</td>
</tr>

</table>



<form action="{$base_url}{$fil_index_gallery}?gid={$gallery_id}" method="post">

<input type="hidden" name="cmd" value="verifyPassword" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">##GALLERY_06##</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>##GALLERY_07##</th>
<td><input type="password" name="password" value="" class="forminput" style="width:100%;" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="submit" class="formbutton" style="width:100%;" value="##GALLERY_08##" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>


{include_template file="index.layout/main.footer.tpl"}


