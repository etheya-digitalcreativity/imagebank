{if $galleries != false}

	<table class="table-thumbnail">
	
	<thead>
	<tr>
	<th>##GALLERY_09##</th>
	</tr>
	</thead>
	
	<tbody>
	<tr>
	<td class="options">
		
		<div class="option-title">##GALLERY_10##: </div>
			
		{if $set_default_thumb_size == "both"}
		<div class="pagination">
			
			<a href="javascript:toggleLarge();updateCookie(3,'large','1|{$set_thumb_per_page}|{$set_gallery_per_page}|{$set_view_thumb_view}');">##GALLERY_11##</a>
			
			<a href="javascript:toggleSmall();updateCookie(3,'small','1|{$set_thumb_per_page}|{$set_gallery_per_page}|{$set_view_thumb_view}');">##GALLERY_12##</a>
			
		</div>
		{/if}
		
		<div class="pagination">
			
			<form action="{$gallery_url}" method="post" onsubmit="return false;">
			<input type="text" size="3" maxlength="3" id="gallery_per_page" name="gallery_per_page" value="{$set_gallery_per_page}" class="page-button" />
			<a href="{$gallery_url}" onclick="javascript:if (document.getElementById('gallery_per_page').value != '0'){literal} { {/literal}updateCookie(2,document.getElementById('gallery_per_page').value,'1|{$set_thumb_per_page}|{$set_gallery_per_page}|{$set_view_thumb_view}'); {literal}} else {{/literal} alert('##GALLERY_13##'); return false; {literal}}{/literal}">##GALLERY_14##</a>
			</form>
			
		</div>			
	
	</td>
	
	</tr>

	<tr>
	<td class="options">

		<div class="option-title">##GALLERY_15##: </div>
			
		<div class="pagination">
		
			{section name="gpages" loop=$gpage_numbers}
			
				{if $gpage_numbers[gpages] == $gpage_current}
				
					<a href="{$gpage_links[gpages]}" class="current">{$gpage_numbers[gpages]}</a>
				
				{else}
				
					<a href="{$gpage_links[gpages]}">{$gpage_numbers[gpages]}</a>
				
				{/if}
			
			{/section}
		
		</div>
	
	</td>
	</tr>

	<tr>
	<td>

		{if $set_default_thumb_size == "both" || $set_default_thumb_size == "large"}
		<div id="gallery-large"{if $set_view_thumb_view == "small" && $set_default_thumb_size == "both"} style="display:none;"{/if}>
			
		{section name="tn" loop=$galleries}
		
			<div class="thumb-lrg" style="width:200px; height:260px;">
			
			<table>
			<thead>
			<tr>
			<th colspan="2"><a href="{$galleries[tn].url}"><img src="{$base_url}{$fil_psg_image_thumbnail}?file={$galleries[tn].key_large_path}" {$galleries[tn].key_large_dims.2} class="thumbnail" alt="{$galleries[tn].title}" title="{$galleries[tn].title}" /></a></th>
			</tr>
			</thead>
			<tbody>
			<tr>
			<th>##GALLERY_16##</th>
			<td><a href="{$galleries[tn].url}" title="{$galleries[tn].title}" class="plain">{$galleries[tn].title|truncate:20:"...":true}</a></td>
			</tr>
			<tr>
			<th>##GALLERY_17##</th>
			<td>{$galleries[tn].date|date_format:"%B %e, %Y"}</td>
			</tr>
			<tr>
			<th>##GALLERY_18##</th>
			<td>{$galleries[tn].image_count} ##GALLERY_19##</td>
			</tr>
			</tbody>
			</table>
			
			</div>
			
		{/section}
				
		<br clear="all" />
				
		</div>
		{/if}
		
		{if $set_default_thumb_size == "both" || $set_default_thumb_size == "small"}
		<div id="gallery-small"{if $set_view_thumb_view == "large" && $set_default_thumb_size == "both"} style="display:none;"{/if}>
			
		{section name="tn" loop=$galleries}
		
			<div class="thumb-sml" style="width:190px; height:170px;">
			
			<table>
			<thead>
			<tr>
			<th colspan="2"><a href="{$galleries[tn].url}"><img src="{$base_url}{$fil_psg_image_thumbnail}?file={$galleries[tn].key_small_path}" {$galleries[tn].key_small_dims.2} class="thumbnail" alt="{$galleries[tn].title}" title="{$galleries[tn].title}" /></a></th>
			</tr>
			</thead>
			<tbody>
			<tr>
			<th>##GALLERY_16##</th>
			<td><a href="{$galleries[tn].url}" title="{$galleries[tn].title}" class="plain">{$galleries[tn].title|truncate:20:"...":true}</a></td>
			</tr>
			<tr>
			<th>##GALLERY_17##</th>
			<td>{$galleries[tn].date|date_format:"%B %e, %Y"}</td>
			</tr>
			<tr>
			<th>##GALLERY_18##</th>
			<td>{$galleries[tn].image_count} ##GALLERY_19##</td>
			</tr>
			</tbody>
			</table>
			
			</div>
			
		{/section}
				
		<br clear="all" />
				
		</div>
		{/if}
		
	</td>
	</tr>
	</tbody>
	
	</table>

{/if}
