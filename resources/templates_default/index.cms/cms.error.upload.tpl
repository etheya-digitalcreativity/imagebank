{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}



<form method="post" action="{$base_url}{$fil_admin_library_import}" onsubmit="javascript:history.go(-1);">

<table class="table-text" border="0" cellpadding="3" cellspacing="0">

<tr>
<th>##CMS_001##</th>

<td>

<h1>##CMS_002##</h1>

<p>{$error_message}</p>

</td>
<td>&nbsp;</td>
</tr>

<tr>
<td>&nbsp;</td>
<td><input type="button" value="##CMS_003##" style="width:100%;" class="forminput" onclick="javascript:history.go(-1);" /></td>
<td>&nbsp;</td>
</tr>

</table>

</form>


{include_template file="index.layout/main.footer.tpl"}