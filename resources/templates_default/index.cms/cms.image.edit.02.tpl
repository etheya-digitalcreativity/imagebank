{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}

<form action="{$base_url}{$fil_index_account}" method="post">

<table class="table-text">

<tr>
<th>##CMS_099##</th>

<td>

<h1>##CMS_100##</h1>

<p>##CMS_101##</p>

</td>
<td>&nbsp;</td>
</tr>

<tr>
<td>&nbsp;</td>
<td>

{if $return}

	<input type="button" onclick="javascript:location.href='{$referer}';" value="##CMS_102##" style="width:100%;" class="forminput" />

{else}

	<input type="submit" value="##CMS_103##" style="width:100%;" class="forminput" />

{/if}

</td>
<td>&nbsp;</td>
</tr>

</table>

</form>

{include_template file="index.layout/main.footer.tpl"}