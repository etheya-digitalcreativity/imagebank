{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}


<form method="post" action="{$base_url}{$fil_index_cms}">

<input type="hidden" name="directory" value="{$directory}" />

<input type="hidden" name="temp_name" value="{$temp_name}" />

<input type="hidden" name="cmd" value="importNewImages" />

<table class="table-text">

<tr>
<th>##CMS_028##</td>

<td>

<h1>##CMS_029##</h1>

<p>##CMS_030##</p>

{if $jpeg_error}

{include_template file="index.snippets/notification.warning.tpl" form_title_text="##CMS_031##<br /><br />##CMS_032##"}

{/if}

</td>
<td>&nbsp;</td>
</tr>

<tr>
<th>&nbsp;</th>
<td><input type="submit" name="submit" value="##CMS_033##" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</table>

<table class="table-import">

	<tr>
	<th>##CMS_034##</th>
	</tr>
	
	{section name="assets" loop=$icon_path}
	
		<tr class="{cycle values='list-one,list-two'}">
		
		<td>
		
		<div class="import-box" style="border-right:1px solid #BBB;">
			
			<input type="hidden" name="image_filename[]" value="{$file_name[assets]}" />
			<input type="hidden" name="image_width[]" value="{$image_width[assets]}" />
			<input type="hidden" name="image_height[]" value="{$image_height[assets]}" />
			
			<span class="bold">##CMS_035##</span> {$file_name[assets]|truncate:30}<br />
			
			<span class="bold">##CMS_036##</span> {$image_width[assets]} x {$image_height[assets]} ##CMS_037##<br /><br />
			
			<table style="width:100%;"><tr><td style="text-align:center;"><a href="javascript:openPop('{$base_url}{$fil_psg_image_comping}?html={$comp_path[assets]}&amp;','{$comp_width[assets]-2}','{$comp_height[assets]-2}','no','no','no','no','no','no','no');"><img src="{$base_url}{$fil_psg_image_thumbnail}?file={$large_path[assets]}&amp;" border="0" width="{$large_width[assets]}" height="{$large_height[assets]}" title="{$file_name[assets]}" alt="{$file_name[assets]}" class="thumbnail" /></a></td></tr></table>
			
			<br />
			
			<input type="checkbox" name="image_model_release[{$smarty.section.assets.index}]" class="forminput" /> <span class="bold">##CMS_038##</span><br />
		
			<input type="checkbox" name="image_property_release[{$smarty.section.assets.index}]" class="forminput" /> <span class="bold">##CMS_039##</span>
		
		</div>
		
		<div class="import-box" style="border-right:1px solid #BBB;">
			
			<input type="hidden" name="iptc_creator[{$smarty.section.assets.index}]" value="{$iptc_creator[assets]}" />
			<input type="hidden" name="iptc_jobtitle[{$smarty.section.assets.index}]" value="{$iptc_jobtitle[assets]}" />
			<input type="hidden" name="iptc_city[{$smarty.section.assets.index}]" value="{$iptc_city[assets]}" />
			<input type="hidden" name="iptc_country[{$smarty.section.assets.index}]" value="{$iptc_country[assets]}" />
			<input type="hidden" name="iptc_credit[{$smarty.section.assets.index}]" value="{$iptc_credit[assets]}" />
			<input type="hidden" name="iptc_source[{$smarty.section.assets.index}]" value="{$iptc_source[assets]}" />
			<input type="hidden" name="iptc_object[{$smarty.section.assets.index}]" value="{$iptc_object[assets]}" />
			<input type="hidden" name="iptc_byline[{$smarty.section.assets.index}]" value="{$iptc_byline[assets]}" />
			
			<span class="bold">##CMS_040##</span><br />
			<input type="text" value="{$iptc_headline[assets]}" placeholder="Image title" name="image_title[]" class="forminput" style="width:100%;" />
			
			<br /><br /><span class="bold">##CMS_041##</span><br />
			<textarea name="image_caption[]" class="forminput" style="width:100%;" rows="4">{$iptc_caption[assets]}</textarea>
			
			<br /><br /><span class="bold">##CMS_042##</span><br />
			<textarea name="image_keywords[]" class="forminput" style="width:100%;" rows="4">{$iptc_keywords[assets]}</textarea>
			
		</div>
		
		<div class="import-box" style="border-right:1px solid #BBB;">
			
			<input type="radio" name="date_type[{$smarty.section.assets.index}]" value="custom" checked="checked" />
			
			<span class="bold">##CMS_043##</span><br />
			{html_select_date field_order="DMY" field_array="date_custom" prefix=$smarty.section.assets.index start_year="1970" reverse_years="TRUE" time=$exif_date[assets]}<br /><br />
		
			{if $exif_date[assets]}
			
				<input type="radio" name="date_type[{$smarty.section.assets.index}]" value="exif" checked="checked" />
				
				<span class="bold">##CMS_044##</span><br />
				{html_select_date field_order="DMY" field_array="date_exif" prefix=$smarty.section.assets.index start_year="1970" reverse_years="TRUE" time=$exif_date[assets]}<br />
				
				<input type="hidden" name="date_exif[{$smarty.section.assets.index}Hour]" value="{$exif_date[assets]|date_format:'%H'}" />
				<input type="hidden" name="date_exif[{$smarty.section.assets.index}Minute]" value="{$exif_date[assets]|date_format:'%M'}" />
				<input type="hidden" name="date_exif[{$smarty.section.assets.index}Second]" value="{$exif_date[assets]|date_format:'%S'}" /><br /><br />
				
			{else}
			
				<span class="bold">##CMS_045##</span><br /><br />
				
				<input type="hidden" name="date_exif[{$smarty.section.assets.index}Day]" value="" />
				<input type="hidden" name="date_exif[{$smarty.section.assets.index}Month]" value="" />
				<input type="hidden" name="date_exif[{$smarty.section.assets.index}Year]" value="" />
				<input type="hidden" name="date_exif[{$smarty.section.assets.index}Hour]" value="" />
				<input type="hidden" name="date_exif[{$smarty.section.assets.index}Minute]" value="" />
				<input type="hidden" name="date_exif[{$smarty.section.assets.index}Second]" value="" />
				
			{/if}
			
			{if $iptc_date[assets]}
			
				<input type="radio" name="date_type[{$smarty.section.assets.index}]" value="iptc" checked="checked" />
				
				<span class="bold">##CMS_046##</span><br />
				{html_select_date field_order="DMY" field_array="date_iptc" prefix=$smarty.section.assets.index start_year="1970" reverse_years="TRUE" time=$iptc_date[assets]}<br /><br />
			
			{else}
				
				<span class="bold">##CMS_047##</span><br /><br />
				
				<input type="hidden" name="date_iptc[{$smarty.section.assets.index}Day]" value="" />
				<input type="hidden" name="date_iptc[{$smarty.section.assets.index}Month]" value="" />
				<input type="hidden" name="date_iptc[{$smarty.section.assets.index}Year]" value="" />
			{/if}
			
			<span class="bold">##CMS_048##</span><br />
			<input type="radio" name="image_rights_type[{$smarty.section.assets.index}]" class="forminput" value="10" checked="checked" /> ##CMS_049##<br />
			<input type="radio" name="image_rights_type[{$smarty.section.assets.index}]" class="forminput" value="11" /> ##CMS_050##
			
			<br /><br /><span class="bold">##CMS_051##</span><br />
			<input type="text" value="{$image_rights_text}" placeholder="##CMS_052##" name="image_rights_text[{$smarty.section.assets.index}]" class="forminput" style="width:100%;" />
			
		</div>
		
		</td>
		
		</tr>
	
	{/section}

</table>

</form>

{include_template file="index.layout/main.footer.tpl"}