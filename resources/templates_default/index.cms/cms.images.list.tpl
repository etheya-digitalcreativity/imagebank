{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}


<table class="table-text">

<tr>
<th>##CMS_104##</th>

<td>

<h1>##CMS_105##</h1>

<p>##CMS_106##</p>

{if $set_image_watermark_text != "" && (($set_image_watermark == "11" && !$ses_psg_userid) || ($set_image_watermark == "12"))}
	
	{include_template file="index.snippets/notification.note.tpl" form_title_text="##CMS_107##"}
	
{/if}


</td>
<td>&nbsp;</td>
</tr>

</table>

<script type="text/javascript" language="javascript">
{literal}

function selectall(formElement)   { setAllSelectState(true,formElement); }
function deselectall(formElement) { setAllSelectState(false,formElement); }
	
function setAllSelectState(state,formElement) {

	var elFrm = document.img_grid; 		
	for (var i = 0; i < elFrm.elements.length; i++) {
		var ourEl = elFrm.elements[i];
		if (ourEl.type == "checkbox")
			if (ourEl.name.indexOf(formElement) != -1)
				ourEl.checked = state;
		}

}

{/literal}
</script>


<table class="table-listbox">

<tr>
<th colspan="7">##CMS_108##</th>
</tr>

	<tr>
	<td class="options" colspan="7">
	
		<div class="option-title">##CMS_109## </div>
			
		<div class="pagination">
			
			<form action="{$fil_index_cms}" method="post" onsubmit="return false;">
			<input type="text" size="3" maxlength="3" id="thumb_per_page" name="thumb_per_page" value="{$set_thumb_per_page}" class="page-button" />
			<a href="{$fil_index_cms}" onclick="javascript:if (document.getElementById('thumb_per_page').value != '0'){literal} { {/literal}updateCookie(1,document.getElementById('thumb_per_page').value,'1|{$set_thumb_per_page}|{$set_gallery_per_page}|{$set_view_thumb_view}'); {literal}} else {{/literal} alert('##CMS_111##'); return false; {literal}}{/literal}">##CMS_110##</a>
			</form>
			
		</div>			
	</td>
	</tr>
	
	<tr>
	<td class="options" colspan="7">

<form method="post" name="img_grid" action="{$base_url}{$fil_index_cms}">


		<div class="option-title">##CMS_112## </div>
			
		<div class="pagination">
		
			{section name="ipages" loop=$ipage_numbers}
			
				{if $ipage_numbers[ipages] == $ipage_current}
				
					<a href="{$ipage_links[ipages]}" class="current">{$ipage_numbers[ipages]}</a>
				
				{else}
				
					<a href="{$ipage_links[ipages]}">{$ipage_numbers[ipages]}</a>
				
				{/if}
			
			{/section}
			
		</div>
	
	</td>
	</tr>



<tr>
	{if $ses_psg_deletor}
		<th><input title="##CMS_113##" type="checkbox" name="toggle" onclick="javascript:document.img_grid.toggle.checked ? selectall('images[]') : deselectall('images[]')" checked="checked" /></th>
	{/if}
	<th style="text-align:center; width:60px;">##CMS_114##</th>
	<th colspan="2">##CMS_115##</th>
	<th>##CMS_1163##</th>	
	<th>##CMS_117##</th>
	<th colspan="2">##CMS_118##</th>
</tr>

{section name="assets" loop=$icon_path}

	<tr class="{cycle values='list-one,list-two'}">
	
	{if $ses_psg_deletor}
	<td style="width:18px;"><input id="radio_row_{$smarty.section.assets.index}" type="checkbox" name="images[]" value="{$image_id[assets]}" checked="checked" /></td>
	{/if}
	
	<td style="text-align:center;">
	{if $image_active[assets]}
	<img src="{$base_url}resources/images/icons/16x16/tick.png" alt="Inactive" width="16" height="16" border="0" title="##CMS_119##" />
	{else}
	<img src="{$base_url}resources/images/icons/16x16/cross.png" alt="Inactive" width="16" height="16" border="0" title="##CMS_120##" />
	{/if}
	</td>
							
	<td style="width:45px; height:45px; text-align:center;"><a href="javascript:imagePreviewWindow('{$base_url}{$fil_psg_image_comping}?html={$comp_path[assets]}&amp;','{$comp_width[assets]}','{$comp_height[assets]}');"><img src="{$base_url}{$fil_psg_image_thumbnail}?file={$icon_path[assets]}&amp;" border="0" width="{$icon_width[assets]}" height="{$icon_height[assets]}" title="{$image_filename[assets]}" alt="{$image_filename[assets]}" class="icon" /></a>
	</td>
	
	<td><a class="cl" href="javascript:imagePreviewWindow('{$base_url}{$fil_psg_image_comping}?html={$comp_path[assets]}&amp;','{$comp_width[assets]}','{$comp_height[assets]}');">{$image_filename[assets]}</a></td>
	
	<td>{$image_title[assets]}</td>
							
	<td>{$image_width[assets]} x {$image_height[assets]} pixels</td>
	
	<td>
	
	<ul class="actionlist">
	{if $ses_psg_image_editor}<li><a href="{$base_url}{$fil_index_cms}?cmd=formEditImage&amp;id={$image_id[assets]}&amp;return=true">##CMS_121##</a></li>{/if}
	</ul>
	
	</td>
	
	<td><img src="{$base_url}resources/themes/{$set_theme}/images/spacer.gif" alt="" width="1" height="38" border="0" /></td>
							
	</tr>

{sectionelse}

	<tr class="{cycle values='list-one,list-two'}">
	
	<td colspan="7">##CMS_122##</td>
	
	</tr>

{/section}	

	
</table>

{if $images_present && $ses_psg_deletor}
	
	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">##CMS_123##</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="list-one">
		<th>##CMS_124##</th>
		<td>
		
		<input type="radio" name="cmd" value="formDeleteFromLibrary" class="forminput" /> ##CMS_125##<br />
		
		</td>
		<td>&nbsp;</td>
	</tr>

	<tr class="list-one">
		<th>&nbsp;</th>
		<td><input type="submit" name="submit" value="##CMS_126##" style="width:100%;" class="formbutton" /></td>
		<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
	</table>

{/if}

</form>


{include_template file="index.layout/main.footer.tpl"}
