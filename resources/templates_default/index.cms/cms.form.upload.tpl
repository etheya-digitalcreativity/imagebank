{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}

	
<table class="table-text">

<tr>
<th>##CMS_063##</th>

<td>

<h1>##CMS_064##</h1>

<p>##CMS_065##</p>

</td>
<td>&nbsp;</td>
</tr>

</table>


<form enctype="multipart/form-data" action="{$base_url}{$fil_index_cms}" method="post">

<input type="hidden" name="cmd" value="processUploadArchive" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">##CMS_066##</th>
</tr>

</thead>

<tbody>

{if $set_gd_upload}

	<tr class="list-one">
	<th>##CMS_067##</th>
	<td>##CMS_068##<br /><br /><input type="file" name="file_archive" /></td>
	<td>&nbsp;</td>
	</tr>
	
{/if}

<tr class="list-one">
<th>&nbsp;</th>
<td><input type="submit" name="submit" value="##CMS_069##" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>

<form enctype="multipart/form-data" action="{$base_url}{$fil_index_cms}" method="post">

<input type="hidden" name="cmd" value="processUploadJPEG" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">##CMS_070##</th>
</tr>

</thead>

<tbody>

{if $set_gd_upload}

	<tr class="list-one">
	<th>##CMS_071##</th>
	<td>##CMS_072##<br /><br /><input type="file" name="img_high" id="get_file[]" /></td>
	<td>&nbsp;</td>
	</tr>
	
{/if}

<tr class="list-two">
<th>##CMS_073##</th>
<td><div id="files_message">##CMS_074##</div><table><tbody id="files_list"></tbody></table></td>
<td>&nbsp;</td>
</tr>

<tr class="list-two">
<th>&nbsp;</th>
<td><input type="submit" name="submit" value="##CMS_075##" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>

<script language="javascript" type="text/javascript">
// <![CDATA[
	{literal}
	
	createMultiSelector();
	
	function createMultiSelector() {
		var multi_selector = new MultiSelector( document.getElementById( 'files_list' ), 5 );
		multi_selector.addElement( document.getElementById( 'get_file[]' ) );
	}

	function updateFileBrowser(url) {
		Effect.toggle('processing','appear',{duration:0.3});
		var ajax = new Ajax.Updater('forms',url,{method:'get',asynchronous:true,onComplete: showResponse});
	}
	
	function showResponse () {
		$('filebrowser').innerHTML = req.responseText;
		Effect.toggle('processing','appear',{duration:0.3});
	}
	{/literal}
// ]]>
</script>

{include_template file="index.layout/main.footer.tpl"}