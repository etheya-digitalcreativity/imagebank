{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}

<table class="table-text">

<tr>
<th>##CMS_004##</th>

<td>

	<h1>##CMS_005##</h1>
	
	<p>##CMS_006##</p>
	
	{include_template file="index.snippets/notification.warning.tpl" form_title_text="##CMS_007##"}

</td>
<td>&nbsp;</td>
</tr>

</table>

{if ($display=="TRUE") }

	<form method="post" name="img_grid" action="{$base_url}{$fil_index_cms}" target="_top" onsubmit="return confirmSubmit('##CMS_008##');">
	
	<table class="table-listbox">

	<tr>
		<th style="padding-left:0px; width:20px;"><input title="Toggle all checkboxes" type="checkbox" name="toggle" onclick="javascript:document.img_grid.toggle.checked ? selectall('images[]') : deselectall('images[]')" checked="checked" /></th>
		<th>##CMS_009##</th>	
		<th>##CMS_010##</th>	
		<th>##CMS_011##</th>	
		<th>##CMS_012##</th>	
		<th colspan="2">##CMS_013##</th>	
	</tr>
	
	{section name=files loop=$filename}
		
		<tr class="{cycle values='list-one,list-two'}">
		<td style="padding-left:0px; width:20px; height:30px;"><input type="checkbox" border="0" name="images[]" value="{$filename[files]}" class="forminput"{if $resizeable[files].0 != "0"} checked="checked"{/if} /></td>
		<td><img src="{$base_url}resources/themes/{$set_theme}/images/icons/image-x-generic.gif" alt="" title="" width="22" height="22" style="vertical-align:middle;" />&nbsp;&nbsp;{$filename[files]|truncate:50}</td>
		<td>{$filesize[files]}kb</td>
		<td>{$filemtime[files]|date_format:"%B %e, %Y"}</td>
		<td>{$width[files]} x {$height[files]}</td>
		<td>{$resizeable[files].1} ##CMS_014##</td>
		<td>{if $resizeable[files].0 == "0"}<ul class="actionlist"><li><a href="javascript:void(0);" onclick="javascript:alert('##CMS_015##');" style="background:#AB1313; color:#FFF; font-weight:bold;">##CMS_016##</a></li></ul>{/if}</td>
		</tr>
	
	{/section}
	
	</table>

	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">##CMS_017##</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>##CMS_018##</th>
	<td>
	
	##CMS_019##
	
	<br /><br />
	
	##CMS_020##
	
	<br /><br />
	
	{if $show_directories}
	
	<select name="directory" onchange="javascript:this.form.directory_name.value=this.value;" style="width:100%;" class="forminput">
	<option value="">##CMS_021##</option>
	<option value="">------------------------------</option>
	{section name="lib" loop=$library}
	<option value="{$library[lib]}">{$library[lib]}</option>
	{/section}
	</select><br />
	
	{/if}
	
	<input type="text" name="directory_name" value="" style="width:100%;" class="forminput" /></td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>##CMS_022##</th>
	<td>
	<input type="radio" name="cmd" value="approveMetaData" checked="checked" /> ##CMS_023##<br />
	<input type="radio" name="cmd" value="formDeleteImageFiles" /> ##CMS_024##
	</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td><input type="submit" value="##CMS_025##" style="width:100%;" class="formbutton" /></td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
	</table>
			
	</form>

	<script type="text/javascript" language="javascript">
	// <![CDATA[
	{literal}
	
	function selectall(formElement)   { setAllSelectState(true,formElement); }
	function deselectall(formElement) { setAllSelectState(false,formElement); }
		
	function setAllSelectState(state,formElement) {
	
		var elFrm = document.img_grid; 		
		for (var i = 0; i < elFrm.elements.length; i++) {
			var ourEl = elFrm.elements[i];
			if (ourEl.type == "checkbox")
				if (ourEl.name.indexOf(formElement) != -1)
					ourEl.checked = state;
			}
	
	}
	
	{/literal}
	// ]]>
	</script>
	
{else}

	<table class="table-form">
	
	<thead>
	
	<tr>
		<th colspan="3">##CMS_026##</th>	
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two'}">
	
		<th>&nbsp;</th>
		
		<td>##CMS_027##</td>
		
		<td>&nbsp;</td>
		
	</tr>
	
	</tbody>
	
	</table>
	
{/if}

{include_template file="index.layout/main.footer.tpl"}