{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}

{if $complete_failure}

	<table class="table-text">
	
	<tr>
	<th>##CMS_053##</th>
	
	<td>
	
		<h1>##CMS_054##</h1>
		
		<p>##CMS_055##</p>
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	</table>
	
{else}
	
	{if $partial_failure}
	
		<table class="table-text">
		
		<tr>
		<th>##CMS_056##</th>
		
		<td>
		
			<h1>##CMS_057##</h1>
			
			<p>##CMS_058##</p>
		
		</td>
		<td>&nbsp;</td>
		</tr>
		
		</table>
		
		<table class="table-listbox">
		
		<tr>
		<th>##CMS_059##</th>
		</tr>
		
		{section name="failed" loop="$failed_images"}
		
		<tr>
		<td style="height:30px;">{$failed_images[failed]}</td>
		</tr>
		
		{/section}
		
		</table>
		
	{else}
	
		<table class="table-text">
		
		<tr>
		<th>##CMS_060##</th>
		
		<td>
		
			<h1>##CMS_061##</h1>
			
			<p>##CMS_062##</p>
		
		</td>
		<td>&nbsp;</td>
		</tr>
		
		</table>
	
	{/if}

	
{/if}

{include_template file="index.layout/main.footer.tpl"}