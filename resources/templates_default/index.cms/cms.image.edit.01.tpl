{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}

<table class="table-text">

<tr>
<th>##CMS_076##</th>

<td>

<h1>##CMS_077##</h1>

<p>##CMS_078##</p>

{if !$image_active}{include_template file="index.snippets/notification.note.tpl" form_title_text="##CMS_079##"}{/if}


</td>
<td>&nbsp;</td>
</tr>

</table>


{include_template file="index.search/image.hud.color.palette.tpl"}


<form action="{$base_url}{$fil_index_cms}" style="margin-top:0; margin-bottom:0" method="post" name="edit_image">

<input type="hidden" name="cmd" value="actionSaveImage" />

<input type="hidden" name="referer" value="{$referer}" />

<input type="hidden" name="return" value="{$return}" />

<input type="hidden" name="image_id" value="{$image_id}" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">##CMS_080##</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>##CMS_081##</th>
<td><a href="javascript:openPop('{$base_url}{$fil_psg_image_comping}?html={$image_comp_encoded}&amp;','{$image_comp_dims.0-2}','{$image_comp_dims.1-2}','no','no','no','no','no','no','no');"><img src="{$base_url}{$fil_psg_image_thumbnail}?file={$image_large_encoded}&amp;" width="{$image_large_dims.0}" height="{$image_large_dims.1}" title="Click here to show this image at full size" alt="##CMS_082##" class="thumbnail" /></a></td>
<td>&nbsp;</td>
</tr>

	
<tr class="{cycle values='list-one,list-two'}">
<th>##CMS_083##</th>
<td><input type="checkbox" name="image_colr_enable" class="forminput" onclick="javascript:Effect.toggle('color_picker','slide',{literal}{duration:0.3}{/literal});" {if $image_colr_enable}checked="checked"{/if} /><br />
	
	<div id="color_picker" style="display:{if $image_colr_enable}inline{else}none{/if};">
	
	{literal}<script language="javascript" type="text/javascript">function openHUD() { Effect.Appear('hud-color',{duration:0.5}); }</script>{/literal}
	
	<table>
	<tr>
	<td style="vertical-align:top;">
		
		<b>##CMS_084##</b>
		
		<div style="width:180px;">
		
		<div style="float:left; width:30px; height:30px; background:#{$image_colr_average_hex}; margin:5px; cursor:pointer;" onclick="javascript:openHUD();"></div>
		<div style="float:left; width:130px; margin:5px;">
		
			<div>##CMS_085##: {$image_colr_average_hex}</div>
			<div>##CMS_086##: {$image_colr_average_dec}</div>
		
		</div>
		
		</div>
		
	</td>
		
	<td style="vertical-align:top;">
		
		<b>##CMS_087##</b>
		
		<div style="width:180px;">
		
		<div style="float:left; width:30px; height:30px; background:#{$image_colr_hex.3}; margin:5px; cursor:pointer;" id="selected-color-form" onclick="javascript:openHUD();"></div>
		<div style="float:left; width:130px; margin:5px;">
		
			<div id="color-form-hex">##CMS_085##: {$image_colr_hex.3}</div>
			<div id="color-form-rgb">##CMS_086##: {$image_colr_dec.3}</div>
		
		</div>
		
		</div>
		
		<input type="hidden" name="image_colr_r" id="image_colr_r" value="{$image_colr_r}" />
		<input type="hidden" name="image_colr_g" id="image_colr_g" value="{$image_colr_g}" />
		<input type="hidden" name="image_colr_b" id="image_colr_b" value="{$image_colr_b}" />

	</td>
	</tr>

	</table>
	
	</div>

</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
	<th>##CMS_088##</th>
	<td><input type="radio" class="forminput" name="date_option" value="Today" checked="checked" />
	
	{html_select_date prefix="toda_" time=$image_mysql_date start_year="-100" display_days=true all_extra='class="forminput"'}
	{html_select_time prefix="toda_" time=$image_mysql_date all_extra='class="forminput"'}
	
	</td>
	<td>&nbsp;</td>
	</tr>

{if $exif_date_unx != ""}

	<tr class="{cycle values='list-one,list-two'}">
	<td><b>##CMS_089##</td>
	<td><input type="radio" class="forminput" name="date_option" value="EXIF" />
	
	{html_select_date prefix="exif_" time=$exif_date_unx start_year="-100" display_days=true all_extra='class="forminput"'}
	{html_select_time prefix="exif_" time=$exif_date_unx all_extra='class="forminput"'}

	</td>
	<td>&nbsp;</td>
	</tr>

{/if}

{if $iptc_date_unx != ""}

	<tr class="{cycle values='list-one,list-two'}">
	<td><b>##CMS_090##</td>
	<td><input type="radio" class="forminput" name="date_option" value="IPTC" />
	
	{html_select_date prefix="iptc_" time=$iptc_date_unx start_year="-100" display_days=true all_extra='class="forminput"'}
	{html_select_time prefix="iptc_" time=$iptc_date_unx all_extra='class="forminput"'}

	</td>
	<td>&nbsp;</td>
	</tr>

{/if}

<tr class="{cycle values='list-one,list-two'}">
<th>##CMS_091##</th>
<td><input type="text" name="image_title" value="{$image_title}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>##CMS_092##</th>
<td><textarea name="image_caption" style="width:100%;" rows="5" class="forminput" cols="10">{$image_caption}</textarea></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>##CMS_093##</th>
<td valign="top"><textarea cols="30" rows="10" name="image_keywords" style="width:100%;" class="forminput">{$image_keywords}</textarea></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>##CMS_094##</th>
<td><input type="text" name="image_width" value="{$image_width}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>##CMS_095##</th>
<td><input type="text" name="image_height" value="{$image_height}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>##CMS_096##</th>
<td><input type="text" name="image_copyright" value="{$image_copyright}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

<table class="table-form">

<thead>

<tr>
<th colspan="3">##CMS_097##</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>{$set_extra_field_01}:</th>
<td><input type="text" name="image_extra_01" value="{$image_extra_01}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>{$set_extra_field_02}:</th>
<td><input type="text" name="image_extra_02" value="{$image_extra_02}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>{$set_extra_field_03}:</th>
<td><input type="text" name="image_extra_03" value="{$image_extra_03}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>{$set_extra_field_04}:</th>
<td><input type="text" name="image_extra_04" value="{$image_extra_04}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>{$set_extra_field_05}:</th>
<td><input type="text" name="image_extra_05" value="{$image_extra_05}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<td>&nbsp;</td>
<td><input type="submit" name="submit" value="##CMS_098##" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>

{include_template file="index.layout/main.footer.tpl"}