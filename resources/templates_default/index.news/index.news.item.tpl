{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}



<table class="table-text">

<tbody>

<tr>

<td colspan="2">

<h1>{$blog_title}</h1>

<p>{$blog_date|date_format:"%A, %B %e, %Y"}</p>

{if $blog_gallery_id > 0}<a href="{$blog_gallery_url}"><img src="{$base_url}{$fil_psg_image_thumbnail}?file={$image_icon_path}" {$image_icon_dims} class="thumbnail" style="margin:0px 5px 5px 0px;" alt="{$gallery_title}" title="{$gallery_title}" align="left" border="0" /></a>{/if}

<p>{$blog_content}</p>

{if $blog_gallery_id > 0}

	<p>##NEWS_01##: <a href="{$blog_gallery_url}">{$gallery_title}</a></p>

{/if}


<p><a href="{$base_url}{$fil_index_news}">##NEWS_02##</a></p>

</td>

<td>&nbsp;</td>

</tr>

<tbody>

</table>

<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="index.layout/main.footer.tpl"}

