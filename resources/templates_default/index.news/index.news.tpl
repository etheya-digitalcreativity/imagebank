{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}


<!-- LOAD THE NEWS HEADLINES PLUGIN -->
{load_pixie name="news_headlines" options="50:5"}


<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="index.layout/main.footer.tpl"}

