{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}


<!-- INCLUDE THE IMAGE DETAIL VIEW TEMPLATE -->
{if $image.basic.width > 480}
	<!-- The image is too wide to fit into the 'wide view' -->
	{include_template file="index.image/view.detail.thin.tpl"}
{else}
	<!-- Load the image template for the wide screen view -->
	{include_template file="index.image/view.detail.wide.tpl"}
{/if}


<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="index.layout/main.footer.tpl"}

