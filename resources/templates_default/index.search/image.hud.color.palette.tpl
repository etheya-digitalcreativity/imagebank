<div style="z-index:2; position:absolute; top:170px; left:20px; width:356px; display:none;" id="hud-color">

<div style="cursor:move;" id="hud-handle"><img src="{$base_url}resources/images/hud/hud.color.palette.title.{if $browser_name == 'ie' && $browser_version <= 6}gif{else}png{/if}" width="356" height="19" border="0" usemap ="#hud-map" alt="##COLOR_HUD_00##" /></div>

{literal}
<script type="text/javascript" language="javascript">
 
 new Draggable('hud-color',{handle:'hud-handle'});

 function closeHUD() {
 Effect.Fade('hud-color', { duration: 0.5 });
 }
 
</script>
{/literal}

<map id="hud-map" name="hud-map">
{literal}<area shape="circle" coords="20,10,6" href="javascript:closeHUD();" alt="##COLOR_HUD_01##" title="##COLOR_HUD_01##" style="cursor:pointer;" />{/literal}
</map>

<div style="background:url('{$base_url}resources/images/hud/hud.color.palette.body.{if $browser_name == 'ie' && $browser_version <= 6}gif{else}png{/if}'); color:#FFF; padding:16px;">

	<table border="0" cellspacing="0" cellpadding="0" align="center" width="324">
	
	<tr>
	
	<td>
	
	<p>##COLOR_HUD_02##:</p>
	
	</td>
	
	</tr>
	
	</table>
	
	<table border="0" cellspacing="0" cellpadding="0" width="324" style="cursor:crosshair;">
	
	<tr>
	<td bgcolor="#F04040"><a href="javascript:setSavedColor(15,4,4);" onmouseover="javascript:setHoverColor(15,4,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#F06040"><a href="javascript:setSavedColor(15,6,4);" onmouseover="javascript:setHoverColor(15,6,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#F08040"><a href="javascript:setSavedColor(15,8,4);" onmouseover="javascript:setHoverColor(15,8,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#F0A040"><a href="javascript:setSavedColor(15,10,4);" onmouseover="javascript:setHoverColor(15,10,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#F0C040"><a href="javascript:setSavedColor(15,12,4);" onmouseover="javascript:setHoverColor(15,12,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#F0E040"><a href="javascript:setSavedColor(15,14,4);" onmouseover="javascript:setHoverColor(15,14,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#F0F040"><a href="javascript:setSavedColor(15,15,4);" onmouseover="javascript:setHoverColor(15,15,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#E0F040"><a href="javascript:setSavedColor(14,15,4);" onmouseover="javascript:setHoverColor(14,15,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#C0F040"><a href="javascript:setSavedColor(12,15,4);" onmouseover="javascript:setHoverColor(12,15,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#A0F040"><a href="javascript:setSavedColor(10,15,4);" onmouseover="javascript:setHoverColor(10,15,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#80F040"><a href="javascript:setSavedColor(8,15,4);" onmouseover="javascript:setHoverColor(8,15,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#60F040"><a href="javascript:setSavedColor(6,15,4);" onmouseover="javascript:setHoverColor(6,15,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40F040"><a href="javascript:setSavedColor(4,15,4);" onmouseover="javascript:setHoverColor(4,15,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40F060"><a href="javascript:setSavedColor(4,15,6);" onmouseover="javascript:setHoverColor(4,15,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40F080"><a href="javascript:setSavedColor(4,15,8);" onmouseover="javascript:setHoverColor(4,15,8);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40F0A0"><a href="javascript:setSavedColor(4,15,10);" onmouseover="javascript:setHoverColor(4,15,10);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40F0C0"><a href="javascript:setSavedColor(4,15,12);" onmouseover="javascript:setHoverColor(4,15,12);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40F0E0"><a href="javascript:setSavedColor(4,15,14);" onmouseover="javascript:setHoverColor(4,15,14);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40F0F0"><a href="javascript:setSavedColor(4,15,15);" onmouseover="javascript:setHoverColor(4,15,15);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40E0F0"><a href="javascript:setSavedColor(4,14,15);" onmouseover="javascript:setHoverColor(4,14,15);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40C0F0"><a href="javascript:setSavedColor(4,12,15);" onmouseover="javascript:setHoverColor(4,12,15);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40A0F0"><a href="javascript:setSavedColor(4,10,15);" onmouseover="javascript:setHoverColor(4,10,15);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#4080F0"><a href="javascript:setSavedColor(4,8,15);" onmouseover="javascript:setHoverColor(4,8,15);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#4060F0"><a href="javascript:setSavedColor(4,6,15);" onmouseover="javascript:setHoverColor(4,6,15);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#4040F0"><a href="javascript:setSavedColor(4,4,15);" onmouseover="javascript:setHoverColor(4,4,15);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#6040F0"><a href="javascript:setSavedColor(6,4,15);" onmouseover="javascript:setHoverColor(6,4,15);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#8040F0"><a href="javascript:setSavedColor(8,4,15);" onmouseover="javascript:setHoverColor(8,4,15);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#A040F0"><a href="javascript:setSavedColor(10,4,15);" onmouseover="javascript:setHoverColor(10,4,15);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#C040F0"><a href="javascript:setSavedColor(12,4,15);" onmouseover="javascript:setHoverColor(12,4,15);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#E040F0"><a href="javascript:setSavedColor(14,4,15);" onmouseover="javascript:setHoverColor(14,4,15);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#F040F0"><a href="javascript:setSavedColor(15,4,15);" onmouseover="javascript:setHoverColor(15,4,15);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#F040E0"><a href="javascript:setSavedColor(15,4,14);" onmouseover="javascript:setHoverColor(15,4,14);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#F040C0"><a href="javascript:setSavedColor(15,4,12);" onmouseover="javascript:setHoverColor(15,4,12);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#F040A0"><a href="javascript:setSavedColor(15,4,10);" onmouseover="javascript:setHoverColor(15,4,10);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#F04080"><a href="javascript:setSavedColor(15,4,8);" onmouseover="javascript:setHoverColor(15,4,8);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#F04060"><a href="javascript:setSavedColor(15,4,6);" onmouseover="javascript:setHoverColor(15,4,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	</tr>
	<tr>
	<td bgcolor="#E04040"><a href="javascript:setSavedColor(14,4,4);" onmouseover="javascript:setHoverColor(14,4,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#E06040"><a href="javascript:setSavedColor(14,6,4);" onmouseover="javascript:setHoverColor(14,6,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#E07040"><a href="javascript:setSavedColor(14,7,4);" onmouseover="javascript:setHoverColor(14,7,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#E09040"><a href="javascript:setSavedColor(14,9,4);" onmouseover="javascript:setHoverColor(14,9,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#E0B040"><a href="javascript:setSavedColor(14,11,4);" onmouseover="javascript:setHoverColor(14,11,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#E0C040"><a href="javascript:setSavedColor(14,12,4);" onmouseover="javascript:setHoverColor(14,12,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#E0E040"><a href="javascript:setSavedColor(14,14,4);" onmouseover="javascript:setHoverColor(14,14,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#C0E040"><a href="javascript:setSavedColor(12,14,4);" onmouseover="javascript:setHoverColor(12,14,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#B0E040"><a href="javascript:setSavedColor(11,14,4);" onmouseover="javascript:setHoverColor(11,14,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#90E040"><a href="javascript:setSavedColor(9,14,4);" onmouseover="javascript:setHoverColor(9,14,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#70E040"><a href="javascript:setSavedColor(7,14,4);" onmouseover="javascript:setHoverColor(7,14,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#60E040"><a href="javascript:setSavedColor(6,14,4);" onmouseover="javascript:setHoverColor(6,14,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40E040"><a href="javascript:setSavedColor(4,14,4);" onmouseover="javascript:setHoverColor(4,14,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40E060"><a href="javascript:setSavedColor(4,14,6);" onmouseover="javascript:setHoverColor(4,14,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40E070"><a href="javascript:setSavedColor(4,14,7);" onmouseover="javascript:setHoverColor(4,14,7);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40E090"><a href="javascript:setSavedColor(4,14,9);" onmouseover="javascript:setHoverColor(4,14,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40E0B0"><a href="javascript:setSavedColor(4,14,11);" onmouseover="javascript:setHoverColor(4,14,11);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40E0C0"><a href="javascript:setSavedColor(4,14,12);" onmouseover="javascript:setHoverColor(4,14,12);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40E0E0"><a href="javascript:setSavedColor(4,14,14);" onmouseover="javascript:setHoverColor(4,14,14);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40C0E0"><a href="javascript:setSavedColor(4,12,14);" onmouseover="javascript:setHoverColor(4,12,14);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40B0E0"><a href="javascript:setSavedColor(4,11,14);" onmouseover="javascript:setHoverColor(4,11,14);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#4090E0"><a href="javascript:setSavedColor(4,9,14);" onmouseover="javascript:setHoverColor(4,9,14);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#4070E0"><a href="javascript:setSavedColor(4,7,14);" onmouseover="javascript:setHoverColor(4,7,14);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#4060E0"><a href="javascript:setSavedColor(4,6,14);" onmouseover="javascript:setHoverColor(4,6,14);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#4040E0"><a href="javascript:setSavedColor(4,4,14);" onmouseover="javascript:setHoverColor(4,4,14);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#6040E0"><a href="javascript:setSavedColor(6,4,14);" onmouseover="javascript:setHoverColor(6,4,14);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#7040E0"><a href="javascript:setSavedColor(7,4,14);" onmouseover="javascript:setHoverColor(7,4,14);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#9040E0"><a href="javascript:setSavedColor(9,4,14);" onmouseover="javascript:setHoverColor(9,4,14);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#B040E0"><a href="javascript:setSavedColor(11,4,14);" onmouseover="javascript:setHoverColor(11,4,14);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#C040E0"><a href="javascript:setSavedColor(12,4,14);" onmouseover="javascript:setHoverColor(12,4,14);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#E040E0"><a href="javascript:setSavedColor(14,4,14);" onmouseover="javascript:setHoverColor(14,4,14);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#E040C0"><a href="javascript:setSavedColor(14,4,12);" onmouseover="javascript:setHoverColor(14,4,12);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#E040B0"><a href="javascript:setSavedColor(14,4,11);" onmouseover="javascript:setHoverColor(14,4,11);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#E04090"><a href="javascript:setSavedColor(14,4,9);" onmouseover="javascript:setHoverColor(14,4,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#E04070"><a href="javascript:setSavedColor(14,4,7);" onmouseover="javascript:setHoverColor(14,4,7);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#E04060"><a href="javascript:setSavedColor(14,4,6);" onmouseover="javascript:setHoverColor(14,4,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	</tr>
	<tr>
	<td bgcolor="#C03030"><a href="javascript:setSavedColor(12,3,3);" onmouseover="javascript:setHoverColor(12,3,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#C05030"><a href="javascript:setSavedColor(12,5,3);" onmouseover="javascript:setHoverColor(12,5,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#C06030"><a href="javascript:setSavedColor(12,6,3);" onmouseover="javascript:setHoverColor(12,6,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#C08030"><a href="javascript:setSavedColor(12,10,3);" onmouseover="javascript:setHoverColor(12,10,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#C09030"><a href="javascript:setSavedColor(12,9,3);" onmouseover="javascript:setHoverColor(12,9,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#C0B030"><a href="javascript:setSavedColor(12,11,3);" onmouseover="javascript:setHoverColor(12,11,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#C0C030"><a href="javascript:setSavedColor(12,12,3);" onmouseover="javascript:setHoverColor(12,12,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#B0C030"><a href="javascript:setSavedColor(11,12,3);" onmouseover="javascript:setHoverColor(11,12,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#90C030"><a href="javascript:setSavedColor(9,12,3);" onmouseover="javascript:setHoverColor(9,12,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#80C030"><a href="javascript:setSavedColor(8,12,3);" onmouseover="javascript:setHoverColor(8,12,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#60C030"><a href="javascript:setSavedColor(6,12,3);" onmouseover="javascript:setHoverColor(6,12,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#50C030"><a href="javascript:setSavedColor(5,12,3);" onmouseover="javascript:setHoverColor(5,12,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#30C030"><a href="javascript:setSavedColor(3,12,3);" onmouseover="javascript:setHoverColor(3,12,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#30C050"><a href="javascript:setSavedColor(3,12,5);" onmouseover="javascript:setHoverColor(3,12,5);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#30C060"><a href="javascript:setSavedColor(3,12,6);" onmouseover="javascript:setHoverColor(3,12,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#30C080"><a href="javascript:setSavedColor(3,12,10);" onmouseover="javascript:setHoverColor(3,12,10);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#30C090"><a href="javascript:setSavedColor(3,12,9);" onmouseover="javascript:setHoverColor(3,12,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#30C0B0"><a href="javascript:setSavedColor(3,12,11);" onmouseover="javascript:setHoverColor(3,12,11);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#30C0C0"><a href="javascript:setSavedColor(3,12,12);" onmouseover="javascript:setHoverColor(3,12,12);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#30B0C0"><a href="javascript:setSavedColor(3,11,12);" onmouseover="javascript:setHoverColor(3,11,12);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#3090C0"><a href="javascript:setSavedColor(3,9,12);" onmouseover="javascript:setHoverColor(3,9,12);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#3080C0"><a href="javascript:setSavedColor(3,8,12);" onmouseover="javascript:setHoverColor(3,8,12);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#3060C0"><a href="javascript:setSavedColor(3,6,12);" onmouseover="javascript:setHoverColor(3,6,12);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#3050C0"><a href="javascript:setSavedColor(3,5,12);" onmouseover="javascript:setHoverColor(3,5,12);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#3030C0"><a href="javascript:setSavedColor(3,3,12);" onmouseover="javascript:setHoverColor(3,3,12);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#5030C0"><a href="javascript:setSavedColor(5,3,12);" onmouseover="javascript:setHoverColor(5,3,12);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#6030C0"><a href="javascript:setSavedColor(6,3,12);" onmouseover="javascript:setHoverColor(6,3,12);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#8030C0"><a href="javascript:setSavedColor(8,3,12);" onmouseover="javascript:setHoverColor(8,3,12);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#9030C0"><a href="javascript:setSavedColor(9,3,12);" onmouseover="javascript:setHoverColor(9,3,12);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#B030C0"><a href="javascript:setSavedColor(11,3,12);" onmouseover="javascript:setHoverColor(11,3,12);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#C030C0"><a href="javascript:setSavedColor(12,3,12);" onmouseover="javascript:setHoverColor(12,3,12);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#C030B0"><a href="javascript:setSavedColor(12,3,11);" onmouseover="javascript:setHoverColor(12,3,11);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#C03090"><a href="javascript:setSavedColor(12,3,9);" onmouseover="javascript:setHoverColor(12,3,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#C03080"><a href="javascript:setSavedColor(12,3,8);" onmouseover="javascript:setHoverColor(12,3,8);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#C03060"><a href="javascript:setSavedColor(12,3,6);" onmouseover="javascript:setHoverColor(12,3,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#C03050"><a href="javascript:setSavedColor(12,3,5);" onmouseover="javascript:setHoverColor(12,3,5);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	</tr>
	<tr>
	<td bgcolor="#B03030"><a href="javascript:setSavedColor(11,3,3);" onmouseover="javascript:setHoverColor(11,3,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#B04030"><a href="javascript:setSavedColor(11,4,3);" onmouseover="javascript:setHoverColor(11,4,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#B05030"><a href="javascript:setSavedColor(11,5,3);" onmouseover="javascript:setHoverColor(11,5,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#B07030"><a href="javascript:setSavedColor(11,7,3);" onmouseover="javascript:setHoverColor(11,7,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#B08030"><a href="javascript:setSavedColor(11,8,3);" onmouseover="javascript:setHoverColor(11,8,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#B09030"><a href="javascript:setSavedColor(11,9,3);" onmouseover="javascript:setHoverColor(11,9,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#B0B030"><a href="javascript:setSavedColor(11,11,3);" onmouseover="javascript:setHoverColor(11,11,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#90B030"><a href="javascript:setSavedColor(9,11,3);" onmouseover="javascript:setHoverColor(9,11,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#80B030"><a href="javascript:setSavedColor(8,11,3);" onmouseover="javascript:setHoverColor(8,11,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#70B030"><a href="javascript:setSavedColor(7,11,3);" onmouseover="javascript:setHoverColor(7,11,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#50B030"><a href="javascript:setSavedColor(5,11,3);" onmouseover="javascript:setHoverColor(5,11,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#40B030"><a href="javascript:setSavedColor(4,11,3);" onmouseover="javascript:setHoverColor(4,11,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#30B030"><a href="javascript:setSavedColor(3,11,3);" onmouseover="javascript:setHoverColor(3,11,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#30B040"><a href="javascript:setSavedColor(3,11,4);" onmouseover="javascript:setHoverColor(3,11,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#30B050"><a href="javascript:setSavedColor(3,11,5);" onmouseover="javascript:setHoverColor(3,11,5);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#30B070"><a href="javascript:setSavedColor(3,11,7);" onmouseover="javascript:setHoverColor(3,11,7);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#30B080"><a href="javascript:setSavedColor(3,11,8);" onmouseover="javascript:setHoverColor(3,11,8);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#30B090"><a href="javascript:setSavedColor(3,11,9);" onmouseover="javascript:setHoverColor(3,11,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#30B0B0"><a href="javascript:setSavedColor(3,11,11);" onmouseover="javascript:setHoverColor(3,11,11);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#3090B0"><a href="javascript:setSavedColor(3,9,11);" onmouseover="javascript:setHoverColor(3,9,11);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#3080B0"><a href="javascript:setSavedColor(3,8,11);" onmouseover="javascript:setHoverColor(3,8,11);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#3070B0"><a href="javascript:setSavedColor(3,7,11);" onmouseover="javascript:setHoverColor(3,7,11);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#3050B0"><a href="javascript:setSavedColor(3,5,11);" onmouseover="javascript:setHoverColor(3,5,11);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#3040B0"><a href="javascript:setSavedColor(3,4,11);" onmouseover="javascript:setHoverColor(3,4,11);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#3030B0"><a href="javascript:setSavedColor(3,3,11);" onmouseover="javascript:setHoverColor(3,3,11);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#4030B0"><a href="javascript:setSavedColor(4,3,11);" onmouseover="javascript:setHoverColor(4,3,11);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#5030B0"><a href="javascript:setSavedColor(5,3,11);" onmouseover="javascript:setHoverColor(5,3,11);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#7030B0"><a href="javascript:setSavedColor(7,3,11);" onmouseover="javascript:setHoverColor(7,3,11);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#8030B0"><a href="javascript:setSavedColor(8,3,11);" onmouseover="javascript:setHoverColor(8,3,11);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#9030B0"><a href="javascript:setSavedColor(9,3,11);" onmouseover="javascript:setHoverColor(9,3,11);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#B030B0"><a href="javascript:setSavedColor(11,3,11);" onmouseover="javascript:setHoverColor(11,3,11);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#B03090"><a href="javascript:setSavedColor(11,3,9);" onmouseover="javascript:setHoverColor(11,3,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#B03080"><a href="javascript:setSavedColor(11,3,8);" onmouseover="javascript:setHoverColor(11,3,8);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#B03070"><a href="javascript:setSavedColor(11,3,7);" onmouseover="javascript:setHoverColor(11,3,7);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#B03050"><a href="javascript:setSavedColor(11,3,5);" onmouseover="javascript:setHoverColor(11,3,5);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#B03040"><a href="javascript:setSavedColor(11,3,4);" onmouseover="javascript:setHoverColor(11,3,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	</tr>
	<tr>
	<td bgcolor="#902020"><a href="javascript:setSavedColor(9,2,2);" onmouseover="javascript:setHoverColor(9,2,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#904020"><a href="javascript:setSavedColor(9,4,2);" onmouseover="javascript:setHoverColor(9,4,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#905020"><a href="javascript:setSavedColor(9,5,2);" onmouseover="javascript:setHoverColor(9,5,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#906020"><a href="javascript:setSavedColor(9,6,2);" onmouseover="javascript:setHoverColor(9,6,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#907020"><a href="javascript:setSavedColor(9,7,2);" onmouseover="javascript:setHoverColor(9,7,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#908020"><a href="javascript:setSavedColor(9,8,2);" onmouseover="javascript:setHoverColor(9,8,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#909020"><a href="javascript:setSavedColor(9,9,2);" onmouseover="javascript:setHoverColor(9,9,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#809020"><a href="javascript:setSavedColor(8,9,2);" onmouseover="javascript:setHoverColor(8,9,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#709020"><a href="javascript:setSavedColor(7,9,2);" onmouseover="javascript:setHoverColor(7,9,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#609020"><a href="javascript:setSavedColor(6,9,2);" onmouseover="javascript:setHoverColor(6,9,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#509020"><a href="javascript:setSavedColor(5,9,2);" onmouseover="javascript:setHoverColor(5,9,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#409020"><a href="javascript:setSavedColor(4,9,2);" onmouseover="javascript:setHoverColor(4,9,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#209020"><a href="javascript:setSavedColor(2,9,2);" onmouseover="javascript:setHoverColor(2,9,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#209040"><a href="javascript:setSavedColor(2,9,4);" onmouseover="javascript:setHoverColor(2,9,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#209050"><a href="javascript:setSavedColor(2,9,5);" onmouseover="javascript:setHoverColor(2,9,5);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#209060"><a href="javascript:setSavedColor(2,9,6);" onmouseover="javascript:setHoverColor(2,9,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#209070"><a href="javascript:setSavedColor(2,9,7);" onmouseover="javascript:setHoverColor(2,9,7);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#209080"><a href="javascript:setSavedColor(2,9,8);" onmouseover="javascript:setHoverColor(2,9,8);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#209090"><a href="javascript:setSavedColor(2,9,9);" onmouseover="javascript:setHoverColor(2,9,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#208090"><a href="javascript:setSavedColor(2,10,9);" onmouseover="javascript:setHoverColor(2,10,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#207090"><a href="javascript:setSavedColor(2,7,9);" onmouseover="javascript:setHoverColor(2,7,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#206090"><a href="javascript:setSavedColor(2,6,9);" onmouseover="javascript:setHoverColor(2,6,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#205090"><a href="javascript:setSavedColor(2,5,9);" onmouseover="javascript:setHoverColor(2,5,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#204090"><a href="javascript:setSavedColor(2,4,9);" onmouseover="javascript:setHoverColor(2,4,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#202090"><a href="javascript:setSavedColor(2,2,9);" onmouseover="javascript:setHoverColor(2,2,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#402090"><a href="javascript:setSavedColor(4,2,9);" onmouseover="javascript:setHoverColor(4,2,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#502090"><a href="javascript:setSavedColor(5,2,9);" onmouseover="javascript:setHoverColor(5,2,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#602090"><a href="javascript:setSavedColor(6,2,9);" onmouseover="javascript:setHoverColor(6,2,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#702090"><a href="javascript:setSavedColor(7,2,9);" onmouseover="javascript:setHoverColor(7,2,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#802090"><a href="javascript:setSavedColor(8,2,9);" onmouseover="javascript:setHoverColor(8,2,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#902090"><a href="javascript:setSavedColor(9,2,9);" onmouseover="javascript:setHoverColor(9,2,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#902080"><a href="javascript:setSavedColor(9,2,10);" onmouseover="javascript:setHoverColor(9,2,10);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#902070"><a href="javascript:setSavedColor(9,2,7);" onmouseover="javascript:setHoverColor(9,2,7);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#902060"><a href="javascript:setSavedColor(9,2,6);" onmouseover="javascript:setHoverColor(9,2,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#902050"><a href="javascript:setSavedColor(9,2,5);" onmouseover="javascript:setHoverColor(9,2,5);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#902040"><a href="javascript:setSavedColor(9,2,4);" onmouseover="javascript:setHoverColor(9,2,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	</tr>
	<tr>
	<td bgcolor="#802020"><a href="javascript:setSavedColor(8,2,2);" onmouseover="javascript:setHoverColor(8,2,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#803020"><a href="javascript:setSavedColor(8,3,2);" onmouseover="javascript:setHoverColor(8,3,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#804020"><a href="javascript:setSavedColor(8,4,2);" onmouseover="javascript:setHoverColor(8,4,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#805020"><a href="javascript:setSavedColor(8,5,2);" onmouseover="javascript:setHoverColor(8,5,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#806020"><a href="javascript:setSavedColor(8,6,2);" onmouseover="javascript:setHoverColor(8,6,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#807020"><a href="javascript:setSavedColor(8,7,2);" onmouseover="javascript:setHoverColor(8,7,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#808020"><a href="javascript:setSavedColor(8,8,2);" onmouseover="javascript:setHoverColor(8,8,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#708020"><a href="javascript:setSavedColor(7,8,2);" onmouseover="javascript:setHoverColor(7,8,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#608020"><a href="javascript:setSavedColor(6,8,2);" onmouseover="javascript:setHoverColor(6,8,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#508020"><a href="javascript:setSavedColor(5,8,2);" onmouseover="javascript:setHoverColor(5,8,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#408020"><a href="javascript:setSavedColor(4,8,2);" onmouseover="javascript:setHoverColor(4,8,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#308020"><a href="javascript:setSavedColor(3,8,2);" onmouseover="javascript:setHoverColor(3,8,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#208020"><a href="javascript:setSavedColor(2,10,2);" onmouseover="javascript:setHoverColor(2,10,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#208030"><a href="javascript:setSavedColor(2,10,3);" onmouseover="javascript:setHoverColor(2,10,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#208040"><a href="javascript:setSavedColor(2,10,4);" onmouseover="javascript:setHoverColor(2,10,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#208050"><a href="javascript:setSavedColor(2,10,5);" onmouseover="javascript:setHoverColor(2,10,5);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#208060"><a href="javascript:setSavedColor(2,10,6);" onmouseover="javascript:setHoverColor(2,10,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#208070"><a href="javascript:setSavedColor(2,10,7);" onmouseover="javascript:setHoverColor(2,10,7);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#208080"><a href="javascript:setSavedColor(2,10,8);" onmouseover="javascript:setHoverColor(2,10,8);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#207080"><a href="javascript:setSavedColor(2,7,8);" onmouseover="javascript:setHoverColor(2,7,8);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#206080"><a href="javascript:setSavedColor(2,6,8);" onmouseover="javascript:setHoverColor(2,6,8);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#205080"><a href="javascript:setSavedColor(2,5,8);" onmouseover="javascript:setHoverColor(2,5,8);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#204080"><a href="javascript:setSavedColor(2,4,8);" onmouseover="javascript:setHoverColor(2,4,8);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#203080"><a href="javascript:setSavedColor(2,3,8);" onmouseover="javascript:setHoverColor(2,3,8);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#202080"><a href="javascript:setSavedColor(2,2,10);" onmouseover="javascript:setHoverColor(2,2,10);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#302080"><a href="javascript:setSavedColor(3,2,10);" onmouseover="javascript:setHoverColor(3,2,10);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#402080"><a href="javascript:setSavedColor(4,2,10);" onmouseover="javascript:setHoverColor(4,2,10);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#502080"><a href="javascript:setSavedColor(5,2,10);" onmouseover="javascript:setHoverColor(5,2,10);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#602080"><a href="javascript:setSavedColor(6,2,10);" onmouseover="javascript:setHoverColor(6,2,10);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#702080"><a href="javascript:setSavedColor(7,2,10);" onmouseover="javascript:setHoverColor(7,2,10);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#802080"><a href="javascript:setSavedColor(8,2,10);" onmouseover="javascript:setHoverColor(8,2,10);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#802070"><a href="javascript:setSavedColor(8,2,7);" onmouseover="javascript:setHoverColor(8,2,7);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#802060"><a href="javascript:setSavedColor(8,2,6);" onmouseover="javascript:setHoverColor(8,2,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#802050"><a href="javascript:setSavedColor(8,2,5);" onmouseover="javascript:setHoverColor(8,2,5);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#802040"><a href="javascript:setSavedColor(8,2,4);" onmouseover="javascript:setHoverColor(8,2,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#802030"><a href="javascript:setSavedColor(8,2,3);" onmouseover="javascript:setHoverColor(8,2,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	</tr>
	<tr>
	<td bgcolor="#601010"><a href="javascript:setSavedColor(6,1,1);" onmouseover="javascript:setHoverColor(6,1,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#602010"><a href="javascript:setSavedColor(6,2,1);" onmouseover="javascript:setHoverColor(6,2,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#603010"><a href="javascript:setSavedColor(6,3,1);" onmouseover="javascript:setHoverColor(6,3,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#604010"><a href="javascript:setSavedColor(6,4,1);" onmouseover="javascript:setHoverColor(6,4,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#604010"><a href="javascript:setSavedColor(6,4,1);" onmouseover="javascript:setHoverColor(6,4,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#605010"><a href="javascript:setSavedColor(6,5,1);" onmouseover="javascript:setHoverColor(6,5,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#606010"><a href="javascript:setSavedColor(6,6,1);" onmouseover="javascript:setHoverColor(6,6,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#506010"><a href="javascript:setSavedColor(5,6,1);" onmouseover="javascript:setHoverColor(5,6,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#406010"><a href="javascript:setSavedColor(4,6,1);" onmouseover="javascript:setHoverColor(4,6,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#406010"><a href="javascript:setSavedColor(4,6,1);" onmouseover="javascript:setHoverColor(4,6,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#306010"><a href="javascript:setSavedColor(3,6,1);" onmouseover="javascript:setHoverColor(3,6,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#206010"><a href="javascript:setSavedColor(2,6,1);" onmouseover="javascript:setHoverColor(2,6,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#106010"><a href="javascript:setSavedColor(1,6,1);" onmouseover="javascript:setHoverColor(1,6,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#106020"><a href="javascript:setSavedColor(1,6,2);" onmouseover="javascript:setHoverColor(1,6,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#106030"><a href="javascript:setSavedColor(1,6,3);" onmouseover="javascript:setHoverColor(1,6,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#106040"><a href="javascript:setSavedColor(1,6,4);" onmouseover="javascript:setHoverColor(1,6,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#106040"><a href="javascript:setSavedColor(1,6,4);" onmouseover="javascript:setHoverColor(1,6,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#106050"><a href="javascript:setSavedColor(1,6,5);" onmouseover="javascript:setHoverColor(1,6,5);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#106060"><a href="javascript:setSavedColor(1,6,6);" onmouseover="javascript:setHoverColor(1,6,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#105060"><a href="javascript:setSavedColor(1,5,6);" onmouseover="javascript:setHoverColor(1,5,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#104060"><a href="javascript:setSavedColor(1,4,6);" onmouseover="javascript:setHoverColor(1,4,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#104060"><a href="javascript:setSavedColor(1,4,6);" onmouseover="javascript:setHoverColor(1,4,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#103060"><a href="javascript:setSavedColor(1,3,6);" onmouseover="javascript:setHoverColor(1,3,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#102060"><a href="javascript:setSavedColor(1,2,6);" onmouseover="javascript:setHoverColor(1,2,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#101060"><a href="javascript:setSavedColor(1,1,6);" onmouseover="javascript:setHoverColor(1,1,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#201060"><a href="javascript:setSavedColor(2,1,6);" onmouseover="javascript:setHoverColor(2,1,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#301060"><a href="javascript:setSavedColor(3,1,6);" onmouseover="javascript:setHoverColor(3,1,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#401060"><a href="javascript:setSavedColor(4,1,6);" onmouseover="javascript:setHoverColor(4,1,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#401060"><a href="javascript:setSavedColor(4,1,6);" onmouseover="javascript:setHoverColor(4,1,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#501060"><a href="javascript:setSavedColor(5,1,6);" onmouseover="javascript:setHoverColor(5,1,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#601060"><a href="javascript:setSavedColor(6,1,6);" onmouseover="javascript:setHoverColor(6,1,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#601050"><a href="javascript:setSavedColor(6,1,5);" onmouseover="javascript:setHoverColor(6,1,5);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#601040"><a href="javascript:setSavedColor(6,1,4);" onmouseover="javascript:setHoverColor(6,1,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#601040"><a href="javascript:setSavedColor(6,1,4);" onmouseover="javascript:setHoverColor(6,1,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#601030"><a href="javascript:setSavedColor(6,1,3);" onmouseover="javascript:setHoverColor(6,1,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#601020"><a href="javascript:setSavedColor(6,1,2);" onmouseover="javascript:setHoverColor(6,1,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	</tr>
	<tr>
	<td bgcolor="#401010"><a href="javascript:setSavedColor(4,1,1);" onmouseover="javascript:setHoverColor(4,1,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#402010"><a href="javascript:setSavedColor(4,2,1);" onmouseover="javascript:setHoverColor(4,2,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#402010"><a href="javascript:setSavedColor(4,2,1);" onmouseover="javascript:setHoverColor(4,2,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#403010"><a href="javascript:setSavedColor(4,3,1);" onmouseover="javascript:setHoverColor(4,3,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#403010"><a href="javascript:setSavedColor(4,3,1);" onmouseover="javascript:setHoverColor(4,3,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#404010"><a href="javascript:setSavedColor(4,4,1);" onmouseover="javascript:setHoverColor(4,4,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#404010"><a href="javascript:setSavedColor(4,4,1);" onmouseover="javascript:setHoverColor(4,4,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#404010"><a href="javascript:setSavedColor(4,4,1);" onmouseover="javascript:setHoverColor(4,4,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#304010"><a href="javascript:setSavedColor(3,4,1);" onmouseover="javascript:setHoverColor(3,4,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#304010"><a href="javascript:setSavedColor(3,4,1);" onmouseover="javascript:setHoverColor(3,4,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#204010"><a href="javascript:setSavedColor(2,4,1);" onmouseover="javascript:setHoverColor(2,4,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#204010"><a href="javascript:setSavedColor(2,4,1);" onmouseover="javascript:setHoverColor(2,4,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#104010"><a href="javascript:setSavedColor(1,4,1);" onmouseover="javascript:setHoverColor(1,4,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#104020"><a href="javascript:setSavedColor(1,4,2);" onmouseover="javascript:setHoverColor(1,4,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#104020"><a href="javascript:setSavedColor(1,4,2);" onmouseover="javascript:setHoverColor(1,4,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#104030"><a href="javascript:setSavedColor(1,4,3);" onmouseover="javascript:setHoverColor(1,4,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#104030"><a href="javascript:setSavedColor(1,4,3);" onmouseover="javascript:setHoverColor(1,4,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#104040"><a href="javascript:setSavedColor(1,4,4);" onmouseover="javascript:setHoverColor(1,4,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#104040"><a href="javascript:setSavedColor(1,4,4);" onmouseover="javascript:setHoverColor(1,4,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#104040"><a href="javascript:setSavedColor(1,4,4);" onmouseover="javascript:setHoverColor(1,4,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#103040"><a href="javascript:setSavedColor(1,3,4);" onmouseover="javascript:setHoverColor(1,3,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#103040"><a href="javascript:setSavedColor(1,3,4);" onmouseover="javascript:setHoverColor(1,3,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#102040"><a href="javascript:setSavedColor(1,2,4);" onmouseover="javascript:setHoverColor(1,2,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#102040"><a href="javascript:setSavedColor(1,2,4);" onmouseover="javascript:setHoverColor(1,2,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#101040"><a href="javascript:setSavedColor(1,1,4);" onmouseover="javascript:setHoverColor(1,1,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#201040"><a href="javascript:setSavedColor(2,1,4);" onmouseover="javascript:setHoverColor(2,1,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#201040"><a href="javascript:setSavedColor(2,1,4);" onmouseover="javascript:setHoverColor(2,1,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#301040"><a href="javascript:setSavedColor(3,1,4);" onmouseover="javascript:setHoverColor(3,1,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#301040"><a href="javascript:setSavedColor(3,1,4);" onmouseover="javascript:setHoverColor(3,1,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#401040"><a href="javascript:setSavedColor(4,1,4);" onmouseover="javascript:setHoverColor(4,1,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#401040"><a href="javascript:setSavedColor(4,1,4);" onmouseover="javascript:setHoverColor(4,1,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#401040"><a href="javascript:setSavedColor(4,1,4);" onmouseover="javascript:setHoverColor(4,1,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#401030"><a href="javascript:setSavedColor(4,1,3);" onmouseover="javascript:setHoverColor(4,1,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#401030"><a href="javascript:setSavedColor(4,1,3);" onmouseover="javascript:setHoverColor(4,1,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#401020"><a href="javascript:setSavedColor(4,1,2);" onmouseover="javascript:setHoverColor(4,1,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#401020"><a href="javascript:setSavedColor(4,1,2);" onmouseover="javascript:setHoverColor(4,1,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	</tr>
	<tr>
	<td bgcolor="#300000"><a href="javascript:setSavedColor(3,0,0);" onmouseover="javascript:setHoverColor(3,0,0);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#301000"><a href="javascript:setSavedColor(3,1,0);" onmouseover="javascript:setHoverColor(3,1,0);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#301000"><a href="javascript:setSavedColor(3,1,0);" onmouseover="javascript:setHoverColor(3,1,0);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#302000"><a href="javascript:setSavedColor(3,2,0);" onmouseover="javascript:setHoverColor(3,2,0);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#302000"><a href="javascript:setSavedColor(3,2,0);" onmouseover="javascript:setHoverColor(3,2,0);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#302000"><a href="javascript:setSavedColor(3,2,0);" onmouseover="javascript:setHoverColor(3,2,0);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#303000"><a href="javascript:setSavedColor(3,3,0);" onmouseover="javascript:setHoverColor(3,3,0);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#203000"><a href="javascript:setSavedColor(2,3,0);" onmouseover="javascript:setHoverColor(2,3,0);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#203000"><a href="javascript:setSavedColor(2,3,0);" onmouseover="javascript:setHoverColor(2,3,0);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#203000"><a href="javascript:setSavedColor(2,3,0);" onmouseover="javascript:setHoverColor(2,3,0);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#103000"><a href="javascript:setSavedColor(1,3,0);" onmouseover="javascript:setHoverColor(1,3,0);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#103000"><a href="javascript:setSavedColor(1,3,0);" onmouseover="javascript:setHoverColor(1,3,0);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#003000"><a href="javascript:setSavedColor(0,3,0);" onmouseover="javascript:setHoverColor(0,3,0);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#003010"><a href="javascript:setSavedColor(0,3,1);" onmouseover="javascript:setHoverColor(0,3,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#003010"><a href="javascript:setSavedColor(0,3,1);" onmouseover="javascript:setHoverColor(0,3,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#003020"><a href="javascript:setSavedColor(0,3,2);" onmouseover="javascript:setHoverColor(0,3,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#003020"><a href="javascript:setSavedColor(0,3,2);" onmouseover="javascript:setHoverColor(0,3,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#003020"><a href="javascript:setSavedColor(0,3,2);" onmouseover="javascript:setHoverColor(0,3,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#003030"><a href="javascript:setSavedColor(0,3,3);" onmouseover="javascript:setHoverColor(0,3,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#002030"><a href="javascript:setSavedColor(0,2,3);" onmouseover="javascript:setHoverColor(0,2,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#002030"><a href="javascript:setSavedColor(0,2,3);" onmouseover="javascript:setHoverColor(0,2,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#002030"><a href="javascript:setSavedColor(0,2,3);" onmouseover="javascript:setHoverColor(0,2,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#001030"><a href="javascript:setSavedColor(0,1,3);" onmouseover="javascript:setHoverColor(0,1,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#001030"><a href="javascript:setSavedColor(0,1,3);" onmouseover="javascript:setHoverColor(0,1,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#000030"><a href="javascript:setSavedColor(0,0,3);" onmouseover="javascript:setHoverColor(0,0,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#100030"><a href="javascript:setSavedColor(1,0,3);" onmouseover="javascript:setHoverColor(1,0,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#100030"><a href="javascript:setSavedColor(1,0,3);" onmouseover="javascript:setHoverColor(1,0,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#200030"><a href="javascript:setSavedColor(2,0,3);" onmouseover="javascript:setHoverColor(2,0,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#200030"><a href="javascript:setSavedColor(2,0,3);" onmouseover="javascript:setHoverColor(2,0,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#200030"><a href="javascript:setSavedColor(2,0,3);" onmouseover="javascript:setHoverColor(2,0,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#300030"><a href="javascript:setSavedColor(3,0,3);" onmouseover="javascript:setHoverColor(3,0,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#300020"><a href="javascript:setSavedColor(3,0,2);" onmouseover="javascript:setHoverColor(3,0,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#300020"><a href="javascript:setSavedColor(3,0,2);" onmouseover="javascript:setHoverColor(3,0,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#300020"><a href="javascript:setSavedColor(3,0,2);" onmouseover="javascript:setHoverColor(3,0,2);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#300010"><a href="javascript:setSavedColor(3,0,1);" onmouseover="javascript:setHoverColor(3,0,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	<td bgcolor="#300010"><a href="javascript:setSavedColor(3,0,1);" onmouseover="javascript:setHoverColor(3,0,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="9" height="9" /></a></td>
	</tr>
	
	<tr>
	<td colspan="36" align="left">
	
	<table border="0" cellspacing="0" cellpadding="0" align="center" width="324">
		<tr>
			<td bgcolor="#000000"><a href="javascript:setSavedColor(0,0,0);" onmouseover="javascript:setHoverColor(0,0,0);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="29" height="19" /></a></td>
			<td bgcolor="#101010"><a href="javascript:setSavedColor(1,1,1);" onmouseover="javascript:setHoverColor(1,1,1);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="29" height="19" /></a></td>
			<td bgcolor="#303030"><a href="javascript:setSavedColor(3,3,3);" onmouseover="javascript:setHoverColor(3,3,3);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="29" height="19" /></a></td>
			<td bgcolor="#404040"><a href="javascript:setSavedColor(4,4,4);" onmouseover="javascript:setHoverColor(4,4,4);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="29" height="19" /></a></td>
			<td bgcolor="#606060"><a href="javascript:setSavedColor(6,6,6);" onmouseover="javascript:setHoverColor(6,6,6);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="29" height="19" /></a></td>
			<td bgcolor="#808080"><a href="javascript:setSavedColor(8,8,8);" onmouseover="javascript:setHoverColor(8,8,8);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="29" height="19" /></a></td>
			<td bgcolor="#909090"><a href="javascript:setSavedColor(9,9,9);" onmouseover="javascript:setHoverColor(9,9,9);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="29" height="19" /></a></td>
			<td bgcolor="#B0B0B0"><a href="javascript:setSavedColor(11,11,11);" onmouseover="javascript:setHoverColor(11,11,11);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="29" height="19" /></a></td>
			<td bgcolor="#C0C0C0"><a href="javascript:setSavedColor(12,12,12);" onmouseover="javascript:setHoverColor(12,12,12);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="29" height="19" /></a></td>
			<td bgcolor="#E0E0E0"><a href="javascript:setSavedColor(14,14,14);" onmouseover="javascript:setHoverColor(14,14,14);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="29" height="19" /></a></td>
			<td bgcolor="#F0F0F0"><a href="javascript:setSavedColor(15,15,15);" onmouseover="javascript:setHoverColor(15,15,15);"><img border="0" src="{$base_url}resources/images/spacer.gif" alt="" width="29" height="19" /></a></td>
		</tr>
	</table>
	
	</td>
	</tr>
	
	</table>

	<br /><br />
	
	<script language="javascript" type="text/javascript">
	
	{literal}
	
	var hexChars = "0123456789ABCDEF";
	
	var dec_r;
	var dec_g;
	var dec_b;
	
	var hex_r;
	var hex_g;
	var hex_b;
	
	function Dec2Hex (Dec) {
	
		var a = Dec % 16;
		
		var b = (Dec - a)/16; hex = "" + hexChars.charAt(b) + hexChars.charAt(a);
		
		return hex;
		
	}

	function setHoverColor(mod_r,mod_g,mod_b) {
	
		var dec_r = 16 * mod_r;
		var dec_g = 16 * mod_g;
		var dec_b = 16 * mod_b;
		
		var hex_r = Dec2Hex (dec_r);
		var hex_g = Dec2Hex (dec_g);
		var hex_b = Dec2Hex (dec_b);
		
		var x = new getObj('current-color');
		var y = document.getElementById('color-hover-hex');
		var z = document.getElementById('color-hover-rgb');
		
		x.style.background = '#' + hex_r + hex_g + hex_b;
		y.innerHTML = 'Hex: ' + hex_r + hex_g + hex_b;
		z.innerHTML = 'RGB: ' + dec_r + ', ' + dec_g + ', ' + dec_b;
		
	}
		
	function setSavedColor(mod_r,mod_g,mod_b) {
	
		var dec_r = 16 * mod_r;
		var dec_g = 16 * mod_g;
		var dec_b = 16 * mod_b;
		
		var hex_r = Dec2Hex (dec_r);
		var hex_g = Dec2Hex (dec_g);
		var hex_b = Dec2Hex (dec_b);
		
		var x = new getObj('selected-color');
		var y = document.getElementById('color-select-hex');
		var z = document.getElementById('color-select-rgb');
		
		x.style.background = '#' + hex_r + hex_g + hex_b;
		y.innerHTML = 'Hex: ' + hex_r + hex_g + hex_b;
		z.innerHTML = 'RGB: ' + dec_r + ', ' + dec_g + ', ' + dec_b;
		
		var form_x = new getObj('selected-color-form');
		var form_y = document.getElementById('color-form-hex');
		var form_z = document.getElementById('color-form-rgb');
		
		form_x.style.background = '#' + hex_r + hex_g + hex_b;
		form_y.innerHTML = 'Hex: ' + hex_r + hex_g + hex_b;
		form_z.innerHTML = 'RGB: ' + dec_r + ', ' + dec_g + ', ' + dec_b;
		
		document.search_advanced.image_colr_r.value = mod_r;
		document.search_advanced.image_colr_g.value = mod_g;
		document.search_advanced.image_colr_b.value = mod_b;
		
	}
		
	
	{/literal}
	
	</script>
		
	<b>##COLOR_HUD_03##:</b>
	
	<div style="width:326px;">
	
	<div style="float:left; width:30px; height:30px; background:#{$image_colr_hex.3}; margin:5px;" id="current-color"></div>
	<div style="float:left; width:150px; margin:5px;">
	
		<div id="color-hover-hex">##COLOR_HUD_04##: {$image_colr_hex.3}</div>
		<div id="color-hover-rgb">##COLOR_HUD_05##: {$image_colr_dec.3}</div>
	
	</div>
	
	</div>
	
	<br clear="all"/>
	
	<b>##COLOR_HUD_06##:</b>
	
	<div style="width:326px;">
	
	<div style="float:left; width:30px; height:30px; background:#{$image_colr_hex.3}; margin:5px;" id="selected-color"></div>
	<div style="float:left; width:150px; margin:5px;">
	
		<div id="color-select-hex">##COLOR_HUD_04##: {$image_colr_hex.3}</div>
		<div id="color-select-rgb">##COLOR_HUD_05##: {$image_colr_dec.3}</div>
	
	</div>
	
	</div>
	
	<br clear="all" />
	
</div>

<div id="hud-foot"><img src="{$base_url}resources/images/hud/hud.color.palette.foot.{if $browser_name == 'ie' && $browser_version <= 6}gif{else}png{/if}" width="356" height="11" border="0" alt="" /></div>

</div>