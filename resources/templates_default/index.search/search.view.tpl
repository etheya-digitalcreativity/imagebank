{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}


{if $cmd == "noresults"}

	<table class="table-text">
	
	<tr>
	<th>##SEARCH_RES_01##</th>
	<td>
	
	<h1>##SEARCH_RES_02##</h1>
	
	<p>##SEARCH_RES_03##</p>
		
	</td>
	<td>&nbsp;</td>
	</tr>
	
	</table>

<!-- SHOW INFORMATION ABOUT THE SEARCH RESULTS -->
{elseif $results_total == 1}

	<table class="table-text">
	
	<tr>
	<th>##SEARCH_RES_04##</th>
	<td>
	
	<h1>##SEARCH_RES_05##</h1>
	
	<p>##SEARCH_RES_06##</p>
	
	<form action="" method="get" onsubmit="return false;">
	
	<input type="button" onclick="javascript:Effect.toggle('search-form-toggle','blind',{literal}{duration:0.0}{/literal});" value="##SEARCH_RES_07##" style="width:100%;" class="formbutton" />
	
	</form>
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	</table>

{elseif $results_total > 1}

	<table class="table-text">
	
	<tr>
	<th>##SEARCH_RES_08##</td>
	<td>
	
	{if $results_total>=$set_search_max_limit}
	
		<h1>##SEARCH_RES_09##</h1>
		
		<p>##SEARCH_RES_10##</p>
		
	{/if}
	
	<h1>##SEARCH_RES_11##</h1>
	
	<p>##SEARCH_RES_12##</p>
	
	<form action="" method="get" onsubmit="return false;">
	
	<input type="button" onclick="javascript:Effect.toggle('search-form-toggle','blind',{literal}{duration:0.0}{/literal});" value="##SEARCH_RES_07##" style="width:100%;" class="formbutton" />
	
	</form>
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	</table>

{/if}





<!-- INCLUDE THE SEARCH FORM -->
{if $cmd == "noresults"}

	{load_pixie name="search_form" options="advanced"}

{else}

	<div id="search-form-toggle" style="display:none;">{load_pixie name="search_form" options="advanced"}</div>

{/if}







<!-- INCLUDE TEMPLATE FOR DISPLAYING IMAGE THUMBNAILS -->
{include_template file="index.image/view.thumbnails.tpl"}



<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="index.layout/main.footer.tpl"}


