{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}

{if $search_failed}

	<table class="table-text">
	
	<tr>
	<th>##SEARCH_PAGE_01##</th>
	<td>
	
	<h1>##SEARCH_PAGE_02##</h1>
	
	<p>##SEARCH_PAGE_03##</p>
	
	<p>##SEARCH_PAGE_04##</p>
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	</table>

{else}

	<table class="table-text">
	
	<tr>
	<th>##SEARCH_PAGE_05##</th>
	<td>
	
	<h1>##SEARCH_PAGE_06##</h1>
	
	<p>##SEARCH_PAGE_03##</p>
	
	<p>##SEARCH_PAGE_04##</p>
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	</table>

{/if}

<!-- INCLUDE THE SEARCH FORM -->
{load_pixie name="search_form" options="advanced"}

<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="index.layout/main.footer.tpl"}

