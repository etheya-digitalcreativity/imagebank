{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}


<table class="table-text">

<tr>
<th>##STORE_142##</th>
<td>

<h1>##STORE_143##</h1>

<p>##STORE_144##</p>

</td>
<td>&nbsp;</td>
</tr>

</table>




<form action="{$base_url}{$fil_index_store}" method="post">

<input type="hidden" name="cart_id" value="{$cart_id}" />

<input type="hidden" name="cmd" value="transactionAddMessage" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">##STORE_145##</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>##STORE_146##</th>
<td><textarea name="message" rows="4" style="width:100%;" class="forminput" cols="18"></textarea></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="submit" name="submit" value="##STORE_147##" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>

	
{if $message_on == TRUE}

	<table class="table-form">
	
	<thead>
	
	<tr>
		<th colspan="3">##STORE_148##</th>	
	</tr>
	
	</thead>
	
	<tbody>
	
		{section name="messages" loop="$message_id"}
		
			<tr class="{cycle values='list-one,list-two'}">
			
			<td>&nbsp;</td>
			
				<td valign="top">
				
				<table class="messages">
				<tr>
				<th>##STORE_149##</th>
				<td>{$message_time[messages]|date_format:"%B %e, %Y %H:%M"}</td>
				</tr>
				<tr>
				<th>##STORE_150##</th>
				<td><a href="mailto:{$message_user_email[messages]}" class="plain">{$message_username[messages]|nl2br}</a></td>
				</tr>
				<tr>
				<th>##STORE_151##</th>
				<td>{$message_text[messages]|nl2br}</td>
				</tr>
				</table>
								
				</td>
				
				<td>&nbsp;</td>
			
			</tr>
							
		{/section}
	
	</tbody>
	
	</table>

{/if}
	

<table class="table-cart">
	
	<thead>
	
	<tr>
	<th colspan="3">##STORE_154##</th>
	</tr>

	</thead>
	
	<tbody>
	
	{section name="image" loop="$image_id"}
	
		<tr class="{cycle values='list-one,list-two'}">
			
		<th>
			
			{if $image_comp_path[image] == TRUE}
			
				<a href="javascript:openPop('{$base_url}{$fil_psg_image_comping}?html={$image_comp_path[image]}&amp;','{$image_comp_size[image].0-2}','{$image_comp_size[image].1-2}','no','no','no','no','no','no','no');"><img src="{$fil_psg_image_thumbnail}?file={$image_small_path[image]}" {$image_small_dims[image]} class="thumbnail" alt="{$image_title[image]}" /></a>
			
			{else}
			
				<img src="{$fil_psg_image_thumbnail}?file={$image_small_path[image]}" {$image_small_dims[image]} class="thumbnail" alt="{$image_title[image]}" />
			
			{/if}
		
		</th>
			
		<td>
			
		<table class="cart-item">
			
			{if $image_filename[image]!=""}<tr>
			<th>##STORE_155##</th>
			<td>
			<a href="javascript:openPop('{$base_url}{$fil_psg_image_comping}?html={$image_comp_path[image]}&amp;','{$image_comp_size[image].0-2}','{$image_comp_size[image].1-2}','no','no','no','no','no','no','no');">{$image_filename[image]}</a>
			<input type="hidden" name="cart_item_id[]" value="{$cart_item_id[image]}" />
			<input type="hidden" name="image_id[]" value="{$image_id[image]}" />
			<input type="hidden" name="image_filename[]" value="{$image_filename[image]}" />
			</td>
			</tr>{/if}
			
			<tr>
			<th>##STORE_156##</th>
			<td>{if $image_title[image]!=""}{$image_title[image]}{else}N/A{/if}<input type="hidden" name="image_title[]" value="{$image_title[image]}" /></td>
			</tr>
			
			{if $image_comp_path[image] == TRUE}
			
				<tr>
				<th>##STORE_157##</th>
				<td>{$set_store_symbol}{$cart_item_price[image]}</td>
				</tr>
			
				<tr>
				<th>##STORE_158##</th>
				<td>{if $cart_item_shipping[image] > 0}{$set_store_symbol}{$cart_item_shipping[image]}{else}##STORE_160##{/if}</td>
				</tr>
				
			{else}
			
				<tr>
				<th>##STORE_159##</th>
				<td>##STORE_161##</td>
				</tr>
			
			{/if}
				
			{if $cart_item_type[image] == "physical"}

				<tr>
				<th>##STORE_162##</th>
				<td>{$cart_item_quantity[image]}</td>
				</tr>

			{/if}
			
			{if $cart_status == 4 && $download_link[image] && $cart_item_type[image] == "digital"}
			
				<tr>
				<th>##STORE_163##</th>
				<td>
				
				##STORE_164## 
				
				{if $download_jpg[image]}	<a href="{$download_jpg[image]}" title="##STORE_163##">JPG</a>{/if}
				{if $download_jp2[image]} | <a href="{$download_jp2[image]}" title="##STORE_163##">JPEG 2000</a>{/if}
				{if $download_tif[image]} | <a href="{$download_tif[image]}" title="##STORE_163##">TIFF</a>{/if}
				{if $download_psd[image]} | <a href="{$download_psd[image]}" title="##STORE_163##">PSD</a>{/if}
				
				
				{if $download_remaining[image] == 1}
				
					- {$download_remaining[image]} ##STORE_165##
				
				{else}
				
					- {$download_remaining[image]} ##STORE_166##
				
				{/if}
				
				</td>
				</tr>
			
			{/if}
			
			</table>				

		</td>
				
		<td>&nbsp;</td>
			
		</tr>
						
	{/section}
	
	</tbody>
	
</table>


<table class="table-form">

<thead>

<tr>
<th colspan="3">##STORE_167##</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>##STORE_168##</th>
<td>{$set_store_symbol}{$subtotal}</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>##STORE_169##</th>
<td>{$set_store_symbol}{$tax_total} ({$sales_tax}%)<br />{$set_store_tax_number|nl2br}</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>##STORE_170##</th>
<td>{$set_store_symbol}{$shipping}</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>##STORE_171##</th>
<td>{$set_store_symbol}{$total}</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>##STORE_172##</th>
<td>
{if !$payment_method}##STORE_173##{/if}
{if $payment_method == "10"}##STORE_175##{/if}
{if $payment_method == "11"}##STORE_174##{/if}
{if $payment_method == "12"}##STORE_176##{/if}
{if $payment_method == "13"}##STORE_177##{/if}
{if $payment_method == "14"}##STORE_178##{/if}
{if $payment_method == "15"}##STORE_179##{/if}
</td>
<td>&nbsp;</td>
</tr>

{if $set_store_offline_payment != ""}
<tr class="{cycle values='list-one,list-two'}">
<th>##STORE_180##</th>
<td>{$set_store_offline_payment}</td>
<td>&nbsp;</td>
</tr>
{/if}

</tbody>

</table>

</tbody>

</table>

{if $cart_status == 3 && ($set_store_func_cheque || $set_store_func_moneyorder || $set_store_func_postalorder || $set_store_func_banktransfer)}

	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">##STORE_181##</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	{if $set_store_offline_payment != ""}
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>##STORE_182##</th>
	<td>{$set_store_offline_payment}</td>
	<td>&nbsp;</td>
	</tr>
	{/if}
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td>
	
	<form action="{$base_url}{$fil_index_store}" method="post" onsubmit="return confirmSubmit('##STORE_183##');">
	
	{if $set_store_func_cheque}&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="payment_method" value="11"{if $payment_method == "11" || !$payment_method} checked="checked"{/if} /> ##STORE_184##<br />{/if}
	
	{if $set_store_func_postalorder}&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="payment_method" value="12"{if $payment_method == "12"} checked="checked"{/if} /> ##STORE_185##<br />{/if}
	
	{if $set_store_func_moneyorder}&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="payment_method" value="13"{if $payment_method == "13"} checked="checked"{/if} /> ##STORE_186##<br />{/if}
	
	{if $set_store_func_banktransfer}&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="payment_method" value="14"{if $payment_method == "14"} checked="checked"{/if} /> ##STORE_187##<br />{/if}
	
	<br />
	
	<input type="hidden" name="cart_id" value="{$cart_id}" />
	<input type="hidden" name="cmd" value="submitPaymentMethod" />
	<input type="submit" name="action" value="##STORE_188##" style="width:100%;" class="formbutton" />
	</form>
	
	</td>
	<td>&nbsp;</td>
	</tr>

	</table>
	
{/if}

{if $cart_status == 3 && $set_store_func_paypal == 1}

	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">##STORE_189##</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>##STORE_190##</th>
	<td>
	
	<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
	<input type="submit" name="action" value="##STORE_191##" style="width:100%;" class="formbutton" />
	<input type="hidden" name="tax_cart" value="{$tax_total}" />
	<input type="hidden" name="cmd" value="_cart" />
	<input type="hidden" name="upload" value="1">
	
	{section name="image" loop="$image_id"}
	
	<input type="hidden" name="item_name_{$smarty.section.image.index+1}" value="{$image_filename[image]}" />
	<input type="hidden" name="amount_{$smarty.section.image.index+1}" value="{$cart_item_price[image]}" />
	<input type="hidden" name="quantity_{$smarty.section.image.index+1}" value="{$cart_item_quantity[image]}" />
	<input type="hidden" name="shipping_{$smarty.section.image.index+1}" value="{$cart_item_shipping[image]}" />
	<input type="hidden" name="shipping2_{$smarty.section.image.index+1}" value="{$cart_item_shipping_multiple[image]}" />
	<input type="hidden" name="on0_{$smarty.section.image.index+1}" value="Your requirements" />
	<input type="hidden" name="os0_{$smarty.section.image.index+1}" value="{$item_usage_text[image]}" />
	
	{/section}		
	
	<input type="hidden" name="redirect_cmd" value="_xclick" />
	{if $set_store_flatrate_ship}<input type="hidden" name="handling_cart" value="{$set_store_flatrate_ship_val}" />{else}<input type="hidden" name="handling_cart" value="{$shipping}" />{/if}
	<input type="hidden" name="business" value="{$set_store_paypal_email}" />
	<input type="hidden" name="item_name" value="Pixaria Gallery Shopping Cart" />
	<input type="hidden" name="item_number" value="{$cart_id}" />
	<input type="hidden" name="custom" value="{$ses_psg_userid}" />
	<input type="hidden" name="invoice" value="{$cart_id}" />
	<input type="hidden" name="on0" value="cart_id" />
	<input type="hidden" name="os0" value="{$cart_id}" />
	<input type="hidden" name="on1" value="userid" />
	<input type="hidden" name="os1" value="{$ses_psg_userid}" />
	<input type="hidden" name="cs" value="0" />
	<input type="hidden" name="currency_code" value="{$set_store_currency}" />
	<input type="hidden" name="no_note" value="0" />
	<input type="hidden" name="cn" value="Any other comments" />
	<input type="hidden" name="return" value="{$base_url}{$fil_index_account}" />
	<input type="hidden" name="rm" value="2" />
	<input type="hidden" name="cancel_return" value="{$base_url}{$fil_index_cart}" />
	<input type="hidden" name="notify_url" value="{$base_url}{$fil_psg_paypal}" />
	</form>

	</td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
	</table>

{/if}

{include_template file="index.layout/main.footer.tpl"}


