{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/simple.header.tpl"}


<table class="table-text">

<tr>
<th>##STORE_117##</th>

<td>

	<h1>##STORE_118##</h1>
		
	<p>##STORE_119##</p>
	
	<img src="{$image_data.files.large_url}" class="thumbnail" {$image_data.files.large_size.2} alt="{$image_data.basic.title}" />
	
</td>
</tr>

</table>

{if $products_digital}

	<form action="{$base_url}{$fil_index_store}" name="create" method="post">
	
	<input type="hidden" name="cmd" value="recalculatePricePopup" />
	
	<input type="hidden" name="cart_item_id" value="{$cart_item_id}" />
	
	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="2">##STORE_126##</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	{section name="rules" loop=$calc_id}
				
		{if $calc_ar_name[rules].0 != ""}
			
		<tr class="{cycle values='list-one,list-two'}">
		<th>{$calc_name[rules]}</th>
		
		<td>
		
			<select name="options[]" class="forminput" style="width:100%;">
			
			{section name="options" loop=$calc_ar_id[rules]}
			
			<option value="{$calc_ar_id[rules][options]}">{$calc_ar_name[rules][options]}</option>
						
			{/section}
			
			</select>
		
		</td>
		</tr>
		
		{/if}
		
	{/section}	
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td><input type="submit" value="##STORE_127##" style="width:100%;" class="formbutton" /></td>
	</tr>
	
	</tbody>
	
	</table>
	
	</form>

{/if}

<!-- INCLUDE THE TOP HTML TEMPLATE -->
{include_template file="index.layout/simple.footer.tpl"}

