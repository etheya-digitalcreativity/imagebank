{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/simple.header.tpl"}


<table class="table-text">
<tr>
<th>##STORE_112##</th>

<td>

	<h1>##STORE_113##</h1>

	<img src="{$image_data.files.large_url}" class="thumbnail" {$image_data.files.large_size.2} alt="{$image_data.basic.title}" />
	
	<p>##STORE_114##</p>
	
	<p><a href="javascript:window.opener.location='{$base_url}{$fil_index_store}';self.close();">##STORE_115##</a></p>
	
	<p><a href="javascript:self.close();">##STORE_116##</a></p>
	
</td>
</tr>

</table>

{include_template file="index.layout/simple.footer.tpl"}

