{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/simple.header.tpl"}

<form action="{$base_url}{$fil_index_calculator}" method="post">

<table class="table-text">

<tr>
<th>##STORE_082##</th>
<td>

	<h1>##STORE_083##</h1>
		
	<p>##STORE_084##</p>
	
	<img src="{$image_data.files.large_url}" class="thumbnail" {$image_data.files.large_size.2} alt="{$image_data.basic.title}" />
	
	<p><a href="javascript:window.opener.location='{$base_url}{$fil_index_store}';self.close();">##STORE_085##</a></p>
	
	<p><a href="javascript:self.close();">##STORE_086##</a></p>
	
</td>
</tr>

<tr>
<th>&nbsp;</th>
<td><input type="button" onclick="javascript:self.close();" value="##STORE_086##" style="width:100%;" class="formbutton" /></td>
</tr>

</table>

</form>


<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="index.layout/simple.footer.tpl"}


