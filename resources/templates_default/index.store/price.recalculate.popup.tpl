{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/simple.header.tpl"}

<table class="table-text">
<tr>
<th>##STORE_101##</th>

<td>

	<h1>##STORE_102##</h1>
		
	<p>##STORE_103##</p>
	
	<img src="{$image_data.files.large_url}" class="thumbnail" {$image_data.files.large_size.2} alt="{$image_data.basic.title}" />
	
</td>
</tr>

</table>


<form action="{$base_url}{$fil_index_store}" method="post">

<input type="hidden" name="cart_item_id" value="{$cart_item_id}" />

<input type="hidden" name="cmd" value="updateImagePricePopup" />

<table class="table-form">

<thead>

<tr>
<th colspan="2">##STORE_104##</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>##STORE_105##</th>
<td>

{section name="option" loop=$ar_description}
{$ar_description[option]}<br />
<input type="hidden" name="ar_descriptions[]" value="{$ar_description[option]}" />
<input type="hidden" name="options[]" value="{$ar_id[option]}" />
{/section}

</td>
</tr>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>##STORE_106##</th>
<td>{$set_store_symbol}{$new_price}</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="submit" value="##STORE_111##" style="width:100%;" class="formbutton" /></td>
</tr>

</tbody>

</table>

</form>


{include_template file="index.layout/simple.footer.tpl"}


