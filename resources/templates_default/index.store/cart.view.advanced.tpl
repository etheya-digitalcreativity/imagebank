{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}


{if $terms == "on"}

	<table class="table-text">
	
	<tr>
	<th>##STORE_031##</td>
	<td>
	{if $set_contact_details != ""}
	
	<h1>##STORE_032##</h1>
	
	<p>{$set_contact_details|nl2br}</p>
	
	{/if}
	
	<form action="{$base_url}{$fil_index_store}" method="post">
	<input type="button" name="continue" value="##STORE_033##" onclick="javascript:history.go(-1);" class="formbutton" style="width:100%;" />
	</form>
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	</table>

{else}

	<table class="table-text">
	
	<tr>
	<th>##STORE_034##</td>
	<td>
	{if $set_contact_details != ""}
	
	<h1>##STORE_071##</h1>
	
	<p>{$set_contact_details|nl2br}</p>
	
	{/if}
	
	<form action="{$base_url}{$fil_index_store}" method="post">
	<input type="button" name="continue" value="##STORE_035##" onclick="javascript:location.href='{if $referer != ''}{$referer}{else}{$base_url}{/if}';" class="formbutton" style="width:100%;" />
	</form>
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	</table>
	
	
	<form method="post" action="{$base_url}{$fil_index_store}" id="quantities">
	
	<input type="hidden" name="cmd" value="updateProductQuantities" />
	
	<input type="hidden" name="cart_id" value="{$cart_id}" />
	
	<table class="table-cart">
	
	<thead>
	
		<tr>
		<th colspan="3">##STORE_036##</th>	
		</tr>
	
	</thead>
	
	<tbody>
	
		{section name="image" loop="$image_id"}
		
				<tr class="{cycle values='list-one,list-two'}">
				
				<th style="height:200px;">
				
				{if $image_comp_path[image] == TRUE}
				
					<a href="javascript:openPop('{$base_url}{$fil_psg_image_comping}?html={$image_comp_path[image]}&amp;','{$image_comp_size[image].0-2}','{$image_comp_size[image].1-2}','no','no','no','no','no','no','no');"><img src="{$fil_psg_image_thumbnail}?file={$image_large_path[image]}" {$image_large_dims[image]} class="thumbnail" alt="{$image_title[image]}" /></a>
				
				{else}
				
					<img src="{$fil_psg_image_thumbnail}?file={$image_large_path[image]}" {$image_large_dims[image]} class="thumbnail" alt="{$image_title[image]}" />
				
				{/if}
				
				</th>
				
				<td>
				
					<table class="cart-item">
					
					{if $image_filename[image]!=""}<tr>
					<th>##STORE_037##</th>
					<td><a href="javascript:openPop('{$base_url}{$fil_psg_image_comping}?html={$image_comp_path[image]}&amp;','{$image_comp_size[image].0-2}','{$image_comp_size[image].1-2}','no','no','no','no','no','no','no');">{$image_filename[image]}</a></td>
					</tr>{/if}
					
					<tr>
					<th>##STORE_038##</th>
					<td>{if $image_title[image]!=""}{$image_title[image]}{else}N/A{/if}</td>
					</tr>
					
					<tr>
					<th>##STORE_039##</th>
					<td>{if $item_price[image] > 0}{$set_store_symbol}{$item_price[image]}{else}N/A{/if}</td>
					</tr>
					
					{if $cart_item_type[image] == "physical"}
					
						<tr>
						<th>##STORE_040##</th>
						<td>
						<input type="text" size="2" maxlength="3" name="quantity[]" value="{$cart_item_quantity[image]}" />
						<input type="hidden" name="cart_item_id[]" value="{$cart_item_id[image]}" />
						</td>
						</tr>
						
						<tr>
						<th>##STORE_041##</th>
						<td>
						{if $set_store_flatrate_ship}
							##STORE_042##
						{else}
							{if $cart_item_shipping[image] > 0}{$set_store_symbol}{$cart_item_shipping[image]}{else}##STORE_043##{/if} (##STORE_044##)<br />
							{if $cart_item_shipping_multiple[image] > 0}{$set_store_symbol}{$cart_item_shipping_multiple[image]}{else}##STORE_043##{/if} (##STORE_045##)
						{/if}
						</td>
						</tr>
					
					{/if}
					
					<tr>
					<th>##STORE_046##</th>
					<td>
					
					{if $image_comp_path[image] == TRUE}
						
						{$item_usage_text[image]|nl2br}
					
					{else}
					
						##STORE_047##
					
					{/if}
					
					</td>
					</tr>
					
					<tr>
					<th>##STORE_048##</th>
					<td>
					{if $cart_item_type[image] == "digital"}<a href="{$calculator_href[image]}" title="##STORE_050##">##STORE_049##</a><br />{/if}
					<a href="{$base_url}{$fil_index_store}?cmd=removeCartItem&amp;cart_item_id={$cart_item_id[image]}" title="##STORE_052##">##STORE_051##</a>
					</td>
					</tr>
					
					</table>
					
				</td>
					
				<td>&nbsp;</td>
				
				</tr>
							
		{/section}
	
	</tbody>
	
	</table>
	
	
	<table class="table-form">
	
		<thead>
		
		<tr>
		<th colspan="3">##STORE_053##</th>
		</tr>
		
		</thead>
		
		<tbody>
			
		<tr class="{cycle values='list-one,list-two'}">
		<td>&nbsp;</td>
		<td><input type="submit" name="submit" value="##STORE_054##" style="width:100%;" class="formbutton" /></td>
		<td>&nbsp;</td>
		</tr>
		
		<tr class="{cycle values='list-one,list-two'}">
		<td>&nbsp;</td>
		<td><input type="button" onclick="window.location.href='{$fil_index_store}?cmd=emptyCart'" name="submit" value="##STORE_055##" style="width:100%;" class="formbutton" /></td>
		<td>&nbsp;</td>
		</tr>
		
		</tbody>
	
	</table>
	
	</form>

{/if}

{if $price_complete}

	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">##STORE_056##</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>##STORE_057##</th>
	<td>{$set_store_symbol}{$subtotal}</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>##STORE_058##</th>
	<td>{$set_store_symbol}{$tax_total} (##STORE_059##)<br />{$set_store_tax_number|nl2br}</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>##STORE_060##</th>
	<td>{$set_store_symbol}{$shipping}{if $set_store_shipping_message != "" && $set_store_default_ship == 1}<br />{$set_store_shipping_message|nl2br}{/if}</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>##STORE_061##</th>
	<td>{$set_store_symbol}{$total}</td>
	<td>&nbsp;</td>
	</tr>
	
	</table>
	
	{include_template file="index.store/payment.methods.tpl"}
	
{/if}

{if $quote_complete && !$price_complete && $set_store_quote_option}

	<form action="{$base_url}{$fil_index_store}" method="post" onsubmit="return confirmSubmit('##STORE_062##');">
	
	<input type="hidden" name="cart_id" value="{$cart_id}" />
	<input type="hidden" name="cmd" value="submitCartAdvanced" />
	
	<table class="table-form">

	<thead>
	
	<tr>
	<th colspan="3">##STORE_063##</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>&nbsp;</th>
	<td>
	
	<h1>##STORE_064##</h1>
	
	<p>##STORE_065##</p>
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td><input type="submit" name="action" value="##STORE_066##" class="formbutton" style="width:100%;" /></td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
	</table>
	
	</form>

{/if}

{if !$quote_complete && !$price_complete}

	<form action="{$base_url}{$fil_index_cart}" method="post">
		
	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">##STORE_067##</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>&nbsp;</th>
	<td>
	
	<h1>##STORE_068##</h1>
	
	<p>##STORE_069##</p>
	
	</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td><input type="submit" name="submit" disabled="disabled" value="##STORE_070##" style="width:100%;" class="formbutton" /></td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
	</table>
	
	</form>

{/if}





<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="index.layout/main.footer.tpl"}


