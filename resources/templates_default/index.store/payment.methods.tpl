{if $set_store_func_terms_conditions && $terms == ""}
		
		<form method="post" action="{$base_url}{$fil_index_store}">
		
		<table class="table-form">
		
		<thead>
		
		<tr>
		<th colspan="3">##STORE_087##</th>
		</tr>
		
		</thead>
		
		<tbody>
		
		<tr class="{cycle values='list-one,list-two'}">
		<th>&nbsp;</th>
		<td><div style="width:98%; height:200px; overflow:auto; background:#FFF; border:1px solid #AAA; padding:3px;"><div style="width:95%;">{$set_store_terms_conditions|nl2br}</div></div></td>
		<td>&nbsp;</td>
		</tr>
	
		<tr class="{cycle values='list-one,list-two' advance='0'}">
		<th>&nbsp;</th>
		<td><input type="checkbox" name="terms" class="forminput" /> <b>##STORE_088##</b></td>
		<td>&nbsp;</td>
		</tr>
	
		<tr class="{cycle values='list-one,list-two'}">
		<th>&nbsp;</th>
		<td><input type="submit" name="tandc" value="##STORE_089##" class="formbutton" style="width:100%;" /></td>
		<td>&nbsp;</td>
		</tr>
	
		</table>
		
		</form>
		
{elseif $terms == "on" || !$set_store_func_terms_conditions}
	
	{if $set_store_func_cheque || $set_store_func_moneyorder || $set_store_func_postalorder || $set_store_func_banktransfer}
	
		<table class="table-form">
		
		<thead>
		
		<tr>
		<th colspan="3">##STORE_090##</th>
		</tr>
		
		</thead>
		
		<tbody>
		
		{if $set_store_offline_payment != ""}
		<tr class="{cycle values='list-one,list-two' advance='0'}">
		<th>##STORE_091##:</th>
		<td>{$set_store_offline_payment}</td>
		<td>&nbsp;</td>
		</tr>
		{/if}
		
		<tr class="{cycle values='list-one,list-two'}">
		<th>&nbsp;</th>
		<td>
		
		<form action="{$base_url}{$fil_index_store}" method="post" onsubmit="return confirmSubmit('##STORE_092##');">
		
		{if $set_store_func_cheque}{assign var="check" value=1"}&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="payment_method" value="11" checked="checked" /> ##STORE_093##<br />{/if}
		
		{if $set_store_func_postalorder}{assign var="check" value=1"}&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="payment_method" value="12"{if !$check} checked="checked"{/if} /> ##STORE_094##<br />{/if}
		
		{if $set_store_func_moneyorder}{assign var="check" value=1"}&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="payment_method" value="13"{if !$check} checked="checked"{/if} /> ##STORE_095##<br />{/if}
		
		{if $set_store_func_banktransfer}{assign var="check" value=1"}&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="payment_method" value="14"{if !$check} checked="checked"{/if} /> ##STORE_096##<br />{/if}
		
		<br />
		
		<input type="hidden" name="cart_id" value="{$cart_id}" />
		<input type="hidden" name="cmd" value="submitPaymentMethod" />
		<input type="submit" name="action" value="##STORE_091##" style="width:100%;" class="formbutton" />
		</form>
		
		</td>
		<td>&nbsp;</td>
		</tr>
	
		</table>
		
	{/if}
	
	{if $set_store_func_paypal || $set_store_func_2co}
	
		<table class="table-form">
		
		<thead>
		
		<tr>
		<th colspan="3">##STORE_097##</th>
		</tr>
		
		</thead>
		
		<tbody>
		
		{if $set_store_func_paypal}
		
		<tr class="{cycle values='list-one,list-two'}">
		<th>##STORE_098##:</th>
		<td>
		
		<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
		<input type="submit" name="action" value="##STORE_098##" style="width:100%;" class="formbutton" />
		<input type="hidden" name="tax_cart" value="{$tax_total}" />
		<input type="hidden" name="cmd" value="_cart" />
		<input type="hidden" name="upload" value="1">
		
		{section name="image" loop="$image_id"}
		
		<input type="hidden" name="item_name_{$smarty.section.image.index+1}" value="{$image_filename[image]}" />
		<input type="hidden" name="amount_{$smarty.section.image.index+1}" value="{$item_price[image]}" />
		<input type="hidden" name="quantity_{$smarty.section.image.index+1}" value="{$cart_item_quantity[image]}" />
		<input type="hidden" name="shipping_{$smarty.section.image.index+1}" value="{$cart_item_shipping[image]}" />
		<input type="hidden" name="shipping2_{$smarty.section.image.index+1}" value="{$cart_item_shipping_multiple[image]}" />
		<input type="hidden" name="on0_{$smarty.section.image.index+1}" value="Your requirements" />
		<input type="hidden" name="os0_{$smarty.section.image.index+1}" value="{$item_usage_text[image]}" />
		
		{/section}		
		
		<input type="hidden" name="redirect_cmd" value="_xclick" />
		{if $set_store_flatrate_ship}<input type="hidden" name="handling_cart" value="{$set_store_flatrate_ship_val}" />{/if}
		<input type="hidden" name="business" value="{$set_store_paypal_email}" />
		<input type="hidden" name="item_name" value="Pixaria Gallery Shopping Cart" />
		<input type="hidden" name="item_number" value="{$cart_id}" />
		<input type="hidden" name="custom" value="{$ses_psg_userid}" />
		<input type="hidden" name="invoice" value="{$cart_id}" />
		<input type="hidden" name="on0" value="cart_id" />
		<input type="hidden" name="os0" value="{$cart_id}" />
		<input type="hidden" name="on1" value="userid" />
		<input type="hidden" name="os1" value="{$ses_psg_userid}" />
		<input type="hidden" name="cs" value="0" />
		<input type="hidden" name="currency_code" value="{$set_store_currency}" />
		<input type="hidden" name="no_note" value="0" />
		<input type="hidden" name="cn" value="Any other comments" />
		<input type="hidden" name="return" value="{$base_url}{$fil_index_account}" />
		<input type="hidden" name="rm" value="2" />
		<input type="hidden" name="cancel_return" value="{$base_url}{$fil_index_cart}" />
		<input type="hidden" name="notify_url" value="{$base_url}{$fil_psg_paypal}" />
		</form>
		
		</td>
		<td>&nbsp;</td>
		</tr>
		
		{/if}
		
		{if $set_store_func_2co}
		
		<tr class="{cycle values='list-one,list-two'}">
		<th>##STORE_099##:</th>
		<td>
	
		<form action="https://www.2checkout.com/2co/buyer/purchase" method="post">
		<input type="hidden" name="sid" value="{$set_store_2co_sid}" />
		{if $set_store_2co_demo}<input type="hidden" name="demo" value="Y" />{/if}
		<input type="hidden" name="total" value="{$total}" />
		<input type="hidden" name="cart_order_id" value="{$cart_id}" />
		<input type="hidden" name="shipping_method" value="11" />
		<input type="hidden" name="userid" value="{$ses_psg_userid}" />
		<input type="hidden" name="c_prod" value="pixaria_order" />
		<input type="hidden" name="c_name" value="Cart items" />
		<input type="hidden" name="c_description" value="Digital images and products" />
		<input type="hidden" name="c_price" value="{$total}" />
		<input type="hidden" name="id_type" value="2" />
		<input type="submit" name="action" value="##STORE_100##" style="width:100%;" class="formbutton" />
		</form>
		
		</td>
		<td>&nbsp;</td>
		</tr>
		
		{/if}
		
		</tbody>
		
		</table>
		
	{/if}

{/if}