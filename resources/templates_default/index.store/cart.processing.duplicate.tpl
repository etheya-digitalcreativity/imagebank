{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}

<table class="table-text">

<tr>
<th>##STORE_009##</th>
<td>

<h1>##STORE_010##</h1>

<p>##STORE_011##</p>

<ol>

<li>##STORE_012##</li>
<li>##STORE_013##</li>

</ol>

<p>##STORE_014##</p>

<ul>
<li>##STORE_015##: {$transaction_time}</li>
<li>##STORE_016##: {$payment_method}</li>
<li>##STORE_017##: {$transaction_id}</li>
<li>##STORE_018##: {$cart_id}</li>
</ul>

<p><a href="{$base_url}{$fil_index_account}">##STORE_019##</a></p>

</td>
<td>&nbsp;</td>
</tr>

</table>

<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="index.layout/main.footer.tpl"}


