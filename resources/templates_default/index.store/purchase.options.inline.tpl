{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}


<table class="table-text">
<tr>
<th>##STORE_117##</th>

<td>

	<h1>##STORE_118##</h1>
	
	<p>##STORE_119##</p>
	
	<img src="{$image_data.files.large_url}" class="thumbnail" {$image_data.files.large_size.2} alt="{$image_data.basic.title}" />
	
</td>
<td>&nbsp;</td>
</tr>

</table>

{if $products_physical}
	
	<form action="{$base_url}{$fil_index_store}" name="create" method="post">
	
	<input type="hidden" name="cmd" value="addProductsInline" />
	<input type="hidden" name="referer" value="{$referer}" />
	<input type="hidden" name="image_id" value="{$image_id}" />
	
	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">##STORE_120##</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	{if !$image_data.basic.image_product_link}
		
		{* All products are available for this image *}
	
		{section name="products" loop=$prod_id}
				
			<tr class="{cycle values='list-one,list-two'}">
			<th>{$prod_name[products]}</th>
			<td>
			
			{if $ses_psg_userid != ""}
			
				<table style="border-collapse:collapse; border-spacing:0px;">
				<tr>
				<td style="width:20px; vertical-align:top;"><input type="checkbox" name="prod_id[]" value="{$prod_id[products]}" class="forminput" /></td>
				<td style="width:300px;"><b>{$set_store_symbol}{$prod_price[products]}</b><br />{$prod_description[products]}</td>
				</tr>
				
				<tr>
				<td style="width:20px;"><input type="text" name="quantity[{$prod_id[products]}]" value="1" size="2" maxlength="3" /></td>
				<td style="width:300px;"><b>##STORE_121##</b></td>
				</tr>
				
				</table>
			
			{else}
			
				<b>{$set_store_symbol}{$prod_price[products]}</b><br />{$prod_description[products]}
			
			{/if}
			
			</td>
			<td>&nbsp;</td>
			</tr>
		
		{/section}
	
	{else}
		
		{* A limited selection of physical image products are linked to this image *}
		
		{section name="products" loop=$prod_id}
			
			{section name="enabled" loop=$image_data.extra.products}
			
				{if $image_data.extra.products[enabled] == $prod_id[products]}
				
					<tr class="{cycle values='list-one,list-two'}">
					<th>{$prod_name[products]}</th>
					<td>
					
					{if $ses_psg_userid != ""}
					
						<table style="border-collapse:collapse; border-spacing:0px;">
						<tr>
						<td style="width:20px; vertical-align:top;"><input type="checkbox" name="prod_id[]" value="{$prod_id[products]}" class="forminput" /></td>
						<td style="width:300px;"><b>{$set_store_symbol}{$prod_price[products]}</b><br />{$prod_description[products]}</td>
						</tr>
						
						<tr>
						<td style="width:20px;"><input type="text" name="quantity[{$prod_id[products]}]" value="1" size="2" maxlength="3" /></td>
						<td style="width:300px;"><b>##STORE_121##</b></td>
						</tr>
						
						</table>
					
					{else}
					
						<b>{$set_store_symbol}{$prod_price[products]}</b><br />{$prod_description[products]}
					
					{/if}
					
					</td>
					<td>&nbsp;</td>
					</tr>
				
				{/if}
			
			{/section}
			
		{/section}
	
	{/if}
	
	{if $ses_psg_userid != ""}
	
		<tr class="{cycle values='list-one,list-two'}">
		<th>&nbsp;</th>
		<td><input type="submit" value="##STORE_122##" style="width:100%;" class="formbutton" /></td>
		<td>&nbsp;</td>
		</tr>
	
	{/if}
	
	</tbody>
	
	</table>
	
	</form>
	
{/if}

{if !$set_store_calculate_login && !$ses_psg_userid && $set_store_type == 12}

	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">##STORE_123##</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr>
	<th>##STORE_124##</th>
	<td>##STORE_125##</td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
	</table>
	
{elseif $products_digital && $set_store_type == 12}

	<form action="{$base_url}{$fil_index_store}" name="create" method="post">
	
	<input type="hidden" name="cmd" value="calculatePriceInline" />
	
	<input type="hidden" name="image_id" value="{$image_id}" />
	
	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">##STORE_126##</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	{section name="rules" loop=$calc_id}
				
		{if $calc_ar_name[rules].0 != ""}
			
		<tr class="{cycle values='list-one,list-two'}">
		<th>{$calc_name[rules]}</th>
		
		<td>
		
			<select name="options[]" class="forminput" style="width:100%;">
			
			{section name="options" loop=$calc_ar_id[rules]}
			
			<option value="{$calc_ar_id[rules][options]}">{$calc_ar_name[rules][options]}</option>
						
			{/section}
			
			</select>
		
		</td>
		<td>&nbsp;</td>
		</tr>
		
		{/if}
		
	{/section}	
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td><input type="submit" value="##STORE_127##" style="width:100%;" class="formbutton" /></td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
	</table>
	
	</form>

{/if}

{if $set_store_type == 11}

	<form action="{$base_url}{$fil_index_store}" method="post">
	
	<input type="hidden" name="image_id" value="{$image_id}" />
	
	<input type="hidden" name="cmd" value="addImageQuoteInline" />
	
	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">##STORE_128##</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>##STORE_129##</th>
	<td><textarea name="quote_text" style="width:100%;" class="forminput" rows="5" cols="20"></textarea></td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td><input type="submit" value="##STORE_130##" style="width:100%;" class="formbutton" /></td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
	</table>
	
	</form>

{elseif $set_store_type == 10}

	<form action="{$base_url}{$fil_index_store}" method="post">
	
	<input type="hidden" name="image_id" value="{$image_id}" />
	
	<input type="hidden" name="cmd" value="addImageQuoteInline" />
	
	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">##STORE_131##</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>##STORE_132##</th>
	<td>##STORE_133##</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td><input type="submit" value="##STORE_134##" style="width:100%;" class="formbutton" /></td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
	</table>
	
	</form>

{/if}


{include_template file="index.layout/main.footer.tpl"}

