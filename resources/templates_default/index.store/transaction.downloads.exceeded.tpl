{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}


<form name="download" action="{$php_self}">

<table class="table-text">
<tr>
<th>##STORE_135##</th>

<td>

	<h1>##STORE_136##</h1>

	<p>##STORE_137##</p>
	
	<ol>
	
	{section name="error" loop=$error_message}
		
		<li class="html-error-warning">{$error_message[error]}</li>
	
	{/section}
	</ol>
	
	<input type="button" name="button" onclick="javascript:history.go(-1);" value="##STORE_138##" class="forminput" style="width:100%;" />
	
</td>
<td>&nbsp;</td>
</tr>

</table>

</form>


<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="index.layout/main.footer.tpl"}

