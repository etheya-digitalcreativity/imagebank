{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/2000/REC-xhtml1-20000126/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	
	<!-- Query String: {$query_string} -->
	<!-- Base URI: {$base_uri} -->

	<!-- Browser Name: {$browser_name} -->
	<!-- Browser Version: {$browser_version} -->
	
	<!-- OS Name: {$browser_OS_name} -->
	<!-- OS Version: {$browser_OS_version} -->
	
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	
	<title>{$set_site_name} &raquo; {$page_title}</title>
	
	<meta http-equiv="imagetoolbar" content="no" />

	{if $image.basic.keywords != ""}<meta name="keywords" content="{$image.basic.keywords}" />{else}<meta name="keywords" content="gallery, pixaria, web, php, album, photography" />{/if}
	
	{if $image.files.canonical_url != ""}<link rel="canonical" href="{$image.files.canonical_url}" />{/if}


	{if $image.basic.caption != ""}<meta name="description" content="{$image.basic.caption}" />{else}
	<meta name="description" content="This photo gallery website is powered by Pixaria Gallery software." />{/if}
	
	
	{if $image.files.large_url != ""}<meta name="thumbnail_url" content="{$image.files.large_url}" />{/if}
	
	
	<link rel="stylesheet" href="{$base_url}resources/themes/{$set_theme}/css/pixaria.css" />
	<link rel="stylesheet" href="{$base_url}resources/themes/{$set_theme}/css/pixaria.3.0.css" />
	
	{if $js_lightbox}<link rel="stylesheet" href="{$base_url}resources/themes/{$set_theme}/css/pixaria.lightbox.css" />{/if}
	
	{if $set_func_rss == "1" }<link href="{$base_url}{$fil_index_rss}" rel="alternate" type="application/rss+xml" title="{$set_site_name}" />{/if}
	
	{if $gallery_media_rss != ''}{$gallery_media_rss}{/if}
	
	<script type="text/javascript" language="javascript">
	<!--
	var baseUrl 	= '{$base_url}';
	var baseTheme 	= '{$set_theme}';
	-->
	</script>
	
	<script type="text/javascript" language="javascript" src="{$base_url}resources/javascript/scriptaculous/prototype.js"></script>
	<script type="text/javascript" language="javascript" src="{$base_url}resources/javascript/scriptaculous/scriptaculous.js"></script>

	<script type="text/javascript" language="javascript" src="{$base_url}resources/themes/{$set_theme}/javascript/pixaria.global.js"></script>
	
	{if $js_lightbox}<script type="text/javascript" language="javascript" src="{$base_url}resources/themes/{$set_theme}/javascript/pixaria.lightbox.js"></script>{/if}
	{if $js_multifile}<script type="text/javascript" language="javascript" src="{$base_url}resources/themes/{$set_theme}/javascript/pixaria.multifile.js"></script>{/if}
	
	<!--[if lt IE 7]>
	<script defer type="text/javascript" src="{$base_url}resources/themes/{$set_theme}/javascript/pixaria.pngfix.js"></script>
	<![endif]-->
		
</head>

<body>

<div id="content">

	{if !$ses_psg_full_user && $set_login_default}
	
		<!-- IF THE USER IS NOT LOGGED IN AND THE SETTING TO LIMIT ACCESS FOR LOGGED OUT USERS IS ON THEN SHOW A LIMITED NAVIGATION BAR -->
		<div id="header">
	
			<div id="logo"><img src="{$base_url}resources/themes/{$set_theme}/images/logo.gif" alt="{$set_site_name}" width="293" height="52" border="0" /></div>
			
			{load_pixie name="cart_items"}
			
			<div id="navigation">
			
			<ul id="nav">
			
				<li><a href="{$base_url}{$fil_index_account}?cmd=userRegistrationStart" class="navigation-link">##NAVIGATION_01##</a></li>
			
				<li><a href="{$base_url}{$fil_index_login}" class="navigation-link">##NAVIGATION_02##</a></li>

				{if $set_func_user_language_select}
					
					<li><a href="javascript:void(0);" class="navigation-link" onclick="javascript:Effect.toggle('lang-picker', 'blind', {literal}{ duration: 0.1 }{/literal});" style="padding-top:3px;"><img src="{$base_url}/resources/images/flags/{$smarty.const.LOCAL_LANGUAGE}_small.png" alt="" width="24" height="24" border="0"></a></li>
				
				{/if}

			</ul>
			
			</div>
			
		</div>
	
	{else}

		<!-- SHOW THE NORMAL NAVIGATION BAR -->
		<div id="header">
		
			<div id="logo"><img src="{$base_url}resources/themes/{$set_theme}/images/logo.gif" alt="{$set_site_name}" width="293" height="52" border="0" /></div>
			
			{load_pixie name="cart_items"}
			
			<div id="navigation">
			
			<ul id="nav">
				
				<li><a href="{$base_url}{$fil_index}" class="navigation-link">##NAVIGATION_03##</a></li>
				
				{if $set_func_news}
					
					<!-- IF THE NEWS FUNCTIONS ARE ON -->
					<li><a href="{$base_url}{$fil_index_news}" class="navigation-link">##NAVIGATION_04##</a></li>
					
				{/if}
				
				{if $set_func_search}
					
					<!-- IF THE SEARCH FUNCTIONS ARE ON -->
					<li><a href="{$base_url}{$fil_index_search}" class="navigation-link">##NAVIGATION_05##</a></li>
					
				{/if}
				
				<li><a href="{$base_url}{$fil_index_account}" class="navigation-link">##NAVIGATION_06##</a></li>
				
				{if $set_func_lightbox}
					
					<!-- IF THE LIGHTBOX FUNCTIONS ARE ON -->
					<li><a href="{$base_url}{$fil_index_lightbox}" class="navigation-link">##NAVIGATION_07##</a></li>
				
				{/if}
				
				{if $set_func_store && $sys_store_enabled}
					
					<!-- IF THE E-COMMERCE FUNCTIONS ARE ON -->
					<li><a href="{$base_url}{$fil_index_store}" class="navigation-link">##NAVIGATION_08##</a></li>
				
				{/if}
				
				{if $set_func_contact}
					
					<!-- IF THE CONTACT FORM FUNCTIONS ARE ON -->
					<li><a href="{$base_url}{$fil_index_contact}" class="navigation-link">##NAVIGATION_09##</a></li>
				
				{/if}
				
				{if $ses_psg_administrator}
					
					<!-- IF THE USER IS AN ADMINISTRATOR -->
					<li><a href="{$base_url}{$fil_admin_dashboard}" class="navigation-link">##NAVIGATION_10##</a></li>
					
				{/if}
				
				
				{if $ses_psg_full_user}
				
					<!-- IF LOGGED IN -->
					<li><a href="{$base_url}{$fil_index_login}?cmd=signout" class="navigation-link">##NAVIGATION_11##</a></li>
				
				
				{else}
					
					<!-- IF NOT LOGGED IN -->
					<li><a href="{$base_url}{$fil_index_account}?cmd=userRegistrationStart" class="navigation-link">##NAVIGATION_01##</a></li>
				
					<li><a href="{$base_url}{$fil_index_login}" class="navigation-link">##NAVIGATION_02##</a></li>
				
				{/if}
				
				{if $set_func_user_language_select}
					
					<li><a href="javascript:void(0);" class="navigation-link" onclick="javascript:Effect.toggle('lang-picker', 'blind', {literal}{ duration: 0.1 }{/literal});" style="padding-top:3px;"><img src="{$base_url}/resources/images/flags/{$smarty.const.LOCAL_LANGUAGE}_small.png" alt="" width="24" height="24" border="0"></a></li>
				
				{/if}
				
			</ul>
			
			<div id="search">{load_pixie name="search_form"}</div>
			
			</div>
		
		</div>
	
	{/if}


<div>
	
<div id="front-outerdiv">

<div id="front-innerdiv">
	
{include_template file="index.snippets/language.picker.html"}