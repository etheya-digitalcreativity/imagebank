{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/2000/REC-xhtml1-20000126/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
		
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	
	<title>{$set_site_name} &raquo; {$page_title}</title>
	
	<meta name="keywords" content="gallery, photostream, web, php, album, photography" />
	
	<meta name="description" content="This photo gallery website is powered by Pixaria Gallery software." />
	
	<link rel="stylesheet" href="{$base_url}resources/themes/{$set_theme}/css/pixaria.css" />
	
	<script type="text/javascript" language="javascript">
	<!--
	var baseUrl 	= '{$base_url}';
	var baseTheme 	= '{$set_theme}';
	-->
	</script>
	
	<script type="text/javascript" language="javascript" src="{$base_url}resources/javascript/scriptaculous/prototype.js"></script>
	<script type="text/javascript" language="javascript" src="{$base_url}resources/javascript/scriptaculous/scriptaculous.js"></script>

	<script type="text/javascript" language="javascript" src="{$base_url}resources/themes/{$set_theme}/javascript/pixaria.global.js"></script>
	
	<!--[if lt IE 7]>
	<script defer type="text/javascript" src="{$base_url}resources/javascript/pixaria.pngfix.js"></script>
	<![endif]-->
		
</head>

<body>

<div id="popup-content">
		
<div id="front-outerdiv">

<div id="front-innerdiv">
	
