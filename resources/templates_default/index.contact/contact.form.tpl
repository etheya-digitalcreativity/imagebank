{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}


<table class="table-text">

<tr>
<th>##CTCT_FRM_01##</th>
<td>

{if $problem || $captcha_failure}

	<h1>##CTCT_FRM_10##</h1>
	
	<p>##CTCT_FRM_11##:</p>

	<ol>
	
	{if $captcha_failure}
		<li class="html-error-warning">##CTCT_FRM_CHECK_03##</li>
	{/if}
	
	{section name="problems" loop=$errors}
		
		<li class="html-error-warning">{$errors[problems]}</li>
	
	{/section}
	</ol>
	
{else}

	<h1>##CTCT_FRM_02##</h1>
	
	<p>##CTCT_FRM_03##</p>
	
{/if}

</td>
<td>&nbsp;</td>
</tr>

</table>



<form action="{$base_url}{$fil_index_contact}" method="post">

<input type="hidden" name="cmd" value="sendMessage" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">##CTCT_FRM_13##:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>##CTCT_FRM_04##:</th>
<td><input type="text" maxlength="50" size="30" name="name" value="{$name}" style="width:100%;" class="forminput" /></td>
<td> <span class="required">##CTCT_FRM_05##</span></td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>##CTCT_FRM_06##:</th>
<td><input type="text" maxlength="50" size="30" name="email" value="{$email}" style="width:100%;" class="forminput" /></td>
<td> <span class="required">##CTCT_FRM_05##</span></td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>##CTCT_FRM_07##:</th>
<td><input type="text" maxlength="50" size="30" name="telephone" value="{$telephone}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>##CTCT_FRM_08##:</th>
<td><textarea name="message" rows="10" style="width:100%;" class="forminput">{$message}</textarea></td>
<td style="vertical-align:top;"> <span class="required">##CTCT_FRM_05##</span></td>
</tr>

{if $set_func_captcha_contact}
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>##CTCT_FRM_CHECK_01##:</th>
	<td>
	##CTCT_FRM_CHECK_02##:<br /><br />
	<img src="{$base_url}index.captcha.php?key={$captcha_key}" width="332" height="60" alt="Captcha Image" />
	<input type="hidden" name="captcha_key" value="{$captcha_key}" />
	</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>&nbsp;</th>
	<td><input type="text" maxlength="5" size="60" name="captcha_code" style="width:50px;" class="forminput" /> <span class="required">##CTCT_FRM_05##</span></td>
	<td></td>
	</tr>
{/if}


<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="submit" name="submit" value="##CTCT_FRM_09##" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>



<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="index.layout/main.footer.tpl"}
