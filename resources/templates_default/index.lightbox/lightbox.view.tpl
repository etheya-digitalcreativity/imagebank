{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}

{if !$thumbnails && !$ses_psg_userid}

<table class="table-text">

<tr>
<th>##LIGHTBOX_65##</th>
<td>

<h1>##LIGHTBOX_66##</h1>

<p>##LIGHTBOX_67##</p>

</td>
<td>&nbsp;</td>
</tr>

</table>

{elseif !$thumbnails && $ses_psg_userid != ""}

<table class="table-text">

<tr>
<th>##LIGHTBOX_68##</th>
<td>

<h1>##LIGHTBOX_69##</h1>

<p>##LIGHTBOX_70##</p>

<p>##LIGHTBOX_71##</p>

<a href="{$base_url}{$fil_index_lightbox}?cmd=showLightboxList" title="##LIGHTBOX_73##">##LIGHTBOX_72##</a>

</td>
<td>&nbsp;</td>
</tr>

</table>

{/if}

{if $referer != "" && $thumbnails != ""}

<table class="table-text">

<tr>
<th>##LIGHTBOX_74##</th>
<td>

<h1>##LIGHTBOX_75##</h1>

<p>##LIGHTBOX_76##</p>

<a href="{$referer}" title="##LIGHTBOX_77##">##LIGHTBOX_78##</a>

</td>
<td>&nbsp;</td>
</tr>

</table>

{/if}

<!-- INCLUDE TEMPLATE FOR DISPLAYING IMAGE THUMBNAILS -->
{include_template file="index.image/view.thumbnails.tpl"}



	
<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="index.layout/main.footer.tpl"}


