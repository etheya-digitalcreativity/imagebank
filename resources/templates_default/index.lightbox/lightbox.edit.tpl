{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}





<!-- SHOW THE LIGHTBOX EDITOR PAGE -->

<form method="post" name="lightbox_edit" action="{$base_url}{$fil_index_lightbox}">

<input type="hidden" name="lightbox_order" value="" />

<input type="hidden" name="lightbox_id" value="{$lightbox_id}" />

<input type="hidden" name="cmd" value="saveChangesToLightbox" />

<table class="table-text">

<tr>
<th>##LIGHTBOX_06##</th>
<td>

<h1>##LIGHTBOX_07##</h1>

<p>##LIGHTBOX_08##</p>

<p>##LIGHTBOX_09##</p>

</td>
<td>&nbsp;</td>
</tr>

</table>

<table class="table-form">

<thead>

<tr>
<th colspan="3">##LIGHTBOX_10##</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>##LIGHTBOX_11##</th>
<td><input type="text" name="lightbox_name" maxlength="30" style="width:100%;" class="forminput" value="{$lightbox_name}" /></td>
<td>&nbsp;</td>
</tr>


<tr class="{cycle values='list-one,list-two'}">
<th>##LIGHTBOX_12##</th>
<td><input type="checkbox" name="lightbox_default" class="forminput"{if $ses_psg_lightbox_id==$lightbox_id} checked="checked"{/if} /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="button" onclick="javascript:saveLightbox();document.lightbox_edit.submit();" value="##LIGHTBOX_13##" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>


<table class="table-thumbnail">

<thead>
				
	<tr>
	<th>##LIGHTBOX_14##</th>
	</tr>
	
</thead>

<tbody>

<tr>
<td>


<!-- START OF SMALL THUMBNAIL SECTION -->
<div id="image-small">
				
{section name="tn" loop=$thumbnails}

	<div class="thumb-sml" id="image_{$thumbnails[tn].basic.id}" style="width:190px; height:190px; cursor:move;">
	
	<table>
	
	<thead>
	<tr>
	<th colspan="2"><a href="javascript:void(0);" ondblclick="{$thumbnails[tn].url_popup}" title="##LIGHTBOX_15##"><img src="{$thumbnails[tn].files.small_url}" class="thumbnail" {$thumbnails[tn].files.small_size.2} alt="" /></a></th>
	</tr>
	</thead>
	
	<tbody>
	<tr>
	<th>##LIGHTBOX_16##</th>
	<td>{$thumbnails[tn].basic.title|truncate:20:"...":true}</td>
	</tr>
	
	<tr>
	<th>##LIGHTBOX_17##</th>
	<td>{$thumbnails[tn].basic.date|date_format:"%B %e, %Y"}</td>
	</tr>
	
	<tr>
	<th>##LIGHTBOX_18##</th>
	<td>{$thumbnails[tn].basic.file_name|truncate:20:"...":true}</td>
	</tr>
	
	</tbody>
	
	</table>
	
	</div>
	
{/section}
		
<br clear="all"/>

</div>

</td>
</tr>



</tbody>

</table>

<!-- THIS SCRIPT CALLS THE PROTOTYPE AND SCRIPTACLUOUS LIBRARIES FOR DRAG AND DROP -->
<script type="text/javascript" language="javascript">

{literal}

function saveLightbox() {

		var list = document.getElementById('image-small');
		var items = list.getElementsByTagName('div');
		var ids = '';
			
		for (var i = 0; i < items.length; i++)  {
			if (i > 0) ids += '|';
			var id = items[i].getAttribute('id');
			ids += id;
		}
		
		document.lightbox_edit.lightbox_order.value = ids;
		return true;

}

Sortable.create('image-small',{tag:'div',overlap:'horizontal',constraint:false,

	onUpdate:function() {
	
	var list = document.getElementById('image-small');
	var items = list.getElementsByTagName('div');
	var ids = '';
		
	for (var i = 0; i < items.length; i++)  {
		if (i > 0) ids += '|';
		var id = items[i].getAttribute('id');
		ids += id;
	}
	
	document.lightbox_edit.lightbox_order.value = ids;
	return true;

	}

})

{/literal}

</script>
	


<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="index.layout/main.footer.tpl"}

