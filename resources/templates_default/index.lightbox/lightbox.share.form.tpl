{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}




<table class="table-text">

<tr>
<th>##LIGHTBOX_48##</th>
<td>

{if $problem}

	<h1>##LIGHTBOX_49##</h1>
	
	<p>##LIGHTBOX_50##</p>

	<ol>
	{section name="problems" loop=$errors}
		
		<li class="html-error-warning">{$errors[problems]}</li>
	
	{/section}
	</ol>
	
{else}

	<h1>##LIGHTBOX_51##</h1>
	
	<p>##LIGHTBOX_52##</p>
	
{/if}

</td>
<td>&nbsp;</td>
</tr>

</table>



<form action="{$base_url}{$fil_index_lightbox}" method="post">

<input type="hidden" name="cmd" value="actionSendLightbox" />

<input type="hidden" name="lightbox_id" value="{$lightbox_id}" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">##LIGHTBOX_53##</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>##LIGHTBOX_54##</th>
<td><input type="text" maxlength="50" size="30" name="name" value="{$name}" style="width:100%;" class="forminput" /></td>
<td> <span class="required">##LIGHTBOX_55##</span></td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>##LIGHTBOX_56##</th>
<td><input type="text" maxlength="50" size="30" name="email" value="{$email}" style="width:100%;" class="forminput" /></td>
<td> <span class="required">##LIGHTBOX_55##</span></td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>##LIGHTBOX_57##</th>
<td><input type="text" maxlength="50" size="30" name="subject" value="{if $subject != ""}{$subject}{else}##LIGHTBOX_79##{/if}" style="width:100%;" class="forminput" /></td>
<td> <span class="required">##LIGHTBOX_55##</span></td>
</tr>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>##LIGHTBOX_58##</th>
<td>
<textarea name="message" rows="10" style="width:100%;" class="forminput">{if $message != ""}{$message}{else}
##LIGHTBOX_59##

{$lightbox_url}

{$ses_psg_name}
{/if}</textarea>
</td>
<td style="vertical-align:top;"> <span class="required">##LIGHTBOX_55##</span></td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="submit" name="submit" value="##LIGHTBOX_60##" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>



<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="index.layout/main.footer.tpl"}
