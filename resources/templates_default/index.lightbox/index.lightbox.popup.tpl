{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/simple.header.tpl"}


<table class="table-text">
<tr>
<th>##LIGHTBOX_01##</th>

<td>

	<h1>##LIGHTBOX_02##</h1>
	
	<img src="{$image.files.large_url}" border="0" {$image.files.large_size.2} title="{$image.basic.title}" alt="{$image.basic.title}" class="image" />
	
	<p>##LIGHTBOX_03##</p>
	
	<p><a href="javascript:window.opener.location='{$base_url}{$fil_index_lightbox}';self.close();">##LIGHTBOX_04##</a></p>
	
	<p><a href="javascript:self.close();">##LIGHTBOX_05##</a></p>
	
</td>
</tr>

</table>


<!-- INCLUDE THE TOP HTML TEMPLATE -->
{include_template file="index.layout/simple.footer.tpl"}

