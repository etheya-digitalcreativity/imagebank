{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}




<table class="table-text">

<tr>
<th>##LIGHTBOX_27##</th>
<td>


<h1>##LIGHTBOX_28##</h1>

<p>##LIGHTBOX_29##</p>

<p>##LIGHTBOX_30##</p>

{if $active_lightbox == 0 && $lightbox_count > 0}

	<p class="html-error-warning">##LIGHTBOX_31##</p>

{/if}

</td>
<td>&nbsp;</td>
</tr>
</table>




<form action="{$base_url}{$fil_index_lightbox}" method="post">

<input type="hidden" name="cmd" value="createNewLightbox" />

<table class="table-form">

{if $lightbox_count < 10}

	<thead>

	<tr>
	<th colspan="3">##LIGHTBOX_32##</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>##LIGHTBOX_33##</th>
	<td><input type="text" name="lightbox_name" maxlength="30" style="width:100%;" class="forminput" value="{$formal_title}" /></td>
	<td>&nbsp;</td>
	</tr>
	
	
	{if $lightbox_count > 0}
	
		<tr class="{cycle values='list-one,list-two' advance='0'}">
		<th>##LIGHTBOX_34##</th>
		<td><input type="checkbox" name="lightbox_default" class="forminput" checked="checked" /> (##LIGHTBOX_35##)</td>
		<td>&nbsp;</td>
		</tr>
	
	{else}
	
		<tr class="{cycle values='list-one,list-two' advance='0'}">
		<th>##LIGHTBOX_34##:</th>
		<td><input type="checkbox" name="lightbox_default_off" disabled="disabled" class="forminput" checked="checked" /><input type="hidden" name="lightbox_default" value="on" /> (##LIGHTBOX_35##)</td>
		<td>&nbsp;</td>
		</tr>
	
	{/if}
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td><input type="submit" name="submit" value="##LIGHTBOX_36##" style="width:100%;" class="formbutton" /></td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
{/if}

</table>

</form>


<table class="table-listbox">

<tr>
	<th>##LIGHTBOX_37##</th>	
	<th>##LIGHTBOX_38##</th>	
	<th>##LIGHTBOX_39##</th>	
	<th>##LIGHTBOX_40##</th>	
</tr>

	{section name="lightboxes" loop=$lightbox_id}
	
		<tr class="{cycle values='list-one,list-two'}">
		
		<td style="height:30px;">{$lightbox_name[lightboxes]}</td>
		
		<td>{$lightbox_items[lightboxes]}</td>
		
		<td>{$lightbox_date[lightboxes]|date_format:"%A, %B %e, %Y"}</td>
		
		<td>
		
		<ul class="actionlist">
		
		<li><a href="{$base_url}{$fil_index_lightbox}?lid={$lightbox_id[lightboxes]}">##LIGHTBOX_41##</a></li>
		
		{if $set_func_contact_sheet}<li><a href="{$base_url}{$fil_index_lightbox}?lid={$lightbox_id[lightboxes]}&amp;cmd=downloadContactSheet" {if $lightbox_items[lightboxes] < 1 } onclick="javascript:return false;"{/if}>##LIGHTBOX_42##</a></li>{/if}
	
		<li><a href="{$base_url}{$fil_index_lightbox}?cmd=showDefaultLightboxEditor&amp;lid={$lightbox_id[lightboxes]}">##LIGHTBOX_43##</a></li>
		
		{if $ses_psg_full_user}<li><a href="{$base_url}{$fil_index_lightbox}?cmd=formSendLightbox&amp;lid={$lightbox_id[lightboxes]}">##LIGHTBOX_44##</a></li>{/if}
		
		{if $lightbox_id[lightboxes] != $active_lightbox}<li><a href="{$base_url}{$fil_index_lightbox}?cmd=makeLightboxActive&amp;lightbox_id={$lightbox_id[lightboxes]}">##LIGHTBOX_45##</a></li>{/if}
		
		{if $lightbox_id[lightboxes] != $active_lightbox}<li><a href="{$base_url}{$fil_index_lightbox}?cmd=deleteLightbox&amp;lightbox_id={$lightbox_id[lightboxes]}">##LIGHTBOX_46##</a></li>{/if}
		
		</td>
											
		</tr>
	
	{sectionelse}
	
		<tr class="{cycle values='list-one,list-two'}">
		
		<td colspan="5" style="height:30px;">##LIGHTBOX_47##</td>
		
		</tr>
	
	{/section}	

</table>




<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="index.layout/main.footer.tpl"}

