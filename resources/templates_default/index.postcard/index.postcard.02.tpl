{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}




<table class="table-text">

<tr>
<th>

{if !$checked}

	##POSTCARD_12##

{else}

	##POSTCARD_13##

{/if}

</th>
<td>

<h1>##POSTCARD_14##</h1>

<p>##POSTCARD_15##</p>

<form action="{$base_url}{$fil_index_postcard}" method="post">

<input type="hidden" name="id" value="{$id}" />
<input type="hidden" name="recipient" value="{$recipient}" />
<input type="hidden" name="email" value="{$email}" />
<input type="hidden" name="name" value="{$name}" />
<input type="hidden" name="font_name" value="{$font_name}" />
<input type="hidden" name="postcard_text" value="{$send_text}" />
<input type="hidden" name="cmd" value="sendPostcard" />
<input type="hidden" name="return" value="{$return}" />


{if $checked}

<input type="submit" value="##POSTCARD_16##" class="formbutton" name="action" style="width:100%;" />

{ else }

<input type="submit" value="##POSTCARD_16##" class="formbutton" name="action" disabled="disabled" style="width:100%;" />

{/if}

&nbsp;&nbsp;

<input type="button" value="##POSTCARD_17##" class="formbutton" name="button" onclick="javascript:history.go(-1);" style="width:100%;" />

</form>


</td>
<td>&nbsp;</td>
</tr>

</table>

<table class="table-text">

<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>

<tr>
<td colspan="2" style="text-align:center;"><img src="{$image_url}" alt="##POSTCARD_18##" border="0" {$card_size} {if $set_func_disable_saving}ondragstart="javascript:return false;" oncontextmenu="javascript:return false;"{/if} /></td>
<td>&nbsp;</td>
</tr>

</table>



<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="index.layout/main.footer.tpl"}


