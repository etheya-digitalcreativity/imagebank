{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}




<table class="table-text">

<tr>
<th>##POSTCARD_01##</th>
<td>

<h1>##POSTCARD_02##</h1>

<p>##POSTCARD_03##</p>

</td>
<td>&nbsp;</td>
</tr>

<tr>
<td colspan="2" style="text-align:center;"><img src="{$image_url}" alt="{$pcm_chosen_photo}" border="0" {$card_size} {if $set_func_disable_saving}ondragstart="javascript:return false;" oncontextmenu="javascript:return false;"{/if} /></td>
<td>&nbsp;</td>

</table>

<br />

<form action="{$base_url}{$fil_index_postcard}" method="post">

<input type="hidden" name="id" value="{$id}" />
<input type="hidden" name="return" value="{$return}" />
<input type="hidden" name="cmd" value="formCheckPostcard" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">##POSTCARD_04##</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>##POSTCARD_05##<br />##POSTCARD_06##</th>
<td><textarea name="postcard_text" rows="7" cols="40" class="forminput" style="width:100%;">{$text_unencoded}</textarea></td>
<td>&nbsp;</td>
</tr>

{if $set_func_postcards_user == "0" && $ses_psg_userid == ""}

	<tr class="{cycle values='list-one,list-two'}">
	<th>##POSTCARD_07##</th>
	<td><input type="text" name="name" class="forminput" value="{$name}" style="width:100%;" /></td>
	<td>&nbsp;</td>
	</tr>
	
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>##POSTCARD_08##</th>
	<td><input type="text" name="email" class="forminput" value="{$email}" style="width:100%;" /></td>
	<td>&nbsp;</td>
	</tr>

{/if}

<tr class="{cycle values='list-one,list-two'}">
<th>##POSTCARD_09##</th>
<td><input type="text" name="recipient" class="forminput" value="{$recipient}" style="width:100%;" /></td>
<td>&nbsp;</td>
</tr>


<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>##POSTCARD_10##</th>
<td>
	
<input type="radio" name="font_name" value="arialbd.ttf" class="forminput" checked="checked" /> <img src="{$base_url}resources/themes/{$set_theme}/images/postcards/font.en.arial.gif" alt="Arial" width="100" height="16" border="0" /><br />

<input type="radio" name="font_name" value="comicbd.ttf" class="forminput" /> <img src="{$base_url}resources/themes/{$set_theme}/images/postcards/font.en.comic.gif" alt="Comic Sans" width="100" height="16" border="0" /><br />
	
</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="submit" value="##POSTCARD_11##" class="formbutton" name="action" style="width:100%;" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>




<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="index.layout/main.footer.tpl"}


