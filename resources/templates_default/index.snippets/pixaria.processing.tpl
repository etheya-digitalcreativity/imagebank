{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/2000/REC-xhtml1-20000126/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	
		<title>{$set_site_name}</title>
		
		{$meta_header}
		
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		
		<link rel="stylesheet" href="{$base_url}resources/themes/{$set_theme}/css/pixaria.processing.css" />
		
</head>

<body>
		
<div id="content">
	
	<div class="bodytext">
	
		<table border="0" cellspacing="3" cellpadding="0" width="600">
		
		<tr>
		<td align="center" valign="top">
		
		<p><img src="{$base_url}resources/themes/{$set_theme}/images/loaders/loader-detail.gif" width="32" height="32" alt="##HTML_SNIP_03##" /></p>
		
		<p align="center" style="font-size:14pt;">##HTML_SNIP_04##</p>

		<p align="center">##HTML_SNIP_05##</p>
	
		</td>
				
		</tr>
		
		</table>
			
	</div>
			
</div>
			
</body>

</html>