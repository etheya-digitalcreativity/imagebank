<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0"> 

<channel>

<title>{$set_site_name}</title> 

<link>{$base_url}</link>

<description>{$set_site_name}</description>

<language>##LANG_CODE##</language>

<managingEditor>{$set_site_name}</managingEditor>

<webMaster>{$set_site_name}</webMaster>

<copyright>##RSS_01## {$blog_date|date_format:"%Y"} {$set_site_name}</copyright>

{section name="items" loop="$blog_item_id"}

	<item>
		<title>{$blog_title[items]}</title>
		<link>{$blog_link[items]}</link>
		<description>
		<![CDATA[<table>
			<tr>
			<td valign="top"><img src="{$base_url}{$fil_psg_image_thumbnail}?file={$blog_thumb[items]}" {$blog_thumb_dims[items]} alt="{$blog_title[items]}" /></td>
			<td valign="top" style="color:#333; font-family:Helvetica,Arial,Geneva,Swiss,SunSans-Regular; font-size:9pt;">{$blog_content[items]}</td>
			</tr>
			</table>]]>
		</description>
		<pubDate>{$blog_date_unx[items]|date_format:"%a, %d %b %Y %T GMT"}</pubDate>
		<guid>{$blog_link[items]}</guid>
	</item>

{/section}

</channel>

</rss>