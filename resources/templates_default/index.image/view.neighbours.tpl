{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}
{if $view_mode != "image"}

<table class="table-thumbnail">
	
	<thead>
	
	<tr>
	
		<th>

		<div class="pages"><a href="{$image_prev}"><img src="{$base_url}resources/themes/{$set_theme}/images/previous.gif" alt="Previous" title="Go to the previous image" width="16" height="16" border="0" /></a> <a href="{$image_next}"><img src="{$base_url}resources/themes/{$set_theme}/images/next.gif" alt="Next" title="Go to the next image" width="16" height="16" border="0" /></a></div>
		

		{if $view_mode == "gallery"}
			##IM_NEAR_01##
		{elseif $view_mode == "search"}
			##IM_NEAR_02##
		{elseif $view_mode == "lightbox"}
			##IM_NEAR_03##
		{/if}
		</th>
	
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr>
	
		<td>
		
		<table width="100%">
		
		<tr>
		
		{section name="tn" loop=$thumbnails}
			
			<td align="center">
			
			{if $thumbnails[tn].basic.id == $image.basic.id}
			
				<div class="neighbour" id="{$smarty.section.tn.index}">
			
				<table>
				<tr>
				<td style="text-align:center;"><img src="{$thumbnails[tn].files.small_url}&amp;" class="thumbnail" border="0" alt="{$thumbnails[tn].basic.title}" {$thumbnails[tn].files.small_size.2} /></td>
				</tr>
				</table>
				
				<div style="text-align:center; font-weight:bold; font-size:8pt;">##IM_NEAR_04## {$thumbnails_numbers[tn]}</div>
			
				</div>
			
			{else}
	
				<div class="neighbour-opaque" id="{$smarty.section.tn.index}" onmouseover="document.getElementById({$smarty.section.tn.index}).className='neighbour';" onmouseout="document.getElementById({$smarty.section.tn.index}).className='neighbour-opaque';" onclick="javascript:location.href='{$thumbnails_urls[tn]}';" title="{$thumbnails[tn].basic.title}">
			
				<table>
				<tr>
				<td style="text-align:center;"><img src="{$thumbnails[tn].files.small_url}&amp;" class="thumbnail" border="0" alt="{$thumbnails[tn].basic.title}" {$thumbnails[tn].files.small_size.2} /></td>
				</tr>
				</table>
				
				<div style="text-align:center; font-size:8pt;">##IM_NEAR_04## {$thumbnails_numbers[tn]}</div>
				
				</div>
				
			{/if}
			
			</td>
			
		{/section}
		
		</tr>
		
		</table>
		
		</td>
	
	</tr>
	
	</tbody>
	
</table>

{/if}