{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}

{*

<!--

This template is used to show details of a
single image and is called by templates used
in the search engine, gallery and lightbox.

Display of images in popup windows is handled by
a separate template because links and form controls
in those windows have to behave in a different way

-->

*}

<div id="image-detail">

	<div class="breadcrumbs">
		
		<div class="pages"><a href="{$image_prev}"><img src="{$base_url}resources/themes/{$set_theme}/images/previous.gif" alt="Previous" title="Go to the previous image" width="16" height="16" border="0" /></a> <a href="{$image_next}"><img src="{$base_url}resources/themes/{$set_theme}/images/next.gif" alt="Next" title="Go to the next image" width="16" height="16" border="0" /></a></div>
		
		{if $view_mode == "gallery"}
			
			<!-- SHOW TABLE HEAD FOR GALLERY -->
			<a href="{$base_url}{$fil_index}" class="plain">##IM_DETAIL_01##</a> &raquo; <a href="{$base_url}{$fil_index_gallery}" class="plain">##IM_DETAIL_02##</a>{section name="nav loop=$nav_href} &raquo; <a href="{$nav_href[nav]}" class="plain">{$nav_title[nav]}</a>{/section} &raquo; {$image.basic.title}
		
		{elseif $view_mode == "image"}
		
			<!-- SHOW TABLE HEAD FOR IMAGE -->
			<a href="{$base_url}{$fil_index}" class="plain">##IM_DETAIL_01##</a> &raquo; {$image.basic.title}
		
		{elseif $view_mode == "search"}
		
			<!-- SHOW TABLE HEAD FOR SEARCH -->
			<a href="{$base_url}{$fil_index}" class="plain">##IM_DETAIL_01##</a> &raquo; <a href="{$base_url}{$fil_index_search}?sid={$search_id}" class="plain">##IM_DETAIL_03##</a> &raquo; {$image.basic.title}
		
		{elseif $view_mode == "lightbox"}
		
			<!-- SHOW TABLE HEAD FOR LIGHTBOX -->
			<a href="{$base_url}{$fil_index}" class="plain">##IM_DETAIL_01##</a> &raquo; <a href="{$base_url}{$fil_index_lightbox}" class="plain">##IM_DETAIL_04##</a> &raquo; {$image.basic.title}
		
		{else}
		
			<!-- SHOW TABLE HEAD FOR IMAGE -->
			{$image.basic.title}
		
		{/if}

	</div>
	
	<div class="imagebox-thin">
		
		<div class="panel-image">
		
		{if $set_image_output_format=="11" }
			
			<table class="image" align="center" cellpadding="0" cellspacing="0"><tr><td style="padding:0px; margin:0px;"><object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" {$image.files.comp_size.2} id="image">
			<param name="allowScriptAccess" value="sameDomain" />
			<param name="movie" value="{$image.files.comp_url}" />
			<param name="quality" value="high" />
			<param name="bgcolor" value="#FFFFFF" />
			<embed src="{$image.files.comp_url}" quality="high" bgcolor="#FFFFFF" {$image.files.comp_size.2} name="image" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
			</object></td></tr></table>
			
			<br />
			<span align="center">##IM_DETAIL_05##</span>
			<br />
		
		{elseif $set_image_output_format=="12" }
			
	
			<table class="image" align="center" cellpadding="0" cellspacing="0"><tr><td style="padding:0px; margin:0px;"><embed src="{$image.files.comp_url}"
			type="image/svg+xml" pluginspage="http://www.adobe.com/svg/viewer/install/" {$image.files.comp_size.2}></td></tr></table>
			
			<br />
			<span align="center">##IM_DETAIL_06##</span>
			<br />			
	
		{elseif $set_image_output_format=="10" }
			
			<img src="{$image.files.comp_url}" border="0" {$image.files.comp_size.2} title="{$image.basic.title}" alt="{$image.basic.title}" class="image" {if $set_func_disable_saving}ondragstart="javascript:return false;" oncontextmenu="javascript:return false;"{/if} />
			
		{/if}
		
		{if $set_image_watermark_text != "" && (($set_image_watermark == "11" && !$ses_psg_userid) || ($set_image_watermark == "12"))}
			
			<br />
			<span>{$set_image_watermark_text}</span>
			<br />
			
		{/if}
		
		</div>
		
	</div>

	<div class="infobox-thin">
			
		<div id="tab-container">
			<div id="mainmenu">
				<ul id="tabs">
					<li><a href="#tab1" rel="nofollow">##IM_DETAIL_07##</a></li>
					<li><a href="#tab2" rel="nofollow">##IM_DETAIL_08##</a></li>
				</ul>
			</div>
			<div class="panel" id="loader" style="display:block; text-align:center;">
			<table style="width:90%;"><tr><td style="text-align:center;"><img src="{$base_url}resources/themes/{$set_theme}/images/loaders/loader-detail.gif" alt="" width="32" height="32" border="0" /></td></tr></table>
			</div>
			<div class="panel" id="tab1">
				
				<table class="table-image-view">
					
					<tr>
					<td colspan="2">
					
					<h1>{$image.basic.title}</h1>
					
					{if $image.basic.caption != ""}<p>{$image.basic.caption}</p>{/if}
				
					</td>
					</tr>
					
					<tr>
					<th>##IM_DETAIL_09##:</th>
					<td>{$image.basic.date|date_format:"%A, %B %e %Y"}</td>
					</tr>
					
					<tr>
					<th>##IM_DETAIL_10##:</th>
					<td>{$image.basic.file_name}</td>
					</tr>
					
					<tr>
					<th>##IM_DETAIL_11##:</th>
					<td>{if $image.basic.rights_type == 11}##IM_DETAIL_12##{else}##IM_DETAIL_13##{/if}</td>
					</tr>
					
					<tr>
					<th>##IM_DETAIL_42##:</th>
					<td>
					{if $image.basic.model_release}##IM_DETAIL_43##<br />{/if}
					{if $image.basic.property_release}##IM_DETAIL_44##<br />{/if}
					{if !$image.basic.model_release || !$image.basic.property_release}##IM_DETAIL_45##{/if}
					</td>
					</tr>
					
					<tr>
					<th>##IM_DETAIL_14##:</th>
					<td>{if $image.basic.rights_type == 11}{$image.basic.rights_text}{else}##IM_DETAIL_15##{/if}</td>
					</tr>
					
					<tr>
					<th>##IM_DETAIL_16##:</th>
					<td>
					
					{if $set_func_postcards == 1 && ($ses_psg_userid != "" || $set_func_postcards_user == "0")}<a href="{$base_url}{$fil_index_postcard}?id={$image.basic.id}" rel="nofollow">##IM_DETAIL_17##</a><br />{/if}
					{if $image.basic.cart_link}<a href="{$image.basic.cart_add.url_default}" title="Purchase options" rel="nofollow">##IM_DETAIL_18##</a><br />{/if}
					{if $set_func_lightbox == 1}
					{if $view_mode != "lightbox" || !$lightbox_is_mine}
					<a href="{$image.basic.lightbox_add.url_default}" title="##IM_DETAIL_19##" rel="nofollow">##IM_DETAIL_19##</a><br />
					{else}
					<a href="{$image.basic.lightbox_remove}" title="##IM_DETAIL_47##" rel="nofollow">##IM_DETAIL_47##</a><br />
					{/if}
					{/if}
					
					{if $ses_psg_administrator}<a href="{$image.files.admin_edit_return}">##IM_DETAIL_46##</a><br />{/if}
					
					</td>
					</tr>
					
					{if $image.basic.keywords_present}
					
						<tr>
						<th>##IM_DETAIL_20##:</th>
						<td>
							
							{if $set_func_search == TRUE}
							
								<form method="post" action="{$base_url}{$fil_index_search}" style="margin-top:0px; margin-bottom:0px;">
								
								<input type="hidden" name="cmd" value="doSearch" />
								
								<input type="hidden" name="kwd" value="1" />
								
								<input type="hidden" name="new" value="1" />
											
								{section name="keywords" loop=$image.basic.keywords_array}
								
								<div class="keywords">
								
								<input type="checkbox" name="keywords_array[]" value="{$image.basic.keywords_array[keywords]|escape:'url'}" /><a href="{$base_url}{$fil_index_search}?keywords={$image.basic.keywords_array_escaped[keywords]}&amp;new=1&amp;kwd=1&amp;cmd=doSearch" rel="nofollow">{$image.basic.keywords_array[keywords]}</a>
								
								</div>
								
								{/section}
											
								<br clear="all" />
								
								<br />
								
								<input type="submit" style="width:90%;" value="##IM_DETAIL_23##" class="formbutton" />
								
								</form>
							
							{else}
										
								{section name="keywords" loop=$image.basic.keywords_array}
								
								<div style="min-width:110px; padding-bottom:3px; float:left;">
								
								{$image.basic.keywords_array[keywords]}
								
								</div>
								
								{/section}
							
							{/if}
							
						</td>
						</tr>
						
					{else}
					
						<tr>
						<th>##IM_DETAIL_20##:</th>
						<td>##IM_DETAIL_21##</td>
						</tr>
				
					{/if}
					
				</table>
				
			</div>
			
			<div class="panel" id="tab2">
				
				{if $set_func_download_comp}
				<h1>##IM_DETAIL_24##</h1>
				
				<p>##IM_DETAIL_25##:</p>
				
				<dl class="download-list">
					<dt><a href="{$image.files.download_comp}" rel="nofollow"><img src="{$base_url}resources/themes/{$set_theme}/images/icons/download-jpg.gif" alt="" width="40" height="40" border="0" /></a></dt>
					<dd><a href="{$image.files.download_comp}" rel="nofollow">##IM_DETAIL_26##</a></dd>
					<dd>##IM_DETAIL_27##</dd>
				</dl>
				{/if}
				
				{if $image.files.download_urls.jpg != "" || $image.files.download_urls.jp2 != "" || $image.files.download_urls.tif != "" || $image.files.download_urls.psd != "" || $image.files.download_urls.eps != ""}
				
					<h1>##IM_DETAIL_28##</h1>
					
					<p>##IM_DETAIL_28##:</p>
				
					<dl class="download-list">
						
						{if $image.files.download_urls.jpg != ""}
							<dt><a href="{$image.files.download_urls.jpg}" rel="nofollow"><img src="{$base_url}resources/themes/{$set_theme}/images/icons/download-jpg.gif" alt="" width="40" height="40" border="0" /></a></dt>
							<dd><a href="{$image.files.download_urls.jpg}" rel="nofollow">##IM_DETAIL_30##</a></dd>
							<dd>##IM_DETAIL_31##</dd>
						{/if}
						
						{if $image.files.download_urls.jp2 != ""}
							<dt><a href="{$image.files.download_urls.jp2}" rel="nofollow"><img src="{$base_url}resources/themes/{$set_theme}/images/icons/download-jp2.gif" alt="" width="40" height="40" border="0" /></a></dt>
							<dd><a href="{$image.files.download_urls.jp2}" rel="nofollow">##IM_DETAIL_32##</a></dd>
							<dd>##IM_DETAIL_33##</dd>
						{/if}
						
						{if $image.files.download_urls.tif != ""}
							<dt><a href="{$image.files.download_urls.tif}" rel="nofollow"><img src="{$base_url}resources/themes/{$set_theme}/images/icons/download-tiff.gif" alt="" width="40" height="40" border="0" /></a></dt>
							<dd><a href="{$image.files.download_urls.tif}" rel="nofollow">##IM_DETAIL_34##</a></dd>
							<dd>##IM_DETAIL_35##</dd>
						{/if}
						
						{if $image.files.download_urls.psd != ""}
							<dt><a href="{$image.files.download_urls.psd}" rel="nofollow"><img src="{$base_url}resources/themes/{$set_theme}/images/icons/download-psd.gif" alt="" width="40" height="40" border="0" /></a></dt>
							<dd><a href="{$image.files.download_urls.psd}" rel="nofollow">##IM_DETAIL_36##</a></dd>
							<dd>##IM_DETAIL_37##</dd>
						{/if}
						
						{if $image.files.download_urls.eps != ""}
							<dt><a href="{$image.files.download_urls.eps}" rel="nofollow"><img src="{$base_url}resources/themes/{$set_theme}/images/icons/download-eps.gif" alt="" width="40" height="40" border="0" /></a></dt>
							<dd><a href="{$image.files.download_urls.eps}" rel="nofollow">##IM_DETAIL_38##</a></dd>
							<dd>##IM_DETAIL_39##</dd>
						{/if}
						
					</dl>
				
				{else}
				
					<h1>##IM_DETAIL_40##</h1>
					
					<p>##IM_DETAIL_41##</p>
				
				{/if}
				
			</div>
			
		</div>
		
	</div>

	<br clear="all" />
			
</div>

{include_template file="index.image/view.neighbours.tpl"}


