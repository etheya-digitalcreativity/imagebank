{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/simple.header.tpl"}

{*

<!--

This template is used to show details of a
single image in a popup window.

Display of images outside of popup windows is handled
by a separate template because links and form controls
in those windows have to behave in a different way

-->

*}

<div id="image-detail">

		<div class="breadcrumbs">
			
			{if $view_mode == "gallery"}
				
				<!-- SHOW TABLE HEAD FOR GALLERY -->
				<a href="javascript:window.opener.location='{$base_url}{$fil_index}';window.close();" class="plain">##IM_DETAIL_01##</a> &raquo; <a href="javascript:window.opener.location='{$base_url}{$fil_index_gallery}';window.close();" class="plain">##IM_DETAIL_02##</a>{section name="nav loop=$nav_href} &raquo; <a href="{$nav_href[nav]}" class="plain">{$nav_title[nav]}</a>{/section} &raquo; {$image.basic.title}
			
			{elseif $view_mode == "image"}
			
				<!-- SHOW TABLE HEAD FOR IMAGE -->
				<a href="javascript:window.opener.location='{$base_url}{$fil_index}';window.close();" class="plain">##IM_DETAIL_01##</a> &raquo; {$image.basic.title}
			
			{elseif $view_mode == "search"}
			
				<!-- SHOW TABLE HEAD FOR SEARCH -->
				<a href="javascript:window.opener.location='{$base_url}{$fil_index}';window.close();" class="plain">##IM_DETAIL_01##</a> &raquo; <a href="javascript:window.opener.location='{$base_url}{$fil_index_search}';window.close();" class="plain">##IM_DETAIL_03##</a> &raquo; {$image.basic.title}
			
			{elseif $view_mode == "lightbox"}
			
				<!-- SHOW TABLE HEAD FOR LIGHTBOX -->
				<a href="javascript:window.opener.location='{$base_url}{$fil_index}';window.close();" class="plain">##IM_DETAIL_01##</a> &raquo; <a href="javascript:window.opener.location='{$base_url}{$fil_index_lightbox}';window.close();" class="plain">##IM_DETAIL_04##</a> &raquo; {$image.basic.title}
			
			{else}
			
				<!-- SHOW TABLE HEAD FOR IMAGE -->
				{$image.basic.title}
			
			{/if}
	
		</div>
	
	<div class="imagebox-thin">
		
		<div class="panel-image">
		
		{if $set_image_output_format=="11" }
			
			<table class="image" align="center" cellpadding="0" cellspacing="0"><tr><td style="padding:0px; margin:0px;"><object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" {$image.files.comp_size.2} id="image">
			<param name="allowScriptAccess" value="sameDomain" />
			<param name="movie" value="{$image.files.comp_url}" />
			<param name="quality" value="high" />
			<param name="bgcolor" value="#FFFFFF" />
			<embed src="{$image.files.comp_url}" quality="high" bgcolor="#FFFFFF" {$image.files.comp_size.2} name="image" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
			</object></td></tr></table>
			
			<br />
			<span align="center">##IM_DETAIL_05##</span>
			<br />
		
		{elseif $set_image_output_format=="12" }
			
	
			<table class="image" align="center" cellpadding="0" cellspacing="0"><tr><td style="padding:0px; margin:0px;"><embed src="{$image.files.comp_url}"
			type="image/svg+xml" pluginspage="http://www.adobe.com/svg/viewer/install/" {$image.files.comp_size.2}></td></tr></table>
			
			<br />
			<span align="center">##IM_DETAIL_06##</span>
			<br />			
	
		{elseif $set_image_output_format=="10" }
			
			<img src="{$image.files.comp_url}" border="0" {$image.files.comp_size.2} title="{$image.basic.title}" alt="{$image.basic.title}" class="image" {if $set_func_disable_saving}ondragstart="javascript:return false;" oncontextmenu="javascript:return false;"{/if} />
			
		{/if}
		
		{if $set_image_watermark_text != "" && (($set_image_watermark == "11" && !$ses_psg_userid) || ($set_image_watermark == "12"))}
			
			<br />
			<span>{$set_image_watermark_text}</span>
			<br />
			
		{/if}
		
		</div>
		
	</div>

	<div class="infobox-thin">
			
		<div id="tab-container">
			<div id="mainmenu">
				<ul id="tabs">
					<li><a href="#tab1">##IM_DETAIL_07##</a></li>
					<li><a href="#tab2">##IM_DETAIL_08##</a></li>
				</ul>
			</div>
			<div class="panel" id="loader" style="display:block; text-align:center;">
			<table style="width:90%;"><tr><td style="text-align:center;"><img src="{$base_url}resources/themes/{$set_theme}/images/loaders/loader-detail.gif" alt="" width="32" height="32" border="0" /></td></tr></table>
			</div>
			<div class="panel" id="tab1">
				
				<h1>{$image.basic.title}</h1>
					
				{if $image.basic.caption != ""}<p>{$image.basic.caption}</p>{/if}
				
				<table class="table-image-view">
					
					<tr>
					<th>##IM_DETAIL_09##:</th>
					<td>{$image.basic.date|date_format:"%A, %B %e %Y"}</td>
					</tr>
					
					<tr>
					<th>##IM_DETAIL_10##:</th>
					<td>{$image.basic.file_name}</td>
					</tr>
					
					<tr>
					<th>##IM_DETAIL_11##:</th>
					<td>{if $image.basic.rights_type == 11}##IM_DETAIL_12##{else}##IM_DETAIL_13##{/if}</td>
					</tr>
					
					<tr>
					<th>##IM_DETAIL_14##:</th>
					<td>{if $image.basic.rights_type == 11}{$image.basic.rights_text}{else}##IM_DETAIL_15##{/if}</td>
					</tr>
					
					<tr>
					<th>##IM_DETAIL_16##:</th>
					<td>
					
					{if $set_func_postcards == 1 && ($ses_psg_userid != "" || $set_func_postcards_user == "0")}<a href="#" onclick="javascript:window.opener.location.href='{$base_url}{$fil_index_postcard}?id={$image.basic.id}';window.close();">##IM_DETAIL_17##</a><br />{/if}
					{if $image.basic.cart_link}<a href="{$image.basic.cart_add.url_default}" title="##IM_DETAIL_18##">##IM_DETAIL_18##</a><br />{/if}
					{if $set_func_lightbox == 1}<a href="{$image.basic.lightbox_add.url_default}" title="##IM_DETAIL_19##">##IM_DETAIL_19##</a><br />{/if}
					
					</td>
					</tr>
	
					<tr>
					<th>##IM_DETAIL_20##:</th>
					<td>
					
						{if !$image.basic.keywords_present}
						
							<span>##IM_DETAIL_21##</span>
						
						{elseif $set_func_search == TRUE}
						
							<!-- THIS JAVASCRIPT FUNCTION SENDS ANY SEARCH QUERY BACK TO THE PARENT WINDOW -->
							<script language="javascript" type="text/javascript">
							<!--
							{literal}		
								
							function getKeywords() {
								
								var url		= '{/literal}{$base_url}{$fil_index_search}?kwd=1&cmd=doSearch&new=1&{literal}';
								var items 	= getSelectedCheckboxValue(document.forms[0]);
								var ids 	= '';
								
								for (var i = 0; i < items.length; i++)  {
									if (i >= 0) ids += 'keywords_array[]=';
									var id = items[i] + '&';
									ids += id;
								}
								
								if (items.length > 0) {
									
									window.opener.location = url + ids;
								
									self.close();
								
								} else {
								
									alert("{/literal}##IM_DETAIL_22##{literal}");
								
								}
							}
							
							{/literal}
							// -->
							</script>
							
							<form method="post" name="search-popup" action="{$base_url}{$fil_index_search}" style="margin-top:0px; margin-bottom:0px;">
							
							<input type="hidden" name="cmd" value="doSearch" />
							
							<input type="hidden" name="kwd" value="1" />
							
							<input type="hidden" name="new" value="1" />
							
							<table>
							
							<tr>
							<td valign="top">
							
							{section name="keywords" loop=$image.basic.keywords_array}
							
							<div class="keywords">
							
							<input type="checkbox" name="keywords_array[]" value="{$image.basic.keywords_array_escaped[keywords]}" /><a href="javascript:window.opener.location='{$base_url}{$fil_index_search}?keywords={$image.basic.keywords_array_escaped[keywords]|escape:'quotes'}&amp;new=1&amp;kwd=1&amp;cmd=doSearch';self.close();" rel="external nofollow">{$image.basic.keywords_array[keywords]}</a><br />
							
							</div>
							
							{/section}
							
							</td>
							</tr>
							
							</table>
							
							<input type="button" onclick="getKeywords()" style="width:100%;" value="##IM_DETAIL_23##" class="formbutton" />
							
							</form>
						
						{else}
						
							<table cellpadding="3" cellspacing="0" border="0">
							
							<tr>
							<td valign="top" style="font-size:9pt;">
							
							{section name="keywords" loop=$image.basic.keywords_array}
							
							<div style="width:45%; padding-bottom:3px; float:left;">
					
							{$image.basic.keywords_array[keywords]}<br />
							
							</div>
							
							{/section}
							
							</td>
							</tr>
							
							</table>
						
						{/if}
						
					</td>
					</tr>

				</table>
				
			</div>
			
			<div class="panel" id="tab2">
				
				<h1>##IM_DETAIL_24##</h1>
				
				<p>##IM_DETAIL_25##:</p>
				
				<dl class="download-list">
					<dt><a href="{$image.files.download_comp}"><img src="{$base_url}resources/themes/{$set_theme}/images/icons/download-jpg.gif" alt="" width="40" height="40" border="0" /></a></dt>
					<dd><a href="{$image.files.download_comp}">##IM_DETAIL_26##</a></dd>
					<dd>##IM_DETAIL_27##</dd>
				</dl>
					
				{if $image.files.download_urls.jpg != "" || $image.files.download_urls.jp2 != "" || $image.files.download_urls.tif != "" || $image.files.download_urls.psd != "" || $image.files.download_urls.eps != ""}
				
					<h1>##IM_DETAIL_28##</h1>
					
					<p>##IM_DETAIL_29##:</p>
				
					<dl class="download-list">
						
						{if $image.files.download_urls.jpg != ""}
							<dt><a href="{$image.files.download_urls.jpg}"><img src="{$base_url}resources/themes/{$set_theme}/images/icons/download-jpg.gif" alt="" width="40" height="40" border="0" /></a></dt>
							<dd><a href="{$image.files.download_urls.jpg}">##IM_DETAIL_30##</a></dd>
							<dd>##IM_DETAIL_31##</dd>
						{/if}
						
						{if $image.files.download_urls.jp2 != ""}
							<dt><a href="{$image.files.download_urls.jp2}"><img src="{$base_url}resources/themes/{$set_theme}/images/icons/download-jp2.gif" alt="" width="40" height="40" border="0" /></a></dt>
							<dd><a href="{$image.files.download_urls.jp2}">##IM_DETAIL_32##</a></dd>
							<dd>##IM_DETAIL_33##</dd>
						{/if}
						
						{if $image.files.download_urls.tif != ""}
							<dt><a href="{$image.files.download_urls.tif}"><img src="{$base_url}resources/themes/{$set_theme}/images/icons/download-tif.gif" alt="" width="40" height="40" border="0" /></a></dt>
							<dd><a href="{$image.files.download_urls.tif}">##IM_DETAIL_34##</a></dd>
							<dd>##IM_DETAIL_35##</dd>
						{/if}
						
						{if $image.files.download_urls.psd != ""}
							<dt><a href="{$image.files.download_urls.psd}"><img src="{$base_url}resources/themes/{$set_theme}/images/icons/download-psd.gif" alt="" width="40" height="40" border="0" /></a></dt>
							<dd><a href="{$image.files.download_urls.psd}">##IM_DETAIL_36##</a></dd>
							<dd>##IM_DETAIL_37##</dd>
						{/if}
						
						{if $image.files.download_urls.eps != ""}
							<dt><a href="{$image.files.download_urls.eps}"><img src="{$base_url}resources/themes/{$set_theme}/images/icons/download-eps.gif" alt="" width="40" height="40" border="0" /></a></dt>
							<dd><a href="{$image.files.download_urls.eps}">##IM_DETAIL_38##</a></dd>
							<dd>##IM_DETAIL_39##</dd>
						{/if}
						
					</dl>
				
				{else}
				
					<h1>##IM_DETAIL_40##</h1>
					
					<p>##IM_DETAIL_41##</p>
				
				{/if}
				
			</div>
			
		</div>
		
		<br clear="all" />
		
	</div>

	<br clear="all" />
		
</div>

<!-- INCLUDE THE TOP HTML TEMPLATE -->
{include_template file="index.layout/simple.footer.tpl"}

