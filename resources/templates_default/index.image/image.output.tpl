{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
	<title>##IM_OUTPUT_01##</title>
	<meta name="generator" content="Pixaria Gallery" />
</head>
<body style="margin:0px; background-color:#FFFFFF;">

{if $set_image_output_format == 12}
	
	<embed src="{$base_url}{$fil_psg_image_comping}?file={$file}&amp;" type="image/svg+xml" pluginspage="http://www.adobe.com/svg/viewer/install/" width="{$width}" height="{$height}" >
	
{elseif $set_image_output_format == 11}

	<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,0,0" width="{$width}" height="{$height}" id="image">
	<param name="allowScriptAccess" value="sameDomain" />
	<param name="movie" value="{$base_url}{$fil_psg_image_comping}?file={$file}&amp;" />
	<param name="quality" value="high" />
	<param name="bgcolor" value="#FFFFFF" />
	<embed src="{$base_url}{$fil_psg_image_comping}?file={$file}&amp;" quality="high" bgcolor="#FFFFFF" width="{$width}" height="{$height}" name="image" allowScriptAccess="sameDomain" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" />
	</object>
			
{else}

	<img src="{$base_url}{$fil_psg_image_comping}?file={$file}&amp;" width="{$width}" height="{$height}" alt="##IM_OUTPUT_02##" title="##IM_OUTPUT_01##" {if $set_func_disable_saving}ondragstart="javascript:return false;" oncontextmenu="javascript:return false;"{/if} />

{/if}

</body>
</html>