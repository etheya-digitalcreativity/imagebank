{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}


{*

<!--

This template is used to show pages of image thumbnails
and is called by templates used in the search engine,
gallery and lightbox pages.

-->

*}

{if $thumbnails != false}

	<table class="table-thumbnail">
	
	<thead>
		
		{if $view_mode == "gallery"}
			
			<tr>
			<th>##IM_THUMBS_01##</th>
			</tr>
		
		{elseif $view_mode == "lightbox"}
			
			<tr>
			<th>##IM_THUMBS_02##</th>
			</tr>
		
		{elseif $view_mode == "search"}
			
			<tr>
			<th>##IM_THUMBS_03##</th>
			</tr>
		
		{else}
			
			<tr>
			<th>##IM_THUMBS_04##</th>
			</tr>
		
		{/if}
		
	</thead>
	
	<tbody>
	
	{if $set_func_lightbox == 1 && ($set_thumb_notes == 11 || $view_mode == "lightbox")}
				
		<tr>
		<td class="options">
			
			<div class="option-title">##IM_THUMBS_05## </div>
			
			<div class="pagination">
			
			{if $set_thumb_notes == 11}
			
				{if $view_mode != "lightbox" && !$lightbox_is_mine}
				<a href="javascript:addSelectionToLightbox();" rel="nofollow">##IM_THUMBS_06##</a>
				{else}
				<a href="javascript:removeSelectionFromLightbox();" rel="nofollow">##IM_THUMBS_07##</a>
				{/if}
				
				<a href="javascript:selectAllImages('images[]');" rel="nofollow">##IM_THUMBS_08##</a>
				<a href="javascript:deselectAllImages('images[]');" rel="nofollow">##IM_THUMBS_09##</a>
			
			{/if}
			
			{if $view_mode == "lightbox"}
			<!-- We are viewing a lightbox -->
			
				{if $lightbox_is_mine}
				<!-- This lightbox belongs to the logged in user so they can edit it -->
				 <a href="{$base_url}{$fil_index_lightbox}?cmd=showDefaultLightboxEditor&amp;lid={$lightbox_id}" title="##IM_THUMBS_11##">##IM_THUMBS_10##</a>
				 <a href="{$base_url}{$fil_index_lightbox}?cmd=showLightboxList" title="##IM_THUMBS_13##">##IM_THUMBS_12##</a>
				 {if $ses_psg_full_user}<a href="{$base_url}{$fil_index_lightbox}?cmd=formSendLightbox&amp;lid={$lightbox_id}">##IM_THUMBS_14##</a>{/if}
				{/if}
			
				{if $set_func_contact_sheet && $lightbox_pdf_url != ""}
				<!-- The user can download a PDF contact sheet of this lightbox -->
				 <a href="{$lightbox_pdf_url}" title="##IM_THUMBS_16##">##IM_THUMBS_15##</a>
				{/if}
			
			{/if}
			</div>
		
		</td>
		
		</tr>
	
	{/if}
	
	<tr>
	<td class="options">
	
		<div class="option-title">##IM_THUMBS_17## </div>
		
		{if $set_default_thumb_size == "both"}
		<div class="pagination">
			
			<a href="javascript:void(0);" onclick="javascript:toggleLarge();updateCookie(3,'large','1|{$set_thumb_per_page}|{$set_gallery_per_page}|{$set_view_thumb_view}');" rel="nofollow">##IM_THUMBS_18##</a>
			
			<a href="javascript:void(0);" onclick="javascript:toggleSmall();updateCookie(3,'small','1|{$set_thumb_per_page}|{$set_gallery_per_page}|{$set_view_thumb_view}');" rel="nofollow">##IM_THUMBS_19##</a>
			
		</div>
		{/if}
			
			
		<div class="pagination">
			
			<form action="{$base_page}" method="post" onsubmit="return false;">
			<input type="text" size="3" maxlength="3" id="thumb_per_page" name="thumb_per_page" value="{$set_thumb_per_page}" class="page-button" />
			<a href="{$base_page}" onclick="javascript:if (document.getElementById('thumb_per_page').value != '0'){literal} { {/literal}updateCookie(1,document.getElementById('thumb_per_page').value,'1|{$set_thumb_per_page}|{$set_gallery_per_page}|{$set_view_thumb_view}'); {literal}} else {{/literal} alert('##IM_THUMBS_20##'); return false; {literal}}{/literal}">##IM_THUMBS_21##</a>
			</form>
			
		</div>			
	</td>
	</tr>
	
	<tr>
	<td class="options">

		<div class="option-title">##IM_THUMBS_22## </div>
			
		<div class="pagination">
		
			{section name="ipages" loop=$ipage_numbers}
			
				{if $ipage_numbers[ipages] == $ipage_current}
				
					<a href="{$ipage_links[ipages]}" class="current">{$ipage_numbers[ipages]}</a>
				
				{else}
				
					<a href="{$ipage_links[ipages]}">{$ipage_numbers[ipages]}</a>
				
				{/if}
			
			{/section}
			
		</div>
	
	</td>
	</tr>

	<tr>
	<td>
	
	
		{if $set_thumb_notes == 10}
			
			<!-- SHOW THE BASIC THUMBNAIL VIEW -->
			
			{if $set_default_thumb_size == "both" || $set_default_thumb_size == "large"}
			<div id="image-large"{if $set_view_thumb_view == "small" && $set_default_thumb_size == "both"} style="display:none;"{/if}>
		
			{section name="tn" loop=$thumbnails}
			
				<div class="thumb-lrg" style="width:190px; height:260px;">
				
				<table>
				
				<thead>
				<tr>
				<th colspan="2"><a href="{$thumbnails[tn].url_default}"><img src="{$thumbnails[tn].files.large_url}" class="thumbnail" {$thumbnails[tn].files.large_size.2} alt="{$thumbnails[tn].basic.caption}" title="{$thumbnails[tn].basic.caption}" /></a></th>
				</tr>
				</thead>
				
				<tbody>
				
				<tr>
				<th>##IM_THUMBS_27##</th>
				<td><a href="{$thumbnails[tn].url_default}" title="{$thumbnails[tn].basic.title}" class="plain">{$thumbnails[tn].basic.title|truncate:20:"...":true}</a></td>
				</tr>

				<tr>
				<th>##IM_THUMBS_28##</th>
				<td>
				
				{if $set_func_simple_slideshow}
				<a href="{$thumbnails[tn].files.comp_url}" rel="lightbox[gallery]" title="{$thumbnails[tn].basic.caption}" onclick="javascript:return false;" rel="nofollow">##IM_THUMBS_23##</a><br />
				{/if}
				
				<a href="{$thumbnails[tn].files.download_comp}" rel="nofollow">##IM_THUMBS_24##</a>				
				
				{if $set_func_lightbox == 1}
				{if $view_mode != "lightbox" && !$lightbox_is_mine}
				<a href="{$thumbnails[tn].basic.lightbox_add.url_default}" title="##IM_THUMBS_25##" rel="nofollow">##IM_THUMBS_25##</a><br />
				{else}
				<a href="{$thumbnails[tn].basic.lightbox_remove}" title="##IM_THUMBS_26##" rel="nofollow">##IM_THUMBS_26##</a><br />
				{/if}
				{/if}
				
				</td>
				</tr>

				</tbody>
				
				</table>
				
				</div>
				
			{/section}
					
			<br clear="all"/>
					
			</div>
			{/if}
			
			{if $set_default_thumb_size == "both" || $set_default_thumb_size == "small"}
			<div id="image-small"{if $set_view_thumb_view == "large" && $set_default_thumb_size == "both"} style="display:none;"{/if}>
				
			{section name="tn" loop=$thumbnails}
			
				<div class="thumb-sml" style="width:190px; height:180px;">
				
				<table>
				
				<thead>
				<tr>
				<th colspan="2"><a href="{$thumbnails[tn].url_default}"><img src="{$thumbnails[tn].files.small_url}" class="thumbnail" {$thumbnails[tn].files.small_size.2} alt="{$thumbnails[tn].basic.caption}" title="{$thumbnails[tn].basic.caption}" /></a></th>
				</tr>
				</thead>
				
				<tbody>
				
				<tr>
				<th>##IM_THUMBS_27##</th>
				<td><a href="{$thumbnails[tn].url_default}" title="{$thumbnails[tn].basic.title}" class="plain">{$thumbnails[tn].basic.title|truncate:20:"...":true}</a></td>
				</tr>

				<tr>
				<th>##IM_THUMBS_28##</th>
				<td>
				
				{if $set_func_simple_slideshow}
				<a href="{$thumbnails[tn].files.comp_url}" rel="lightbox[gallery]" title="{$thumbnails[tn].basic.caption}" onclick="javascript:return false;" rel="nofollow">##IM_THUMBS_23##</a><br />
				{/if}
				
				<a href="{$thumbnails[tn].files.download_comp}" rel="nofollow">##IM_THUMBS_24##</a>		
				
				{if $set_func_lightbox == 1}
				{if $view_mode != "lightbox" && !$lightbox_is_mine}
				<a href="{$thumbnails[tn].basic.lightbox_add.url_default}" title="##IM_THUMBS_25##" rel="nofollow">##IM_THUMBS_25##</a><br />
				{else}
				<a href="{$thumbnails[tn].basic.lightbox_remove}" title="##IM_THUMBS_26##" rel="nofollow">##IM_THUMBS_26##</a><br />
				{/if}
				{/if}
				
				</td>
				</tr>

				</tbody>
				
				</table>
				
				</div>
				
			{/section}
					
			<br clear="all"/>
					
			</div>
			{/if}
			
		{else}
		
			<!-- SHOW THE DETAILED THUMBNAIL VIEW -->
			
			<!-- START OF LARGE THUMBNAIL SECTION -->
			{if $set_default_thumb_size == "both" || $set_default_thumb_size == "large"}
			<div id="image-large"{if $set_view_thumb_view == "small" && $set_default_thumb_size == "both"} style="display:none;"{/if}>
		
			<form method="post" id="thumb_grid_large" name="thumb_grid_large" action="{$base_url}{$fil_index_lightbox}" style="margin-top:0px; margin-bottom:0px;">
		
			{if $view_mode=="lightbox" && $lightbox_is_mine}
			
				<input type="hidden" name="cmd" value="removeImagesFromLightbox" />
			
			{else}
			
				<input type="hidden" name="cmd" value="addImagesToLightbox" />
			
			{/if}
			
			{section name="tn" loop=$thumbnails}
			
				<div class="thumb-lrg" style="width:200px; height:340px;">
				
				<table>
				
				<thead>
				<tr>
				<th colspan="2"><a href="{$thumbnails[tn].url_default}"><img src="{$thumbnails[tn].files.large_url}" class="thumbnail" {$thumbnails[tn].files.large_size.2} alt="{$thumbnails[tn].basic.caption}" title="{$thumbnails[tn].basic.caption}" /></a></th>
				</tr>
				</thead>
				
				<tbody>
				<tr>
				<th>##IM_THUMBS_27##</th>
				<td><a href="{$thumbnails[tn].url_default}" title="{$thumbnails[tn].basic.title}" class="plain">{$thumbnails[tn].basic.title|truncate:20:"...":true}</a></td>
				</tr>
				
				<tr>
				<th>##IM_THUMBS_29##</th>
				<td>{$thumbnails[tn].basic.date|date_format:"%B %e, %Y"}</td>
				</tr>
				
				<tr>
				<th>##IM_THUMBS_30##</th>
				<td>{$thumbnails[tn].basic.file_name|truncate:20:"...":true}</td>
				</tr>
				
				{if $set_func_lightbox == 1}
				
					<tr>
					<th>##IM_THUMBS_31##</th>
					<td><input type="checkbox" name="images[]" value="{$thumbnails[tn].basic.id}" style="font-size:0.8em;" class="forminput" /></td>
					</tr>
				
				{/if}
				
				<tr>
				<th>##IM_THUMBS_28##</th>
				<td>
				
				{if $thumbnails[tn].basic.cart_link == 1}<a href="{$thumbnails[tn].basic.cart_add.url_default}" title="##IM_THUMBS_32##">##IM_THUMBS_32##</a><br />{/if}
				
				{if $set_func_lightbox == 1}
				{if $view_mode != "lightbox" && !$lightbox_is_mine}
				<a href="{$thumbnails[tn].basic.lightbox_add.url_default}" title="##IM_THUMBS_25##" rel="nofollow">##IM_THUMBS_25##</a><br />
				{else}
				<a href="{$thumbnails[tn].basic.lightbox_remove}" title="##IM_THUMBS_26##" rel="nofollow">##IM_THUMBS_26##</a><br />
				{/if}
				{/if}
				
				{if $set_func_simple_slideshow}
				<a href="{$thumbnails[tn].files.comp_url}" rel="lightbox[gallery]" title="{$thumbnails[tn].basic.caption}" onclick="javascript:return false;" rel="nofollow">##IM_THUMBS_23##</a><br />
				{/if}
				
				<a href="{$thumbnails[tn].files.download_comp}" rel="nofollow">##IM_THUMBS_24##</a>				
				</td>
				</tr>

				</tbody>

				</table>
				
				</div>
				
			{/section}
					
			<br clear="all"/>
			
			</form>
			
			</div>
			{/if}
			<!-- END OF LARGE THUMBNAIL SECTION -->
			
			<!-- START OF SMALL THUMBNAIL SECTION -->
			{if $set_default_thumb_size == "both" || $set_default_thumb_size == "small"}
			<div id="image-small"{if $set_view_thumb_view == "large" && $set_default_thumb_size == "both"} style="display:none;"{/if}>
				
			<form method="post" id="thumb_grid_small" name="thumb_grid_small" action="{$base_url}{$fil_index_lightbox}" style="margin-top:0px; margin-bottom:0px;">
		
			{if $view_mode=="lightbox" && $lightbox_is_mine}
			
				<input type="hidden" name="cmd" value="removeImagesFromLightbox" />
			
			{else}
			
				<input type="hidden" name="cmd" value="addImagesToLightbox" />
			
			{/if}
			
			{section name="tn" loop=$thumbnails}
			
				<div class="thumb-sml" style="width:190px; height:270px;">
				
				<table>
				
				<thead>
				<tr>
				<th colspan="2"><a href="{$thumbnails[tn].url_default}"><img src="{$thumbnails[tn].files.small_url}" class="thumbnail" {$thumbnails[tn].files.small_size.2} alt="{$thumbnails[tn].basic.caption}" title="{$thumbnails[tn].basic.caption}" /></a></th>
				</tr>
				</thead>
				
				<tbody>
				<tr>
				<th>##IM_THUMBS_27##</th>
				<td><a href="{$thumbnails[tn].url_default}" title="{$thumbnails[tn].basic.title}" class="plain">{$thumbnails[tn].basic.title|truncate:20:"...":true}</a></td>
				</tr>
				
				<tr>
				<th>##IM_THUMBS_29##</th>
				<td>{$thumbnails[tn].basic.date|date_format:"%B %e, %Y"}</td>
				</tr>
				
				<tr>
				<th>##IM_THUMBS_30##</th>
				<td>{$thumbnails[tn].basic.file_name|truncate:20:"...":true}</td>
				</tr>
				
				{if $set_func_lightbox == 1}
				
					<tr>
					<th>##IM_THUMBS_31##</th>
					<td><input type="checkbox" name="images[]" value="{$thumbnails[tn].basic.id}" style="font-size:0.8em;" class="forminput" /></td>
					</tr>
				
				{/if}
				
				<tr>
				<th>##IM_THUMBS_28##</th>
				<td>
				{if $thumbnails[tn].basic.cart_link == 1}<a href="{$thumbnails[tn].basic.cart_add.url_default}" title="##IM_THUMBS_32##" rel="nofollow">##IM_THUMBS_32##</a><br />{/if}
				{if $set_func_lightbox == 1}
				{if $view_mode != "lightbox" && !$lightbox_is_mine}
				<a href="{$thumbnails[tn].basic.lightbox_add.url_default}" title="##IM_THUMBS_25##" rel="nofollow">##IM_THUMBS_25##</a><br />
				{else}
				<a href="{$thumbnails[tn].basic.lightbox_remove}" title="##IM_THUMBS_26##" rel="nofollow">##IM_THUMBS_26##</a><br />
				{/if}
				{/if}

				{if $set_func_simple_slideshow}
				<a href="{$thumbnails[tn].files.comp_url}" rel="lightbox[gallery]" title="{$thumbnails[tn].basic.caption}" onclick="javascript:return false;" rel="nofollow">##IM_THUMBS_23##</a><br />
				{/if}
				
				<a href="{$thumbnails[tn].files.download_comp}" rel="nofollow">##IM_THUMBS_24##</a>

				</td>
				</tr>

				</tbody>
				
				</table>
				
				</div>
				
			{/section}
					
			<br clear="all"/>
			
			</form>
			
			</div>
			{/if}
			<!-- END OF SMALL THUMBNAIL SECTION -->
		
		{/if}
	
	
	
	</td>
	</tr>
	
	<tr>
	<td class="options">

		<div class="option-title">##IM_THUMBS_22## </div>
			
		<div class="pagination">
		
			{section name="ipages" loop=$ipage_numbers}
			
				{if $ipage_numbers[ipages] == $ipage_current}
				
					<a href="{$ipage_links[ipages]}" class="current">{$ipage_numbers[ipages]}</a>
				
				{else}
				
					<a href="{$ipage_links[ipages]}">{$ipage_numbers[ipages]}</a>
				
				{/if}
			
			{/section}
			
		</div>
	
	</td>
	</tr>

	</tbody>
	
	</table>

{/if}
