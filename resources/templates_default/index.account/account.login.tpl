{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}





<table class="table-text">
<tr>
<th>##LOGIN_002##</th>

<td>

<h1>##LOGIN_003##</h1>

<p>##LOGIN_004##</p>

<p><a href="{$base_url}{$fil_index_account}?cmd=userRegistrationStart">##LOGIN_014##</a></p>

<h1>##LOGIN_005##</h1>

{if $output_problem == "1" }<p class="html-error-warning">{$output_problem_status}{/if}

<p>##LOGIN_006##</p>

<p>##LOGIN_007##</p>

</td>
<td>&nbsp;</td>
</tr>

</table>





<form action="{$sys_base_url}{$fil_index_login}" method="post">

<input type="hidden" name="referer" value="{$output_referer}" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">##LOGIN_008##</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>##LOGIN_009##:</th>
<td><input type="text" maxlength="50" size="20" name="email_address" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

{if $set_login_memory}
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>##LOGIN_010##:</th>
	<td><input type="password" maxlength="50" size="20" name="password" style="width:100%;" class="forminput" /></td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>##LOGIN_011##:</th>
	<td><input type="checkbox" name="cookie_persist" class="forminput" /></td>
	<td>&nbsp;</td>
	</tr>
	
{else}

	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>##LOGIN_012##:</th>
	<td><input type="password" maxlength="50" size="20" name="password" style="width:100%;" class="forminput" /></td>
	<td>&nbsp;</td>
	</tr>
	
{/if}

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="submit" name="submit" value="##LOGIN_013##" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>





<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="index.layout/main.footer.tpl"}