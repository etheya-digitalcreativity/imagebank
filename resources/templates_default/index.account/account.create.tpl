{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}





<table class="table-text">

<tr>
<th>##ACCOUNT_001##</th>
<td>

{if $problem || $captcha_failure}

	<h1>##ACCOUNT_002##</h1>
	
	<p>##ACCOUNT_003##</p>

	<ol>
	
	{if $captcha_failure}
		<li class="html-error-warning">##ACCOUNT_004##</li>
	{/if}
	
	{section name="problems" loop=$profile_errors}
		
		<li class="html-error-warning">{$profile_errors[problems]}</li>
	
	{/section}
	</ol>
	
{else}

	<h1>##ACCOUNT_005##</h1>
	
	<p>##ACCOUNT_006##</p>

{/if}


</td>
<td>&nbsp;</td>
</tr>


</table>





<table>

<form action="{$base_url}{$fil_index_account}" method="post">

<input type="hidden" name="cmd" value="userRegistrationComplete" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">##ACCOUNT_007##</th>
</tr>

</thead>

<tbody>

{if $data_salutation == "1"}
<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_008##</th>
<td><input type="text" name="formal_title" style="width:100%;" class="forminput" value="{$formal_title}" /></td>
<td>{if $data_salutation_req == "1"} <span class="required">##ACCOUNT_009##</span>{/if}</td>
</tr>
{/if}

<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_010##</th>
<td><input type="text" maxlength="50" size="30" name="first_name" value="{$first_name}" style="width:100%;" class="forminput" /></td>
<td> <span class="required">##ACCOUNT_009##</span></td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_011##</th>
<td><input type="text" maxlength="50" size="30" name="family_name" value="{$family_name}" style="width:100%;" class="forminput" /></td>
<td> <span class="required">##ACCOUNT_009##</span></td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_012##</th>
<td>
<input type="text" maxlength="50" size="30" name="email_address" value="{$email_address}" style="width:100%;" class="forminput" />
<input type="hidden" name="email_address_old" value="{$email_address}" /></td>
<td> <span class="required">##ACCOUNT_009##</span></td>
</tr>

{if $data_telephone == 1}
<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_013##</th>
<td><input type="text" maxlength="50" size="30" name="telephone" value="{$telephone}" style="width:100%;" class="forminput" /></td>
<td>{if $data_telephone_req == "1"} <span class="required">##ACCOUNT_009##</span>{/if}</td>
</tr>
{/if}
	
{if $data_mobile == 1}
<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_014##</th>
<td><input type="text" maxlength="50" size="30" name="mobile" value="{$mobile}" style="width:100%;" class="forminput" /></td>
<td>{if $data_mobile_req == "1"} <span class="required">##ACCOUNT_009##</span>{/if}</td>
</tr>
{/if}
	
{if $data_facsimile == 1}
<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_015##</th>
<td><input type="text" maxlength="50" size="30" name="fax" value="{$fax}" style="width:100%;" class="forminput" /></td>
<td>{if $data_facsimile_req == "1"} <span class="required">##ACCOUNT_009##</span>{/if}</td>
</tr>
{/if}
	
{if $data_business_type == 1}
<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_016##</th>
<td><input type="text" maxlength="500" size="60" name="other_business_type" value="{$other_business_type}" style="width:100%;" class="forminput" /></td>
</td>
<td>{if $data_business_req == "1"} <span class="required">##ACCOUNT_009##</span>{/if}</td>
</tr>
{/if}

{if $data_company_name == 1}
<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_017##</th>
<td><input type="text" maxlength="500" size="60" name="other_company_name" value="{$other_company_name}" style="width:100%;" class="forminput" /></td>
</td>
<td>{if $data_company_name_req == "1"} <span class="required">##ACCOUNT_009##</span>{/if}</td>
</tr>
{/if}

{if $data_business_position == 1}
<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_018##</th>
<td><input type="text" maxlength="500" size="60" name="other_business_position" value="{$other_business_position}" style="width:100%;" class="forminput" /></td>
</td>
<td>{if $data_business_position_req == "1"} <span class="required">##ACCOUNT_009##</span>{/if}</td>
</tr>
{/if}

{if $data_image_interest == 1}
<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>##ACCOUNT_019##</th>
<td><textarea rows="4" name="other_image_interest" style="width:100%;" class="forminput" onKeyDown="textCounter(this.form.other_image_interest,this.form.other_image_interestleft,255);" onKeyUp="textCounter(this.form.other_image_interest,this.form.other_image_interestleft,255);">{$other_image_interest}</textarea></td>
</td>
<td>{if $data_image_interest_req == "1"} <span class="required">##ACCOUNT_009##</span>{/if}</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input readonly="readonly" type="text" name="other_image_interestleft" size="3" maxlength="3" value="255" tabindex="101" class="forminput"> ##ACCOUNT_020##</td>
<td>&nbsp;</td>
</tr>
{/if}

{if $data_frequency == 1}
<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_021##</th>
<td><input type="text" maxlength="500" size="60" name="other_frequency" value="{$other_frequency}" style="width:100%;" class="forminput" /></td>
</td>
<td>{if $data_frequency_req == "1"} <span class="required">##ACCOUNT_009##</span>{/if}</td>
</tr>
{/if}

{if $data_circulation == 1}
<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_022##</th>
<td><input type="text" maxlength="500" size="60" name="other_circulation" value="{$other_circulation}" style="width:100%;" class="forminput" /></td>
</td>
<td>{if $data_circulation_req == "1"} <span class="required">##ACCOUNT_009##</span>{/if}</td>
</tr>
{/if}

{if $data_territories == 1}
<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_023##</th>
<td><input type="text" maxlength="500" size="60" name="other_territories" value="{$other_territories}" style="width:100%;" class="forminput" /></td>
</td>
<td>{if $data_territories_req == "1"} <span class="required">##ACCOUNT_009##</span>{/if}</td>
</tr>
{/if}

{if $data_website == 1}
<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_024##</th>
<td><input type="text" maxlength="500" size="60" name="other_website" value="{$other_website}" style="width:100%;" class="forminput" />
{if $data_website_req == "1"} <span class="required">##ACCOUNT_009##</span>{/if}</td>
</td>
<td>&nbsp;</td>
</tr>
{/if}

</tbody>

</table>



{if $data_address == 1}
<table class="table-form">

<thead>

<tr>
<th colspan="3">Address information:</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_025##</th>
<td><input type="text" maxlength="50" size="30" name="addr1" value="{$addr1}" style="width:100%;" class="forminput" /></td>
<td>{if $data_address_req == "1"} <span class="required">##ACCOUNT_009##</span>{/if}</td>
</tr>
	
<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="text" maxlength="50" size="30" name="addr2" value="{$addr2}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>
	
<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="text" maxlength="50" size="30" name="addr3" value="{$addr3}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>
	
<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_026##</th>
<td><input type="text" maxlength="50" size="30" name="city" value="{$city}" style="width:100%;" class="forminput" /></td>
<td>{if $data_address_req == "1"} <span class="required">##ACCOUNT_009##</span>{/if}</td>
</tr>
	
<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_027##</th>
<td><input type="text" maxlength="50" size="30" name="region" value="{$region}" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>
	
<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_028##</th>
<td>

	<select name="country" style="width:100%;" class="forminput">
	
		<option value="">##ACCOUNT_029##</option>
		
		<option value=""></option>
	
		<option value="{$iso_codes.12}">{$printable_names.12}</option>
		<option value="{$iso_codes.20}">{$printable_names.20}</option>
		<option value="{$iso_codes.57}">{$printable_names.57}</option>
		<option value="{$iso_codes.72}">{$printable_names.72}</option>
		<option value="{$iso_codes.79}">{$printable_names.79}</option>
		<option value="{$iso_codes.102}">{$printable_names.102}</option>
		<option value="{$iso_codes.104}">{$printable_names.104}</option>
		<option value="{$iso_codes.149}">{$printable_names.149}</option>
		<option value="{$iso_codes.152}">{$printable_names.152}</option>
		<option value="{$iso_codes.159}">{$printable_names.159}</option>
		<option value="{$iso_codes.198}">{$printable_names.198}</option>
		<option value="{$iso_codes.204}">{$printable_names.204}</option>
		<option value="{$iso_codes.224}">{$printable_names.224}</option>
		<option value="{$iso_codes.225}">{$printable_names.225}</option>
		
		<option value=""></option>
	
	{section name="country" loop=$iso_codes}

		{if $country == $iso_codes[country]}
		
			<option value="{$iso_codes[country]}" selected="selected">{$printable_names[country]}</option>
		
		{elseif $country == $printable_names[country]}
		
			<option value="{$iso_codes[country]}" selected="selected">{$printable_names[country]}</option>
		
		{else}
		
			<option value="{$iso_codes[country]}">{$printable_names[country]}</option>
		
		{/if}

	{/section}
	
	</select>

</td>
<td>&nbsp;</td>
</tr>
	
<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_030##</td>
<td><input type="text" maxlength="50" size="30" name="postal_code" value="{$postal_code}" style="width:100%;" class="forminput" /></td>
<td>{if $data_address_req == "1"} <span class="required">##ACCOUNT_009##</span>{/if}</td>
</tr>

</tbody>

</table>
{/if}




<table class="table-form">

<thead>

<tr>
<th colspan="3">##ACCOUNT_031##</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>##ACCOUNT_032##</th>
<td valign="top"><input type="password" maxlength="50" size="30" name="password_1" value="{$password_1}" style="width:100%;" class="forminput" /></td>
<td> <span class="required">##ACCOUNT_009##</span></td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td valign="top">##ACCOUNT_033##</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_034##</th>
<td><input type="password" maxlength="50" size="30" name="password_2" value="{$password_2}" style="width:100%;" class="forminput" /></td>
<td> <span class="required">##ACCOUNT_009##</span></td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_035##</th>
<td><input type="text" maxlength="500" size="60" name="password_rem_que" value="{$password_rem_que}" style="width:100%;" class="forminput" /></td>
<td> <span class="required">##ACCOUNT_009##</span></td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_036##</th>
<td><input type="text" maxlength="500" size="60" name="password_rem_ans" value="{$password_rem_ans}" style="width:100%;" class="forminput" /></td>
<td> <span class="required">##ACCOUNT_009##</span></td>
</tr>

{if $set_func_captcha_registration}
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>##ACCOUNT_037##</th>
	<td>
	##ACCOUNT_038##<br /><br />
	<img src="{$base_url}index.captcha.php?key={$captcha_key}" width="332" height="60" alt="##ACCOUNT_039##" />
	<input type="hidden" name="captcha_key" value="{$captcha_key}" />
	</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>&nbsp;</th>
	<td><input type="text" maxlength="5" size="60" name="captcha_code" style="width:50px;" class="forminput" /> <span class="required">##ACCOUNT_009##</span></td>
	<td></td>
	</tr>
{/if}

{if $data_message != 1}
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td><input type="submit" name="submit" value="##ACCOUNT_040##" style="width:100%;" class="formbutton" /></td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	
	</table>
	
	</form>

{else}

	</tbody>
	
	</table>

	<table class="table-form">
	
	<thead>
	
	<tr>
	<th colspan="3">##ACCOUNT_041##</th>
	</tr>
	
	</thead>
	
	<tbody>
	
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>##ACCOUNT_042##</th>
	<td><textarea rows="4" name="other_message" style="width:100%;" class="forminput" onKeyDown="textCounter(this.form.other_message,this.form.other_message_textleft,255);" onKeyUp="textCounter(this.form.other_message,this.form.other_message_textleft,255);"></textarea>
	{if $data_message_req == "1"} <span class="required">##ACCOUNT_009##</span>{/if}</td>
	</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two' advance='0'}">
	<th>&nbsp;</td>
	<td><input readonly="readonly" type="text" name="other_message_textleft" size="3" maxlength="3" value="255" tabindex="101" class="forminput"> ##ACCOUNT_020##</td>
	<td>&nbsp;</td>
	</tr>
	
	<tr class="{cycle values='list-one,list-two'}">
	<th>&nbsp;</th>
	<td><input type="submit" name="submit" value="##ACCOUNT_040##" style="width:100%;" class="formbutton" /></td>
	<td>&nbsp;</td>
	</tr>
	
	</tbody>
	</tbody>
	
	</table>
	
	</form>

{/if}




<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->

{include_template file="index.layout/main.footer.tpl"}

