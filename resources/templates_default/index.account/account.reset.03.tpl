{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}





<table class="table-text">

<tr>
<th>##ACCOUNT_070##</th>
<td>

{if $output_problem}

	<h1>##ACCOUNT_071##</h1>
	
	<p>##ACCOUNT_072##</p>

	<ul>
	{section name="problems" loop=$output_problem_status}
		
		<li class="html-error-warning">{$output_problem_status[problems]}</li>
	
	{/section}
	</ul>
	
{else}

	<h1>##ACCOUNT_073##</h1>
	
	<p>##ACCOUNT_074##</p>

{/if}

</td>
<td>&nbsp;</td>
</tr>

</table>





<form action="{$base_url}{$fil_index_account}" method="post">

<input type="hidden" name="cmd" value="resetPasswordNewPassword" />

<input type="hidden" name="email_address" value="{$email_address}" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">##ACCOUNT_075##</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_076##</th>
<td><input type="password" maxlength="50" size="20" name="password_1" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>


<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>##ACCOUNT_077##</th>
<td><input type="password" maxlength="50" size="20" name="password_2" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td><input type="submit" name="submit" value="##ACCOUNT_062##" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>




<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="index.layout/main.footer.tpl"}


