{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}


<table class="table-text">

<tr>
<th>##ACCOUNT_096##</th>
<td>

<h1>##ACCOUNT_097##</h1>

<p>##ACCOUNT_098##</p>

{if !$ses_psg_account_status}{include_template file="index.snippets/notification.note.tpl" form_title_text="##ACCOUNT_099##"}{/if}

<p><a href="{$base_url}{$fil_index_account}?cmd=editUserAccount">##ACCOUNT_100##</a></p>

<p><a href="{$base_url}{$fil_index_lightbox}?cmd=showLightboxList">##ACCOUNT_101##</a></p>

{if $ses_psg_photographer}

	<p><a href="{$base_url}{$fil_index_cms}">##ACCOUNT_102##</a></p>
	
	<p><a href="{$base_url}{$fil_index_cms}?cmd=uploadNewImages">##ACCOUNT_103##</a></p>

{/if}

</td>
<td>&nbsp;</td>
</tr>

</table>

{if $carts_present == TRUE && $set_func_store == TRUE && $sys_store_enabled}

	<table class="table-listbox">
	
	<tr>
		<th>##ACCOUNT_104##</td>	
		<th>##ACCOUNT_105##</td>	
		<th>##ACCOUNT_106##</td>	
		<th>##ACCOUNT_107##</td>	
		<th>##ACCOUNT_108##</td>	
		<th>##ACCOUNT_109##</td>	
	</tr>

		{section name="carts" loop="$cart_id"}
		
				<tr class="{cycle values='list-one,list-two'}">
				
				<td style="height:30px;">{$cart_status_text[carts]}</td>
									
				<td>{$cart_items[carts]}</td>
									
				<td>{if $date_created[carts] != "0000-00-00 00:00:00"}{$date_created[carts]|date_format:"%B %e, %Y %H:%M"}{/if}</td>
									
				<td>{if $date_processed[carts] != "0000-00-00 00:00:00"}{$date_processed[carts]|date_format:"%B %e, %Y %H:%M"}{else}##ACCOUNT_110##{/if}</td>
									
				<td>{if $date_completed[carts] != "0000-00-00 00:00:00"}{$date_completed[carts]|date_format:"%B %e, %Y %H:%M"}{else}##ACCOUNT_111##{/if}</td>
									
				<td>
				
				<ul class="actionlist">
				<li><a href="{$cart_view_url[carts]}" title="##ACCOUNT_113##">##ACCOUNT_112##</a></lI>
				</ul>
				
				</td>
				
				</tr>
							
		{/section}
		
	</table>

{/if}

{include_template file="index.layout/main.footer.tpl"}

