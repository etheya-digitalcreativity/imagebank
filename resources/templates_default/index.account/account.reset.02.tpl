{*

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

*}{include_template file="index.layout/main.header.tpl"}





<table class="table-text">

<tr>
<th>##ACCOUNT_063##</th>
<td>

<h1>##ACCOUNT_064##</h1>

<p>##ACCOUNT_065##</p>

{if $output_problem == "1" }

	<ul>
	<li class="html-error-warning">##ACCOUNT_066##</li>
	</ul>
	
{/if}

</td>
<td>&nbsp;</td>
</tr>

</table>






<form action="{$base_url}{$fil_index_account}" method="post">

<input type="hidden" name="cmd" value="resetPasswordAnswerQuestion" />

<input type="hidden" name="email_address" value="{$email_address}" />

<table class="table-form">

<thead>

<tr>
<th colspan="3">##ACCOUNT_067##</th>
</tr>

</thead>

<tbody>

<tr class="{cycle values='list-one,list-two'}">
<th>##ACCOUNT_068##</th>
<td>{$password_rem_que}</td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two' advance='0'}">
<th>##ACCOUNT_069##</th>
<td><input type="text" maxlength="50" size="20" name="password_rem_ans" style="width:100%;" class="forminput" /></td>
<td>&nbsp;</td>
</tr>

<tr class="{cycle values='list-one,list-two'}">
<th>&nbsp;</th>
<td ><input type="submit" name="submit" value="##ACCOUNT_062##" style="width:100%;" class="formbutton" /></td>
<td>&nbsp;</td>
</tr>

</tbody>

</table>

</form>




<!-- INCLUDE THE BOTTOM HTML TEMPLATE -->
{include_template file="index.layout/main.footer.tpl"}

