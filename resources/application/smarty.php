<?php if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

/*
*
*	INCLUDE ANOTHER TEMPLATE
*
*	This is a Smarty extension function used to include templates from within another template
*	it is required in all scripts in order for Smarty to intialise correctly
*
*/
function smarty_include_template ($params, &$smarty) {

	if (!isset($params['file'])) {
	
		$smarty->trigger_error("eval: missing 'file' parameter");
		return;
		
	} else {
		
		// Run any localisation strings through the translation engine
		$smarty->assign("form_title_text", preg_replace_callback('/\$([a-zA-Z0-9_]*)/', 'smarty_localisation_variable_parser_callback', $params['form_title_text']));
		
		// Security
		if (eregi("http://",$params['file'])) { return; }
		if (eregi("ftp://",$params['file'])) { return; }
		
		$result = smarty_fetch($params['file'], $smarty);
		
	}
	
	return $result;
}

/*
*
*	RENDERS A TEMPLATE AND OUTPUTS THE CONTENTS TO THE BROWSER
*
*	Checks to see if template file is in the theme directory and
*	if it isn't loads the default template file instead
*/
function smarty_display ($template, &$smarty) {
	
	global $cfg, $smarty, $admin_page;
	
	// Security
	if (eregi("http://",$template)) { return; }
	if (eregi("ftp://",$template)) { return; }
	
	if ($admin_page) {
	
		$smarty->display($template);
		
	} else {
	
		if (file_exists(SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/templates/".$template)) {
			
			$smarty->display(SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/templates/".$template);
		
		} else {
			
			$smarty->display($template);
		
		}
	
	}
	
}

/*
*
*	FETCHES A TEMPLATE AND RETURNS THE PARSED OUTPUT
*
*	Checks to see if template file is in the theme directory and
*	if it isn't loads the default template file instead
*
*/
function smarty_fetch ($template, &$smarty) {

	global $cfg, $admin_page;
	
	// Security
	if (eregi("http://",$template)) { return; }
	if (eregi("ftp://",$template)) { return; }
	
	if ($admin_page) {
	
		$result = $smarty->fetch($template);
		
	} else {
		
		if (file_exists(SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/templates/".$template)) {
			
			$result = $smarty->fetch(SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/templates/".$template);
		
		} else {
			
			$result = $smarty->fetch($template);
		
		}
	
	}
	
	return $result;

}

/*
*
*	LOAD A PIXARIA PLUGIN
*
*	This is a Smarty extension function used to load Pixies
*	It is not yet integrated and should not be used
*
*/
function smarty_load_pixie ($params, &$smarty) {

	global $cfg, $ses;

	if (!isset($params['name'])) {
	
		$smarty->trigger_error("eval: Missing 'name' parameter");
		return;
		
	} elseif (!is_dir(SYS_BASE_PATH."resources/pixies/".$params['name'])) {
		
		$smarty->trigger_error("eval: Requested Pixie does not exist");
		return;
		
	} else {
		
		// Set the path to this Pixie
		$pixie_dir = SYS_BASE_PATH."resources/pixies/".$params['name']."/";
		
		// Get the options for this Pixie
		$pixie_params = explode(":",$params['options']);
		
		// Security
		if (eregi("http://",$pixie_dir)) { return; }
		if (eregi("ftp://",$pixie_dir)) { return; }
		
		// Include the main Pixie file
		include($pixie_dir . "pixie.logic.php");
		
	}
	
	return $pixie_return;
	
}

/*
*
*	HTML FORM PLUGIN FOR SELECTING MULTIPLE ITEMS
*
*	Has the same capabilities as a multiple select HTML element
*	except the form elements used are checkboxes in a styled div.
*
*/
function smarty_fancy_select_multiple ($params, &$smarty) {
	
    require_once $smarty->_get_plugin_filepath('shared','escape_special_chars');

    foreach($params as $_key => $_val) {
    
        switch($_key) {
        	
        	case 'style':
        		if ($_val != "") {
        			$$_key = $_val;
        		}
        	break;
        	
       		case 'default_option':
       		case 'default_value':
       		case 'name':
       		case 'rows':
       		case 'width':
        		if ($_val != "") {
        			$$_key = $_val;
        		} else {
                    $smarty->trigger_error("html_fancy_select_multiple: attribute '$_key' must be present", E_USER_NOTICE);
        		}
        	break;
        	
 			case 'selected':
                if(is_array($_val)) {
                    $$_key = smarty_function_escape_special_chars($_val);
                } elseif ($_val == "") {
                	$$_key = array();
                } else {
                    $smarty->trigger_error("html_fancy_select_multiple: extra attribute '$_key' must be an array", E_USER_NOTICE);
                }
 			break;
 			
 			case 'options':
 			case 'values':
                if(is_array($_val)) {
                    $$_key = smarty_function_escape_special_chars($_val);
                } else {
                    $smarty->trigger_error("html_fancy_select_multiple: extra attribute '$_key' must be an array", E_USER_NOTICE);
                }
            break;
            
        }
        
	}
	
	if ($default_option != "") {
		
		if ($selected == "") { $selected = $default_value; }
		
		$default_option = array($default_option);
		$default_value	= array($default_value);
		
		array_merge($default_option,$options);
		array_merge($default_value,$values);
		
	}
	
	if (count($options) < $rows) {
	
		for ($i=0; $i < $rows; $i++) {
			$row[] = $i;
		}
	
	} else {
	
		for ($i=0; $i < count($options); $i++) {
			$row[] = $i;
		}
	
	}
	
	if (is_numeric($rows)) {
		
		$height = $rows * 25;
	
	}
	
	$smarty->assign("fsm_style",$style);
	$smarty->assign("fsm_name",$name);
	$smarty->assign("fsm_rows",$row);
	$smarty->assign("fsm_width",$width);
	$smarty->assign("fsm_height",$height);
	$smarty->assign("fsm_selected",$selected);
	$smarty->assign("fsm_options",$options);
	$smarty->assign("fsm_values",$values);
	
	$template = "admin.snippets/fancy.select.multiple.html";
	
	$smarty->display($template);

}

/*
*
*	HTML FORM PLUGIN FOR SELECTING MULTIPLE ITEMS
*
*	Has the same capabilities as a drop down menu HTML element
*	except the form elements used are radio buttons in a styled div.
*
*/
function smarty_fancy_select_single ($params, &$smarty) {
	
    require_once $smarty->_get_plugin_filepath('shared','escape_special_chars');

    foreach($params as $_key => $_val) {
    
        switch($_key) {
        
        	case 'style':
        		if ($_val != "") {
        			$$_key = $_val;
        		}
        	break;
        	
       		case 'default_option':
       		case 'default_value':
       		case 'name':
       		case 'rows':
       		case 'width':
        		if ($_val != "") {
        			$$_key = $_val;
        		} else {
                    $smarty->trigger_error("html_fancy_select_single: attribute '$_key' must be present", E_USER_NOTICE);
        		}
        	break;
        	
 			case 'selected':
                if ($_val != "") {
                	$$_key = $_val;
                } else {
                    $$_key = "";
                }
 			break;
 			
 			case 'options':
 			case 'values':
                if(is_array($_val)) {
                    $$_key = smarty_function_escape_special_chars($_val);
                } else {
                    $smarty->trigger_error("html_fancy_select_single: extra attribute '$_key' must be an array", E_USER_NOTICE);
                }
            break;
            
        }
        
	}
	
	if ($default_option != "") {
		
		if ($selected == "") { $selected = $default_value; }
		
		$default_option = array($default_option);
		$default_value	= array($default_value);
		
		$options 		= array_merge($default_option,$options);
		$values 		= array_merge($default_value,$values);
		
	}

	if (count($options) < $rows) {
	
		for ($i=0; $i < $rows; $i++) {
			$row[] = $i;
		}
	
	} else {
	
		for ($i=0; $i < count($options); $i++) {
			$row[] = $i;
		}
	
	}
	
	if (is_numeric($rows)) {
		
		$height = $rows * 25;
	
	}
	
	if ($selected == "") { $selected = $values[0]; }
	
	$smarty->assign("fsm_style",$style);
	$smarty->assign("fss_name",$name);
	$smarty->assign("fss_rows",$row);
	$smarty->assign("fss_width",$width);
	$smarty->assign("fss_height",$height);
	$smarty->assign("fss_selected",$selected);
	$smarty->assign("fss_options",$options);
	$smarty->assign("fss_values",$values);
	
	$template = "admin.snippets/fancy.select.single.html";
	
	$smarty->display($template);

}

/*
*
*	FANCY COLUMN HIERARCHICAL VIEW
*
*/
function smarty_fancy_column ($params, &$smarty) {
		
    require_once $smarty->_get_plugin_filepath('shared','escape_special_chars');

	foreach($params as $_key => $_val) {
	
        switch($_key) {
        
       		case 'cols':
       		case 'rows':
       		case 'input_name':
        		if ($_val != "") {
        			$$_key = $_val;
        		} else {
                    $smarty->trigger_error("html_fancy_select_single: attribute '$_key' must be present", E_USER_NOTICE);
        		}
        	break;
        	
 			case 'title':
 			case 'url':
 			case 'data':
                if(is_array($_val)) {
                    $$_key = smarty_function_escape_special_chars($_val);
                } else {
                    $smarty->trigger_error("html_fancy_select_single: extra attribute '$_key' must be an array", E_USER_NOTICE);
                }
            break;
            
        }

	}
	
	if ($cols == '') {
		$cols = 2;
	}
	
	if ($rows == '') {
		$rows = 6;
	}
	
	$smarty->assign("fcv_unique",mt_rand());
	
	$template = "plugins/fancy.column.html";
	
	$smarty->assign('fcv_cols',$cols);
	$smarty->assign('fcv_rows',$rows);
	$smarty->assign('fcv_title',$title);
	$smarty->assign('fcv_url',$url);
	$smarty->assign('fcv_input_name',$input_name);
	$smarty->assign('fcv_data',$data);
	
	$smarty->display($template);
	
	$smarty->clear_assign('fcv_cols');
	$smarty->clear_assign('fcv_rows');
	$smarty->clear_assign('fcv_title');
	$smarty->clear_assign('fcv_url');
	$smarty->clear_assign('fcv_input_name');
	$smarty->clear_assign('fcv_data');
	$smarty->clear_assign('fcv_unique');
		
}

/*
*
*	TEXTBOX LIST
*
*/
function smarty_textbox_list ($params, &$smarty) {
	
    require_once $smarty->_get_plugin_filepath('shared','escape_special_chars');

	foreach($params as $_key => $_val) {
	
        switch($_key) {
        
       		case 'width':
       		case 'height':
       		case 'name':
         		if ($_val != "") {
        			$$_key = $_val;
        		} else {
                    $smarty->trigger_error("date_picker: attribute '$_key' must be present", E_USER_NOTICE);
        		}
        	break;
 			case 'items':
                if (is_array($_val)) {
                    $$_key = smarty_function_escape_special_chars($_val);
                } elseif (trim($_val != '')) {
                	$_val = explode(',',$_val);
                	foreach ($_val as $value) {
                		$data[] = trim($value);
                	}
                	$$_key = $data;
                }
            break;
            
       }

	}
	
	$template = "plugins/textboxlist.html";
	
	if (count($items) > 0) {
		$smarty->assign("tbl_keywords",$items);
	}
	
	$smarty->assign("tbl_keywords_count",count($items));
	$smarty->assign("tbl_unique",mt_rand());
	$smarty->assign('tbl_name',$name);
	$smarty->assign('tbl_width',$width);
	$smarty->assign('tbl_height',$height);
	
	$smarty->display($template);

	$smarty->clear_assign('tbl_unique');
	$smarty->clear_assign('tbl_id');
	$smarty->clear_assign('tbl_name');
	$smarty->clear_assign('tbl_width');
	$smarty->clear_assign('tbl_height');
	$smarty->clear_assign('tbl_keywords');
	$smarty->clear_assign('tbl_keywords_count');

}

/*
*
* YAHOO UI DATE PICKER 
*
* Takes arguments:
*
* 1) Element ID
* 2) Element Name
* 3) Default date (in in any format compatible with strtotime)
*
*/
function smarty_date_picker ($params, &$smarty) {
	
    require_once $smarty->_get_plugin_filepath('shared','escape_special_chars');

	foreach($params as $_key => $_val) {
	
        switch($_key) {
        
       		case 'id':
       		case 'name':
         		if ($_val != "") {
        			$$_key = $_val;
        		} else {
                    $smarty->trigger_error("date_picker: attribute '$_key' must be present", E_USER_NOTICE);
        		}
        	break;
      		case 'default':
        		$$_key = $_val;
         	break;
       }

	}
	
	$template = "plugins/date.html";
	
	if ($default != '') {
	
		if (($timestamp = strtotime($default)) !== false) {
		
			$smarty->assign("datepicker_date",date("d/m/Y",$timestamp));
			$smarty->assign("datepicker_selected",date("m/d/Y",$timestamp));
			$smarty->assign("datepicker_pagedate",date("m/Y",$timestamp));
			
		}
		
	}
	
	$smarty->assign("datepicker_unique",mt_rand());
	$smarty->assign("datepicker_id",$id);
	$smarty->assign("datepicker_name",$name);

	$smarty->display($template);
	
	$smarty->clear_assign('datepicker_unique');
	$smarty->clear_assign('datepicker_id');
	$smarty->clear_assign('datepicker_name');
	$smarty->clear_assign('datepicker_date');
	$smarty->clear_assign('datepicker_selected');
	$smarty->clear_assign('datepicker_pagedate');
	
}

/*
*
*	LOCALISATION PROCESSOR
*
*/
function smarty_localisation_processor ($tpl_source, &$smarty) {
	
	if (!is_array($GLOBALS['_STR_'])) {
	  die("Error loading Multilanguage Support");
	}
	
	// Now replace the matched language strings with the entry in the file
	return preg_replace_callback('/##(.+?)##/', 'smarty_localisation_callback', $tpl_source);
	
}

/*
*
*	LOCALISATION PROCESSOR CALLBACK
*
*/
function smarty_localisation_callback ($key) {

	$data = $key[1];
	
	return smarty_localisation_variable_parser ($GLOBALS['_STR_'][$data]);
	
}

/*
*
*	LOCALISATION VARIABLE PARSER
*
*	Used for parsing variables in localisation strings
*	into cachable, executable PHP code.
*
*/
function smarty_localisation_variable_parser ($string) {
	
	// Now replace the matched language strings with the entry in the file
	return preg_replace_callback('/\$([a-zA-Z0-9_]*)/', 'smarty_localisation_variable_parser_callback', $string);
	
}

/*
*
*	LOCALISATION VARIABLE PARSER CALLBACK
*
*/
function smarty_localisation_variable_parser_callback ($key) {

	global $smarty;
	
	if (!is_object($smarty)) {
	  die("Error accessing Smarty object");
	}
	
	$data = $key[1];
	
	return ("<?php echo \$this->_tpl_vars['$data']; ?>");
	
}


?>