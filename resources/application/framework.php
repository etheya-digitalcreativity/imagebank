<?php if (PIXGALL != "ON") { print("Direct access to this script is not allowed."); exit; }

function AppErrorHandler ($errno, $errstr, $errfile, $errline) {
	
	global $__app_error;
	
	switch ($errno) {
	
		case E_USER_ERROR:
		
			$error_message  = "<b>My ERROR</b> [$errno] $errstr<br />\n";
			$error_message .= "  Fatal error on line $errline in file $errfile";
			$error_message .= ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
			$error_message .= "Aborting...<br />\n";
			
			print $error_message;
		
			exit(1);
			
		break;
		
		case E_USER_WARNING:
		
			$error_message  = "<b>WARNING</b><br />$errstr";
			
			$__app_error[] = $error_message;
		
		break;
		
		case E_USER_NOTICE:
		
			//$error_message  = "<b>My NOTICE</b> [$errno] $errstr<br />\n";
			
		break;
		
		default:
		
			//$error_message  = "Unknown error type: [$errno] $errstr<br />\n";
			
		break;
		
	}
	
	// Don't execute PHP internal error handler
	return true;	

}

// Use the application error handler for all error reporting
set_error_handler('AppErrorHandler');

AppFramework::Init();

class AppFramework {
	
	/*
	*
	*
	*
	*/
 	function Init () {
		
		global $cfg;
		
		require_once(BASE_PATH . 'pixaria.config' . EXT);
		
		// Place config file settings into constants
		define('DB_HOST',$cfg['sys']['db_host']);
		define('DB_NAME',$cfg['sys']['db_name']);
		define('DB_USER',$cfg['sys']['db_uname']);
		define('DB_PASS',$cfg['sys']['db_pword']);
		define('ENCRYPT',$cfg['sys']['encryption_key']);
		define('BASE_URL',$cfg['sys']['base_url']);
		
		$controller_path = AppFramework::getControllerPath();

		// Set the include path for files used in this script
		ini_set("include_path", RES_PATH . '/includes/');
		
		if (!file_exists(CTRL_PATH . $controller_path . EXT)) {
			$controller_path = DEFAULT_CONTROLLER;
		}
		
		AppFramework::loadController($controller_path);
	
	}
	
	/*
	*
	*
	*
	*/
	function loadController ($controller) {
		
		require_once RES_PATH . 'classes/class.Controller' . EXT;
		require_once CTRL_PATH . $controller . EXT;
		
		$obj = new $controller();
		
	}
	
	/*
	*
	*
	*
	*/
	function getControllerPath () {
	
		$path = url_decode();
		
		return $path['controller'];
		
	}
	
}

?>