/* Compressed by the perl version of jsmin. */

function toggleLarge(set_thumb_per_page,set_gallery_per_page){if(document.getElementById('gallery-large')){Effect.Appear('gallery-large',{duration:0.0});Effect.Fade('gallery-small',{duration:0.0});}
if(document.getElementById('image-large')){Effect.Appear('image-large',{duration:0.0});Effect.Fade('image-small',{duration:0.0});}}
function toggleSmall(set_thumb_per_page,set_gallery_per_page){if(document.getElementById('gallery-large')){Effect.Appear('gallery-small',{duration:0.0});Effect.Fade('gallery-large',{duration:0.0});}
if(document.getElementById('image-large')){Effect.Appear('image-small',{duration:0.0});Effect.Fade('image-large',{duration:0.0});}}
function updateCookie(cookieKey,cookieValue,cookieDefault){cookieKey=parseInt(cookieKey);var pixariaCookie=readCookie('psg_prefs');if(!pixariaCookie){createCookie('psg_prefs',cookieDefault,360);var pixariaCookie=cookieDefault;}
var cookieElements=pixariaCookie.split("|");var cookieVersion=cookieElements[0];var imagesPerPage=cookieElements[1];var galleriesPerPage=cookieElements[2];var viewThumbView=cookieElements[3];if(cookieKey==1){cookieData='1|'+cookieValue+'|'+galleriesPerPage+'|'+viewThumbView;}
if(cookieKey==2){cookieData='1|'+imagesPerPage+'|'+cookieValue+'|'+viewThumbView;}
if(cookieKey==3){cookieData='1|'+imagesPerPage+'|'+galleriesPerPage+'|'+cookieValue;}
createCookie('psg_prefs',cookieData,360)}
function createCookie(name,value,days){if(days){var date=new Date();date.setTime(date.getTime()+(days*24*60*60*1000));var expires="; expires="+date.toGMTString();}
else var expires="";document.cookie=name+"="+value+expires+"; path=/";}
function readCookie(name){var nameEQ=name+"=";var ca=document.cookie.split(';');for(var i=0;i<ca.length;i++){var c=ca[i];while(c.charAt(0)==' ')c=c.substring(1,c.length);if(c.indexOf(nameEQ)==0)return c.substring(nameEQ.length,c.length);}
return null;}
function eraseCookie(name){createCookie(name,"",-1);}
function imagePreviewWindow(strFile,nWidth,nHeight){width=parseInt(nWidth)-2;height=parseInt(nHeight)-2;window.open(strFile,"Image","toolbar=no,location=no,directories=no,menubar=no,status=yes,scrollbars=no,resizable=no,width="+width+",height="+height);}
function confirmSubmit(theCommand)
{var msg=theCommand;return confirm(msg);}
function openPop(strFile,nWidth,nHeight,bStatus,bscroll,btoolbar,blocation,bresize,bdirectories,bmenubar){window.open(strFile,"help","toolbar="+btoolbar+",location="+blocation+",directories="+bdirectories+",menubar="+bmenubar+",status="+bStatus+",scrollbars="+bscroll+",resizable="+bresize+",width="+nWidth+",height="+nHeight);}
function textCounter(field,countfield,maxlimit){if(field.value.length>maxlimit)
field.value=field.value.substring(0,maxlimit);else
countfield.value=maxlimit-field.value.length;}
var DHTML=(document.getElementById||document.all||document.layers);function getObj(name)
{if(document.getElementById)
{this.obj=document.getElementById(name);this.style=document.getElementById(name).style;}
else if(document.all)
{this.obj=document.all[name];this.style=document.all[name].style;}
else if(document.layers)
{this.obj=document.layers[name];this.style=document.layers[name];}}
function selectAllImages(formElement){setAllSelect(true,formElement);}
function deselectAllImages(formElement){setAllSelect(false,formElement);}
function setAllSelect(state,formElement){var formName=getThumbnailFormId();var elFrm=document.getElementById(formName);for(var i=0;i<elFrm.elements.length;i++){var ourEl=elFrm.elements[i];if(ourEl.type=="checkbox")
if(ourEl.name.indexOf(formElement)!=-1)
ourEl.checked=state;}}
function getSelectedImages(){var formName=getThumbnailFormId();var elFrm=document.getElementById(formName);var strParams="";for(var i=0;i<elFrm.elements.length;i++){var ourEl=elFrm.elements[i];if(ourEl.type=="checkbox")
if(ourEl.checked&&(ourEl.name.indexOf("photoid[]")!=-1))
strParams=strParams+ourEl.value+",";}
strParams=strParams.substr(0,strParams.length-1);return strParams;}
function addSelectionToLightbox(){var formName=getThumbnailFormId();var elFrm=document.getElementById(formName);elFrm.submit();}
function removeSelectionFromLightbox(){var formName=getThumbnailFormId();var elFrm=document.getElementById(formName);elFrm.submit();}
function getThumbnailFormId(){if(document.getElementById('image-small')==null){return'thumb_grid_large';}else if(document.getElementById('image-large')==null){return'thumb_grid_small';}
var thumbsFormSmall=new getObj('image-small');var thumbsFormLarge=new getObj('image-large');if(thumbsFormSmall.style.display=='none'){return'thumb_grid_large';}else{return'thumb_grid_small';}}
function getSelectedCheckbox(buttonGroup){var retArr=new Array();var lastElement=0;if(buttonGroup[0]){for(var i=0;i<buttonGroup.length;i++){if(buttonGroup[i].checked){retArr.length=lastElement;retArr[lastElement]=i;lastElement++;}}}else{if(buttonGroup.checked){retArr.length=lastElement;retArr[lastElement]=0;}}
return retArr;}
function getSelectedCheckboxValue(buttonGroup){var retArr=new Array();var selectedItems=getSelectedCheckbox(buttonGroup);if(selectedItems.length!=0){retArr.length=selectedItems.length;for(var i=0;i<selectedItems.length;i++){if(buttonGroup[selectedItems[i]]){retArr[i]=buttonGroup[selectedItems[i]].value;}else{retArr[i]=buttonGroup.value;}}}
return retArr;}
function changeCSS(theClass,element,value){var cssRules;if(document.all){cssRules='rules';}
else if(document.getElementById){cssRules='cssRules';}
for(var S=0;S<document.styleSheets.length;S++){for(var R=0;R<document.styleSheets[S][cssRules].length;R++){if(document.styleSheets[S][cssRules][R].selectorText==theClass){document.styleSheets[S][cssRules][R].style[element]=value;}}}}
Event.observe(window,'load',function(){changeCSS('.thumbnail','background','#FFF');},false);var Fabtabs=Class.create();Fabtabs.prototype={initialize:function(element){this.element=$(element);var options=Object.extend({},arguments[1]||{});this.menu=$A(this.element.getElementsByTagName('a'));var loader=document.getElementById('loader');loader.style.display='none';this.show(this.getInitialTab());this.menu.each(this.setupTab.bind(this));},setupTab:function(elm){Event.observe(elm,'click',this.activate.bindAsEventListener(this),false)},activate:function(ev){var elm=Event.findElement(ev,"a");Event.stop(ev);this.show(elm);this.menu.without(elm).each(this.hide.bind(this));},hide:function(elm){$(elm).removeClassName('active-tab');$(this.tabID(elm)).removeClassName('active-tab-body');},show:function(elm){$(elm).addClassName('active-tab');$(this.tabID(elm)).addClassName('active-tab-body');},tabID:function(elm){return elm.href.match(/#(\w.+)/)[1];},getInitialTab:function(){if(document.location.href.match(/#(\w.+)/)){var loc=RegExp.$1;var elm=this.menu.find(function(value){return value.href.match(/#(\w.+)/)[1]==loc;});return elm||this.menu.first();}else{return this.menu.first();}}}
Event.observe(window,'load',function(){if(document.getElementById('tab-container')){new Fabtabs('tabs');}},false);function $RF(el,radioGroup){if($(el).type&&$(el).type.toLowerCase()=='radio'){var radioGroup=$(el).name;var el=$(el).form;}else if($(el).tagName.toLowerCase()!='form'){return false;}
var checked=$(el).getInputs('radio',radioGroup).find(function(re){return re.checked;});return(checked)?$F(checked):null;}
function saveDefaultLanguage(formName,inputName){var language=$RF(formName,inputName);createCookie('pixaria_language',language,'2000');location.href=location.href;}