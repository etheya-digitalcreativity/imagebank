-- PIXARIA UPDATE 1.3.1 - 1.3.2

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.3.2' WHERE `name` = 'database_version' LIMIT 1 ;

-- PIXARIA UPDATE 1.3.2 - 1.3.3

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.3.3' WHERE `name` = 'database_version' LIMIT 1 ;

-- PIXARIA UPDATE 1.3.3 - 1.4.0

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.4.0' WHERE `name` = 'database_version' LIMIT 1 ;

UPDATE `psg_settings` SET `name` = 'store_flatrate_ship', `value` = '1' WHERE `name` = 'store_default_ship' LIMIT 1 ;

UPDATE `psg_settings` SET `name` = 'store_flatrate_ship_val' WHERE `name` = 'store_default_ship_val' LIMIT 1 ;

INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('store_func_cheque', '0'), ('store_func_postalorder', '0');
INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('store_func_moneyorder', '0'), ('store_func_banktransfer', '0');
INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('view_thumb_view', 'large');
INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('search_default_order', '1');

-- PIXARIA UPDATE 1.4.0 - 1.4.1

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.4.1' WHERE `name` = 'database_version' LIMIT 1 ;

-- PIXARIA UPDATE 1.4.1 - 1.4.2

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.4.2' WHERE `name` = 'database_version' LIMIT 1 ;

INSERT INTO `psg_settings` (`name`, `value`) VALUES ('image_popup_width', '650'), ('image_popup_height', '500');

INSERT INTO `psg_settings` (`name`, `value`) VALUES ('cart_popup_width', '650'), ('cart_popup_height', '500');

INSERT INTO `psg_settings` (`name`, `value`) VALUES ('lbox_popup_width', '500'), ('lbox_popup_height', '400');


-- PIXARIA UPDATE 1.4.2 - 1.4.3

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.4.3' WHERE `name` = 'database_version' LIMIT 1 ;




-- PIXARIA UPDATE 1.4.3 - 1.5.0

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.5.0' WHERE `name` = 'database_version' LIMIT 1 ;

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'image_watermark_pos', '10'
);

ALTER TABLE `psg_images` DROP INDEX `image_filename` ,
ADD FULLTEXT `image_filename` (
`image_title` ,
`image_caption` ( 1 ) ,
`image_keywords` ( 1 ) ,
`image_filename` ( 1 )
);

-- PIXARIA UPDATE 1.5.0 - 1.5.1

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.5.1' WHERE `name` = 'database_version' LIMIT 1 ;

-- PIXARIA UPDATE 1.5.1 - 1.5.2

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.5.2' WHERE `name` = 'database_version' LIMIT 1 ;

-- PIXARIA UPDATE 1.5.2 - 1.5.3

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.5.3' WHERE `name` = 'database_version' LIMIT 1 ;


-- PIXARIA UPDATE 1.5.3 - 2.0.0

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '2.0.0' WHERE `name` = 'database_version' LIMIT 1 ;

CREATE TABLE `psg_keyword_tags` (`id` INT( 5 ) NOT NULL AUTO_INCREMENT , `keyword` VARCHAR( 50 )  NOT NULL , PRIMARY KEY ( `id` ));

CREATE TABLE `psg_keywords_images` (
`id` INT( 5 ) NOT NULL AUTO_INCREMENT ,
`image_id` INT( 5 ) NOT NULL ,
`keyword_id` INT( 5 ) NOT NULL ,
PRIMARY KEY ( `id` )
);

ALTER TABLE `psg_lightbox_members` CHANGE `order` `order` INT( 6 ) NOT NULL;

INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('browse_thumb_count', '5');

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'search_image_color', '1'
), (
'search_orientation', '1'
);

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'func_contact_sheet', '1'
), (
'contact_sheet_quality', 'low'
);

ALTER TABLE `psg_galleries` ADD `gallery_short_title` VARCHAR( 50 ) AFTER `gallery_title` ;

CREATE TABLE `psg_download_log` (
`id` INT( 6 ) NOT NULL AUTO_INCREMENT ,
`image_id` INT( 7 ) NOT NULL ,
`userid` VARCHAR( 7 ) NOT NULL ,
`time` DATETIME NOT NULL ,
`type` ENUM( 'jpg', 'psd', 'jp2', 'eps', 'tif' ) NOT NULL ,
`transaction` ENUM( 'free', 'pay' ) NOT NULL ,
PRIMARY KEY ( `id` )
);

UPDATE `psg_settings` SET `value` = 'Illuminux' WHERE `name` = 'theme' LIMIT 1 ;

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'keywords_updated', '0'
);

ALTER TABLE `psg_galleries` ADD `gallery_active` INT( 1 ) DEFAULT '1' NOT NULL AFTER `gallery_id` ;

ALTER TABLE `psg_images` ADD `image_active` INT( 1 ) DEFAULT '1' NOT NULL AFTER `image_id` ;

CREATE TABLE `psg_images_products` (
`imgprod_id` INT( 8 ) DEFAULT '0' NOT NULL ,
`image_id` INT( 8 ) DEFAULT '0' NOT NULL ,
`product_id` INT( 8 ) DEFAULT '0' NOT NULL
);

ALTER TABLE `psg_images` ADD `image_product_link` INT( 1 ) DEFAULT '0' NOT NULL AFTER `image_sale` ;

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'func_contact', '1'
);

ALTER TABLE `psg_images` ADD `image_trashed` INT( 1 ) DEFAULT '0' NOT NULL AFTER `image_counter` ,
ADD `image_content_type` ENUM( '10', '11', '12', '13' ) DEFAULT '10' NOT NULL AFTER `image_trashed` ;