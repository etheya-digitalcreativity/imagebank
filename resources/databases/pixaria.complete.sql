-- 
-- Table structure for table `psg_banned_domains`
-- 

CREATE TABLE `psg_banned_domains` (
  `id` int(4) NOT NULL auto_increment,
  `domain_name` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=7 ;

-- 
-- Dumping data for table `psg_banned_domains`
-- 

INSERT INTO `psg_banned_domains` (`id`, `domain_name`) VALUES (2, 'hotmail.com');
INSERT INTO `psg_banned_domains` (`id`, `domain_name`) VALUES (5, 'yahoo.com');
INSERT INTO `psg_banned_domains` (`id`, `domain_name`) VALUES (6, 'iname.com');

-- --------------------------------------------------------

-- 
-- Table structure for table `psg_blog`
-- 

CREATE TABLE `psg_blog` (
  `blog_item_id` int(5) NOT NULL auto_increment,
  `blog_title` varchar(255) NOT NULL default '',
  `blog_content` longtext NOT NULL,
  `blog_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `blog_author` int(8) NOT NULL default '0',
  `blog_gallery_id` int(10) NOT NULL default '0',
  UNIQUE KEY `blog_item_id` (`blog_item_id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `psg_blog`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `psg_cart`
-- 

CREATE TABLE `psg_cart` (
  `cart_id` int(10) NOT NULL auto_increment,
  `store_type` int(2) NOT NULL default '0',
  `status` int(1) NOT NULL default '0',
  `userid` int(8) NOT NULL default '0',
  `subtotal` decimal(6,2) NOT NULL default '0.00',
  `tax` decimal(6,1) NOT NULL default '0.0',
  `tax_total` decimal(6,2) NOT NULL default '0.00',
  `shipping` decimal(6,2) NOT NULL default '0.00',
  `total` decimal(6,2) NOT NULL default '0.00',
  `transaction_id` varchar(255) NOT NULL default '',
  `remote_txn_id` varchar(255) NOT NULL default '',
  `paid` int(2) NOT NULL default '0',
  `method` int(2) NOT NULL default '0',
  `usage_text` tinytext NOT NULL,
  `transaction_info` longtext NOT NULL,
  `date_created` datetime NOT NULL default '0000-00-00 00:00:00',
  `date_processed` datetime NOT NULL default '0000-00-00 00:00:00',
  `date_completed` datetime NOT NULL default '0000-00-00 00:00:00',
  UNIQUE KEY `id` (`cart_id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `psg_cart`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `psg_cart_members`
-- 

CREATE TABLE `psg_cart_members` (
  `id` int(10) NOT NULL auto_increment,
  `cart_id` int(10) NOT NULL default '0',
  `userid` int(10) NOT NULL default '0',
  `image_id` int(10) NOT NULL default '0',
  `image_path` varchar(255) NOT NULL default '',
  `usage_text` text NOT NULL,
  `price` decimal(10,0) NOT NULL default '0',
  `image_filename` varchar(255) NOT NULL default '',
  `image_title` varchar(255) NOT NULL default '',
  `quote_text` text NOT NULL,
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `psg_cart_members`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `psg_categories`
-- 

CREATE TABLE `psg_categories` (
  `category_id` int(6) NOT NULL auto_increment,
  `category_parent` int(6) NOT NULL default '0',
  `category_title` varchar(255) default NULL,
  `category_description` text NOT NULL,
  `category_key` int(11) NOT NULL default '0',
  `category_created` datetime NOT NULL default '0000-00-00 00:00:00',
  `category_edited` datetime NOT NULL default '0000-00-00 00:00:00',
  UNIQUE KEY `ID` (`category_id`),
  FULLTEXT KEY `DBSEARCH` (`category_title`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `psg_categories`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `psg_categories_members`
-- 

CREATE TABLE `psg_categories_members` (
  `id` int(10) NOT NULL auto_increment,
  `image_id` int(10) NOT NULL default '0',
  `category_id` int(10) NOT NULL default '0',
  UNIQUE KEY `id` (`id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `psg_categories_members`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `psg_galleries`
-- 

CREATE TABLE `psg_galleries` (
  `gallery_id` int(6) NOT NULL auto_increment,
  `gallery_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `gallery_parent` int(6) NOT NULL default '0',
  `gallery_type` int(2) NOT NULL default '10',
  `gallery_userid` int(7) NOT NULL default '0',
  `gallery_title` varchar(255) default NULL,
  `gallery_description` text NOT NULL,
  `gallery_password` varchar(255) NOT NULL default '',
  `gallery_permissions` int(2) NOT NULL default '10',
  `gallery_key` int(11) NOT NULL default '0',
  `gallery_created` datetime NOT NULL default '0000-00-00 00:00:00',
  `gallery_edited` datetime NOT NULL default '0000-00-00 00:00:00',
  `gallery_mime_type` int(2) default NULL,
  `gallery_datasize` int(20) NOT NULL default '0',
  `gallery_search_string` text,
  `gallery_order` longtext NOT NULL,
  UNIQUE KEY `ID` (`gallery_id`),
  FULLTEXT KEY `DBSEARCH` (`gallery_title`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `psg_galleries`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `psg_gallery_order`
-- 

CREATE TABLE `psg_gallery_order` (
  `order_id` int(14) NOT NULL default '0',
  `gallery_id` int(9) NOT NULL default '0',
  `image_id` int(9) NOT NULL default '0'
) TYPE=MyISAM;

-- 
-- Dumping data for table `psg_gallery_order`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `psg_gallery_view`
-- 

CREATE TABLE `psg_gallery_view` (
  `gallery_id` int(6) NOT NULL default '0',
  `userid` int(6) NOT NULL default '0'
) TYPE=MyISAM;

-- 
-- Dumping data for table `psg_gallery_view`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `psg_groups`
-- 

CREATE TABLE `psg_groups` (
  `group_id` int(11) NOT NULL auto_increment,
  `group_name` varchar(50) NOT NULL default '0',
  UNIQUE KEY `group_id` (`group_id`)
) TYPE=MyISAM AUTO_INCREMENT=12 ;

-- 
-- Dumping data for table `psg_groups`
-- 

INSERT INTO `psg_groups` (`group_id`, `group_name`) VALUES (10, 'Family and Friends');
INSERT INTO `psg_groups` (`group_id`, `group_name`) VALUES (11, 'Business Contacts');

-- --------------------------------------------------------

-- 
-- Table structure for table `psg_groups_members`
-- 

CREATE TABLE `psg_groups_members` (
  `group_id` int(11) NOT NULL default '0',
  `userid` int(11) NOT NULL default '0'
) TYPE=MyISAM;

-- 
-- Dumping data for table `psg_groups_members`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `psg_images`
-- 

CREATE TABLE `psg_images` (
  `image_id` int(30) NOT NULL auto_increment,
  `image_path` varchar(50) default NULL,
  `image_userid` int(7) NOT NULL default '0',
  `image_category_id` int(20) default NULL,
  `image_filename` varchar(255) default NULL,
  `image_title` varchar(255) NOT NULL default '',
  `image_caption` text,
  `image_filetype` varchar(255) default NULL,
  `image_keywords` text,
  `image_width` int(5) default NULL,
  `image_height` int(5) default NULL,
  `image_copyright` varchar(255) default NULL,
  `image_headline` varchar(255) default NULL,
  `image_date` datetime NOT NULL default '0000-00-00 00:00:00',
  `image_permissions` int(2) NOT NULL default '10',
  `image_rights_type` int(2) NOT NULL default '10',
  `image_rights_text` text,
  `image_colr_r` int(3) default '0',
  `image_colr_g` int(3) default '0',
  `image_colr_b` int(3) default '0',
  `image_price` decimal(6,2) NOT NULL default '0.00',
  `image_sale` int(1) NOT NULL default '0',
  `image_synd` int(1) NOT NULL default '0',
  `image_synd_url` varchar(255) NOT NULL default '0',
  `image_synd_userid` int(15) NOT NULL default '0',
  PRIMARY KEY  (`image_id`),
  FULLTEXT KEY `image_filename` (`image_filename`,`image_title`,`image_caption`,`image_keywords`,`image_copyright`,`image_headline`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `psg_images`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `psg_images_viewers`
-- 

CREATE TABLE `psg_images_viewers` (
  `id` int(10) NOT NULL auto_increment,
  `image_id` int(10) NOT NULL default '0',
  `group_id` int(10) NOT NULL default '0',
  UNIQUE KEY `id` (`id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `psg_images_viewers`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `psg_lightbox_members`
-- 

CREATE TABLE `psg_lightbox_members` (
  `id` int(10) NOT NULL auto_increment,
  `lightbox_id` varchar(8) NOT NULL default '',
  `userid` varchar(8) NOT NULL default '',
  `image_id` varchar(8) NOT NULL default '',
  `order` varchar(6) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `psg_lightbox_members`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `psg_lightboxes`
-- 

CREATE TABLE `psg_lightboxes` (
  `id` int(6) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `date` datetime NOT NULL default '0000-00-00 00:00:00',
  `userid` int(8) NOT NULL default '0',
  `type` int(2) NOT NULL default '10',
  `status` int(2) NOT NULL default '10',
  UNIQUE KEY `id` (`id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `psg_lightboxes`
-- 

INSERT INTO `psg_lightboxes` VALUES ('0','Default Lightbox',NOW(),'1000001','10','10');

-- --------------------------------------------------------

-- 
-- Table structure for table `psg_settings`
-- 

CREATE TABLE `psg_settings` (
  `name` varchar(255) NOT NULL default '',
  `value` varchar(255) NOT NULL default ''
) TYPE=MyISAM;

-- 
-- Dumping data for table `psg_settings`
-- 

INSERT INTO `psg_settings` (`name`, `value`) VALUES ('gallery_per_page', '12');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('store_func_paypal', '0');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('path_incoming', 'resources/incoming/');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('user_ban_domains', '1');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('database_version', '1.0.0');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('user_auto_approve', '1');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('st_email', 'info@pixaria.com');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('st_gdversion', '1');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('gallery_order', '10');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('image_output_format', '10');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('language', 'en');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('gd_quality', '85');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('site_name', 'Pixaria Default');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('path_library', 'resources/library/');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('store_images_forsale', '13');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('temporary', '/tmp/');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('theme', 'Advance');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('store_default_price', '10.00');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('store_type', '11');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('image_watermark_text', '');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('image_watermark', '10');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('theme_width', '850');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('func_lightbox', '0');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('func_store', '0');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('func_postcards', '0');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('thumb_per_page', '12');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('thumb_notes', '11');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('thumb_width', '11');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('date_format', 'dmy');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('smarty_cache', '1');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('func_rss', '1');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('store_currency', 'GBP');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('store_paypal_email', 'info@pixaria.com');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('contact_email', 'info@pixaria.com');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('contact_name', 'Administrator');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('func_categories', '1');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('func_news', '1');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('contact_details', '');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('html_statcode', '');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('login_default', '0');
INSERT INTO `psg_settings` (`name`, `value`) VALUES ('login_notify', '0');

-- --------------------------------------------------------

-- 
-- Table structure for table `psg_sys_groups`
-- 

CREATE TABLE `psg_sys_groups` (
  `sys_group_id` int(11) NOT NULL auto_increment,
  `sys_group_name` varchar(50) NOT NULL default '0',
  UNIQUE KEY `group_id` (`sys_group_id`)
) TYPE=MyISAM AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `psg_sys_groups`
-- 

INSERT INTO `psg_sys_groups` (`sys_group_id`, `sys_group_name`) VALUES (1, 'Administrators');
INSERT INTO `psg_sys_groups` (`sys_group_id`, `sys_group_name`) VALUES (2, 'Image editors');

-- --------------------------------------------------------

-- 
-- Table structure for table `psg_sys_groups_members`
-- 

CREATE TABLE `psg_sys_groups_members` (
  `sys_group_id` int(11) NOT NULL default '0',
  `sys_userid` int(11) NOT NULL default '0'
) TYPE=MyISAM;

-- 
-- Dumping data for table `psg_sys_groups_members`
-- 

INSERT INTO `psg_sys_groups_members` (`sys_group_id`, `sys_userid`) VALUES (1, 1000001);
INSERT INTO `psg_sys_groups_members` (`sys_group_id`, `sys_userid`) VALUES (2, 1000001);

-- --------------------------------------------------------

-- 
-- Table structure for table `psg_txn_messages`
-- 

CREATE TABLE `psg_txn_messages` (
  `message_id` int(5) NOT NULL auto_increment,
  `message_text` mediumtext NOT NULL,
  `message_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `message_userid` int(8) NOT NULL default '0',
  `message_cart_id` int(20) NOT NULL default '0',
  `message_type` int(2) NOT NULL default '10',
  UNIQUE KEY `message_id` (`message_id`)
) TYPE=MyISAM AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `psg_txn_messages`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `psg_users`
-- 

CREATE TABLE `psg_users` (
  `userid` int(6) NOT NULL auto_increment,
  `auth_level` int(2) default '0',
  `email_address` varchar(255) default NULL,
  `formal_title` varchar(10) default NULL,
  `other_title` varchar(10) default NULL,
  `first_name` varchar(255) default NULL,
  `middle_initial` char(1) default NULL,
  `family_name` varchar(255) default NULL,
  `password` varchar(255) default NULL,
  `password_rem_que` varchar(255) default NULL,
  `password_rem_ans` varchar(255) default NULL,
  `date_registered` datetime NOT NULL default '0000-00-00 00:00:00',
  `date_edited` datetime NOT NULL default '0000-00-00 00:00:00',
  `date_expiration` datetime NOT NULL default '0000-00-00 00:00:00',
  `account_status` tinyint(1) NOT NULL default '0',
  `telephone` varchar(30) default NULL,
  `mobile` varchar(30) default NULL,
  `fax` varchar(30) default NULL,
  `addr1` varchar(30) default NULL,
  `addr2` varchar(30) default NULL,
  `addr3` varchar(30) default NULL,
  `city` varchar(30) default NULL,
  `region` varchar(30) default NULL,
  `country` varchar(255) default NULL,
  `postal_code` varchar(10) default NULL,
  `storage_space` int(5) NOT NULL default '0',
  `renewal_period` int(2) NOT NULL default '10',
  `active_image_box` int(6) NOT NULL default '0',
  `other_business_type` varchar(255) NOT NULL default '',
  `other_business_position` varchar(255) NOT NULL default '',
  `other_image_interest` varchar(255) NOT NULL default '',
  `other_frequency` varchar(255) NOT NULL default '',
  `other_circulation` varchar(255) NOT NULL default '',
  `other_territories` varchar(255) NOT NULL default '',
  `other_website` varchar(255) NOT NULL default '',
  `other_message` varchar(255) NOT NULL default '',
  `other_publication` varchar(255) NOT NULL default '',
  KEY `IDNumber` (`userid`)
) TYPE=MyISAM PACK_KEYS=0 AUTO_INCREMENT=1000009 ;

-- 
-- Dumping data for table `psg_users`
-- 

INSERT INTO `psg_users` (`userid`, `auth_level`, `email_address`, `formal_title`, `other_title`, `first_name`, `middle_initial`, `family_name`, `password`, `password_rem_que`, `password_rem_ans`, `date_registered`, `date_edited`, `date_expiration`, `account_status`, `telephone`, `mobile`, `fax`, `addr1`, `addr2`, `addr3`, `city`, `region`, `country`, `postal_code`, `storage_space`, `renewal_period`, `active_image_box`, `other_business_type`, `other_business_position`, `other_image_interest`, `other_frequency`, `other_circulation`, `other_territories`, `other_website`, `other_message`, `other_publication`) VALUES (1000001, 11, 'test@test.local', '', '', 'Website', '', 'Admin', '5f4dcc3b5aa765d61d8327deb882cf99', 'test', 'test', '2005-10-30 13:25:09', '2005-12-16 18:21:18', '2005-10-30 13:25:09', 1, '', '', '', '', '', '', '', '', '', '', 0, 0, 1, '', '', '', '', '', '', 'http://www.pixaria.com/', '', '');

-- --------------------------------------------------------

-- 
-- Table structure for table `psg_users_data`
-- 

CREATE TABLE `psg_users_data` (
  `name` varchar(255) NOT NULL default '',
  `value` tinyint(1) NOT NULL default '0'
) TYPE=MyISAM;

-- 
-- Dumping data for table `psg_users_data`
-- 

INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('salutation', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('salutation_req', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('telephone', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('telephone_req', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('mobile', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('mobile_req', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('facsimile', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('facsimile_req', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('address', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('address_req', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('business_position', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('business_position_req', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('business_type', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('business_type_req', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('image_interest', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('image_interest_req', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('frequency', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('frequency_req', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('circulation', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('circulation_req', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('territories', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('territories_req', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('website', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('website_req', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('message', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('message_req', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('company_name', 0);
INSERT INTO `psg_users_data` (`name`, `value`) VALUES ('company_name_req', 0);

-- PIXARIA UPDATE 1.0.0 - 1.0.1

-- CHANGE THUMBNAIL SIZE
UPDATE `psg_settings` SET `value` = '10' WHERE `name` = 'thumb_width' LIMIT 1 ;


-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.0.1' WHERE `name` = 'database_version' LIMIT 1 ;

-- PIXARIA UPDATE 1.0.1 - 1.0.2

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.0.2' WHERE `name` = 'database_version' LIMIT 1 ;

-- ADD NEW TABLE TO HOLD SEARCH KEYWORDS

CREATE TABLE `psg_search_log` (
`keyword_id` INT( 8 ) NOT NULL AUTO_INCREMENT ,
`keyword` VARCHAR( 255 ) NOT NULL ,
UNIQUE (
`keyword_id`
)
) TYPE = MYISAM ;

ALTER TABLE `psg_search_log` ADD `type` VARCHAR( 3 ) NOT NULL AFTER `keyword` ;

-- ADD A DATABASE SETTING ITEM FOR ENABLING SEARCH LOGGING
INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'func_search_log', '1'
);

-- ADD A DATABASE SETTING ITEM FOR ENABLING SEARCHING
INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'func_search', '1'
);


ALTER TABLE `psg_cart_members` CHANGE `price` `price` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0';

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'func_sidebar', '1'
);


ALTER TABLE `psg_images` DROP INDEX `image_filename` ,
ADD FULLTEXT `image_filename` (
`image_title` ,
`image_caption` ,
`image_keywords`
);


ALTER TABLE `psg_images` ADD `image_model_release` INT( 1 ) DEFAULT '0' NOT NULL AFTER `image_synd_userid` ,
ADD `image_property_release` INT( 1 ) DEFAULT '0' NOT NULL AFTER `image_model_release` ;

-- PIXARIA UPDATE 1.0.2 - 1.0.3

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.0.3' WHERE `name` = 'database_version' LIMIT 1 ;

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'postcard_watermark', '10'
);


INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'image_href', 'inline'
);



CREATE TABLE `psg_user_log` (
`log_userid` INT( 7 ) NOT NULL ,
`log_ip` VARCHAR( 15 ) NOT NULL ,
`log_date` DATETIME NOT NULL ,
`log_host` VARCHAR( 255 ) NOT NULL
) TYPE = MYISAM;


INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'search_log_date', NOW( )
), (
'login_log_date', NOW( )
);


INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'cart_href', 'inline'
), (
'lightbox_href', 'inline'
);

-- PIXARIA UPDATE 1.0.3 - 1.0.4

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.0.4' WHERE `name` = 'database_version' LIMIT 1 ;

ALTER TABLE `psg_galleries` ADD `gallery_counter` INT( 8 ) NOT NULL ;

ALTER TABLE `psg_images` ADD `image_counter` INT( 8 ) NOT NULL ;


-- 
-- Table structure for table `psg_calc_arithmetic`
-- 

CREATE TABLE `psg_calc_arithmetic` (
  `ar_id` int(8) NOT NULL auto_increment,
  `ar_calc_id` int(8) NOT NULL default '0',
  `ar_operand` varchar(10) NOT NULL default '',
  `ar_factor` varchar(15) NOT NULL default '',
  `ar_name` varchar(50) NOT NULL default '',
  `ar_description` varchar(255) NOT NULL default '',
  `ar_active` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`ar_id`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `psg_calc_arithmetic`
-- 

INSERT INTO `psg_calc_arithmetic` VALUES (1, 1, '1', '10.00', 'Brochure', 'Image will be used in a brochure', 0);
INSERT INTO `psg_calc_arithmetic` VALUES (2, 1, '1', '20.00', 'Magazine', 'Image will be used in a magazine', 0);
INSERT INTO `psg_calc_arithmetic` VALUES (3, 1, '1', '10.00', 'Website', 'Image will be used on a website', 0);
INSERT INTO `psg_calc_arithmetic` VALUES (4, 1, '1', '30.00', 'Newspaper', 'Image will be used in a newspaper', 0);
INSERT INTO `psg_calc_arithmetic` VALUES (6, 2, '3', '1.2', 'Half page', 'The image will take up half a page', 0);
INSERT INTO `psg_calc_arithmetic` VALUES (7, 2, '3', '1.4', 'Full page', 'The image will take up a full page', 0);
INSERT INTO `psg_calc_arithmetic` VALUES (8, 2, '3', '1.1', 'Quarter page', 'The image will take up a quarter page', 0);
INSERT INTO `psg_calc_arithmetic` VALUES (9, 2, '3', '2.0', 'Centrefold spread', 'The image wil be a centre page spread', 0);

-- --------------------------------------------------------

-- 
-- Table structure for table `psg_calc_objects`
-- 

CREATE TABLE `psg_calc_objects` (
  `calc_id` int(8) NOT NULL auto_increment,
  `calc_name` varchar(50) NOT NULL default '',
  `calc_description` varchar(255) NOT NULL default '',
  `calc_active` tinyint(1) NOT NULL default '0',
  `calc_order` tinyint(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`calc_id`)
) TYPE=MyISAM;

-- 
-- Dumping data for table `psg_calc_objects`
-- 

INSERT INTO `psg_calc_objects` VALUES (1, 'Where will this image be used?', 'Provide information about the type of product where this image will be used.', 0, 1);
INSERT INTO `psg_calc_objects` VALUES (2, 'What size will the image be when it is reproduced?', 'Please provide information about how big this image will be relative to the medium it will be reproduced in.', 0, 0);


UPDATE psg_images SET image_filetype = 'image/jpeg';

ALTER TABLE `psg_user_log` ADD `log_success` INT( 1 ) DEFAULT '1' NOT NULL AFTER `log_userid` ,
ADD `log_email` VARCHAR( 100 ) NOT NULL AFTER `log_success` ;


INSERT INTO `psg_sys_groups` VALUES ('0', 'Photographers');

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'func_postcards_user', '0'
);

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'func_mod_rewrite', '0'
);

-- PIXARIA UPDATE 1.0.4 - 1.0.5

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.0.5' WHERE `name` = 'database_version' LIMIT 1 ;

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'func_store_download', '0'
);

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'store_download_attempts', '3'
);

ALTER TABLE `psg_cart_members` ADD `cart_download_counter` INT( 3 ) DEFAULT '0' NOT NULL AFTER `quote_text` ;



-- PIXARIA UPDATE 1.0.5 - 1.1.0

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.1.0' WHERE `name` = 'database_version' LIMIT 1 ;

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'store_default_tax', '17.5'
);

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'store_default_ship', '0'
), (
'store_default_ship_val', '0.00'
);

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'store_shipping_message', ''
);

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'store_quote_option', '1'
);

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'login_memory', '1'
);

-- PIXARIA UPDATE 1.1.0 - 1.1.1

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.1.1' WHERE `name` = 'database_version' LIMIT 1 ;

-- PIXARIA UPDATE 1.1.1 - 1.1.2

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.1.2' WHERE `name` = 'database_version' LIMIT 1 ;


UPDATE `psg_images` SET `image_permissions` = '10' WHERE `image_permissions` = '0';

ALTER TABLE `psg_images` CHANGE `image_permissions` `image_permissions` ENUM( '10', '11', '12' ) NOT NULL DEFAULT '10';

ALTER TABLE `psg_images` ADD `image_extra_01` VARCHAR( 255 ) ,
ADD `image_extra_02` VARCHAR( 255 ) ,
ADD `image_extra_03` VARCHAR( 255 ) ,
ADD `image_extra_04` VARCHAR( 255 ) ,
ADD `image_extra_05` VARCHAR( 255 ) ;


INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('extra_field_01', 'Extra image data 1');
INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('extra_field_02', 'Extra image data 2');
INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('extra_field_03', 'Extra image data 3');
INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('extra_field_04', 'Extra image data 4');
INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('extra_field_05', 'Extra image data 5');

INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('store_tax_number', '');

INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('store_calculate_login', '0');


-- PIXARIA UPDATE 1.1.2 - 1.1.3

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.1.3' WHERE `name` = 'database_version' LIMIT 1 ;


-- PIXARIA UPDATE 1.1.3 - 1.1.4

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.1.4' WHERE `name` = 'database_version' LIMIT 1 ;


ALTER TABLE `psg_users` CHANGE `other_publication` `other_company_name` VARCHAR( 255 ) NOT NULL;

-- CREATE THE PRODUCTS TABLE
CREATE TABLE `psg_products` (
`prod_id` INT( 6 ) NOT NULL AUTO_INCREMENT ,
`prod_active` TINYINT( 1 ) DEFAULT '0' NOT NULL ,
`prod_type` ENUM( 'IMG', 'IND' ) NOT NULL ,
`prod_name` VARCHAR( 255 ) NOT NULL ,
`prod_description` TEXT NOT NULL ,
`prod_price` DECIMAL( 6, 2 ) NOT NULL ,
UNIQUE (
`prod_id`
)
) TYPE = MYISAM;


-- PIXARIA UPDATE 1.1.4 - 1.1.5

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.1.5' WHERE `name` = 'database_version' LIMIT 1 ;


INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('store_offline_payment', '');


-- PIXARIA UPDATE 1.1.5 - 1.1.6

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.1.6' WHERE `name` = 'database_version' LIMIT 1 ;


INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'search_caption', '1'
), (
'search_title', '1'
);

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'search_opt_keywords', '1'
), (
'search_opt_date', '1'
);

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'search_opt_galleries', '1'
), (
'search_opt_model_release', '1'
);

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'search_opt_property_release', '1'
);

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'func_downloads_user', '10'
), (
'func_downloads', '0'
);



-- PIXARIA UPDATE 1.1.6 - 1.2.0

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.2' WHERE `name` = 'database_version' LIMIT 1 ;

ALTER TABLE `psg_images` ADD `image_colr_enable` TINYINT( 1 ) DEFAULT '0' NOT NULL AFTER `image_rights_text`;

ALTER TABLE `psg_cart_members` ADD `item_type` ENUM( 'digital', 'physical' ) DEFAULT 'digital' NOT NULL AFTER `cart_id`;

ALTER TABLE psg_images ADD INDEX image_id (image_id);
ALTER TABLE psg_gallery_order ADD INDEX image_id (image_id);

-- PIXARIA UPDATE 1.2.0 - 1.2.1

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.2.1' WHERE `name` = 'database_version' LIMIT 1 ;


-- PIXARIA UPDATE 1.2.1 - 1.2.2

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.2.2' WHERE `name` = 'database_version' LIMIT 1 ;

-- PIXARIA UPDATE 1.2.2 - 1.3.0

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.3.0' WHERE `name` = 'database_version' LIMIT 1 ;

INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('search_filename', '1');

ALTER TABLE `psg_products` ADD `prod_shipping` DECIMAL( 6, 2 ) DEFAULT '0.00' NOT NULL ,
ADD `prod_shipping_multiple` DECIMAL( 6, 2 ) DEFAULT '0.00' NOT NULL ;

ALTER TABLE `psg_cart_members` ADD `shipping` DECIMAL( 6, 2 ) DEFAULT '0.00' NOT NULL AFTER `price` ,
ADD `shipping_multiple` DECIMAL( 6, 2 ) DEFAULT '0.00' NOT NULL AFTER `shipping` ,
ADD `quantity` INT( 3 ) DEFAULT '1' NOT NULL AFTER `shipping_multiple` ;

-- List from http://27.org/isocountrylist/

CREATE TABLE IF NOT EXISTS psg_country (
  iso CHAR(2) NOT NULL PRIMARY KEY,
  name VARCHAR(80) NOT NULL,
  printable_name VARCHAR(80) NOT NULL,
  iso3 CHAR(3),
  numcode SMALLINT
);

INSERT INTO psg_country VALUES ('AF','AFGHANISTAN','Afghanistan','AFG','004');
INSERT INTO psg_country VALUES ('AL','ALBANIA','Albania','ALB','008');
INSERT INTO psg_country VALUES ('DZ','ALGERIA','Algeria','DZA','012');
INSERT INTO psg_country VALUES ('AS','AMERICAN SAMOA','American Samoa','ASM','016');
INSERT INTO psg_country VALUES ('AD','ANDORRA','Andorra','AND','020');
INSERT INTO psg_country VALUES ('AO','ANGOLA','Angola','AGO','024');
INSERT INTO psg_country VALUES ('AI','ANGUILLA','Anguilla','AIA','660');
INSERT INTO psg_country VALUES ('AQ','ANTARCTICA','Antarctica',NULL,NULL);
INSERT INTO psg_country VALUES ('AG','ANTIGUA AND BARBUDA','Antigua and Barbuda','ATG','028');
INSERT INTO psg_country VALUES ('AR','ARGENTINA','Argentina','ARG','032');
INSERT INTO psg_country VALUES ('AM','ARMENIA','Armenia','ARM','051');
INSERT INTO psg_country VALUES ('AW','ARUBA','Aruba','ABW','533');
INSERT INTO psg_country VALUES ('AU','AUSTRALIA','Australia','AUS','036');
INSERT INTO psg_country VALUES ('AT','AUSTRIA','Austria','AUT','040');
INSERT INTO psg_country VALUES ('AZ','AZERBAIJAN','Azerbaijan','AZE','031');
INSERT INTO psg_country VALUES ('BS','BAHAMAS','Bahamas','BHS','044');
INSERT INTO psg_country VALUES ('BH','BAHRAIN','Bahrain','BHR','048');
INSERT INTO psg_country VALUES ('BD','BANGLADESH','Bangladesh','BGD','050');
INSERT INTO psg_country VALUES ('BB','BARBADOS','Barbados','BRB','052');
INSERT INTO psg_country VALUES ('BY','BELARUS','Belarus','BLR','112');
INSERT INTO psg_country VALUES ('BE','BELGIUM','Belgium','BEL','056');
INSERT INTO psg_country VALUES ('BZ','BELIZE','Belize','BLZ','084');
INSERT INTO psg_country VALUES ('BJ','BENIN','Benin','BEN','204');
INSERT INTO psg_country VALUES ('BM','BERMUDA','Bermuda','BMU','060');
INSERT INTO psg_country VALUES ('BT','BHUTAN','Bhutan','BTN','064');
INSERT INTO psg_country VALUES ('BO','BOLIVIA','Bolivia','BOL','068');
INSERT INTO psg_country VALUES ('BA','BOSNIA AND HERZEGOVINA','Bosnia and Herzegovina','BIH','070');
INSERT INTO psg_country VALUES ('BW','BOTSWANA','Botswana','BWA','072');
INSERT INTO psg_country VALUES ('BV','BOUVET ISLAND','Bouvet Island',NULL,NULL);
INSERT INTO psg_country VALUES ('BR','BRAZIL','Brazil','BRA','076');
INSERT INTO psg_country VALUES ('IO','BRITISH INDIAN OCEAN TERRITORY','British Indian Ocean Territory',NULL,NULL);
INSERT INTO psg_country VALUES ('BN','BRUNEI DARUSSALAM','Brunei Darussalam','BRN','096');
INSERT INTO psg_country VALUES ('BG','BULGARIA','Bulgaria','BGR','100');
INSERT INTO psg_country VALUES ('BF','BURKINA FASO','Burkina Faso','BFA','854');
INSERT INTO psg_country VALUES ('BI','BURUNDI','Burundi','BDI','108');
INSERT INTO psg_country VALUES ('KH','CAMBODIA','Cambodia','KHM','116');
INSERT INTO psg_country VALUES ('CM','CAMEROON','Cameroon','CMR','120');
INSERT INTO psg_country VALUES ('CA','CANADA','Canada','CAN','124');
INSERT INTO psg_country VALUES ('CV','CAPE VERDE','Cape Verde','CPV','132');
INSERT INTO psg_country VALUES ('KY','CAYMAN ISLANDS','Cayman Islands','CYM','136');
INSERT INTO psg_country VALUES ('CF','CENTRAL AFRICAN REPUBLIC','Central African Republic','CAF','140');
INSERT INTO psg_country VALUES ('TD','CHAD','Chad','TCD','148');
INSERT INTO psg_country VALUES ('CL','CHILE','Chile','CHL','152');
INSERT INTO psg_country VALUES ('CN','CHINA','China','CHN','156');
INSERT INTO psg_country VALUES ('CX','CHRISTMAS ISLAND','Christmas Island',NULL,NULL);
INSERT INTO psg_country VALUES ('CC','COCOS (KEELING) ISLANDS','Cocos (Keeling) Islands',NULL,NULL);
INSERT INTO psg_country VALUES ('CO','COLOMBIA','Colombia','COL','170');
INSERT INTO psg_country VALUES ('KM','COMOROS','Comoros','COM','174');
INSERT INTO psg_country VALUES ('CG','CONGO','Congo','COG','178');
INSERT INTO psg_country VALUES ('CD','CONGO, THE DEMOCRATIC REPUBLIC OF THE','Congo, the Democratic Republic of the','COD','180');
INSERT INTO psg_country VALUES ('CK','COOK ISLANDS','Cook Islands','COK','184');
INSERT INTO psg_country VALUES ('CR','COSTA RICA','Costa Rica','CRI','188');
INSERT INTO psg_country VALUES ('CI','COTE D\'IVOIRE','Cote D\'Ivoire','CIV','384');
INSERT INTO psg_country VALUES ('HR','CROATIA','Croatia','HRV','191');
INSERT INTO psg_country VALUES ('CU','CUBA','Cuba','CUB','192');
INSERT INTO psg_country VALUES ('CY','CYPRUS','Cyprus','CYP','196');
INSERT INTO psg_country VALUES ('CZ','CZECH REPUBLIC','Czech Republic','CZE','203');
INSERT INTO psg_country VALUES ('DK','DENMARK','Denmark','DNK','208');
INSERT INTO psg_country VALUES ('DJ','DJIBOUTI','Djibouti','DJI','262');
INSERT INTO psg_country VALUES ('DM','DOMINICA','Dominica','DMA','212');
INSERT INTO psg_country VALUES ('DO','DOMINICAN REPUBLIC','Dominican Republic','DOM','214');
INSERT INTO psg_country VALUES ('EC','ECUADOR','Ecuador','ECU','218');
INSERT INTO psg_country VALUES ('EG','EGYPT','Egypt','EGY','818');
INSERT INTO psg_country VALUES ('SV','EL SALVADOR','El Salvador','SLV','222');
INSERT INTO psg_country VALUES ('GQ','EQUATORIAL GUINEA','Equatorial Guinea','GNQ','226');
INSERT INTO psg_country VALUES ('ER','ERITREA','Eritrea','ERI','232');
INSERT INTO psg_country VALUES ('EE','ESTONIA','Estonia','EST','233');
INSERT INTO psg_country VALUES ('ET','ETHIOPIA','Ethiopia','ETH','231');
INSERT INTO psg_country VALUES ('FK','FALKLAND ISLANDS (MALVINAS)','Falkland Islands (Malvinas)','FLK','238');
INSERT INTO psg_country VALUES ('FO','FAROE ISLANDS','Faroe Islands','FRO','234');
INSERT INTO psg_country VALUES ('FJ','FIJI','Fiji','FJI','242');
INSERT INTO psg_country VALUES ('FI','FINLAND','Finland','FIN','246');
INSERT INTO psg_country VALUES ('FR','FRANCE','France','FRA','250');
INSERT INTO psg_country VALUES ('GF','FRENCH GUIANA','French Guiana','GUF','254');
INSERT INTO psg_country VALUES ('PF','FRENCH POLYNESIA','French Polynesia','PYF','258');
INSERT INTO psg_country VALUES ('TF','FRENCH SOUTHERN TERRITORIES','French Southern Territories',NULL,NULL);
INSERT INTO psg_country VALUES ('GA','GABON','Gabon','GAB','266');
INSERT INTO psg_country VALUES ('GM','GAMBIA','Gambia','GMB','270');
INSERT INTO psg_country VALUES ('GE','GEORGIA','Georgia','GEO','268');
INSERT INTO psg_country VALUES ('DE','GERMANY','Germany','DEU','276');
INSERT INTO psg_country VALUES ('GH','GHANA','Ghana','GHA','288');
INSERT INTO psg_country VALUES ('GI','GIBRALTAR','Gibraltar','GIB','292');
INSERT INTO psg_country VALUES ('GR','GREECE','Greece','GRC','300');
INSERT INTO psg_country VALUES ('GL','GREENLAND','Greenland','GRL','304');
INSERT INTO psg_country VALUES ('GD','GRENADA','Grenada','GRD','308');
INSERT INTO psg_country VALUES ('GP','GUADELOUPE','Guadeloupe','GLP','312');
INSERT INTO psg_country VALUES ('GU','GUAM','Guam','GUM','316');
INSERT INTO psg_country VALUES ('GT','GUATEMALA','Guatemala','GTM','320');
INSERT INTO psg_country VALUES ('GN','GUINEA','Guinea','GIN','324');
INSERT INTO psg_country VALUES ('GW','GUINEA-BISSAU','Guinea-Bissau','GNB','624');
INSERT INTO psg_country VALUES ('GY','GUYANA','Guyana','GUY','328');
INSERT INTO psg_country VALUES ('HT','HAITI','Haiti','HTI','332');
INSERT INTO psg_country VALUES ('HM','HEARD ISLAND AND MCDONALD ISLANDS','Heard Island and Mcdonald Islands',NULL,NULL);
INSERT INTO psg_country VALUES ('VA','HOLY SEE (VATICAN CITY STATE)','Holy See (Vatican City State)','VAT','336');
INSERT INTO psg_country VALUES ('HN','HONDURAS','Honduras','HND','340');
INSERT INTO psg_country VALUES ('HK','HONG KONG','Hong Kong','HKG','344');
INSERT INTO psg_country VALUES ('HU','HUNGARY','Hungary','HUN','348');
INSERT INTO psg_country VALUES ('IS','ICELAND','Iceland','ISL','352');
INSERT INTO psg_country VALUES ('IN','INDIA','India','IND','356');
INSERT INTO psg_country VALUES ('ID','INDONESIA','Indonesia','IDN','360');
INSERT INTO psg_country VALUES ('IR','IRAN, ISLAMIC REPUBLIC OF','Iran, Islamic Republic of','IRN','364');
INSERT INTO psg_country VALUES ('IQ','IRAQ','Iraq','IRQ','368');
INSERT INTO psg_country VALUES ('IE','IRELAND','Ireland','IRL','372');
INSERT INTO psg_country VALUES ('IL','ISRAEL','Israel','ISR','376');
INSERT INTO psg_country VALUES ('IT','ITALY','Italy','ITA','380');
INSERT INTO psg_country VALUES ('JM','JAMAICA','Jamaica','JAM','388');
INSERT INTO psg_country VALUES ('JP','JAPAN','Japan','JPN','392');
INSERT INTO psg_country VALUES ('JO','JORDAN','Jordan','JOR','400');
INSERT INTO psg_country VALUES ('KZ','KAZAKHSTAN','Kazakhstan','KAZ','398');
INSERT INTO psg_country VALUES ('KE','KENYA','Kenya','KEN','404');
INSERT INTO psg_country VALUES ('KI','KIRIBATI','Kiribati','KIR','296');
INSERT INTO psg_country VALUES ('KP','KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF','Korea, Democratic People\'s Republic of','PRK','408');
INSERT INTO psg_country VALUES ('KR','KOREA, REPUBLIC OF','Korea, Republic of','KOR','410');
INSERT INTO psg_country VALUES ('KW','KUWAIT','Kuwait','KWT','414');
INSERT INTO psg_country VALUES ('KG','KYRGYZSTAN','Kyrgyzstan','KGZ','417');
INSERT INTO psg_country VALUES ('LA','LAO PEOPLE\'S DEMOCRATIC REPUBLIC','Lao People\'s Democratic Republic','LAO','418');
INSERT INTO psg_country VALUES ('LV','LATVIA','Latvia','LVA','428');
INSERT INTO psg_country VALUES ('LB','LEBANON','Lebanon','LBN','422');
INSERT INTO psg_country VALUES ('LS','LESOTHO','Lesotho','LSO','426');
INSERT INTO psg_country VALUES ('LR','LIBERIA','Liberia','LBR','430');
INSERT INTO psg_country VALUES ('LY','LIBYAN ARAB JAMAHIRIYA','Libyan Arab Jamahiriya','LBY','434');
INSERT INTO psg_country VALUES ('LI','LIECHTENSTEIN','Liechtenstein','LIE','438');
INSERT INTO psg_country VALUES ('LT','LITHUANIA','Lithuania','LTU','440');
INSERT INTO psg_country VALUES ('LU','LUXEMBOURG','Luxembourg','LUX','442');
INSERT INTO psg_country VALUES ('MO','MACAO','Macao','MAC','446');
INSERT INTO psg_country VALUES ('MK','MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','Macedonia, the Former Yugoslav Republic of','MKD','807');
INSERT INTO psg_country VALUES ('MG','MADAGASCAR','Madagascar','MDG','450');
INSERT INTO psg_country VALUES ('MW','MALAWI','Malawi','MWI','454');
INSERT INTO psg_country VALUES ('MY','MALAYSIA','Malaysia','MYS','458');
INSERT INTO psg_country VALUES ('MV','MALDIVES','Maldives','MDV','462');
INSERT INTO psg_country VALUES ('ML','MALI','Mali','MLI','466');
INSERT INTO psg_country VALUES ('MT','MALTA','Malta','MLT','470');
INSERT INTO psg_country VALUES ('MH','MARSHALL ISLANDS','Marshall Islands','MHL','584');
INSERT INTO psg_country VALUES ('MQ','MARTINIQUE','Martinique','MTQ','474');
INSERT INTO psg_country VALUES ('MR','MAURITANIA','Mauritania','MRT','478');
INSERT INTO psg_country VALUES ('MU','MAURITIUS','Mauritius','MUS','480');
INSERT INTO psg_country VALUES ('YT','MAYOTTE','Mayotte',NULL,NULL);
INSERT INTO psg_country VALUES ('MX','MEXICO','Mexico','MEX','484');
INSERT INTO psg_country VALUES ('FM','MICRONESIA, FEDERATED STATES OF','Micronesia, Federated States of','FSM','583');
INSERT INTO psg_country VALUES ('MD','MOLDOVA, REPUBLIC OF','Moldova, Republic of','MDA','498');
INSERT INTO psg_country VALUES ('MC','MONACO','Monaco','MCO','492');
INSERT INTO psg_country VALUES ('MN','MONGOLIA','Mongolia','MNG','496');
INSERT INTO psg_country VALUES ('MS','MONTSERRAT','Montserrat','MSR','500');
INSERT INTO psg_country VALUES ('MA','MOROCCO','Morocco','MAR','504');
INSERT INTO psg_country VALUES ('MZ','MOZAMBIQUE','Mozambique','MOZ','508');
INSERT INTO psg_country VALUES ('MM','MYANMAR','Myanmar','MMR','104');
INSERT INTO psg_country VALUES ('NA','NAMIBIA','Namibia','NAM','516');
INSERT INTO psg_country VALUES ('NR','NAURU','Nauru','NRU','520');
INSERT INTO psg_country VALUES ('NP','NEPAL','Nepal','NPL','524');
INSERT INTO psg_country VALUES ('NL','NETHERLANDS','Netherlands','NLD','528');
INSERT INTO psg_country VALUES ('AN','NETHERLANDS ANTILLES','Netherlands Antilles','ANT','530');
INSERT INTO psg_country VALUES ('NC','NEW CALEDONIA','New Caledonia','NCL','540');
INSERT INTO psg_country VALUES ('NZ','NEW ZEALAND','New Zealand','NZL','554');
INSERT INTO psg_country VALUES ('NI','NICARAGUA','Nicaragua','NIC','558');
INSERT INTO psg_country VALUES ('NE','NIGER','Niger','NER','562');
INSERT INTO psg_country VALUES ('NG','NIGERIA','Nigeria','NGA','566');
INSERT INTO psg_country VALUES ('NU','NIUE','Niue','NIU','570');
INSERT INTO psg_country VALUES ('NF','NORFOLK ISLAND','Norfolk Island','NFK','574');
INSERT INTO psg_country VALUES ('MP','NORTHERN MARIANA ISLANDS','Northern Mariana Islands','MNP','580');
INSERT INTO psg_country VALUES ('NO','NORWAY','Norway','NOR','578');
INSERT INTO psg_country VALUES ('OM','OMAN','Oman','OMN','512');
INSERT INTO psg_country VALUES ('PK','PAKISTAN','Pakistan','PAK','586');
INSERT INTO psg_country VALUES ('PW','PALAU','Palau','PLW','585');
INSERT INTO psg_country VALUES ('PS','PALESTINIAN TERRITORY, OCCUPIED','Palestinian Territory, Occupied',NULL,NULL);
INSERT INTO psg_country VALUES ('PA','PANAMA','Panama','PAN','591');
INSERT INTO psg_country VALUES ('PG','PAPUA NEW GUINEA','Papua New Guinea','PNG','598');
INSERT INTO psg_country VALUES ('PY','PARAGUAY','Paraguay','PRY','600');
INSERT INTO psg_country VALUES ('PE','PERU','Peru','PER','604');
INSERT INTO psg_country VALUES ('PH','PHILIPPINES','Philippines','PHL','608');
INSERT INTO psg_country VALUES ('PN','PITCAIRN','Pitcairn','PCN','612');
INSERT INTO psg_country VALUES ('PL','POLAND','Poland','POL','616');
INSERT INTO psg_country VALUES ('PT','PORTUGAL','Portugal','PRT','620');
INSERT INTO psg_country VALUES ('PR','PUERTO RICO','Puerto Rico','PRI','630');
INSERT INTO psg_country VALUES ('QA','QATAR','Qatar','QAT','634');
INSERT INTO psg_country VALUES ('RE','REUNION','Reunion','REU','638');
INSERT INTO psg_country VALUES ('RO','ROMANIA','Romania','ROM','642');
INSERT INTO psg_country VALUES ('RU','RUSSIAN FEDERATION','Russian Federation','RUS','643');
INSERT INTO psg_country VALUES ('RW','RWANDA','Rwanda','RWA','646');
INSERT INTO psg_country VALUES ('SH','SAINT HELENA','Saint Helena','SHN','654');
INSERT INTO psg_country VALUES ('KN','SAINT KITTS AND NEVIS','Saint Kitts and Nevis','KNA','659');
INSERT INTO psg_country VALUES ('LC','SAINT LUCIA','Saint Lucia','LCA','662');
INSERT INTO psg_country VALUES ('PM','SAINT PIERRE AND MIQUELON','Saint Pierre and Miquelon','SPM','666');
INSERT INTO psg_country VALUES ('VC','SAINT VINCENT AND THE GRENADINES','Saint Vincent and the Grenadines','VCT','670');
INSERT INTO psg_country VALUES ('WS','SAMOA','Samoa','WSM','882');
INSERT INTO psg_country VALUES ('SM','SAN MARINO','San Marino','SMR','674');
INSERT INTO psg_country VALUES ('ST','SAO TOME AND PRINCIPE','Sao Tome and Principe','STP','678');
INSERT INTO psg_country VALUES ('SA','SAUDI ARABIA','Saudi Arabia','SAU','682');
INSERT INTO psg_country VALUES ('SN','SENEGAL','Senegal','SEN','686');
INSERT INTO psg_country VALUES ('CS','SERBIA AND MONTENEGRO','Serbia and Montenegro',NULL,NULL);
INSERT INTO psg_country VALUES ('SC','SEYCHELLES','Seychelles','SYC','690');
INSERT INTO psg_country VALUES ('SL','SIERRA LEONE','Sierra Leone','SLE','694');
INSERT INTO psg_country VALUES ('SG','SINGAPORE','Singapore','SGP','702');
INSERT INTO psg_country VALUES ('SK','SLOVAKIA','Slovakia','SVK','703');
INSERT INTO psg_country VALUES ('SI','SLOVENIA','Slovenia','SVN','705');
INSERT INTO psg_country VALUES ('SB','SOLOMON ISLANDS','Solomon Islands','SLB','090');
INSERT INTO psg_country VALUES ('SO','SOMALIA','Somalia','SOM','706');
INSERT INTO psg_country VALUES ('ZA','SOUTH AFRICA','South Africa','ZAF','710');
INSERT INTO psg_country VALUES ('GS','SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS','South Georgia and the South Sandwich Islands',NULL,NULL);
INSERT INTO psg_country VALUES ('ES','SPAIN','Spain','ESP','724');
INSERT INTO psg_country VALUES ('LK','SRI LANKA','Sri Lanka','LKA','144');
INSERT INTO psg_country VALUES ('SD','SUDAN','Sudan','SDN','736');
INSERT INTO psg_country VALUES ('SR','SURINAME','Suriname','SUR','740');
INSERT INTO psg_country VALUES ('SJ','SVALBARD AND JAN MAYEN','Svalbard and Jan Mayen','SJM','744');
INSERT INTO psg_country VALUES ('SZ','SWAZILAND','Swaziland','SWZ','748');
INSERT INTO psg_country VALUES ('SE','SWEDEN','Sweden','SWE','752');
INSERT INTO psg_country VALUES ('CH','SWITZERLAND','Switzerland','CHE','756');
INSERT INTO psg_country VALUES ('SY','SYRIAN ARAB REPUBLIC','Syrian Arab Republic','SYR','760');
INSERT INTO psg_country VALUES ('TW','TAIWAN, PROVINCE OF CHINA','Taiwan, Province of China','TWN','158');
INSERT INTO psg_country VALUES ('TJ','TAJIKISTAN','Tajikistan','TJK','762');
INSERT INTO psg_country VALUES ('TZ','TANZANIA, UNITED REPUBLIC OF','Tanzania, United Republic of','TZA','834');
INSERT INTO psg_country VALUES ('TH','THAILAND','Thailand','THA','764');
INSERT INTO psg_country VALUES ('TL','TIMOR-LESTE','Timor-Leste',NULL,NULL);
INSERT INTO psg_country VALUES ('TG','TOGO','Togo','TGO','768');
INSERT INTO psg_country VALUES ('TK','TOKELAU','Tokelau','TKL','772');
INSERT INTO psg_country VALUES ('TO','TONGA','Tonga','TON','776');
INSERT INTO psg_country VALUES ('TT','TRINIDAD AND TOBAGO','Trinidad and Tobago','TTO','780');
INSERT INTO psg_country VALUES ('TN','TUNISIA','Tunisia','TUN','788');
INSERT INTO psg_country VALUES ('TR','TURKEY','Turkey','TUR','792');
INSERT INTO psg_country VALUES ('TM','TURKMENISTAN','Turkmenistan','TKM','795');
INSERT INTO psg_country VALUES ('TC','TURKS AND CAICOS ISLANDS','Turks and Caicos Islands','TCA','796');
INSERT INTO psg_country VALUES ('TV','TUVALU','Tuvalu','TUV','798');
INSERT INTO psg_country VALUES ('UG','UGANDA','Uganda','UGA','800');
INSERT INTO psg_country VALUES ('UA','UKRAINE','Ukraine','UKR','804');
INSERT INTO psg_country VALUES ('AE','UNITED ARAB EMIRATES','United Arab Emirates','ARE','784');
INSERT INTO psg_country VALUES ('GB','UNITED KINGDOM','United Kingdom','GBR','826');
INSERT INTO psg_country VALUES ('US','UNITED STATES','United States','USA','840');
INSERT INTO psg_country VALUES ('UM','UNITED STATES MINOR OUTLYING ISLANDS','United States Minor Outlying Islands',NULL,NULL);
INSERT INTO psg_country VALUES ('UY','URUGUAY','Uruguay','URY','858');
INSERT INTO psg_country VALUES ('UZ','UZBEKISTAN','Uzbekistan','UZB','860');
INSERT INTO psg_country VALUES ('VU','VANUATU','Vanuatu','VUT','548');
INSERT INTO psg_country VALUES ('VE','VENEZUELA','Venezuela','VEN','862');
INSERT INTO psg_country VALUES ('VN','VIET NAM','Viet Nam','VNM','704');
INSERT INTO psg_country VALUES ('VG','VIRGIN ISLANDS, BRITISH','Virgin Islands, British','VGB','092');
INSERT INTO psg_country VALUES ('VI','VIRGIN ISLANDS, U.S.','Virgin Islands, U.s.','VIR','850');
INSERT INTO psg_country VALUES ('WF','WALLIS AND FUTUNA','Wallis and Futuna','WLF','876');
INSERT INTO psg_country VALUES ('EH','WESTERN SAHARA','Western Sahara','ESH','732');
INSERT INTO psg_country VALUES ('YE','YEMEN','Yemen','YEM','887');
INSERT INTO psg_country VALUES ('ZM','ZAMBIA','Zambia','ZMB','894');
INSERT INTO psg_country VALUES ('ZW','ZIMBABWE','Zimbabwe','ZWE','716');





-- PIXARIA UPDATE 1.3.0 - 1.3.1

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.3.1' WHERE `name` = 'database_version' LIMIT 1 ;

CREATE TABLE `psg_payscript_log` (
`id` INT( 6 ) NOT NULL AUTO_INCREMENT ,
`ip_address` VARCHAR( 15 ) NOT NULL ,
`host_name` VARCHAR( 255 ) NOT NULL ,
`event_time` DATETIME NOT NULL ,
`post_vars` MEDIUMTEXT NOT NULL ,
`get_vars` MEDIUMTEXT NOT NULL ,
`cookie_vars` MEDIUMTEXT NOT NULL ,
UNIQUE (
`id`
)
);


-- PIXARIA UPDATE 1.3.1 - 1.3.2

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.3.2' WHERE `name` = 'database_version' LIMIT 1 ;

-- PIXARIA UPDATE 1.3.2 - 1.3.3

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.3.3' WHERE `name` = 'database_version' LIMIT 1 ;

-- PIXARIA UPDATE 1.3.3 - 1.4.0

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.4.0' WHERE `name` = 'database_version' LIMIT 1 ;

UPDATE `psg_settings` SET `name` = 'store_flatrate_ship', `value` = '1' WHERE `name` = 'store_default_ship' LIMIT 1 ;

UPDATE `psg_settings` SET `name` = 'store_flatrate_ship_val' WHERE `name` = 'store_default_ship_val' LIMIT 1 ;

INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('store_func_cheque', '0'), ('store_func_postalorder', '0');
INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('store_func_moneyorder', '0'), ('store_func_banktransfer', '0');
INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('view_thumb_view', 'large');
INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('search_default_order', '1');


-- PIXARIA UPDATE 1.4.0 - 1.4.1

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.4.1' WHERE `name` = 'database_version' LIMIT 1 ;

-- PIXARIA UPDATE 1.4.1 - 1.4.2

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.4.2' WHERE `name` = 'database_version' LIMIT 1 ;

INSERT INTO `psg_settings` (`name`, `value`) VALUES ('image_popup_width', '650'), ('image_popup_height', '500');

INSERT INTO `psg_settings` (`name`, `value`) VALUES ('cart_popup_width', '650'), ('cart_popup_height', '500');

INSERT INTO `psg_settings` (`name`, `value`) VALUES ('lbox_popup_width', '500'), ('lbox_popup_height', '400');



-- PIXARIA UPDATE 1.4.3 - 1.5.0

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.5.0' WHERE `name` = 'database_version' LIMIT 1 ;

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'image_watermark_pos', '10'
);

ALTER TABLE `psg_images` DROP INDEX `image_filename` ,
ADD FULLTEXT `image_filename` (
`image_title` ,
`image_caption` ( 1 ) ,
`image_keywords` ( 1 ) ,
`image_filename` ( 1 )
);

-- PIXARIA UPDATE 1.5.0 - 1.5.1

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.5.1' WHERE `name` = 'database_version' LIMIT 1 ;

-- PIXARIA UPDATE 1.5.1 - 1.5.2

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.5.2' WHERE `name` = 'database_version' LIMIT 1 ;

-- PIXARIA UPDATE 1.5.2 - 1.5.3

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '1.5.3' WHERE `name` = 'database_version' LIMIT 1 ;


-- PIXARIA UPDATE 1.5.3 - 2.0.0

-- CHANGE DATABASE VERSION
UPDATE `psg_settings` SET `value` = '2.0.0' WHERE `name` = 'database_version' LIMIT 1 ;

CREATE TABLE `psg_keyword_tags` (`id` INT( 5 ) NOT NULL AUTO_INCREMENT , `keyword` VARCHAR( 50 )  NOT NULL , PRIMARY KEY ( `id` ));

CREATE TABLE `psg_keywords_images` (
`id` INT( 5 ) NOT NULL AUTO_INCREMENT ,
`image_id` INT( 5 ) NOT NULL ,
`keyword_id` INT( 5 ) NOT NULL ,
PRIMARY KEY ( `id` )
);

ALTER TABLE `psg_lightbox_members` CHANGE `order` `order` INT( 6 ) NOT NULL;

INSERT INTO `psg_settings` ( `name` , `value` ) VALUES ('browse_thumb_count', '5');

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'search_image_color', '1'
), (
'search_orientation', '1'
);

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'func_contact_sheet', '1'
), (
'contact_sheet_quality', 'low'
);

ALTER TABLE `psg_galleries` ADD `gallery_short_title` VARCHAR( 50 ) AFTER `gallery_title` ;

CREATE TABLE `psg_download_log` (
`id` INT( 6 ) NOT NULL AUTO_INCREMENT ,
`image_id` INT( 7 ) NOT NULL ,
`userid` VARCHAR( 7 ) NOT NULL ,
`time` DATETIME NOT NULL ,
`type` ENUM( 'jpg', 'psd', 'jp2', 'eps', 'tif' ) NOT NULL ,
`transaction` ENUM( 'free', 'pay' ) NOT NULL ,
PRIMARY KEY ( `id` )
);

UPDATE `psg_settings` SET `value` = 'Illuminux' WHERE `name` = 'theme' LIMIT 1 ;

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'keywords_updated', '0'
);

ALTER TABLE `psg_galleries` ADD `gallery_active` INT( 1 ) DEFAULT '1' NOT NULL AFTER `gallery_id` ;

ALTER TABLE `psg_images` ADD `image_active` INT( 1 ) DEFAULT '1' NOT NULL AFTER `image_id` ;

CREATE TABLE `psg_images_products` (
`imgprod_id` INT( 8 ) DEFAULT '0' NOT NULL ,
`image_id` INT( 8 ) DEFAULT '0' NOT NULL ,
`product_id` INT( 8 ) DEFAULT '0' NOT NULL
);

ALTER TABLE `psg_images` ADD `image_product_link` INT( 1 ) DEFAULT '0' NOT NULL AFTER `image_sale` ;

INSERT INTO `psg_settings` ( `name` , `value` ) 
VALUES (
'func_contact', '1'
);

ALTER TABLE `psg_images` ADD `image_trashed` INT( 1 ) DEFAULT '0' NOT NULL AFTER `image_counter` ,
ADD `image_content_type` ENUM( '10', '11', '12', '13' ) DEFAULT '10' NOT NULL AFTER `image_trashed` ;

