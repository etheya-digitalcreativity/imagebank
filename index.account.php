<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set up constant for front end activities
define("WORKSPACE","FE");

// Set the include path for files used in this script
ini_set("include_path","resources/includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Send HTTP header and don't cache
pix_http_headers("html","");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

$objIndexAccount = new IndexAccount();

class IndexAccount {
	
	var $_dbl;
	
	/*
	*
	*	
	*
	*/
	function IndexAccount () {
		
		global $cfg, $smarty, $objEnvData, $ses;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		switch ($objEnvData->fetchGlobal('cmd')) {
			
			case "userRegistrationStart":
				$ses['psg_full_user'] ? $this->displayAccountHomepage() : $this->userRegistrationStart();
			break;
			
			case "userRegistrationComplete":
				$ses['psg_full_user'] ? $this->displayAccountHomepage() : $this->userRegistrationComplete();
			break;
			
			case "editUserAccount":
				$this->editUserAccount();
			break;
			
			case "saveUserAccountChanges":
				$this->saveUserAccountChanges();
			break;
			
			case "acceptInvitation":
				$this->acceptInvitation();
			break;
			
			case "confirmCompleteRegistration":
				$this->confirmCompleteRegistration();
			break;
			
			case "resetPasswordStart":
				$this->resetPasswordStart();
			break;
			
			case "resetPasswordEnterEmail":
				$this->resetPasswordEnterEmail();
			break;
			
			case "resetPasswordAnswerQuestion":
				$this->resetPasswordAnswerQuestion();
			break;
			
			case "resetPasswordNewPassword":
				$this->resetPasswordNewPassword();
			break;
			
			default:
				$this->displayAccountHomepage();
			break;
		
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function displayAccountHomepage () {
		
		global $smarty, $cfg, $ses;
		
		// If the user isn't logged in, bounce to the login screen
		pix_authorise_user("basic");
		
		// Set the userid
		$userid = $ses['psg_userid'];
		
		// Get an array of all shopping carts this user has
		$sql	 = "SELECT ".PIX_TABLE_CART.".*, COUNT(id) AS cart_items FROM ".PIX_TABLE_CART."
		
					LEFT JOIN ".PIX_TABLE_CRME." ON ".PIX_TABLE_CRME.".cart_id = ".PIX_TABLE_CART.".cart_id
						
					WHERE ".PIX_TABLE_CART.".userid = '$userid'
					
					GROUP BY ".PIX_TABLE_CART.".cart_id
					
					ORDER BY ".PIX_TABLE_CART.".cart_id DESC";
		
		// Retrieve image information from database
		$cart_data = $this->_dbl->sqlSelectRows($sql);
		
		if (is_array($cart_data)) {
		
			foreach ($cart_data as $key => $value) {
			
				$cart_id[]				= $value['cart_id'];
				$cart_items[]			= $value['cart_items'];
				$cart_status[]			= $value['status'];
				$transaction_id[]		= $value['transaction_id'];
				$date_created[]			= $value['date_created'];
				$date_processed[]		= $value['date_processed'];
				$date_completed[]		= $value['date_completed'];
				
				switch ($value['status']) {
				
					case 1:
							
						$cart_status_text[] = $GLOBALS['_STR_']['ACCOUNT_121'];
						$cart_view_url[]	= $cfg['sys']['base_url'].$cfg['fil']['index_store'];
						
					break;
				
					case 2:
							
						$cart_status_text[] = $GLOBALS['_STR_']['ACCOUNT_122'];
						$cart_view_url[]	= $cfg['sys']['base_url'].$cfg['fil']['index_store']."?cmd=viewTransaction&amp;cart_id=".$value['cart_id'];
						
					break;
				
					case 3:
							
						$cart_status_text[] = $GLOBALS['_STR_']['ACCOUNT_123'];
						$cart_view_url[]	= $cfg['sys']['base_url'].$cfg['fil']['index_store']."?cmd=viewTransaction&amp;cart_id=".$value['cart_id'];
					
					break;
				
					case 4:
							
						$cart_status_text[] = $GLOBALS['_STR_']['ACCOUNT_124'];
						$cart_view_url[]	= $cfg['sys']['base_url'].$cfg['fil']['index_store']."?cmd=viewTransaction&amp;cart_id=".$value['cart_id'];
					
					break;
				
				}
			
			}
			
			$smarty->assign("carts_present",(bool)TRUE);
			$smarty->assign("cart_id",$cart_id);
			$smarty->assign("cart_items",$cart_items);
			$smarty->assign("cart_status",$cart_status);
			$smarty->assign("cart_status_text",$cart_status_text);
			$smarty->assign("cart_view_url",$cart_view_url);
			$smarty->assign("status_color",$status_color);
			$smarty->assign("transaction_id",$transaction_id);
			$smarty->assign("date_created",$date_created);
			$smarty->assign("date_processed",$date_processed);
			$smarty->assign("date_completed",$date_completed);
			
		}
		
		
		// Define html page title
		$smarty->assign("page_title",$GLOBALS['_STR_']['ACCOUNT_082']);
		
		// Output html from template file
		$smarty->pixDisplay('index.account/index.account.tpl');

	}
	
	/*
	*
	*	
	*
	*/
	function userRegistrationStart () {
		
		global $cfg, $smarty;
		
		// Load the country class
		require_once ("class.PixariaCountry.php");
		
		// Initialise the country class
		$objCountry = new PixariaCountry();
		
		// Assign country data to Smarty
		$smarty->assign("iso_codes",$objCountry->getIsoCodes());
		$smarty->assign("printable_names",$objCountry->getPrintableNames());
		
		// Assign Smarty data for required registration information
		$this->smartyAssignRegistrationFields();
		
		// Assign Smarty data for country drop down menu
		$this->smartyAssignCountryData();
		
		// Generate a captcha key
		$smarty->assign("captcha_key",captchaKey());
		
		// Define html page title
		$smarty->assign("page_title",$GLOBALS['_STR_']['ACCOUNT_083']);
		
		// Output html from template file
		$smarty->pixDisplay('index.account/account.create.tpl');
	
	}
	
	/*
	*
	*	
	*
	*/
	function userRegistrationComplete () {
	
		global $objEnvData, $cfg, $ses, $smarty;
		
		require_once("class.PixariaUser.php");
		
		if ($ses['psg_userid']) {
			$objPixariaUser = new PixariaUser($ses['psg_userid']);
		} else {
			$objPixariaUser = new PixariaUser();
		}
		
		$formal_title 				= $objEnvData->fetchGlobal('formal_title');
		$other_title 				= $objEnvData->fetchGlobal('other_title');
		$first_name 				= $objEnvData->fetchGlobal('first_name');
		$middle_initial 			= $objEnvData->fetchGlobal('middle_initial');
		$family_name				= $objEnvData->fetchGlobal('family_name');
		$email_address				= $objEnvData->fetchGlobal('email_address');
		$password_1					= $objEnvData->fetchGlobal('password_1');
		$password_2					= $objEnvData->fetchGlobal('password_2');
		$password_rem_que			= $objEnvData->fetchGlobal('password_rem_que');
		$password_rem_ans			= $objEnvData->fetchGlobal('password_rem_ans');
		$date_registered 			= $objEnvData->fetchGlobal('date_registered');
		$date_edited 				= $objEnvData->fetchGlobal('date_edited');
		$date_expiration 			= $objEnvData->fetchGlobal('date_expiration');
		$account_status 			= $objEnvData->fetchGlobal('account_status');
		$telephone 					= $objEnvData->fetchGlobal('telephone');
		$mobile 					= $objEnvData->fetchGlobal('mobile');
		$fax 						= $objEnvData->fetchGlobal('fax');
		$addr1 						= $objEnvData->fetchGlobal('addr1');
		$addr2 						= $objEnvData->fetchGlobal('addr2');
		$addr3 						= $objEnvData->fetchGlobal('addr3');
		$city 						= $objEnvData->fetchGlobal('city');
		$region 					= $objEnvData->fetchGlobal('region');
		$country 					= $objEnvData->fetchGlobal('country');
		$postal_code 				= $objEnvData->fetchGlobal('postal_code');
		$other_business_type 		= $objEnvData->fetchGlobal('other_business_type');
		$other_business_position 	= $objEnvData->fetchGlobal('other_business_position');
		$other_image_interest 		= $objEnvData->fetchGlobal('other_image_interest');
		$other_frequency 			= $objEnvData->fetchGlobal('other_frequency');
		$other_circulation 			= $objEnvData->fetchGlobal('other_circulation');
		$other_territories 			= $objEnvData->fetchGlobal('other_territories');
		$other_website 				= $objEnvData->fetchGlobal('other_website');
		$other_company_name 		= $objEnvData->fetchGlobal('other_company_name');
		$other_message 				= $objEnvData->fetchGlobal('other_message');
		
		// Otherwise, set user data properties
		$objPixariaUser->setSalutation($formal_title);
		$objPixariaUser->setFirstName($first_name);
		$objPixariaUser->setInitial($middle_initial);
		$objPixariaUser->setFamilyName($family_name);
		$objPixariaUser->setEmail($email_address);
		$objPixariaUser->setPassword1($password_1);
		$objPixariaUser->setPassword2($password_2);
		$objPixariaUser->setReminderQuestion($password_rem_que);
		$objPixariaUser->setReminderAnswer($password_rem_ans);
		$objPixariaUser->setTelephone($telephone);
		$objPixariaUser->setMobile($mobile);
		$objPixariaUser->setFax($fax);
		$objPixariaUser->setAddress1($addr1);
		$objPixariaUser->setAddress2($addr2);
		$objPixariaUser->setAddress3($addr3);
		$objPixariaUser->setCity($city);
		$objPixariaUser->setRegion($region);
		$objPixariaUser->setCountry($country);
		$objPixariaUser->setPostCode($postal_code);
		$objPixariaUser->setBusinessType($other_business_type);
		$objPixariaUser->setPosition($other_business_position);
		$objPixariaUser->setInterest($other_image_interest);
		$objPixariaUser->setPubFrequency($other_frequency);
		$objPixariaUser->setPubCirculation($other_circulation);
		$objPixariaUser->setTerritories($other_territories);
		$objPixariaUser->setWebsite($other_website);
		$objPixariaUser->setCompanyName($other_company_name);
		$objPixariaUser->setMessage($other_message);
		
		/*
		*	Validate captcha (if it's being used
		*/
		$captcha_code 		= strtolower($objEnvData->fetchGlobal('captcha_code'));
		$captcha_key		= $objEnvData->fetchGlobal('captcha_key');
		list($captcha_db) 	= $this->_dbl->sqlSelectRow("SELECT `code` FROM ".PIX_TABLE_CAPT." WHERE `key` = '".mysql_real_escape_string($captcha_key)."'");
		if ((strtolower($captcha_db) !== $captcha_code) && $cfg['set']['func_captcha_registration']) { $captcha_failure = true; $smarty->assign("captcha_failure",$captcha_failure); }
		
		
		/*
		* 	Edit or create the user's profile information
		*	depending on whether they have a userid already
		*/
		if ($ses['psg_userid']) {
			
			// Set the appropriate account status
			if ($cfg['set']['user_auto_approve'] == 1) { $objPixariaUser->setAccountStatus(1); } else { $objPixariaUser->setAccountStatus(0); }
			
			// Edit the user's empty profile to add all their new details
			$success = $objPixariaUser->editUser('user','edit',false);
			
		} elseif (!$captcha_failure) {
		
			$success = $objPixariaUser->createNewUserComplete();
			
		}
		
		/*
		*	Check if the profile was created successfully
		*/
		if ($success && !$captcha_failure) { // The new user account is now complete
	
			// Define html page title
			$smarty->assign("page_title",$GLOBALS['_STR_']['ACCOUNT_084']);
		
			// Output html from template file
			$smarty->pixDisplay('index.account/account.created.tpl');

		} else { // The validation failed and the user needs to edit some bits
			
			// Assign user variables to smarty
			$smarty->assign("formal_title",stripslashes($formal_title));
			$smarty->assign("first_name",stripslashes($first_name));
			$smarty->assign("middle_initial",stripslashes($middle_initial));
			$smarty->assign("family_name",stripslashes($family_name));
			$smarty->assign("email_address",$objPixariaUser->getEmail());
			$smarty->assign("telephone",stripslashes($telephone));
			$smarty->assign("mobile",stripslashes($mobile));
			$smarty->assign("fax",stripslashes($fax));
			$smarty->assign("addr1",stripslashes($addr1));
			$smarty->assign("addr2",stripslashes($addr2));
			$smarty->assign("addr3",stripslashes($addr3));
			$smarty->assign("city",stripslashes($city));
			$smarty->assign("region",stripslashes($region));
			$smarty->assign("country",stripslashes($country));
			$smarty->assign("postal_code",stripslashes($postal_code));
			$smarty->assign("password_1",stripslashes($password_1));
			$smarty->assign("password_2",stripslashes($password_2));
			$smarty->assign("password_rem_que",stripslashes($password_rem_que));
			$smarty->assign("password_rem_ans",stripslashes($password_rem_ans));
			$smarty->assign("other_business_type",stripslashes($other_business_type));
			$smarty->assign("other_business_position",stripslashes($other_business_position));
			$smarty->assign("other_image_interest",stripslashes($other_image_interest));
			$smarty->assign("other_frequency",stripslashes($other_frequency));
			$smarty->assign("other_circulation",stripslashes($other_circulation));
			$smarty->assign("other_territories",stripslashes($other_territories));
			$smarty->assign("other_website",stripslashes($other_website));
			$smarty->assign("other_company_name",stripslashes($other_company_name));
			$smarty->assign("other_message",stripslashes($other_message));

			$smarty->assign("profile_errors",$objPixariaUser->getProfileErrors());
			$smarty->assign("problem",true);
			
			// Assign Smarty data for required registration information
			$this->smartyAssignRegistrationFields();
			
			// Assign Smarty data for country drop down menu
			$this->smartyAssignCountryData();
			
			// Generate a captcha key
			$smarty->assign("captcha_key",captchaKey());
		
			// Define html page title
			$smarty->assign("page_title",$GLOBALS['_STR_']['ACCOUNT_085']);
		
			// Output html from template file
			$smarty->pixDisplay('index.account/account.create.tpl');

		}
			
	}
	
	/*
	*
	*	
	*
	*/
	function editUserAccount () {
	
		global $objEnvData, $cfg, $ses, $smarty;
		
		// If the user isn't logged in, bounce to the login screen
		pix_authorise_user("basic");
		
		$userid = $ses['psg_userid'];
		
		require_once("class.PixariaUser.php");
		
		$objPixariaUser = new PixariaUser($userid);
		
		$userid 					= $objPixariaUser->getUserID();
		$email_address 				= $objPixariaUser->getEmail();
		$formal_title 				= $objPixariaUser->getSalutation();
		$other_title 				= $objPixariaUser->getOtherSalutation();
		$first_name 				= $objPixariaUser->getFirstName();
		$middle_initial 			= $objPixariaUser->getInitial();
		$family_name				= $objPixariaUser->getFamilyName();
		$password_rem_que			= $objPixariaUser->getReminderQuestion();
		$password_rem_ans			= $objPixariaUser->getReminderAnswer();
		$date_registered 			= $objPixariaUser->getDateRegistered();
		$date_edited 				= $objPixariaUser->getDateEdited();
		$date_expiration 			= $objPixariaUser->getDateExpires();
		$account_status 			= $objPixariaUser->getAccountStatus();
		$telephone 					= $objPixariaUser->getTelephone();
		$mobile 					= $objPixariaUser->getMobile();
		$fax 						= $objPixariaUser->getFax();
		$addr1 						= $objPixariaUser->getAddress1();
		$addr2 						= $objPixariaUser->getAddress2();
		$addr3 						= $objPixariaUser->getAddress3();
		$city 						= $objPixariaUser->getCity();
		$region 					= $objPixariaUser->getRegion();
		$country 					= $objPixariaUser->getCountry();
		$postal_code 				= $objPixariaUser->getPostCode();
		$other_business_type 		= $objPixariaUser->getBusinessType();
		$other_business_position 	= $objPixariaUser->getBusinessPosition();
		$other_image_interest 		= $objPixariaUser->getInterest();
		$other_frequency 			= $objPixariaUser->getPubFrequency();
		$other_circulation 			= $objPixariaUser->getPubCirculation();
		$other_territories 			= $objPixariaUser->getTerritories();
		$other_website 				= $objPixariaUser->getWebsite();
		$other_company_name 		= $objPixariaUser->getCompanyName();
		$other_message 				= $objPixariaUser->getMessage();
		
		// Assign user variables to smarty
		$smarty->assign("userid",$userid);
		$smarty->assign("formal_title",stripslashes($formal_title));
		$smarty->assign("other_title",stripslashes($other_title));
		$smarty->assign("first_name",stripslashes($first_name));
		$smarty->assign("middle_initial",stripslashes($middle_initial));
		$smarty->assign("family_name",stripslashes($family_name));
		$smarty->assign("email_address",stripslashes($email_address));
		$smarty->assign("telephone",stripslashes($telephone));
		$smarty->assign("mobile",stripslashes($mobile));
		$smarty->assign("fax",stripslashes($fax));
		$smarty->assign("addr1",stripslashes($addr1));
		$smarty->assign("addr2",stripslashes($addr2));
		$smarty->assign("addr3",stripslashes($addr3));
		$smarty->assign("city",stripslashes($city));
		$smarty->assign("region",stripslashes($region));
		$smarty->assign("country",stripslashes($country));
		$smarty->assign("postal_code",stripslashes($postal_code));
		$smarty->assign("password_rem_que",stripslashes($password_rem_que));
		$smarty->assign("password_rem_ans",stripslashes($password_rem_ans));
		$smarty->assign("date_registered",strtotime($date_registered));
		$smarty->assign("date_edited",strtotime($date_edited));
		$smarty->assign("account_status",$account_status);
		$smarty->assign("other_business_type",stripslashes($other_business_type));
		$smarty->assign("other_business_position",stripslashes($other_business_position));
		$smarty->assign("other_image_interest",stripslashes($other_image_interest));
		$smarty->assign("other_frequency",stripslashes($other_frequency));
		$smarty->assign("other_circulation",stripslashes($other_circulation));
		$smarty->assign("other_territories",stripslashes($other_territories));
		$smarty->assign("other_website",stripslashes($other_website));
		$smarty->assign("other_company_name",stripslashes($other_company_name));
		$smarty->assign("other_message",stripslashes($other_message));
		
		// Assign Smarty data for required registration information
		$this->smartyAssignRegistrationFields();
		
		// Assign Smarty data for country drop down menu
		$this->smartyAssignCountryData();
		
		// Define html page title
		$smarty->assign("page_title",$GLOBALS['_STR_']['ACCOUNT_086']);
		
		// Output html from template file
		$smarty->pixDisplay('index.account/account.edit.tpl');
	
	}
	
	/*
	*
	*	
	*
	*/
	function saveUserAccountChanges () {
	
		global $objEnvData, $cfg, $ses, $smarty;
		
		// If the user isn't logged in, bounce to the login screen
		pix_authorise_user("basic");
		
		$userid = $ses['psg_userid'];
		
		require_once("class.PixariaUser.php");
		
		$objPixariaUser = new PixariaUser($userid);
		
		$formal_title 				= $objEnvData->fetchGlobal('formal_title');
		$other_title 				= $objEnvData->fetchGlobal('other_title');
		$first_name 				= $objEnvData->fetchGlobal('first_name');
		$middle_initial 			= $objEnvData->fetchGlobal('middle_initial');
		$family_name				= $objEnvData->fetchGlobal('family_name');
		$email_address				= $objEnvData->fetchGlobal('email_address');
		$password_1					= $objEnvData->fetchGlobal('password_1');
		$password_2					= $objEnvData->fetchGlobal('password_2');
		$password_rem_que			= $objEnvData->fetchGlobal('password_rem_que');
		$password_rem_ans			= $objEnvData->fetchGlobal('password_rem_ans');
		$date_registered 			= $objEnvData->fetchGlobal('date_registered');
		$date_edited 				= $objEnvData->fetchGlobal('date_edited');
		$date_expiration 			= $objEnvData->fetchGlobal('date_expiration');
		$account_status 			= $objEnvData->fetchGlobal('account_status');
		$telephone 					= $objEnvData->fetchGlobal('telephone');
		$mobile 					= $objEnvData->fetchGlobal('mobile');
		$fax 						= $objEnvData->fetchGlobal('fax');
		$addr1 						= $objEnvData->fetchGlobal('addr1');
		$addr2 						= $objEnvData->fetchGlobal('addr2');
		$addr3 						= $objEnvData->fetchGlobal('addr3');
		$city 						= $objEnvData->fetchGlobal('city');
		$region 					= $objEnvData->fetchGlobal('region');
		$country 					= $objEnvData->fetchGlobal('country');
		$postal_code 				= $objEnvData->fetchGlobal('postal_code');
		$other_business_type 		= $objEnvData->fetchGlobal('other_business_type');
		$other_business_position 	= $objEnvData->fetchGlobal('other_business_position');
		$other_image_interest 		= $objEnvData->fetchGlobal('other_image_interest');
		$other_frequency 			= $objEnvData->fetchGlobal('other_frequency');
		$other_circulation 			= $objEnvData->fetchGlobal('other_circulation');
		$other_territories 			= $objEnvData->fetchGlobal('other_territories');
		$other_website 				= $objEnvData->fetchGlobal('other_website');
		$other_company_name 		= $objEnvData->fetchGlobal('other_company_name');
		$other_message 				= $objEnvData->fetchGlobal('other_message');
		
		// Otherwise, set user data properties
		$objPixariaUser->setSalutation($formal_title);
		$objPixariaUser->setFirstName($first_name);
		$objPixariaUser->setInitial($middle_initial);
		$objPixariaUser->setFamilyName($family_name);
		$objPixariaUser->setEmail($email_address);
		$objPixariaUser->setPassword1($password_1);
		$objPixariaUser->setPassword2($password_2);
		$objPixariaUser->setReminderQuestion($password_rem_que);
		$objPixariaUser->setReminderAnswer($password_rem_ans);
		$objPixariaUser->setTelephone($telephone);
		$objPixariaUser->setMobile($mobile);
		$objPixariaUser->setFax($fax);
		$objPixariaUser->setAddress1($addr1);
		$objPixariaUser->setAddress2($addr2);
		$objPixariaUser->setAddress3($addr3);
		$objPixariaUser->setCity($city);
		$objPixariaUser->setRegion($region);
		$objPixariaUser->setCountry($country);
		$objPixariaUser->setPostCode($postal_code);
		$objPixariaUser->setBusinessType($other_business_type);
		$objPixariaUser->setPosition($other_business_position);
		$objPixariaUser->setInterest($other_image_interest);
		$objPixariaUser->setPubFrequency($other_frequency);
		$objPixariaUser->setPubCirculation($other_circulation);
		$objPixariaUser->setTerritories($other_territories);
		$objPixariaUser->setWebsite($other_website);
		$objPixariaUser->setCompanyName($other_company_name);
		$objPixariaUser->setMessage($other_message);
		
		// Edit the user's profile information
		// Apply data validation in mode = 'user' and type = 'edit'
		$success = $objPixariaUser->editUser('user','edit',false);
		
		if ($success) { // The new user account is now complete
	
			// Define html page title
			$smarty->assign("page_title",$GLOBALS['_STR_']['ACCOUNT_087']);
		
			// Output html from template file
			$smarty->pixDisplay('index.account/account.edit.success.tpl');

		} else { // The validation failed and the user needs to edit some bits
		
			// Assign user variables to smarty
			$smarty->assign("userid",$userid);
			$smarty->assign("key",$submitted_key);
			$smarty->assign("formal_title",stripslashes($formal_title));
			$smarty->assign("first_name",stripslashes($first_name));
			$smarty->assign("middle_initial",stripslashes($middle_initial));
			$smarty->assign("family_name",stripslashes($family_name));
			$smarty->assign("email_address",$objPixariaUser->getEmail());
			$smarty->assign("telephone",stripslashes($telephone));
			$smarty->assign("mobile",stripslashes($mobile));
			$smarty->assign("fax",stripslashes($fax));
			$smarty->assign("addr1",stripslashes($addr1));
			$smarty->assign("addr2",stripslashes($addr2));
			$smarty->assign("addr3",stripslashes($addr3));
			$smarty->assign("city",stripslashes($city));
			$smarty->assign("region",stripslashes($region));
			$smarty->assign("country",stripslashes($country));
			$smarty->assign("postal_code",stripslashes($postal_code));
			$smarty->assign("password_rem_que",stripslashes($password_rem_que));
			$smarty->assign("password_rem_ans",stripslashes($password_rem_ans));
			$smarty->assign("other_business_type",stripslashes($other_business_type));
			$smarty->assign("other_business_position",stripslashes($other_business_position));
			$smarty->assign("other_image_interest",stripslashes($other_image_interest));
			$smarty->assign("other_frequency",stripslashes($other_frequency));
			$smarty->assign("other_circulation",stripslashes($other_circulation));
			$smarty->assign("other_territories",stripslashes($other_territories));
			$smarty->assign("other_website",stripslashes($other_website));
			$smarty->assign("other_company_name",stripslashes($other_company_name));
			$smarty->assign("other_message",stripslashes($other_message));

			$smarty->assign("profile_errors",$objPixariaUser->getProfileErrors());
			$smarty->assign("problem",true);
			
			// Assign Smarty data for required registration information
			$this->smartyAssignRegistrationFields();
			
			// Assign Smarty data for country drop down menu
			$this->smartyAssignCountryData();
			
			// Define html page title
			$smarty->assign("page_title",$GLOBALS['_STR_']['ACCOUNT_086']);
		
			// Output html from template file
			$smarty->pixDisplay('index.account/account.edit.tpl');

		}
			
	}
	
	/*
	*
	*	
	*
	*/
	function acceptInvitation () {
		
		global $objEnvData, $cfg, $smarty;
		
		$userid 		= $objEnvData->fetchGlobal('userid');
		$submitted_key	= $objEnvData->fetchGlobal('key');
		
		$generated_key	= strtoupper(substr(md5($userid.$cfg['sys']['encryption_key']),0,6));
		
		if ($submitted_key == $generated_key) {
			
			require_once("class.PixariaUser.php");
			
			$objPixariaUser = new PixariaUser($userid);
			
			// Check the user hasn't already accepted the invite
			if ($objPixariaUser->getPassword() != "") {
		
				$this->invitationError();
				
			}
			
			$userid 					= $objPixariaUser->getUserID();
			$email_address 				= $objPixariaUser->getEmail();
			$formal_title 				= $objPixariaUser->getSalutation();
			$other_title 				= $objPixariaUser->getOtherSalutation();
			$first_name 				= $objPixariaUser->getFirstName();
			$middle_initial 			= $objPixariaUser->getInitial();
			$family_name				= $objPixariaUser->getFamilyName();
			$password_rem_que			= $objPixariaUser->getReminderQuestion();
			$password_rem_ans			= $objPixariaUser->getReminderAnswer();
			$date_registered 			= $objPixariaUser->getDateRegistered();
			$date_edited 				= $objPixariaUser->getDateEdited();
			$date_expiration 			= $objPixariaUser->getDateExpires();
			$account_status 			= $objPixariaUser->getAccountStatus();
			$telephone 					= $objPixariaUser->getTelephone();
			$mobile 					= $objPixariaUser->getMobile();
			$fax 						= $objPixariaUser->getFax();
			$addr1 						= $objPixariaUser->getAddress1();
			$addr2 						= $objPixariaUser->getAddress2();
			$addr3 						= $objPixariaUser->getAddress3();
			$city 						= $objPixariaUser->getCity();
			$region 					= $objPixariaUser->getRegion();
			$country 					= $objPixariaUser->getCountry();
			$postal_code 				= $objPixariaUser->getPostCode();
			$other_business_type 		= $objPixariaUser->getBusinessType();
			$other_business_position 	= $objPixariaUser->getBusinessPosition();
			$other_image_interest 		= $objPixariaUser->getInterest();
			$other_frequency 			= $objPixariaUser->getPubFrequency();
			$other_circulation 			= $objPixariaUser->getPubCirculation();
			$other_territories 			= $objPixariaUser->getTerritories();
			$other_website 				= $objPixariaUser->getWebsite();
			$other_company_name 		= $objPixariaUser->getCompanyName();
			$other_message 				= $objPixariaUser->getMessage();
			
			// Assign user variables to smarty
			$smarty->assign("userid",$userid);
			$smarty->assign("key",$submitted_key);
			$smarty->assign("formal_title",stripslashes($formal_title));
			$smarty->assign("other_title",stripslashes($other_title));
			$smarty->assign("first_name",stripslashes($first_name));
			$smarty->assign("middle_initial",stripslashes($middle_initial));
			$smarty->assign("family_name",stripslashes($family_name));
			$smarty->assign("email_address",stripslashes($email_address));
			$smarty->assign("telephone",stripslashes($telephone));
			$smarty->assign("mobile",stripslashes($mobile));
			$smarty->assign("fax",stripslashes($fax));
			$smarty->assign("addr1",stripslashes($addr1));
			$smarty->assign("addr2",stripslashes($addr2));
			$smarty->assign("addr3",stripslashes($addr3));
			$smarty->assign("city",stripslashes($city));
			$smarty->assign("region",stripslashes($region));
			$smarty->assign("country",stripslashes($country));
			$smarty->assign("postal_code",stripslashes($postal_code));
			$smarty->assign("password_rem_que",stripslashes($password_rem_que));
			$smarty->assign("password_rem_ans",stripslashes($password_rem_ans));
			$smarty->assign("date_registered",strtotime($date_registered));
			$smarty->assign("date_edited",strtotime($date_edited));
			$smarty->assign("account_status",$account_status);
			$smarty->assign("other_business_type",stripslashes($other_business_type));
			$smarty->assign("other_business_position",stripslashes($other_business_position));
			$smarty->assign("other_image_interest",stripslashes($other_image_interest));
			$smarty->assign("other_frequency",stripslashes($other_frequency));
			$smarty->assign("other_circulation",stripslashes($other_circulation));
			$smarty->assign("other_territories",stripslashes($other_territories));
			$smarty->assign("other_website",stripslashes($other_website));
			$smarty->assign("other_company_name",stripslashes($other_company_name));
			$smarty->assign("other_message",stripslashes($other_message));
			
			// Assign Smarty data for required registration information
			$this->smartyAssignRegistrationFields();
			
			// Assign Smarty data for country drop down menu
			$this->smartyAssignCountryData();
			
			// Define html page title
			$smarty->assign("page_title",$GLOBALS['_STR_']['ACCOUNT_088']);
		
			// Output html from template file
			$smarty->pixDisplay('index.account/invitation.accept.tpl');
	
		} else {
		
			$this->invitationError();
			
		}
		
	}

	/*
	*
	*	
	*
	*/
	function invitationError () {	
	
		global $cfg, $objEnvData, $smarty;
		
		// Define html page title
		$smarty->assign("page_title",$GLOBALS['_STR_']['ACCOUNT_085']);
		
		// Output html from template file
		$smarty->pixDisplay('index.account/invitation.error.tpl');
		
		exit;
		
	}

	/*
	*
	*	
	*
	*/
	function confirmCompleteRegistration () {
		
		global $cfg, $objEnvData, $smarty;
		
		$userid 		= $objEnvData->fetchGlobal('userid');
		$submitted_key	= $objEnvData->fetchGlobal('key');
		
		$generated_key	= strtoupper(substr(md5($userid.$cfg['sys']['encryption_key']),0,6));
		
		if ($submitted_key == $generated_key) {
		
			$userid 					= $objEnvData->fetchGlobal('userid');
			$formal_title 				= $objEnvData->fetchGlobal('formal_title');
			$other_title 				= $objEnvData->fetchGlobal('other_title');
			$first_name 				= $objEnvData->fetchGlobal('first_name');
			$middle_initial 			= $objEnvData->fetchGlobal('middle_initial');
			$family_name				= $objEnvData->fetchGlobal('family_name');
			$password_1					= $objEnvData->fetchGlobal('password_1');
			$password_2					= $objEnvData->fetchGlobal('password_2');
			$password_rem_que			= $objEnvData->fetchGlobal('password_rem_que');
			$password_rem_ans			= $objEnvData->fetchGlobal('password_rem_ans');
			$date_registered 			= $objEnvData->fetchGlobal('date_registered');
			$date_edited 				= $objEnvData->fetchGlobal('date_edited');
			$date_expiration 			= $objEnvData->fetchGlobal('date_expiration');
			$account_status 			= $objEnvData->fetchGlobal('account_status');
			$telephone 					= $objEnvData->fetchGlobal('telephone');
			$mobile 					= $objEnvData->fetchGlobal('mobile');
			$fax 						= $objEnvData->fetchGlobal('fax');
			$addr1 						= $objEnvData->fetchGlobal('addr1');
			$addr2 						= $objEnvData->fetchGlobal('addr2');
			$addr3 						= $objEnvData->fetchGlobal('addr3');
			$city 						= $objEnvData->fetchGlobal('city');
			$region 					= $objEnvData->fetchGlobal('region');
			$country 					= $objEnvData->fetchGlobal('country');
			$postal_code 				= $objEnvData->fetchGlobal('postal_code');
			$other_business_type 		= $objEnvData->fetchGlobal('other_business_type');
			$other_business_position 	= $objEnvData->fetchGlobal('other_business_position');
			$other_image_interest 		= $objEnvData->fetchGlobal('other_image_interest');
			$other_frequency 			= $objEnvData->fetchGlobal('other_frequency');
			$other_circulation 			= $objEnvData->fetchGlobal('other_circulation');
			$other_territories 			= $objEnvData->fetchGlobal('other_territories');
			$other_website 				= $objEnvData->fetchGlobal('other_website');
			$other_company_name 		= $objEnvData->fetchGlobal('other_company_name');
			$other_message 				= $objEnvData->fetchGlobal('other_message');
			
			// Load the Pixaria user class
			require_once("class.PixariaUser.php");
			
			// Instantiate the user object
			$objPixariaUser = new PixariaUser($userid);
			
			// Check the user hasn't already activated their account and if they have bounce to the login screen
			if ($objPixariaUser->getPassword() != "") { $this->bounceToLoginScreen(); }
			
			// Otherwise, set user data properties
			$objPixariaUser->setSalutation($formal_title);
			$objPixariaUser->setFirstName($first_name);
			$objPixariaUser->setInitial($middle_initial);
			$objPixariaUser->setFamilyName($family_name);
			$objPixariaUser->setPassword1($password_1);
			$objPixariaUser->setPassword2($password_2);
			$objPixariaUser->setReminderQuestion($password_rem_que);
			$objPixariaUser->setReminderAnswer($password_rem_ans);
			$objPixariaUser->setTelephone($telephone);
			$objPixariaUser->setMobile($mobile);
			$objPixariaUser->setFax($fax);
			$objPixariaUser->setAddress1($addr1);
			$objPixariaUser->setAddress2($addr2);
			$objPixariaUser->setAddress3($addr3);
			$objPixariaUser->setCity($city);
			$objPixariaUser->setRegion($region);
			$objPixariaUser->setCountry($country);
			$objPixariaUser->setPostCode($postal_code);
			$objPixariaUser->setBusinessType($other_business_type);
			$objPixariaUser->setPosition($other_business_position);
			$objPixariaUser->setInterest($other_image_interest);
			$objPixariaUser->setPubFrequency($other_frequency);
			$objPixariaUser->setPubCirculation($other_circulation);
			$objPixariaUser->setTerritories($other_territories);
			$objPixariaUser->setWebsite($other_website);
			$objPixariaUser->setCompanyName($other_company_name);
			$objPixariaUser->setMessage($other_message);
			
			// Edit the user's profile information
			// Apply data validation in mode = 'user' and type = 'create'
			$success = $objPixariaUser->editUser('user','create',true);
			
			// Apply default privileges (photographer user)
			if ($cfg['set']['user_default_priv_photographer']) {
				$objPixariaUser->makePhotographer();
			}

			// Apply default privileges (image editor user)
			if ($cfg['set']['user_default_priv_imgedit']) {
				$objPixariaUser->makeImageEditor();
			}

			// Apply default privileges (image delete user)
			if ($cfg['set']['user_default_priv_imgdelete']) {
				$objPixariaUser->makeImageDelete();
			}

			// Apply default privileges (high res download user)
			if ($cfg['set']['user_default_priv_hires']) {
				$objPixariaUser->makeHiResDownload();
			}
			
			if ($success) { // The new user account is now complete
		
				// Define html page title
				$smarty->assign("page_title",$GLOBALS['_STR_']['ACCOUNT_084']);
			
				// Output html from template file
				$smarty->pixDisplay('index.account/account.created.tpl');
	
			} else { // The validation failed and the user needs to edit some bits
			
				// Assign user variables to smarty
				$smarty->assign("userid",$userid);
				$smarty->assign("key",$submitted_key);
				$smarty->assign("formal_title",stripslashes($formal_title));
				$smarty->assign("first_name",stripslashes($first_name));
				$smarty->assign("middle_initial",stripslashes($middle_initial));
				$smarty->assign("family_name",stripslashes($family_name));
				$smarty->assign("email_address",$objPixariaUser->getEmail());
				$smarty->assign("telephone",stripslashes($telephone));
				$smarty->assign("mobile",stripslashes($mobile));
				$smarty->assign("fax",stripslashes($fax));
				$smarty->assign("addr1",stripslashes($addr1));
				$smarty->assign("addr2",stripslashes($addr2));
				$smarty->assign("addr3",stripslashes($addr3));
				$smarty->assign("city",stripslashes($city));
				$smarty->assign("region",stripslashes($region));
				$smarty->assign("country",stripslashes($country));
				$smarty->assign("postal_code",stripslashes($postal_code));
				$smarty->assign("password_rem_que",stripslashes($password_rem_que));
				$smarty->assign("password_rem_ans",stripslashes($password_rem_ans));
				$smarty->assign("other_business_type",stripslashes($other_business_type));
				$smarty->assign("other_business_position",stripslashes($other_business_position));
				$smarty->assign("other_image_interest",stripslashes($other_image_interest));
				$smarty->assign("other_frequency",stripslashes($other_frequency));
				$smarty->assign("other_circulation",stripslashes($other_circulation));
				$smarty->assign("other_territories",stripslashes($other_territories));
				$smarty->assign("other_website",stripslashes($other_website));
				$smarty->assign("other_company_name",stripslashes($other_company_name));
				$smarty->assign("other_message",stripslashes($other_message));

				$smarty->assign("profile_errors",$objPixariaUser->getProfileErrors());
				$smarty->assign("problem",true);
				
				// Assign Smarty data for required registration information
				$this->smartyAssignRegistrationFields();
				
				// Assign Smarty data for country drop down menu
				$this->smartyAssignCountryData();
				
				// Define html page title
				$smarty->assign("page_title",$GLOBALS['_STR_']['ACCOUNT_088']);
			
				// Output html from template file
				$smarty->pixDisplay('index.account/invitation.accept.tpl');
	
			}
			
			
		} else {
		
			// Define html page title
			$smarty->assign("page_title",$GLOBALS['_STR_']['ACCOUNT_085']);
			
			// Output html from template file
			$smarty->pixDisplay('index.account/invitation.error.tpl');
	
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function resetPasswordStart () {
	
		global $objEnvData, $cfg, $smarty;
	
		// Define html page title
		$smarty->assign("page_title",$GLOBALS['_STR_']['ACCOUNT_089']);
		
		// Output html from template file
		$smarty->pixDisplay('index.account/account.reset.01.tpl');
		
	}
	
	/*
	*
	*	
	*
	*/
	function resetPasswordEnterEmail () {
	
		global $objEnvData, $cfg, $smarty;
	
		// Load user profile from database
		$result = $this->_dbl->sqlSelectRow("SELECT * FROM " . PIX_TABLE_USER . " WHERE email_address = '".mysql_real_escape_string(strtolower($objEnvData->fetchGlobal('email_address')))."'");
		
		// Get the user's password reminder question
		$password_rem_que = $result['password_rem_que'];
		
		if ($password_rem_que == "") {
		
			// Show the e-mail screen again
			$this->resetPasswordStart();
			
		} else {
		
			// Assign password question to smarty
			$smarty->assign("password_rem_que",$password_rem_que);	
			
			// Assign password question to smarty
			$smarty->assign("email_address",strtolower($objEnvData->fetchGlobal('email_address')));	
			
			// Define html page title
			$smarty->assign("page_title",$GLOBALS['_STR_']['ACCOUNT_090']);
			
			// Output html from template file
			$smarty->pixDisplay('index.account/account.reset.02.tpl');
		
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function resetPasswordAnswerQuestion () {
	
		global $objEnvData, $cfg, $smarty;
	
		// Load user profile from database
		$result = $this->_dbl->sqlSelectRow("SELECT * FROM " . PIX_TABLE_USER . " WHERE email_address = '".mysql_real_escape_string(strtolower($objEnvData->fetchGlobal('email_address')))."'");
		
		$password_rem_ans 	= $result['password_rem_ans'];
		$password_rem_que 	= $result['password_rem_que'];
		
		if (strtolower($password_rem_ans) != strtolower($objEnvData->fetchGlobal('password_rem_ans'))) {
		
			// Send error notification to smarty object
			$smarty->assign("output_problem",(bool)true);	
			
			// Assign password question to smarty
			$smarty->assign("password_rem_que",$password_rem_que);	
			
			// Assign password question to smarty
			$smarty->assign("email_address",strtolower($objEnvData->fetchGlobal('email_address')));	
			
			// Define html page title
			$smarty->assign("page_title",$GLOBALS['_STR_']['ACCOUNT_090']);
			
			// Output html from template file
			$smarty->pixDisplay('index.account/account.reset.02.tpl');
		
		} else {
		
			// Assign password question to smarty
			$smarty->assign("email_address",strtolower($objEnvData->fetchGlobal('email_address')));	
			
			// Define html page title
			$smarty->assign("page_title",$GLOBALS['_STR_']['ACCOUNT_091']);
			
			// Output html from template file
			$smarty->pixDisplay('index.account/account.reset.03.tpl');
			
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function resetPasswordNewPassword () {
	
		global $objEnvData, $cfg, $smarty;
	
		$password_1	= $objEnvData->fetchGlobal('password_1');
		$password_2	= $objEnvData->fetchGlobal('password_2');
		
		$problem_status = array();
		
		if ($password_1 !== $password_2) {
		
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['ACCOUNT_092'];
			
		} elseif (!$password_1 || !$password_2) {
		
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['ACCOUNT_093'];
			
		} elseif (!eregi("^([a-zA-Z0-9]{7})([a-zA-Z0-9]+)", $password_1)) {
		
			$problem = "1";
			$problem_status[] = $GLOBALS['_STR_']['ACCOUNT_094'];
			
		}
		
		if ($problem == "1") {
		
			// Send error notification to smarty object
			$smarty->assign("output_problem",(bool)1);	
			$smarty->assign("output_problem_status",$problem_status);		
		
			// Assign password question to smarty
			$smarty->assign("email_address",strtolower($objEnvData->fetchGlobal('email_address')));	
			
			// Define html page title
			$smarty->assign("page_title",$GLOBALS['_STR_']['ACCOUNT_091']);
			
			// Output html from template file
			$smarty->pixDisplay('index.account/account.reset.03.tpl');
			
		} else {
			
			$email_address = $objEnvData->fetchGlobal('email_address');
			
			$sql = "SELECT userid
					
					FROM " . PIX_TABLE_USER . "
					
					WHERE email_address = '".$this->_dbl->escape(strtolower($email_address))."'";
			
			// Load user profile from database
			list ($userid) = $this->_dbl->row($sql);

			// Load the Pixaria user class
			require_once("class.PixariaUser.php");
			
			// Instantiate the user object
			$user = new PixariaUser($userid);
			
			$user->setEmail($objEnvData->fetchGlobal('email_address'));
			$user->setPassword1($password_1);
			$user->setPassword2($password_2);
			
			$user->editUser('user','edit',false);
			
			// Define html page title
			$smarty->assign("page_title",$GLOBALS['_STR_']['ACCOUNT_095']);
			
			// Output html from template file
			$smarty->pixDisplay('index.account/account.reset.04.tpl');
			
		}
	
	}
	
	/*
	*
	*	
	*
	*/
	function smartyAssignRegistrationFields () {
	
		global $objEnvData, $cfg, $smarty;
	
		// Get information about required registration details
		$result = $this->_dbl->sqlSelectRows("SELECT * FROM " . PIX_TABLE_USDA);
		
		if (is_array($result)) {
		
			//	Feed data into Smarty
			for ($i=0; $i<count($result); $i++) {
			
				$name = $result[$i]['name'];
				
				$smarty->assign("data_".$name,$result[$i]['value']);
			
			}
		
		}
		
	}
	
	/*
	*
	*	
	*
	*/
	function smartyAssignCountryData () {
	
		global $objEnvData, $cfg, $smarty;
	
		// Load the country class
		require_once ("class.PixariaCountry.php");
		
		// Initialise the country class
		$objCountry = new PixariaCountry();
		
		// Assign country data to Smarty
		$smarty->assign("iso_codes",$objCountry->getIsoCodes());
		$smarty->assign("printable_names",$objCountry->getPrintableNames());
		
	}
	
	/*
	*
	*	
	*
	*/
	function bounceToLoginScreen () {
		
		global $cfg;
		
		header ("Location: " . $cfg['sys']['base_url'] . $cfg['fil']['index_login']); exit; 

	}
	
}

?>