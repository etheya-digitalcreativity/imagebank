<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/
// Set up constant for front end activities
define("WORKSPACE","FE");

// Set the include path for files used in this script
ini_set("include_path","resources/includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Load Markdown library
require_once ("functions.Markdown.php");
		
// Send HTTP header and don't cache
pix_http_headers("rss","");

setlocale(LC_ALL, 'en_US');

// Initialise the smarty object
$smarty = new Smarty_Pixaria();

$sql = "SELECT *, CONCAT(".$cfg['sys']['table_user'].".first_name,' ',".$cfg['sys']['table_user'].".family_name) AS blog_name
									
		FROM ".$cfg['sys']['table_blog']." 
									
		LEFT JOIN ".$cfg['sys']['table_user']." ON ".$cfg['sys']['table_blog'].".blog_author = ".$cfg['sys']['table_user'].".userid
		
		ORDER BY blog_date DESC";

$blog_info = sql_select_rows($sql);

if (is_array($blog_info) && count($blog_info) > 0) {

	foreach($blog_info as $value) {
		
		$blog_item_id[]		= $value['blog_item_id'];
		$blog_title[]		= stripslashes($value['blog_title']);
		$blog_content[]		= markdown(stripslashes($value['blog_content']));
		$blog_date[]		= $value['blog_date'];
		$blog_date_unx[]	= strtotime($value['blog_date']);
		$blog_gallery_id[]	= $value['blog_gallery_id'];
		$blog_author[]		= stripslashes($value['blog_name']);

		if ($cfg['set']['func_mod_rewrite']) {
			$blog_link[] = $cfg['sys']['base_url'] . "news/" . $value['blog_item_id'];
		} else {
			$blog_link[] = $cfg['sys']['base_url'] . $cfg['fil']['index_news'] . "?id=" . $value['blog_item_id'];
		}
		
			
		$gallery_id = $value['blog_gallery_id'];
		
		$sql = "SELECT ".$cfg['sys']['table_imgs'].".*
				
				FROM ".$cfg['sys']['table_gall']."
				
				LEFT JOIN ".$cfg['sys']['table_imgs']." ON ".$cfg['sys']['table_gall'].".gallery_key = ".$cfg['sys']['table_imgs'].".image_id
				
				WHERE ".$cfg['sys']['table_gall'].".gallery_id = '$gallery_id'";
				
		$result = sql_select_row($sql);
		
		$thumb_file_path = $cfg['sys']['base_library'].$result[image_path]."/80x80/".$result[image_filename];
			
		if (file_exists($thumb_file_path)) {
		
			$thumb_path[]	= base64_encode($thumb_file_path);
			$thumb_size 	= getimagesize($thumb_file_path);
			$thumb_dims[]	= $thumb_size[3];					
							
		} else {
		
			$thumb_file_path = SYS_BASE_PATH."resources/themes/".$cfg['set']['theme']."/images/spacer.gif";
		
			$thumb_path[]	= base64_encode($thumb_file_path);
			$thumb_dims[]	= "width=\"80\" height=\"80\"";					
							
		}
				
	}

}
		
// Assign blog items to Smarty
$smarty->assign("blog_item_id",$blog_item_id);
$smarty->assign("blog_title",$blog_title);
$smarty->assign("blog_content",$blog_content);
$smarty->assign("blog_date",$blog_date);
$smarty->assign("blog_date_unx",$blog_date_unx);
$smarty->assign("blog_gallery_id",$blog_gallery_id);
$smarty->assign("blog_author",$blog_author);
$smarty->assign("blog_link",$blog_link);
$smarty->assign("blog_thumb",$thumb_path);
$smarty->assign("blog_thumb_dims",$thumb_dims);

$smarty->pixDisplay('index.rss/rss.v2.0.tpl');

?>