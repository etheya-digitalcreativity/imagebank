<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set up constant for front end activities
define("WORKSPACE","FE");

// Set the include path for files used in this script
ini_set("include_path","resources/includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Load teh payment data log class
require_once ('class.PixariaPaymentLog.php');

// Initialise the class and put environment data into it
$objDataLog = new PixariaPaymentLog();
$objDataLog->setDataVars();
$objDataLog->createEntry();

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// Get the current time
$time = mktime();

// Set a default response result
$result = 'default';

// Get the PayPal server response
$response = pix_paypal_respond();

// Set system contact vars for script
$toname		= $cfg['set']['contact_name'];
$toemail	= $cfg['set']['contact_email'];
$fromname	= $cfg['set']['contact_name'];
$fromemail	= $cfg['set']['contact_email'];

// Set system contact vars in Smarty
$smarty->assign("toname",$toname);
$smarty->assign("toemail",$toemail);
$smarty->assign("fromname",$fromname);
$smarty->assign("fromemail",$fromemail);

$message	 = "\nPAYPAL ALERT MESSAGE\n\n";
$message	.= $GLOBALS['_STR_']['PAYPAL_01'].": ".apost("receiver_email")."\n";
$message	.= $GLOBALS['_STR_']['PAYPAL_02'].": ".apost("first_name")." ".apost("last_name")."\n";
$message	.= $GLOBALS['_STR_']['PAYPAL_03'].": ".apost("payer_email")."\n";
$message	.= $GLOBALS['_STR_']['PAYPAL_04'].": ".apost("item_name")."\n";
$message	.= $GLOBALS['_STR_']['PAYPAL_05'].": ".apost("item_number")."\n";
$message	.= $GLOBALS['_STR_']['PAYPAL_06'].": ".apost("payment_status")."\n";

if (apost("pending_reason") !== '') { $message	.= $GLOBALS['_STR_']['PAYPAL_07'].": ".apost("pending_reason")."\n"; }

$message	.= $GLOBALS['_STR_']['PAYPAL_08'].": ".apost("payment_date")."\n";

//	Check if there is a shopping cart for this userid
//	if the cart doesn't exists or is empty send a message
//	to the administrator explaining what's happened.

switch ($response) { // Case statement switched on the transaction response from PayPal ( VERIFIED / INVALID / ERROR )
	
	case 'VERIFIED': // PayPal has approved and verified this transaction
		
		switch(apost('payment_status')) { // Switch on the payment_status returned by PayPal
	
			case 'Completed': // The cach has been put into the receipient PayPal account
				
				// Count the number entries with this remote_txn_id
				$count 	= sql_count("SELECT count(remote_txn_id) FROM ".$cfg['sys']['table_cart']." WHERE remote_txn_id='".apost("txn_id")."'");
				
				if ($count >= 1) { // There is already a transaction_id for this item
					
					$result = "DUPLICATE TXN_ID";
					$message .= "\ntxn_id: ".apost("txn_id");
					pix_send_email($toemail,$toname,$message,$fromemail,$fromname,$result);
				
					break;
				
				} else { // Go ahead and update the database with this new transaction
					
					$remote_txn_id		= addslashes(apost('txn_id'));
					$transaction_info	= addslashes(urlencode(serialize($_POST)));
					$cart_id			= apost('invoice');
					
					$sql = "UPDATE ".$cfg['sys']['table_cart']."
							
							SET remote_txn_id 		= '$remote_txn_id',
								transaction_info 	= '$transaction_info',
								paid 				= '1',
								method				= '10',
								status 				= '4',
								date_processed		= NOW(),
								date_completed		= NOW()
								
							WHERE cart_id = '$cart_id'";
							
					@mysql_query($sql);
				
					// Set the value of the user making the purchase
					$userid = apost("custom");
					
					// Set the name of the user who's bought the images
					$smarty->assign("name",$ses['psg_name']);
					
					// Load e-mail message templates
					$message = $smarty->fetch("email.templates/buy.notify.tpl");
							
					//	The whole thing worked correctly, send the user a response
					pix_send_email(apost("payer_email"),$name,$message,$fromemail,$fromname,"Your purchase");
									
				}

			break;

			case 'Pending':
			
				$result = apost("payment_status");
				pix_send_email($toemail,$toname,$message,$fromemail,$fromname,$result);
				
			break;
				
			case 'Refunded':
			
				$result = apost("payment_status");
				pix_send_email($toemail,$toname,$message,$fromemail,$fromname,$result);
				
			break;
				
			default:
			
				$result = apost("payment_status");
				report("paypal.php", apost("payment_status"));
				pix_send_email($toemail,$toname,$message,$fromemail,$fromname,$result);
				
			break;
				
		}
		
	break;
	
	
	case 'INVALID':
	
		$result = $response;
		pix_send_email($toemail,$toname,$message,$fromemail,$fromname,$result);
	
	break;
		
		
	case 'ERROR':
	
		$result = $response;
		pix_send_email($toemail,$toname,$message,$fromemail,$fromname,$result);
	
	break;
		
		
	default:
	
		$result = 'DEFAULT';
		pix_send_email($toemail,$toname,$message,$fromemail,$fromname,$result);
	
	break;
}

/*
*
*	This function sends a PayPal IPN message to PayPal
*
*/

function pix_paypal_respond() {

	global $cfg;
	global $_POST;
	global $GLOBAL;

	$response=''; 
	
	$host = 'www.paypal.com';

	$script = '/cgi-bin/webscr';

	$output = 'cmd=_notify-validate';

	foreach ($_POST as $key => $value) { $output .= "&$key=".urlencode(stripslashes($value)); }

	$header  = "POST $script HTTP/1.0\r\n";
	$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
	$header .= "Content-Length: ".strlen($output)."\r\n\r\n";

	if ($GLOBAL["ssl_enable"] == '1') {

		if (!$fp = fsockopen("ssl://".$host, 443, $errno, $errstr, 30)) {

			@fclose($fp); 
		
			return ('SSL Connect Failure');
			
		}

	} else {
	
		if (!$fp = fsockopen($host, 80, $errno, $errstr, 30)) {
		
			@fclose($fp); 
		
			return ('PayPal Connect Failure');
			
		}
		
	}

	@fputs($fp, $header.$output);
	
	while (!feof($fp)) { $response .= fgets($fp, 1024); }

	@fclose($fp);

	if (ereg("VERIFIED", $response)) { return("VERIFIED"); }

	if (ereg("INVALID", $response)) { return("INVALID"); }

	return("ERROR");

}


?>