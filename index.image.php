<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set up constant for front end activities
define("WORKSPACE","FE");

// Set the include path for files used in this script
ini_set("include_path","resources/includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// 	Go to login page if the default action for logged out users 
//	is to require logging in or registering before viewing content
pix_login_redirect();

// Send HTTP header and don't cache
pix_http_headers("html","");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

$image_id = $objEnvData->fetchGlobal('image_id');

// Load the PixariaImage class 
require_once ('class.PixariaImage.php');

// Create the image object
$objPixariaImage = new PixariaImage($image_id);

// Increment the image view counter
$objPixariaImage->updateImageViewCounter();

// Add detailed information about this image to the array
$smarty->assign("image",$objPixariaImage->getImageDataComplete());

// Tell Smarty the page title
$smarty->assign("page_title","View image");

// Tell Smarty we're viewing in single image mode
$smarty->assign("view_mode","image");

switch ($objEnvData->fetchGet('cmd')) {

	case pop:
	
		// Load the standard image view page
		$smarty->pixDisplay('index.image/image.popup.tpl');
	
	break;
	
	default:
		
		// Load the standard image view page
		$smarty->pixDisplay('index.image/image.standard.tpl');
	
	break;

}
	
?>