<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set up constant for front end activities
define("WORKSPACE","FE");

// Set the include path for files used in this script
ini_set("include_path","resources/includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Send HTTP header and don't cache
pix_http_headers("html","");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

$objIndexContact = new IndexContact();

class IndexContact {
	
	var $_dbl;
	
	/*
	*
	*	Class constructor
	*
	*/
	function IndexContact () {
		
		global $cfg, $smarty, $objEnvData, $ses;
		
		// if the contact form is turned off, redirect to the homepage
		if (!$cfg['set']['func_contact']) { header("Location: " . $cfg['sys']['base_url']); }
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		switch ($objEnvData->fetchGlobal('cmd')) {
						
			case "sendMessage":
				$this->sendMessage();
			break;
		
			default:
				$this->showFormContact();
			break;
		
		}
	
	}
	
	/*
	*
	*	Display the contact form to the user
	*
	*/
	function showFormContact () {
		
		global $smarty, $cfg, $ses;
				
		// Generate a captcha key
		$smarty->assign("captcha_key",captchaKey());
		
		// Define html page title
		$smarty->assign("page_title",$GLOBALS['_STR_']['CTCT_FRM_TITLE_01']);
		
		// Output html from template file
		$smarty->pixDisplay('index.contact/contact.form.tpl');

	}
	
	/*
	*
	*	Send the message to the site administrator
	*
	*/
	function sendMessage () {
		
		// Import globals
		global $smarty, $cfg, $objEnvData;
		
		// Load form data
		$name 			= $objEnvData->fetchPost('name');
		$email 			= $objEnvData->fetchPost('email');
		$telephone 		= $objEnvData->fetchPost('telephone');
		$message 		= $objEnvData->fetchPost('message');
		
		
		/*
		*	Validate captcha (if it's being used
		*/
		$captcha_code 		= strtolower($objEnvData->fetchGlobal('captcha_code'));
		$captcha_key		= $objEnvData->fetchGlobal('captcha_key');
		list($captcha_db) 	= $this->_dbl->sqlSelectRow("SELECT `code` FROM ".PIX_TABLE_CAPT." WHERE `key` = '".mysql_real_escape_string($captcha_key)."'");
		if ((strtolower($captcha_db) !== $captcha_code) && $cfg['set']['func_captcha_contact']) { $captcha_failure = true; $smarty->assign("captcha_failure",$captcha_failure); }
		


		// If the information submitted is valid
		if ($this->validateEmailAddress($email) && $name != "" && $message != "" && !$captcha_failure) {
			
			// Substitute text if no telephone number supplied
			if ($telephone == "") { $telephone = "No telephone number was given"; }
			
			// Format the message
			$message_data  = $GLOBALS['_STR_']['CTCT_FRM_RESPONSE_01'].":      $name\n";
			$message_data .= $GLOBALS['_STR_']['CTCT_FRM_RESPONSE_02'].":    $email\n";
			$message_data .= $GLOBALS['_STR_']['CTCT_FRM_RESPONSE_03'].": $telephone\n";
			$message_data .= "\n".$GLOBALS['_STR_']['CTCT_FRM_RESPONSE_04'].":\n\n";
			$message_data .= $message;
			
			// Send the e-mail
			sendEmail($cfg['set']['contact_email'], $cfg['set']['contact_email'], $message_data, $email, $name, $GLOBALS['_STR_']['CTCT_FRM_RESPONSE_SUBJECT']);
			
			// Define html page title
			$smarty->assign("page_title",$GLOBALS['_STR_']['CTCT_FRM_TITLE_02']);
			
			// Output html from template file
			$smarty->pixDisplay('index.contact/contact.sent.tpl');
		
		} else { // If there are problems with the form data
			
			// Validate the e-mail address
			if (!$this->validateEmailAddress($email)) {
				$errors[] = $GLOBALS['_STR_']['CTCT_FRM_ERROR_01'];
			} else {
				$smarty->assign("email",$email);
			}
			
			// Validate the user's name
			if ($name == "") {
				$errors[] = $GLOBALS['_STR_']['CTCT_FRM_ERROR_02'];
			}
			
			// Validate the message
			if ($message == "") {
				$errors[] = $GLOBALS['_STR_']['CTCT_FRM_ERROR_03'];
			}
			
			// Send errors to Smarty
			$smarty->assign("errors",$errors);
			
			// Tell Smarty there were errors
			$smarty->assign("problem",(bool)true);
			
			// Pass original data back to Smarty
			$smarty->assign("message",$message);
			$smarty->assign("name",$name);
			$smarty->assign("telephone",$telephone);
			
			// Generate a captcha key
			$smarty->assign("captcha_key",captchaKey());
		
			// Define html page title
			$smarty->assign("page_title",$GLOBALS['_STR_']['CTCT_FRM_TITLE_01']);
			
			// Output html from template file
			$smarty->pixDisplay('index.contact/contact.form.tpl');

		}
		
	}
	
	/*
	*
	*	Validate a submitted e-mail address
	*
	*/
	function validateEmailAddress($email) {
	
		if (eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{1,4}",$email)) {
		
			return TRUE;
		
		} else {
		
			return FALSE;
			
		}
	
	}
	
}

?>