<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

/*	Do not edit these lines */
$cfg 	= array();
$lang 	= array();

/*	The main url of your Pixaria Gallery website (must end in a trailing slash "/") */
$cfg['sys']['base_url'] 		= "http://www.imagebank.etheya.com/";

/*	The hostname of your MySQL server */
$cfg['sys']['db_host'] 			= "mysql.imagebank.etheya.com";


/*	The user name you use to login to MySQL */
$cfg['sys']['db_uname'] 		= "flashwiz";


/*	The password for your MySQL login account */
$cfg['sys']['db_pword'] 		= "idas2000";


/*	The name of the database your Pixaria Gallery data is stored in. */
/*	If it doesn't already exist, you'll have to create it. */
$cfg['sys']['db_name'] 			= "mysql_pixaria_imagebank";


/*	
	Enter some text here to use as an encryption key for your website
	This can be any text you want, a memorable phrase or a phone number
	as long as no one else is likely to guess what it is.
*/
$cfg['sys']['encryption_key'] 	= "ethanfreya07";


/*
*	YOU SHOULD NOT NEED TO CHANGE THESE SETTINGS TO MAKE THE SOFTWARE WORK!
*	
*	If the default value of the following variable doesn't work, replace everything
*	after the '=' sign with the full system path to your base Pixaria Gallery
*	directory in quotes as demonstrated the example below: 
*	
*	$cfg['sys']['base_path'] 	= "/this/is/this/full/path/to/pixaria/";
*/

$cfg['sys']['base_path']		= dirname(__FILE__)."/";

/*	Check that the path defined as the system base path exists */
if (!is_dir($cfg['sys']['base_path']."/")) {
	print("Base path variable is not configured correctly in 'pixaria.config.php'!<br /><br />Please refer to the instructions in 'pixaria.Config.php' for help.");
	exit;
}

/*	Connect to the MySQL database */
if (!$this_db = mysql_connect($cfg['sys']['db_host'], $cfg['sys']['db_uname'], $cfg['sys']['db_pword'])) {
	print "Could not connect to database server: The MySQL server address, username or password may be incorrect!";
	exit;
}

/*	Select the database to work with */
if (!mysql_select_db($cfg['sys']['db_name'],$this_db)) {
	print "Could not open the Pixaria database: The required database for this software doesn't exist on the specified server or has not been set up correctly.";
	exit; 
	
}

/*	Select the database to work with */
if ($cfg['sys']['encryption_key'] == "738AD183CD16C063F90AD686BA7E7EB9") {
	print "Warning: You have not changed the default encryption key in the file 'pixaria.config.php'.";
	exit; 
	
}

/*	Select the database to work with */
if (!is_writable($cfg['sys']['base_path'] . "resources/smarty/templates_c/")) {
	print "Warning: The directory -/resources/smarty/templates_c/ must be writable by PHP to continue.  See the documentation for Pixaria for further assistance.";
	exit; 
	
}

?>