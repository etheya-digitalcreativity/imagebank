<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set up constant for front end activities
define("WORKSPACE","FE");

// Set the include path for files used in this script
ini_set("include_path","resources/includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Send HTTP header and don't cache
pix_http_headers("html","");

// INITIALISE THE SMARTY OBJECT
$smarty = new Smarty_Pixaria;
		
$objProcess2Checkout = new Process2Checkout ();

/*
*
*	Class for processing receipt data from 2Checkout
*
*/
class Process2Checkout {

	/*
	*
	*	Class constructor
	*
	*/
	function Process2Checkout () {
		
		global $objEnvData, $smarty, $cfg, $lang, $ses;
		
		// Load the database class
		require_once ('class.Database.php');
		
		// Create the database object
		$this->_dbl = new Database();
		
		// Load the payment data log class
		require_once ('class.PixariaPaymentLog.php');
		
		// Initialise the class and put environment data into it
		$objDataLog = new PixariaPaymentLog();
		$objDataLog->setDataVars();
		$objDataLog->createEntry();
		
		// Get the current Unix timestamp
		$time = time();
		
		// Defualt value for hash check
		$hash_check = (bool)FALSE;
		
		$demo					= $objEnvData->fetchGlobal('demo');
		$key					= $objEnvData->fetchGlobal('key');
		$order_number			= $objEnvData->fetchGlobal('order_number');
		$card_holder_name		= $objEnvData->fetchGlobal('card_holder_name');
		$street_address			= $objEnvData->fetchGlobal('street_address');
		$city					= $objEnvData->fetchGlobal('city');
		$state					= $objEnvData->fetchGlobal('state');
		$zip					= $objEnvData->fetchGlobal('zip');
		$country				= $objEnvData->fetchGlobal('country');
		$email					= $objEnvData->fetchGlobal('email');
		$userid					= $objEnvData->fetchGlobal('userid');
		$phone					= $objEnvData->fetchGlobal('phone');
		$cart_order_id			= $objEnvData->fetchGlobal('cart_order_id');
		$cart_id				= $objEnvData->fetchGlobal('cart_id');
		$credit_card_processed	= $objEnvData->fetchGlobal('credit_card_processed');
		$total					= $objEnvData->fetchGlobal('total');
		$ship_name				= $objEnvData->fetchGlobal('ship_name');
		$ship_street_address	= $objEnvData->fetchGlobal('ship_street_address');
		$ship_city				= $objEnvData->fetchGlobal('ship_city');
		$ship_state				= $objEnvData->fetchGlobal('ship_state');
		$ship_zip				= $objEnvData->fetchGlobal('ship_zip');
		$ship_country			= $objEnvData->fetchGlobal('ship_country');
		$shipping_method		= $objEnvData->fetchGlobal('shipping_method');
		
		// Assign variables to Smarty
		$smarty->assign("payment_method","2Checkout");
		$smarty->assign("transaction_id",$order_number);
		$smarty->assign("cart_id",$cart_id);
		$smarty->assign("userid",$userid);
		$smarty->assign("user_name",$ses['psg_name']);
		$smarty->assign("transaction_time",date("l dS \of F Y h:i:s A"));

		if ($demo == "Y") {
			$hash_string = $cfg['set']['store_2co_secret'].$cfg['set']['store_2co_sid']."1".$total;
		} else {
			$hash_string = $cfg['set']['store_2co_secret'].$cfg['set']['store_2co_sid'].$order_number.$total;
		}
		
		$string_hash = strtoupper(md5($hash_string));
	
		if ($string_hash === $key) { $hash_check = (bool)TRUE; }
		
		// The transaction was correctly verified
		if ($order_number != "" && $hash_check) {
			
			if ($credit_card_processed == "Y") {
			
				// Items have been purchased and paid for
				$this->updateCartProcessed();
					
			} else {
				
				// The items have been purchased but not yet paid for
				$this->transactionPartial();
			}
			
		} else {
		
			// An error has occured - failed hash check (possible fraud) or no order number
			$this->transactionError();
			
		}
	
	}
	
	/*
	*
	*	Update the cart with the new processed information
	*
	*/
	function updateCartProcessed () {
	
		global $objEnvData, $smarty, $cfg, $lang, $ses;
		
		// Localise variables
		$order_number 	= $objEnvData->fetchGlobal('order_number');
		$cart_id 		= $objEnvData->fetchGlobal('cart_id');
		$userid 		= $objEnvData->fetchGlobal('userid');
		$email	 		= $objEnvData->fetchGlobal('email');
		
		// Count the number entries with this remote_txn_id
		$count 	= $this->_dbl->sqlCountRows("SELECT count(remote_txn_id) FROM ".PIX_TABLE_CART." WHERE remote_txn_id = '$order_number'");
		
		if ($count >= 1) { // There is already a transaction_id for this item
			
			$subject = "Duplicate 2Checkout Transaction";
			$message = $smarty->pixFetch('email.templates/buy.notify.duplicate.tpl');
			
			$this->sendEmail($cfg['set']['contact_email'],$cfg['set']['contact_name'],$subject,$message);
		
			// An error has occured - duplicate transaction
			$this->transactionDuplicate();
		
		} else { // Go ahead and update the database with this new transaction
			
			$subject = "New 2Checkout Transaction";
			$message = $smarty->pixFetch('email.templates/buy.notify.administrator.tpl');
			
			$this->sendEmail($cfg['set']['contact_email'],$cfg['set']['contact_name'],$subject,$message);
		
			$transaction_info	= urlencode(serialize($_POST));
			
			$sql = "UPDATE ".PIX_TABLE_CART."
					
					SET remote_txn_id 		= '$order_number',
						transaction_info 	= '$transaction_info',
						paid 				= '1',
						method				= '15',
						status 				= '4',
						date_processed		= NOW(),
						date_completed		= NOW()
						
					WHERE cart_id = '$cart_id'";
					
			$this->_dbl->sqlQuery($sql);
		
			// Set the name of the user who's bought the images
			$smarty->assign("name",$ses['psg_name']);
			
			// Load e-mail message templates
			$message = $smarty->fetch("email.templates/buy.notify.tpl");
					
			//	The whole thing worked correctly, send the user a response
			$this->sendEmail($email,$name,"Your ".$cfg['set']['site_name']." purchase",$message);
			
			// Set the page title
			$smarty->assign("page_title","Your transaction is complete");
		
			// Output HTML
			$smarty->pixDisplay('index.store/cart.processing.success.tpl');
		
			exit;
			
		}
	
	}
	
	/*
	*
	*	The transaction has been processed but still awaits payment
	*
	*/
	function transactionPartial () {
	
		global $objEnvData, $smarty, $cfg, $lang, $ses;
		
		$subject = "New 2Checkout Transaction (partial)";
		$message = $smarty->pixFetch('email.templates/buy.notify.administrator.tpl');
		
		$this->sendEmail($cfg['set']['contact_email'],$cfg['set']['contact_name'],$subject,$message);
		
		$transaction_info	= urlencode(serialize($_POST));
		
		$sql = "UPDATE ".PIX_TABLE_CART."
				
				SET remote_txn_id 		= '$order_number',
					transaction_info 	= '$transaction_info',
					status 				= '3'
					
				WHERE cart_id = '$cart_id'";
				
		$this->_dbl->sqlQuery($sql);
	
		// Set the page title
		$smarty->assign("page_title","Transaction partially processed");
		
		// Output HTML
		$smarty->pixDisplay('index.store/cart.processing.partial.tpl');
		
		exit;
	
	}
	
	/*
	*
	*	There was an error (no order_id or failed key check)
	*
	*/
	function transactionError () {
	
		global $objEnvData, $smarty, $cfg, $lang, $ses;
		
		// Set the page title
		$smarty->assign("page_title","Transaction not processed");
		
		// Output HTML
		$smarty->pixDisplay('index.store/cart.processing.error.tpl');
		
		exit;
	
	}
	
	/*
	*
	*	Duplicate transaction (possible page refresh/double click on previous page)
	*
	*/
	function transactionDuplicate () {
	
		global $objEnvData, $smarty, $cfg, $lang, $ses;
		
		// Set the page title
		$smarty->assign("page_title","An error has occurred");
		
		// Output HTML
		$smarty->pixDisplay('index.store/cart.processing.duplicate.tpl');
		
		exit;
		
	}
	
	/*
	*
	*	Send an e-mail
	*
	*/
	function sendEmail ($email, $name, $subject, $message) {
	
		global $objEnvData, $smarty, $cfg, $lang, $ses;
		
		$toname		= $cfg['set']['contact_name'];
		$toemail	= $cfg['set']['contact_email'];
		$fromname	= $cfg['set']['contact_name'];
		$fromemail	= $cfg['set']['contact_email'];
		
		// Send an e-mail
		sendEmail($toemail,$toname,$message,$fromemail,$fromname,$subject);
	
	}
	
}




?>