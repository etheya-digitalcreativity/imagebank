<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set up constant for front end activities
define("WORKSPACE","FE");

// Set the include path for files used in this script
ini_set("include_path","resources/includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Send HTTP header and don't cache
pix_http_headers("html","");

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

// 	Go to login page if the default action for logged out users 
//	is to require logging in or registering before viewing content
pix_login_redirect();

// Set the page title
$smarty->assign("page_title","Welcome to " . $cfg['set']['site_name']);

// Display the HTML output
$smarty->pixDisplay('index.tpl');

?>