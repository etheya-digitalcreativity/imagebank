<?php 

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// 	Set error reporting to minor errors only
error_reporting(E_ERROR | E_WARNING | E_PARSE);

// Display errors = off
ini_set("display_errors","on");

$__app_error = array();

$resources_folder 		= "resources";
$controllers_folder 	= "controllers";
$views_folder 			= "views";
$application_folder 	= "application";
$models_folder 			= "models";

/*
*
* SET THE SERVER PATH
*
* Let's attempt to determine the full-server path to the "system"
* folder in order to reduce the possibility of path problems.
* Note: We only attempt this if the user hasn't specified a 
* full server path.
*
*/
if (function_exists('realpath') AND @realpath(dirname(__FILE__)) !== FALSE) {

	$base_folder = realpath(dirname(__FILE__)).'/';
	
}


/*
*
* SET TIMEZONE HANDLING TO EUROPE/LONDON
*
*/
if (function_exists('date_default_timezone_set')) { date_default_timezone_set('Europe/London'); }


/*
*
* DEFINE APPLICATION CONSTANTS
*
* EXT			- The file extension.  Typically ".php"
* FCPATH		- The full server path to THIS file
* SELF			- The name of THIS file (typically "index.php")
* MODEL_PATH	- The full server path to the "models" directory
* BASE_PATH		- The full server path to the base folder
* SYS_BASE_PATH - The full server path to the base folder
* CTRL_PATH		- The full server path to the "controllers" folder
* VIEW_PATH		- The full server path to the "views" folder
*
*/
define('EXT', '.'.pathinfo(__FILE__, PATHINFO_EXTENSION));
define('FCPATH', __FILE__);
define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));
define('BASE_PATH', $base_folder);
define('SYS_BASE_PATH', $base_folder);
define('RES_PATH', BASE_PATH.$resources_folder.'/');
define('MODEL_PATH', RES_PATH.$models_folder.'/');
define('CTRL_PATH', RES_PATH.$controllers_folder.'/');
define('VIEW_PATH', RES_PATH.$views_folder.'/');
define('APP_PATH', RES_PATH.$application_folder.'/');
define('APP_NAME','Pixaria Gallery');
define('PIXGALL','ON');


/*
*
* SET THE DEFAULT CONTROLLER NAME TO USE
*
*/
define('DEFAULT_CONTROLLER','welcome');

$app = url_decode();

switch ($app['controller']) {

	case 'gallery':
	
		if (eregi("^[A-Za-z0-9_\-][A-Za-z0-9_\-]*$",$app['method']) && $app['params_1'] == "") {
			
			$_GET['gid'] = $app['method'];
		
		} elseif ($app['params_1'] == "pages" && is_numeric($app['params_2'])) {
			
			$_GET['gid'] = $app['method'];
			$_GET['ipg'] = $app['params_2'];
			
		} elseif ($app['params_1'] == "page" && is_numeric($app['params_2'])) {
			
			$_GET['gid'] = $app['method'];
			$_GET['gpg'] = $app['params_2'];
			
		} elseif ($app['params_1'] == "image" && is_numeric($app['params_2'])) {
			
			$_GET['gid'] = $app['method'];
			$_GET['img'] = $app['params_2'];
			
		}
		
		require_once("index.gallery.php");
		exit;
	
	break;
	
	case 'news':
	
		if (is_numeric($app['method'])) {
			$_GET['id'] = $app['method'];
		}
		
		require_once("index.news.php");
		exit;
	
	break;
	
	case 'image':
		
		if ($app['method'] == 'detail') {
			
			$_GET['image_id'] 			= $app['params_1'];
			
			require_once("index.image.php");
			exit;
			
		} elseif (is_numeric($app['method'])) {
		
			$_GET['image_id'] 			= $app['method'];
			$_GET['image_hash'] 		= $app['params_1'];
			$_GET['image_file_name'] 	= $app['params_2'];
			
			require_once("pixaria.image.php");
			exit;
	
		}
		
	break;
	
	case 'keyword':
	
		if (trim($app['method']) != "") {
			$_POST['keywords_simple_boolean'] 	= $app['method'];
			$_POST['cmd'] 						= "doSearch";
			$_POST['new'] 						= "1";
		}
	
		require_once("index.search.php");
		exit;
	
	break;
	
	case 'index.php': case '':
		
		require_once("index.welcome.php");
		exit;
		
	break;
	
	default:
	
		/*
		*
		* LOAD THE MAIN APPLICATION
		*
		*/
		require_once APP_PATH.'framework'.EXT;
		
		exit;
		
	break;

}

/*
*
*	APPLICATION CONTROLLER - RUNTIME CONFIGURATION - PIXARIA VERS BELOW 3.0
*	
*	This function determines what controller and method to execute based on
*	data supplied in the request URI, either as directory paths from a search
*	engine friendly type URL or by supplying controller, method and parameter
*	settings in the HTTP 'get' query string.
*
*	Example request with commands in directory path:
*	
*		http://mysite.com/controller/method/params1/params2/
*
*	Example HTTP get request with commands in query string:
*	
*		http://mysite.com/?c=controller&m=method&p1=params1&p2=params2
*
*/

function url_decode () {
	
	// Get the document URI and trim off leading and trailing slashes
	$request_uri	= trim($_SERVER['REQUEST_URI'],'/');
	
	// If we're in a subdirectory, then get this from the script name
	$path_prefix 	= trim(str_replace("/index.php","",$_SERVER['SCRIPT_NAME']),"/");
	
	// If we're in a subdirectory, then remove this from the request URI 
	if ($path_prefix != "") { $request_uri = preg_replace('|^'.preg_quote($path_prefix).'|',"",$request_uri); }
	
	// Separate the request URI directory path from any query string
	list ($request_uri, $query_string) = explode('?',$request_uri);
	
	// Convert the request URI into an array of directory names
	$uri_parts = explode("/",trim($request_uri,'/'));
	
	// If there is a request URI then assign controller, method and parameters from this
	if ($request_uri == '') {
	
		$uri_parts = explode("/",trim($_GET['uri'],'/'));
		
	} 
	
	$app['controller'] 	= $uri_parts[0];	
	$app['method'] 		= $uri_parts[1];	
	$app['params_1']	= $uri_parts[2];	
	$app['params_2'] 	= $uri_parts[3];	
	$app['params_3'] 	= $uri_parts[4];	
	$app['params_4'] 	= $uri_parts[5];	
	$app['params_5'] 	= $uri_parts[6];	
	$app['params_6'] 	= $uri_parts[7];	
	$app['params_7'] 	= $uri_parts[8];	
	
	// Return the application array
	return $app;
	
}

?>