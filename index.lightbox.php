<?php

/*
*
*	Pixaria Gallery
*	Copyright Jamie Longstaff
*
*/

// Set up constant for front end activities
define("WORKSPACE","FE");

// Set the include path for files used in this script
ini_set("include_path","resources/includes/");

// Load in the Pixaria settings and includes
include ("pixaria.Initialise.php");

// Send HTTP header and don't cache
pix_http_headers("html","");

// If there is no userid set, then create a temporary profile
pixCreateTemporaryUserId();

// Initialise the smarty object
$smarty = new Smarty_Pixaria;

$objIndexLightbox = new IndexLightbox();

class IndexLightbox {
	
	var $objLightbox			= "";
	var $thumbnail_page 		= "";
	var $lightbox_image 		= "";
	var $lightbox_id 			= "";
	var $lightbox_userid 		= "";
	var $lightbox_status 		= "";
	var $lightbox_type 			= "";
	var $lightbox_image_list 	= "";
	var $lightbox_name 			= "";
	var $ipages 				= "";
	var $submitted_access_key 	= "";
	var $referer 				= "";
	
	function IndexLightbox () {
		
		global $objEnvData, $cfg, $ses;
		
		$this->thumbnail_page		= $objEnvData->fetchGlobal('ipg'); // The image thumbnail icon page we are currently on
		$this->lightbox_image		= $objEnvData->fetchGlobal('img'); // The number of a specific image we want to view in the lightbox
		
		$this->submitted_access_key	= $objEnvData->fetchGlobal('key'); // The access key given by the user
		
		if (is_numeric($objEnvData->fetchGlobal('lid'))) {
		
			$this->lightbox_id = $objEnvData->fetchGlobal('lid'); // The number of a specific image we want to view in the lightbox
		
		} elseif (is_numeric($objEnvData->fetchGlobal('lightbox_id'))) {
		
			$this->lightbox_id = $objEnvData->fetchGlobal('lightbox_id'); // The number of a specific image we want to view in the lightbox
		
		}
		
		require_once ('class.PixariaLightbox.php');
		
		$this->populateLightboxData();
		
		/*
		*	Now we choose the correct command to run
		*/
		
		// Select the appropriate action based on the command given
		switch ($objEnvData->fetchGlobal('cmd')) {
			
			case "addImageToLightbox":
			
				$this->addImageToLightbox();
			
			break;
			
			case "addImagesToLightbox":
			
				$this->addImagesToLightbox();
			
			break;
			
			case "removeImageFromLightbox":
			
				$this->removeImageFromLightbox();
			
			break;
			
			case "removeImagesFromLightbox":
			
				$this->removeImagesFromLightbox();
			
			break;
			
			case "showDefaultLightboxEditor":
			
				$this->showDefaultLightboxEditor();
			
			break;
			
			case "saveChangesToLightbox":
			
				$this->saveChangesToLightbox();
			
			break;
			
			case "showLightboxList":
			
				$this->showLightboxList();
			
			break;
			
			case "createNewLightbox":
			
				$this->createNewLightbox();
			
			break;
			
			case "makeLightboxActive":
			
				$this->makeLightboxActive();
			
			break;
			
			case "deleteLightbox":
			
				$this->deleteLightbox();
			
			break;
			
			case "formSendLightbox":
			
				$this->formSendLightbox();
			
			break;
			
			case "actionSendLightbox":
			
				$this->actionSendLightbox();
			
			break;
			
			case "downloadContactSheet":
			
				$this->downloadContactSheet();
			
			break;
			
			default:

				/*
				*	Next we need to do some security checking for the new sharing features
				*/
				
				// There is an access key provided but it is incorrect or there is no lightbox ID
				if ($this->lightbox_id == "") {
					
					// Display the 'no lightbox' warning
					$this->displayLightboxErrorNullbox();
				
				} elseif ($this->submitted_access_key != "" && $this->lightbox_access_key != $this->submitted_access_key) {
					
					// Display the permission denied warning
					$this->displayLightboxError();
				
				// The user is neither the owner of this lightbox or an administrator
				} elseif ($this->submitted_access_key == "" && $ses['psg_userid'] != $this->lightbox_userid && $ses['psg_administrator'] != true) {
					
					// Display the permission denied warning
					$this->displayLightboxError();
				
				}
				
				if (!is_numeric($this->lightbox_image)) { // We're viewing thumbnails
					
					// View the lightbox
					$this->showDefaultLightbox();
					
				} else { // We're viewing an image inside a gallery
				
					// View an image from this lightbox
					$this->showDefaultLightboxImage();

				}

			break;
		
		}
		
	}
	
	/*
	*	
	*	
	*	
	*/
	function populateLightboxData () {
		
		global $ses, $cfg, $objEnvData;
		
		// Override the default number of thumbnails to show on a single page if we're editing a lightbox
		if ($objEnvData->fetchGlobal('cmd') == "showDefaultLightboxEditor" || $objEnvData->fetchGlobal('cmd') == "downloadContactSheet") { $cfg['set']['thumb_per_page'] = 500; }
		
		// Instantiate the lightbox class
		$this->objLightbox = new PixariaLightbox($this->lightbox_id, $this->thumbnail_page, $this->lightbox_image, $this->submitted_access_key);
		
		// Get information about this lightbox
		$this->lightbox_id			= $this->objLightbox->getLightboxId();
		$this->lightbox_userid		= $this->objLightbox->getLightboxUserId();
		$this->lightbox_status		= $this->objLightbox->getLightboxStatus();
		$this->lightbox_type		= $this->objLightbox->getLightboxType();
		$this->lightbox_image_list	= $this->objLightbox->getLightboxMultiImageData();
		$this->lightbox_name		= $this->objLightbox->getLightboxName();
		$this->ipages				= $this->objLightbox->getMultiImagePageNavigation();
		$this->lightbox_image_data	= $this->objLightbox->getLightboxImageData();
		$this->lightbox_access_key	= $this->objLightbox->getLightboxAccessKey();
		$this->lightbox_pdf_url		= $this->objLightbox->getPDFContactSheetURL();
		$this->lightbox_is_mine		= $this->objLightbox->getIsMyLightbox();
		
	}
	
	/*
	*	
	*	
	*	
	*/
	function showDefaultLightbox () {
		
		global $smarty;
		
		// Load variables into Smarty
		$smarty->assign("referer",$this->referer);
		$smarty->assign("thumbnails",$this->lightbox_image_list);
		$smarty->assign("ipage_current",$this->ipages[0]);
		$smarty->assign("ipage_numbers",$this->ipages[1]);
		$smarty->assign("ipage_links",$this->ipages[2]);
		$smarty->assign("base_page",$this->ipages[4]);
		$smarty->assign("page_title","Lightbox: ".$this->lightbox_name);
		$smarty->assign("view_mode","lightbox");
		$smarty->assign("lightbox_id",$this->lightbox_id);
		$smarty->assign("lightbox_pdf_url",$this->lightbox_pdf_url);
		$smarty->assign("lightbox_is_mine",$this->lightbox_is_mine);
		
		// Check if the lightbox is empty
		if ($this->lightbox_image_list == "") {
			// Show the lightbox editor
			$this->showLightboxList();
		}
		
		// We need the lightbox javascript on this page
		$smarty->assign("js_lightbox",true);

		// Display the lightbox page
		$smarty->pixDisplay('index.lightbox/lightbox.view.tpl');
		
		// Stop running the script here
		exit;
		
	}
	
	/*
	*	
	*	
	*	
	*/
	function showDefaultLightboxImage () {
		
		global $smarty;
		
		// Load variables into Smarty
		$smarty->assign("ipage_current",$this->ipages[0]);
		$smarty->assign("ipage_numbers",$this->ipages[1]);
		$smarty->assign("ipage_links",$this->ipages[2]);
		$smarty->assign("view_mode","lightbox");
		$smarty->assign("image",$this->lightbox_image_data['image_detail']);
		$smarty->assign("thumbnails",$this->lightbox_image_data['image_neighbours']);
		$smarty->assign("thumbnails_urls",$this->lightbox_image_data['image_urls']);
		$smarty->assign("thumbnails_numbers",$this->lightbox_image_data['image_numbers']);
		$smarty->assign("thumbnails_count",$this->lightbox_image_data['image_count']);
		$smarty->assign("image_next",$this->lightbox_image_data['image_next_url']);
		$smarty->assign("image_prev",$this->lightbox_image_data['image_prev_url']);
		$smarty->assign("page_title",$this->lightbox_image_data['image_detail']['basic']['title']);
		$smarty->assign("lightbox_pdf_url",$this->lightbox_pdf_url);
		$smarty->assign("lightbox_is_mine",$this->lightbox_is_mine);
		
		// Display the lightbox page
		$smarty->pixDisplay('index.lightbox/lightbox.view.image.tpl');
		
		// Stop running the script here
		exit;
		
	}
	
	/*
	*	
	*	This function shows the user a form to edit their lightbox
	*	
	*/
	function showDefaultLightboxEditor () {
		
		// If the user isn't logged in, bounce to the login screen
		pix_authorise_user("temp");
		
		global $smarty, $cfg;
		
		// Load variables into Smarty
		$smarty->assign("thumbnails",$this->lightbox_image_list);
		$smarty->assign("ipage_current",$this->ipages[0]);
		$smarty->assign("ipage_numbers",$this->ipages[1]);
		$smarty->assign("ipage_links",$this->ipages[2]);
		$smarty->assign("page_title","Lightbox: ".$this->lightbox_name);
		$smarty->assign("view_mode","lightbox");
		$smarty->assign("lightbox_id",$this->lightbox_id);
		$smarty->assign("lightbox_name",$this->lightbox_name);
		
		// Display the lightbox page
		$smarty->pixDisplay('index.lightbox/lightbox.edit.tpl');
		
		// Stop running the script here
		exit;
		
	}
	
	function saveChangesToLightbox () {
				
		// If the user isn't logged in, bounce to the login screen
		pix_authorise_user("temp");
		
		global $objEnvData, $cfg, $smarty;
		
		$this->objLightbox->setLightboxName($objEnvData->fetchGlobal('lightbox_name'));
		$this->objLightbox->setLightboxOrder($objEnvData->fetchGlobal('lightbox_order'));
		$this->objLightbox->setLightboxDefault($objEnvData->fetchGlobal('lightbox_default'));
		
		$this->objLightbox->updateLightbox();
		
		// Print out the waiting screen
		$smarty->pixProcessing($cfg['sys']['base_url'].$cfg['fil']['index_lightbox'] . "?cmd=showLightboxList","1");
		
		// Stop running the script here
		exit;
		
	}
	
	/*
	*	
	*	
	*	
	*/
	function showLightboxList () {
		
		// If the user isn't logged in, bounce to the login screen
		pix_authorise_user("temp");
		
		global $cfg, $smarty, $objEnvData, $ses;
		
		// Load active lightboxes for this user
		$sql = "SELECT ".$cfg['sys']['table_lbox'].".*, ".$cfg['sys']['table_user'].".*, COUNT(lightbox_id) AS items FROM ".$cfg['sys']['table_user']."
				
				LEFT JOIN ".$cfg['sys']['table_lbox']." ON ".$cfg['sys']['table_user'].".userid = ".$cfg['sys']['table_lbox'].".userid
				
				LEFT JOIN ".$cfg['sys']['table_lbme']." ON ".$cfg['sys']['table_lbme'].".lightbox_id = ".$cfg['sys']['table_lbox'].".id
				
				WHERE ".$cfg['sys']['table_user'].".userid = '".$ses['psg_userid']."'
				
				AND ".$cfg['sys']['table_lbox'].".type = '10'
				
				AND ".$cfg['sys']['table_lbox'].".status = '10'
				
				GROUP BY ".$cfg['sys']['table_lbox'].".id";

		$result = $this->objLightbox->_dbl->sqlSelectRows($sql);
		
		if (is_array($result)) {
			
			$smarty->assign("lightbox_count",count($result));
			
			foreach ($result as $key => $value) {
				
				$lightbox_id[]			= $value['id'];
				$lightbox_items[]		= $value['items'];
				$lightbox_name[]		= stripslashes($value['name']);
				$lightbox_date[]		= $value['date'];
				$lightbox_type[]		= $value['type'];
				$lightbox_status[]		= $value['status'];
				$lightbox_access_key[]	= strtoupper(substr(md5($cfg['sys']['encryption_key'].$value['id']),0,6));
				
			}
			
		} else {
			
			$smarty->assign("lightbox_count",0);
		
		}
		
		
		
		// Load lightbox information into Smarty
		$smarty->assign("active_lightbox",$result[0]['active_image_box']);
		$smarty->assign("lightbox_id",$lightbox_id);
		$smarty->assign("lightbox_items",$lightbox_items);
		$smarty->assign("lightbox_name",$lightbox_name);
		$smarty->assign("lightbox_date",$lightbox_date);
		$smarty->assign("lightbox_type",$lightbox_type);
		$smarty->assign("lightbox_status",$lightbox_status);
		$smarty->assign("lightbox_access_key",$lightbox_access_key);
		
		
		// Define html page title
		$smarty->assign("page_title",$GLOBALS['_STR_']['LIGHTBOX_47_1']);
		
		// Output html from template file
		$smarty->pixDisplay('index.lightbox/lightbox.list.tpl');
	
		// Stop running the script here
		exit;
		
	}
	
	/*
	*	
	*	Display a page allowing the user to get a shring URL for a lightbox
	*	
	*/
	function displayLightboxError () {
	
		global $smarty, $cfg, $ses;
		
		// Define html page title
		$smarty->assign("page_title","Access Denied");
		
		// Output html from template file
		$smarty->pixDisplay('index.lightbox/lightbox.error.tpl');
	
		// Stop running the script here
		exit;
		
	}
	
	/*
	*	
	*	Display an error page
	*	
	*/
	function displayLightboxErrorNullbox () {
	
		global $smarty, $cfg, $ses;
		
		// Define html page title
		$smarty->assign("page_title","Access Denied");
		
		// Output html from template file
		$smarty->pixDisplay('index.lightbox/lightbox.error.nullbox.tpl');
	
		// Stop running the script here
		exit;
		
	}
	
	/*
	*	
	*	Change which lightbox is the active one for this user
	*	
	*/
	function makeLightboxActive () {
	
		// If the user isn't logged in, bounce to the login screen
		pix_authorise_user("temp");
		
		global $cfg, $objEnvData, $smarty;
	
		$this->objLightbox->makeLightboxActive($objEnvData->fetchGlobal('lightbox_id'));
		
		// Print out the waiting screen
		$smarty->pixProcessing($cfg['sys']['base_url'].$cfg['fil']['index_lightbox'] . "?cmd=showLightboxList","1");
	
		// Stop running the script here
		exit;
		
	}
	
	/*
	*	
	*	Delete a selected lightbox from the current user's collection 
	*	
	*/
	function deleteLightbox () {
		
		// If the user isn't logged in, bounce to the login screen
		pix_authorise_user("temp");
		
		global $cfg, $objEnvData, $smarty;
	
		// Delete the lightbox
		$this->objLightbox->deleteLightbox($objEnvData->fetchGlobal('lightbox_id'));

		// Print out the waiting screen
		$smarty->pixProcessing( $cfg['sys']['base_url'].$cfg['fil']['index_lightbox'] . "?cmd=showLightboxList","1");
		
		// Stop running the script here
		exit;
		
	}
	
	/*
	*	
	*	
	*	
	*/
	function createNewLightbox () {
		
		// If the user isn't logged in, bounce to the login screen
		pix_authorise_user("temp");
		
		global $ses, $cfg, $objEnvData, $smarty;
	
		$this->objLightbox->setLightboxName($objEnvData->fetchGlobal('lightbox_name'));
		$this->objLightbox->setLightboxDefault($objEnvData->fetchGlobal('lightbox_default'));
		
		$this->objLightbox->createLightbox();
		
		// Print out the waiting screen
		$smarty->pixProcessing( $cfg['sys']['base_url'].$cfg['fil']['index_lightbox'] . "?cmd=showLightboxList","1");
		
		// Stop running the script here
		exit;
		
	}
	
	/*
	*	
	*	
	*	
	*/
	function removeImageFromLightbox () {
		
		// If the user isn't logged in, bounce to the login screen
		pix_authorise_user("temp");
		
		global $ses, $cfg, $smarty, $objEnvData;
		
		$image_id = $objEnvData->fetchGlobal('image_id');
		
		// Remove image from lightbox
		if ($this->objLightbox->removeImageFromLightbox($image_id)) {
			
			// Print out the waiting screen
			$smarty->pixProcessing($cfg['sys']['base_url'].$cfg['fil']['index_lightbox']."?&amp;lid=$this->lightbox_id","1");
			
			// Stop running script
			exit;		
						
		} else { // Remove from lightbox failed
			
			// Print out the waiting screen
			$smarty->pixProcessing($cfg['sys']['base_url'].$cfg['fil']['index_lightbox']."?lid=$this->lightbox_id","1");
			
			// Stop running script
			exit;
		
		}

	}
	
	/*
	*	
	*	
	*	
	*/
	function removeImagesFromLightbox () {
	
		// If the user isn't logged in, bounce to the login screen
		pix_authorise_user("temp");
		
		global $ses, $cfg, $smarty, $objEnvData;
		
		// Get the array of images
		$images = $objEnvData->fetchGlobal('images');
		
		if (is_array($images)) {
			
			// Loop through the array of images and remove from lightbox
			foreach ($images as $key => $value) {
			
				// Delete images from lightbox
				$this->objLightbox->removeImageFromLightbox($value);
							
			}	
						
		} else { // No array of images to remove from the lightbox
			
			// Print out the waiting screen
			$smarty->pixProcessing($cfg['sys']['base_url'].$cfg['fil']['index_lightbox'],"1");
			
			// Stop running script
			exit;
		
		}

		// Print out the waiting screen
		$smarty->pixProcessing($cfg['sys']['base_url'].$cfg['fil']['index_lightbox']."?lid=$this->lightbox_id","1");
		
		// Stop running script
		exit;
			
	}
	
	/*
	*	
	*	
	*	
	*/
	function addImageToLightbox () {
	
		// If the user isn't logged in, bounce to the login screen
		pix_authorise_user("temp");
		
		global $ses, $cfg, $smarty, $objEnvData;
		
		// Get the value of the image ID
		$image_id = $objEnvData->fetchGlobal('image_id');
		
		// Add an image to the lightbox
		if ($this->objLightbox->addImageToLightbox($image_id)) {
			
			// Set the default value
			if ($cfg['set']['lightbox_href'] == "popup" || $cfg['set']['image_href'] == "popup") {
				
				// Load the image information class
				require_once ("class.PixariaImage.php");
				
				// Load information about this image for Smarty
				$objImage = new PixariaImage($image_id);
				
				// Get all the information available for this image
				$smarty->assign("image",$objImage->getImageDataComplete());
				
				// Set the page title
				$smarty->assign("page_title","Lightbox updated");
				
				// Show confirmation page in popup window
				$smarty->pixDisplay("index.lightbox/index.lightbox.popup.tpl");
			
			} else {
				
				// Update the lightbox with new data
				$this->populateLightboxData();
				
				// Set the referring page
				$this->referer = $_SERVER['HTTP_REFERER'];
			
				// Show the user's lightbox
				$this->showDefaultLightbox();
							
			}
			
			// Stop running script
			exit;		
						
		} else { // Add image to lightbox failed
			
			// Show the user's lightbox
			$this->showDefaultLightbox();
			
		}
	
	}
	
	/*
	*	
	*	
	*	
	*/
	function addImagesToLightbox () {
	
		// If the user isn't logged in, bounce to the login screen
		pix_authorise_user("temp");
		
		global $ses, $cfg, $smarty, $objEnvData;
		
		// Get the array of image data
		$images = $objEnvData->fetchGlobal('images');
		
		// Add an image to the lightbox
		if (is_array($images)) {
			
			// Loop through the array of images and add them to the lightbox
			foreach ($images as $key => $value) {
			
				// Add images to lightbox
				$this->objLightbox->addImageToLightbox($value);
							
			}

			// Update the lightbox with new data
			$this->populateLightboxData();
				
			// Set the referring page
			$this->referer = $_SERVER['HTTP_REFERER'];
			
			// Show the user's lightbox
			$this->showDefaultLightbox();
			
		} else { // Add image to lightbox failed
			
			// Show the user's lightbox
			$this->showDefaultLightbox();
		
		}
	
	}
	
	/*
	*	
	*	Displays a form which the user can use to send a lightbox URL to a friend
	*	
	*/
	function formSendLightbox () {
	
		// If the user isn't logged in, bounce to the login screen
		pix_authorise_user("basic");
		
		global $ses, $cfg, $smarty, $objEnvData;
		
		$lightbox_id = $objEnvData->fetchGlobal('lid');
				
		$smarty->assign("lightbox_id",$lightbox_id);
		$smarty->assign("lightbox_url",$cfg['sys']['base_url'] . $cfg['fil']['index_lightbox'] . "?lid=$lightbox_id&amp;key=" . strtoupper(substr(md5($cfg['sys']['encryption_key'].$lightbox_id),0,6)));
		
		// Define html page title
		$smarty->assign("page_title","Share a lightbox");
			
		// Output HTML
		$smarty->pixDisplay('index.lightbox/lightbox.share.form.tpl');
		
	}
	
	/*
	*	
	*	
	*	
	*/
	function actionSendLightbox () {
	
		// If the user isn't logged in, bounce to the login screen
		pix_authorise_user("basic");
		
		global $ses, $cfg, $smarty, $objEnvData;
		
		$lightbox_id 	= $objEnvData->fetchPost('lightbox_id');
		$name 			= $objEnvData->fetchPost('name');
		$email 			= $objEnvData->fetchPost('email');
		$message 		= $objEnvData->fetchPost('message');
		$subject 		= $objEnvData->fetchPost('subject');
		
		// Get information about the user sending the message
		$sender 		= getUserContactDetails($ses['psg_userid']);
		
		// Load information needed to send the mail
		$sender_email	= $sender[1];
		$sender_name	= $sender[0];
		
		// If the information submitted is valid
		if ($this->validateEmailAddress($email) && $name != "" && $message != "" && $subject != "") {
			
			$message .= "\n\n\nThis message was sent from the lightbox sharing feature at " . $cfg['sys']['base_url'] . ".";
			
			// Send the e-mail
			sendEmail($email, $name, $message, $sender_email, $sender_name, $subject);
			
			// Set the recipient name
			$smarty->assign("name",$name);

			// Set the lightbox id
			$smarty->assign("lightbox_id",$lightbox_id);

			// Define html page title
			$smarty->assign("page_title","Your message has been sent");
			
			// Output html from template file
			$smarty->pixDisplay('index.lightbox/lightbox.share.sent.tpl');
		
		// If there are problems with the form data
		} else {
			
			// Validate the e-mail address
			if (!$this->validateEmailAddress($email)) {
				$errors[] = "The e-mail address you entered was not valid.";
			} else {
				$smarty->assign("email",$email);
			}
			
			// Validate the user's name
			if ($name == "") {
				$errors[] = "You must enter your own name.";
			}
			
			// Validate the message
			if ($message == "") {
				$errors[] = "You must enter a message.";
			}
			
			// Validate the message
			if ($subject == "") {
				$errors[] = "You must enter a subject for your message.";
			}
			
			// Send errors to Smarty
			$smarty->assign("errors",$errors);
			
			// Tell Smarty there were errors
			$smarty->assign("problem",(bool)true);
			
			// Pass original data back to Smarty
			$smarty->assign("message",$message);
			$smarty->assign("name",$name);
			
			// Define html page title
			$smarty->assign("page_title","Share a lightbox");
			
			// Output html from template file
			$smarty->pixDisplay('index.lightbox/lightbox.share.form.tpl');

		}
		
	}
	
	/*
	*
	*	Validate a submitted e-mail address
	*
	*/
	function validateEmailAddress($email) {
	
		if (eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{1,4}",$email)) {
		
			return TRUE;
		
		} else {
		
			return FALSE;
			
		}
	
	}
	
	/*
	*	
	*	
	*	
	*/
	function downloadContactSheet () {
		
		global $cfg;
		
		// Load the free PDF library
		require(SYS_BASE_PATH.'resources/ext/FPDF/class.FreePDF.php');

		$pdf=new FPDF();
		$pdf->AddPage();
		$pdf->SetFont('Arial','',10);

		$i = 1;
		$horizontal_offset 	= 10;
		$vertical_offset	= 10;
		
		while ( list($key, $value) = @each($this->lightbox_image_list) ) {
			
			if ($cfg['set']['contact_sheet_quality'] == "low") {
				$image_path = $cfg['sys']['base_library'] . $value['basic']['path'] . "/160x160/" . $value['basic']['file_name_full'];
			} else {
				$image_path = $cfg['sys']['base_library'] . $value['basic']['path'] . "/".COMPING_DIR."/" . $value['basic']['file_name_full'];
			}
			
			$image_data = @getimagesize($image_path);
			
			$imagesx = $image_data[0];
			$imagesy = $image_data[1];
			$aspect_ratio = $imagesx / $imagesy;
			
			$title_text = textTruncate($value['basic']['title'],25);
			
			$title_width = $pdf->GetStringWidth($title_text);
			
			if ($imagesx > $imagesy) {
			
				$img_width			= 40;
				$img_height 		= ceil(40 / $aspect_ratio);
				$height_addition 	= ceil((40 - $img_height) / 2);
				$width_addition 	= 0;
				
			} else {
			
				$img_width			= ceil(40 * $aspect_ratio);
				$img_height 		= 40;
				$height_addition 	= 0;
				$width_addition 	= ceil((40 - $img_width) / 2);
			
			}
			
			$v_offset_text = $vertical_offset + 46;
			$h_offset_text = $horizontal_offset + ceil(40 - $title_width) / 2;
			
			$pdf->Image($image_path,$horizontal_offset+$width_addition,$vertical_offset+$height_addition,$img_width,$img_height);
			$pdf->Text($h_offset_text,$v_offset_text,$title_text);
			
			$horizontal_offset = $horizontal_offset + 50;
			
			if (($i % 4) === 0) { $horizontal_offset = 10; $vertical_offset = $vertical_offset + 55; }
			
			if (($i % 20) === 0) { $pdf->AddPage(); $horizontal_offset = 10; $vertical_offset = 10; }
			
			$i++;
		
		}
		
		$pdf->Output();

		exit;
		
	}
	
}

?>